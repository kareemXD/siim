@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
    <div class="row justify-content-center">
        <h3>Panel de Control</h3>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary text-center" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    @can('read_support')
        <div class="row justify-content-end">
            <button type="button" class="btn btn-primary mr-3 mb-2" data-toggle="modal" data-target="#modal-create-ticket"><i class="fas fa-ticket-alt mr-2"></i> Crear Ticket</button>
        </div>
    @endcan
    <div class="table-responsive">
        <table class="table dataTable">
            <thead>
                <tr>
                    <th scope="col">Folio</th>
                    <th scope="col">Fecha de Solicitud</th>
                    <th scope="col">Solicitante</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Prioridad</th>
                    <th scope="col">Estatus</th>
                </tr>
            </thead>
            <tbody>
                @if (count($tickets) > 0)
                    @foreach ($tickets as $ticket)
                        <tr class="edit-ticket" data-id="{{ $ticket->id }}">
                            <td>{{ $ticket->folio }}</td>
                            <td>{{ $ticket->created_at }}</td>
                            <td>{{ $ticket->user->name }}</td>
                            <td>{{ $ticket->descripcion }}</td>
                            <td style="background-color: {{ $ticket->priority->color }}; color: #fff;">{{ $ticket->priority->nombre }}</td>
                            @if ($ticket->status->id == 3)
                                <td style="background-color: {{ $ticket->status->color }}; color: #fff;">{{ $ticket->status->nombre }}, {{ $ticket->fecha_termino }}</td>
                            @else
                                <td style="background-color: {{ $ticket->status->color }}; color: #fff;">{{ $ticket->status->nombre }}</td>
                            @endif
                        </tr>
                    @endforeach
                @endif
                
            </tbody>
        </table>
    </div>
@endsection

@section('modals')
    @include('partials.modals.modal_status_ticket')
@endsection

@section('js')
    <script>
        $(function(){
            $("#support").addClass('active');
            $(document).on('dblclick', '.edit-ticket', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.ticket.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    
                    var modal = $("#modal-edit-ticket");
                    if(result.estatus_id == 1)
                    {
                        modal.find('.finished').css("display", "none");
                        modal.find('.comment').css("display", "none");
                        modal.find('.on-revision').css("display", "initial");
                        modal.find('.on-revision').data("id", result.id);
                    }

                    if(result.estatus_id == 2)
                    {
                        modal.find('.finished').css("display", "initial");
                        modal.find('.comment').css("display", "initial");
                        modal.find('.on-revision').css("display", "none");
                        modal.find('.ticket-id').val(result.id);
                        
                    }
                    
                    if(result.estatus_id == 3)
                    {
                        modal.find('.finished').css("display", "none");
                        modal.find('#comment').attr("disabled", true);
                        modal.find('.on-revision').css("display", "none");
                        modal.find('#comment').val(result.comentario);
                        modal.find('#date_f').val(result.fecha_termino);

                    }
                    modal.find('.priority-bar').css("background-color", result.prioridad_color);
                    modal.find('.priority-bar').text(result.prioridad_txt);
                    modal.find('.status-bar').css("background-color", result.estatus_color);
                    modal.find('.status-bar').text(result.estatus_txt);
                    modal.find('#ticket').val(result.folio);
                    modal.find('#user').val(result.solicitante);
                    modal.find('#description').val(result.descripcion);
                    modal.find('#date_i').val(result.created_at);
                    modal.modal("show");
                });
            });

            $(document).on("click", ".on-revision", function (){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('change.ticket.status') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id,
                        status: 2
                    },
                }).done(function(result){
                    location.reload();
                });
            });
            
            $(document).on("click", ".finished", function (){
                $("#finish-ticket").submit();
            });
        });
    </script>
@endsection