{{-- Modal Logout --}}
<div class="modal fade" id="modal-all-notifications" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="{{ route('update.profile') }}" aria-label="{{ __('Register') }}">
            @csrf
            <input type="hidden" name="operation" value="profile">
            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Notificaciones</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-xl-12">
                        <ul class="list-group">
                            <?php 
                                $user = Auth::user();
                                $notifications = $user->getAllNotifications();
                            ?>
                            @foreach ($notifications as $notification)
                                @if ($notification->estatus == 0)
                                    <li class="list-group-item view-notification-li" data-id="{{ $notification->id }}">{{ $notification->contenido }}</li>        
                                @else
                                    <li class="list-group-item view-notification-li vi" data-id="{{ $notification->id }}">
                                        {{ $notification->contenido }}
                                        <span class="notification-viewd">Visto <i class="fas fa-check"></i></span>
                                    </li>        
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                </div>
            </div>
        </form>

    </div>
</div>
<script>
    $(function(){
        $(document).on("click", ".view-notification-li", function(e){
            e.preventDefault();
            var _this = $(this),
                id = _this.data("id"),
                token = "{{ csrf_token() }}";

                var arr_not = ($(".notifications_number").text()).split("");
                var notifications_number = parseInt(arr_not[0]) - 1;
            $.ajax({
                url: "{{ route('update.notification') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                }
            }).done(function(result){
                var modal = $("#modal-notification");
                
                modal.find('#date').val(result.created_at);
                modal.find('#notification').val(result.contenido);
                modal.modal("show");
                
                _this.addClass("vi");
                _this.append('<span class="notification-viewd">Visto <i class="fas fa-check"></i></span>');
                if(notifications_number == 0)
                {
                    $(".notifications_number").remove();
                }else
                {
                    $(".notifications_number").text(notifications_number);
                }
                
            });
        });
    });
</script>