{{-- Modal Logout --}}
<div class="modal fade" id="modal-view-marker" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" action="{{ route('geo.update.marker') }}">
            @csrf
            <input type="hidden" name="marker" id="marker-id"/>
            <input type="hidden" name="type_marker" id="type-marker"/>
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Información de Registro</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col">
                            <label for="tipo-marker-up">Tipo de Registro</label>
                            <select class="custom-select" disabled id="tipo-marker-up" readonly name="tipo">
                                <option selected>Selecciona el Tipo</option>
                                <option value="op-covid">Casos Covid 19</option>
                                <option value="op-filtros">Filtros Sanitarios</option>
                                <option value="op-hospitales">Hospitales</option>
                                <option value="op-beneficiarios">Beneficiarios</option>
                                <option value="op-tiendas">Tiendas Participantes</option>
                                <option value="op-delitos">Delitos</option>
                                <option value="op-concentracion">Concetación de Personas</option>
                                <option value="op-transito">Incidentes de Transito</option>
                                <option value="op-cementerios">Cementerios</option>
                                <option value="op-empresas">Empresas</option>
                                <option value="op-actividades">Actividades Servicios Publicos</option>
                                <option value="op-violencia">Casos de Violencia</option>
                                <option value="op-dosificadores">Dosificadores</option>
                                <option value="op-comites">Comites de Salud</option>
                                <option value="op-consulta">Consulta Medica</option>
                                <option value="op-traslados">Traslado de Pacientes</option>
                                <option value="op-comedores">Comedores Solidarios</option>
                                <option value="op-caretas">Caretas</option>
                                <option value="op-direcciones">Direcciones</option>
                                <option value="op-aeconomicos">Apoyos Economicos</option>
                            </select>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Casos de Covid
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-covid" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="origin-covid">Origen</label>
                                <select id="origin-covid" class="custom-select" disabled name="covi_origin">
                                    <option selected>Seleccione el Origen</option>
                                    <option value="Centro de Salud">Centro de Salud</option>
                                    <option value="Linea Covid">Linea Covid</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="tipo-covid">Tipo de Contagio</label>
                                <select id="tipo-covid" class="custom-select" disabled name="covi_tipo">
                                    <option selected>Seleccione el tipo</option>
                                    <option value="Directo">Directo</option>
                                    <option value="Indirecto">Indirecto</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="name-covid">Nombre</label>
                                <input type="text" class="form-control" id="name-covid" disabled name="covi_name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="sex-covid">Sexo</label>
                                <select class="custom-select" id="sex-covid" disabled name="covi_sex">
                                    <option selected>Seleccione el Sexo</option>
                                    <option value="Hombre">Hombre</option>
                                    <option value="Mujer">Mujer</option>
                                    <option value="Indefinido">Indefinido</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="age-covid">Edad</label>
                                <input type="number" class="form-control" id="age-covid" disabled name="covi_age">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-covid">Localidad</label>
                                <select class="custom-select" id="localidad-covid" disabled name="covi_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="status-covid">Estatus</label>
                                <select class="custom-select" id="status-covid" name="covi_status">
                                    <option value="">Seleccione el Estatus</option>
                                    <option value="Positivo">Positivo</option>
                                    <option value="Alta">Alta</option>
                                    <option value="Defunción">Defunción</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="place-covid">Lugar de Atención</label>
                                <input type="text" class="form-control" id="place-covid" disabled name="covi_place">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="onservation-covid">Observaciones</label>
                                <textarea class="form-control" id="onservation-covid" rows="3" disabled name="covi_onservation"></textarea>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Beneficiarios
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-beneficiarios" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="folio-beneficiarios">Folio</label>
                                <input type="number" class="form-control" id="folio-beneficiarios" disabled name="ben_folio">
                            </div>
                            <div class="form-group col">
                                <label for="status-beneficiarios">¿ Califica ?</label>
                                <select class="custom-select" id="status-beneficiarios" disabled name="ben_status">
                                    <option value="">Seleccione el Estatus</option>
                                    <option value="Sí">Sí</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="name-beneficiarios">Nombre</label>
                                <input type="text" class="form-control" id="name-beneficiarios" disabled name="ben_name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="curp-beneficiarios">CURP</label>
                                <input type="text" class="form-control" id="curp-beneficiarios" disabled name="ben_curp">
                            </div>
                            <div class="form-group col">
                                <label for="ine-beneficiarios">Numero INE</label>
                                <input type="text" class="form-control" id="ine-beneficiarios" disabled name="ben_ine">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-beneficiarios">Domicilio</label>
                                <input type="text" class="form-control" id="address-beneficiarios" disabled name="ben_address">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-beneficiarios">Localidad</label>
                                <select class="custom-select" id="localidad-beneficiarios" disabled name="ben_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="sector-beneficiarios">Sector Economico</label>
                                <input type="text" class="form-control" id="sector-beneficiarios" disabled name="ben_sector">
                            </div>
                            <div class="form-group col">
                                <label for="phone-beneficiarios">Celular</label>
                                <input type="number" class="form-control" id="phone-beneficiarios" disabled name="ben_phone">
                            </div>
                            <div class="form-group col">
                                <label for="cp-beneficiarios">Codigo Postal</label>
                                <input type="number" class="form-control" id="cp-beneficiarios" disabled name="ben_cp">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="onservation-beneficiarios">Observaciones</label>
                                <textarea class="form-control" id="onservation-beneficiarios" rows="3" disabled name="ben_onservation"></textarea>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las tiendas participantes
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-tiendas" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="licencia-tiendas">Licencia</label>
                                <input type="text" class="form-control" id="licencia-tiendas" disabled name="tie_licencia">
                            </div>
                            <div class="form-group col">
                                <label for="giro-tiendas">Giro del Negocio</label>
                                <input type="text" class="form-control" id="giro-tiendas" disabled name="tie_giro">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="negocio-tiendas">Nombre del Negocio</label>
                                <input type="text" class="form-control" id="negocio-tiendas" disabled name="tie_negocio">
                            </div>
                            <div class="form-group col">
                                <label for="propietario-tiendas">Propietario</label>
                                <input type="text" class="form-control" id="propietario-tiendas" disabled name="tie_propietario">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-tiendas">Domicilio</label>
                                <input type="text" class="form-control" id="address-tiendas" disabled name="tie_address">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-tiendas">Localidad</label>
                                <select class="custom-select" id="localidad-tiendas" disabled name="tie_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="phone-tiendas">Telefono</label>
                                <input type="number" class="form-control" id="phone-tiendas" disabled name="tie_phone">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="date-tiendas">Fecha de Registro</label>
                                <input type="date" class="form-control" id="date-tiendas" disabled name="tie_date">
                            </div>
                            <div class="form-group col">
                                <label for="calca-tiendas">¿ Calca entregada ?</label>
                                <input type="text" class="form-control" id="calca-tiendas" disabled name="tie_calca">
                            </div>
                            <div class="form-group col">
                                <label for="caretas-tiendas">No. Caretas Entregadas</label>
                                <input type="numeric" class="form-control" id="caretas-tiendas" name="tie_caretas">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Siniestros de Seguridad Publica
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-delitos" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="reporte-delitos">Reporte</label>
                                <input type="text" class="form-control" id="reporte-delitos" disabled name="del_reporte">
                            </div>
                            <div class="form-group col">
                                <label for="delito-delitos">Tipo Delito</label>
                                <input type="text" class="form-control" id="delito-delitos" disabled name="del_delito">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-delitos">Ubicación</label>
                                <input type="text" class="form-control" id="address-delitos" disabled name="del_address">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-delitos">Localidad</label>
                                <select class="custom-select" id="localidad-delitos" disabled name="del_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="date-delitos">Fecha de Registro</label>
                                <input type="date" class="form-control" id="date-delitos" disabled name="del_date">
                            </div>
                            <div class="form-group col">
                                <label for="hour-delitos">Hora de Registro</label>
                                <input type="time" class="form-control" id="hour-delitos" disabled name="del_hour">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las Concentrasion de Personas
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-concentracion" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="delito-concentracion">Tipo</label>
                                <input type="text" class="form-control" id="delito-concentracion" disabled name="con_delito">
                            </div>
                            <div class="form-group col">
                                <label for="date-concentracion">Fecha de Registro</label>
                                <input type="date" class="form-control" id="date-concentracion" disabled name="con_date">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-concentracion">Ubicación</label>
                                <input type="text" class="form-control" id="address-concentracion" disabled name="con_address">
                            </div>
                            <div class="form-group col">
                                <label for="motivo-concentracion">Motivo</label>
                                <input type="text" class="form-control" id="motivo-concentracion" disabled name="con_motivo">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Incidentes de Transito
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-transito" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="folio-transito">Folio</label>
                                <input type="text" class="form-control" id="folio-transito" disabled name="tran_folio">
                            </div>
                            <div class="form-group col">
                                <label for="incidente-transito">Tipo Incidente</label>
                                <input type="text" class="form-control" id="incidente-transito" disabled name="tran_incidente">
                            </div>
                            <div class="form-group col">
                                <label for="afectado-transito">Estatus Afectado</label>
                                <input type="text" class="form-control" id="afectado-transito" disabled name="tran_afectado">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-transito">Ubicación</label>
                                <input type="text" class="form-control" id="address-transito" disabled name="tran_address">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-transito">Localidad</label>
                                <select class="custom-select" id="localidad-transito" disabled name="tran_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="date-transito">Fecha de Registro</label>
                                <input type="date" class="form-control" id="date-transito" disabled name="tran_date">
                            </div>
                            <div class="form-group col">
                                <label for="hour-transito">Hora de Registro</label>
                                <input type="time" class="form-control" id="hour-transito" disabled name="tran_hour">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Filtros Sanitarios
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-filtros" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="localidad-filtros">Localidad</label>
                                <input type="text" class="form-control" id="localidad-filtros" disabled name="tran_localidad">
                            </div>
                            <div class="form-group col">
                                <label for="personal-filtros">Numero de Personal</label>
                                <input type="text" class="form-control" id="personal-filtros" name="fil_personal">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="turno-mat-filtros">Responsable Turno Matutino</label>
                                <input type="text" class="form-control required" id="turno-mat-filtros" name="fil_turno_mat">
                            </div>
                            <div class="form-group col">
                                <label for="turno-ves-filtros">Responsable Turno Vespertino</label>
                                <input type="text" class="form-control required" id="turno-ves-filtros" name="fil_turno_ves">
                            </div>
                            <div class="form-group col">
                                <label for="turno-noc-filtros">Responsable Turno Nocturno</label>
                                <input type="text" class="form-control required" id="turno-noc-filtros" name="fil_turno_noc">
                            </div>
                        </div>
                        <div class="row gray-border">
                            <div class="form-group col mt-1">
                                <div class="table-filter" style="display:none; text-align:left;">
                                    <button type='button' data-toggle='modal' data-target='#modal-add-person-filter' class='btn btn-primary'><i class='fas fa-plus-circle ml-2'></i> Nuevo Registro</button>
                                </div>
                                <table id="table-filter-persons" class="table table-sm dataTable">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Turno</th>
                                            <th>Personas</th>
                                            <th class="w-15" style="width:15%;">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="filtro-personas"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Hospitales
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-hospitales" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="nombre-hospitales">Nombre</label>
                                <input type="text" class="form-control" id="nombre-hospitales" disabled name="hosp_nombre">
                            </div>
                            <div class="form-group col">
                                <label for="actividad-hospitales">Actividad</label>
                                <input type="text" class="form-control" id="actividad-hospitales" disabled name="hosp_actividad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="razon-hospitales">Razon Social</label>
                                <input type="text" class="form-control" id="razon-hospitales" disabled name="hosp_razon">
                            </div>
                            <div class="form-group col">
                                <label for="personas-hospitales">Numero de Personas</label>
                                <input type="text" class="form-control" id="personas-hospitales" disabled name="hosp_personas">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="vial-hospitales">Ubicación</label>
                                <input type="text" class="form-control" id="vial-hospitales" disabled name="hosp_vial">
                            </div>
                            <div class="form-group col">
                                <label for="camas-hospitales">¿Camas Disponibles?</label>
                                <input type="text" class="form-control" id="camas-hospitales" name="hosp_camas">
                            </div>
                        </div>
                        <div class="row gray-border">
                            <div class="form-group col mt-1">
                                <div class="table-filter" style="display:none; text-align:left;">
                                    <button type='button' data-toggle='modal' data-target='#modal-add-apoyo-hospital' class='btn btn-primary'><i class='fas fa-plus-circle ml-2'></i> Nuevo Registro</button>
                                </div>
                                <table class="table table-sm dataTable3">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Tipo</th>
                                            <th>Cantidad</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-apoyo-body"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Cementerios
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-cementerios" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="ubicacion-cementerios">Ubicación</label>
                                <input type="text" id="ubicacion-cementerios" class="form-control" name="ceme_ubicacion">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="espacios-cementerios">¿ Espacios Disponibles ?</label>
                                <input type="text" id="espacios-cementerios" class="form-control" name="ceme_espacios">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las Empresas
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-empresas" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="nombre-empresas">Nombre</label>
                                <input id="nombre-empresas" type="text" class="form-control" name="emp_nombre">
                            </div>
                            <div class="form-group col">
                                <label for="giro-empresas">Giro</label>
                                <input id="giro-empresas" type="text" class="form-control" name="emp_giro">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="domicilio-empresas">Domicilio</label>
                                <input id="domicilio-empresas" type="text" class="form-control" name="emp_domicilio">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-empresas">Localidad</label>
                                <select id="localidad-empresas"  class="custom-select" name="emp_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="ss-empresas">Empleados Afiliados al SS</label>
                                <select id="ss-empresas"  class="custom-select" name="emp_ss">
                                    <option value="">Seleccione el Estatus</option>
                                    <option value="Sí">Sí</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="empleados-empresas">No. Empleados</label>
                                <input id="empleados-empresas" type="number" class="form-control" name="emp_empleados">
                            </div>
                            <div class="form-group col">
                                <label for="despidos-empresas">No. Despidos</label>
                                <input id="despidos-empresas" type="number" class="form-control" name="emp_despidos">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="vacantes-empresas">No. Vacantes</label>
                                <input id="vacantes-empresas" type="number" class="form-control" name="emp_vacantes">
                            </div>
                            <div class="form-group col">
                                <label for="apoyos-empresas">No. Apoyos</label>
                                <input id="apoyos-empresas" type="number" class="form-control" name="emp_apoyos">
                            </div>
                            <div class="form-group col">
                                <label for="tipo-empresas">Tipo de Apoyos</label>
                                <input id="tipo-empresas" type="text" class="form-control" name="emp_tipo">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las actividades de Servicios Publicos
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-actividades" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="tipo-actividades">Tipo de Servicio</label>
                                <select id="tipo-actividades" class="custom-select" name="serv_tipo">
                                    <option value="">Seleccione un tipo</option>
                                    <option value="Sanitización de espacios públicos">Sanitización de espacios públicos</option>
                                    <option value="Reparacion de luminaria">Reparacion de luminaria</option>
                                    <option value="Limpieza">Limpieza</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="localidad-actividades">Localidad</label>
                                <select id="localidad-actividades"  class="custom-select" name="serv_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="numero-actividades">Cantidad de Servicios</label>
                                <input id="numero-actividades" type="number" class="form-control" name="serv_numero">
                            </div>
                            <div class="form-group col">
                                <label for="fecha-actividades">Fecha</label>
                                <input id="fecha-actividades" type="date" class="form-control" name="serv_fecha">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las actividades de Servicios Publicos
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-consume" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="nombre-consume">Nombre</label>
                                <input id="nombre-consume" type="text" class="form-control" name="consu_nombre">
                            </div>
                            <div class="form-group col">
                                <label for="tipo-consume">Tipo de Negocio</label>
                                <select id="tipo-consume"  class="custom-select" name="consu_tipo">
                                    <option value="">Seleccione un tipo</option>
                                    <option value="Formal">Formal</option>
                                    <option value="Informal">Informal</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="numero-consume">No. Empleados</label>
                                <input id="numero-consume" type="number" class="form-control" name="consu_numero">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-consume">Localidad</label>
                                <select id="localidad-consume"  class="custom-select" name="consu_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para casos de violencia
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-violencia" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="tipo-violencia">Tipo de Atención</label>
                                <input id="tipo-violencia" type="text" class="form-control" name="viol_tipo">
                            </div>
                            <div class="form-group col">
                                <label for="nombre-violencia">Nombre del Agresor</label>
                                <input id="nombre-violencia" type="text" class="form-control" name="viol_nombre">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="direccion-violencia">Dirección</label>
                                <input id="direccion-violencia" type="text" class="form-control" name="viol_direccion">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-violencia">Localidad</label>
                                <select id="localidad-violencia" class="custom-select" name="viol_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="numero-violencia">Personas Agredidas</label>
                                <input id="numero-violencia" type="text" class="form-control" name="viol_numero">
                            </div>
                            <div class="form-group col">
                                <label for="reincidencia-violencia">¿ Reincidencia ? </label>
                                <select id="reincidencia-violencia"  class="custom-select" name="viol_reincidencia">
                                    <option value="">Seleccione una opción</option>
                                    <option value="Sí">Sí</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                     {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Oromapas Dosificadores
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-dosificadores" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="numero-dosificadores">No. Identificador</label>
                                <input id="numero-dosificadores" type="text" class="form-control" name="dosi_numero">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-dosificadores">Localidad</label>
                                <select id="localidad-dosificadores" class="custom-select" name="dosi_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row gray-border">
                            <div class="form-group col mt-1">
                                <div class="table-filter" style="display:none; text-align:left;">
                                    <button type='button' data-toggle='modal' data-target='#modal-add-dosificador' class='btn btn-primary'><i class='fas fa-plus-circle ml-2'></i> Nuevo Registro</button>
                                </div>
                                <table id="table-dosificarores" class="table table-sm dataTable2">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Tipo</th>
                                            <th>Observaciones</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-dosificarores-body"></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Oromapas Reportes
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-orservicios" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="tipo-orservicios">Tipo de Servicio</label>
                                <select id="tipo-orservicios" class="custom-select" name="orserv_tipo">
                                    <option value="">Seleccione un tipo</option>
                                    <option value="Fuga">Fuga</option>
                                    <option value="Reparación de Alcantarillado">Reparación de Alcantarillado</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="date-orservicios">Fecha</label>
                                <input id="date-orservicios" type="date" class="form-control" name="orserv_date">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="direccion-orservicios">Dirección</label>
                                <input id="direccion-orservicios" type="text" class="form-control" name="orserv_direccion">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-orservicios">Localidad</label>
                                <select id="localidad-orservicios" class="custom-select" name="orserv_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="observation-orservicios">Observaciones</label>
                                <textarea id="observation-orservicios" name="orserv_observation" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Comites de Salud
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-comites" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="represent-comites">Representante</label>
                                <input id="represent-comites" type="text" class="form-control" name="comite_represent">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="localidad-comites">Localidad</label>
                                <select id="localidad-comites" class="custom-select" name="comite_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="reports-comites">No. de Reportes</label>
                                <input id="reports-comites" type="number" class="form-control" name="comite_reports">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Consultas Medicas
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-consulta" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="nombre-consulta">Nombre</label>
                                <input  id="nombre-consulta" type="text" class="form-control" name="consulta_nombre">
                            </div>
                            <div class="form-group col">
                                <label for="edad-consulta">Edad</label>
                                <input  id="edad-consulta" type="numeric" class="form-control" name="consulta_edad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="sex-consulta">Sexo</label>
                                <select id="sex-consulta"  class="custom-select" name="consulta_sex">
                                    <option selected>Seleccione el Sexo</option>
                                    <option value="Hombre">Hombre</option>
                                    <option value="Mujer">Mujer</option>
                                    <option value="Indefinido">Indefinido</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="localidad-consulta">Localidad</label>
                                <select id="localidad-consulta"  class="custom-select" name="consulta_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="sintomas-consulta">Sintomas</label>
                                <textarea id="sintomas-consulta"  name="consulta_sintomas" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Traslado de Pacientes
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-traslados" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="sex-traslados">Sexo</label>
                                <select id="sex-traslados"  class="custom-select" name="traslados_sex">
                                    <option selected>Seleccione el Sexo</option>
                                    <option value="Hombre">Hombre</option>
                                    <option value="Mujer">Mujer</option>
                                    <option value="Indefinido">Indefinido</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="ocupacion-traslados">Ocupación</label>
                                <input  id="ocupacion-traslados" type="text" class="form-control" name="traslados_ocupacion">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="derecho-traslados">¿ Derechoabiente ? </label>
                                <select id="derecho-traslados"  class="custom-select" name="traslados_derecho">
                                    <option value="">Seleccione una opción</option>
                                    <option value="Sí">Sí</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="lugar-traslados">Lugar de Traslado</label>
                                <input  id="lugar-traslados" type="text" class="form-control" name="traslados_lugar">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Comedores Solidarios
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-comedores" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="nombre-comedores">Nombre</label>
                                <input  id="nombre-comedores" type="text" class="form-control" name="comedores_nombre">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-comedores">Localidad</label>
                                <select id="localidad-comedores"  class="custom-select" name="comedores_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row gray-border">
                            <div class="form-group col mt-1">
                                <div class="table-filter" style="display:none; text-align:left;">
                                    <button type='button' data-toggle='modal' data-target='#modal-add-porciones' class='btn btn-primary'><i class='fas fa-plus-circle ml-2'></i> Nuevo Registro</button>
                                </div>
                                <table class="table table-sm dataTable4">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Porciones Entregadas</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Apoyos DIF
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-aeconomicos" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="tipo-aeconomicos">Tipo</label>
                                <input id="tipo-aeconomicos" type="text" class="form-control" name="aeconomicos_tipo">
                            </div>
                            <div class="form-group col">
                                <label for="cantidad-aeconomicos">Cantidad</label>
                                <input id="cantidad-aeconomicos" type="text" class="form-control" name="aeconomicos_cantidad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="name-aeconomicos">Nombre</label>
                                <input id="name-aeconomicos" type="text" class="form-control" name="aeconomicos_name">
                            </div>
                            <div class="form-group col">
                                <label for="curp-aeconomicos">CURP</label>
                                <input id="curp-aeconomicos" type="text" class="form-control" name="aeconomicos_curp">
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="form-group col">
                                <label for="ine-aeconomicos">Numero INE</label>
                                <input id="ine-aeconomicos" type="text" class="form-control" name="aeconomicos_ine">
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-aeconomicos">Calle</label>
                                <input id="address-aeconomicos" type="text" class="form-control" name="aeconomicos_address">
                            </div>
                            <div class="form-group col">
                                <label for="col-aeconomicos">Colonia</label>
                                <input id="col-aeconomicos" type="text" class="form-control" name="aeconomicos_col">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-aeconomicos">Localidad</label>
                                <select id="localidad-aeconomicos" class="custom-select" name="aeconomicos_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Apoyos DIF
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-apoyos" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="tipo-apoyo">Tipo</label>
                                <select id="tipo-apoyo"  class="custom-select" name="apoyo_tipo">
                                    <option value="">Seleccione una opción</option>
                                    <option value="DESPENSA">DESPENSA</option>
                                    <option value="APOYO ECONOMICO">APOYO ECONOMICO</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="name-apoyos">Nombre</label>
                                <input  id="name-apoyos" type="text" class="form-control" name="apoyo_name">
                            </div>
                            <div class="form-group col">
                                <label for="curp-apoyos">CURP</label>
                                <input  id="curp-apoyos" type="text" class="form-control" name="apoyo_curp">
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="form-group col">
                                <label for="ine-apoyos">Numero INE</label>
                                <input  id="ine-apoyos" type="text" class="form-control" name="apoyo_ine">
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-apoyos">Domicilio</label>
                                <input  id="address-apoyos" type="text" class="form-control" name="apoyo_address">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-apoyos">Localidad</label>
                                <select id="localidad-apoyos"  class="custom-select" name="apoyo_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las caretas de Padron y Licencias
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-caretas" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="name-caretas">Nombre</label>
                                <input  id="name-caretas" type="text" class="form-control" name="caretas_name">
                            </div>
                            <div class="form-group col">
                                <label for="tipo-caretas">Tipo de Negocio</label>
                                <select id="tipo-caretas"  class="custom-select" name="caretas_tipo">
                                    <option value="">Seleccione una opción</option>
                                    <option value="Fijo">Fijo</option>
                                    <option value="Semifijo">Semifijo</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col">
                                <label for="localidad-caretas">Localidad</label>
                                <select id="localidad-caretas"  class="custom-select" name="caretas_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="numero-caretas">No. Caretas Entregadas</label>
                                <input  id="numero-caretas" type="numeric" class="form-control" name="caretas_numero">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las Direcciones del Ayuntaminto
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-up-direcciones" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="name-direcciones">Nombre</label>
                                <input id="name-direcciones" type="text" class="form-control" name="direcciones_name">
                            </div>
                        </div>
                        <div class="row gray-border">
                            <div class="form-group col mt-1">
                                <div class="table-filter" style="display:none; text-align:left;">
                                    <button type='button' id="show-modal-direction-reports" class='btn btn-primary'><i class='fas fa-plus-circle ml-2'></i> Nuevo Registro</button>
                                </div>
                                <table class="table table-sm dataTable5">
                                    <thead>
                                        <tr>
                                            <th>Fecha</th>
                                            <th>Unidad</th>
                                            <th>Cantidad</th>
                                            <th>Costo</th>
                                        </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button class="btn btn-primary btn-submit-marker" id="btn-update-marker" disabled type="submit"><i class="fas fa-check mr-2"></i> Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on("click", "#show-modal-direction-reports", function(){
            var modal = $("#modal-add-direction-report");
            $.ajax({
                url: "{{ route('geo.get.units') }}",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                },
            }).done(function(result){
                console.log(result);
                modal.find("#unidad-report").empty();
                modal.find("#unidad-report").append("<option value=''>Seleccione una Unidad</option>");
                result.forEach(function(item){
                    modal.find("#unidad-report").append("<option value='"+item.unidad+"'>"+item.unidad+"</option>");
                });
                modal.modal("show");
            });
        });
    });
</script>