{{-- Modal Logout --}}
<div class="modal fade" id="modal-add-marker" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" action="{{ route('geo.register.marker') }}">
            @csrf
            <input type="hidden" id="latitud" name="latitud" />  
            <input type="hidden" id="longitud" name="longitud" />  
            <input type="hidden" id="x" name="x" />  
            <input type="hidden" id="y" name="y" />  
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Información de Registro</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col">
                            <label for="tipo-marker">Tipo de Registro</label>
                            <select class="custom-select select2" id="tipo-marker" name="tipo">
                                <option selected>Selecciona el Tipo</option>
                                <optgroup label="Generales">
                                    <option value="op-filtros">Filtros Sanitarios</option>
                                    <option value="op-direcciones">Direcciones</option>
                                </optgroup>
                                <optgroup label="Desarrollo y Bienestar Social">
                                    <option value="op-covid">Casos Covid 19</option>
                                    <option value="op-consulta">Consulta Medica</option>
                                    <option value="op-comites">Comites de Salud</option>
                                    <option value="op-beneficiarios">Beneficiarios Vales Despensa</option>
                                    <option value="op-violencia">Casos de Violencia</option>
                                </optgroup>
                                <optgroup label="Padrón y Licencias">
                                    <option value="op-tiendas">Tiendas Participantes</option>
                                    <option value="op-caretas">Caretas</option>
                                </optgroup>
                                <optgroup label="Seguridad Pública y Transito">
                                    <option value="op-delitos">Siniestros Seguridad Pública</option>
                                    <option value="op-concentracion">Concentración de Personas</option>
                                    <option value="op-transito">Incidentes de Transito</option>
                                </optgroup>
                                <optgroup label="Obras Públicas">
                                    <option value="op-cementerios">Cementerios</option>
                                </optgroup>
                                <optgroup label="Desarrollo Económico">
                                    <option value="op-empresas">Empresas</option>
                                    <option value="op-consume">Consume lo Local</option>
                                </optgroup>
                                <optgroup label="Servicios Públicos">
                                    <option value="op-actividades">Actividades Servicios Publicos</option>
                                </optgroup>
                                <optgroup label="Oromapas">
                                    <option value="op-dosificadores">Dosificadores</option>
                                    <option value="op-orservicios">Reportes Oromapas</option>
                                </optgroup>
                                <optgroup label="Protección Civil">
                                    <option value="op-traslados">Traslado de Pacientes</option>
                                </optgroup>
                                <optgroup label="DIF">
                                    <option value="op-comedores">Comedores Solidarios</option>
                                    <option value="op-apoyos">Apoyos DIF</option>
                                    <option value="op-aeconomicos">Apoyos Economicos</option>
                                </optgroup>
                            </select>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Casos de Covid
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-covid" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="origin-covid">Origen</label>
                                <select class="custom-select" name="covi_origin">
                                    <option selected>Seleccione el Origen</option>
                                    <option value="Centro de Salud">Centro de Salud</option>
                                    <option value="Linea Covid">Linea Covid</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="tipo-covid">Tipo de Contagio</label>
                                <select class="custom-select" name="covi_tipo">
                                    <option selected>Seleccione el tipo</option>
                                    <option value="Directo">Directo</option>
                                    <option value="Indirecto">Indirecto</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="name-covid">Nombre</label>
                                <input type="text" class="form-control" name="covi_name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="sex-covid">Sexo</label>
                                <select class="custom-select" name="covi_sex">
                                    <option selected>Seleccione el Sexo</option>
                                    <option value="Hombre">Hombre</option>
                                    <option value="Mujer">Mujer</option>
                                    <option value="Indefinido">Indefinido</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="age-covid">Edad</label>
                                <input type="number" class="form-control" name="covi_age">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-covid">Localidad</label>
                                <select class="custom-select" name="covi_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="status-covid">Estatus</label>
                                <select class="custom-select" name="covi_status">
                                    <option value="">Seleccione el Estatus</option>
                                    <option value="Positivo">Positivo</option>
                                    <option value="Alta">Alta</option>
                                    <option value="Defunción">Defunción</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="place-covid">Lugar de Atención</label>
                                <input type="text" class="form-control" name="covi_place">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="onservation-covid">Observaciones</label>
                                <textarea class="form-control" rows="3" name="covi_onservation"></textarea>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Beneficiarios
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-beneficiarios" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="folio-beneficiarios">Folio</label>
                                <input type="number" class="form-control" name="ben_folio">
                            </div>
                            <div class="form-group col">
                                <label for="status-beneficiarios">¿ Califica ?</label>
                                <select class="custom-select" name="ben_status">
                                    <option value="">Seleccione el Estatus</option>
                                    <option value="Sí">Sí</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="name-beneficiarios">Nombre</label>
                                <input type="text" class="form-control" name="ben_name">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="curp-beneficiarios">CURP</label>
                                <input type="text" class="form-control" name="ben_curp">
                            </div>
                            <div class="form-group col">
                                <label for="ine-beneficiarios">Numero INE</label>
                                <input type="text" class="form-control" name="ben_ine">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-beneficiarios">Domicilio</label>
                                <input type="text" class="form-control" name="ben_address">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-beneficiarios">Localidad</label>
                                <select class="custom-select" name="ben_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="sector-beneficiarios">Sector Economico</label>
                                <input type="text" class="form-control" name="ben_sector">
                            </div>
                            <div class="form-group col">
                                <label for="phone-beneficiarios">Celular</label>
                                <input type="number" class="form-control" name="ben_phone">
                            </div>
                            <div class="form-group col">
                                <label for="cp-beneficiarios">Codigo Postal</label>
                                <input type="number" class="form-control" name="ben_cp">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="onservation-beneficiarios">Observaciones</label>
                                <textarea class="form-control" rows="3" name="ben_onservation"></textarea>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las tiendas participantes
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-tiendas" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="licencia-tiendas">Licencia</label>
                                <input type="text" class="form-control" name="tie_licencia">
                            </div>
                            <div class="form-group col">
                                <label for="giro-tiendas">Giro del Negocio</label>
                                <input type="text" class="form-control" name="tie_giro">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="negocio-tiendas">Nombre del Negocio</label>
                                <input type="text" class="form-control" name="tie_negocio">
                            </div>
                            <div class="form-group col">
                                <label for="propietario-tiendas">Propietario</label>
                                <input type="text" class="form-control" name="tie_propietario">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-tiendas">Domicilio</label>
                                <input type="text" class="form-control" name="tie_address">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-tiendas">Localidad</label>
                                <select class="custom-select" name="tie_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="phone-tiendas">Telefono</label>
                                <input type="number" class="form-control" name="tie_phone">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="date-tiendas">Fecha de Registro</label>
                                <input type="date" class="form-control" name="tie_date">
                            </div>
                            <div class="form-group col">
                                <label for="calca-tiendas">¿ Calca entregada ?</label>
                                <input type="text" class="form-control" name="tie_calca">
                            </div>
                            <div class="form-group col">
                                <label for="caretas-tiendas">No. Caretas Entregadas</label>
                                <input type="number" class="form-control" name="tie_caretas">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Siniestros de Seguridad Publica
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-delitos" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="reporte-delitos">Reporte</label>
                                <input type="text" class="form-control" name="del_reporte">
                            </div>
                            <div class="form-group col">
                                <label for="delito-delitos">Tipo Delito</label>
                                <input type="text" class="form-control" name="del_delito">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-delitos">Ubicación</label>
                                <input type="text" class="form-control" name="del_address">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-delitos">Localidad</label>
                                <select class="custom-select" name="del_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="date-delitos">Fecha de Registro</label>
                                <input type="date" class="form-control" name="del_date">
                            </div>
                            <div class="form-group col">
                                <label for="hour-delitos">Hora de Registro</label>
                                <input type="time" class="form-control" name="del_hour">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las Concentrasion de Personas
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-concentracion" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="delito-concentracion">Tipo</label>
                                <input type="text" class="form-control" name="con_delito">
                            </div>
                            <div class="form-group col">
                                <label for="date-concentracion">Fecha de Registro</label>
                                <input type="date" class="form-control" name="con_date">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-concentracion">Ubicación</label>
                                <input type="text" class="form-control" name="con_address">
                            </div>
                            <div class="form-group col">
                                <label for="motivo-concentracion">Motivo</label>
                                <input type="text" class="form-control" name="con_motivo">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Incidentes de Transito
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-transito" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="folio-transito">Folio</label>
                                <input type="text" class="form-control" name="tran_folio">
                            </div>
                            <div class="form-group col">
                                <label for="incidente-transito">Tipo Incidente</label>
                                <input type="text" class="form-control" name="tran_incidente">
                            </div>
                            <div class="form-group col">
                                <label for="afectado-transito">Estatus Afectado</label>
                                <input type="text" class="form-control" name="tran_afectado">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-transito">Ubicación</label>
                                <input type="text" class="form-control" name="tran_address">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-transito">Localidad</label>
                                <select class="custom-select" name="tran_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="date-transito">Fecha de Registro</label>
                                <input type="date" class="form-control" name="tran_date">
                            </div>
                            <div class="form-group col">
                                <label for="hour-transito">Hora de Registro</label>
                                <input type="time" class="form-control" name="tran_hour">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Filtros Sanitarios
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-filtros" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="localidad-filtros">Localidad</label>
                                <select class="custom-select" name="fil_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="personal-filtros">Numero de Personal</label>
                                <input type="text" class="form-control" name="fil_personal">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="turno_mat-filtros">Responsable Turno Matutino</label>
                                <input type="text" class="form-control" name="fil_turno_mat">
                            </div>
                            <div class="form-group col">
                                <label for="turno_ves-filtros">Responsable Turno Vespertino</label>
                                <input type="text" class="form-control" name="fil_turno_ves">
                            </div>
                            <div class="form-group col">
                                <label for="turno_noc-filtros">Responsable Turno Nocturno</label>
                                <input type="text" class="form-control" name="fil_turno_noc">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Cementerios
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-cementerios" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="ubicacion-cementerios">Ubicación</label>
                                <input type="text" class="form-control" name="ceme_ubicacion">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="espacios-cementerios">¿ Espacios Disponibles ?</label>
                                <input type="number" class="form-control" name="ceme_espacios">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las Empresas
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-empresas" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="nombre-empresas">Nombre</label>
                                <input type="text" class="form-control" name="emp_nombre">
                            </div>
                            <div class="form-group col">
                                <label for="giro-empresas">Giro</label>
                                <input type="text" class="form-control" name="emp_giro">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="domicilio-empresas">Domicilio</label>
                                <input type="text" class="form-control" name="emp_domicilio">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-empresas">Localidad</label>
                                <select class="custom-select" name="emp_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="ss-empresas">Empleados Afiliados al SS</label>
                                <select class="custom-select" name="emp_ss">
                                    <option value="">Seleccione el Estatus</option>
                                    <option value="Sí">Sí</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="empleados-empresas">No. Empleados</label>
                                <input type="number" class="form-control" name="emp_empleados">
                            </div>
                            <div class="form-group col">
                                <label for="despidos-empresas">No. Despidos</label>
                                <input type="number" class="form-control" name="emp_despidos">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="vacantes-empresas">No. Vacantes</label>
                                <input type="number" class="form-control" name="emp_vacantes">
                            </div>
                            <div class="form-group col">
                                <label for="apoyos-empresas">No. Apoyos</label>
                                <input type="number" class="form-control" name="emp_apoyos">
                            </div>
                            <div class="form-group col">
                                <label for="tipo-empresas">Tipo de Apoyos</label>
                                <input type="text" class="form-control" name="emp_tipo">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las actividades de Servicios Publicos
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-actividades" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="tipo-actividades">Tipo de Servicio</label>
                                <select class="custom-select" name="serv_tipo">
                                    <option value="">Seleccione un tipo</option>
                                    <option value="Sanitización de Parque/Plaza">Sanitización de Parqu4e/Plaza</option>
                                    <option value="Sanitización de Escuela">Sanitización de Escuela</option>
                                    <option value="Sanitización de Hospital">Sanitización de Hospital</option>
                                    <option value="Sanitización de Oficinas Publicas">Sanitización de Oficinas Publicas</option>
                                    <option value="Sanitización de Otros">Sanitización de Otros</option>
                                    <option value="Reparacion de luminaria">Reparacion de luminaria</option>
                                    <option value="Limpieza">Limpieza</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="localidad-actividades">Localidad</label>
                                <select class="custom-select" name="serv_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="numero-actividades">Cantidad de Servicios</label>
                                <input type="number" class="form-control" name="serv_numero">
                            </div>
                            <div class="form-group col">
                                <label for="fecha-actividades">Fecha</label>
                                <input type="date" class="form-control" name="serv_fecha">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Consume lo Local
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-consume" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="nombre-consume">Nombre</label>
                                <input type="text" class="form-control" name="consu_nombre">
                            </div>
                            <div class="form-group col">
                                <label for="tipo-consume">Tipo de Negocio</label>
                                <select class="custom-select" name="consu_tipo">
                                    <option value="">Seleccione un tipo</option>
                                    <option value="Formal">Formal</option>
                                    <option value="Informal">Informal</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="numero-consume">No. Empleados</label>
                                <input type="number" class="form-control" name="consu_numero">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-consume">Localidad</label>
                                <select class="custom-select" name="consu_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para casos de violencia
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-violencia" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="tipo-violencia">Tipo de Atención</label>
                                <input type="text" class="form-control" name="viol_tipo">
                            </div>
                            <div class="form-group col">
                                <label for="nombre-violencia">Nombre del Agresor</label>
                                <input type="text" class="form-control" name="viol_nombre">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="direccion-violencia">Dirección</label>
                                <input type="text" class="form-control" name="viol_direccion">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-violencia">Localidad</label>
                                <select class="custom-select" name="viol_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="numero-violencia">Personas Agredidas</label>
                                <input type="number" class="form-control" name="viol_numero">
                            </div>
                            <div class="form-group col">
                                <label for="reincidencia-violencia">¿ Reincidencia ? </label>
                                <select class="custom-select" name="viol_reincidencia">
                                    <option value="">Seleccione una opción</option>
                                    <option value="Sí">Sí</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Oromapas Dosificadores
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-dosificadores" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="numero-dosificadores">No. Identificador</label>
                                <input type="text" class="form-control" name="dosi_numero">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-dosificadores">Localidad</label>
                                <select class="custom-select" name="dosi_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Oromapas Reportes
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-orservicios" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="tipo-orservicios">Tipo de Servicio</label>
                                <select class="custom-select" name="orserv_tipo">
                                    <option value="">Seleccione un tipo</option>
                                    <option value="Fuga">Fuga</option>
                                    <option value="Reparación de Alcantarillado">Reparación de Alcantarillado</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="date-orservicios">Fecha</label>
                                <input type="date" class="form-control" name="orserv_date">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="direccion-orservicios">Dirección</label>
                                <input type="text" class="form-control" name="orserv_direccion">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-orservicios">Localidad</label>
                                <select class="custom-select" name="orserv_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="observation-orservicios">Observaciones</label>
                                <textarea name="orserv_observation" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Oromapas Reportes
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-comites" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="represent-comites">Representante</label>
                                <input type="text" class="form-control" name="comite_represent">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="localidad-comites">Localidad</label>
                                <select class="custom-select" name="comite_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="reports-comites">No. de Reportes</label>
                                <input type="number" class="form-control" name="comite_reports">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Consultas Medicas
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-consulta" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="nombre-consulta">Nombre</label>
                                <input type="text" class="form-control" name="consulta_nombre">
                            </div>
                            <div class="form-group col">
                                <label for="edad-consulta">Edad</label>
                                <input type="number" class="form-control" name="consulta_edad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="sex-consulta">Sexo</label>
                                <select class="custom-select" name="consulta_sex">
                                    <option selected>Seleccione el Sexo</option>
                                    <option value="Hombre">Hombre</option>
                                    <option value="Mujer">Mujer</option>
                                    <option value="Indefinido">Indefinido</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="localidad-consulta">Localidad</label>
                                <select class="custom-select" name="consulta_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="sintomas-consulta">Sintomas</label>
                                <textarea name="consulta_sintomas" class="form-control" rows="5"></textarea>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Traslado de Pacientes
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-traslados" style="display:none;">
                        
                        <div class="row">
                            <div class="form-group col">
                                <label for="sex-traslados">Sexo</label>
                                <select class="custom-select" name="traslados_sex">
                                    <option selected>Seleccione el Sexo</option>
                                    <option value="Hombre">Hombre</option>
                                    <option value="Mujer">Mujer</option>
                                    <option value="Indefinido">Indefinido</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="ocupacion-traslados">Ocupación</label>
                                <input type="text" class="form-control" name="traslados_ocupacion">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="derecho-traslados">¿ Derechoabiente ? </label>
                                <select class="custom-select" name="traslados_derecho">
                                    <option value="">Seleccione una opción</option>
                                    <option value="Sí">Sí</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="lugar-traslados">Lugar de Traslado</label>
                                <input type="text" class="form-control" name="traslados_lugar">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Comedores Solidarios
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-comedores" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="nombre-comedores">Nombre</label>
                                <input type="text" class="form-control" name="comedores_nombre">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-comedores">Localidad</label>
                                <select class="custom-select" name="comedores_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para los Apoyos DIF
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-aeconomicos" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="tipo-aeconomicos">Tipo</label>
                                <input type="text" class="form-control" name="aeconomicos_tipo">
                            </div>
                            <div class="form-group col">
                                <label for="cantidad-aeconomicos">Cantidad</label>
                                <input type="text" class="form-control" name="aeconomicos_cantidad">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="name-aeconomicos">Nombre</label>
                                <input type="text" class="form-control" name="aeconomicos_name">
                            </div>
                            <div class="form-group col">
                                <label for="curp-aeconomicos">CURP</label>
                                <input type="text" class="form-control" name="aeconomicos_curp">
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="form-group col">
                                <label for="ine-aeconomicos">Numero INE</label>
                                <input type="text" class="form-control" name="aeconomicos_ine">
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-aeconomicos">Calle</label>
                                <input type="text" class="form-control" name="aeconomicos_address">
                            </div>
                            <div class="form-group col">
                                <label for="col-aeconomicos">Colonia</label>
                                <input type="text" class="form-control" name="aeconomicos_col">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-aeconomicos">Localidad</label>
                                <select class="custom-select" name="aeconomicos_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para Apoyos Economicos DIF
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-apoyos" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="tipo-apoyo">Tipo</label>
                                <select class="custom-select" name="apoyo_tipo">
                                    <option value="">Seleccione una opción</option>
                                    <option value="APOYO ECONOMICO">APOYO ECONOMICO</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col">
                                <label for="name-apoyos">Nombre</label>
                                <input type="text" class="form-control" name="apoyo_name">
                            </div>
                            <div class="form-group col">
                                <label for="curp-apoyos">CURP</label>
                                <input type="text" class="form-control" name="apoyo_curp">
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="form-group col">
                                <label for="ine-apoyos">Numero INE</label>
                                <input type="text" class="form-control" name="apoyo_ine">
                            </div>
                        </div> --}}
                        <div class="row">
                            <div class="form-group col">
                                <label for="address-apoyos">Domicilio</label>
                                <input type="text" class="form-control" name="apoyo_address">
                            </div>
                            <div class="form-group col">
                                <label for="localidad-apoyos">Localidad</label>
                                <select class="custom-select" name="apoyo_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las caretas de Padron y Licencias
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-caretas" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="name-caretas">Nombre</label>
                                <input type="text" class="form-control" name="caretas_name">
                            </div>
                            <div class="form-group col">
                                <label for="tipo-caretas">Tipo de Negocio</label>
                                <select class="custom-select" name="caretas_tipo">
                                    <option value="">Seleccione una opción</option>
                                    <option value="Fijo">Fijo</option>
                                    <option value="Semifijo">Semifijo</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="form-group col">
                                <label for="localidad-caretas">Localidad</label>
                                <select class="custom-select" name="caretas_localidad">
                                    <option value="">Seleccione la Localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{ $localidad->nombre_localidad }}">{{$localidad->nombre_localidad}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col">
                                <label for="numero-caretas">No. Caretas Entregadas</label>
                                <input type="numeric" class="form-control" name="caretas_numero">
                            </div>
                        </div>
                    </div>
                    {{-- /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    /////
                    ///// Registro para las Direcciones del Ayuntaminto
                    /////
                    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// --}}
                    <div id="op-direcciones" style="display:none;">
                        <div class="row">
                            <div class="form-group col">
                                <label for="name-direcciones">Nombre</label>
                                <input type="text" class="form-control" name="direcciones_name">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button class="btn btn-primary btn-submit-marker" disabled type="submit"><i class="fas fa-check mr-2"></i> Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on("change", "#tipo-marker", function(){
            var _this = $(this);
            if($(".panel-visible").is(":visible")){
                $(".panel-visible").animate({height:"toggle"}, "slow", function(){
                    $(".panel-visible").find("input").each(function() {
                        $(this).removeAttr("required");
                    });
                    $(".panel-visible").find("select").each(function() {
                        $(this).removeAttr("required");
                    });
                    $(".panel-visible").removeClass("panel-visible");

                    $("#"+_this.val()).addClass("panel-visible");
                    $("#"+_this.val()).find("input").each(function() {
                        $(this).attr("required", true);
                    });
                     $("#"+_this.val()).find("select").each(function() {
                        $(this).attr("required", true);
                    });
                    $("#"+_this.val()).animate({height:"toggle"}, "slow");
                });
            }else{
                $("#"+_this.val()).find("input").each(function() {
                    $(this).attr("required", true);
                });
                    $("#"+_this.val()).find("select").each(function() {
                    $(this).attr("required", true);
                });
                $("#"+_this.val()).addClass("panel-visible");
                $("#"+_this.val()).animate({height:"toggle"}, "slow");
            }
            $(".btn-submit-marker").removeAttr("disabled");
        });
    });
</script>