{{-- Modal Logout --}}
<div class="modal fade" id="modal-add-porciones" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" id="form-porciones">
            @csrf
            <input type="hidden" id="comedor-id" name="comedor_id">
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Información de Registro</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col">
                            <label for="date-porcion">Fecha</label>
                            <input type="date" class="form-control" name="porcion_date">
                        </div>
                        <div class="form-group col">
                            <label for="cant-porcion">porciones</label>
                            <input type="number" class="form-control" name="porcion_cant">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button class="btn btn-primary" id="btn-porciones" type="button"><i class="fas fa-check mr-2"></i> Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on("click", "#btn-porciones", function(){
            // e.preventDefault();
            var data = $("#form-porciones").serialize();
            $.ajax({
                url: "{{ route('geo.register.porciones') }}",
                type: "POST",
                data: data
            }).done(function(result){
                // console.log(result);
                table4.row.add([result.fecha, result.porciones]).draw( false );
                $("#modal-add-porciones").modal("hide");
            });
        });
    });
</script>