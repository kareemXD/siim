{{-- Modal Logout --}}
<div class="modal fade" id="modal-add-dosificador" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" id="form-dosificadores">
            @csrf
            <input type="hidden" id="dosificador-id" name="dosificador_id">
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Información de Registro</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col">
                            <label for="tipo-dosifi">Tipo de Acción</label>
                            <select class="custom-select" name="dosifi_tipo">
                                <option value="">Seleccione un tipo</option>
                                <option value="Verificación">Verificación</option>
                                <option value="Cloración">Cloración</option>
                            </select>
                        </div>
                        <div class="form-group col">
                            <label for="date-dosifi">Fecha</label>
                            <input type="date" class="form-control" name="dosifi_date">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label for="observation-dosifi">Observaciones</label>
                            <textarea name="dosifi_observation" class="form-control" rows="5"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button class="btn btn-primary" id="btn-submit-dosificador" type="button"><i class="fas fa-check mr-2"></i> Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on("click", "#btn-submit-dosificador", function(){
            // e.preventDefault();
            var data = $("#form-dosificadores").serialize();
            $.ajax({
                url: "{{ route('geo.register.dosificador') }}",
                type: "POST",
                data: data
            }).done(function(result){
                // console.log(result);
                table2.row.add([result.fecha, result.tipo, result.observaciones]).draw( false );
                $("#modal-add-dosificador").modal("hide");
            });
        });
    });
</script>