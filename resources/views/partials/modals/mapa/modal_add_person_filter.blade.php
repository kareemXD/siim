{{-- Modal Logout --}}
<div class="modal fade" id="modal-add-person-filter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" id="form-filter-persons">
            @csrf
            <input type="hidden" id="filter-id" name="filter_id">
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Información de Registro</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col">
                            <label for="date-person">Fecha</label>
                            <input type="date" class="form-control" name="person_date">
                        </div>
                        <div class="form-group col">
                            <label for="turn-person">Turno</label>
                            <select class="custom-select" name="person_turn">
                                <option value="">Seleccione el Turno</option>
                                <option value="Matutino">Matutino</option>
                                <option value="Vespertino">Vespertino</option>
                                <option value="Nocturno">Nocturno</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label for="vehi-person">No. Vehiculos</label>
                            <input type="number" class="form-control" name="person_vehi">
                        </div>
                        <div class="form-group col">
                            <label for="persons-person">No. Personas</label>
                            <input type="number" class="form-control" name="person_persons">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label for="locals-person">No. Locales</label>
                            <input type="number" class="form-control" name="person_locals">
                        </div>
                        <div class="form-group col">
                            <label for="foran-person">No. Foraneos</label>
                            <input type="number" class="form-control" name="person_foran">
                        </div>
                        <div class="form-group col">
                            <label for="returns-person">No. Retornos</label>
                            <input type="number" class="form-control" name="person_returns">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button class="btn btn-primary" id="btn-submit-filter-person" type="button"><i class="fas fa-check mr-2"></i> Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on("click", "#btn-submit-filter-person", function(){
            // e.preventDefault();
            var data = $("#form-filter-persons").serialize();
            $.ajax({
                url: "{{ route('geo.register.filter.person') }}",
                type: "POST",
                data: data
            }).done(function(result){
                // console.log(result);
                table.row.add([result.fecha, result.turno, result.personas, "<button type='button' data-id='"+result.id+"' class='btn btn-primary btn-view-data-filtro'><i class='fas fa-eye'></i></button>"]).draw( false );;
                $("#modal-add-person-filter").modal("hide");
            });
        });
    });
</script>