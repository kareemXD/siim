{{-- Modal Logout --}}
<div class="modal fade" id="modal-add-apoyo-hospital" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" id="form-apoyo-hospital">
            @csrf
            <input type="hidden" id="hospital-id" name="hospital_id">
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Información de Registro</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col">
                            <label for="tipo-hosp">Tipo de Apoyo</label>
                            <select class="custom-select" name="hosp_tipo">
                                <option value="">Seleccione un tipo</option>
                                <option value="Caretas">Caretas</option>
                                <option value="Pruebas Rapidas">Pruebas Rapidas</option>
                                <option value="Ropa Quirurgica">Ropa Quirurgica</option>
                                <option value="Cubrebocas">Cubrebocas</option>
                                <option value="Tunel Sanitizante">Tunel Sanitizante</option>
                                <option value="Sanitización a Hospital">Sanitización a Hospital</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label for="date-hosp">Fecha</label>
                            <input type="date" class="form-control" name="hosp_date">
                        </div>
                        <div class="form-group col">
                            <label for="cant-hosp">Cantidad</label>
                            <input type="number" class="form-control" name="hosp_cant">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button class="btn btn-primary" id="btn-apoyo-hospital" type="button"><i class="fas fa-check mr-2"></i> Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on("click", "#btn-apoyo-hospital", function(){
            // e.preventDefault();
            var data = $("#form-apoyo-hospital").serialize();
            $.ajax({
                url: "{{ route('geo.register.apoyo.hospital') }}",
                type: "POST",
                data: data
            }).done(function(result){
                // console.log(result);
                table3.row.add([result.fecha, result.tipo, result.cantidad]).draw( false );
                $("#modal-add-apoyo-hospital").modal("hide");
            });
        });
    });
</script>