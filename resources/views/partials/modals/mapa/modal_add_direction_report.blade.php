{{-- Modal Logout --}}
<div class="modal fade" id="modal-add-direction-report" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" id="form-reports">
            @csrf
            <input type="hidden" id="direction-id" name="direction_id">
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Información de Registro</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col">
                            <label for="date-report">Fecha</label>
                            <input type="date" class="form-control required" name="report_date">
                        </div>
                        <div class="form-group col">
                            <label for="unidad-report">Unidad</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-primary btn-other" data-name="report_unidad" data-class="required" type="button"  data-toggle="tooltip" data-placement="top" title="Añadir una Unidad"><i class="fas fa-plus-circle"></i></button>
                                </div>
                                <select id="unidad-report" class="custom-select required" name="report_unidad">
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label for="cantidad-report">Cantidad</label>
                            <input type="number" class="form-control required" name="report_cantidad">
                        </div>
                        <div class="form-group col">
                            <label for="cost-report">Costo</label>
                            <input type="number" class="form-control required" name="report_cost">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button class="btn btn-primary" id="btn-reports" type="button"><i class="fas fa-check mr-2"></i> Guardar</button>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on("click", ".btn-other", function(){
            $('[data-toggle="tooltip"]').tooltip("hide");
            var parent = $(this).parents(".input-group");
            var _this = $(this);
            parent.find("#unidad-report").fadeOut("slow", function(){
                parent.find("#unidad-report").removeClass("required");
                parent.append('<input type="text" class="form-control inputInserted '+_this.data("class")+'" name="new_'+_this.data("name")+'" placeholder="Escriba el nombre de la Unidad"> ').show('slow');
                _this.html('<i class="fas fa-minus-circle"></i>');
                _this.removeClass("btn-other");
                _this.addClass("remove-unit");
                _this.attr("data-original-title", "Seleccionar Unidad").tooltip('show');
            });
        });

        $(document).on("click", ".remove-unit", function(){
            $('[data-toggle="tooltip"]').tooltip("hide");
            var parent = $(this).parents(".input-group");
            var _this = $(this);
            parent.find(".inputInserted").fadeOut("slow", function(){
                $(this).remove();
                parent.find("#unidad-report").fadeIn("slow")
                parent.find("#unidad-report").addClass("required")
                _this.html('<i class="fas fa-plus-circle"></i>')
                _this.removeClass("remove-unit");
                _this.addClass("btn-other");
                _this.attr("data-original-title", "Añadir una Unidad").tooltip('show');
            });
        });

        $(document).on("click", "#btn-reports", function(){
            let flag = true;
            $("#form-reports").find(".required").each(function() {
                if(!$(this).val() || $(this).val().length == 0){
                    flag = false;
                    $(this).addClass("is-invalid");
                    $(this).parent().find(".select2").addClass("is-invalid");
                }else{
                    if($(this).hasClass("is-invalid")){
                        $(this).removeClass("is-invalid");
                        $(this).parent().find(".select2").removeClass("is-invalid");
                    }
                }
            });
            if(flag)
            {
                var data = $("#form-reports").serialize();
                $.ajax({
                    url: "{{ route('geo.register.report') }}",
                    type: "POST",
                    data: data
                }).done(function(result){
                    // console.log(result);
                    table5.row.add([result.fecha, result.unidad, result.cantidad, result.costo]).draw( false );
                    $("#modal-add-direction-report").modal("hide");
                });
            }
            // e.preventDefault();
        });
    });
</script>