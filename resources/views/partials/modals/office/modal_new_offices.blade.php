{{-- Modal Logout --}}
<div class="modal fade" id="modal-new-offices" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" action="{{ route('store.office.data') }}" class='NewOffice' aria-label="{{ __('Register') }}" enctype="multipart/form-data">
            @csrf            
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Oficio</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                <input type="checkbox" id="pmdu_field" name="pmdu_field" value="true">
                            </div>
                        </div>
                        <input type="text" disabled class="form-control" value="¿ Recepción de escrito del PMDU ?">
                    </div>
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="">No. Oficio</label>
                            <input class="form-control {{ $errors->has('no_oficio') ? ' is-invalid' : '' }}" type="text" name="no_oficio"  required>                       
                        </div>
                        <div class="col-xl-6">
                            <label for="">Emisor</label>
                            <input class="form-control {{ $errors->has('emisor') ? ' is-invalid' : '' }}" type="text" name="emisor"  required> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <label for="">Organismo o Dependencia</label>
                            <input class="form-control {{ $errors->has('dependencia') ? ' is-invalid' : '' }}" type="text" name="dependencia"  required> 
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="">Fecha Emision</label>
                            <input class="form-control {{ $errors->has('fecha_emision') ? ' is-invalid' : '' }}" type="date" name="fecha_emision"  required>                       
                        </div>
                        <div class="col-xl-6">
                            <label for="">Fecha Recepcion</label>
                            <input class="form-control {{ $errors->has('fecha_recepcion') ? ' is-invalid' : '' }}" type="date" name="fecha_recepcion"  required>                       
                        </div>                       
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <label for="">Turnado a</label>
                                <select class="form-control {{ $errors->has('responsable') ? ' is-invalid' : '' }}" name="responsable" required>
                                    <option value="">Seleccione un usuario</option>
                                        @foreach (Auth::user()->profile->position->direction->positions as $position)
                                            @foreach ($position->profile as $profile)
                                                @if($profile->user)
                                                    <option value="{{$position->id}}">{{$position->nombre}}</option>
                                                @endif
                                            @endforeach
                                        @endforeach
                                </select>  
                        </div>                           
                    </div>
                    <div class="row mb-2">
                        <div class="col-xl-12">
                                <label for="">Asunto</label>
                                <textarea class="form-control {{ $errors->has('asunto') ? ' is-invalid' : '' }}" name="asunto" cols="30" rows="5"></textarea> 
                        </div>
                    </div>
                    <div class="alert alert-danger file-size" role="alert">
                    
                    </div>                                    
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="oficio">Adjuntar Oficio</label>
                            <input required type='file' id='oficios' name='oficio'/>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-xl-5">
                            <input data-target="#answerDate" data-toggle="collapse" id="lbresp" aria-expanded="false" class=" {{ $errors->has('name') ? ' is-invalid' : '' }}" value="1" type="checkbox" name="respuesta" >
                            <label for="lbresp">Requiere respuesta?</label>
                        </div>                                      
                    </div>
                    <div class="row mb-2">
                        <div class="col-xl-12 collapse" id='answerDate'>
                            <label for="">Atender antes de:</label>                            
                            <input class="form-control {{ $errors->has('fecha_emision') ? ' is-invalid' : '' }}" type="date" name="fecha_respuesta" >                       
                        </div>     
                    </div>
                    <div class="row">
                        <div class="col-xl-5">
                            <input data-target="#copia" data-toggle="collapse" aria-expanded="false" class=" {{ $errors->has('name') ? ' is-invalid' : '' }}" value="1" type="checkbox" name="copia" id="collapseCopy">
                            <label for="collapseCopy">Turnar copia de conocimiento a:</label>
                        </div>                       
                    </div>                  
                    <div id="copia" class=" row collapse">
                        <div class="col-xl-10 ">
                            <select disabled class="form-control {{ $errors->has('responsable') ? ' is-invalid' : '' }}" name="userAttach[]" id="copy">
                                <option value="">Seleccione un usuario</option>
                                    @foreach (Auth::user()->profile->position->direction->positions as $position)
                                        @foreach ($position->profile as $profile)
                                            @if($profile->user)
                                                <option value="{{$position->id}}">{{$position->nombre}}</option>
                                            @endif
                                        @endforeach
                                    @endforeach
                            </select>                                
                        </div>
                        <div class="col-xl-2">
                            <button type='button' class="btn btn-primary btn-add-attach"><i class="fas fa-plus-circle"></i></button>
                        </div>
                    </div> 
                    <div id="add-attach"></div>
                    <input type="hidden" value="0" name="tipo">                                    
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit"><i class="fas fa-check mr-2"></i> Guardar</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                </div>
            </div>
        </form>

    </div>
</div>
<script>
 $(function(){
        $('#oficios').simpleFilePreview();        
        $(document).on("click", ".btn-add-attach", function(){
            $("#add-attach").append('<div class="row mt-2">\
                    <div class="col-xl-10 ">\
                        <select class="form-control {{ $errors->has('responsable') ? ' is-invalid' : '' }}" name="userAttach[]" >\
                            <option value="">Seleccione un usuario</option>\
                                @foreach (Auth::user()->profile->position->direction->positions as $profile)\
                                    <option value="{{$profile->id}}">{{$profile->nombre}}</option>\
                                @endforeach\
                        </select>\
                    </div>\
                    <div class="col-xl-2">\
                        <button type="button" class="btn btn-danger btn-delete-subaction"><i class="fa fa-trash-alt"></i></button>\
                    </div>\
            </div>'
            );
        });

        $(document).on("click", ".btn-delete-subaction", function(){
            var _this = $(this),
                val_sub = 0,
                index = 0;
            _this.parents(".row").fadeOut("slow", function(){
                $(this).remove();
            });

        });

        $('#copia').on('shown.bs.collapse', function () {
            $('#copy').prop('disabled',false);
        });
        $('#copia').on('hide.bs.collapse', function () {
            $('#copy').prop('disabled',true);
            $('#add-attach').html('');
        });

        $(document).on("change", "#pmdu_field", function(){
            if($(this).is(":checked")){
                $('input[name="no_oficio"]').attr("disabled", true);
            }else{
                $('input[name="no_oficio"]').attr("disabled", false);
            }
        });
    });
</script>




<div class="modal fade" id="modal-details-offices" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <form method="POST" action="{{ route('store.office.data') }}" class="storeFile" aria-label="{{ __('Register') }}" enctype="multipart/form-data">
        <div class="modal-dialog modal-lg" role="document">
            @csrf            
            <div class="modal-content">
                <div class="modal-header">                   
                    <h5 class="modal-title" id="exampleModalLabel">Detalle oficio de solicitud</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="alert alert-primary messageNotification" role="alert">
                    
                </div>
                <div class="modal-body">
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="">No. Oficio</label>
                            <input class="form-control" type="text" name="no_oficio" id="no_oficio" required>                       
                        </div>
                        <div class="col-xl-6">
                            <label id="lbemisor" for="">Emisor</label>
                            <input class="form-control" type="text" name="emisor" id="emisor" required> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <label for="">Organismo o Dependencia</label>
                            <input class="form-control" type="text" name="dependencia" id="dependencia" required> 
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="">Fecha Emision</label>
                            <input class="form-control d" type="date" name="fecha_emision" id="fecha_emision" required>                       
                        </div>
                        <div class="col-xl-6 fecha_entrega">
                            <label id="lbdate" for="">Fecha Recepcion</label>
                            <input class="form-control lid" type="date" name="fecha_recepcion" id="fecha_recepcion" required>                       
                        </div>                       
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <label for="">Titular o encargado</label>
                            <input type="text" class="form-control" name="responsable" id="responsable">     
                        </div>                           
                    </div>
                    <div class="row mb-2">
                        <div class="col-xl-12">
                            <label for="">Asunto</label>
                            <textarea class="form-control" name="asunto" id="asunto" cols="30" rows="5"></textarea> 
                        </div>
                    </div>
                    <input type="hidden" class="type"  name="tipo">      
                    <div id="WithCopy">
                    </div>
                    <div class="alert alert-danger file-size" role="alert">
                    
                    </div>    
                    <div class="row mb-2">
                        <div class="col-xl-4 mt-4  adjuntar">
                            <a href="#" class="btn btn-primary oficio">Descargar Oficio</a>                        
                        </div>
                        <div class="col=xl-4 fechaRespuesta">

                        </div>
                        <div class="col-xl-4  mt-4 notification">
                                                   
                        </div>                   
                    </div>
                </form>                                       
                    <form method="POST" action="{{ route('store.answer.data') }}" aria-label="{{ __('Register') }}" id="storeAnswer" enctype="multipart/form-data">
                        @csrf                  
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Oficio de atencion</h5>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xl-6">
                                <label for="">No. Oficio</label>
                                <input class="form-control {{ $errors->has('no_oficio_r') ? ' is-invalid' : '' }}" type="text" name="no_oficio_r" id="no_oficio_r" required>                       
                            </div>
                            <div class="col-xl-6">
                                <label for="">Emisor</label>
                                <input class="form-control {{ $errors->has('emisor_r') ? ' is-invalid' : '' }}" type="text" name="emisor_r" id="emisor_r" required> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12">
                                <label for="">Organismo o Dependencia</label>
                                <input class="form-control {{ $errors->has('dependencia_r') ? ' is-invalid' : '' }}" type="text" name="dependencia_r" id="dependencia_r" required> 
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xl-6">
                                <label for="">Fecha Emision</label>
                                <input class="form-control {{ $errors->has('fecha_emision_r') ? ' is-invalid' : '' }}" type="date" name="fecha_emision_r" id="fecha_emision_r" required>                       
                            </div>
                            <div class="col-xl-6">
                                <label for="">Fecha Recepcion</label>
                                <input class="form-control {{ $errors->has('fecha_recepcion_r') ? ' is-invalid' : '' }}" type="date" name="fecha_recepcion_r" id="fecha_recepcion_r" required>                       
                            </div>                       
                        </div>
                        <div class="row mb-2">
                            <div class="col-xl-12">
                                    <label for="">Asunto</label>
                                    <textarea class="form-control {{ $errors->has('asunto_r') ? ' is-invalid' : '' }}" name="asunto_r" id="asunto_r" cols="30" rows="5"></textarea> 
                            </div>
                        </div>                        
                        {{-- <div class="row mb-2"> --}}
                            {{-- <div class="col-xl-6">
                                <label for="">No. Oficio</label>
                                <input class="form-control  }}" type="text" name="no_oficio"  required>                       
                            </div> --}}
                            {{-- <div class="col-xl-12">
                                <label for="">Organismo o dependencia</label>
                                <input class="form-control " type="text" name="dependencia"  required> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12">
                                <label for="">Dirigido a</label>
                                <input class="form-control " type="text" name="emisor"  required> 
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xl-6">
                                <label for="">Fecha Emision</label>
                                <input class="form-control " type="date" name="fecha_emision"  required>                       
                            </div>
                            <div class="col-xl-6">
                                <label for="">Fecha Entrega</label>
                                <input class="form-control" type="date" name="fecha_recepcion" disabled  required>                       
                            </div>                       
                        </div>
                        <div class="row mb-2">
                            <div class="col-xl-12">
                                    <label for="">Asunto</label>
                                    <textarea class="form-control " name="asunto"  cols="30" rows="5"></textarea> 
                            </div>
                        </div>          --}}
                        <div class="form-row respuesta">
                            
                        </div>                                        
                    </form>                              
                </div>
                <div class="modal-footer footer-answer">
                    <button class="btn btn-primary saveFile" type="submit"><i class="fas fa-check mr-2"></i> Guardar</button>
                    <button class="btn btn-primary answerFile" type="button"><i class="fas fa-check mr-2"></i> Guardar</button>
                    <button class="btn btn-primary fileReaded" type="button"><i class="fas fa-check mr-2"></i> Marcar como leido</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                </div>
            </div>    
        </div>
    </div>
    <script>
     $(function(){
            $('#oficios').simpleFilePreview();
     });
    </script>
    






    {{-- Modal Logout --}}
<div class="modal fade" id="modal-new-offices-extern" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form method="POST" action="{{ route('store.office.data') }}" aria-label="{{ __('Register') }}" enctype="multipart/form-data">
                @csrf            
                <div class="modal-content" id="detalles">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Nuevo Oficio</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row mb-2">
                            <div class="col-xl-5">
                                <input data-target="#officeAtencion" id='lbresp2' data-toggle="collapse" aria-expanded="false" class="oficioAtencion" value="2" type="checkbox" name="tipo" >
                                <label for="lbresp2">Oficio de respuesta?</label>
                            </div> 
                        </div>
                        <div id="officeAtencion" class=" row collapse">
                            <div class="col-xl-10 ">
                                <select disabled class="form-control " required name="id_padre" id="padres">
                                    <option value="">Seleccione un oficio</option>
                                        @foreach ($oficios as $oficio)
                                            @if($oficio->estatus == 2 && $oficio->respuesta == true)
                                                @foreach ($oficio->responsable()->wherePivot('type', 0)->get() as $responsable)
                                                    @if ($responsable->id == Auth::user()->profile->position->id)
                                                        <option value="{{$oficio->id}}">{{$oficio->no_oficio}}({{$oficio->dependencia}})</option>
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                </select>                                
                            </div>
                        </div>
                        <div class="row mb-2">
                            {{-- <div class="col-xl-6">
                                <label for="">No. Oficio</label>
                                <input class="form-control  }}" type="text" name="no_oficio"  required>                       
                            </div> --}}
                            <div class="col-xl-12">
                                <label for="">Organismo o Dependencia Receptora</label>
                                <input class="form-control " type="text" name="dependencia"  required> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12">
                                <label for="">Receptor del Oficio</label>
                                <input class="form-control " type="text" name="emisor"  required> 
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col-xl-6">
                                <label for="">Fecha Emision</label>
                                <input class="form-control " type="date" name="fecha_emision"  required>                       
                            </div>
                            <div class="col-xl-6">
                                <label for="">Fecha Entrega</label>
                                <input class="form-control" type="date" name="fecha_recepcion" disabled  required>                       
                            </div>                       
                        </div>
                        <div class="row mb-2">
                            <div class="col-xl-12">
                                    <label for="">Asunto</label>
                                    <textarea class="form-control " name="asunto"  cols="30" rows="5"></textarea> 
                            </div>
                        </div>                                    
                        <div class="form-row file">
                            {{-- <div class="form-group col">
                                <label for="oficio">Adjuntar Oficio</label>
                                <input type='file' class='oficios' name='oficio'/>
                            </div> --}}
                        </div>                     
                        <div class="row copiaConocimiento">
                            <div class="col-xl-5">
                                <input data-target="#copia2" id='lbcopia2' data-toggle="collapse" aria-expanded="false" class="" value="1" type="checkbox" name="copia" >
                                <label for="lbcopia2">Turnar copa de conocimiento a:</label>
                            </div>                       
                        </div>                  
                        <div id="copia2" class=" row collapse">
                            <div class="col-xl-10 ">
                                <select disabled class="form-control " name="userAttach[]" id="attach2">
                                    <option value="">Seleccione un usuario</option>
                                        @foreach (Auth::user()->profile->position->direction->positions as $position)
                                            @foreach ($position->profile as $profile)
                                                @if($profile->user)
                                                    <option value="{{$position->id}}">{{$position->nombre}}</option>
                                                @endif
                                            @endforeach
                                        @endforeach
                                </select>                                
                            </div>
                            <div class="col-xl-2">
                                <button type='button' class="btn btn-primary btn-add-attach2"><i class="fas fa-plus-circle"></i></button>
                            </div>
                        </div> 
                        <div id="add-attach2"></div>
                        <input type="hidden" value="1" id="type" name="tipo">                                    
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="submit"><i class="fas fa-check mr-2"></i> Guardar</button>
                        <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    </div>
                </div>
            </form>
    
        </div>
    </div>
    <script>
     $(function(){
            $('.oficios').simpleFilePreview();        
        });
        $(document).on("click", ".btn-add-attach2", function(){
            $("#add-attach2").append('<div class="row mt-2">\
                    <div class="col-xl-10 ">\
                        <select class="form-control {{ $errors->has('responsable') ? ' is-invalid' : '' }}" name="userAttach[]" >\
                            <option value="">Seleccione un usuario</option>\
                                @foreach (Auth::user()->profile->position->direction->positions as $profile)\
                                    <option value="{{$profile->id}}">{{$profile->nombre}}</option>\
                                @endforeach\
                        </select>\
                    </div>\
                    <div class="col-xl-2">\
                        <button type="button" class="btn btn-danger btn-delete-subaction"><i class="fa fa-trash-alt"></i></button>\
                    </div>\
            </div>'
            );
            
        });
    
        $(document).on("click", ".btn-delete-subaction", function(){
            var _this = $(this),
                val_sub = 0,
                index = 0;
            _this.parents(".row").fadeOut("slow", function(){
                $(this).remove();
            });
    
        });

        $('#copia2').on('shown.bs.collapse', function () {
            $('#attach2').prop('disabled',false);
        });
        $('#copia2').on('hide.bs.collapse', function () {
            $('#attach2').prop('disabled',true);
            $('#add-attach2').html('');
        });

        $('#officeAtencion').on('shown.bs.collapse', function () {
            $('#padres').prop('disabled',false);
            $('#type').prop('disabled',true);
            $('.copiaConocimiento').hide();
            
        });
        $('#officeAtencion').on('hide.bs.collapse', function () {
            $('#padres').prop('disabled',true);
            $('#type').prop('disabled',false);
            $('.copiaConocimiento').show();
        });
    </script>
    
    