{{-- Modal Logout --}}
<div class="modal fade" id="modal-update-user"  role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        @can('write_users')
        <form method="POST" action="{{ route('update.profile') }}" aria-label="{{ __('Register') }}">
            @csrf
            <input type="hidden" name="operation" value="update">
            <input type="hidden" name="id" id="id">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Usuario</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-5 col-form-label text-md-right">Nombre</label>

                        <div class="col-md-6">
                            <input type="text" class="name form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="ape_pat" class="col-md-5 col-form-label text-md-right">Apellido Paterno</label>

                        <div class="col-md-6">
                            <input  type="text" class="ape_pat form-control{{ $errors->has('ape_pat') ? ' is-invalid' : '' }}" name="ape_pat" value="{{ old('ape_pat') }}" required >

                            @if ($errors->has('ape_pat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ape_pat') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ape_mat" class="col-md-5 col-form-label text-md-right">Apellido Materno</label>

                        <div class="col-md-6">
                            <input  type="text" class="ape_mat form-control{{ $errors->has('ape_mat') ? ' is-invalid' : '' }}" name="ape_mat" value="{{ old('ape_mat') }}" required >

                            @if ($errors->has('ape_mat'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('ape_mat') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="email" class="col-md-5 col-form-label text-md-right">Correo Electronico</label>

                        <div class="col-md-6">
                            <input  type="email" readonly class="email form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-5 col-form-label text-md-right">Contraseña</label>

                        <div class="col-md-6">
                            <input  type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" >

                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-5 col-form-label text-md-right">Confirmar Contraseña</label>

                        <div class="col-md-6">
                            <input  type="password" class="form-control" name="password_confirmation" >
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="position" class="col-md-5 col-form-label text-md-right">Seleccione un Puesto</label>
                        <div class="col-md-6 ">
                            <select class="form-control select2 position" name="position" required >
                                <option value="" selected>Seleccione un rol</option>
                                @foreach ($positions as $position)
                                    <option value="{{ $position->id }}">{{ $position->nombre }} ( {{ $position->direction->abreviacion }} )</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="role" class="col-md-5 col-form-label text-md-right">Seleccione un Rol</label>
                        <div class="col-md-6 ">
                            <select class="form-control select2 role" name="role" required>
                                <option value="" selected disabled>Seleccione un rol</option>
                                @foreach ($roles as $role)
                                    <option value="{{ $role->name }}">{{ $role->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-user-plus mr-2"></i> Registrar</button>
                </div>
            </div>
        </form>
        @else
        <form method="POST" action="{{ route('update.profile') }}" aria-label="{{ __('Register') }}">
                @csrf
                <input type="hidden" name="operation" value="update">
                <input type="hidden" name="id" id="id">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Actualizar Usuario</h5>
                        <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="name" class="col-md-5 col-form-label text-md-right">Nombre</label>
    
                            <div class="col-md-6">
                                <input disabled type="text" class="name form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
    
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <label for="ape_pat" class="col-md-5 col-form-label text-md-right">Apellido Paterno</label>
    
                            <div class="col-md-6">
                                <input  disabled type="text" class="ape_pat form-control{{ $errors->has('ape_pat') ? ' is-invalid' : '' }}" name="ape_pat" value="{{ old('ape_pat') }}" required >
    
                                @if ($errors->has('ape_pat'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ape_pat') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ape_mat" class="col-md-5 col-form-label text-md-right">Apellido Materno</label>
    
                            <div class="col-md-6">
                                <input  disabled type="text" class="ape_mat form-control{{ $errors->has('ape_mat') ? ' is-invalid' : '' }}" name="ape_mat" value="{{ old('ape_mat') }}" required >
    
                                @if ($errors->has('ape_mat'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ape_mat') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <label for="email" class="col-md-5 col-form-label text-md-right">Correo Electronico</label>
    
                            <div class="col-md-6">
                                <input  disabled type="email" readonly class="email form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
    
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <label for="position" class="col-md-5 col-form-label text-md-right">Seleccione un Puesto</label>
                            <div class="col-md-6 ">
                                <select disabled class="form-control select2 position" name="position" required >
                                    <option value="" selected>Seleccione un rol</option>
                                    @foreach ($positions as $position)
                                        <option value="{{ $position->id }}">{{ $position->nombre }} ( {{ $position->direction->abreviacion }} )</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
    
                        <div class="form-group row">
                            <label for="role" class="col-md-5 col-form-label text-md-right">Seleccione un Rol</label>
                            <div class="col-md-6 ">
                                <select disabled class="form-control select2 role" name="role" required>
                                    <option value="" selected disabled>Seleccione un rol</option>
                                    @foreach ($roles as $role)
                                        <option value="{{ $role->name }}">{{ $role->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    </div>
                </div>
            </form>
            @endcan

    </div>
</div>
<script>
    $(function(){
        var operation = "{{ old('operation') }}";
        if(operation == "update")
        {
            $("#modal-update-user").modal("show");
        }
    });
</script>