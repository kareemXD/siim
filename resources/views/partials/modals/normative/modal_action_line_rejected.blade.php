{{-- Modal Action Line --}}
<div class="modal fade" id="modal-action-line-rejected" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Linea de Acción Rechazada</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Normativa</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="reject-normative">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Eje</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="reject-axe">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Objetivo</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="reject-objective">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Estrategia</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="reject-strategy">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Linea de Acción</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="reject-action-line">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Motivo del Rechazo</label>
                    <div class="col-md-10">
                        <textarea class="form-control" id="reject-description" rows="3" disabled></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal" >Cerrar</button>
            </div>
        </div>
    </div>
</div>