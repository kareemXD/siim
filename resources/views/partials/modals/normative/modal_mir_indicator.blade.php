{{-- Modal Action Line --}}
<div class="modal fade" id="modal-mir-indicator" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Asignar Meta</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" id="direction" name="direction" value="{{ Auth::user()->profile->position->direction->id }}">
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Normativa</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="mir-normative">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Programa</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="mir-program">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Nivel</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="mir-level">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Formula</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="mir-operation">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Indicador</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="mir-indicator">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-reject-mir-indicator id-mir-indicator" type="button" >Rechazar</button>
                <button class="btn btn-success btn-accept-mir-indicator id-mir-indicator" type="button" >Aceptar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $(document).on("click", ".btn-accept-mir-indicator", function(){
            $('.wrapper-spinner-initial').fadeIn('fast');
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('accept.mir.indicator') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                    direction: $("#direction").val(),
                },
            }).done(function(result){
                $('[data-mir="'+id+'"]').removeClass("bg-warning");
                $('[data-mir="'+id+'"]').addClass("bg-success");
                $('[data-mir="'+id+'"]').html("Acceptada");
                $('.wrapper-spinner-initial').fadeOut('fast');
                $("#modal-mir-indicator").modal("hide");
            });
        });

        $(document).on("click", ".btn-reject-mir-indicator", function(){
            $("#modal-mir-indicator").modal("hide");
            var id = $(this).data("id");
            modal = $("#modal-reject-mir-indicator");
            modal.find('[name="id_mir_indicator"]').val(id);
            modal.modal("show");
        });
        
    });
</script>