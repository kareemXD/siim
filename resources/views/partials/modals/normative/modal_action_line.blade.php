{{-- Modal Action Line --}}
<div class="modal fade" id="modal-action-line" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Asignar Linea de Acción</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Normativa</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="normative">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Eje</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="axe">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Objetivo</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="objective">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Estrategia</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="strategy">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Linea de Acción</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="action-line">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-reject-action-line id-action-line" type="button" >Rechazar</button>
                <button class="btn btn-success btn-accept-action-line id-action-line" type="button" >Aceptar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $(document).on("click", ".btn-accept-action-line", function(){
            $('.wrapper-spinner-initial').fadeIn('fast');
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('accept.pm.action.line') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                $('[data-pm="'+id+'"]').removeClass("bg-warning");
                $('[data-pm="'+id+'"]').addClass("bg-success");
                $('[data-pm="'+id+'"]').html("Acceptada");
                $('.wrapper-spinner-initial').fadeOut('fast');
                $("#modal-action-line").modal("hide");

            });
        });
        $(document).on("click", ".btn-reject-action-line", function(){
            $("#modal-action-line").modal("hide");
            var id = $(this).data("id");
            modal = $("#modal-reject-action-line");
            modal.find('[name="id_action_line"]').val(id);
            modal.modal("show");
        });
        
    });
</script>