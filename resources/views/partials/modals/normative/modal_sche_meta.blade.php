{{-- Modal Action Line --}}
<div class="modal fade" id="modal-sche-meta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Asignar Meta</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Normativa</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="sche-normative">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Objetivo</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="sche-objective">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Meta</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="sche-meta">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-reject-sche-indicator id-sche-indicator" type="button" >Rechazar</button>
                <button class="btn btn-success btn-accept-sche-indicator id-sche-indicator" type="button" >Aceptar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $(document).on("click", ".btn-accept-sche-indicator", function(){
            $('.wrapper-spinner-initial').fadeIn('fast');
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('accept.sche.meta') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                $('[data-age="'+id+'"]').removeClass("bg-warning");
                $('[data-age="'+id+'"]').addClass("bg-success");
                $('[data-age="'+id+'"]').html("Acceptada");
                $('.wrapper-spinner-initial').fadeOut('fast');
                $("#modal-sche-meta").modal("hide");
            });
        });

        $(document).on("click", ".btn-reject-sche-indicator", function(){
            $("#modal-sche-meta").modal("hide");
            var id = $(this).data("id");
            modal = $("#modal-reject-sche-meta");
            modal.find('[name="id_sche_meta"]').val(id);
            modal.modal("show");
        });
        
    });
</script>