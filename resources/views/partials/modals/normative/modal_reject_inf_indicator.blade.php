{{-- Modal Action Line --}}
<div class="modal fade" id="modal-reject-inf-indicatior" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Rechazar Indicador</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="reject-form-inf" action="{{ route('reject.inf.indicator') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="id_inf_indicator">
                    <div class="form-group">
                        <label for="description">Motivo del Rechazo</label>
                        <textarea class="form-control" name="description" rows="3" required></textarea>
                    </div>
                    <div class="form-group">
                        <div class="input-group input-file" name="file">
                            <span class="input-group-btn">
                                <button class="btn btn-primary btn-choose btn-border-initial" type="button">Seleccione Oficio</button>
                            </span>
                            <input type="text" class="form-control" placeholder='Seleccione Oficio...' required/>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger " type="button" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-success " type="button" onclick="event.preventDefault(); document.getElementById('reject-form-inf').submit();">Continuar</button>
            </div>
        </div>
    </div>
</div>

<script>
function bs_input_file() {
	$(".input-file").before(
		function() {
			if ( ! $(this).prev().hasClass('input-ghost') ) {
				var element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
				element.attr("name",$(this).attr("name"));
				element.change(function(){
					element.next(element).find('input').val((element.val()).split('\\').pop());
				});
				$(this).find("button.btn-choose").click(function(){
					element.click();
				});
				$(this).find('input').css("cursor","pointer");
				$(this).find('input').mousedown(function() {
					$(this).parents('.input-file').prev().click();
					return false;
				});
				return element;
			}
		}
	);
}
$(function() {
	bs_input_file();
});
</script>