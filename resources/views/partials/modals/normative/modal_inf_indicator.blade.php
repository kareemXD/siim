{{-- Modal Action Line --}}
<div class="modal fade" id="modal-inf-indicatior" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Asignar Indicador</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Normativa</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="inf-normative">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Agenda</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="inf-schedule">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Eje</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="inf-axe">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Tema</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="inf-topic">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Indicador</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="inf-indicator">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger btn-reject-inf-indicator id-inf-indicator" type="button" >Rechazar</button>
                <button class="btn btn-success btn-accept-inf-indicator id-inf-indicator" type="button" >Aceptar</button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(){
        $(document).on("click", ".btn-accept-inf-indicator", function(){
            $('.wrapper-spinner-initial').fadeIn('fast');
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('accept.inf.indicator') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                $('[data-inf="'+id+'"]').removeClass("bg-warning");
                $('[data-inf="'+id+'"]').addClass("bg-success");
                $('[data-inf="'+id+'"]').html("Acceptada");
                $('.wrapper-spinner-initial').fadeOut('fast');
                $("#modal-inf-indicatior").modal("hide");

            });
        });

        $(document).on("click", ".btn-reject-inf-indicator", function(){
            $("#modal-inf-indicatior").modal("hide");
            var id = $(this).data("id");
            modal = $("#modal-reject-inf-indicatior");
            modal.find('[name="id_inf_indicator"]').val(id);
            modal.modal("show");
        });
        
    });
</script>