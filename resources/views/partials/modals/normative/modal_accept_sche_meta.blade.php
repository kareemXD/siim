{{-- Modal Action Line --}}
<div class="modal fade" id="modal-accept-sche-meta" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Asignar Meta</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="accept-form-sche" action="{{ route('accept.sche.meta') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id_sche_meta">
                    <div class="form-group row">
                        <label for="position" class="col-md-5 col-form-label text-md-right">Seleccione el Puesto Responsable</label>
                        <div class="col-md-6 ">
                            <select class="form-control select2" name="position" required >
                                <option value="" selected>Seleccione el Puesto Responsable</option>
                                @foreach (Auth::user()->profile->position->direction->positions as $position)
                                <option value="{{ $position->id }}">{{ $position->nombre }} ( {{ $position->direction->abreviacion }} )</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cerrar</button>
                <button class="btn btn-success" type="button" onclick="event.preventDefault(); document.getElementById('accept-form-sche').submit();">Asignar</button>
            </div>
        </div>
    </div>
</div>