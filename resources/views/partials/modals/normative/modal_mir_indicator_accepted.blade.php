{{-- Modal Indicator INAFED --}}
<div class="modal fade" id="modal-mir-indicator-accepted" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Indicador Asignado</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Normativa</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="accept-mir-normative">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Programa</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="accept-mir-program">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Nivel</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="accept-mir-level">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Formula</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="accept-mir-operation">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-md-2 col-form-label text-md-right">Indicador</label>
                    <div class="col-md-10">
                        <input  type="text" disabled class="form-control" id="accept-mir-indicator">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal" >Cerrar</button>
            </div>
        </div>
    </div>
</div>