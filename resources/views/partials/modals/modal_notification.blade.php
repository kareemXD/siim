{{-- Modal Logout --}}
<div class="modal fade" id="modal-notification" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Notificación</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-row">
                    <div class="col-md-12 col-sm-12">
                        <label for="date">Fecha</label>
                        <input type="text" class="form-control" id="date" disabled>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-12">
                        <label for="notification">Notificación</label>
                        <textarea class="form-control" id="notification" rows="3" disabled></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>