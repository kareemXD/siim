{{-- Modal Logout --}}
<div class="modal fade" id="modal-update-action-goal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Segimiento del POA</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form method="POST" action="{{ route('store.action.goals') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" id="action_id" name="action_id">
                <input type="hidden" id="trimester" name="trimester" value="{{ $trimester["number"] }}">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="action">Actividad</label>
                            <input disabled type="text" class="form-control {{ $errors->has('action') ? ' is-invalid' : '' }}" id="action" name="action">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="udem">Unidad de Medida</label>
                            <input disabled type="text" class="form-control {{ $errors->has('udem') ? ' is-invalid' : '' }}" id="udem">
                        </div>
                    </div>
                    <div class="form-row">
                        <table class="table table-sm">
                            <thead class="no-hover">
                                <tr>
                                    <th style="text-align:center;" colspan="2">Meta Anual</th>
                                    @if ($trimester["number"] == 1)
                                        <th style="text-align:center;" colspan="5">1° Trimeste</th>
                                    @elseif ($trimester["number"] == 2)
                                        <th style="text-align:center;" colspan="5">2° Trimeste</th>
                                    @elseif ($trimester["number"] == 3)
                                        <th style="text-align:center;" colspan="5">3° Trimeste</th>
                                    @elseif ($trimester["number"] == 4)
                                        <th style="text-align:center;" colspan="5">4° Trimeste</th>
                                    @endif
                                </tr>
                                <tr>
                                    <th style="text-align:center;">P</th>
                                    <th style="text-align:center;">R</th>
                                    @foreach ($trimester["mounths"] as $mounth)
                                        <th style="text-align:center;">{{ $mounth }}</th>
                                    @endforeach
                                    <th style="text-align:center;">P</th>
                                    <th style="text-align:center;">R</th>
                                </tr>
                            </thead>
                            <tbody id="actions-poa">
                                <tr>
                                    <td class="wd-14"><input id="anual-p" disabled type="number" step=".01" min="0" class="form-control"></td>
                                    <td class="wd-14">
                                        <input id="old-value" disabled type="hidden" >
                                        <input id="anual-r" disabled type="number" step=".01" min="0" class="form-control" >
                                    </td>
                                    @foreach ($trimester["mounths"] as $mounth)
                                        <td class="wd-14"><input required id="{{ $mounth }}" type="number" step=".01" min="0" class="form-control mounth" name="{{ $mounth }}"></td>
                                    @endforeach
                                    <td class="wd-14"><input id="trimestre-p" disabled type="number" step=".01" min="0" class="form-control"></td>
                                    <td class="wd-14"><input id="trimestre-r" disabled type="number" step=".01" min="0" class="form-control"></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div id="subactions">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cerrar</button>
                    <button class="btn btn-primary"type="submit" ><i class="fas fa-save"></i> Guardar</button>
                </div>
            <form>
        </div>
    </div>
</div>
<script>
    $(function(){
        $(document).on("focusout", ".mounth", function(){
            var annual = Number($("#anual-r").val());
            var old_value = Number($("#old-value").val());
            var value = Number($(this).val());
            var tri = Number($("#trimestre-r").val());
            
            var tri_total = (tri - old_value) + value;
            var anual_total = (annual - old_value) + value;
            $("#trimestre-r").val(tri_total);
            $("#anual-r").val(anual_total);
        });
        $(document).on("focus", ".mounth", function(){
            $("#old-value").val($(this).val());
        });
        
        $(document).on("focus", ".sub_mounth", function(){
            $(this).parents("tr").find(".sub-old-value").val($(this).val());
        });
        
        $(document).on("focusout", ".sub_mounth", function(){
            var annual = Number($(this).parents("tr").find(".sub-anual-r").val());
            var old_value = Number($(this).parents("tr").find(".sub-old-value").val());
            var value = Number($(this).val());
            var tri = Number($(this).parents("tr").find(".sub-trimestre-r").val());
            var tri_total = (tri - old_value) + value;
            var anual_total = (annual - old_value) + value;
            $(this).parents("tr").find(".sub-trimestre-r").val(tri_total);
            $(this).parents("tr").find(".sub-anual-r").val(anual_total);

            var parent = $(this).data("parent");
            var total = 0;
            $('[data-parent="'+parent+'"]').each(function(i){
                total += Number($(this).val());
            });
            old_value = $("#"+parent).val();
            tri = Number($("#trimestre-r").val());
            annual = Number($("#anual-r").val());

            tri_total = (tri - old_value) + total;
            anual_total = (annual - old_value) + total;
            $("#"+parent).val(total);
            $("#trimestre-r").val(tri_total);
            $("#anual-r").val(anual_total);
        });
    });
</script>