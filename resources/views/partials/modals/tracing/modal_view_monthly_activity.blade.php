{{-- Modal Logout --}}
<div class="modal fade" id="modal-view-monthly-activity" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Visualizar Actividad</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-row">
                    <div class="form-group col">
                        <label for="mounth">Mes</label>
                        <select disabled class="form-control select2" id="view-mounth" >
                            <option value="">Seleccione el Mes</option>
                            @foreach ($meses as $key => $mes)
                                <option value="{{ $mes['num'] }}">{{ $mes['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col">
                        <label for="location">Localidad</label>
                        <select disabled class="form-control select2" id="view-location" multiple="multiple">
                            <option value="">Seleccione la Localidad</option>
                            @foreach ($locations as $location)
                                <option value="{{ $location->id }}">{{ $location->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="name2">Nombre o Tema de la Actividad Relevante</label>
                        <input disabled type="text" class="form-control" id="view-name2">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="beneficiaries">Beneficiarios (Hombres)</label>
                        <input disabled type="number" class="form-control" id="view-beneficiaries-men" >
                    </div>
                    <div class="form-group col">
                        <label for="beneficiaries">Beneficiarios (Mujeres)</label>
                        <input disabled type="number" class="form-control" id="view-beneficiaries-women" >
                    </div>
                    <div class="form-group col">
                        <label for="amount">Presupuesto</label>
                        <input disabled  type="number" step=".01" class="form-control" id="view-amount" >
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="description">Descripción</label>
                        <textarea disabled class="form-control overflow-true"  id="view-description" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col">
                        <div class="div-evidences">
                            <ul class="flex">
                                
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('#view-evidences').simpleFilePreview();
    });
</script>