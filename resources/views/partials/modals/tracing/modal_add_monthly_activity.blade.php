{{-- Modal Logout --}}
<div class="modal fade" id="modal-add-monthly-activity" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar Actividad</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form method="POST" action="{{ route('store.monthly.report') }}" enctype="multipart/form-data">
                    @csrf
                <input type="hidden" name="direction" value="{{ $direction->id }}">
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="mounth">Mes</label>
                            <select class="form-control {{ $errors->has('mounth') ? ' is-invalid' : '' }} select2" name="mounth" required>
                                @php
                                    // $now = Carbon\Carbon::now("America/Mexico_City");
                                    // $now = Carbon\Carbon::create(2019, 06, 5, 0, 0, 0, 'America/Mexico_City');
                                @endphp
                                
                                <option select value="">Seleccione Mes</option>
                                @foreach ($meses as $key => $mes)
                                    @php
                                        // $first = Carbon\Carbon::create($now->format("Y"), explode("-", $mes["num"])[0], 01, 0, 0, 0, 'America/Mexico_City')->addDays(4);
                                        // $last = Carbon\Carbon::create($now->format("Y"), explode("-", $mes["num"])[0] + 1, 01, 0, 0, 0, 'America/Mexico_City')->addDays(4);
                                    @endphp
                                    {{-- @if ($now > $first && $now <= $last)
                                        <option select value="{{ $mes['num'] }}">{{ $mes['name'] }}</option>
                                    @else
                                        <option disabled>{{ $mes['name'] }}</option>
                                    @endif --}}
                                    <option value="{{ $mes['num'] }}">{{ $mes['name'] }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('mounth'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('mounth') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="location">Localidad(es)</label>
                            <select class="form-control {{ $errors->has('location') ? ' is-invalid' : '' }} select2" name="location[]" required multiple="multiple">
                                @foreach ($locations as $location)
                                    <option value="{{ $location->id }}">{{ $location->nombre }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('location'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('location') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="name2">Nombre o Tema de la Actividad Relevante</label>
                            <input required type="text" class="form-control {{ $errors->has('name2') ? ' is-invalid' : '' }}" id="name2" name="name">
                            @if ($errors->has('name2'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name2') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="beneficiaries-men">Beneficiarios (Hombres)</label>
                            <input required type="number" min="0" class="form-control {{ $errors->has('beneficiaries-men') ? ' is-invalid' : '' }}" id="beneficiaries-men" name="beneficiaries_men">
                            @if ($errors->has('beneficiaries-men'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('beneficiaries-men') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="beneficiaries-women">Beneficiarios (Mujeres)</label>
                            <input required type="number"min="0" class="form-control {{ $errors->has('beneficiaries-women') ? ' is-invalid' : '' }}" id="beneficiaries-women" name="beneficiaries_women">
                            @if ($errors->has('beneficiaries-women'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('beneficiaries-women') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="amount">Presupuesto</label>
                            <input required type="number" step=".01" min="0" class="form-control {{ $errors->has('amount') ? ' is-invalid' : '' }}" id="amount" name="amount">
                            @if ($errors->has('amount'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('amount') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="description">Descripción</label>
                            <textarea required class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" id="description" rows="3"></textarea>
                            @if ($errors->has('description'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="evidences">Archivos Adjuntos</label>
                            <input type='file' id='evidences' name='evidences[]' multiple='multiple' />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle"></i> Cerrar</button>
                    <button class="btn btn-primary"type="submit" ><i class="fas fa-save"></i> Guardar</button>
                </div>
            <form>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('#evidences').simpleFilePreview();
    });
</script>
@if($errors->any())
    <script>
        $(function(){
            $("#modal-add-monthly-activity").modal("show");
        });
    </script>
@endif