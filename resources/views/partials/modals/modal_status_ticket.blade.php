
{{-- Modal View and edit ticket detaills --}}
<div class="modal fade" id="modal-edit-ticket" tabindex="-1" role="dialog" aria-labelledby="modal-edit-ticket-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-edit-ticket-label">Detalles del Folio</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 priority-bar"></div>
                <div class="col-sm-12 status-bar"></div>

                <div class="form-row">
                    <div class="col-md-6 col-sm-12">
                        <label for="ticket">Folio</label>
                        <input type="text" class="form-control" id="ticket" disabled>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <label for="user">Solicitante</label>
                        <input type="text" class="form-control" id="user" disabled>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-md-6 col-sm-12">
                        <label for="date_i">Fecha de Solicitud</label>
                        <input type="text" class="form-control" id="date_i" disabled>
                    </div>
                    <div class="col-md-6 col-sm-12">
                        <label for="date_f">Fecha de Termino</label>
                        <input type="text" class="form-control" id="date_f" disabled>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col-sm-12">
                        <label for="description">Descripción</label>
                        <textarea class="form-control" id="description" rows="3" disabled></textarea>
                    </div>
                </div>
                <form id="finish-ticket" action="{{ route('finish.ticket') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" class="ticket-id">
                    <div class="form-row comment">
                        <div class="col-sm-12">
                            <label for="comment">Comentario</label>
                            <textarea class="form-control" id="comment" name="comment" rows="3" ></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                @can('write_support')
                <button class="btn btn-primary text-white on-revision" type="button" data-dismiss="modal">En Revisión</button>
                <button class="btn btn-success finished" type="button">Terminado</button>
                @endcan
            </div>
        </div>
    </div>
</div>    


{{-- Modal View and edit ticket detaills --}}
<div class="modal fade" id="modal-create-ticket" tabindex="-1" role="dialog" aria-labelledby="modal-edit-ticket-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form action="{{ route('store.ticket') }}" method="POST">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-edit-ticket-label">Detalles del Folio</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="col-md-12 col-sm-12">
                            <label for="user">Solicitante</label>
                            <input type="text" class="form-control" disabled value="{{ Auth::user()->name }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12 col-sm-12">
                            <label for="date_i">Fecha de Solicitud</label>
                            <input type="text" class="form-control" value="{{ Carbon\Carbon::now('America/Mexico_City') }}" disabled>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-sm-12">
                            <label for="description">Descripción</label>
                            <textarea class="form-control"  name="description" rows="3" required></textarea>
                        </div>
                    </div>
                    <h6 class="text-center mt-3">Prioridad</h5>
                    <div class="form-row justify-content-center ">
                    @if(isset($priorities))
                        @foreach ($priorities as $priority)
                            <div class="form-check form-check-inline" >
                                <input class="form-check-input" type="radio" name="priority" value="{{ $priority->id }}" required>
                                <label class="form-check-label" style="background-color: {{ $priority->color }}; color: #fff; padding: 5px; border-radius: 10px;" for="priority">{{ $priority->nombre }}</label>
                            </div>
                        @endforeach
                    @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
                    <button class="btn btn-success" type="submit"><i class="fas fa-ticket-alt mr-2"></i> Crear Ticket</button>
                </div>
            </div>
        </form>
    </div>
</div>    

