{{-- modal-new-program --}}
<div class="modal fade" id="modal-new-update-program" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" action="{{ route('new.update.program') }}" aria-label="{{ __('Register') }}">
            @csrf
            <input type="hidden" name="id" />
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Programa MIR</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-sm-4">
                            <label for="clave" class=" col-form-label text-md-right">Clave</label>
                            <input  type="text" class="form-control{{ $errors->has('clave') ? ' is-invalid' : '' }}" name="clave" value="{{ old('clave') }}" required placeholder="P.20">
                            @if ($errors->has('clave'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('clave') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="name" class=" col-form-label text-md-right">Nombre</label>
                            <input  type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required placeholder="Nombre del Componente">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="objective_fin" class=" col-form-label text-md-right">Objetivo FIN</label>
                            <textarea class="form-control{{ $errors->has('objective_fin') ? ' is-invalid' : '' }}" name="objective_fin" value="{{ old('objective_fin') }}" required placeholder="Objetivo del nivel Fin" rows="3">{{ old('objective_fin') }}</textarea>
                            @if ($errors->has('objective_fin'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('objective_fin') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="objective_prop" class=" col-form-label text-md-right">Objetivo Proposito</label>
                            <textarea class="form-control{{ $errors->has('objective_prop') ? ' is-invalid' : '' }}" name="objective_prop" value="{{ old('objective_prop') }}" required placeholder="Objetivo del nivel Proposito" rows="3">{{ old('objective_prop') }}</textarea>
                            @if ($errors->has('objective_prop'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('objective_prop') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="presupuesto" class=" col-form-label text-md-right">Presupuesto Asignado</label>
                            <input  type="text" class="form-control{{ $errors->has('presupuesto') ? ' is-invalid' : '' }}" name="presupuesto" value="{{ old('presupuesto') }}" required >
                            @if ($errors->has('presupuesto'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('presupuesto') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle mr-2"></i> Actualizar Programa</button>
                </div>
            </div>
        </form>

    </div>
</div>
<script>
    $(function(){
        var operation = "{{ old('operation') }}";
        if(operation == "store")
        {
            $("#modal-new-user").modal("show");
        }
        $(document).on("change", "#eje", function(){
            let id = $(this).val();
            $.ajax({
                url: "{{ route('new.get.pmd.objectives') }}",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id
                },
            }).done(function(result){
                result.forEach(item => {
                    $('#pmd_objective').append($('<option>', { 
                        value: item.id,
                        text : item.punto + ". " + item.nombre
                    }));
                    
                });
            });
        });
    });
</script>