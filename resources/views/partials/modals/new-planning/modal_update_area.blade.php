{{-- modal-new-program --}}
<div class="modal fade" id="modal-update-area" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="{{ route('new.edit.area') }}" aria-label="{{ __('Register') }}">
            @csrf
            <input type="hidden" name="area_id">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nueva Área</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name" class=" col-form-label text-md-right">Nombre</label>
                        <input  type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus placeholder="Nombre del Área">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="program" class=" col-form-label text-md-right">Programa Presupuestario</label>
                        <select required class="form-control{{ $errors->has('program') ? ' is-invalid' : '' }}" name="program" >
                            <option value="">Seleccione una opción</option>
                            @foreach ($programs as $program)
                                <option value="{{ $program->id }}">{{ $program->clave.". ".$program->nombre }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('program'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('program') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="description" class=" col-form-label text-md-right">Objetivo</label>
                        <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description') }}" required placeholder="Objetivo del área" rows="3">{{ old('description') }}</textarea>
                        @if ($errors->has('description'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('description') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle mr-2"></i> Actualizar área</button>
                </div>
            </div>
        </form>

    </div>
</div>
<script>
    $(function(){
        var operation = "{{ old('operation') }}";
        if(operation == "store")
        {
            $("#modal-new-user").modal("show");
        }
    });
</script>