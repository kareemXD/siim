{{-- modal-new-Component --}}
<div class="modal fade" id="modal-update-componenent" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="max-width: 80vw;" role="document">
        <form id="form-action-update" action="{{ route('new.edit.component') }}" method="POST" style="overflow-x: hidden;">
            @csrf
            <input type="hidden" name="component_id">
            <div class="modal-content" style="padding: 5px;">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Componente</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <fieldset class="col-12 fieldset">
                    <input type="hidden" name="validate_normative_update">
                    <legend class="legend">Normativas:</legend>
                    <div class="row">
                        <div class="col" id="alert-normative-update">
                            
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-5">
                            <select class="select2 normative" id="normative-update">
                                <option value=" " selected disabled>Seleccioné la Normativa</option>
                                @foreach ($normatives as $normative)
                                    @if ($normative->nombre != "MIR")
                                        <option value="{{ $normative->id }}">{{ $normative->nombre }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="col-6">
                            <select class="select2" id="actionLine-update" disabled>
                                <option value=" " selected disabled>Seleccioné la Línea de Acción</option>
                            </select>
                        </div>
                        <div class="col-1">
                            <button type="button" class="btn btn-success btn-add-normative" data-toggle="tooltip" data-placement="top" title="Añadir"><i class="fas fa-plus-circle"></i></button> 
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <table class="table dataTable">
                                <thead>
                                    <tr>
                                        <td class="col-11">Línea de Acción</td>
                                        <td class="col-1">Acción</td>
                                    </tr>
                                </thead>
                                <tbody id="normatives-content-update">
                                    <tr class="text-center" id="text-test">
                                        <td colspan="2">Ninguna Línea de Acción Agregada</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="col-12 fieldset">
                    <legend class="legend">Responsable:</legend>
                    <div class="row mb-3">
                        <div class="col">
                            <select class="select2 responsable" name="responsable" required>
                                <option value="" selected disabled>Seleccioné al Responsable</option>
                                @foreach ($direction->positions as $position)
                                    <option value="{{ $position->id }}">{{ $position->nombre }} ( {{ $position->direction->abreviacion }} )</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </fieldset>
                <fieldset class="col-12 fieldset">
                    <legend class="legend">Componentes:</legend>
                    {{-- <div class="form-group">
                        <label for="program" class=" col-form-label text-md-right">Programa MIR</label>
                        <select required class="form-control{{ $errors->has('program') ? ' is-invalid' : '' }}" name="program" id="program">
                            <option value="">Seleccione una opción</option>
                            @foreach ($programs as $program)
                                <option value="{{ $program->id }}">{{ $program->clave.". ".$program->nombre }}</option>
                            @endforeach
                        </select>
                        @if ($errors->has('program'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('program') }}</strong>
                            </span>
                        @endif
                    </div> --}}
                    <div class="form-row mb-3">
                        <div class="col-6">
                            <label for="type">Sentido del Indicador</label>
                            <select class="select2 settings-update" name="type">
                                <option value="0" selected>Ascendente</option>
                                <option value="1">Descendente</option>
                            </select>
                        </div>
                        <div class="col-6">
                            <label for="value">Tipo de valor del componente</label>
                            <select class="select2 settings-update" name="value">
                                <option value="0" selected>Numerico</option>
                                <option value="1">Porcentual</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row mb-3">
                        <div class="col-6">
                            <label for="periodo">Periodo de Verificación</label>
                            <select class="select2" name="periodo">
                                <option value="Trimestral" selected>Trimestral</option>
                                <option value="Mensual">Mensual</option>
                                <option value="Semestral">Semestral</option>
                                <option value="Anual">Anual</option>
                            </select>
                        </div>
                        <div class="col-6">
                            <label for="value">Dimensión</label>
                            <select class="select2" name="dimension">
                                <option value="Eficacia" selected>Eficacia</option>
                                <option value="Eficiencia">Eficiencia</option>
                                <option value="Calidad">Calidad</option>
                                <option value="Economía">Economía</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row mb-3">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-12">
                                    <label for="activity">Componente</label>
                                    <textarea class="form-control" name="activity" required rows="4"></textarea>
                                    {{-- <input required type="text" class="form-control " name="activity"> --}}
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-4">
                                    <label for="activity">U. de M.</label>
                                    <input required type="text" class="form-control " name="unity" >
                                </div>
                                <div class="col-4">
                                    <label for="beneficiarios">Beneficiarios</label>
                                    <input required min="0" type="number" class="form-control" name="beneficiarios">
                                </div>
                                <div class="col-4">
                                    <label for="activity">Meta Anual</label>
                                    <input required min="0" type="number" step=".01" class="form-control total total-input" name="year" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-3">
                                    <label for="activity">1° Trimestre</label>
                                    <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth trimounth-parent-1 trimounth-input" name="first">
                                </div>
                                <div class="col-3">
                                    <label for="activity">2° Trimestre</label>
                                    <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth trimounth-parent-2 trimounth-input" name="second">
                                </div>
                                <div class="col-3">
                                    <label for="activity">3° Trimestre</label>
                                    <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth trimounth-parent-3 trimounth-input" name="thirth">
                                </div>
                                <div class="col-3">
                                    <label for="activity">4° Trimestre</label>
                                    <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth trimounth-parent-4 trimounth-input" name="four">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row mb-3">
                        <div class="col">
                            <label for="formula">Formula</label>
                            <input required type="text" class="form-control " name="formula">
                        </div>
                    </div>
                    <div class="form-row mb-3">
                        <div class="col">
                            <label for="objetivo">Objetivo del componente</label>
                            <textarea class="form-control" name="objetivo" required rows="3"></textarea>
                        </div>
                    </div>
                    {{-- <div class="row justify-content-end mt-2">
                        <button type="button" class="btn btn-primary mr-3 mb-2 btn-add-action"><i class="fas fa-plus-circle mr-2"></i> Agregar Subacción</button>
                    </div> --}}
                    <div id="actions-poa"></div>
                    <div class="row justify-content-center mt-2">
                        <button type="submit" class="btn btn-primary mr-3 mb-2 btn-submit-action"><i class="fas fa-save mr-2"></i> Guardar</button>
                    </div>
                </fieldset>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on("submit", "#form-action-update", function(e){
            if($('[name="validate_normative_update"]').val() != 1)
            {
                e.preventDefault();
                $("#alert-normative-update").empty();
                $("#alert-normative-update").append('<div class="alert alert-danger text-center">\
                                                Vincule al menos una Normativa.\
                                            </div>');
            }
        });
        
        $(document).on("change", "#normative-update", function(){
            var normative = $(this).val(),
                direction = $('[name="direction"]').val(),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('new.get.action.lines.by.normative') }}",
                type: "POST",
                data: {
                    _token: token,
                    normative: normative,
                    direction: direction,
                },
            }).done(function(result){
                $("#actionLine-update").empty();
                $("#actionLine-update").select2();
                $("#actionLine-update").append('<option value=" " selected disabled>Seleccioné la Línea de Acción</option>');
                result.forEach(element => {
                    if(element.punto == undefined)
                    {
                        $("#actionLine-update").append('<option value="'+element.id+'">'+element.nombre+'</option>');
                    }else
                    {
                        $("#actionLine-update").append('<option value="'+element.id+'">'+ element.punto + ". " +element.nombre+'</option>');
                    }
                });
                $("#actionLine-update").removeAttr("disabled");
            });
        });

        $(document).on("click", ".btn-add-normative", function(){
            var action = $("#actionLine-update option:selected").text();
            var norma = $("#normative-update option:selected").text();
            $("#text-test").remove();
            if(norma == "Plan Municipal")
            {
                $("#normatives-content-update").append('<tr>\
                        <td>'+ action +' <input type="hidden" name="pm_normatives[]" value="'+ $("#actionLine-update").val() +'" /> </td>\
                        <td><button type="button" class="btn btn-danger btn-delete-normative"><i class="fas fa-trash-alt"></i></button></td>\
                    </tr>'
                );
            }
            if(norma == "GDM(INAFED)")
            {
                $("#normatives-content-update").append('<tr>\
                        <td>'+ action +' <input type="hidden" name="inf_normatives[]" value="'+ $("#actionLine-update").val() +'" /> </td>\
                        <td><button type="button" class="btn btn-danger btn-delete-normative"><i class="fas fa-trash-alt"></i></button></td>\
                    </tr>'
                );
            }
            if(norma == "Agenda 2030")
            {
                $("#normatives-content-update").append('<tr>\
                        <td>'+ action +' <input type="hidden" name="sche_normatives[]" value="'+ $("#actionLine-update").val() +'" /> </td>\
                        <td><button type="button" class="btn btn-danger btn-delete-normative"><i class="fas fa-trash-alt"></i></button></td>\
                    </tr>'
                );
            }
            if(norma == "MIR")
            {
                $("#normatives-content-update").append('<tr>\
                        <td>'+ action +' <input type="hidden" name="mir_normatives[]" value="'+ $("#actionLine-update").val() +'" /> </td>\
                        <td><button type="button" class="btn btn-danger btn-delete-normative"><i class="fas fa-trash-alt"></i></button></td>\
                    </tr>'
                );
            }
            $("#actionLine-update").val(" ");
            $("#actionLine-update").select2();
            $('[name="validate_normative_update"]').val(1);
        });

        $(document).on("change", ".settings-update", function(){
            var flag = false;
            var modal = $("#modal-update-componenent");
            $(".settings-update").each(function(i){
                if($(this).val() == 1)
                {
                    flag = true;
                }
            });
            if(flag)
            {
                modal.find(".trimounth").each(function(i){
                    $(this).removeClass("trimounth");
                });
                
                modal.find(".total").each(function(i){
                    $(this).removeAttr("readonly");
                });
            }else
            {
                modal.find(".tri-inp").each(function(i){
                    $(this).addClass("trimounth");
                });
                
                modal.find(".total").each(function(i){
                    $(this).attr("readonly", true);
                });
            }
        });

        $(document).on("click", ".btn-delete-normative-update", function(){
            var _this = $(this);
            _this.parents("tr").fadeOut("slow", function(){
                $(this).remove();
                var trs = $("#normatives-content-update").find("tr");
                if(trs.length == 0)
                {
                    $("#normatives-content-update").append('<tr class="text-center" id="text-test">\
                            <td colspan="2">Ninguna Línea de Acción Agregada</td>\
                        </tr>'
                    );
                    $('[name="validate_normative_update"]').val(0);
                }
            });
            
        });

        $(document).on("focusout", ".trimounth", function(){
            var _this = $(this),
                val_sub = 0,
                val = 0,
                parent = $(this).data("parent");
            $('[data-parent="'+parent+'"]').each(function(i){
                val_sub += Number($(this).val());
            });
            $(".trimounth-parent-"+parent).val(val_sub);
            val_sub = 0;
            $(".trimounth-input").each(function(i){
                val_sub += Number($(this).val());
            });
            $(".total-input").val(val_sub);

            _this.parents(".form-row").find(".trimounth").each(function(i){
                val += Number($(this).val());
            });
            _this.parents(".form-row").find(".total").val(val);
        });

        $(document).on("click", ".btn-add-action", function(){
            var trs = $("#actions-poa").find(".form-row");
            if(trs.length == 0)
            {
                $(".trimounth-input").each(function(i){
                    $(this).val(0);
                });
                $(".total-input").val(0);
                $(".trimounth-input").attr("readonly", true);
                $(".total-input").attr("readonly", true);
            }
            var attri = "readonly";
            $(".settings-update").each(function(i){
                if($(this).val() == 1)
                {
                    attri = "";
                }
            });
            $("#actions-poa").append('<div class="form-row">\
                <div class="col-6">\
                    <label for="activity">Subactividad</label>\
                    <input required type="text" class="form-control" name="subactivity[]">\
                </div>\
                <div class="col-1">\
                    <label for="activity">Meta Anual</label>\
                    <input required '+attri+' type="number" step=".01" min="0" class="form-control total" name="subyear[]">\
                </div>\
                <div class="col-1">\
                    <label for="activity">1° Trimestre</label>\
                    <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth" data-parent="1"  name="subfirst[]">\
                </div>\
                <div class="col-1">\
                    <label for="activity">2° Trimestre</label>\
                    <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth" data-parent="2" name="subsecond[]">\
                </div>\
                <div class="col-1">\
                    <label for="activity">3° Trimestre</label>\
                    <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth" data-parent="3" name="subthirth[]">\
                </div>\
                <div class="col-1">\
                    <label for="activity">4° Trimestre</label>\
                    <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth" data-parent="4" name="subfour[]">\
                </div>\
                <div class="col-1 text-center">\
                    <label for="activity" style="display:block;">Acciones</label>\
                    <button type="button" class="btn btn-danger btn-delete-subaction"><i class="fa fa-trash-alt"></i></button> \
                </div>\
            </div>'
            );
            
        });

        $(document).on("click", ".btn-delete-subaction", function(){
            var _this = $(this),
                val_sub = 0,
                index = 0;
            _this.parents(".form-row").fadeOut("slow", function(){
                $(this).remove();
                _this.parents(".form-row").find(".trimounth").each(function(i){
                    index = i + 1;
                    val_sub = ($(".trimounth-parent-"+index).val()) - (Number($(this).val()));
                    $(".trimounth-parent-"+index).val(val_sub)
                });
                val_sub = 0;
                $(".trimounth-input").each(function(i){
                    val_sub += Number($(this).val());
                });
                $(".total-input").val(val_sub);
                var trs = $("#actions-poa").find(".form-row");
                if(trs.length == 0)
                {
                    $(".trimounth-input").removeAttr("readonly");
                }
            });

        });
    });
</script>