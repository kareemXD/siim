{{-- modal-new-program --}}
<div class="modal fade" id="modal-new-program" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" action="{{ route('new.store.program') }}" aria-label="{{ __('Register') }}">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Nuevo Programa MIR</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-row">
                        <div class="form-group col-sm-4">
                            <label for="clave" class=" col-form-label text-md-right">Clave</label>
                            <input  type="text" class="form-control{{ $errors->has('clave') ? ' is-invalid' : '' }}" name="clave" value="{{ old('clave') }}" required placeholder="P.20">
                            @if ($errors->has('clave'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('clave') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col">
                            <label for="name" class=" col-form-label text-md-right">Nombre</label>
                            <input  type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required placeholder="Nombre del Programa Presupestario">
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label for="description" class=" col-form-label text-md-right">Objetivo</label>
                            <textarea class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description') }}" required placeholder="Objetivo del Componente" rows="3">{{ old('description') }}</textarea>
                            @if ($errors->has('description'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('description') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> --}}
                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label for="eje" class=" col-form-label text-md-right">Eje del PMD</label>
                            <select class="form-control{{ $errors->has('eje') ? ' is-invalid' : '' }}" name="eje" id="eje">
                                <option value="">Seleccione una opción</option>
                                @foreach ($axes as $axe)
                                    <option value="{{ $axe->id }}">{{ $axe->punto.". ".$axe->nombre }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('eje'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('eje') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> --}}
                    {{-- <div class="form-row">
                        <div class="form-group col">
                            <label for="pmd_objective" class=" col-form-label text-md-right">Objetivo del PMD</label>
                            <select class="form-control{{ $errors->has('pmd_objective') ? ' is-invalid' : '' }}" name="pmd_objective" id="pmd_objective">
                                <option value="">Seleccione una opción</option>
                            </select>
                            @if ($errors->has('pmd_objective'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('pmd_objective') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> --}}
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="objective_fin" class=" col-form-label text-md-right">Objetivo FIN</label>
                            <textarea class="form-control{{ $errors->has('objective_fin') ? ' is-invalid' : '' }}" name="objective_fin" value="{{ old('objective_fin') }}" required placeholder="Objetivo del nivel Fin" rows="3">{{ old('objective_fin') }}</textarea>
                            @if ($errors->has('objective_fin'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('objective_fin') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    {{-- <div class="form-row">
                        <div class="form-group col-sm-4">
                            <label for="dimension_fin" class=" col-form-label text-md-right">Dimension FIN</label>
                            <input  type="text" class="form-control{{ $errors->has('dimension_fin') ? ' is-invalid' : '' }}" name="dimension_fin" value="{{ old('dimension_fin') }}" required placeholder="Eficiencia">
                            @if ($errors->has('dimension_fin'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('dimension_fin') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="frecuencia_fin" class=" col-form-label text-md-right">Fecuencia FIN</label>
                            <input  type="text" class="form-control{{ $errors->has('frecuencia_fin') ? ' is-invalid' : '' }}" name="frecuencia_fin" value="{{ old('frecuencia_fin') }}" required placeholder="Anual">
                            @if ($errors->has('frecuencia_fin'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('frecuencia_fin') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="tipo_fin" class=" col-form-label text-md-right">Tipo FIN</label>
                            <input  type="text" class="form-control{{ $errors->has('tipo_fin') ? ' is-invalid' : '' }}" name="tipo_fin" value="{{ old('tipo_fin') }}" required placeholder="Estrategico">
                            @if ($errors->has('tipo_fin'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('tipo_fin') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div> --}}
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="objective_prop" class=" col-form-label text-md-right">Objetivo Proposito</label>
                            <textarea class="form-control{{ $errors->has('objective_prop') ? ' is-invalid' : '' }}" name="objective_prop" value="{{ old('objective_prop') }}" required placeholder="Objetivo del nivel Proposito" rows="3">{{ old('objective_prop') }}</textarea>
                            @if ($errors->has('objective_prop'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('objective_prop') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col">
                            <label for="presupuesto" class=" col-form-label text-md-right">Presupuesto Asignado</label>
                            <input  type="text" class="form-control{{ $errors->has('presupuesto') ? ' is-invalid' : '' }}" name="presupuesto" value="{{ old('presupuesto') }}" required >
                            @if ($errors->has('presupuesto'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('presupuesto') }}</strong>
                                </span>
                            @endif
                        </div>
                        {{-- <div class="form-group col-sm-4">
                            <label for="frecuencia_prop" class=" col-form-label text-md-right">Fecuencia Proposito</label>
                            <input  type="text" class="form-control{{ $errors->has('frecuencia_prop') ? ' is-invalid' : '' }}" name="frecuencia_prop" value="{{ old('frecuencia_prop') }}" required placeholder="Anual">
                            @if ($errors->has('frecuencia_prop'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('frecuencia_prop') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="form-group col-sm-4">
                            <label for="tipo_prop" class=" col-form-label text-md-right">Tipo Proposito</label>
                            <input  type="text" class="form-control{{ $errors->has('tipo_prop') ? ' is-invalid' : '' }}" name="tipo_prop" value="{{ old('tipo_prop') }}" required placeholder="Estrategico">
                            @if ($errors->has('tipo_prop'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('tipo_prop') }}</strong>
                                </span>
                            @endif
                        </div> --}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle mr-2"></i> Registrar Programa</button>
                </div>
            </div>
        </form>

    </div>
</div>
<script>
    $(function(){
        var operation = "{{ old('operation') }}";
        if(operation == "store")
        {
            $("#modal-new-user").modal("show");
        }
        $(document).on("change", "#eje", function(){
            let id = $(this).val();
            $.ajax({
                url: "{{ route('new.get.pmd.objectives') }}",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id
                },
            }).done(function(result){
                result.forEach(item => {
                    $('#pmd_objective').append($('<option>', { 
                        value: item.id,
                        text : item.punto + ". " + item.nombre
                    }));
                    
                });
            });
        });
    });
</script>