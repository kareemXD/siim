<div class="modal fade" id="modal-pre-new" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Previsualización</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="titulo col-xl-12" ></div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 header"></div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 img"> 
        
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-xl-12 description"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>