{{-- Modal Logout --}}
<div class="modal fade" id="modal-observation-pmdu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form id="form-data" method="POST" action="{{ route('response.observation.pmdu') }}" class="modal-content">
            @csrf
            <input type="hidden" name="id" id="id">
            <input type="hidden" name="responsed" id="responsed">
            <input type="hidden" name="resend" id="resend">
            <input type="hidden" name="page" id="page">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Observación PMDU</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <label for="folio">Folio</label>
                        <input class="form-control" disabled id="data-folio" type="text" class="validate">
                    </div>
                    <div class="col-sm-4">
                        <label for="origin">Origen</label>
                        <select id="data-origin" disabled class="form-control">
                            <option value="">Seleccione Origen</option>
                            <option value="Bitacora">Bitacora</option>
                            <option value="Oficio">Oficio</option>
                            <option value="Pagina Web">Pagina Web</option>
                            <option value="Correo">Correo</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="procedence">¿ Procede ? </label>
                        <select id="data-procedence" name="procede" class="form-control">
                            <option value="">Seleccione Procedencia</option>
                            <option value="Procede">Procede</option>
                            <option value="Procede Parcialmente">Procede Parcialmente</option>
                            <option value="No Procede">No Procede</option>
                            <option value="No Procede Rev">No Procede Rev</option>
                            <option value="Relativo a otra área">Relativo a otra área</option>
                            <option value="Sin Aporte">Comentario</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-body gray-border">
                <h5 class="modal-title">Datos del Observador</h5>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="name">Nombre (s)</label>
                        <input class="form-control" id="data-name" disabled type="text" class="required validate">
                    </div>
                    <div class="col-sm-6">
                        <label for="lastname">Apellido (s)</label>
                        <input class="form-control" id="data-lastname" disabled type="text" class="required validate">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="sex">Sexo</label>
                        <select id="data-sex" disabled class="form-control">
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="age">Edad</label>
                        <input class="form-control" id="data-age" disabled type="number">
                    </div>
                    <div class="col-sm-4">
                        <label for="tel">Telefono</label>
                        <input class="form-control" id="data-tel" disabled type="number">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="email">Correo</label>
                        <input class="form-control" id="data-email" disabled type="email" class="required validate">
                    </div>
                    <div class="col-sm-4">
                        <label for="address">Dirección</label>
                        <input class="form-control" id="data-address" disabled type="text" class="required validate">
                    </div>
                    <div class="col-sm-4">
                        <label for="community">Localidad</label>
                        <select id="data-community" disabled class="form-control">
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="scholarship">Escolaridad</label>
                        <select id="data-scholarship" disabled class="form-control">
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label for="occupation">Ocupación</label>
                        <select id="data-occupation" disabled class="form-control">
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-body gray-border">
                <h5 class="modal-title">Secciones Observadas</h5>
                <div class="row mt-2">
                    @include('partials.modals.webPage.pmdu_image')
                </div>
            </div>
            <div class="modal-body gray-border">
                <h5 class="modal-title">Observacion (es)</h5>
                <div class="row mt-2">
                    <div class="col-sm-12">
                        <textarea id="data-observation" disabled class="form-control" rows="10"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-body gray-border">
                <h5 class="modal-title">Respuesta</h5>
                <div class="row mt-2">
                    <div class="col-sm-12">
                        <textarea id="data-response" class="form-control" name="response" rows="10"></textarea>
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-sm-12">
                        <label for="copy">Copia para:</label>
                        <input class="form-control" id="data-copy" name="copy" type="text" placeholder="Ejemplo: C.c.p. IMPLAN; C.c.p. Desarrollo Urbano">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                @can('write_pmdu_observations')
                    <button class="btn btn-primary" id="btn-submit" type="button"><i class="fas fa-save mr-2"></i> Responder</button>
                @endcan
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on("click", "#btn-submit", function(e){
            e.preventDefault();
            
            $.confirm({
                closeIcon: true,
                boxWidth: '500px',
                useBootstrap: false,
                title: '¿ Estas Seguro (a) ?',
                content: '¿ Cual es la acción que desea realizar ?',
                buttons: {
                    Confirmar: {
                        text: 'Enviar correo y Actualizar',
                        btnClass: 'btn btn-primary btn-confirm',
                        action: function(){

                            $("#form-data")[0].submit();
                        }
                    },
                    Cancelar: {
                        text: 'Solo Actualizar',
                        btnClass: 'btn btn-secondary btn-confirm',
                        action: function(){
                            $("#resend").val(false);
                            $("#form-data")[0].submit();
                        }
                    }
                }
            });
        });
    });
</script>