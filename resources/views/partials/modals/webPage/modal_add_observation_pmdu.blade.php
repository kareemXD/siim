{{-- Modal Logout --}}
<div class="modal fade" id="modal-add-observation-pmdu" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form id="form-add-data" method="POST" action="{{ route('add.observation.pmdu') }}" class="modal-content">
            @csrf
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Observación PMDU</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-4">
                        <label for="folio">Folio</label>
                        <input class="form-control" required id="new-folio" name="folio" type="text" class="required validate">
                    </div>
                    <div class="col-sm-4">
                        <label for="origin">Origen</label>
                        <select id="new-origin" required name="origin" class="form-control">
                            <option value="">Seleccione Origen</option>
                            <option value="Bitacora">Bitacora</option>
                            <option value="Oficio">Oficio</option>
                            <option value="Correo">Correo</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="procedence">¿ Procede ? </label>
                        <select id="new-procedence" required name="procedence" class="form-control">
                            <option value="">Seleccione Procedencia</option>
                            <option value="Procede">Procede</option>
                            <option value="Procede Parcialmente">Procede Parcialmente</option>
                            <option value="No Procede">No Procede</option>
                            <option value="No Procede Rev">No Procede Rev</option>
                            <option value="Relativo a otra área">Relativo a otra área</option>
                            <option value="Sin Aporte">Comentario</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-body gray-border">
                
                <h5 class="modal-title">Datos del Observador</h5>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="name">Nombre (s)</label>
                        <input class="form-control" id="new-name" name="name" type="text" class="required validate">
                    </div>
                    <div class="col-sm-6">
                        <label for="lastname">Apellido (s)</label>
                        <input class="form-control" id="new-lastname" name="lastname" type="text" class="required validate">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="sex">Sexo</label>
                        <select id="new-sex" name="sex" class="form-control">
                            <option value="" selected>Seleccione el Sexo</option>
                            <option value="Masculino">Masculino</option>
                            <option value="Femenino">Femenino</option>
                            <option value="Otro">Otro</option>
                        </select>
                    </div>
                    <div class="col-sm-4">
                        <label for="age">Edad</label>
                        <input class="form-control" id="new-age" name="age" type="number">
                    </div>
                    <div class="col-sm-4">
                        <label for="tel">Telefono</label>
                        <input class="form-control" id="new-tel" name="tel" type="number">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <label for="email">Correo</label>
                        <input class="form-control" id="new-email" name="email" type="email" class="required validate">
                    </div>
                    <div class="col-sm-4">
                        <label for="address">Dirección</label>
                        <input class="form-control" id="new-address" name="address" type="text" class="required validate">
                    </div>
                    <div class="col-sm-4">
                        <label for="community">Localidad</label>
                        <select id="new-community" name="community" class="form-control">
                            <option value="" selected>Seleccione una localidad</option>
                            @foreach ($locations as $location)
                                <option value="{{ $location->nombre }}">{{ $location->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="scholarship">Escolaridad</label>
                        <select id="new-scholarship" name="scholarship" class="form-control">
                            <option value="" disabled selected>Seleccione los estudios</option>
                            <option value="Sin Estudios">Sin Estudios</option>
                            <option value="Primaria">Primaria</option>
                            <option value="Secundaria">Secundaria</option>
                            <option value="Bachillerato">Bachillerato</option>
                            <option value="Universidad">Universidad</option>
                            <option value="Maestria">Maestria</option>
                            <option value="Doctorado">Doctorado</option>
                        </select>
                    </div>
                    <div class="col-sm-6">
                        <label for="occupation">Ocupación</label>
                        <select id="new-occupation" name="occupation" class="form-control">
                            <option value="" disabled selected>Seleccione la ocupación</option>
                            @foreach ($sectores as $sector)
                                <option value="{{ $sector->nombre }}">{{ $sector->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-body gray-border">
                <h5 class="modal-title">Observacion (es)</h5>
                <div class="row mt-2">
                    <div class="col-sm-12">
                        <textarea id="new-observation" name="observation" class="form-control" rows="10"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                <button class="btn btn-primary" type="submit"><i class="fas fa-save mr-2"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
<script>
    
</script>
