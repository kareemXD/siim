{{-- Modal Logout --}}
<div class="modal fade" id="modal-add-signature" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form id="form-signature" method="POST" action="{{ route('signature.pdf.response') }}" class="modal-content" enctype="multipart/form-data">
            @csrf
            <input type="hidden" id="observation_action" name="observation_action" value="1">  {{-- 1 = firmar y enviar, 2 = Solo Firmar --}}
            <div class="modal-body">
                <div class="row mt-3">
                    <div class="col-xl-4"></div>
                    <div class="col-xl-4">
                        <label for="file">  Firma Electronica</label>
                        <input type="file" name="file"  class="file" id="file">
                    </div>
                    <div class="col-xl-4">                      
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <div class="col-sm-8 offset-sm-2">
                        <label for="folio" class="col-sm-2 col-form-label">Folios</label>
                        <div class="col-sm-10">
                            <select name="folio[]" id="folio" class="required select2 form-control {{ $errors->has('folio') ? ' is-invalid' : '' }}" multiple>
                                <option value="" selected>Todos</option>
                                @foreach ($observations as $observation)
                                    @if ($observation->procede == "Relativo a otra área" || $observation->procede == "Sin Aporte")
                                        <option value="{{ $observation->folio }}">{{ $observation->folio }}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                <button class="btn btn-primary" type="button" id="btn-send-signature"><i class="fas fa-save mr-2"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on("click", "#btn-send-signature", function(){
            $.confirm({
                closeIcon: true,
                boxWidth: '500px',
                useBootstrap: false,
                title: '¿ Estas Seguro (a) ?',
                content: '¿ Cual es la acción que desea realizar ?',
                buttons: {
                    Confirmar: {
                        text: 'Enviar correos y Firmar',
                        btnClass: 'btn btn-primary btn-confirm',
                        action: function(){

                            $("#form-signature")[0].submit();
                        }
                    },
                    Cancelar: {
                        text: 'Solo Firmar',
                        btnClass: 'btn btn-secondary btn-confirm',
                        action: function(){
                            $("#observation_action").val(2);
                            $("#form-signature")[0].submit();
                        }
                    }
                }
            });
        });
    });
</script>