{{-- Modal Logout --}}
<div class="modal fade" id="modal-send-ofices" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" id="form-send-ofices" action="{{ route('send.all.ofices') }}" class="modal-content" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                <div class="form-group mt-1 mb-1 row margin-10">
                    <div class="col-sm-8 offset-sm-2">
                        <label for="procedence">Oficios a Enviar</label>
                        <select required name="procedence" class="form-control">
                            <option value="">Seleccione Procedencia</option>
                            <option value="Procede">Procede</option>
                            <option value="Procede Parcialmente">Procede Parcialmente</option>
                            <option value="No Procede">No Procede</option>
                            <option value="No Procede Rev">No Procede Rev</option>
                            <option value="Relativo a otra área">Relativo a otra área</option>
                            <option value="Sin Aporte">Comentario</option>
                        </select>
                    </div>
                </div>
                <div class="form-group mt-1 mb-1 row margin-10">
                    <div class="col-sm-8 offset-sm-2">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="send_again" name="send_again" value="true">
                            <label class="custom-control-label" for="send_again">¿ Reenviar oficios ?</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                <button class="btn btn-primary" type="button" id="btn-send-ofices"><i class="fas fa-save mr-2"></i> Guardar</button>
            </div>
        </form>
    </div>
</div>
<script>
    $(function(){
        $(document).on("click", "#btn-send-ofices", function(){
            $.confirm({
                closeIcon: true,
                boxWidth: '500px',
                useBootstrap: false,
                title: '¿ Estas Seguro (a) ?',
                content: 'Esta acción realiza el envió de los oficios.',
                buttons: {
                    Confirmar: {
                        text: 'Aceptar',
                        btnClass: 'btn btn-primary btn-confirm',
                        action: function(){
                            $("#form-send-ofices")[0].submit();
                        }
                    },
                    Cancelar: {
                        text: 'Cerrar',
                        btnClass: 'btn btn-secondary btn-confirm',
                        action: function(){
                            
                        }
                    }
                }
            });
        });
    });
</script>