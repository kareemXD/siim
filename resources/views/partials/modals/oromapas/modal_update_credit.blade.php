<div class="modal fade" id="modal-update-credit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form  method="POST" action="{{route('update.credit')}}"  id="frSaldo" aria-label="{{ __('Register') }}" enctype="multipart/form-data">
            @csrf            
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Saldo</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row ml-5">
                        <div class="col-xl-4"></div>
                        <div class="col-xl-4">
                            <label for="file">  Archivo CSV</label>
                            <input type="file" name="file"  class="file" id="file">
                        </div>
                        <div class="col-xl-4">                      
                        </div>
                    </div>
                    <div class="row mt-5">
                        <div class="col-xl-4"></div>
                        <div class="col-xl-4">
                                <select class="form-control localidad" name="localidad" id="localidades">
                                    <option value="" selected>Seleccione una localidad</option>
                                    @foreach ($localidades as $localidad)
                                        <option value="{{$localidad->shape}}">{{$localidad->nombre}}</option>
                                    @endforeach
                                </select>
                        </div>
                        <div class="col-xl-4">
                        </div>
                    </div>                                                              
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="submit"><i class="fas fa-check mr-2"></i> Guardar</button>
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                </div>
            </div>
        </form>
    </div>
</div>