{{-- Modal Logout --}}
<div class="modal fade" id="modal-edit-predio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" class='NewOffice' id="frPredio" aria-label="{{ __('Register') }}" enctype="multipart/form-data">
            @csrf            
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Predio</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="contrato">No. Contrato</label>
                            <input class="form-control {{ $errors->has('contrato') ? ' is-invalid' : '' }}" type="text" name="contrato" id="contrato"  >                       
                        </div>
                        <div class="col-xl-6">
                            <label for="">Calle</label>
                            <input class="form-control {{ $errors->has('calle') ? ' is-invalid' : '' }}" type="text" name="calle" id="calle"  > 
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-12">
                            <label for="contrato">Numero</label>
                            <input class="form-control {{ $errors->has('numero') ? ' is-invalid' : '' }}" type="text" name="numero" id="numero"  >                       
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="contrato">Entre Calle 1</label>
                            <input class="form-control {{ $errors->has('calle_1') ? ' is-invalid' : '' }}" type="text" name="calle_1" id="calle_1"  >                       
                        </div>
                        <div class="col-xl-6">
                            <label for="contrato">Entre Calle 2</label>
                            <input class="form-control {{ $errors->has('calle_2') ? ' is-invalid' : '' }}" type="text" name="calle_2" id="calle_2"  >                       
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="contrato">Colonia</label>
                            <input class="form-control {{ $errors->has('colonia') ? ' is-invalid' : '' }}" type="text" name="colonia" id="colonia"  >                       
                        </div>
                        <div class="col-xl-6">
                            <label for="contrato">Localidad</label>
                            <input class="form-control {{ $errors->has('localidad') ? ' is-invalid' : '' }}" type="text" name="localidad" id="localidad"  >                       
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="contrato">Tipo de servicio</label>
                            <input class="form-control {{ $errors->has('tipoServicio') ? ' is-invalid' : '' }}" type="text" name="tipoServicio" id="tipoServicio"  >                       
                        </div>
                        <div class="col-xl-6">
                            <label for="contrato">Giro</label>
                            <input class="form-control {{ $errors->has('giro') ? ' is-invalid' : '' }}" type="text" name="giro" id="giro"  >                       
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            {{-- <label for="contrato">Tiene Agua</label> --}}
                            {{-- <input class="form-control {{ $errors->has('agua') ? ' is-invalid' : '' }}" type="text" name="agua" id="agua"  > --}}
                            <div class="form-group">
                                <span class="switch">
                                    <input type="checkbox" value='true' class="switch" name="agua" id="switch-agua">
                                    <label for="switch-agua">Tiene Agua?</label>
                                </span>
                            </div>                       
                        </div>
                        <div class="col-xl-6">
                            {{-- <label for="contrato">Tiene Drenaje</label> --}}
                            <div class="form-group">
                                <span class="switch">
                                    <input type="checkbox" value='true' class="switch" name="drenaje" id="switch-drenaje">
                                    <label for="switch-drenaje">Tiene Drenaje?</label>
                                </span>
                            </div>
                            {{-- <input class="form-control {{ $errors->has('drenaje') ? ' is-invalid' : '' }}" type="text" name="drenaje" id="drenaje"  >                        --}}
                        </div>
                        <div class="col-xl-6">
                            <label for="contrato">Serie del medidor</label>
                            <input class="form-control {{ $errors->has('medidorSerie') ? ' is-invalid' : '' }}" type="text" name="medidorSerie" id="medidorSerie"  >
                            <input type="hidden" name="id" id="predio">  
                        </div>
                    </div>                                                              
                </div>
                

                <div class="modal-footer">
                    @can("write_oromapas_son")
                        <button class="btn btn-primary" type="submit"><i class="fas fa-check mr-2"></i> Guardar</button>
                    @endcan
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                </div>
            </div>
        </form>

    </div>
</div>
{{-- Modal Logout --}}
<div class="modal fade" id="modal-new-predio" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST" class='NewOffice' id="frNewPredio" aria-label="{{ __('Register') }}" enctype="multipart/form-data">
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Predio</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="contrato">No. Contrato</label>
                            <input class="form-control {{ $errors->has('contrato') ? ' is-invalid' : '' }}" type="text" name="contrato"   >                       
                        </div>
                        <div class="col-xl-6">
                            <label for="">Calle</label>
                            <input class="form-control {{ $errors->has('calle') ? ' is-invalid' : '' }}" type="text" name="calle"   > 
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-12">
                            <label for="contrato">Numero</label>
                            <input class="form-control {{ $errors->has('numero') ? ' is-invalid' : '' }}" type="text" name="numero"   >                       
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="contrato">Entre Calle 1</label>
                            <input class="form-control {{ $errors->has('calle_1') ? ' is-invalid' : '' }}" type="text" name="calle_1"   >                       
                        </div>
                        <div class="col-xl-6">
                            <label for="contrato">Entre Calle 2</label>
                            <input class="form-control {{ $errors->has('calle_2') ? ' is-invalid' : '' }}" type="text" name="calle_2"   >                       
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="contrato">Colonia</label>
                            <input class="form-control {{ $errors->has('colonia') ? ' is-invalid' : '' }}" type="text" name="colonia"   >                       
                        </div>
                        <div class="col-xl-6">
                            <label for="contrato">Localidad</label>
                            <input class="form-control {{ $errors->has('localidad') ? ' is-invalid' : '' }}" type="text" name="localidad"   >                       
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="contrato">Tipo de servicio</label>
                            <input class="form-control {{ $errors->has('tipoServicio') ? ' is-invalid' : '' }}" type="text" name="tipoServicio"   >                       
                        </div>
                        <div class="col-xl-6">
                            <label for="contrato">Giro</label>
                            <input class="form-control {{ $errors->has('giro') ? ' is-invalid' : '' }}" type="text" name="giro"   >                       
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            {{-- <label for="contrato">Tiene Agua</label> --}}
                            {{-- <input class="form-control {{ $errors->has('agua') ? ' is-invalid' : '' }}" type="text" name="agua" id="agua"  > --}}
                            <div class="form-group">
                                <span class="switch">
                                    <input type="checkbox" value='true' class="switch" name="agua" id="switch-agua2">
                                    <label for="switch-agua2">¿Tiene Agua?</label>
                                </span>
                            </div>                       
                        </div>
                        <div class="col-xl-6">
                            {{-- <label for="contrato">Tiene Drenaje</label> --}}
                            <div class="form-group">
                                <span class="switch">
                                    <input type="checkbox" value='true' class="switch" name="drenaje" id="switch-drenaje2">
                                    <label for="switch-drenaje2">¿Tiene Drenaje?</label>
                                </span>
                            </div>
                            {{-- <input class="form-control {{ $errors->has('drenaje') ? ' is-invalid' : '' }}" type="text" name="drenaje"  >                        --}}
                        </div>
                        <div class="col-xl-6">
                            <label for="contrato">Serie del medidor</label>
                            <input class="form-control {{ $errors->has('medidorSerie') ? ' is-invalid' : '' }}" type="text" name="medidorSerie"  >
                        </div>
                    </div>                                                              
                </div>
                

                <div class="modal-footer">
                    @can("write_oromapas_son")
                        <button class="btn btn-primary" type="submit"><i class="fas fa-check mr-2"></i> Guardar</button>
                    @endcan
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                </div>
            </div>
        </form>

    </div>
</div>

<div class="modal fade" id="modal-edit-hidraulica" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST"  class='NewOffice' id="frHidraulica" aria-label="{{ __('Register') }}" enctype="multipart/form-data">
            @csrf            
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hidraulica Detalles</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="layer">Layer</label>
                            <input  class="form-control {{ $errors->has('layer') ? ' is-invalid' : '' }}" type="text" name="layer" id="layer"  >                       
                        </div>
                        <div class="col-xl-6">
                            <label for="longitud">Longitud</label>
                            <input class="form-control {{ $errors->has('longitud') ? ' is-invalid' : '' }}" type="text" name="longitud" id="longitud"  > 
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="diametro">Diametro</label>
                            <input  class="form-control {{ $errors->has('diametro') ? ' is-invalid' : '' }}" type="text" name="diametro" id="diametro"  >
                        </div>
                        <div class="col-xl-6">
                            <label for="diametro">Pendiente</label>
                            <input  class="form-control {{ $errors->has('pendiente') ? ' is-invalid' : '' }}" type="text" name="pendiente" id="pendiente"  >
                            <input type="hidden" name="id" id="hidraulica">                       
                        </div>
                    </div>                                                              
                </div>
                <div class="modal-footer">
                    @can("write_oromapas_son")
                        <button class="btn btn-primary" type="submit"><i class="fas fa-check mr-2"></i> Guardar</button>
                    @endcan
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                </div>
            </div>
        </form>

    </div>
</div>

<div class="modal fade" id="modal-edit-sanitaria" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST"  class='NewOffice' id="frSanitaria" aria-label="{{ __('Register') }}" enctype="multipart/form-data">
            @csrf            
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Sanitaria Detalles</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="layer">Layer</label>
                            <input  class="form-control {{ $errors->has('layer') ? ' is-invalid' : '' }}" type="text" name="layer" id="layer_s"  >                       
                        </div>
                        <div class="col-xl-6">
                            <label for="longitud">Longitud</label>
                            <input class="form-control {{ $errors->has('longitud') ? ' is-invalid' : '' }}" type="text" name="longitud" id="longitud_s"  > 
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="diametro">Diametro</label>
                            <input  class="form-control {{ $errors->has('diametro') ? ' is-invalid' : '' }}" type="text" name="diametro" id="diametro_s"  >
                        </div>
                        <div class="col-xl-6">
                            <label for="diametro">Pendiente</label>
                            <input  class="form-control {{ $errors->has('pendiente') ? ' is-invalid' : '' }}" type="text" name="pendiente" id="pendiente_s"  >
                            <input type="hidden" name="id" id="sanitaria">                       
                        </div>
                    </div>                                                              
                </div>
                <div class="modal-footer">
                    @can("write_oromapas_son")
                        <button class="btn btn-primary" type="submit"><i class="fas fa-check mr-2"></i> Guardar</button>
                    @endcan
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                </div>
            </div>
        </form>

    </div>
</div>
<div class="modal fade" id="modal-edit-pozo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form method="POST"  class='NewOffice' id="frPozo" aria-label="{{ __('Register') }}" enctype="multipart/form-data">
            @csrf            
            <div class="modal-content" id="detalles">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Pozo Detalles</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="layer_p">Layer</label>
                            <input  class="form-control {{ $errors->has('layer') ? ' is-invalid' : '' }}" type="text" name="layer" id="layer_p"  >                       
                        </div>
                        <div class="col-xl-6">
                            <label for="longitud">Nivel Raz</label>
                            <input class="form-control {{ $errors->has('longitud') ? ' is-invalid' : '' }}" type="text" name="nivel_raz" id="nivel_raz"  > 
                        </div>
                    </div>                                                              
                    <div class="row mb-2">
                        <div class="col-xl-6">
                            <label for="diametro">Nivel Arras</label>
                            <input  class="form-control {{ $errors->has('diametro') ? ' is-invalid' : '' }}" type="text" name="nivel_arras" id="nivel_arras"  >
                        </div>
                        <div class="col-xl-6">
                            <label for="diametro">Diferencia</label>
                            <input  class="form-control {{ $errors->has('pendiente') ? ' is-invalid' : '' }}" type="text" name="diferencia" id="diferencia"  >
                            <input type="hidden" name="id" id="pozo">                       
                        </div>
                    </div>                                                              
                </div>
                <div class="modal-footer">
                    @can("write_oromapas_son")
                        <button class="btn btn-primary" type="submit"><i class="fas fa-check mr-2"></i> Guardar</button>
                    @endcan
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                </div>
            </div>
        </form>

    </div>
</div>