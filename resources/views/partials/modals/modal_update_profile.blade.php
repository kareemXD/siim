{{-- Modal Logout --}}
<div class="modal fade" id="modal-update-profile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form method="POST" action="{{ route('update.profile.client') }}" aria-label="{{ __('Register') }}">
            @csrf
            <input type="hidden" name="operation" value="profile">
            <input type="hidden" name="id" value="{{ Auth::user()->id }}">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Actualizar Pefil</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">Nombre</label>
                        
                        <div class="col-md-6">
                            <input id="name" disabled value="{{ Auth::user()->name }}" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
        
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
        
                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">Correo Electronico</label>
        
                        <div class="col-md-6">
                            <input id="email" value="{{ Auth::user()->email }}" disabled type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">
        
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
        
                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">Contraseña</label>
        
                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
        
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
        
                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">Confirmar Contraseña</label>
        
                        <div class="col-md-6">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="fas fa-times-circle mr-2"></i> Cerrar</button>
                    <button type="submit" class="btn btn-primary"><i class="fas fa-user-edit mr-2"></i> Actualizar</button>
                </div>
            </div>
        </form>

    </div>
</div>
<script>
    $(function(){
        var operation = "{{ old('operation') }}";
        if(operation == "profile")
        {
            $("#modal-update-profile").modal("show");
        }
    });
</script>