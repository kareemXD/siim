@php setlocale(LC_TIME, 'es'); @endphp

<style>
    .red{
        background-color: #f44336;
        color: #fff;
    }
    .blue{
        background-color: #283593;
        color: #fff;
    }
    .green{
        background-color: #bbdefb;
        color: #000;
    }
    .col-5 {
        width: 50%;
    }
    .col-3 {
        width: 30%;
    }
    .col-9 {
        width: 90%;
    }
    .col-8 {
        width: 80%;
    }
    .col-1 {
        width: 10%;
    }
    .col-2 {
        width: 20%;
    }
    .col-05 {
        width: 5%;
    }
    .col-03 {
        width: 3%;
    }
    .col-06 {
        width: 6%;
    }
    .col-07 {
        width: 7%;
    }
    .col-08 {
        width: 8%;
    }
    .col-09 {
        width: 9%;
    }
    .col-25 {
        width: 25%;
    }
    .col-16 {
        width: 16%;
    }
    .col-11 {
        width: 11%;
    }
    .col-15 {
        width: 15%;
    }
    .text-center{
        text-align:center;
    }
    .black {
        font-weight:bold;
    }
    .span {
        font-size:8px;
    }
    .fo-10 {
        font-size:10px;
    }

    td {
        font-size: 8.5px;
    }
    .tb-small td {
        font-size: 7px;
    }
    td {
        font-size: 8.5px;
    }
    .bg-red {
        background-color: #ef5350;
        color: #fff;
    }
    .bg-green{ 
        background-color: #66bb6a;
        color: #fff;
    }
    .bg-yellow{ 
        background-color: #fbc02d;
        color: #fff;
    }
</style>

<table CELLPADDING="3" style="border: 1px solid #2196f3; " border="1" >
    @if ($direction->hasDad())
        <tr>
            <td class="fo-10">
                <span class="black">Dirección: </span>
                {{ $direction->dad->nombre." (".$direction->nombre.") " }}
            </td>
        </tr>
    @else
        <tr>
            <td class="fo-10">
                <span class="black">Dirección: </span>
                {{ $direction->nombre }}
            </td>
        </tr>
    @endif
    <?php
        if(!empty($direction->director))
        {
            $director = $direction->director->profile()->orderBy('id', 'DESC')->first();
        }
    ?>
    @if (!empty($director))
        <tr>
            <td class="fo-10">
                <span class="black">Director: </span>
                {{ $director->nombre." ".$director->apellido_pat." ".$director->apellido_mat }}
            </td>
        </tr>
    @endif
</table>
<table style="border: 1px solid #2196f3; " border="1" >
    <tr>
        <td >
            <span class="span">PDM = Plan de Desarrollo Municipal, GDM = Guía para el Desarrollo Municipal, A2030 = Agenda 2030</span>
        </td>
    </tr>
</table><br><br>
@php
    $program = 0;
    $oldprogram = [];
    $area = 0;
    $oldarea = [];
@endphp
@foreach($data as $mir_area)
@php
    $program = $mir_area->program->id;
    $area = $mir_area->id;
@endphp
@if (!in_array($program, $oldprogram))
<table CELLPADDING="3" style="border: 1px solid #2196f3;" border="1" >
    <tr>
        <td class="black" style="text-align:justify; background-color:#0d47a1; color:#fff;">
            Programa Presupuestario: {{ $mir_area->program->clave.". ".$mir_area->program->nombre }}
        </td>
    </tr>
    <!-- <tr>
        <td class="black" style="text-align:justify; background-color:#1565c0; color:#fff;">
            Objetivo del Programa: {{ $mir_area->program->objetivo }}
        </td>
    </tr> -->
    <tr>
        <td class="black" style="text-align:justify; background-color:#1976d2 ; color:#fff;">
            Presupuesto asignado al programa: $ {{ number_format($mir_area->program->presupuesto, 2) }}
        </td>
    </tr>
</table> <br><br>
<table CELLPADDING="5" style="border: 1px solid #2196f3;" border="1" >
    <tr>
        <td style="width: 10%;" class="black">FIN</td>
        <td style="width: 90%;"><b></b> {{ $mir_area->program->objetivo_fin }}</td>
    </tr>
    <tr>
        <td style="width: 10%;" class="black">PROPÓSITO</td>
        <td style="width: 90%;"><b></b> {{ $mir_area->program->objetivo_prop }}</td>
    </tr>
    
</table> <br><br>

@endif
@php
    $oldprogram[] = $mir_area->program->id;
    $oldarea[] = $mir_area->id;
@endphp
@endforeach
@php
    $program = 0;
    $oldprogram = 0;
    $area = 0;
    $oldarea = 0;
@endphp
@foreach ($data as $mir_area)

<table CELLPADDING="5" style="border: 1px solid; " border="1" >
    <tr style="text-align:justify; background-color:#1976d2; color:#fff;">
        <td style="width: 10%;" class="black">ÁREA</td>
        <td style="width: 90%;"><b>{{ $mir_area->nombre }}</b></td>
    </tr>
</table><br><br>
@foreach ($mir_area->components as $index=> $componet )
<table class="tb-small" CELLPADDING="2" style="border: 1px solid #2196f3;" border="1" >
    @if ($area != $oldarea  || $index ==0)
        <tr style="background-color:#4f81bd ; color:#fff;">
            <td rowspan="2" class="text-center black" style="width: 10%;">Estrategias</td>
            <td rowspan="2" class="text-center black" style="width: 10%;">Lineas de Acción</td>
            <td rowspan="2" class="text-center black" style="width: 9%;">Nivel</td>
            <td rowspan="2" class="text-center black" style="width: 14%;">Nombre del Indicador</td>
            <td rowspan="2" class="text-center black" style="width: 14%;">Formula</td>
            <td rowspan="2" class="text-center black" style="width: 5%;">Dimensión</td>
            <td class="text-center black" style="width: 15%;">Reporte Trimestral</td>
            <td class="text-center black" style="width: 10%;">Meta Anual Acumulada</td>
            <td rowspan="2" class="text-center black" style="width: 6%;">Unidad de Medida Absoluta</td>

            <td rowspan="2" class="text-center black" style="width: 7%;">Responsable</td>
        </tr>
        <tr style="background-color:#4f81bd ; color:#fff;">
            <td class="text-center black" style="width: 5%;">Valor Alcanzado</td>
            <td class="text-center black" style="width: 5%;">Valor Programado</td>
            <td class="text-center black" style="width: 5%;">Porcentaje Alcanzado</td>
            <td class="text-center black" style="width: 5%;">Meta Absoluta Programada</td>
            <td class="text-center black" style="width: 5%;">Meta anual alcanzada</td>
        </tr>
    @endif
    
    <tr>
        <td style="width: 10%;">
            @if (count($componet->pm_action_lines) > 0)
                @foreach ($componet->pm_action_lines as $actionline)
                    PMD. {{ $actionline->strategy->punto }} <br>
                @endforeach
            @endif
            @if (count($componet->inf_action_lines) > 0)
                @foreach ($componet->inf_action_lines as $actionline)
                    GDM. {{ $actionline->topic->punto }} <br>
                @endforeach
            @endif
            @if (count($componet->sche_metas) > 0)
                @foreach ($componet->sche_metas as $actionline)
                    A2030. {{ $actionline->objective->punto }} <br>
                @endforeach
            @endif
        </td>
        <td style="width: 10%;">
            @if (count($componet->pm_action_lines) > 0)
                @foreach ($componet->pm_action_lines as $actionline)
                    {{ $actionline->punto }} <br>
                @endforeach
            @endif
            @if (count($componet->inf_action_lines) > 0)
                @foreach ($componet->inf_action_lines as $actionline)
                    {{ $actionline->punto }} <br>
                @endforeach
            @endif
            @if (count($componet->sche_metas) > 0)
                @foreach ($componet->sche_metas as $actionline)
                    {{ $actionline->punto }} <br>
                @endforeach
            @endif
        </td>
        <td style="width: 9%;"><b>COMPONENTE</b></td>
        <td style="width: 14%;">{{ $componet->actividad }}</td>
        <td style="width: 14%;">{{ $componet->formula }}</td>
        <td style="width: 5%;">{{ $componet->dimension }}</td>
        {{-- <td>{{ $componet->periodo_verificacion }}</td> --}}
        <td style="width: 5%;">{{ $componet->trimestre_1_r }}</td>
        <td style="width: 5%;">{{ $componet->trimestre_1 }}</td>
        <td style="width: 5%;" ></td>
        <td style="width: 5%;">{{ $componet->meta_anual }}</td>
        <td style="width: 5%;"></td>
        <td style="width: 6%;">{{ $componet->unidad_medida }}</td>
        <td style="width: 7%;">
            @if (!empty($componet->puesto_id))
                {{ $componet->responsable->nombre }}
            @else
                Sin Responsable
            @endif
        </td>
    </tr>
    
    
    @if (count($componet->actions) > 0)
        @foreach ($componet->actions as $key => $action)
            <tr>
                <td style="width: 10%;">
                    @if (count($action->pm_action_lines) > 0)
                        @foreach ($action->pm_action_lines as $actionline)
                            PMD. {{ $actionline->strategy->punto }} <br>
                        @endforeach
                    @endif
                    @if (count($action->inf_action_lines) > 0)
                        @foreach ($action->inf_action_lines as $actionline)
                            GDM. {{ $actionline->topic->punto }} <br>
                        @endforeach
                    @endif
                    @if (count($action->sche_metas) > 0)
                        @foreach ($action->sche_metas as $actionline)
                            A2030. {{ $actionline->objective->punto }} <br>
                        @endforeach
                    @endif
                </td>
                <td style="width: 10%;">
                    @if (count($action->pm_action_lines) > 0)
                        @foreach ($action->pm_action_lines as $actionline)
                            {{ $actionline->punto }} <br>
                        @endforeach
                    @endif
                    @if (count($action->inf_action_lines) > 0)
                        @foreach ($action->inf_action_lines as $actionline)
                            {{ $actionline->punto }} <br>
                        @endforeach
                    @endif
                    @if (count($action->sche_metas) > 0)
                        @foreach ($action->sche_metas as $actionline)
                            {{ $actionline->punto }} <br>
                        @endforeach
                    @endif
                </td>
                <td style="width: 9%;"><b>ACTIVIDAD</b></td>
                <td style="width: 14%;">{{ $action->actividad }}</td>
                <td style="width: 14%;">{{ $action->formula }}</td>
                <td style="width: 5%;">{{ $action->dimension }}</td>
                <td style="width: 5%;">{{ $action->trimestre_1_r }}</td>
                <td style="width: 5%;">{{ $action->trimestre_1 }}</td>
                <td style="width: 5%;" class="{{ $action->getAdvanceColorTrimestre() }}">{{ $action->getAdvanceTrimestre() }}%</td>
                <td style="width: 5%;">{{ $action->meta_anual }}</td>
                <td style="width: 5%;" class="{{ $action->getAdvanceColor() }}">{{ $action->getAdvance() }}%</td>
                <td style="width: 6%;">{{ $action->unidad_medida }}</td>
                <td style="width: 7%;">
                    @if (!empty($action->puesto_id))
                        {{ $action->responsable->nombre }}
                    @else
                        Sin Responsable
                    @endif
                </td>
            </tr>
        @endforeach
    @else
        <tr>
            <td style="text-align:center;" class="red" colspan="10">
                Sin Actividades
            </td>
        </tr>
    @endif
</table> <br><br>
@php
    $oldprogram = $mir_area->program->id;
    $oldarea = $mir_area->id;
@endphp
@endforeach
@endforeach
@if (!empty($director))
<br><br><br><br><br><br>
<table>
    <tr>
        <td style="text-align:center; font-weight:bold;">
            <span class="span">________________________________________________________________</span>
        </td>
    </tr>
    <tr>
        <td style="text-align:center; font-weight:bold; font-size:1.2em;">
            <span>{{ $director->nombre." ".$director->apellido_pat." ".$director->apellido_mat }}</span>
        </td>
    </tr>
    <tr>
            <td style="text-align:center; font-weight:bold; font-size:1em;">
            <span>{{ $director->position->descripcion }}</span>
        </td>
    </tr>
</table><br><br>
@endif
