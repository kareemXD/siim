@php setlocale(LC_TIME, 'es'); @endphp

<style>

</style> 

<table border="1px" cellpadding="3">
    <tbody>
        <tr>
            <td style="width:10%;">#{{ $count }}</td>
            <td style="width:30%;">Folio: {{ $data->folio }}</td>
            <td style="width:30%;">Origen: {{ $data->origen }}</td>
            <td style="width:30%;">¿Procede?: {{ ($data->procede == "Sin Aporte") ? "Comentario" : $data->procede }}</td>
            {{-- <td style="width:30%;">
                ¿Procede?: 
                @switch($data->procede)
                    @case("Sin Aporte")
                        Comentario
                        @break
                    @case("Procede")
                    @case("Procede Parcialmente")
                    @case("No Procede")
                        En Estudio
                        @break
                @endswitch
            </td> --}}
        </tr>
        <tr>
            <td colspan="2">Nombre: {{ $data->nombre." ".$data->apellidos }}</td>
            <td>Correo: {{ $data->correo }}</td>
            <td>Telefono: {{ $data->telefono }}</td>
        </tr>
        <tr>
            <td colspan="4">{{ $data->observaciones }}</td>
        </tr>
    </tbody>
</table>

