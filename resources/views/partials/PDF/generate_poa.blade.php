@php setlocale(LC_TIME, 'es'); @endphp

<style>
    .red{
        background-color: #f44336;
        color: #fff;
    }
    .blue{
        background-color: #13629D;
        color: #fff;
    }
    .col-5 {
        width: 50%;
    }
    .col-1 {
        width: 10%;
    }
</style>

{{-- Genera Lineas de Accion Plan Municipal --}}
@php
$old_normative = "";
$old_eje = "";
$old_objetivo = "";
$old_estrategia = "";
$old_linea = "";
@endphp
@foreach ($data["pm"] as $actionLine)
    @php
        $normative = $actionLine->strategy->objective->axe->normative->id;
        $eje = $actionLine->strategy->objective->axe->punto;
        $objetivo = $actionLine->strategy->objective->punto;
        $estrategia = $actionLine->strategy->punto;
        $linea = $actionLine->punto;
    @endphp
    @if ($normative != $old_normative)
    <p><b>Normativa: </b> {{ $actionLine->strategy->objective->axe->normative->nombre }}</p>
    @endif
    @if ($eje != $old_eje)
    <p><b>Eje: </b> {{ $actionLine->strategy->objective->axe->punto }}. {{ $actionLine->strategy->objective->axe->nombre }}</p>
    @endif
    @if ($objetivo != $old_objetivo)
    <p><b>Objetivo: </b> {{ $actionLine->strategy->objective->punto }}. {{ $actionLine->strategy->objective->nombre }}</p>
    @endif
    @if ($estrategia != $old_estrategia)
    <p><b>Estrategia: </b> {{ $actionLine->strategy->punto }}. {{ $actionLine->strategy->nombre }}</p>
    @endif
    @if ($linea != $old_linea)
    <p><b>Linea de Acción: </b> {{ $actionLine->punto }}. {{ $actionLine->nombre }}</p>
    @endif
    <table style="border: 1px solid #2196f3; " border="1" >
        @if (count($actionLine->actions()->where("direccion_id", Auth::user()->profile->position->direction->id)->get()) > 0)
            <tr>
                <td class="col-5">Actividad</td>
                <td class="col-1">Meta Anual</td>
                <td class="col-1">1° Trimestre</td>
                <td class="col-1">2° Trimestre</td>
                <td class="col-1">3° Trimestre</td>
                <td class="col-1">4° Trimestre</td>
            </tr>
            @foreach ($actionLine->actions()->where("direccion_id", Auth::user()->profile->position->direction->id)->get() as $key1 => $action)
                <tr class="blue">
                    @php
                    $i = $key1 + 1
                    @endphp 
                        
                    <td>{{ $i.".- ".$action->actividad }}</td>
                    <td>{{ $action->meta_anual }}</td>
                    <td>{{ $action->trimestre_1 }}</td>
                    <td>{{ $action->trimestre_2 }}</td>
                    <td>{{ $action->trimestre_3 }}</td>
                    <td>{{ $action->trimestre_4 }}</td>
                </tr>
                @if(!empty($action->subactions))
                    @foreach ($action->subactions as $key2 => $subaction)
                        @php
                        $a = $key2 + 1
                        @endphp
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $i.".".$a.".- ".$subaction->actividad }}</td>
                            <td>{{ $subaction->meta_anual }}</td>
                            <td>{{ $subaction->trimestre_1 }}</td>
                            <td>{{ $subaction->trimestre_2 }}</td>
                            <td>{{ $subaction->trimestre_3 }}</td>
                            <td>{{ $subaction->trimestre_4 }}</td>
                        </tr>
                    @endforeach
                @endif
            @endforeach
        @else
            <tr class="red">
                <td colspan="6" style="text-align:center;">Ningua Actividad Asignada</td>
            </tr>
        @endif
    </table>
    @php
        $old_normative = $actionLine->strategy->objective->axe->normative->id;
        $old_eje = $actionLine->strategy->objective->axe->punto;
        $old_objetivo = $actionLine->strategy->objective->punto;
        $old_estrategia = $actionLine->strategy->punto;
        $old_linea = $actionLine->punto;
    @endphp
@endforeach