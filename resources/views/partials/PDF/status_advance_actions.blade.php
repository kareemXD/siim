@php setlocale(LC_TIME, 'es'); @endphp

<style>
    .red{
        background-color: #f44336;
        color: #fff;
    }
    .yellow{
        background-color: #ffcc80;
        color: #000;
    }
    .green{
        background-color: #43a047;
        color: #000;
    }
    .col-5 {
        width: 50%;
    }
    .col-3 {
        width: 30%;
    }
    .col-9 {
        width: 90%;
    }
    .col-8 {
        width: 80%;
    }
    .col-1 {
        width: 10%;
    }
    .col-2 {
        width: 20%;
    }
    .col-05 {
        width: 5%;
    }
    .col-03 {
        width: 3%;
    }
    .col-06 {
        width: 6%;
    }
    .col-25 {
        width: 25%;
    }
    .col-16 {
        width: 16%;
    }
    .col-11 {
        width: 11%;
    }
    .col-15 {
        width: 15%;
    }
    .text-center{
        text-align:center;
    }
    .black {
        font-weight:bold;
    }
    .span {
        font-size:8px;
    }

</style> 
<table style="border: 1px solid #2196f3;" border="1" >
    <tr>
        <td class="col-10" style="text-align:center;">
            <h4>{{ $data_line["nombre"] }}</h4>
        </td>
    </tr>
</table>
<?php
    $count = 0;
    $finished = 0;
    $process = 0;
    $null = 0;
?>
<table style="border: 1px solid #2196f3;" border="1" >
    <thead>
    @if ($data_line["alias"] == "mir")
        @foreach ($data_line["lines"] as $key => $actionline)
            @if($actionline->program->periodo == config('implan.periods.previous'))
                <?php
                    $total_actions = 0;

                    foreach ($actionline->actions as $key => $action_line) {
                        if($action_line->program->period == config('implan.periods.previous'))
                        {
                            $total_actions++;
                        }
                    }
                ?>
                @if($total_actions > 0)
                    @if ($count == 0)
                        <tr>
                    @endif
                    <?php 
                        $fraction = 1 / $total_actions; 

                        foreach ($actionline->actions as $action) {
                            if($action->meta_anual > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                $advance = (($action->meta_anual_r * 100 ) / $action->meta_anual);
                                if($advance >= 100)
                                {
                                    $finished++;
                                }
                            }
                        }
                        $percetage = $fraction * $finished;
                    ?>
                    @if ($percetage >= 1)
                        <td class="green black col-1" style="text-align:center;">
                            <span>{{ $actionline->program->punto }}-{{ $actionline->punto }}</span>
                        </td>
                    @elseif($percetage > 0 && $percetage < 1)
                        <td class="yellow black col-1" style="text-align:center;">
                            <span>{{ $actionline->program->punto }}-{{ $actionline->punto }}</span>
                        </td>
                    @else
                        <td class="red black col-1" style="text-align:center;">
                            <span>{{ $actionline->program->punto }}-{{ $actionline->punto }}</span>
                        </td>
                    @endif
                    <?php
                        $count++;
                        $finished = 0;
                    ?>
                    @if($count == 10)
                        </tr>
                        <?php
                            // dd($actionline->actions, $total_actions, $fraction, $finished, $count);
                            $count = 0;
                        ?>
                    @endif 
                @endif
            @endif
        @endforeach
        @if($count < 10 && $count > 0)
            </tr>
        @endif
    @else
        @foreach ($data_line["lines"] as $key => $actionline)
            <?php
                $total_actions = 0;
                foreach ($actionline->actions as $key => $action_line) {
                    if($action_line->program->period == config('implan.periods.previous'))
                    {
                        $total_actions++;
                    }
                }
                
            ?>
            @if($total_actions > 0)
                @if ($count == 0)
                    <tr>
                @endif
                <?php 
                    $fraction = 1 / $total_actions; 
                    foreach ($actionline->actions as $action) {
                        if($action->meta_anual > 0 && $action->program->period == config('implan.periods.previous'))
                        {
                            
                            $advance = (($action->meta_anual_r * 100 ) / $action->meta_anual);
                            if($advance >= 100)
                            {
                                $finished++;
                            }
                        }
                    }
                    $percetage = $fraction * $finished;
                ?>
                @if ($percetage >= 1)
                    <td class="green black col-1" style="text-align:center;">
                        <span>{{ $actionline->punto }}</span>
                    </td>
                @elseif($percetage > 0 && $percetage < 1)
                    <td class="yellow black col-1" style="text-align:center;">
                        <span>{{ $actionline->punto }}</span>
                    </td>
                @else
                    <td class="red black col-1" style="text-align:center;">
                        <span>{{ $actionline->punto }}</span>
                    </td>
                @endif
                <?php
                    $count++;
                    $finished = 0;
                ?>
                @if($count == 10)
                    </tr>
                    <?php
                        // dd($actionline->actions, $total_actions, $fraction, $finished, $count);
                        $count = 0;
                    ?>
                @endif 
            @endif
        @endforeach
        @if($count < 10 && $count > 0)
            </tr>
        @endif
    @endif
</thead>
</table>

