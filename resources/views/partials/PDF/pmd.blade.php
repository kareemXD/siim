@php setlocale(LC_TIME, 'es'); @endphp

<style>
    .red{
        background-color: #f44336;
        color: #fff;
    }
    .yellow{
        background-color: #ffcc80;
        color: #000;
    }
    .green{
        background-color: #43a047;
        color: #000;
    }
    .gray{
        background-color: #9e9e9e;
        color: #000;
    }
    .blue{
        background-color: #13629D;
        color: #000;
    }
    .col-09 {
        width: 9%;
        text-align: center;
    }
    .col-27 {
        width: 27%;
        text-align: center;
    }
    .col-06 {
        width: 6%;
        text-align: center;
    }
    .rotate {
        text-align: center;
    }
    .black {
        font-weight:bold;
        text-align: center;
    }
</style> 

<h4 class="black">100C = 100% Concluidas, NC = No Concluidas, NA = No Atendidas, Tot. = Total</h4>
<table border="1px">
    <thead>
        <tr>
            <th class="col-27">Eje</th>
            <th class="col-09">Total de LA del PMD</th>
            <th class="col-09"># de LA sin Trabajos</th>
            <th class="col-09">%</th>
            <th class="col-09">Atendidas al 100%</th>
            <th class="col-06">%</th>
            <th class="col-09">No logro el 100%</th>
            <th class="col-06">%</th>
            <th class="col-09">No Atendidas</th>
            <th class="col-06">%</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($stadistics as $key => $data)
            <tr>
                <td class="col-27">{{ $data["name"] }}</td>
                <td class="col-09">{{ $data["la_total"] }}</td>
                <td class="col-09">{{ $data["la_not_worked"] }}</td>
                <td class="col-09">{{ $data["la_not_worked_per"] }}</td>
                <td class="col-09">{{ $data["la_finished"] }}</td>
                <td class="col-06">{{ $data["la_finished_per"] }}</td>
                <td class="col-09">{{ $data["la_process"] }}</td>
                <td class="col-06">{{ $data["la_process_per"] }}</td>
                <td class="col-09">{{ $data["la_null"] }}</td>
                <td class="col-06">{{ $data["la_null_per"] }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

