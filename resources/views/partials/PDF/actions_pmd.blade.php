@php setlocale(LC_TIME, 'es'); @endphp

<style>
    .red{
        background-color: #f44336;
        color: #fff;
    }
    .yellow{
        background-color: #ffcc80;
        color: #000;
    }
    .green{
        background-color: #43a047;
        color: #000;
    }
    .gray{
        background-color: #9e9e9e;
        color: #000;
    }
    .blue{
        background-color: #13629D;
        color: #000;
    }
    .col-035 {
        width: 2.8%;
        text-align: center;
    }
    .col-16 {
        width: 16%;
        text-align: center;
    }
    .col-14 {
        width: 14%;
        text-align: center;
    }
    .rotate {
        text-align: center;
    }
    .black {
        font-weight:bold;
        text-align: center;
    }
</style> 

<h4 class="black">100C = 100% Concluidas, NC = No Concluidas, NA = No Atendidas, Tot. = Total</h4>
<table border="1px">
    <thead>
        <tr>
            <th rowspan="2" class="col-16">Direccion</th>
            @foreach ($stadistics["axes"] as $axe)
                <th class="col-14" scope="col" colspan="5">{{ $axe }}</th>
            @endforeach
        </tr>
        <tr>
            @foreach ($stadistics["axes"] as $axe)
                <th class="rotate green"><span>100C</span></th>
                <th class="rotate yellow"><span>NC</span></th>
                <th class="rotate red"><span>NA</span></th>
                <th class="rotate gray"><span>NP</span></th>
                <th class="rotate blue"><span>Tot.</span></th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @php
            unset($stadistics["axes"]);   
        @endphp
        @foreach ($stadistics as $data)
            <tr>
                <td class="col-16">{{ $data["name"] }}</td>
                @foreach ($data["axes"] as $axe)
                    <td class="col-035">{{ $axe["finish"] }}</td>
                    <td class="col-035">{{ $axe["process"] }}</td>
                    <td class="col-035">{{ $axe["null"] }}</td>
                    <td class="col-035">{{ $axe["empty"] }}</td>
                    <td class="col-035">{{ $axe["total"] }}</td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>

