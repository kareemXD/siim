@php setlocale(LC_TIME, 'es'); @endphp

<style>
    .red{
        background-color: #f44336;
        color: #fff;
    }
    .yellow{
        background-color: #ffcc80;
        color: #000;
    }
    .green{
        background-color: #43a047;
        color: #000;
    }
    .col-5 {
        width: 50%;
    }
    .col-3 {
        width: 30%;
    }
    .col-9 {
        width: 90%;
    }
    .col-8 {
        width: 80%;
    }
    .col-1 {
        width: 10%;
    }
    .col-2 {
        width: 20%;
    }
    .col-05 {
        width: 5%;
    }
    .col-03 {
        width: 3%;
    }
    .col-06 {
        width: 6%;
    }
    .col-25 {
        width: 25%;
    }
    .col-16 {
        width: 16%;
    }
    .col-11 {
        width: 11%;
    }
    .col-15 {
        width: 15%;
    }
    .text-center{
        text-align:center;
    }
    .black {
        font-weight:bold;
    }
    .span {
        font-size:8px;
    }

</style> 
<table style="border: 1px solid #2196f3;" border="1" >
    <tr>
        <td class="col-10" style="text-align:center;">
            <h4>{{ $data_line["nombre"] }}</h4>
        </td>
    </tr>
</table>
<?php
    $count = 0;
?>
<table style="border: 1px solid #2196f3;" border="1" >
    <thead>
        @if ($data_line["alias"] == "mir")
            @foreach ($data_line["lines"] as $key => $actionline)
                @if($actionline->program->periodo == config('implan.periods.previous'))
                    @if ($count == 0)
                        <tr>
                    @endif

                    @if (count($actionline->actions) > 0)
                        <td class="green black col-1" style="text-align:center;">
                            <span>{{ $actionline->program->punto }}-{{ $actionline->punto }}</span>
                        </td>
                    @elseif(count($actionline->directions) > 0)
                        <td class="yellow black col-1" style="text-align:center;">
                            <span>{{ $actionline->program->punto }}-{{ $actionline->punto }}</span>
                        </td>
                    @else
                        <td class="red black col-1" style="text-align:center;">
                            <span>{{ $actionline->program->punto }}-{{ $actionline->punto }}</span>
                        </td>
                    @endif
                    
                    <?php
                        $count++;
                    ?>
                    @if($count == 10)
                        </tr>
                        <?php
                            $count = 0;
                        ?>
                    @endif
                @endif
            @endforeach
            @if($count < 10 && $count > 0)
                </tr>
            @endif
        @else
            @foreach ($data_line["lines"] as $key => $actionline)
                @if ($count == 0)
                    <tr>
                @endif

                @if (count($actionline->actions) > 0)
                    <td class="green black col-1" style="text-align:center;">
                        <span>{{ $actionline->punto }}</span>
                    </td>
                @elseif(count($actionline->directions) > 0)
                    <td class="yellow black col-1" style="text-align:center;">
                        <span>{{ $actionline->punto }}</span>
                    </td>
                @else
                    <td class="red black col-1" style="text-align:center;">
                        <span>{{ $actionline->punto }}</span>
                    </td>
                @endif
                <?php
                    $count++;
                ?>
                @if($count == 10)
                    </tr>
                    <?php
                        $count = 0;
                    ?>
                @endif
            @endforeach
            @if($count < 10 && $count > 0)
                </tr>
            @endif
        @endif
    </thead>
</table>

