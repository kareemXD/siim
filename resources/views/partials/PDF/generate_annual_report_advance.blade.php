@php setlocale(LC_TIME, 'es'); @endphp

<style>
    .red{
        background-color: #f44336;
        color: #fff;
    }
    .blue{
        background-color: #283593;
        color: #fff;
    }
    .green{
        background-color: #bbdefb;
        color: #000;
    }
    .col-5 {
        width: 50%;
    }
    .col-3 {
        width: 30%;
    }
    .col-9 {
        width: 90%;
    }
    .col-8 {
        width: 80%;
    }
    .col-1 {
        width: 10%;
    }
    .col-2 {
        width: 20%;
    }
    .col-05 {
        width: 5%;
    }
    .col-03 {
        width: 3%;
    }
    .col-06 {
        width: 6%;
    }
    .col-07 {
        width: 7%;
    }
    .col-08 {
        width: 8%;
    }
    .col-09 {
        width: 9%;
    }
    .col-25 {
        width: 25%;
    }
    .col-16 {
        width: 16%;
    }
    .col-11 {
        width: 11%;
    }
    .col-15 {
        width: 15%;
    }
    .col-25 {
        width: 25%;
    }
    .text-center{
        text-align:center;
    }
    .black {
        font-weight:bold;
    }
    .span {
        font-size:8px;
    }
    .fo-10 {
        font-size:10px;
    }

    td {
        text-align: center;
        font-size: 10px;
    }
    
    .h-30{
        height: 30;
        vertical-align: middle;
    }

    .bg-red {
        background-color: #ef5350;
        color: #fff;
    }
    .bg-green{ 
        background-color: #66bb6a;
        color: #fff;
    }
    .bg-yellow{ 
        background-color: #fbc02d;
        color: #fff;
    }
</style>

@foreach ($data as $direction)
<table style="border: 1px solid #2196f3;" border="1" >
    <tr>
        <td colspan="12" class="blue black" style="text-align:center;">
            {{ $direction["name"] }}
        </td>
    </tr>
    <tr>
        @foreach ($direction["meses"] as $mes)
            <td valign="middle" class="{{ $mes['col'] }} h-30">{{ $mes["name"] }}</td>
        @endforeach
    </tr>
</table> <br><br><br>
@endforeach
