@php setlocale(LC_TIME, 'es'); @endphp

<style>
    .red{
        background-color: #f44336;
        color: #fff;
    }
    .blue{
        background-color: #283593;
        color: #fff;
    }
    .green{
        background-color: #bbdefb;
        color: #000;
    }
    .col-5 {
        width: 50%;
    }
    .col-3 {
        width: 30%;
    }
    .col-9 {
        width: 90%;
    }
    .col-8 {
        width: 80%;
    }
    .col-1 {
        width: 10%;
    }
    .col-2 {
        width: 20%;
    }
    .col-05 {
        width: 5%;
    }
    .col-03 {
        width: 3%;
    }
    .col-06 {
        width: 6%;
    }
    .col-07 {
        width: 7%;
    }
    .col-08 {
        width: 8%;
    }
    .col-09 {
        width: 9%;
    }
    .col-25 {
        width: 25%;
    }
    .col-16 {
        width: 16%;
    }
    .col-11 {
        width: 11%;
    }
    .col-15 {
        width: 15%;
    }
    .col-25 {
        width: 25%;
    }
    .text-center{
        text-align:center;
    }
    .black {
        font-weight:bold;
    }
    .span {
        font-size:8px;
    }
    .fo-10 {
        font-size:10px;
    }

    td {
        font-size: 8.5px;
    }
    .bg-red {
        background-color: #ef5350;
        color: #fff;
    }
    .bg-green{ 
        background-color: #66bb6a;
        color: #fff;
    }
    .bg-yellow{ 
        background-color: #fbc02d;
        color: #fff;
    }
</style>

<table style="border: 1px solid #2196f3; " border="1" >
    <tr>
        <td class="fo-10">
            <span class="black">Dirección: </span>
            {{ $direction->nombre }}
        </td>
    </tr>
    <?php
        if(!empty($direction->director))
        {
            $director = $direction->director->profile()->orderBy('id', 'DESC')->first();
        }
    ?>
    @if (!empty($director))
        <tr>
            <td class="fo-10">
                <span class="black">Director: </span>
                {{ $director->nombre." ".$director->apellido_pat." ".$director->apellido_mat }}
            </td>
        </tr>
    @endif
</table><br><br>

<table style="border: 1px solid #2196f3; " border="1" >
    <tr>
        <td >
            <span class="span">PDM = Plan de Desarrollo Municipal, ADM = Agenda para el Desarrollo Municipal, A2030 = Agenda 2030, P.P = Programa Presupuestario</span>
        </td>
    </tr>
</table><br><br>

@foreach ($data as $program)
<table style="border: 1px solid #2196f3;" border="1" >
    <tr>
        <td colspan="10" class="blue black" style="text-align:center;">
            Área o Proyecto Específico: {{ $program->nombre }}
        </td>
    </tr>
    <tr>
        <td colspan="10" class="green" style="text-align:justify;">
            <span class="black">Objetivo del Área o Proyecto Específico: </span>{{ $program->descripcion }}
        </td>
    </tr>
    <tr>
        <td class="text-center black col-09" rowspan="2">Estrategia</td>
        <td class="text-center black col-06" rowspan="2">Linea</td>
        <td class="text-center black col-25" rowspan="2">Actividad</td>
        <td class="text-center black col-09" rowspan="2">U.M.</td>
        <td class="text-center black col-06" colspan="2">1° Tr</td>
        <td class="text-center black col-06" colspan="2">2° Tr</td>
        <td class="text-center black col-06" colspan="2">3° Tr</td>
        <td class="text-center black col-06" colspan="2">4° Tr</td>
        <td class="text-center black col-06" colspan="2">Meta Anual</td>
        <td class="text-center black col-06" rowspan="2">Avance</td>
        <td class="text-center black col-15" rowspan="2">Responsable</td>
    </tr>
    <tr>
        <td class="text-center black">P</td>
        <td class="text-center black">R</td>
        <td class="text-center black">P</td>
        <td class="text-center black">R</td>
        <td class="text-center black">P</td>
        <td class="text-center black">R</td>
        <td class="text-center black">P</td>
        <td class="text-center black">R</td>
        <td class="text-center black">P</td>
        <td class="text-center black">R</td>
    </tr>
    @if (count($program->actions) > 0)
        @foreach ($program->actions as $action)
            <tr>
                <td >
                    @if (count($action->pm_action_lines) > 0)
                        @foreach ($action->pm_action_lines as $actionline)
                            PMD. {{ $actionline->strategy->punto }} <br>
                        @endforeach
                    @endif
                    @if (count($action->inf_action_lines) > 0)
                        @foreach ($action->inf_action_lines as $actionline)
                            ADM. {{ $actionline->topic->punto }} <br>
                        @endforeach
                    @endif
                    @if (count($action->sche_metas) > 0)
                        @foreach ($action->sche_metas as $actionline)
                            A2030. {{ $actionline->objective->punto }} <br>
                        @endforeach
                    @endif
                    @if (count($action->mir_idicators) > 0)
                        @foreach ($action->mir_idicators as $actionline)
                            {{ $actionline->program->punto }} <br>
                        @endforeach
                    @endif
                </td>
                <td >
                    @if (count($action->pm_action_lines) > 0)
                        @foreach ($action->pm_action_lines as $actionline)
                            {{ $actionline->punto }} <br>
                        @endforeach
                    @endif
                    @if (count($action->inf_action_lines) > 0)
                        @foreach ($action->inf_action_lines as $actionline)
                            {{ $actionline->punto }} <br>
                        @endforeach
                    @endif
                    @if (count($action->sche_metas) > 0)
                        @foreach ($action->sche_metas as $actionline)
                            {{ $actionline->punto }} <br>
                        @endforeach
                    @endif
                    @if (count($action->mir_idicators) > 0)
                        @foreach ($action->mir_idicators as $actionline)
                            {{ $actionline->punto }} <br>
                        @endforeach
                    @endif
                </td>
                <td>{{ $action->actividad }}</td>
                <td>{{ $action->unidad_medida }}</td>
                <td style="text-align:center;">{{ fmod($action->trimestre_1, 1) !== 0.00 ? $action->trimestre_1 : intval($action->trimestre_1) }}</td>
                <td style="text-align:center;">{{ fmod($action->trimestre_1_r, 1) !== 0.00 ? $action->trimestre_1_r : intval($action->trimestre_1_r) }}</td>
                <td style="text-align:center;">{{ fmod($action->trimestre_2, 1) !== 0.00 ? $action->trimestre_2 : intval($action->trimestre_2) }}</td>
                <td style="text-align:center;">{{ fmod($action->trimestre_2_r, 1) !== 0.00 ? $action->trimestre_2_r : intval($action->trimestre_2_r) }}</td>
                <td style="text-align:center;">{{ fmod($action->trimestre_3, 1) !== 0.00 ? $action->trimestre_3 : intval($action->trimestre_3) }}</td>
                <td style="text-align:center;">{{ fmod($action->trimestre_3_p, 1) !== 0.00 ? $action->trimestre_3_p : intval($action->trimestre_3_p) }}</td>
                <td style="text-align:center;">{{ fmod($action->trimestre_4, 1) !== 0.00 ? $action->trimestre_4 : intval($action->trimestre_4) }}</td>
                <td style="text-align:center;">{{ fmod($action->trimestre_4_r, 1) !== 0.00 ? $action->trimestre_4_r : intval($action->trimestre_4_r) }}</td>
                <td style="text-align:center;">{{ fmod($action->meta_anual, 1) !== 0.00 ? $action->meta_anual : intval($action->meta_anual) }}</td>
                <td style="text-align:center;">{{ fmod($action->meta_anual_r, 1) !== 0.00 ? $action->meta_anual_r : intval($action->meta_anual_r) }}</td>
                <td style="text-align:center; " class="{{ $action->getAdvanceColor() }}">{{ $action->getAdvance() }} %</td>
                <td>
                    @if (!empty($action->puesto_id))
                        {{ $action->responsable->nombre }}
                    @else
                        Sin Responsable
                    @endif
                </td>
            </tr>
            @if (count($action->subactions) > 0)
                @foreach ($action->subactions as $subactions)
                    <tr>
                        <td colspan="2" style="border-top:0px solid #fff;"></td>
                        <td>{{ $subactions->actividad }}</td>
                        <td style="border-top:0px solid #fff;"></td>
                        <td style="text-align:center;">{{ fmod($subactions->trimestre_1, 1) !== 0.00 ? $subactions->trimestre_1 : intval($subactions->trimestre_1) }}</td>
                        <td style="text-align:center;">{{ fmod($subactions->trimestre_1_r, 1) !== 0.00 ? $subactions->trimestre_1_r : intval($subactions->trimestre_1_r) }}</td>
                        <td style="text-align:center;">{{ fmod($subactions->trimestre_2, 1) !== 0.00 ? $subactions->trimestre_2 : intval($subactions->trimestre_2) }}</td>
                        <td style="text-align:center;">{{ fmod($subactions->trimestre_2_r, 1) !== 0.00 ? $subactions->trimestre_2_r : intval($subactions->trimestre_2_r) }}</td>
                        <td style="text-align:center;">{{ fmod($subactions->trimestre_3, 1) !== 0.00 ? $subactions->trimestre_3 : intval($subactions->trimestre_3) }}</td>
                        <td style="text-align:center;">{{ fmod($subactions->trimestre_3_p, 1) !== 0.00 ? $subactions->trimestre_3_p : intval($subactions->trimestre_3_p) }}</td>
                        <td style="text-align:center;">{{ fmod($subactions->trimestre_4, 1) !== 0.00 ? $subactions->trimestre_4 : intval($subactions->trimestre_4) }}</td>
                        <td style="text-align:center;">{{ fmod($subactions->trimestre_4_r, 1) !== 0.00 ? $subactions->trimestre_4_r : intval($subactions->trimestre_4_r) }}</td>
                        <td style="text-align:center;">{{ fmod($subactions->meta_anual, 1) !== 0.00 ? $subactions->meta_anual : intval($subactions->meta_anual) }}</td>
                        <td style="text-align:center;">{{ fmod($subactions->meta_anual_r, 1) !== 0.00 ? $subactions->meta_anual_r : intval($subactions->meta_anual_r) }}</td>
                        <td style="text-align:center;" class="{{ $action->getAdvanceColor() }}">{{ $action->getAdvance() }} %</td>
                        <td style="border-top:0px solid #fff;"></td>
                    </tr>
                    @endforeach
                    @endif
                    @endforeach
                    @else
                    <tr>
            <td style="text-align:center;" class="red" colspan="10">
                Sin Actividades
            </td>
        </tr>
    @endif
</table> <br><br>
@endforeach
@if (!empty($director))
<br><br><br><br><br><br>
<table>
    <tr>
        <td style="text-align:center; font-weight:bold;">
            <span class="span">________________________________________________________________</span>
        </td>
    </tr>
    <tr>
        <td style="text-align:center; font-weight:bold; font-size:1.2em;">
            <span>{{ $director->nombre." ".$director->apellido_pat." ".$director->apellido_mat }}</span>
        </td>
    </tr>
    <tr>
            <td style="text-align:center; font-weight:bold; font-size:1em;">
            <span>{{ $director->position->descripcion }}</span>
        </td>
    </tr>
</table><br><br>
@endif
