@php setlocale(LC_TIME, 'es'); @endphp

<style>
    .red{
        background-color: #f44336;
        color: #fff;
    }
    .yellow{
        background-color: #ffcc80;
        color: #000;
    }
    .green{
        background-color: #43a047;
        color: #000;
    }
    .gray{
        background-color: #9e9e9e;
        color: #000;
    }
    .blue{
        background-color: #13629D;
        color: #000;
    }
    .col-020 {
        width: 2%;
        text-align: center;
        font-size: 8px;
    }
    .col-10 {
        width: 10%;
        text-align: center;
    }
    .col-14 {
        width: 14%;
        text-align: center;
    }
    .rotate {
        text-align: center;
    }
    .black {
        font-weight:bold;
        text-align: center;
    }
    .small{
        font-size: 8px;
    }
</style> 

<h4 class="black">100C = 100% Concluidas, NC = No Concluidas, NA = No Atendidas, Tot. = Total</h4>
<table border="1px">
    <thead>
        <tr>
            <th rowspan="2" class="col-10">Direccion</th>
            @foreach ($stadistics["axes"] as $axe)
                <th class="col-10" scope="col" colspan="5">{{ $axe }}</th>
            @endforeach
        </tr>
        <tr>
            @foreach ($stadistics["axes"] as $axe)
                <th class="green"><span>100C</span></th>
                <th class="yellow"><span>NC</span></th>
                <th class="red"><span>NA</span></th>
                <th class="gray"><span>NP</span></th>
                <th class="blue"><span>Tot.</span></th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @php
            unset($stadistics["axes"]);   
        @endphp
        @foreach ($stadistics as $data)
            <tr>
                <td class="col-10 small">{{ $data["name"] }}</td>
                @foreach ($data["axes"] as $axe)
                    <td class="col-020">{{ round($axe["finish"], 1) }}</td>
                    <td class="col-020">{{ round($axe["process"], 1) }}</td>
                    <td class="col-020">{{ round($axe["null"], 1) }}</td>
                    <td class="col-020">{{ round($axe["empty"], 1) }}</td>
                    <td class="col-020">{{ round($axe["total"], 1) }}</td>
                @endforeach
            </tr>
        @endforeach
    </tbody>
</table>

