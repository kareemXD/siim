@php setlocale(LC_TIME, 'es'); @endphp

<style>
    .bg-red{
        background-color: #f44336;
        color: #fff;
    }
    .yellow{
        background-color: #ffcc80;
        color: #000;
    }
    .green{
        background-color: #43a047;
        color: #000;
    }
    .gray{
        background-color: #9e9e9e;
        color: #000;
    }
    .blue{
        background-color: #13629D;
        color: #000;
    }
    .col-1 {
        width: 10%;
        text-align: center;
    }
    .col-15 {
        width: 15%;
        text-align: center;
    }
    .col-3 {
        width: 30%;
        text-align: center;
    }
    .col-2 {
        width: 20%;
        text-align: center;
    }
    
</style> 

<h4 class="black">100C = 100% Concluidas, NC = No Concluidas, NA = No Atendidas, Tot. = Total</h4>
<table border="1px">
    <thead>
        <tr>
            <th class="col-15">Objetivo</th>
            <th class="col-1">Linea de Accion</th>
            <th class="col-3">Linea de Accion</th>
            <th class="col-15">Area Responsable</th>
            <th class="col-2">Accion</th>
            <th class="col-1">Avance</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($stadistics as $key => $data)
            @if ($data["type"] == "No Prog.")
                <tr>
                    <td class="col-15">{{ $data["objective"] }}</td>
                    <td class="col-1">{{ $data["point"] }}</td>
                    <td class="col-3">{{ $data["name"] }}</td>
                    <td class="col-15"></td>
                    <td class="col-2"></td>
                    <td class="col-1">No Prog.</td>
                </tr>
            @else
                @php
                    $rows = count($data["actions"]);
                @endphp
                    @if ($rows == 1)
                        <tr>
                            <td class="col-15" rowspan="{{ $rows }}">{{ $data["objective"] }}</td>
                            <td class="col-1" rowspan="{{ $rows }}">{{ $data["point"] }}</td>
                            <td class="col-3" rowspan="{{ $rows }}">{{ $data["name"] }}</td>
                            <td class="col-15">{{ $data["actions"][0]["resp"] }}</td>
                            <td class="col-2">{{ $data["actions"][0]["name"] }}</td>
                            <td class="col-1 bg-red" >{{ $data["actions"][0]["type"] }}</td>
                        </tr>
                    @endif
                @if ($rows > 1)
                    <tr>
                        <td class="col-15" rowspan="{{ $rows }}">{{ $data["objective"] }}</td>
                        <td class="col-1" rowspan="{{ $rows }}">{{ $data["point"] }}</td>
                        <td class="col-3" rowspan="{{ $rows }}">{{ $data["name"] }}</td>
                        <td class="col-15">{{ $data["actions"][0]["resp"] }}</td>
                        <td class="col-2">{{ $data["actions"][0]["name"] }}</td>
                        <td class="col-1 bg-red" >{{ $data["actions"][0]["type"] }}</td>
                    </tr>
                    @php
                        unset($data["actions"][0])
                    @endphp
                    @foreach ($data["actions"] as $action)
                    <tr>
                        <td class="col-15">{{ $action["resp"] }}</td>
                        <td class="col-2">{{ $action["name"] }}</td>
                        <td class="col-1 bg-red">{{ $action["type"] }}</td>
                    </tr>
                    @endforeach
                @endif
            @endif
            
        @endforeach
    </tbody>
</table>

