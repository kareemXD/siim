@extends('layouts.index')

@section('title') Editar Perfil @endsection

@section('css')
    
@endsection

@section('content')
    <div class="row justify-content-center mb-3">
        <h3>Notificaciones</h3>
    </div>

    <div class="col-xl-4">
        <ul class="list-group">
            @foreach ($notifications as $notification)
                @if ($notification->estatus == 0)
                    <li class="list-group-item view-notification-li" data-id="{{ $notification->id }}">{{ $notification->contenido }}</li>        
                @else
                    <li class="list-group-item view-notification-li vi" data-id="{{ $notification->id }}">
                        {{ $notification->contenido }}
                        <span class="notification-viewd">Visto <i class="fas fa-check"></i></span>
                    </li>        
                @endif
            @endforeach
        </ul>
    </div>
@endsection

@section('js')
    <script>
    $(function(){
        $(document).on("click", ".view-notification-li", function(e){
            e.preventDefault();
            var _this = $(this),
                id = _this.data("id"),
                token = "{{ csrf_token() }}";

            $.ajax({
                url: "{{ route('update.notification') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id
                }
            }).done(function(result){
                var modal = $("#modal-notification");
                
                modal.find('#date').val(result.created_at);
                modal.find('#notification').val(result.contenido);
                modal.modal("show");
                
                _this.addClass("vi");
                _this.append('<span class="notification-viewd">Visto <i class="fas fa-check"></i></span>');
                
            });
        });
    });
    </script>
@endsection