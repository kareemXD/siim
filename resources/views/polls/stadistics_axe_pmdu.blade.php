@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class="title-page row justify-content-center mb-3">
    <h3>Generar Estadisticas por Sección</h3>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif

<div class="row justify-content-center">
    <div id="data-table" class="col-12 col-xl-10">
        <table class="table  table-bordered table-sm">
            <thead>
                <tr>
                    <th rowspan="2">Sección</th>
                    <th colspan="3" style="text-align:center;">Observaciones</th>
                </tr>
                <tr>
                    <th>Resueltas</th>
                    <th>En Proceso</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($stadistics as $key => $data)
                    <tr>
                        <td>{{ $data["title"] }}</td>
                        <td>{{ round($data["answered"], 2) }}</td>
                        <td>{{ round($data["not_answered"], 2) }}</td>
                        <td>{{ round($data["total"], 2) }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-12 col-xl-10">
        <canvas id="myChart" ></canvas>
    </div>
    {{-- <div class="col-12 col-xl-8">
        <div class="justify-content-center row">
            <div class="col-3 mb-2">
                <button id="print-stadistics-btn" type="button" class="btn btn-primary btn-block" value="">Imprimir <i class="ml-2 fa fa-chart-bar"></i></button>
            </div>
        </div>
    </div> --}}
</div>
    
@endsection

@section('modals')

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
    <script src="{{ asset('assets/plugins/chart.js/Chart.js') }}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.1/Chart.min.js"></script> --}}
    <script>
        var labels = [];
        var views = [];
        var answered = [];
        var not_answered = [];

        @forEach($stadistics as $key => $axe)
            labels.push("{{ $axe['title'] }}");
            views.push("{{ round($axe['total'], 2) }}");
            answered.push("{{ round($axe['answered'], 2) }}");
            not_answered.push("{{ round($axe['not_answered'], 2) }}");
        @endforeach

		var barChartData = {
			labels: labels,
			datasets: [{
				label: 'Resueltas',
				backgroundColor: "#66bb6a",
				stack: 'Stack 0',
				data: answered
			}, {
				label: 'En Proceso',
				backgroundColor: "#fbc02d",
				stack: 'Stack 1',
				data: not_answered
			},{
				label: 'Total',
				backgroundColor: "#13629D",
				stack: 'Stack 2',
				data: views
			}]
		};
        
		window.onload = function() {
			var char = document.getElementById('myChart').getContext('2d');
			window.myBar = new Chart(char, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Grafica Acciones PMD'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					},
                    animation: {
                        onComplete: function () {
                            var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                            ctx.font = Chart.helpers.fontString(16, "bold", Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];                            
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                });
                            });
                        }
                    }
				}
			});
		};
        $(function(){
            
            $(document).on("click", "#print-stadistics-btn", function(){
                var newCanvas = document.querySelector("#myChart");
                var newCanvasImg = newCanvas.toDataURL("image/png", 1.0);
                $("input[name='image']").val(newCanvasImg);
                $("#print-stadistics").submit();
            });
        });

	</script>
@endsection