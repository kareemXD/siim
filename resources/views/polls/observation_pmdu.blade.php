@extends('layouts.index')

@section('title') Observaciones PMDU @endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
@endsection

@section('content')
    <div class=" mb-3 text-center">
        <h4>Observaciones PMDU</h4>
    </div>
    <div class="table-responsive ">
        <div id="table-pdf" style="display:none;">
            <button type="button" id="btn-export" class="btn btn-secondary"><i class="fas fa-file-pdf mr-2"></i>Exportar a PDF</button>
        </div>
        <div id="table-filter" style="display:none; text-align:left;">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-add-observation-pmdu"><i class="fas fa-plus-circle mr-2"></i>Nueva observación</button>
            <button type="button" class="btn btn-success btn-get-signature" ><i class="fas fa-file-pdf"></i> Frimar Oficios</button>
            <button type="button" data-toggle="modal" data-target="#modal-send-ofices" class="btn btn-warning" ><i class="fas fa-file-pdf"></i> Enviar Oficios</button>
        </div>
        <form id="form-export" target="_blank" action="{{ route('export.to.pdf') }}" method="POST">
            @csrf
            <table class="table table-sm dataTable">
                <thead>
                    <tr>
                        <th scope="col"><input type="text" id="tb-cuenta" name="tb_cuenta" placeholder="No. Solicitud" class="form-control" /></th>
                        <th scope="col"><input type="text" id="tb-nombre" name="tb_nombre" placeholder="Nombre" class="form-control" /></th>
                        <th scope="col"><input type="text" id="tb-correo" name="tb_correo" placeholder="Correo" class="form-control" /></th>
                        <th scope="col"><select class="form-control" id="tb-section" name="tb_section">
                            <option value="">Todos los Origenes</option>
                            <option value="Pagina Web">Pagina Web</option>
                            <option value="Bitacora">Bitacora</option>
                            <option value="Oficio">Oficio</option>
                            <option value="Correo">Correo</option>
                        </select></th>
                        <th scope="col"><input type="text" id="tb-fecha" name="tb_fecha" placeholder="Fecha" class="form-control" /></th>
                        <th scope="col"><select class="form-control" id="tb-procedence" name="tb_procedence">
                            <option value="">Todos</option>
                            <option value="Procede">Procede</option>
                            <option value="Procede Parcialmente">Procede Parcialmente</option>
                            <option value="No Procede">No Procede</option>
                            <option value="No Procede Rev">No Procede Rev</option>
                            {{-- <option value="En Estudio">En Estudio</option> --}}
                            <option value="Relativo a otra área">Relativo a otra área</option>
                            <option value="Comentario">Comentario</option>
                        </select></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    <tr>
                        <th scope="col">Folio</th>
                        <th scope="col">Nombre de Observador</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Origen</th>
                        <th scope="col">Fecha</th>
                        <th scope="col">¿Procede?</th>
                        <th scope="col">Estatus</th>
                        <th scope="col">Enviados</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($observations as $observation)
                        <tr>
                            <td scope="row" style="width:10%;">{{ $observation->folio }}</td>
                            <td>{{ $observation->nombre." ".$observation->apellidos }}</td>
                            <td>{{ $observation->correo }}</td>
                            <td>{{ $observation->origen }}</td>
                            
                            {{-- @php
                                $text = "";
                                foreach ($observation->sections as $key => $section) {
                                    $text .= $section->seccion.", ";
                                }
                                $text = substr($text, 0, -2);
                            @endphp --}}
                            {{-- <td>{{ $text }}</td> --}}

                            <td>{{ Carbon\Carbon::parse($observation->created_at)->format('d-m-Y') }}</td>
                            @php
                                $color = "";
                                $text = "";
                                $type = "";
                                switch ($observation->procede) {
                                    case 'Procede': $color = "#e1bee7"; $text = "Procede"; $type = 1; break;
                                    case 'Procede Parcialmente': $color = "#90caf9"; $text = "Procede Parcialmente"; $type = 2; break;
                                    case 'No Procede': $color = "#ffcc80"; $text = "No Procede"; $type = 3; break;
                                    case 'No Procede Rev': $color = "#c8e6c9"; $text = "No Procede Rev"; $type = 5; break;
                                    case 'Relativo a otra área': $color = "#ff80ab"; $text = "Relativo a otra área"; $type = 4; break;
                                    case 'Sin Aporte': $color = "#fff176"; $text = "Comentario"; $type = 5; break;
                                }
                            @endphp
                            <td style="background-color:{{ $color }};">{{ $text }}</td>
                            <td class="{{ ($observation->estatus == 0 ? 'bg-yellow' : 'bg-green') }}">{{ ($observation->estatus == 0 ? 'En Proceso' : 'Resuelta') }}</td>
                            <td class="{{ ($observation->enviado == 0 ? 'bg-yellow' : 'bg-green') }}">{{ ($observation->enviado == 0 ? 'No Enviado' : 'Enviado') }}</td>
                            <td style="width:6%;">
                                <a href="#!" data-toggle="tooltip" data-placement="top" title="Ver Informacion" data-id="{{ $observation->id }}" class="btn btn-primary btn-view-data"><i class="fas fa-eye"></i></a>
                                @if ($observation->estatus != 0)
                                    <a href="{{ route('generate.pdf.response', $observation->id) }}" target="_BLANK" data-toggle="tooltip" data-placement="top" title="Generar Oficio de Respuesta" class="btn btn-success"><i class="fas fa-eye"></i></a>
                                    {{--  --}}
                                    {{-- <form action="{{ route('generate.pdf.response') }}" method="POST" style="display:inline-block;" target="_BLANK">
                                        @csrf
                                        <input type="hidden" name="observation" value="{{ $observation->id }}">
                                        <button type="submit" target="_BLANK" data-toggle="tooltip" data-placement="top" title="Generar Oficio de Respuesta" class="btn btn-success"><i class="fas fa-file-pdf"></i></button>
                                    </form> --}}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </form>
    </div>
@endsection

@section('modals')
    @include('partials.modals.webPage.modal_observation_pmdu')
    @include('partials.modals.webPage.modal_add_observation_pmdu')
    @include('partials.modals.webPage.modal_add_signature')
    @include('partials.modals.webPage.modal_send_ofices')
@endsection

@section('js')
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script>
        $(function(){
            $('.file').simpleFilePreview();
            @if(session()->exists("alert") == true)
                @if(session("alert") == true)
                    $.alert({
                        title:  "Aviso!!",
                        content: "Acción realizada correctamente.",
                        type: "blue",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                @elseif(session("alert") == false)
                    $.alert({
                        title:  "Aviso!!",
                        content: "Algo sucedio intente mas tarde!!",
                        type: "red",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                @endif
            @elseif(session()->has("alert_sign"))
                $.alert({
                    title:  "Aviso!!",
                    content: "{{ session('alert_sign') }}",
                    type: "blue",
                    buttons: {
                        Aceptar:{
                            text: "Aceptar",
                        }
                    }
                });
            @endif
            var table = $('.dataTable').DataTable({
                pageLength: 10,
                @if(session()->has("page_start"))
                    displayStart: parseInt('{{ session("page_start") }}'),
                @endif
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: '<"table-pdf-container"><"table-filter-container">tip',
                buttons: [
                {
                        extend: 'excel',
                        text: 'Excel',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                },
                {
                        extend: 'pdf',
                        text: 'PDF',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                }],
                initComplete: function(settings){
                    var api = new $.fn.dataTable.Api( settings );
                    $('.table-filter-container', api.table().container()).append(
                        $('#table-filter').detach().show()
                    );
                    
                    $('.table-pdf-container', api.table().container()).append(
                        $('#table-pdf').detach().show()
                    );

                    $('#tb-cuenta').on('keyup', function(){
                        table.columns( 0 ).search( this.value ).draw();
                    });
                    $('#tb-nombre').on('keyup', function(){
                        table.columns( 1 ).search( this.value ).draw();
                    });
                    $('#tb-correo').on('keyup', function(){
                        table.columns( 2 ).search( this.value ).draw();
                    });
                    $('#tb-section').on('change', function(){
                        table.columns( 3 ).search( this.value ).draw();
                    });
                    
                    $('#tb-fecha').on('keyup', function(){
                        table.columns( 4 ).search( this.value ).draw();
                    });
                    $('#tb-procedence').on('change', function(){
                        val = $.fn.dataTable.util.escapeRegex(this.value);
                        table.columns( 5 ).search( val ? '^'+val+'$' : '', true, false ).draw();
                    });
                }
            });

            $(document).on("click", "#btn-export", function(){
                $("#form-export")[0].submit();
            });
            $(document).on("click", ".btn-get-signature", function(){
                var id = $(this).data("id");
                var modal = $("#modal-add-signature");
                    modal.find("#observation").val(id);
                modal.modal("show");
            });
            $(document).on("click", ".btn-view-data", function(){
                var id = $(this).data("id");
                $.ajax({
                    url: "{{ route('get.data.observation.pmdu') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                    },
                }).done(function(result){
                    var modal = $("#modal-observation-pmdu");
                    modal.find("#id").val(result.id);
                    modal.find("#resend").val(true);
                    modal.find("#data-copy").val(result.copy);
                    modal.find("#data-name").val(result.nombre);
                    modal.find("#data-lastname").val(result.apellidos);
                    modal.find("#data-sex").empty();
                    if (result.sexo) {
                        modal.find("#data-sex").append($("<option>",{
                            value: result.sexo,
                            text: result.sexo
                        }));
                    }

                    modal.find("#data-community").empty();
                    if (result.localidad) {
                        modal.find("#data-community").append($("<option>",{
                            value: result.localidad,
                            text: result.localidad
                        }));
                    }

                    modal.find("#data-scholarship").empty();
                    if (result.escolaridad) {
                        modal.find("#data-scholarship").append($("<option>",{
                            value: result.escolaridad,
                            text: result.escolaridad
                        }));
                    }

                    modal.find("#data-occupation").empty();
                    if (result.ocupacion) {
                        modal.find("#data-occupation").append($("<option>",{
                            value: result.ocupacion,
                            text: result.ocupacion
                        }));
                    }
                    
                    modal.find("#data-response").attr("disabled", false);
                    modal.find("#data-response").val(result.respuesta);
                    modal.find("#btn-submit").fadeIn("fast");
                    if (result.respuesta) {
                        // modal.find("#data-response").attr("disabled", true);
                        // modal.find("#btn-submit").fadeOut("fast");   
                    }
                    modal.find("#page").val(table.page.info().start);
                    modal.find("#data-folio").val(result.folio);
                    modal.find("#data-origin").val(result.origen);
                    modal.find("#data-procedence").val(result.procede);
                    modal.find("#data-age").val(result.edad);
                    modal.find("#data-tel").val(result.telefono);
                    modal.find("#data-email").val(result.correo);
                    modal.find("#data-address").val(result.direccion);
                    modal.find("#data-observation").val(result.observaciones);
                    modal.find("#responsed").val(result.responsed);
                    modal.find("#pmdu-image").find("g[class='active']").each(function(){
                        $(this).removeAttr("class");
                    });
                    result.sections.forEach(element => {
                        modal.find("#pmdu-image").find("g[data-section='"+element.seccion+"']").attr("class", "active");
                    });
                    modal.modal("show");
                });
            });
        });
    </script>
@endsection