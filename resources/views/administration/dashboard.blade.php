@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
    <div class=" mb-3 text-center">
        <h3>Panel de Control</h3>
        <h4>( {{ Auth::user()->profile->position->direction->nombre }} )</h4>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary text-center" role="alert">
            {{ session("alert") }}
        </div>
    @endif
        <br><br><br>
    <div class=" mb-3 text-center">
        <h3>Sistemas de Evaluación del Desempeño</h3>
    </div>
    {{-- <div class="container">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tr>
                    <td class="col-8">
                        <span class="black">Objetivo:</span>  <br>
                        {{ Auth::user()->profile->position->direction->getMetadata->where("meta_llave", config('implan.metakeys.description'))->first()->valor }}
                    </td>
                    <td class="col-4">
                        <span class="black">Presupuesto:</span><br>
                        ${{ Auth::user()->profile->position->direction->getMetadata->where("meta_llave", config('implan.metakeys.budget'))->first()->valor }}
                    </td>
                </tr>
            </table>
        </div>
        @foreach ($data as $program)
            <table class="table table-bordered" >
                <tr>
                    <td colspan="14" class="blue black">
                        Tema ó Programa: {{ $program->nombre }}
                    </td>
                </tr>
                <tr>
                    <td rowspan="2">Estrategia</td>
                    <td rowspan="2">Linea</td>
                    <td rowspan="2">Actividad</td>
                    <td rowspan="2">U.M.</td>
                    <td rowspan="2">Meta Anual</td>
                    <td colspan="2">1° Tr</td>
                    <td colspan="2">2° Tr</td>
                    <td colspan="2">3° Tr</td>
                    <td colspan="2">4° Tr</td>
                    <td rowspan="2">Resp.</td>
                </tr>
                <tr>
                    <td>P</td>
                    <td>R</td>
                    <td>P</td>
                    <td>R</td>
                    <td>P</td>
                    <td>R</td>
                    <td>P</td>
                    <td>R</td>
                </tr>
            @foreach ($program->actions as $action)
                <tr>
                    <td >
                        @if (count($action->pm_action_lines) > 0)
                            @foreach ($action->pm_action_lines as $actionline)
                                PM. {{ $actionline->strategy->punto }} <br>
                            @endforeach
                        @endif
                        @if (count($action->inf_action_lines) > 0)
                            @foreach ($action->inf_action_lines as $actionline)
                                INF. {{ $actionline->topic->punto }} <br>
                            @endforeach
                        @endif
                        @if (count($action->sche_metas) > 0)
                            @foreach ($action->sche_metas as $actionline)
                                AGE. {{ $actionline->objective->punto }} <br>
                            @endforeach
                        @endif
                    </td>
                    <td >
                        @if (count($action->pm_action_lines) > 0)
                            @foreach ($action->pm_action_lines as $actionline)
                                {{ $actionline->punto }} <br>
                            @endforeach
                        @endif
                        @if (count($action->inf_action_lines) > 0)
                            @foreach ($action->inf_action_lines as $actionline)
                                {{ $actionline->punto }} <br>
                            @endforeach
                        @endif
                        @if (count($action->sche_metas) > 0)
                            @foreach ($action->sche_metas as $actionline)
                                {{ $actionline->punto }} <br>
                            @endforeach
                        @endif
                    </td>
                    <td >{{ $action->actividad }}</td>
                    <td >{{ $action->unidad_medida }}</td>
                    <td >{{ $action->meta_anual }}</td>
                    <td >{{ $action->trimestre_1 }}</td>
                    <td ></td>
                    <td >{{ $action->trimestre_2 }}</td>
                    <td ></td>
                    <td >{{ $action->trimestre_3 }}</td>
                    <td ></td>
                    <td >{{ $action->trimestre_4 }}</td>
                    <td ></td>
                    <td >
                        @if (count($action->pm_action_lines) > 0)
                            @foreach ($action->pm_action_lines as $actionline)
                                
                                {{ $resp->nombre }} <br>
                            @endforeach
                        @endif
                        @if (count($action->inf_action_lines) > 0)
                            @foreach ($action->inf_action_lines as $actionline)
                               
                                {{ $resp->nombre }} <br>
                            @endforeach
                        @endif
                        @if (count($action->sche_metas) > 0)
                            @foreach ($action->sche_metas as $actionline)
                                
                                {{ $resp->nombre }} <br>
                            @endforeach
                        @endif
                    </td>
                </tr>
            @endforeach
        </table>

        @endforeach
    </div> --}}
@endsection

@section('modals')
    @include('partials.modals.modal_status_ticket')
@endsection

@section('js')
    <script>
        $(function(){
            $("#dashboard").addClass('active');
            $(document).on('dblclick', '.edit-ticket', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.ticket.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    var modal = $("#modal-edit-ticket");
                    if(result.estatus_id == 1)
                    {
                        modal.find('.finished').css("display", "none");
                        modal.find('.comment').css("display", "none");
                        modal.find('.on-revision').css("display", "initial");
                        modal.find('.on-revision').data("id", result.id);
                    }

                    if(result.estatus_id == 2)
                    {
                        modal.find('.finished').css("display", "initial");
                        modal.find('.comment').css("display", "initial");
                        modal.find('.on-revision').css("display", "none");
                        modal.find('.ticket-id').val(result.id);
                        
                    }
                    modal.find('.priority-bar').css("background-color", result.prioridad_color);
                    modal.find('.priority-bar').text(result.prioridad_txt);
                    modal.find('.status-bar').css("background-color", result.estatus_color);
                    modal.find('.status-bar').text(result.estatus_txt);
                    modal.find('#ticket').val(result.folio);
                    modal.find('#ticket').val(result.folio);
                    modal.find('#user').val(result.solicitante);
                    modal.find('#description').val(result.descripcion);
                    modal.modal("show");
                });
            });

            $(document).on("click", ".on-revision", function (){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('change.ticket.status') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id,
                        status: 2
                    },
                }).done(function(result){
                    $(".alert").text("Se actualizo correctamente el estatus de un ticket.");
                    $(".alert").fadeIn("slow");
                    if($(".alert"))
                    {
                        setTimeout(() => {
                            $(".alert").fadeOut("slow"); 
                        }, 10000);
                    }
                });
            });
            
            $(document).on("click", ".finished", function (){
                $("#finish-ticket").submit();
            });
        });
    </script>
@endsection