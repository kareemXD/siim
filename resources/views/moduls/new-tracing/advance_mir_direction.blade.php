@extends('layouts.index')

@section('title') Avance de la MIR @endsection

@section('css')
    
@endsection

@section('content')
    <div class=" mb-3 text-center">
        <h3>Avance de Mir</h3>
        <h4>( {{ $direction->nombre }} )</h4>
        <h5>( <b>{{ $trimestre["label"] }}</b> )</h5>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary text-center" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    <div class="row mt-3">
        <div class="col-12">
            <table class="table dataTable">
                <thead>
                    <tr>
                        <th scope="col" >Área o Proyecto Específico</th>
                        <th scope="col" >Componente</th>
                    </tr>
                </thead>
                <tbody id="actions-poa">
                    @php
                        if(config('implan.times.planning'))
                        {
                            $period = config('implan.periods.previous');
                        }else {
                            $period = config('implan.periods.actual');
                        }
                    @endphp
                    @foreach($direction->areas()->where('periodo', 2021)->get() as $area)
                    @foreach($area->components()->where('periodo', 2021)->get() as $component)
                        <tr class="edit-program" data-toggle="collapse" data-target="#collapse{{ $component->id }}">
                            <td>{{ $area->nombre }}</td>
                            <td>{{ $component->objetivo }}</td>
                        </tr>
                        <tr class="no-hover son{{ $component->id }}" >
                            <td colspan="6" style="padding:initial;">
                                <div id="collapse{{ $component->id }}" class="collapse ml-5" >
                                    <table class="table table-bordered ">
                                        <thead class="no-hover">
                                            <tr>
                                                <th scope="col" style="text-align:center;" rowspan="2">Actividad</th>
                                                <th scope="col" style="text-align:center;" rowspan="2">U. de M.</th>
                                                @if ($trimestre["number"] == 1)
                                                    <th scope="col" style="text-align:center;" colspan="5">1° Trimeste</th>
                                                @elseif ($trimestre["number"] == 2)
                                                    <th scope="col" style="text-align:center;" colspan="5">2° Trimeste</th>
                                                @elseif ($trimestre["number"] == 3)
                                                    <th scope="col" style="text-align:center;" colspan="5">3° Trimeste</th>
                                                @elseif ($trimestre["number"] == 4)
                                                    <th scope="col" style="text-align:center;" colspan="5">4° Trimeste</th>
                                                @endif
                                                <th scope="col" style="text-align:center;" colspan="3">Meta Anual</th>
                                            </tr>
                                            <tr>
                                                <th scope="col" style="text-align:center;">P</th>
                                                <th scope="col" style="text-align:center;">R</th>
                                                @foreach ($trimestre["mounths"] as $mounth)
                                                    <th scope="col" style="text-align:center;">{{ $mounth }}</th>
                                                @endforeach
                                                <th scope="col" style="text-align:center;">P</th>
                                                <th scope="col" style="text-align:center;">R</th>
                                                <th scope="col" style="text-align:center;">Avance</th>
                                            </tr>
                                        </thead>
                                        <tbody id="actions-poa">
                                            @if (count($component->actions) > 0)
                                                @foreach ($component->actions as $action)
                                                    <tr data-toggle="collapse" data-target="#collapse-action{{ $action->id }}" class="update-action" data-id="{{ $action->id }}">
                                                        <td class="">{{ $action->actividad }}</td>
                                                        <td class="">{{ $action->unidad_medida }}</td>
                                                        @if ($trimestre["number"] == 1)
                                                            <td class="wd-3">{{ fmod($action->trimestre_1, 1) !== 0.00 ? $action->trimestre_1 : intval($action->trimestre_1) }}</td>
                                                            <td class="wd-3">{{ fmod($action->trimestre_1_r, 1) !== 0.00 ? $action->trimestre_1_r : intval($action->trimestre_1_r) }}</td>

                                                            <td class="wd-5">{{ fmod($action->enero, 1) !== 0.00 ? $action->enero : intval($action->enero) }}</td>
                                                            <td class="wd-5">{{ fmod($action->febrero, 1) !== 0.00 ? $action->febrero : intval($action->febrero) }}</td>
                                                            <td class="wd-5">{{ fmod($action->marzo, 1) !== 0.00 ? $action->marzo : intval($action->marzo) }}</td>
                                                        @elseif ($trimestre["number"] == 2)
                                                            <td class="wd-3">{{ fmod($action->trimestre_2, 1) !== 0.00 ? $action->trimestre_2 : intval($action->trimestre_2) }}</td>
                                                            <td class="wd-3">{{ fmod($action->trimestre_2_r, 1) !== 0.00 ? $action->trimestre_2_r : intval($action->trimestre_2_r) }}</td>

                                                            <td class="wd-5">{{ fmod($action->abril, 1) !== 0.00 ? $action->abril : intval($action->abril) }}</td>
                                                            <td class="wd-5">{{ fmod($action->mayo, 1) !== 0.00 ? $action->mayo : intval($action->mayo) }}</td>
                                                            <td class="wd-5">{{ fmod($action->junio, 1) !== 0.00 ? $action->junio : intval($action->junio) }}</td>
                                                        @elseif ($trimestre["number"] == 3)
                                                            <td class="wd-3">{{ fmod($action->trimestre_3, 1) !== 0.00 ? $action->trimestre_3 : intval($action->trimestre_3) }}</td>
                                                            <td class="wd-3">{{ fmod($action->trimestre_3_p, 1) !== 0.00 ? $action->trimestre_3_p : intval($action->trimestre_3_p) }}</td>

                                                            <td class="wd-5">{{ fmod($action->julio, 1) !== 0.00 ? $action->julio : intval($action->julio) }}</td>
                                                            <td class="wd-5">{{ fmod($action->agosto, 1) !== 0.00 ? $action->agosto : intval($action->agosto) }}</td>
                                                            <td class="wd-5">{{ fmod($action->septiembre, 1) !== 0.00 ? $action->septiembre : intval($action->septiembre) }}</td>
                                                        @elseif ($trimestre["number"] == 4)
                                                            <td class="wd-3">{{ fmod($action->trimestre_4, 1) !== 0.00 ? $action->trimestre_4 : intval($action->trimestre_4) }}</td>
                                                            <td class="wd-3">{{ fmod($action->trimestre_4_r, 1) !== 0.00 ? $action->trimestre_4_r : intval($action->trimestre_4_r) }}</td>

                                                            <td class="wd-5">{{ fmod($action->octubre, 1) !== 0.00 ? $action->octubre : intval($action->octubre) }}</td>
                                                            <td class="wd-5">{{ fmod($action->noviembre, 1) !== 0.00 ? $action->noviembre : intval($action->noviembre) }}</td>
                                                            <td class="wd-5">{{ fmod($action->diciembre, 1) !== 0.00 ? $action->diciembre : intval($action->diciembre) }}</td>
                                                        @endif
                                                        
                                                        <td class="wd-5">{{ fmod($action->meta_anual, 1) !== 0.00 ? $action->meta_anual : intval($action->meta_anual) }}</td>
                                                        <td class="wd-5">{{ fmod($action->meta_anual_r, 1) !== 0.00 ? $action->meta_anual_r : intval($action->meta_anual_r) }}</td>
                                                        <td class="wd-5 {{ $action->getAdvanceColor() }}">{{ $action->getAdvance() }}%</td>
                                                    </tr>
                                                    
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6" class="text-center">Ninguna Actividad Registrada</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        @endforeach {{--fin del bucle de los componentes--}}
                    @endforeach  {{--fin del bucle de las areas--}}
                </tbody>
            </table>
        </div>
    </div>
    
@endsection

@section('modals')
    @include('partials.modals.new-tracing.modal_update_action_goal')
@endsection

@section('js')
<script>
$(function(){
    var trimestre = "";
    @if ($trimestre["number"] == 1)
        trimestre = '<th style="text-align:center;" colspan="5">1° Trimeste</th>';
    @elseif ($trimestre["number"] == 2)
        trimestre = '<th style="text-align:center;" colspan="5">2° Trimeste</th>';
    @elseif ($trimestre["number"] == 3)
        trimestre = '<th style="text-align:center;" colspan="5">3° Trimeste</th>';
    @elseif ($trimestre["number"] == 4)
        trimestre = '<th style="text-align:center;" colspan="5">4° Trimeste</th>';
    @endif

    var meses = "";

    @foreach ($trimestre["mounths"] as $mounth)
        meses += '<th style="text-align:center;">{{ $mounth }}</th>';
    @endforeach

    $(document).on("dblclick", ".update-action", function(){
        var action = $(this).data("id"),
        token = "{{ csrf_token() }}";

        $.ajax({
            url: "{{ route('get.action.mir.by.id') }}",
            type: "POST",
            data: {
                _token: token,
                action: action
            },
        }).done(function(result){
            var modal = $("#modal-update-action-goal");
            modal.find("#action_id").val(result.id);
            modal.find("#action").val(result.actividad);
            modal.find("#udem").val(result.unidad_medida);
            modal.find("#anual-p").val(result.meta_anual);
            modal.find("#anual-r").val(result.meta_anual_r);
            modal.find("#anual-r-initial").val(result.meta_anual_r);
            modal.find("#mir_beneficiarios").val(result.beneficiarios);
            
            @if ($trimestre["number"] == 1)
                modal.find("#Enero").val(result.enero);
                modal.find("#Febrero").val(result.febrero);
                modal.find("#Marzo").val(result.marzo);

                modal.find("#trimestre-p").val(result.trimestre_1);
                modal.find("#trimestre-r").val(result.trimestre_1_r);

                modal.find("#Enero").removeAttr('readonly');
                modal.find("#Febrero").removeAttr('readonly');
                modal.find("#Marzo").removeAttr('readonly');

            @elseif ($trimestre["number"] == 2)
                modal.find("#Abril").val(result.abril);
                modal.find("#Mayo").val(result.mayo);
                modal.find("#Junio").val(result.junio);

                modal.find("#trimestre-p").val(result.trimestre_2);
                modal.find("#trimestre-r").val(result.trimestre_2_r);

                modal.find("#Abril").removeAttr('readonly');
                modal.find("#Mayo").removeAttr('readonly');
                modal.find("#Junio").removeAttr('readonly');

            @elseif ($trimestre["number"] == 3)
                modal.find("#Julio").val(result.julio);
                modal.find("#Agosto").val(result.agosto);
                modal.find("#Septiembre").val(result.septiembre);

                modal.find("#trimestre-p").val(result.trimestre_3);
                modal.find("#trimestre-r").val(result.trimestre_3_p);
                modal.find("#Julio").removeAttr('readonly');
                modal.find("#Agosto").removeAttr('readonly');
                modal.find("#Septiembre").removeAttr('readonly');
            
            @elseif ($trimestre["number"] == 4)
                modal.find("#Octubre").val(result.octubre);
                modal.find("#Noviembre").val(result.noviembre);
                modal.find("#Diciembre").val(result.diciembre);

                modal.find("#trimestre-p").val(result.trimestre_4);
                modal.find("#trimestre-r").val(result.trimestre_4_r);
                
                modal.find("#Octubre").removeAttr('readonly');
                modal.find("#Noviembre").removeAttr('readonly');
                modal.find("#Diciembre").removeAttr('readonly');
            @endif
            modal.modal("show");
        });
    });
});
</script>
@endsection