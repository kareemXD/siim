@extends('layouts.index')

@section('title') Noticias @endsection

@section('css')
@endsection

@section('content')
    <div class="row justify-content-center">
        <h3>Control de noticias</h3>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    <div class="row justify-content-end">
        <a href="{{ route('new.notice') }}" class="btn btn-primary mr-3 mb-2"><i class="fas fa-plus-circle mr-2"></i> Nueva noticia</a>
    </div>
    <div class="table-responsive">
        <table class="table dataTable">
            <thead>
                <tr>
                    <th scope="col">Nombre</th>
                    <th scope="col">Icono</th>
                    <th scope="col">Url</th>
                </tr>
            </thead>
            <tbody>
                {{-- @if (count($menus) > 0)
                    @foreach ($menus as $menu)
                        <tr data-id="{{ $menu->id }}" class="menu-row">
                            <td>{{ $menu->nombre }}</td>
                            <td>{{ $menu->icono }}</td>
                            <td>{{ $menu->url }}</td>
                        </tr>
                    @endforeach
                @endif --}}
                
            </tbody>
        </table>
    </div>
@endsection

@section('modals')

@endsection

@section('js')
    <script>
        $(function(){
            $("#users").addClass('active');
            
            $(".dataTable").DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });

            $(document).on("dblclick", ".menu-row", function(){
                var id = $(this).data("id");
                window.location.href = "/navegacion/editar/menu/"+id;
            });
        });
    </script>
@endsection