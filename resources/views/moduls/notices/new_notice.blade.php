@extends('layouts.index')

@section('title') Nueva Noticia @endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
    
@endsection

@section('content')
    <div class="row justify-content-center">
        <h3>Nueva Noticia</h3>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    
    <form action="{{ route('store.notice') }}" method="POST" id="form" enctype="multipart/form-data">
        @csrf
        <div class="row justify-content-center">
            <fieldset class="col-xl-4 col-sm-6 fieldset">
                <legend class="legend">Informacion de la nueva noticia(Español):</legend>
                <div class="form-row">
                    <div class="form-group col-xl-12">
                        <label for="titulo2">Titulo</label>
                        <input required type="text" class="form-control {{ $errors->has('titulo') ? ' is-invalid' : '' }}" id="titulo2" name="titulo" value="{{ old('titulo') }}">
                        @if ($errors->has('titulo'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('titulo') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-xl-12">
                        <label for="descripcionCorta2">Descripcion Corta</label>
                        <input required type="text" name="descripcionCorta" class="form-control {{ $errors->has('descripcionCorta') ? ' is-invalid' : '' }}" id="descripcionCorta2" value="{{ old('descripcionCorta')}}">
                        @if ($errors->has('descripcionCorta'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('descripcionCorta') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                        <div class="form-group col-xl-12">
                            <label for="descripcionEncabezado2">Descripcion Encabezado</label>
                            <textarea name="descripcionEncabezado" class="form-control {{ $errors->has('descripcionEncabezado') ? ' is-invalid' : '' }}" id="descripcionEncabezado2" cols="30" rows="5" value="{{ old('descripcionEncabezado')}}"></textarea>
                            @if ($errors->has('descripcionEncabezado'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('descripcionEncabezado') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                <div class="form-row">
                    <div class="form-group col-xl-12">
                        <label for="descripcionCompleta2">Descripcion Completa</label>
                        <textarea required name="descripcionCompleta" class="form-control {{ $errors->has('descripcionCompleta') ? ' is-invalid' : '' }}" id="descripcionCompleta2" cols="30" rows="5" value="{{ old('descripcionCompleta')}}"></textarea>
                        @if ($errors->has('descripcionCompleta'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('descripcionCompleta') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-xl-12">
                        <label for="file2">Adjuntar Imagen o Video</label>
                        <select required class="form-control" name="" id="opciones">
                            <option value="">Seleccione tipo de archivo</option>
                            <option value="imagen">Imagen</option>
                            <option value="carousel">Carousel</option>
                            <option value="video">Video</option>
                        </select>
                        @if ($errors->has('file'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('file') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row" id="inputFiles">
                   
                </div>
            </fieldset>
            <fieldset class="col-xl-4 col-sm-6 ml-3 fieldset">
                <legend class="legend">Information of the new news (English):</legend>
                <div class="form-row">
                    <div class="form-group col-xl-12">
                        <label for="name2">Title</label>
                        <input required type="text" class="form-control {{ $errors->has('title') ? ' is-invalid' : '' }}" id="name2" name="title" value="{{ old('title') }}">
                        @if ($errors->has('title'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('title') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-xl-12">
                        <label for="shortDescription2">Short Description</label>
                        <input required type="text" name="shortDescription" class="form-control {{ $errors->has('shortDescription') ? ' is-invalid' : '' }}" id="shortDescription2" value="{{ old('shortDescription')}}">
                        @if ($errors->has('shortDescription'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('shortDescription') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-xl-12">
                        <label for="headerDescription2">Header Description</label>
                        <textarea name="headerDescription" class="form-control {{ $errors->has('headerDescription') ? ' is-invalid' : '' }}" id="headerDescription2" cols="30" rows="5" value="{{ old('headerDescription')}}"></textarea>
                        @if ($errors->has('headerDescription'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('headerDescription') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-xl-12">
                        <label for="completeDescription2">Complete Description</label>
                        <textarea required name="completeDescription" class="form-control {{ $errors->has('completeDescription') ? ' is-invalid' : '' }}" id="completeDescription2" cols="30" rows="5" value="{{ old('completeDescription')}}"></textarea>
                        @if ($errors->has('completeDescription'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('completeDescription') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </fieldset>
        </div>
        <div class="row justify-content-center mt-3">
            <button type="button" class="btn btn-success mr-2 mb-2 mostrarModal"><i class="fas fa-square-full mr-2"></i>Previsualización</button>
            <button type="submit" class="btn btn-primary mr-2 mb-2"><i class="fas fa-save mr-2"></i> Guardar</button>
        </div>
    </form>
    
@endsection

@section('modals')
    @include('partials.modals.webPage.notices.modal_pre_new')
@endsection

@section('js')
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script>
        $(document).on('change','#opciones', function () {
            var option = $(this).val();
            switch (option) {
                case "imagen":
                    $("#inputFiles").html(' <div class="form-group"><input required type="file" name="file" id="file2"></div>');
                    $(function(){
                        $('#file2').simpleFilePreview();        
                    });
                    break;
                case "carousel":
                    $("#inputFiles").html('<div class="form-group col-xl-4"><input required type="file" name="file[]" class="file2"></div>\
                                           <div class="form-group col-xl-4"><input required type="file" name="file[]" class="file2"></div>\
                                           <div class="form-group col-xl-4"><input required type="file" name="file[]" class="file2"></div>');
                                           
                    $(".img").html('<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">\
                                        <ol class="carousel-indicators">\
                                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>\
                                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>\
                                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>\
                                        </ol>\
                                        <div class="carousel-inner items">\
                                        </div>\
                                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">\
                                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>\
                                            <span class="sr-only">Previous</span>\
                                        </a>\
                                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">\
                                            <span class="carousel-control-next-icon" aria-hidden="true"></span>\
                                            <span class="sr-only">Next</span>\
                                        </a>\
                                    </div>');                       
                    $(function(){
                        $('.file2').simpleFilePreview();        
                    });
                    break;
                case "video":   
                    $("#inputFiles").html('<div class="form-group"><input type="file" name="file[]" class="video" accept="video/*"></div>');
                    $(function(){
                        $('.video').simpleFilePreview();        
                    });
                    break; 
                default:
                    break;
            }
        });

        $(document).on('click','.mostrarModal',function () {
            var form = $('#form').serializeArray();
            var values = [];
            $.each(form, function(i, field){
                values[field.name] = field.value;
            });
            $(".titulo").html("<h4>"+values["titulo"]+"</h4>");
            $(".header").html("<h5>"+values["descripcionEncabezado"]+"</h5>");
            $(".description").html("<p>"+values["descripcionCompleta"]+"</p>");

            var img = document.getElementsByClassName("element");
            for (var i=0; i<img.length; i++) 
            {
                var element = img[i];
                var parent = element.parentNode;
                var wrapper = document.createElement('div');
                if(i == 0)
                {
                    wrapper.className = "carousel-item active";
                }
                else
                {
                    wrapper.className = "carousel-item";
                }
                parent.replaceChild(wrapper, element);
                wrapper.appendChild(element);
            }
            $('#modal-pre-new').modal('show');
        });

        $(document).on("change", ".file2", function(evt) {
            if(this.files && this.files[0]){
                /* Creamos la Imagen*/
                    var img = $('<img class="d-block w-100 element">');
                /* Asignamos el atributo source , haciendo uso del método createObjectURL*/
                    img.attr('src', URL.createObjectURL(this.files[0]));
                /* Añadimos al Div*/
                $('.items').append(img);  
	        }
        });

        $(document).on("change", ".video", function(evt) {
            $(".img").html('<video width="100%" controls>\
                                <source src="mov_bbb.mp4" id="video_here">\
                            </video>');
            var $source = $('#video_here');
            $source[0].src = URL.createObjectURL(this.files[0]);
            $source.parent()[0].load();
        });

        $(document).on('change','#file2',function(e) {
                $(".img").html('<img id="imgSalida" width="100%" height="100%" src="" />');
                addImage(e); 
        });

        function addImage(e){
            var file = e.target.files[0],
            imageType = /image.*/;
            
            if (!file.type.match(imageType))
            return;
        
            var reader = new FileReader();
            reader.onload = fileOnload;
            reader.readAsDataURL(file);
        }
    
        function fileOnload(e) {
            var result=e.target.result;
            $('#imgSalida').attr("src",result);
        }
    </script>
@endsection