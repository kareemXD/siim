@extends('layouts.index')

@section('title') Mapas @endsection

@section('css')
    
@endsection

@section('content')
<div class="title-page row justify-content-center mb-3">
    <h3>Reporte de Mapas</h3>
</div>
<form id="print-stadistics" target="_BLANK" action="{{ route('geo.print.report') }}" method="post">
    @csrf
    <input type="hidden" name="image">
    <div class="row">
        <div class="form-group col-6 offset-3">
            <label for="tipo-reporte">Reporte</label>
            <select class="custom-select" id="tipo-reporte" name="tipo_reporte">
                <option value="general" selected>Reporte General</option>
                <option value="covid">Casos Covid 19</option>
                <option value="filtros">Filtros Sanitarios</option>
                <option value="hospitales">Hospitales</option>
                <option value="comedores">Comedores</option>
                <option value="apoyos">Beneficiarios apoyos sociales</option>
            </select>
        </div>
    </div>
    <div class="row" id="localidad-container" style="display:none;">
        <div class="form-group col-6 offset-3">
            <label for="localidad-reporte">Localidad</label>
            <select class="custom-select" id="localidad-reporte">
            </select>
        </div>
    </div>
</form>
<div class="row justify-content-center">
    <div class="col-12 col-xl-10">
        <canvas id="myChart" ></canvas>
    </div>
</div>
<div class="col-12 col-xl-12 mt-2">
    <div class="justify-content-center row">
        <div class="col-13 mb-2">
            <button id="print-stadistics-btn" type="button" class="btn btn-primary btn-block" value="">Imprimir <i class="ml-2 fa fa-chart-bar"></i></button>
        </div>
    </div>
</div>
    
@endsection

@section('modals')

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
    <script src="{{ asset('assets/plugins/chart.js/Chart.js') }}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.1/Chart.min.js"></script> --}}
    <script>
        var char = {};
    
        function loadGraphic(){
            var labels = [];
            var total = [];

            labels.push("Casos Covid 19");
            labels.push("Beneficiarios");
            labels.push("Tiendas Participantes");
            labels.push("Delitos");
            labels.push("Concetación de Personas");
            labels.push("Incidentes de Transito");
            labels.push("Filtros Sanitarios");
            labels.push("Hospitales");
            
            total.push("{{ $data['covid'] }}");
            total.push("{{ $data['beneficiarios'] }}");
            total.push("{{ $data['tiendas'] }}");
            total.push("{{ $data['delitos'] }}");
            total.push("{{ $data['concentracion'] }}");
            total.push("{{ $data['transito'] }}");
            total.push("{{ $data['filtros'] }}");
            total.push("{{ $data['hospitales'] }}");

            var barChartData = {
                labels: labels,
                datasets: [{
                    label: 'Contadores',
                    backgroundColor: "#66bb6a",
                    stack: 'Stack 0',
                    data: total
                }]

            };
            container = document.getElementById('myChart').getContext('2d');
			char = new Chart(container, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Grafica Mapas'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					},
                    animation: {
                        onComplete: function () {
                            var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                            ctx.font = Chart.helpers.fontString(16, "bold", Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];                            
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                });
                            });
                        }
                    }
				}
			});
        }
        window.onload = function() {
			loadGraphic();
		};
        $(function(){
            $(document).on("click", "#print-stadistics-btn", function(){
                var newCanvas = document.querySelector("#myChart");
                var newCanvasImg = newCanvas.toDataURL("image/png", 1.0);
                $("input[name='image']").val(newCanvasImg);
                $("#print-stadistics").submit();
            });

            $(document).on("change", "#tipo-reporte", function(){
                $("#localidad-container").fadeOut("fast");
                var tipo = $(this).val();
                $.ajax({
                    url: "{{ route('geo.get.data.report') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        tipo: tipo,
                    },
                }).done(function(result){
                    console.log(result);
                    switch(tipo){
                        case "general":
                            char.destroy();
                            loadGraphic();
                            break;
                        case "covid":
                            char.destroy();
                            var labels = [];
                            var total = [];

                            labels.push("Positivos");
                            // labels.push("Altas");
                            labels.push("Defunciones");
                            
                            total.push(result.positivos);
                            // total.push(result.altas);
                            total.push(result.defunciones);

                            var barChartData = {
                                labels: labels,
                                datasets: [{
                                    label: 'Casos Covid-19',
                                    backgroundColor: "#66bb6a",
                                    stack: 'Stack 0',
                                    data: total
                                }]

                            };
                            container = document.getElementById('myChart').getContext('2d');
                            char = new Chart(container, {
                                type: 'bar',
                                data: barChartData,
                                options: {
                                    title: {
                                        display: true,
                                        text: 'Grafica Mapas'
                                    },
                                    tooltips: {
                                        mode: 'index',
                                        intersect: false
                                    },
                                    responsive: true,
                                    scales: {
                                        xAxes: [{
                                            stacked: true,
                                        }],
                                        yAxes: [{
                                            stacked: true
                                        }]
                                    },
                                    animation: {
                                        onComplete: function () {
                                            var chartInstance = this.chart,
                                            ctx = chartInstance.ctx;
                                            ctx.font = Chart.helpers.fontString(16, "bold", Chart.defaults.global.defaultFontFamily);
                                            ctx.textAlign = 'center';
                                            ctx.textBaseline = 'bottom';

                                            this.data.datasets.forEach(function (dataset, i) {
                                                var meta = chartInstance.controller.getDatasetMeta(i);
                                                meta.data.forEach(function (bar, index) {
                                                    var data = dataset.data[index];                            
                                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                                });
                                            });
                                        }
                                    }
                                }
                            });
                            break;
                        case "filtros":
                            char.destroy();
                            var labels = [];
                            var retornos = [];
                            var foraneos = [];

                            result.forEach(function(item){
                                labels.push(item.nombre);
                                retornos.push(item.retornos);
                                foraneos.push(item.foraneos);
                            });
                            
                            var barChartData = {
                                labels: labels,
                                datasets: [{
                                    label: 'Retornos',
                                    backgroundColor: "#66bb6a",
                                    stack: 'Stack 0',
                                    data: retornos
                                },{
                                    label: 'Foraneos',
                                    backgroundColor: "#2196f3",
                                    stack: 'Stack 1',
                                    data: foraneos
                                }]

                            };
                            container = document.getElementById('myChart').getContext('2d');
                            char = new Chart(container, {
                                type: 'bar',
                                data: barChartData,
                                options: {
                                    title: {
                                        display: true,
                                        text: 'Grafica Mapas'
                                    },
                                    tooltips: {
                                        mode: 'index',
                                        intersect: false
                                    },
                                    responsive: true,
                                    scales: {
                                        xAxes: [{
                                            stacked: true,
                                        }],
                                        yAxes: [{
                                            stacked: true
                                        }]
                                    },
                                    animation: {
                                        onComplete: function () {
                                            var chartInstance = this.chart,
                                            ctx = chartInstance.ctx;
                                            ctx.font = Chart.helpers.fontString(16, "bold", Chart.defaults.global.defaultFontFamily);
                                            ctx.textAlign = 'center';
                                            ctx.textBaseline = 'bottom';

                                            this.data.datasets.forEach(function (dataset, i) {
                                                var meta = chartInstance.controller.getDatasetMeta(i);
                                                meta.data.forEach(function (bar, index) {
                                                    var data = dataset.data[index];                            
                                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                                });
                                            });
                                        }
                                    }
                                }
                            });
                            break;
                        case "hospitales":
                            char.destroy();
                            var labels = [];
                            var camas = [];

                            result.forEach(function(item){
                                labels.push(item.nombre);
                                camas.push(item.camas);
                            });
                            
                            var barChartData = {
                                labels: labels,
                                datasets: [{
                                    label: 'Camas Disponibles',
                                    backgroundColor: "#66bb6a",
                                    stack: 'Stack 0',
                                    data: camas
                                }]

                            };
                            container = document.getElementById('myChart').getContext('2d');
                            char = new Chart(container, {
                                type: 'bar',
                                data: barChartData,
                                options: {
                                    title: {
                                        display: true,
                                        text: 'Grafica Mapas'
                                    },
                                    tooltips: {
                                        mode: 'index',
                                        intersect: false
                                    },
                                    responsive: true,
                                    scales: {
                                        xAxes: [{
                                            stacked: true,
                                        }],
                                        yAxes: [{
                                            stacked: true
                                        }]
                                    },
                                    animation: {
                                        onComplete: function () {
                                            var chartInstance = this.chart,
                                            ctx = chartInstance.ctx;
                                            ctx.font = Chart.helpers.fontString(16, "bold", Chart.defaults.global.defaultFontFamily);
                                            ctx.textAlign = 'center';
                                            ctx.textBaseline = 'bottom';

                                            this.data.datasets.forEach(function (dataset, i) {
                                                var meta = chartInstance.controller.getDatasetMeta(i);
                                                meta.data.forEach(function (bar, index) {
                                                    var data = dataset.data[index];                            
                                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                                });
                                            });
                                        }
                                    }
                                }
                            });
                            break;
                        case "comedores":
                            char.destroy();
                            var labels = [];
                            var porciones = [];

                            result.forEach(function(item){
                                labels.push(item.nombre);
                                porciones.push(item.porciones);
                            });
                            
                            var barChartData = {
                                labels: labels,
                                datasets: [{
                                    label: 'Comedores',
                                    backgroundColor: "#66bb6a",
                                    stack: 'Stack 0',
                                    data: porciones
                                }]

                            };
                            container = document.getElementById('myChart').getContext('2d');
                            char = new Chart(container, {
                                type: 'bar',
                                data: barChartData,
                                options: {
                                    title: {
                                        display: true,
                                        text: 'Grafica Mapas'
                                    },
                                    tooltips: {
                                        mode: 'index',
                                        intersect: false
                                    },
                                    responsive: true,
                                    scales: {
                                        xAxes: [{
                                            stacked: true,
                                        }],
                                        yAxes: [{
                                            stacked: true
                                        }]
                                    },
                                    animation: {
                                        onComplete: function () {
                                            var chartInstance = this.chart,
                                            ctx = chartInstance.ctx;
                                            ctx.font = Chart.helpers.fontString(16, "bold", Chart.defaults.global.defaultFontFamily);
                                            ctx.textAlign = 'center';
                                            ctx.textBaseline = 'bottom';

                                            this.data.datasets.forEach(function (dataset, i) {
                                                var meta = chartInstance.controller.getDatasetMeta(i);
                                                meta.data.forEach(function (bar, index) {
                                                    var data = dataset.data[index];                            
                                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                                });
                                            });
                                        }
                                    }
                                }
                            });
                            break;
                        case "apoyos":
                            $("#localidad-reporte").empty();
                            $("#localidad-reporte").append("<option value=''>Selecciones una Localidad</option>");
                            result.forEach(function(item){
                                $("#localidad-reporte").append("<option value='"+item.nombre_localidad+"'>"+item.nombre_localidad+"</option>");
                            });
                            $("#localidad-container").fadeIn("slow");
                            break;
                        default:
                            char.destroy();
                            break;
                    }
                });
            });

            $(document).on("change", "#localidad-reporte", function(){
                var tipo = $("#tipo-reporte").val();
                var localidad = $(this).val();
                $.ajax({
                    url: "{{ route('geo.get.data.report') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        tipo: tipo,
                        localidad: localidad,
                    },
                }).done(function(result){
                    char.destroy();
                    var labels = [];
                    var despensas = [];
                    var vales = [];
                    var vales2 = [];
                    var economicos = [];

                    labels.push(localidad);
                    despensas.push(result.despensas);
                    vales.push(result.vales);
                    vales2.push(result.vales2);
                    economicos.push(result.economicos);
                    
                    var barChartData = {
                        labels: labels,
                        datasets: [{
                            label: 'Despensas',
                            backgroundColor: "#66bb6a",
                            stack: 'Stack 0',
                            data: despensas
                        },{
                            label: 'Vales de Despensa',
                            backgroundColor: "#2196f3",
                            stack: 'Stack 1',
                            data: vales
                        },{
                            label: 'Vales de Despensa 2°',
                            backgroundColor: "#fbc02d",
                            stack: 'Stack 2',
                            data: vales2
                        },{
                            label: 'Apoyos Economicos',
                            backgroundColor: "#e65100",
                            stack: 'Stack 3',
                            data: economicos
                        }]

                    };
                    container = document.getElementById('myChart').getContext('2d');
                    char = new Chart(container, {
                        type: 'bar',
                        data: barChartData,
                        options: {
                            title: {
                                display: true,
                                text: 'Grafica Mapas'
                            },
                            tooltips: {
                                mode: 'index',
                                intersect: false
                            },
                            responsive: true,
                            scales: {
                                xAxes: [{
                                    stacked: true,
                                }],
                                yAxes: [{
                                    stacked: true
                                }]
                            },
                            animation: {
                                onComplete: function () {
                                    var chartInstance = this.chart,
                                    ctx = chartInstance.ctx;
                                    ctx.font = Chart.helpers.fontString(16, "bold", Chart.defaults.global.defaultFontFamily);
                                    ctx.textAlign = 'center';
                                    ctx.textBaseline = 'bottom';

                                    this.data.datasets.forEach(function (dataset, i) {
                                        var meta = chartInstance.controller.getDatasetMeta(i);
                                        meta.data.forEach(function (bar, index) {
                                            var data = dataset.data[index];                            
                                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                        });
                                    });
                                }
                            }
                        }
                    });
                });
            });
        });

	</script>
@endsection