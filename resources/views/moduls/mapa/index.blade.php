@extends('layouts.index')

@section('title') Mapas @endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css"/> --}}
<link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />
<link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/sidebar/src/L.Control.Sidebar.css') }}" />
<link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/groups/dist/leaflet.groupedlayercontrol.min.css') }}" />

<style>
.map {
    height: 700px;
    width: 100%;
}
</style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <h3>Mapa Covid</h3>
    </div>
    <div class="tab-content mt-2" id="nav-tabContent">
        <div id="sidebar-leaflet"></div>
        <div id="map" class="map"></div> 
    </div>
@endsection

@section('modals')
    @include('partials.modals.mapa.modal_add_marker')
    @include('partials.modals.mapa.modal_view_marker')
    @include('partials.modals.mapa.modal_add_person_filter')
    @include('partials.modals.mapa.modal_add_dosificador')
    @include('partials.modals.mapa.modal_add_apoyo_hospital')
    @include('partials.modals.mapa.modal_add_porciones')
    @include('partials.modals.mapa.modal_add_direction_report')
@endsection

@section('js')
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script>
        var cartoIp = '{{ env("CartoWeb_IP") }}';
        var table = {};
        var table2 = {};
        var table3 = {};
        var table4 = {};
        var table4 = {};
    </script>
    <script src="{{ asset('/assets/plugins/leaflet/leaflet.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/wms/src/leaflet.wms.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/WFS/src/WFS.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/utm.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/sidebar/src/L.Control.Sidebar.js') }}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.js"></script> --}}
    <script src="{{ asset('/assets/plugins/leaflet/pm/pm.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/groups/dist/leaflet.groupedlayercontrol.min.js') }}"></script>
    {{-- <script src="{{ asset('assets/js/mapa.js') }}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script>
        $(function(){
            @if(session()->exists("alert"))
                @if(session("alert") == true)
                    $.alert({
                        title:  "Aviso!!",
                        content: "Acción realizada correctamente.",
                        type: "blue",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                @elseif(session("alert") == false)
                    $.alert({
                        title:  "Aviso!!",
                        content: "Algo sucedio intente mas tarde!!",
                        type: "red",
                        buttons: {
                            Aceptar:{
                                text: "Aceptar",
                            }
                        }
                    });
                @endif
            @endif
            var clicked = true;

            table = $('.dataTable').DataTable({
                pageLength: 10,
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: 'B<"table-filter-container">tip',
                buttons: [
                {
                        extend: 'excel',
                        text: 'Excel',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                },
                {
                        extend: 'pdf',
                        text: 'PDF',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                }],
                    initComplete: function(settings){
                        var _this = $(this);
                        var api = new $.fn.dataTable.Api( settings );
                        $('.table-filter-container', api.table().container()).append(
                            _this.parents(".form-group").find('.table-filter').detach().show()
                        );
                    }
            });
            table2 = $('.dataTable2').DataTable({
                pageLength: 10,
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: 'B<"table-filter-container">tip',
                buttons: [
                {
                        extend: 'excel',
                        text: 'Excel',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                },
                {
                        extend: 'pdf',
                        text: 'PDF',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                }],
                    initComplete: function(settings){
                        var _this = $(this);
                        var api = new $.fn.dataTable.Api( settings );
                        $('.table-filter-container', api.table().container()).append(
                            _this.parents(".form-group").find('.table-filter').detach().show()
                        );
                    }
            });
            table3 = $('.dataTable3').DataTable({
                pageLength: 10,
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: 'B<"table-filter-container">tip',
                buttons: [
                {
                        extend: 'excel',
                        text: 'Excel',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                },
                {
                        extend: 'pdf',
                        text: 'PDF',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                }],
                    initComplete: function(settings){
                        var _this = $(this);
                        var api = new $.fn.dataTable.Api( settings );
                        $('.table-filter-container', api.table().container()).append(
                            _this.parents(".form-group").find('.table-filter').detach().show()
                        );
                    }
            });
            table4 = $('.dataTable4').DataTable({
                pageLength: 10,
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: 'B<"table-filter-container">tip',
                buttons: [
                {
                        extend: 'excel',
                        text: 'Excel',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                },
                {
                        extend: 'pdf',
                        text: 'PDF',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                }],
                    initComplete: function(settings){
                        var _this = $(this);
                        var api = new $.fn.dataTable.Api( settings );
                        $('.table-filter-container', api.table().container()).append(
                            _this.parents(".form-group").find('.table-filter').detach().show()
                        );
                    }
            });
            table5 = $('.dataTable5').DataTable({
                pageLength: 10,
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: 'B<"table-filter-container">tip',
                buttons: [
                {
                        extend: 'excel',
                        text: 'Excel',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                },
                {
                        extend: 'pdf',
                        text: 'PDF',
                        className: 'btn btn-default',
                        exportOptions: {
                            columns: 'th:not(:last-child)'
                        }
                }],
                    initComplete: function(settings){
                        var _this = $(this);
                        var api = new $.fn.dataTable.Api( settings );
                        $('.table-filter-container', api.table().container()).append(
                            _this.parents(".form-group").find('.table-filter').detach().show()
                        );
                    }
            });
            $("#modal-view-marker").on('hidden.bs.modal', function (e) {
                $(this).find(".panel-update").fadeOut("fast", function(){
                    $(this).removeClass("panel-update");
                });
                $(this).find("#btn-update-marker").attr("disabled", true);
            });
            
            $(document).on("click", ".btn-view-data", function(){
                var id = $(this).data("id");
                var operation = $(this).data("operation");
                $.ajax({
                    url: "{{ route('geo.get.data') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                    },
                }).done(function(result){
                    console.log(result);
                    var modal = $("#modal-view-marker");
                    modal.find("#tipo-marker-up").val("op-"+operation);
                    modal.find("#type-marker").val("op-"+operation);
                    modal.find("#marker-id").val(id);
                    switch (operation) {
                        case "covid":
                            modal.find("#origin-covid").val(result.origen);
                            modal.find("#tipo-covid").val(result.tipo);
                            modal.find("#name-covid").val(result.nombre);
                            modal.find("#sex-covid").val(result.sexo);
                            modal.find("#age-covid").val(result.edad);
                            modal.find("#localidad-covid").val(result.localidad);
                            modal.find("#status-covid").val(result.estatus);
                            modal.find("#place-covid").val(result.lu_atencio);
                            modal.find("#onservation-covid").val(result.observacio);
                            modal.find("#onservation-covid").val(result.observacio);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-covid").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-covid").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "beneficiarios":
                            var califica = "";
                            if(result.califica_s == 1){
                                califica = "Sí";
                            }else{
                                califica = "No";
                            }
                            console.log(califica);
                            modal.find("#folio-beneficiarios").val(result.folio);
                            modal.find("#status-beneficiarios").val(califica);
                            modal.find("#name-beneficiarios").val(result.apellido_p);
                            modal.find("#curp-beneficiarios").val(result.clave_unic);
                            modal.find("#ine-beneficiarios").val(result.numero_de);
                            modal.find("#address-beneficiarios").val(result.domicilio);
                            modal.find("#localidad-beneficiarios").val(result.localidad);
                            modal.find("#sector-beneficiarios").val(result.sector_eco);
                            modal.find("#phone-beneficiarios").val(result.celular);
                            modal.find("#cp-beneficiarios").val(result.cp);
                            modal.find("#onservation-beneficiarios").val(result.observacio);
                            modal.find("#op-up-beneficiarios").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-beneficiarios").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "tiendas":
                            modal.find("#licencia-tiendas").val(result.licencia);
                            modal.find("#giro-tiendas").val(result.giro);
                            modal.find("#negocio-tiendas").val(result.nombre);
                            modal.find("#propietario-tiendas").val(result.propietari);
                            modal.find("#address-tiendas").val(result.domicilio);
                            modal.find("#localidad-tiendas").val(result.localidad);
                            modal.find("#phone-tiendas").val(result.telefono);
                            modal.find("#date-tiendas").val(result.registro);
                            modal.find("#calca-tiendas").val(result.calca_entr);
                            modal.find("#caretas-tiendas").val(result.caretas);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-tiendas").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-tiendas").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "delitos":
                            modal.find("#reporte-delitos").val(result.reporte);
                            modal.find("#delito-delitos").val(result.tipo_delit);
                            modal.find("#address-delitos").val(result.ubicacion);
                            modal.find("#localidad-delitos").val(result.localidad);
                            modal.find("#date-delitos").val(result.fecha_regi);
                            modal.find("#hour-delitos").val(result.hora_regis);
                            modal.find("#op-up-delitos").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-delitos").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "concentracion":
                            modal.find("#delito-concentracion").val(result.tipo);
                            modal.find("#date-concentracion").val(result.fecha);
                            modal.find("#address-concentracion").val(result.ubicacion);
                            modal.find("#motivo-concentracion").val(result.motivo);
                            modal.find("#op-up-concentracion").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-concentracion").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "transito":
                            modal.find("#folio-transito").val(result.folio);
                            modal.find("#incidente-transito").val(result.tipo_incid);
                            modal.find("#afectado-transito").val(result.estatus_af);
                            modal.find("#address-transito").val(result.ubicacion);
                            modal.find("#localidad-transito").val(result.localidad);
                            modal.find("#date-transito").val(result.fecha_regi);
                            modal.find("#hour-transito").val(result.hora_regis);
                            modal.find("#op-up-transito").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-transito").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "filtros":
                            $("#modal-add-person-filter").find("#filter-id").val(result["filtro"].id);
                            modal.find("#localidad-filtros").val(result["filtro"].filtro_loc);
                            modal.find("#personal-filtros").val(result["filtro"].personal);
                            modal.find("#turno-mat-filtros").val(result["filtro"].rep_matutino);
                            modal.find("#turno-ves-filtros").val(result["filtro"].rep_vespertino);
                            modal.find("#turno-noc-filtros").val(result["filtro"].rep_nocturno);
                            table.clear().draw();
                            result["personas"].forEach(function(item){
                                table.row.add([item.fecha, item.turno, item.personas, "<button type='button' data-id='"+item.id+"' class='btn btn-primary btn-view-data-filtro'><i class='fas fa-eye'></i></button>"]).draw( false );;
                            });
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-filtros").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-filtros").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "hospitales":
                            $("#modal-add-apoyo-hospital").find("#hospital-id").val(result["data"].id);
                            modal.find("#nombre-hospitales").val(result["data"].nom_estab);
                            modal.find("#actividad-hospitales").val(result["data"].nombre_act);
                            modal.find("#razon-hospitales").val(result["data"].raz_social);
                            modal.find("#personas-hospitales").val(result["data"].per_ocu);
                            modal.find("#vial-hospitales").val(result["data"].nom_vial);
                            modal.find("#camas-hospitales").val(result["data"].camas);
                            table3.clear().draw();
                            result["items"].forEach(function(item){
                                table3.row.add([item.fecha, item.tipo, item.cantidad]).draw();
                            });
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-hospitales").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-hospitales").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "cementerios":
                            modal.find("#ubicacion-cementerios").val(result.nombre);
                            modal.find("#espacios-cementerios").val(result.espacio_di);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-cementerios").animate({height:"toggle"}, "slow", function(){
                                modal.find("#op-up-cementerios").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "empresas":
                            modal.find("#nombre-empresas").val(result.nombre);
                            modal.find("#giro-empresas").val(result.giro_empre);
                            modal.find("#domicilio-empresas").val(result.domicilio);
                            modal.find("#localidad-empresas").val(result.localidad);
                            modal.find("#ss-empresas").val(result.afili_ss);
                            modal.find("#empleados-empresas").val(result.num_trabaj);
                            modal.find("#despidos-empresas").val(result.n_despidos);
                            modal.find("#vacantes-empresas").val(result.num_vacant);
                            modal.find("#apoyos-empresas").val(result.num_apoyos);
                            modal.find("#tipo-empresas").val(result.tipo_apoyo);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-empresas").animate({height:"toggle"}, "slow", function(){
                                modal.find("#op-up-empresas").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "actividades":
                            modal.find("#tipo-actividades").val(result.tipo_servi);
                            modal.find("#localidad-actividades").val(result.localidad);
                            modal.find("#numero-actividades").val(result.num_servic);
                            modal.find("#fecha-actividades").val(result.fecha);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-actividades").animate({height:"toggle"}, "slow", function(){
                                modal.find("#op-up-actividades").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "consume":
                            modal.find("#nombre-consume").val(result.nombre);
                            modal.find("#tipo-consume").val(result.tipo);
                            modal.find("#numero-consume").val(result.empleados);
                            modal.find("#localidad-consume").val(result.localidad);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-consume").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-consume").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "violencia":
                            modal.find("#tipo-violencia").val(result.tipo_atenc);
                            modal.find("#nombre-violencia").val(result.nom_ager);
                            modal.find("#direccion-violencia").val(result.direccion);
                            modal.find("#localidad-violencia").val(result.localidad);
                            modal.find("#numero-violencia").val(result.num_agredi);
                            modal.find("#reincidencia-violencia").val(result.reincident);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-violencia").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-violencia").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "aeconomicos":
                            modal.find("#tipo-aeconomicos").val(result.apoyo);
                            modal.find("#cantidad-aeconomicos").val(result.cantidad);
                            modal.find("#name-aeconomicos").val(result.nombre);
                            modal.find("#curp-aeconomicos").val(result.curp);
                            modal.find("#address-aeconomicos").val(result.calle);
                            modal.find("#col-aeconomicos").val(result.colonia);
                            modal.find("#localidad-aeconomicos").val(result.localidad);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-aeconomicos").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-aeconomicos").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "dosificadores":
                            $("#modal-add-dosificador").find("#dosificador-id").val(result["data"].id);
                            modal.find("#numero-dosificadores").val(result["data"].num_identi);
                            modal.find("#localidad-dosificadores").val(result["data"].localidad);
                            table2.clear().draw();
                            result["items"].forEach(function(item){
                                table2.row.add([item.fecha, item.tipo, item.observaciones]).draw( false );
                            });
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-dosificadores").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-dosificadores").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "orservicios":
                            modal.find("#tipo-orservicios").val(result.tipo_serv);
                            modal.find("#date-orservicios").val(result.fecha);
                            modal.find("#direccion-orservicios").val(result.direccion);
                            modal.find("#localidad-orservicios").val(result.localidad);
                            modal.find("#observation-orservicios").val(result.observacio);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-orservicios").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-orservicios").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "comites":
                            modal.find("#represent-comites").val(result.representante);
                            modal.find("#localidad-comites").val(result.localidad);
                            modal.find("#reports-comites").val(result.no_reportes);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-comites").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-comites").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "consulta":
                            modal.find("#nombre-consulta").val(result.nombre);
                            modal.find("#edad-consulta").val(result.edad);
                            modal.find("#sex-consulta").val(result.sexo);
                            modal.find("#localidad-consulta").val(result.localidad);
                            modal.find("#sintomas-consulta").val(result.sintomas);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-consulta").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-consulta").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "traslados":
                            modal.find("#sex-traslados").val(result.sexo);
                            modal.find("#ocupacion-traslados").val(result.ocupacion);
                            modal.find("#derecho-traslados").val(result.derechoabiente);
                            modal.find("#lugar-traslados").val(result.lugar);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-traslados").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-traslados").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "comedores":
                            $("#modal-add-porciones").find("#comedor-id").val(result["data"].id);
                            modal.find("#nombre-comedores").val(result["data"].nombre);
                            modal.find("#localidad-comedores").val(result["data"].localidad);
                            table4.clear().draw();
                            result["items"].forEach(function(item){
                                table4.row.add([item.fecha, item.porciones]).draw();
                            });
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-comedores").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-comedores").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "apoyos":
                            modal.find("#tipo-apoyo").val(result.tipo);
                            modal.find("#name-apoyos").val(result.nombre);
                            modal.find("#curp-apoyos").val(result.curp);
                            modal.find("#ine-apoyos").val(result.ine);
                            modal.find("#address-apoyos").val(result.domicilio);
                            modal.find("#localidad-apoyos").val(result.localidad);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-apoyos").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-apoyos").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "caretas":
                            modal.find("#name-caretas").val(result.nombre);
                            modal.find("#tipo-caretas").val(result.tipo);
                            modal.find("#localidad-caretas").val(result.localidad);
                            modal.find("#numero-caretas").val(result.no_caretas);
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-caretas").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-caretas").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                        case "direcciones":
                            $("#modal-add-direction-report").find("#direction-id").val(result["data"].id);
                            modal.find("#name-direcciones").val(result["data"].name);
                            table5.clear().draw();
                            result["items"].forEach(function(item){
                                table5.row.add([item.fecha, item.unidad, item.cantidad, item.costo]).draw( false );
                            });
                            modal.find("#btn-update-marker").removeAttr("disabled");
                            modal.find("#op-up-direcciones").animate({height:"toggle"}, "fast", function(){
                                modal.find("#op-up-direcciones").addClass("panel-update");
                                modal.modal("show");
                            });
                            break;
                    }
                });
            });

        var sidebarData = "";
        var items = 0;
        var marker = {};
        var map = L.map('map',{scrollWheelZoom:true, minZoom: 12}).setView([20.74689, -105.39425], 12);

        var sidebar = L.control.sidebar('sidebar-leaflet', {
            closeButton: true,
            position: 'right',
        });
        map.addControl(sidebar);

        // map.on('click', function () {
        //     sidebar.hide();
        // });
        sidebar.on('hide', function () {
            sidebarData = "";
            sidebar.setContent("");
            for(var mark in marker)
            {
                map.removeLayer(marker[mark]);
            }
            clicked = true;
        });

        var customControl =  L.Control.extend({
            options: {
                position: 'topleft'
            },
            onAdd: function (map) {
                var container = L.DomUtil.create('div', 'leaflet-pm-toolbar leaflet-pm-draw leaflet-bar leaflet-control');
                var btnContainer = L.DomUtil.create('div', 'button-container btn-info-container');
                var leafletControlBtn = L.DomUtil.create('a', 'leaflet-buttons-control-button btn-info-link');
                // var icon = L.DomUtil.create('div', 'control-icon leaflet-pm-icon-info');
                var icon = L.DomUtil.create('i', 'fas fa-file-pdf');
                var leafletControlAction = L.DomUtil.create('div', 'leaflet-pm-actions-container');
                var actionCancel = L.DomUtil.create('div', 'leaflet-pm-action action-cancel');
                var txt = document.createTextNode("Cancelar");
                actionCancel.appendChild(txt);
                leafletControlAction.appendChild(actionCancel);
                leafletControlBtn.appendChild(icon);
                btnContainer.appendChild(leafletControlBtn);
                btnContainer.appendChild(leafletControlAction);
                container.appendChild(btnContainer);
            
                container.onclick = function(){
                    window.location.href = "{{ route('geo.view.report') }}";
                }
            
                return container;
            }
        });
        map.addControl(new customControl());

        var ortofoto = L.tileLayer(cartoIp + '/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:Vuelo_Bajo_Ortofoto@EPSG:900913@png/{z}/{x}/{y}.png', {
            tms: true,
            minZoom: 12,
            maxZoom: 23,
            attribution: '&copy; IMPLAN - Ortofoto Bahia de Banderas 2019'
        });

        var street = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);
        var baseMaps = {
            "<i class='fas fa-map mr-1 ml-1'></i> OpenStreetMap": street,
            "<i class='fas fa-plane mr-1 ml-1'></i> Ortofoto BadeBa": ortofoto
        };

        var covid = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:dse_casos_covid",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Casos Covid 19",
            id: "covid",
            fields: "nombre,localidad",
            fields_pretty: "Nombre,Localidad",
            version: '1.3.0',//wms version (ver get capabilities)
        });

        var beneficiarios = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:dse_beneficiarios_programa_alimenticio",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Beneficiarios",
            id: "beneficiarios",
            fields: "folio,apellido_p,localidad",
            fields_pretty: "Folio,Nombre,Localidad",
            version: '1.3.0',//wms version (ver get capabilities)
        });

        var tiendas = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:dse_tiendas_participantes",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Tiendas Participantes",
            id: "tiendas",
            fields: "licencia,nombre,propietari",
            fields_pretty: "Licencia,Nombre,Propietario",
            version: '1.3.0',//wms version (ver get capabilities)
        });

        var concentracion = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:sp_concentracion_personas",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Concentración de Personas",
            id: "concentracion",
            fields: "tipo,motivo",
            fields_pretty: "Tipo,Motivo",
            version: '1.3.0',//wms version (ver get capabilities)
        });
        
        var delitos = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:sp_delitos",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Delitos",
            id: "delitos",
            fields: "reporte,tipo_delit,localidad",
            fields_pretty: "Reporte,Tipo Delito,Localidad",
            version: '1.3.0',//wms version (ver get capabilities)
        });

        var transito = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:tran_incidentes",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Incidentes de Transito",
            id: "transito",
            fields: "folio,tipo_incid,localidad",
            fields_pretty: "Folio,Tipo Incidente,Localidad",
            version: '1.3.0',//wms version (ver get capabilities)
        });
        
        var filtros = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:dse_filtros_sanitarios",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Filtros Sanitarios",
            id: "filtros",
            fields: "filtro_loc,personal",
            fields_pretty: "Localidad,Personal",
            version: '1.3.0',//wms version (ver get capabilities)
        });
        
        var hospitales = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:dse_hospitales",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Hospitales",
            id: "hospitales",
            fields: "nom_estab,nombre_act",
            fields_pretty: "Nombre,Actividad",
            version: '1.3.0',//wms version (ver get capabilities)
        });
        
        var cementerios = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:obras_cementerios",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Cementerios",
            id: "cementerios",
            fields: "nombre,espacio_di",
            fields_pretty: "Nombre,Espacios Disp.",
            version: '1.3.0',//wms version (ver get capabilities)
        });

        var empresas = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:de_empresas",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Empresas",
            id: "empresas",
            fields: "nombre,num_trabaj,n_despidos",
            fields_pretty: "Nombre,No. Trabajadores,No. Despidos",
            version: '1.3.0',//wms version (ver get capabilities)
        });

        var actividades = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:ser_acciones",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Actividades Servicios Públicos",
            id: "actividades",
            fields: "tipo_servi,num_servic",
            fields_pretty: "Tipo de Servicio,No. Servicios",
            version: '1.3.0',//wms version (ver get capabilities)
        });
        
        var consume = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:de_consume_local",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Consume lo Local",
            id: "consume",
            fields: "nombre,tipo",
            fields_pretty: "Nombre,Tipo Negocio",
            version: '1.3.0',//wms version (ver get capabilities)
        });

        var vilencia = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:dse_violencia",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Casos de Violencia",
            id: "violencia",
            fields: "tipo_atenc,tipo",
            fields_pretty: "Tipo de Atención",
            version: '1.3.0',//wms version (ver get capabilities)
        });

        var dosificadores = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:oromapas_dosificadores",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Dosificadores",
            id: "dosificadores",
            fields: "num_identi",
            fields_pretty: "Identificador",
            version: '1.3.0',//wms version (ver get capabilities)
        });
        
        var orservicios = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:oromapas_servicios",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Servicios",
            id: "orservicios",
            fields: "fecha,tipo_serv",
            fields_pretty: "Fecha,Tipo Servicio",
            version: '1.3.0',//wms version (ver get capabilities)
        });

        var comites = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:dse_comites_salud",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Comites de Salud",
            id: "comites",
            fields: "representante",
            fields_pretty: "Representante",
            version: '1.3.0',//wms version (ver get capabilities)
        });
        
        var consulta = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:dse_consultas_medicas",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Consultas Medicas",
            id: "consulta",
            fields: "nombre",
            fields_pretty: "Nombre",
            version: '1.3.0',//wms version (ver get capabilities)
        });
        
        var traslados = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:pc_traslados",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Traslados de Pacientes",
            id: "traslados",
            fields: "ocupacion,lugar",
            fields_pretty: "Ocupación,Lugar",
            version: '1.3.0',//wms version (ver get capabilities)
        });

        var caretas = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:pl_caretas",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Caretas a Negocios",
            id: "caretas",
            fields: "nombre,tipo,no_caretas",
            fields_pretty: "Nombre,Tipo,No. Caretas",
            version: '1.3.0',//wms version (ver get capabilities)
        });
        
        var comedores = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:dif_comedores",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Comedores Solidarios",
            id: "comedores",
            fields: "nombre",
            fields_pretty: "Nombre",
            version: '1.3.0',//wms version (ver get capabilities)
        });
       
        var apoyos = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:dif_apoyos",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Apoyos DIF",
            id: "apoyos",
            fields: "nombre,tipo",
            fields_pretty: "Nombre,Tipo",
            version: '1.3.0',//wms version (ver get capabilities)
        });

        var direcciones = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:gen_direcciones",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Direcciones Ayuntamiento",
            id: "direcciones",
            fields: "name",
            fields_pretty: "Nombre",
            version: '1.3.0',//wms version (ver get capabilities)
        });
        var aeconomicos = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
            layers: "OI.BahiaBanderas:dif_apoyos_economicos",//nombre de la capa (ver get capabilities)
            format: 'image/png',
            transparent: true,
            minZoom: 12,
            maxZoom: 23,
            title: "Apoyos Económicos DIF",
            id: "aeconomicos",
            fields: "nombre,apoyo,cantidad",
            fields_pretty: "Nombre,Apoyo,Cantidad",
            version: '1.3.0',//wms version (ver get capabilities)
        });

        var overlayMaps = {
            "Generales": {
                "<div class='leaflet-circle' style='background-color: #93fdba;'></div>Filtros Sanitarios": filtros,
                "<div class='leaflet-circle' style='background-color: #2196f3;'></div>Direcciones Ayuntamiento": direcciones,
            },
            "Desarrollo y Bienestar Social":{
                "<div class='leaflet-circle' style='background-color: #db2ca4;'></div>Casos Covid": covid,
                "<div class='leaflet-circle' style='background-color: #ec407a;'></div>Hospitales": hospitales,
                "<div class='leaflet-circle' style='background-color: #d4e157;'></div>Comites de Salud": comites,
                "<div class='leaflet-circle' style='background-color: #ffee58;'></div>Consultas Medicas": consulta,
                "<div class='leaflet-circle' style='background-color: #216608;'></div>Beneficiarios Despensas": beneficiarios,
                "<div class='leaflet-circle' style='background-color: #e91e63;'></div>Casos de Violencia": vilencia,
            },
            "Padrón y Licencias":{
                "<div class='leaflet-circle' style='background-color: #1f78b4;'></div>Tiendas Participantes": tiendas,
                "<div class='leaflet-circle' style='background-color: #aed581;'></div>Caretas a Negocios": caretas,
            },
            "Seguridad Pública":{
                "<div class='leaflet-circle' style='background-color: #db1e2a;'></div>Siniestro Seguridad": delitos,
                "<div class='leaflet-circle' style='background-color: #f7801e;'></div>Concentracion de Personas": concentracion,
            },
            "Transito":{
                "<div class='leaflet-circle' style='background-color: #912d6e;'></div>Incidentes de Transito": transito,
            },
            "Obras Públicas": {
                "<div class='leaflet-circle' style='background-color: #000000;'></div>Cementerios": cementerios,
            },
            "Desarrollo Económico": {
                "<div class='leaflet-circle' style='background-color: #9c27b0;'></div>Empresas": empresas,
                "<div class='leaflet-circle' style='background-color: #26c6da;'></div>Consume lo Local": consume,
            },
            "Servicios Públicos": {
                "<div class='leaflet-circle' style='background-color: #7e57c2;'></div>Actividades Servico Públicos": actividades,
            },
            "Oromapas": {
                "<div class='leaflet-circle' style='background-color: #448aff;'></div>Dosificadores": dosificadores,
                "<div class='leaflet-circle' style='background-color: #4caf50;'></div>Servicios": orservicios,
            },
            "Protección Civil": {
                "<div class='leaflet-circle' style='background-color: #ffa000;'></div>Traslados de Pacientes": traslados,
            },
            "DIF": {
                "<div class='leaflet-circle' style='background-color: #8d6e63;'></div>Comedores Solidarios": comedores,
                "<div class='leaflet-circle' style='background-color: #bf360c;'></div>Apoyos DIF": apoyos,
                "<div class='leaflet-circle' style='background-color: #ce93d8;'></div>Apoyos Económicos": aeconomicos,
            }
        };
        var options2 = {collapsed: false, groupsCollapsable: true, groupsExpandedClass: "fas fa-angle-down", groupsCollapsedClass: "fas fa-angle-right"};
        
        layerControl = L.control.groupedLayers(baseMaps, overlayMaps, options2).addTo(map);

        map.addEventListener('click', function(e){ onMapClick(e);});
        
        function onMapClick(e){
            console.log(sidebar.isVisible());
            if(!sidebar.isVisible())
            {
                var latlngStr = '('+ e.latlng.lat.toFixed(3) + ", " + e.latlng.lng.toFixed(3)+')';
                var modal = $("#modal-add-marker");
                modal.find("#latitud").val(e.latlng.lat);
                modal.find("#longitud").val(e.latlng.lng);
                modal.find("#x").val(e.latlng.utm().x);
                modal.find("#y").val(e.latlng.utm().y);
                var BBOX = map.getBounds()._southWest.lng + ", " + map.getBounds()._southWest.lat + ", " + map.getBounds()._northEast.lng + ", " + map.getBounds()._northEast.lat;
                var WIDTH = map.getSize().x;
                var HEIGHT = map.getSize().y;   
                var X = map.layerPointToContainerPoint(e.layerPoint).x.toFixed(0);
                var Y = map.layerPointToContainerPoint(e.layerPoint).y.toFixed(0);
            
                var layers = e.sourceTarget._layers;
                // console.log(e.latlng.utm());
                for(var layer in layers)
                {
                    if(layers[layer].options.layers)
                    {
                        URL = cartoIp+'/geoserver/BahiaBanderas/wms?request=GetFeatureInfo&service=WMS&version=1.1.1&info_format=application/json&exceptions=application/vnd.ogc.se_xml&layers=OI.'+layers[layer].options.layers+'&styles=&srs=EPSG%3A4326&format=image%2Fpng&bbox='+ BBOX + '&width='+ WIDTH +'&height='+ HEIGHT + '&query_layers=OI.'+layers[layer].options.layers+'&feature_count=1000&x='+ X +'&y='+ Y;
                        $.ajax({
                            type: 'POST',
                            async: false,
                            url: URL,
                            dataType : 'json',
                            success: function (response) {
                                if(response.features.length > 0)
                                {
                                    // console.log(response);
    
                                    items++;
                                    marker[(response.features[0].id.split("."))[0]] = L.geoJson(response, {
                                        onEachFeature: onEachFeatureClosure(layers[layer].options.id, layers[layer].options.title, layers[layer].options.fields, layers[layer].options.fields_pretty, items, response.features.length)
                                    }).addTo(map);
                                    sidebarData += '</tbody></table></div></div></div></div></div>';
                                }
                            },
                            error: function (req, status, error) {
                                // console.log(error);
                            }
                        });
                    }
                }
                if(!sidebarData)
                {
                    marker["marker"] = L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
                    sidebarData += "<div class='row justify-content-end mb-2'>\
                                            <div class='col-12'>\
                                                <button type='button' data-toggle='modal' data-target='#modal-add-marker' class='btn btn-primary'><i class='fas fa-plus-circle ml-2'></i> Nuevo Registro</button>\
                                            </div>\
                                        </div>";
                }
                sidebar.setContent(sidebarData);
                sidebar.show();
                clicked = false;
                function onEachFeatureClosure(id, layer_name, fields, fields_pretty, item, count) {
                    if(item == 1)
                    {
                        sidebarData += "<div class='row justify-content-end mb-2'>\
                                            <div class='col-12'>\
                                                <button type='button' data-toggle='modal' data-target='#modal-add-marker' class='btn btn-primary'><i class='fas fa-plus-circle ml-2'></i> Nuevo Registro</button>\
                                            </div>\
                                        </div>";
                    }   
                    var fields2 = fields.split(",");
                    var fields_pretty2 = fields_pretty.split(",");
                    var ths = "";
                    fields_pretty2.forEach(element => {
                        ths += "<th scope='row'>"+element+"</th>";
                    });
                    sidebarData += '<div class="row">\
                                        <div class="accordion col-12" id="parent-'+id+'">\
                                            <div class="card">\
                                                <div class="card-header">\
                                                    <h2 class="mb-0">\
                                                        <button class="btn btn-link collapsed btn-collapse" type="button" data-toggle="collapse" data-target="#'+id+'" aria-expanded="true">'+layer_name+'( '+count+' )</button>\
                                                    </h2>\
                                                </div>\
                                                <div id="'+id+'" class="collapse" data-parent="#parent-'+id+'">\
                                                    <div class="card-body">\
                                                        <table class="table table-sm">\
                                                            <thead>\
                                                                <tr>\
                                                                    '+ths+'\
                                                                    <th scope="row" style="width:15%;">Acciones</th>\
                                                                </tr>\
                                                            </thead>\
                                                            <tbody>';
                    return function onEachFeature(feature, layer) {
                        console.log(feature);

                        if (feature.properties) {
                            sidebarData += '<tr>';
                            fields2.forEach(element => {
                                sidebarData += "<td>"+feature.properties[element]+"</td>";
                            });
                            sidebarData += '<td><button type="button" data-id="'+feature.id+'" data-operation="'+id+'" class="btn btn-primary btn-view-data"><i class="fas fa-eye"></i></button></td>';
                            sidebarData += '</tr>';
                        }
                    }
                }
                items = 0;
            }
        }
    });
    </script>
@endsection