@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
    <div class="text-center mb-3">
        <h3>Actividades del Programa</h3>
        <h4>( {{ $program->actividad }} )</h4>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary text-center" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    <div class="row justify-content-end">
        <a href="{{ route('new.action.component', $program->id) }}" class="btn btn-primary mr-3 mb-2"><i class="fas fa-plus-circle mr-2"></i> Agregar Actividad</a>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <table class="table dataTable">
                <thead>
                    <tr>
                        <th scope="col">Actividad</th>
                        <th scope="col">Meta Anual</th>
                        <th scope="col">1° Trimeste</th>
                        <th scope="col">2° Trimeste</th>
                        <th scope="col">3° Trimeste</th>
                        <th scope="col">4° Trimeste</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody id="actions-poa">
                    @foreach ($program->actions as $action)
                        <tr data-toggle="collapse" data-target="#collapse{{ $action->id }}" data-id="{{ $action->id }}">
                            <td class="col-6">{{ $action->actividad }}</td>
                            <td class="col-1">{{ $action->meta_anual }}</td>
                            <td class="col-1">{{ $action->trimestre_1 }}</td>
                            <td class="col-1">{{ $action->trimestre_2 }}</td>
                            <td class="col-1">{{ $action->trimestre_3 }}</td>
                            <td class="col-1">{{ $action->trimestre_4 }}</td>
                            <td style="width: 10%">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="btn-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Acciones
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btn-action">
                                        <a class="dropdown-item" href="{{ route('new.edit.action.component', $action->id) }}" >Editar Actividad <i class="fas fa-edit ml-2"></i></a>
                                        <a class="dropdown-item btn-delete-action" data-action="{{ $action->id }}" href="#">Eliminar Actividad <i class="fas fa-trash ml-2"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
@endsection

@section('modals')

@endsection

@section('js')
<script>
    $(function(){
        $(document).on("click", ".btn-delete-action", function(){
            var action = $(this).data("action"),
                token = "{{ csrf_token() }}";
    
            var _this = $(this);
            $.confirm({
                title: '¿ Estas Seguro (a) ?',
                content: 'Se eliminara la actividad.',
                buttons: {
                    Confirmar: {
                        text: 'Confirmar',
                        btnClass: 'btn btn-primary btn-confirm',
                        action: function(){
                            $.ajax({
                                url: "{{ route('new.delete.action.component') }}",
                                type: "POST",
                                data: {
                                    _token: token,
                                    action: action
                                },
                            }).done(function(result){
                                console.log(result);
                                $(".son"+action).fadeOut("slow");
                                _this.parents("tr").fadeOut("slow");
                            });
                        }
                    },
                    Cancelar: {
                        text: 'Cancelar',
                        btnClass: 'btn btn-secondary btn-confirm',
                    }
                }
            });
        });
    });
    </script>
@endsection