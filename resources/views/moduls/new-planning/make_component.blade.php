@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
    <div class=" mb-3 text-center">
        <h3>Matriz de Indicadores ( Periodo 2021 )</h3>
        <h4>( {{ $area->nombre }} - {{ $direction->abreviacion }} )</h4>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary text-center" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    <div class="row justify-content-end">
        {{-- <a href="{{ route('generate.my.poa') }}" class="btn btn-primary mr-3 mb-2" target="_BLANK"><i class="fas fa-file-pdf mr-2"></i> Previsualizar Asignaciones</a> --}}
        <button type="button" class="btn btn-primary mr-3 mb-2" data-toggle="modal" data-target="#modal-new-componenent">
            <i class="fas fa-plus-circle mr-2"></i> Generar Componente
        </button>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <table class="table dataTable">
                <thead>
                    <tr>
                        <th scope="col">Componente</th>
                        <th scope="col">Objetivo</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody id="actions-poa">
                    @foreach ($area->components()->where('periodo', 2021)->get() as $component)
                        <tr class="edit-component" data-toggle="collapse" data-target="#collapse{{ $component->id }}">
                            <td>{{ $component->actividad }}</td>
                            <td>{{ $component->objetivo }}</td>
                            <td style="width: 10%">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Acciones
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a href="#!" class="dropdown-item btn-update-component" data-component="{{ $component->id }}">
                                            Editar Componente <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('new.update.component', $component->id) }}" class="dropdown-item">
                                            Gestionar Actividades <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="#!" class="dropdown-item btn-delete-component" data-component="{{ $component->id }}">
                                            Eliminar <i class="fas fa-trash-alt"></i>
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
@endsection

@section('modals')
    @include('partials.modals.new-planning.modal_new_component')
    @include('partials.modals.new-planning.modal_update_component')
@endsection

@section('js')
<script>
$(function(){
    $(document).on("click", ".btn-delete-component", function(){
        var component = $(this).data("component"),
            token = "{{ csrf_token() }}";

        var _this = $(this);
        $.confirm({
            title: '¿ Estas Seguro (a) ?',
            content: 'Se eliminara el programa y todas las actividades que lo compongan.',
            buttons: {
                Confirmar: {
                    text: 'Confirmar',
                    btnClass: 'btn btn-primary btn-confirm',
                    action: function(){
                        $.ajax({
                            url: "{{ route('new.delete.component') }}",
                            type: "POST",
                            data: {
                                _token: token,
                                component: component
                            },
                        }).done(function(result){
                            _this.parents("tr").fadeOut("slow");
                        });
                    }
                },
                Cancelar: {
                    text: 'Cancelar',
                    btnClass: 'btn btn-secondary btn-confirm',
                }
            }
        });
    });
    $(document).on("click", ".btn-update-component", function(){
        var _this = $(this);
        var component = $(this).data("component");

        $.ajax({
            url: "{{ route('get.component.by.id') }}",
            type: "POST",
            data: {
                _token: "{{ csrf_token() }}",
                component: component
            },
        }).done(function(result){
            console.log(result)
            var modal = $("#modal-update-componenent")
            modal.find("#normatives-content-update").empty();
            result.pm_action_lines.forEach(element => {
                    modal.find("#normatives-content-update").append('<tr>\
                            <td>'+ element.punto + " " +element.nombre +' <input type="hidden" name="pm_normatives[]" value="'+ element.id +'" /> </td>\
                            <td><button type="button" class="btn btn-danger btn-delete-normative-update"><i class="fas fa-trash-alt"></i></button></td>\
                        </tr>'
                    );
            });
            result.inf_action_lines.forEach(element => {
                    modal.find("#normatives-content-update").append('<tr>\
                            <td>'+ element.punto + " " +element.nombre +' <input type="hidden" name="inf_normatives[]" value="'+ element.id +'" /> </td>\
                            <td><button type="button" class="btn btn-danger btn-delete-normative-update"><i class="fas fa-trash-alt"></i></button></td>\
                        </tr>'
                    );
            });
            result.sche_metas.forEach(element => {
                    modal.find("#normatives-content-update").append('<tr>\
                            <td>'+ element.punto + " " +element.nombre +' <input type="hidden" name="sche_normatives[]" value="'+ element.id +'" /> </td>\
                            <td><button type="button" class="btn btn-danger btn-delete-normative-update"><i class="fas fa-trash-alt"></i></button></td>\
                        </tr>'
                    );
            });

            modal.find("select[name=responsable]").val(result.puesto_id)
            modal.find("select[name=type]").val(result.orientacion)
            modal.find("select[name=value]").val(result.valor)
            modal.find("select[name=periodo]").val(result.periodo_verificacion)
            modal.find("select[name=dimension]").val(result.dimension)

            modal.find("textarea[name=activity]").val(result.actividad)
            modal.find("input[name=unity]").val(result.unidad_medida)
            modal.find("input[name=beneficiarios]").val(result.beneficiarios)
            modal.find("input[name=year]").val(result.meta_anual)
            modal.find("input[name=first]").val(result.trimestre_1)
            modal.find("input[name=second]").val(result.trimestre_2)
            modal.find("input[name=thirth]").val(result.trimestre_3)
            modal.find("input[name=four]").val(result.trimestre_4)
            modal.find("input[name=formula]").val(result.formula)
            modal.find("textarea[name=objetivo]").val(result.objetivo)

            modal.find("input[name=component_id]").val(result.id)
            modal.find("input[name=validate_normative_update]").val(1)
            
            modal.find(".select2").select2();
            var flag = false;
            modal.find(".settings-update").each(function(i){
                if($(this).val() == 1)
                {
                    flag = true;
                }
            });
            if(flag)
            {
                modal.find(".trimounth").each(function(i){
                    $(this).removeClass("trimounth");
                });
                
                modal.find(".total").each(function(i){
                    $(this).removeAttr("readonly");
                });
            }else
            {
                modal.find(".tri-inp").each(function(i){
                    $(this).addClass("trimounth");
                });
                
                modal.find(".total").each(function(i){
                    $(this).attr("readonly", true);
                });
            }

            modal.modal("show");
        });
                    
    });
});
</script>
@endsection