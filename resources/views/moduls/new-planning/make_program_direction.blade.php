@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
    <div class=" mb-3 text-center">
        <h3>Área o Proyecto Específico ( Periodo {{ config('implan.periods.previous') }} )</h3>
        <h4>( {{ $direction->nombre }} )</h4>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary text-center" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    <div class="row justify-content-end">
        {{-- <a href="{{ route('generate.my.poa') }}" class="btn btn-primary mr-3 mb-2" target="_BLANK"><i class="fas fa-file-pdf mr-2"></i> Previsualizar Asignaciones</a> --}}
        <button type="button" class="btn btn-primary mr-3 mb-2" data-toggle="modal" data-target="#modal-new-program">
            <i class="fas fa-plus-circle mr-2"></i> Generar Proyecto
        </button>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <table class="table dataTable">
                <thead>
                    <tr>
                        <th scope="col">Área o Proyecto Específico</th>
                        <th scope="col">Objetivo</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody id="actions-poa">
                    @foreach ($direction->programs()->where("period", config('implan.periods.previous'))->get() as $program)
                        <tr class="edit-program" data-toggle="collapse" data-target="#collapse{{ $program->id }}">
                            <td>{{ $program->nombre }}</td>
                            <td>{{ $program->descripcion }}</td>
                            <td>
                                <a href="{{ route('update.program', $program->id) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Editar">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <button type="button" class="btn btn-danger btn-delete-program" data-program="{{ $program->id }}" data-toggle="tooltip" data-placement="top" title="Eliminar">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
                        <tr class="no-hover son{{ $program->id }}" >
                            <td colspan="6" style="padding:initial;">
                                <div id="collapse{{ $program->id }}" class="collapse ml-5" >
                                    <table class="table dataTable">
                                        <thead>
                                            <tr>
                                                <th scope="col">Actividad</th>
                                                <th scope="col">Meta Anual</th>
                                                <th scope="col">1° Trimeste</th>
                                                <th scope="col">2° Trimeste</th>
                                                <th scope="col">3° Trimeste</th>
                                                <th scope="col">4° Trimeste</th>
                                            </tr>
                                        </thead>
                                        <tbody id="actions-poa">
                                            @if (count($program->actions) > 0)
                                                @foreach ($program->actions as $action)
                                                    <tr data-toggle="collapse" data-target="#collapse-action{{ $action->id }}">
                                                        <td class="col-6">{{ $action->actividad }}</td>
                                                        <td class="col-1">{{ $action->meta_anual }}</td>
                                                        <td class="col-1">{{ $action->trimestre_1 }}</td>
                                                        <td class="col-1">{{ $action->trimestre_2 }}</td>
                                                        <td class="col-1">{{ $action->trimestre_3 }}</td>
                                                        <td class="col-1">{{ $action->trimestre_4 }}</td>
                                                    </tr>
                                                    <tr class="no-hover">
                                                        <td colspan="6" style="padding:initial;">
                                                            <div id="collapse-action{{ $action->id }}" class="collapse ml-5" >
                                                                <table class="table">
                                                                    <tr class="no-hover">
                                                                        <th scope="col">Subactividades</th>
                                                                        <th scope="col">Meta Anual</th>
                                                                        <th scope="col">1° Trimeste</th>
                                                                        <th scope="col">2° Trimeste</th>
                                                                        <th scope="col">3° Trimeste</th>
                                                                        <th scope="col">4° Trimeste</th>
                                                                    </tr>
                                                                    @if (count($action->subactions) > 0)
                                                                        @foreach ($action->subactions as $subaction)
                                                                            <tr>
                                                                                <td class="col-6">{{ $subaction->actividad }}</td>
                                                                                <td class="col-1">{{ $subaction->meta_anual }}</td>
                                                                                <td class="col-1">{{ $subaction->trimestre_1 }}</td>
                                                                                <td class="col-1">{{ $subaction->trimestre_2 }}</td>
                                                                                <td class="col-1">{{ $subaction->trimestre_3 }}</td>
                                                                                <td class="col-1">{{ $subaction->trimestre_4 }}</td>
                                                                            </tr>
                                                                        @endforeach
                                                                    @else
                                                                        <tr>
                                                                            <td colspan="6" class="text-center">Ninguna Subactividad Registrada</td>
                                                                        </tr>
                                                                    @endif
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6" class="text-center">Ninguna Actividad Registrada</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
@endsection

@section('modals')
    @include('partials.modals.planning.modal_new_program')
@endsection

@section('js')
<script>
$(function(){
    $(document).on("click", ".btn-delete-program", function(){
        var program = $(this).data("program"),
            token = "{{ csrf_token() }}";

        var _this = $(this);
        $.confirm({
            title: '¿ Estas Seguro (a) ?',
            content: 'Se eliminara el programa y todas las actividades que lo compongan.',
            buttons: {
                Confirmar: {
                    text: 'Confirmar',
                    btnClass: 'btn btn-primary btn-confirm',
                    action: function(){
                        $.ajax({
                            url: "{{ route('delete.program') }}",
                            type: "POST",
                            data: {
                                _token: token,
                                program: program
                            },
                        }).done(function(result){
                            console.log(result);
                            $(".son"+program).fadeOut("slow");
                            _this.parents("tr").fadeOut("slow");
                        });
                    }
                },
                Cancelar: {
                    text: 'Cancelar',
                    btnClass: 'btn btn-secondary btn-confirm',
                }
            }
        });
    });
});
</script>
@endsection