@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class="row justify-content-center mb-3">
    <h3>Diseño de Cabecera de MIR</h3>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif
<div class="row justify-content-end">
    {{-- <a href="{{ route('generate.my.poa') }}" class="btn btn-primary mr-3 mb-2" target="_BLANK"><i class="fas fa-file-pdf mr-2"></i> Previsualizar Asignaciones</a> --}}
    <button type="button" class="btn btn-primary mr-3 mb-2" data-toggle="modal" data-target="#modal-new-program">
        <i class="fas fa-plus-circle mr-2"></i> Nuevo Programa
    </button>
</div>
<div class="row mt-3">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table dataTable table-sm">
                <thead>
                    <tr>
                        <th scope="col">Clave</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Presupesto</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($programs as $program)
                        <tr>
                            <td>{{ $program->clave }}</td>
                            <td>{{ $program->nombre }}</td>
                            <td>$ {{ number_format($program->presupuesto,2) }}</td>
                            <td style="width: 5%">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="btn-action" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Acciones
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="btn-action">
                                        <a class="dropdown-item btn-update-program" href="#" data-id="{{ $program->id }}">Editar Programa <i class="fas fa-edit ml-2"></i></a>
                                        {{-- <a class="dropdown-item" href="#">Eliminar Programa <i class="fas fa-trash ml-2"></i></a> --}}
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection

@section('modals')
    @include('partials.modals.new-planning.modal_new_program')
    @include('partials.modals.new-planning.modal_new_update_program')
@endsection

@section('js')
    <script>
        $(function(){
            $(document).on("click", ".btn-update-program", function(){
                let id = $(this).data("id");
                $.ajax({
                    url: "{{ route('new.get.program.mir') }}",
                    type: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id
                    },
                }).done(function(result){
                    let modal = $("#modal-new-update-program");
                    modal.find("[name='id']").val(result.id);
                    modal.find("[name='name']").val(result.nombre);
                    modal.find("[name='objective_fin']").val(result.objetivo_fin);
                    modal.find("[name='objective_prop']").val(result.objetivo_prop);
                    modal.find("[name='clave']").val(result.clave);
                    modal.find("[name='presupuesto']").val(result.presupuesto);
                    modal.modal("show");
                });
            });
        });
        function refreshObjectivePmd(id_eje, id_objetivo, modal){
            $.ajax({
                url: "{{ route('new.get.pmd.objectives') }}",
                type: "POST",
                data: {
                    _token: "{{ csrf_token() }}",
                    id: id_eje
                },
            }).done(function(result){
                result.forEach(item => {
                    modal.find("[name='pmd_objective']").append($('<option>', { 
                        value: item.id,
                        text : item.punto + ". " + item.nombre
                    }));
                });
                modal.find("[name='pmd_objective']").val(id_objetivo);
            });
        }
    </script>
@endsection