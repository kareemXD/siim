@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class="row justify-content-center mb-3">
    <h3>Asignación de la Agenda 2030</h3>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif
<div class="row justify-content-center">
    <form action="{{ route('new.assign.schedule') }}" method="POST" class="col-12 col-xl-8">
        @csrf
        @if (config('implan.times.planning'))
            <div class="form-group row">
                <label for="action" class="col-md-5 col-form-label text-md-right">Acción a Realizar</label>
                <div class="col-md-6 ">
                    <select class="form-control {{ $errors->has('direction') ? ' is-invalid' : '' }}" id="action">
                        <option value="1" selected>Asignar Indicadores</option>
                        <option value="2">Cambiar Estatus a Indicador</option>
                    </select>
                </div>
            </div>
        @endif
        <div class="form-group row direction-combo">
            <label for="direction" class="col-md-5 col-form-label text-md-right">Seleccione una Dirección</label>
            <div class="col-md-6 ">
                <select class="form-control select2 {{ $errors->has('direction') ? ' is-invalid' : '' }}" name="direction" required>
                    <option value="" selected>Seleccione un rol</option>
                    @foreach ($directions as $direction)
                        <option value="{{ $direction->id }}">{{ $direction->nombre }}</option>
                    @endforeach
                </select>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        @if ($errors->has('password'))
            <div class="alert alert-danger text-center" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </div>
        @endif
        <div class="accordion" id="muncipal-plan">
            @foreach ($objectives as $objective)
                <div class="card">
                    <div class="card-header" >
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#objective{{ $objective->id}}" aria-expanded="true" aria-controls="collapseOne">
                                {{ $objective->punto}}. {{ $objective->nombre}}
                            </button>
                        </h5>
                    </div>
                    <div id="objective{{ $objective->id}}" class="collapse" data-parent="#muncipal-plan">
                        @foreach ($objective->metas()->orderBy("id")->get() as $meta)
                            <div class="card ml-5">
                                <div class="card-header">
                                    <h5 class="mb-0" style="overflow-x:hidden;">
                                        @if ($meta->status == 0)
                                            <input type="checkbox" name="metas[]" class="mr-1" value="{{ $meta->id }}" >
                                        @else
                                            <span class="badge badge-success">Terminda en {{ $meta->finished_at }}</span>
                                        @endif
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#meta{{ $meta->id}}" aria-expanded="false" aria-controls="collapseTwo">
                                            <label style="width:100px;">{{ $meta->punto }}. {{ $meta->nombre }}</label>
                                        </button>
                                    </h5>
                                </div>
                                <div id="meta{{ $meta->id}}" class="collapse" data-parent="#objective{{ $objective->id}}">
                                    <div class="card-body">
                                        <h5 class="card-title text-center">Asignaciones</h5>
                                        <table class="table text-center">
                                            <thead>
                                                <tr>
                                                    <th scope="col">Direccion</th>
                                                    <th scope="col">Accciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @if (count($meta->directions) > 0)
                                                    @foreach ($meta->directions()->wherePivot("period", config('implan.periods.actual'))->get() as $direction)
                                                        <tr>
                                                            <td>{{ $direction->nombre }}</td>
                                                            <td>
                                                                <button type="button" class="btn btn-danger btn-desasignar" data-line="{{ $meta->id }}" data-direction="{{ $direction->id }}">
                                                                    <i class="fas fa-trash-alt mr-2"></i> Desasignar
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @else
                                                    <tr>
                                                        <td colspan="2">Ninguna Asignación</td>
                                                    </tr>
                                                @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row justify-content-center mt-3">
            <button type="submit" class="btn btn-primary mr-2 mb-2"><i class="fas fa-save mr-2"></i> Guardar</button>
        </div>
    </form>
</div>
@endsection

@section('modals')
    
@endsection

@section('js')
    <script>
        $(function(){
            $(document).on("click", ".btn-desasignar", function(){
                var line = $(this).data("line"),
                    direction = $(this).data("direction"),
                    token = "{{ csrf_token() }}";

                var _this = $(this);

                $.confirm({
                    title: '¿ Estas Seguro (a) ?',
                    content: 'Se desasignara la meta a la dependecia.',
                    buttons: {
                        Confirmar: {
                            text: 'Confirmar',
                            btnClass: 'btn btn-primary btn-confirm',
                            action: function(){
                                $.ajax({
                                    url: "{{ route('new.dettach.schedule') }}",
                                    type: "POST",
                                    data: {
                                        _token: token,
                                        line: line,
                                        direction: direction
                                    },
                                }).done(function(result){
                                    _this.parents("tr").fadeOut("slow");
                                });
                            }
                        },
                        Cancelar: {
                            text: 'Cancelar',
                            btnClass: 'btn btn-secondary btn-confirm',
                        }
                    }
                });
                
                
            });
        });
    </script>
@endsection