@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
    <div class=" mb-3 text-center">
        <h3>Áreas ( Periodo 2021 )</h3>
        <h4>( {{ $direction->nombre }} )</h4>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary text-center" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    <div class="row justify-content-end">
        {{-- <a href="{{ route('generate.my.poa') }}" class="btn btn-primary mr-3 mb-2" target="_BLANK"><i class="fas fa-file-pdf mr-2"></i> Previsualizar Asignaciones</a> --}}
        <button type="button" class="btn btn-primary mr-3 mb-2" data-toggle="modal" data-target="#modal-new-program">
            <i class="fas fa-plus-circle mr-2"></i> Añadir Área
        </button>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <table class="table dataTable">
                <thead>
                    <tr>
                        <th scope="col">Área</th>
                        <th scope="col">Objetivo</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody id="actions-poa">
                    @foreach ($direction->areas()->where('periodo', 2021)->get() as $area)
                        <tr class="edit-area" data-toggle="collapse" data-target="#collapse{{ $area->id }}">
                            <td>{{ $area->nombre }}</td>
                            <td>{{ $area->descripcion }}</td>
                            <td style="width: 10%;">
                                <div class="dropdown">
                                    <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Acciones
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item btn-update-area" href="#"   data-id="{{ $area->id }}" 
                                                                                            data-name="{{ $area->nombre }}" 
                                                                                            data-description="{{ $area->descripcion }}" 
                                                                                            data-mir="{{ $area->programa_id }}">
                                            Editar Área <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="{{ route('new.update.area', $area->id) }}" class="dropdown-item">
                                            Gestionar Componentes <i class="fas fa-edit"></i>
                                        </a>
                                        <a href="#!" class="dropdown-item btn-delete-area" data-area="{{ $area->id }}">
                                            Eliminar <i class="fas fa-trash-alt"></i>
                                        </a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
@endsection

@section('modals')
    @include('partials.modals.new-planning.modal_new_area')
    @include('partials.modals.new-planning.modal_update_area')
@endsection

@section('js')
<script>
$(function(){
    $(document).on("click", ".btn-update-area", function(){
        var id = $(this).data("id"),
            name = $(this).data("name"),
            description = $(this).data("description"),
            mir = $(this).data("mir");
        
        var modal = $("#modal-update-area");
        modal.find("input[name='name']").val(name);
        modal.find("select[name='program']").val(mir);
        modal.find("textarea[name='description']").val(description);
        modal.find("input[name='area_id']").val(id);
        modal.modal("show");
    });
    /**/
    /*eliminar área*/
    $(document).on("click", ".btn-delete-area", function(){
        var area = $(this).data("area"),
            token = "{{ csrf_token() }}";

        var _this = $(this);
        $.confirm({
            title: '¿ Estas Seguro (a) ?',
            content: 'Se eliminara el área, componentes y las actividades que lo componen',
            buttons: {
                Confirmar: {
                    text: 'Confirmar',
                    btnClass: 'btn btn-primary btn-confirm',
                    action: function(){
                        $.ajax({
                            url: "{{ route('new.delete.area') }}",
                            type: "POST",
                            data: {
                                _token: token,
                                area: area
                            },
                        }).done(function(result){
                            _this.parents("tr").fadeOut("slow");
                        });
                    }
                },
                Cancelar: {
                    text: 'Cancelar',
                    btnClass: 'btn btn-secondary btn-confirm',
                }
            }
        });
    });
});
</script>
@endsection