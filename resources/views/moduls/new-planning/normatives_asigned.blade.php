@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class=" mb-3 text-center">
    <h3>Marcos de Referencia {{ config('implan.periods.actual') }}</h3>
    <h4>Líneas de Acción / Indicadores</h4>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif
<div class="row justify-content-center">
    <div class="col-12 col-xl-8">
        <div class="accordion" id="muncipal-plan">
            <div class="card">
                <div class="card-header" >
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#lineActions" aria-expanded="true" aria-controls="collapseOne">
                            Plan Municipal
                        </button>
                    </h5>
                </div>
                <div id="lineActions" class="collapse" data-parent="#muncipal-plan">
                    <div class="card-body">
                        <table class="table dataTable">
                            <thead>
                                <tr>
                                    <th scope="col">Linea de Acción</th>
                                    <th scope="col">Estatus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach (Auth::user()->profile->position->direction->actionLines()->wherePivot("period", config('implan.periods.actual'))->get() as $actionLines)
                                    @if ($actionLines->pivot->status == 0)
                                        <tr class="line-action-pm" data-id="{{ $actionLines->id }}" >
                                            <td>{{ $actionLines->punto}}. {{ $actionLines->nombre}}</td>
                                            <td class="bg-warning text-white" data-pm="{{ $actionLines->id }}">Sin asignar</td>
                                        </tr>
                                    @elseif($actionLines->pivot->status == 1)
                                        <tr class="action-accept" data-id="{{ $actionLines->id }}">
                                            <td>{{ $actionLines->punto}}. {{ $actionLines->nombre}}</td>
                                            <td class="bg-success text-white">Acceptada</td>
                                        </tr>
                                    @else
                                        <tr class="action-reject" data-id="{{ $actionLines->id }}">
                                            <td>{{ $actionLines->punto}}. {{ $actionLines->nombre}}</td>
                                            <td class="bg-danger text-white">Rechazada</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="accordion" id="inafed">
            <div class="card">
                <div class="card-header" >
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#indicators" aria-expanded="true" aria-controls="collapseOne">
                            INAFED
                        </button>
                    </h5>
                </div>
                <div id="indicators" class="collapse" data-parent="#inafed">
                    <div class="card-body">
                        <table class="table dataTable">
                            <thead>
                                <tr>
                                    <th scope="col">Linea de Acción</th>
                                    <th scope="col">Estatus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach (Auth::user()->profile->position->direction->infIndicators()->wherePivot("period", config('implan.periods.actual'))->get() as $indicator)
                                    @if ($indicator->pivot->status == 0)
                                        <tr class="indicator-inf" data-id="{{ $indicator->id }}" >
                                            <td>{{ $indicator->punto}}. {{ $indicator->nombre}}</td>
                                            <td class="bg-warning text-white" data-inf="{{ $indicator->id }}">Sin asignar</td>
                                        </tr>
                                    @elseif($indicator->pivot->status == 1)
                                        <tr class="indicator-accept-inf" data-id="{{ $indicator->id }}">
                                            <td>{{ $indicator->punto}}. {{ $indicator->nombre}}</td>
                                            <td class="bg-success text-white">Acceptada</td>
                                        </tr>
                                    @else
                                        <tr class="indicator-reject-inf" data-id="{{ $indicator->id }}">
                                            <td>{{ $indicator->punto}}. {{ $indicator->nombre}}</td>
                                            <td class="bg-danger text-white">Rechazada</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="accordion" id="schedule">
            <div class="card">
                <div class="card-header" >
                    <h5 class="mb-0">
                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#metas" aria-expanded="true" aria-controls="collapseOne">
                            Agenda 2030
                        </button>
                    </h5>
                </div>
                <div id="metas" class="collapse" data-parent="#schedule">
                    <div class="card-body">
                        <table class="table dataTable">
                            <thead>
                                <tr>
                                    <th scope="col">Metas</th>
                                    <th scope="col">Estatus</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach (Auth::user()->profile->position->direction->scheMeta()->wherePivot("period", config('implan.periods.actual'))->get() as $meta)
                                    @if ($meta->pivot->estatus == 0)
                                        <tr class="meta-sche" data-id="{{ $meta->id }}" >
                                            <td>{{ $meta->punto}}. {{ $meta->nombre}}</td>
                                            <td class="bg-warning text-white" data-age="{{ $meta->id }}">Sin asignar</td>
                                        </tr>
                                    @elseif($meta->pivot->estatus == 1)
                                        <tr class="meta-accept-sche" data-id="{{ $meta->id }}">
                                            <td>{{ $meta->punto}}. {{ $meta->nombre}}</td>
                                            <td class="bg-success text-white">Acceptada</td>
                                        </tr>
                                    @else
                                        <tr class="meta-reject-sche" data-id="{{ $meta->id }}">
                                            <td>{{ $meta->punto}}. {{ $meta->nombre}}</td>
                                            <td class="bg-danger text-white">Rechazada</td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    
@endsection

@section('modals')
    {{-- Modal for Municipal plan --}}
    @include('partials.modals.normative.modal_action_line')
    @include('partials.modals.normative.modal_action_line_accepted')
    @include('partials.modals.normative.modal_action_line_rejected')
    @include('partials.modals.normative.modal_reject_action_line')
    {{-- Modal for INAFED --}}
    @include('partials.modals.normative.modal_inf_indicator')
    @include('partials.modals.normative.modal_inf_indicator_accepted')
    @include('partials.modals.normative.modal_inf_indicator_rejected')
    @include('partials.modals.normative.modal_reject_inf_indicator')
    {{-- Modal for Schedule 2030 --}}
    @include('partials.modals.normative.modal_sche_meta')
    @include('partials.modals.normative.modal_reject_sche_meta')
    @include('partials.modals.normative.modal_sche_meta_rejected')
    @include('partials.modals.normative.modal_sche_meta_accepted')
@endsection

@section('js')
    <script>
        $(function(){
            /*************** Events for Municipal Plan ****************/
            $(document).on('dblclick', '.line-action-pm', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('new.get.pm.action.line.by.id') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id,
                    },
                }).done(function(result){
                    var modal = $("#modal-action-line");
                    modal.find('[name="id_action_line"]').val(result.id);
                    modal.find("#normative").val(result.normative.nombre);
                    modal.find("#axe").val(result.axe.punto+". "+result.axe.nombre);
                    modal.find("#objective").val(result.objective.punto+". "+result.objective.nombre);
                    modal.find("#strategy").val(result.strategy.punto+". "+result.strategy.nombre);
                    modal.find("#action-line").val(result.punto+". "+result.nombre);
                    modal.find(".id-action-line").data("id", result.id);
                    modal.modal("show");
                });
            });
            
            $(document).on('dblclick', '.action-accept', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('new.get.pm.action.line.by.id') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id,
                    },
                }).done(function(result){
                    var modal = $("#modal-action-line-accepted");
                    modal.find("#accept-normative").val(result.normative.nombre);
                    modal.find("#accept-axe").val(result.axe.punto+". "+result.axe.nombre);
                    modal.find("#accept-objective").val(result.objective.punto+". "+result.objective.nombre);
                    modal.find("#accept-strategy").val(result.strategy.punto+". "+result.strategy.nombre);
                    modal.find("#accept-action-line").val(result.punto+". "+result.nombre);
                    modal.modal("show");
                });
            });
            
            $(document).on('dblclick', '.action-reject', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('new.get.pm.action.line.reject.by.id') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id,
                    },
                }).done(function(result){
                    var modal = $("#modal-action-line-rejected");
                    modal.find("#reject-normative").val(result.normative.nombre);
                    modal.find("#reject-axe").val(result.axe.punto+". "+result.axe.nombre);
                    modal.find("#reject-objective").val(result.objective.punto+". "+result.objective.nombre);
                    modal.find("#reject-strategy").val(result.strategy.punto+". "+result.strategy.nombre);
                    modal.find("#reject-action-line").val(result.punto+". "+result.nombre);
                    modal.find("#reject-description").val(result.reject.descripcion);
                    modal.modal("show");
                });
            });
            
            /*************** Events for INAFED ****************/

            $(document).on('dblclick', '.indicator-inf', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('new.get.inf.indicator.by.id') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id,
                    },
                }).done(function(result){
                    var modal = $("#modal-inf-indicatior");
                    modal.find("#inf-normative").val(result.normative.nombre);
                    modal.find("#inf-schedule").val(result.schedule.punto+". "+result.schedule.nombre);
                    modal.find("#inf-axe").val(result.axe.punto+". "+result.axe.nombre);
                    modal.find("#inf-topic").val(result.topic.punto+". "+result.topic.nombre);
                    modal.find("#inf-indicator").val(result.punto+". "+result.nombre);
                    modal.find(".id-inf-indicator").data("id", result.id);
                    modal.modal("show");
                });
            });
            
            $(document).on('dblclick', '.indicator-accept-inf', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('new.get.inf.indicator.by.id') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id,
                    },
                }).done(function(result){
                    var modal = $("#modal-inf-indicatior-accepted");
                    modal.find("#accept-inf-normative").val(result.normative.nombre);
                    modal.find("#accept-inf-schedule").val(result.schedule.punto+". "+result.schedule.nombre);
                    modal.find("#accept-inf-axe").val(result.axe.punto+". "+result.axe.nombre);
                    modal.find("#accept-inf-topic").val(result.topic.punto+". "+result.topic.nombre);
                    modal.find("#accept-inf-indicator").val(result.punto+". "+result.nombre);
                    modal.modal("show");
                });
            });
            
            $(document).on('dblclick', '.indicator-reject-inf', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('new.get.inf.indicator.reject.by.id') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id,
                    },
                }).done(function(result){
                    var modal = $("#modal-inf-indicatior-rejected");
                    modal.find("#reject-inf-normative").val(result.normative.nombre);
                    modal.find("#reject-inf-schedule").val(result.schedule.punto+". "+result.schedule.nombre);
                    modal.find("#reject-inf-axe").val(result.axe.punto+". "+result.axe.nombre);
                    modal.find("#reject-inf-topic").val(result.topic.punto+". "+result.topic.nombre);
                    modal.find("#reject-inf-indicator").val(result.punto+". "+result.nombre);
                    modal.find("#reject-description-inf").val(result.reject.descripcion);
                    modal.modal("show");
                });
            });

            /*************** Events for Schedule 2030 ****************/

            $(document).on('dblclick', '.meta-sche', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('new.get.sche.meta.by.id') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id,
                    },
                }).done(function(result){
                    console.log(result);
                    var modal = $("#modal-sche-meta");
                    modal.find("#sche-normative").val(result.normative.nombre);
                    modal.find("#sche-objective").val(result.objective.punto+". "+result.objective.nombre);
                    modal.find("#sche-meta").val(result.punto+". "+result.nombre);
                    modal.find(".id-sche-indicator").data("id", result.id);
                    modal.modal("show");
                });
            });

            $(document).on('dblclick', '.meta-accept-sche', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('new.get.sche.meta.by.id') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id,
                    },
                }).done(function(result){
                    var modal = $("#modal-sche-meta-accepted");
                    modal.find("#accept-sche-normative").val(result.normative.nombre);
                    modal.find("#accept-sche-objective").val(result.objective.punto+". "+result.objective.nombre);
                    modal.find("#accept-sche-meta").val(result.punto+". "+result.nombre);
                    modal.modal("show");
                });
            });

            $(document).on('dblclick', '.meta-reject-sche', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('new.get.sche.meta.reject.by.id') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id,
                    },
                }).done(function(result){
                    var modal = $("#modal-sche-meta-rejected");
                    modal.find("#reject-sche-normative").val(result.normative.nombre);
                    modal.find("#reject-sche-objective").val(result.objective.punto+". "+result.objective.nombre);
                    modal.find("#reject-sche-meta").val(result.punto+". "+result.nombre);
                    modal.find("#reject-description-sche").val(result.reject.descripcion);
                    modal.modal("show");
                });
            });
        });
    </script>
@endsection