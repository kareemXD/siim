@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
    <div class="row justify-content-center mb-3">
        <h3>Editar Acción</h3>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary text-center" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    <form id="form-action" action="{{ route('update.action.poa') }}" method="POST">
        @csrf
        <input type="hidden" name="id" value="{{ $action->id }}">
        <input type="hidden" name="program" value="{{ $action->programas_id }}">
        <input type="hidden" name="direction" value="{{ $direction->id }}">
        <fieldset class="col-12 fieldset">
            <input type="hidden" name="validate_normative" value="1">
            <legend class="legend">Normativas:</legend>
            <div class="row">
                <div class="col" id="alert-normative">
                    
                </div>
            </div>
            <div class="row mb-3">
                <div class="col-5">
                    <select class="select2 normative" id="normative">
                        <option value=" " selected disabled>Seleccioné la Normativa</option>
                        @foreach ($normatives as $normative)
                            <option value="{{ $normative->id }}">{{ $normative->nombre }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-6">
                    <select class="select2" id="actionLine" disabled>
                        <option value=" " selected disabled>Seleccioné la Línea de Acción</option>
                    </select>
                </div>
                <div class="col-1">
                    <button type="button" class="btn btn-success btn-add-normative" data-toggle="tooltip" data-placement="top" title="Añadir"><i class="fas fa-plus-circle"></i></button> 
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <table class="table dataTable">
                        <thead>
                            <tr>
                                <td class="col-11">Línea de Acción</td>
                                <td class="col-1">Acción</td>
                            </tr>
                        </thead>
                        <tbody id="normatives-content">
                            @foreach ($action->pm_action_lines as $actionLine)
                                <tr>
                                    <td class="col-11">{{ $actionLine->punto }}. {{ $actionLine->nombre }} <input type="hidden" name="pm_normatives[]" value="{{ $actionLine->id }}" /> </td>
                                    <td class="col-1"><button type="button" class="btn btn-danger btn-delete-normative"><i class="fas fa-trash-alt"></i></button></td>
                                </tr>
                            @endforeach
                            @foreach ($action->inf_action_lines as $indicator)
                                <tr>
                                    <td class="col-11">{{ $indicator->punto }}. {{ $indicator->nombre }} <input type="hidden" name="inf_normatives[]" value="{{ $indicator->id }}" /> </td>
                                    <td class="col-1"><button type="button" class="btn btn-danger btn-delete-normative"><i class="fas fa-trash-alt"></i></button></td>
                                </tr>
                            @endforeach
                            @foreach ($action->sche_metas as $meta)
                                <tr>
                                    <td class="col-11">{{ $meta->punto }}. {{ $meta->nombre }} <input type="hidden" name="sche_normatives[]" value="{{ $meta->id }}" /> </td>
                                    <td class="col-1"><button type="button" class="btn btn-danger btn-delete-normative"><i class="fas fa-trash-alt"></i></button></td>
                                </tr>
                            @endforeach
                            @foreach ($action->mir_idicators as $mir)
                                <tr>
                                    <td class="col-11">{{ $mir->nombre }} <input type="hidden" name="mir_normatives[]" value="{{ $mir->id }}" /> </td>
                                    <td class="col-1"><button type="button" class="btn btn-danger btn-delete-normative"><i class="fas fa-trash-alt"></i></button></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </fieldset>
        <fieldset class="col-12 fieldset">
            <legend class="legend">Responsable:</legend>
            <div class="row mb-3">
                <div class="col">
                    <select class="select2 responsable" id="responsable" name="responsable" required>
                        <option value="" selected disabled>Seleccioné al Responsable</option>
                        @foreach ($direction->positions as $position)
                            @if ($position->id == $action->puesto_id)
                                <option selected value="{{ $position->id }}">{{ $position->nombre }} ( {{ $position->direction->abreviacion }} )</option>
                            @else
                                <option value="{{ $position->id }}">{{ $position->nombre }} ( {{ $position->direction->abreviacion }} )</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
        </fieldset>
        <fieldset class="col-12 fieldset">
            <legend class="legend">Actividades:</legend>
            <div class="form-row mb-3">
                <div class="col-6">
                    <label for="type">Tipo de Actividad</label>
                    <select class="select2 settings" id="type" name="type">
                        @if ($action->tipo == 0)
                            <option value="0" selected>Actividad con porcentaje de avance acendente</option>
                            <option value="1">Actividad con porcentaje de avance decendente</option>
                        @elseif($action->tipo == 1)
                            <option value="0" >Actividad con porcentaje de avance acendente</option>
                            <option value="1" selected>Actividad con porcentaje de avance decendente</option>
                        @endif
                    </select>
                </div>
                <div class="col-6">
                    <label for="value">Tipo de Valor de la Actividad</label>
                    <select class="select2 settings" id="value" name="value">
                        @if ($action->valor == 0)
                            <option value="0" selected>Numerico</option>
                            <option value="1">Porcentual</option>
                        @elseif($action->valor == 1)
                            <option value="0" >Numerico</option>
                            <option value="1" selected>Porcentual</option>
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-row">
                <div class="col-6">
                    <label for="activity">Actividad</label>
                    <input required type="text" class="form-control " name="activity" value="{{ $action->actividad }}">
                </div>
                <div class="col-1">
                    <label for="unity">U. de M.</label>
                    <input required type="text" class="form-control " name="unity" value="{{ $action->unidad_medida }}">
                </div>
                
                    <div class="col-1">
                        <label for="activity">Meta Anual</label>
                        <input required @if($action->valor == 1 || $action->tipo == 1) @else readonly @endif  min="0" type="number" step=".01" class="form-control total total-input " name="year" value="{{ $action->meta_anual }}">
                    </div>
                    <div class="col-1">
                        <label for="activity">1° Trimeste</label>
                        <input required  type="number" step=".01" min="0" class="form-control tri-inp trimounth trimounth-parent-1 trimounth-input" name="first" value="{{ $action->trimestre_1 }}">
                    </div>
                    <div class="col-1">
                        <label for="activity">2° Trimeste</label>
                        <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth trimounth-parent-2 trimounth-input" name="second" value="{{ $action->trimestre_2 }}">
                    </div>
                    <div class="col-1">
                        <label for="activity">3° Trimeste</label>
                        <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth trimounth-parent-3 trimounth-input" name="thirth" value="{{ $action->trimestre_3 }}">
                    </div>
                    <div class="col-1">
                        <label for="activity">4° Trimeste</label>
                        <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth trimounth-parent-4 trimounth-input" name="four" value="{{ $action->trimestre_4 }}">
                    </div>
                
            </div>
            <div class="row justify-content-end mt-2">
                <button type="button" class="btn btn-primary mr-3 mb-2 btn-add-action"><i class="fas fa-plus-circle mr-2"></i> Agregar Subacción</button>
            </div>
            <div id="actions-poa">
                @foreach ($action->subactions as $subaction)
                <div class="form-row">
                    <div class="col-6">
                        <label for="activity">Subactividad</label>
                        <input required type="text" class="form-control" value="{{ $subaction->actividad }}" name="subactivity[]">
                    </div>
                    <div class="col-1">
                        <label for="activity">Meta Anual</label>
                        <input required readonly type="number" step=".01" min="0" class="form-control total" value="{{ $subaction->meta_anual }}"  name="subyear[]">
                    </div>
                    <div class="col-1">
                        <label for="activity">1° Trimeste</label>
                        <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth" value="{{ $subaction->trimestre_1 }}" data-parent="1" name="subfirst[]">
                    </div>
                    <div class="col-1">
                        <label for="activity">2° Trimeste</label>
                        <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth" value="{{ $subaction->trimestre_2 }}" data-parent="2" name="subsecond[]">
                    </div>
                    <div class="col-1">
                        <label for="activity">3° Trimeste</label>
                        <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth" value="{{ $subaction->trimestre_3 }}" data-parent="3" name="subthirth[]">
                    </div>
                    <div class="col-1">
                        <label for="activity">4° Trimeste</label>
                        <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth" value="{{ $subaction->trimestre_4 }}" data-parent="4" name="subfour[]">
                    </div>
                    <div class="col-1 text-center">
                        <label for="activity" style="display:block;">Acciones</label>
                        <button type="button" class="btn btn-danger btn-delete-subaction"><i class="fas fa-trash-alt"></i></button> 
                    </div>
                </div>
                @endforeach
            </div>
            <div class="row justify-content-center mt-2">
                <button type="submit" class="btn btn-primary mr-3 mb-2"><i class="fas fa-save mr-2"></i> Guardar</button>
            </div>
        </fieldset>
    </form>
@endsection

@section('modals')

@endsection

@section('js')
    <script>
        $(function(){
            $(document).on("submit", "#form-action", function(e){
                if($('[name="validate_normative"]').val() != 1)
                {
                    e.preventDefault();
                    $("#alert-normative").empty();
                    $("#alert-normative").append('<div class="alert alert-danger text-center">\
                                                    Vincule al menos una Normativa.\
                                                </div>');
                }
            });
            $(document).on("change", "#normative", function(){
                var normative = $(this).val(),
                    direction = $('[name="direction"]').val(),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.action.lines.by.normative') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        normative: normative,
                        direction: direction,
                    },
                }).done(function(result){
                    $("#actionLine").empty();
                    $("#actionLine").append('<option value=" " selected disabled>Seleccioné la Normativa</option>');
                    result.forEach(element => {
                        if(element.punto == undefined)
                        {
                            $("#actionLine").append('<option value="'+element.id+'">'+element.nombre+'</option>');
                        }else
                        {
                            $("#actionLine").append('<option value="'+element.id+'">'+ element.punto + ". " +element.nombre+'</option>');
                        }
                    });
                    $("#actionLine").removeAttr("disabled");
                });
            });

            $(document).on("click", ".btn-add-normative", function(){
                var action = $("#actionLine option:selected").text();
                var norma = $("#normative option:selected").text();
                $("#text-test").remove();
                if(norma == "Plan Municipal")
                {
                    $("#normatives-content").append('<tr>\
                            <td>'+ action +' <input type="hidden" name="pm_normatives[]" value="'+ $("#actionLine").val() +'" /> </td>\
                            <td><button type="button" class="btn btn-danger btn-delete-normative"><i class="fas fa-trash-alt"></i></button></td>\
                        </tr>'
                    );
                }
                if(norma == "INAFED")
                {
                    $("#normatives-content").append('<tr>\
                            <td>'+ action +' <input type="hidden" name="inf_normatives[]" value="'+ $("#actionLine").val() +'" /> </td>\
                            <td><button type="button" class="btn btn-danger btn-delete-normative"><i class="fas fa-trash-alt"></i></button></td>\
                        </tr>'
                    );
                }
                if(norma == "Agenda 2030")
                {
                    $("#normatives-content").append('<tr>\
                            <td>'+ action +' <input type="hidden" name="sche_normatives[]" value="'+ $("#actionLine").val() +'" /> </td>\
                            <td><button type="button" class="btn btn-danger btn-delete-normative"><i class="fas fa-trash-alt"></i></button></td>\
                        </tr>'
                    );
                }
                if(norma == "MIR")
                {
                    $("#normatives-content").append('<tr>\
                            <td>'+ action +' <input type="hidden" name="mir_normatives[]" value="'+ $("#actionLine").val() +'" /> </td>\
                            <td><button type="button" class="btn btn-danger btn-delete-normative"><i class="fas fa-trash-alt"></i></button></td>\
                        </tr>'
                    );
                }
                $("#actionLine").val(" ");
                $("#actionLine").select2();
                $('[name="validate_normative"]').val(1);
            });

            $(document).on("change", ".settings", function(){
                var flag = false;
                $(".settings").each(function(i){
                    if($(this).val() == 1)
                    {
                        flag = true;
                    }
                });
                if(flag)
                {
                    $(".trimounth").each(function(i){
                        $(this).removeClass("trimounth");
                    });
                    
                    $(".total").each(function(i){
                        $(this).removeAttr("readonly");
                    });
                }else
                {
                    $(".tri-inp").each(function(i){
                        $(this).addClass("trimounth");
                    });
                    
                    $(".total").each(function(i){
                        $(this).attr("readonly", true);
                    });
                }
            });

            $(document).on("click", ".btn-delete-normative", function(){
                var _this = $(this);
                _this.parents("tr").fadeOut("slow", function(){
                    $(this).remove();
                    var trs = $("#normatives-content").find("tr");
                    if(trs.length == 0)
                    {
                        $("#normatives-content").append('<tr class="text-center" id="text-test">\
                                <td colspan="2">Ninguna Línea de Acción Agregada</td>\
                            </tr>'
                        );
                        $('[name="validate_normative"]').val(0);
                    }
                });
            });

            $(document).on("focusout", ".trimounth", function(){
                var _this = $(this),
                    val_sub = 0,
                    val = 0,
                    parent = $(this).data("parent");
                $('[data-parent="'+parent+'"]').each(function(i){
                    val_sub += Number($(this).val());
                });
                $(".trimounth-parent-"+parent).val(val_sub);
                val_sub = 0;
                $(".trimounth-input").each(function(i){
                    val_sub += Number($(this).val());
                });
                $(".total-input").val(val_sub);

                _this.parents(".form-row").find(".trimounth").each(function(i){
                    val += Number($(this).val());
                });
                _this.parents(".form-row").find(".total").val(val);
            });

            $(document).on("click", ".btn-add-action", function(){
                var trs = $("#actions-poa").find(".form-row");
                if(trs.length == 0)
                {
                    $(".trimounth-input").each(function(i){
                        $(this).val(0);
                    });
                    $(".total-input").val(0);
                    $(".trimounth-input").attr("readonly", true);
                }
                var attri = "readonly";
                $(".settings").each(function(i){
                    if($(this).val() == 1)
                    {
                        attri = "";
                    }
                });
                $("#actions-poa").append('<div class="form-row">\
                    <div class="col-6">\
                        <label for="activity">Subactividad</label>\
                        <input required type="text" class="form-control" name="subactivity[]">\
                    </div>\
                    <div class="col-1">\
                        <label for="activity">Meta Anual</label>\
                        <input required '+attri+' type="number" step=".01" min="0" class="form-control total" name="subyear[]">\
                    </div>\
                    <div class="col-1">\
                        <label for="activity">1° Trimeste</label>\
                        <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth" data-parent="1"  name="subfirst[]">\
                    </div>\
                    <div class="col-1">\
                        <label for="activity">2° Trimeste</label>\
                        <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth" data-parent="2" name="subsecond[]">\
                    </div>\
                    <div class="col-1">\
                        <label for="activity">3° Trimeste</label>\
                        <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth" data-parent="3" name="subthirth[]">\
                    </div>\
                    <div class="col-1">\
                        <label for="activity">4° Trimeste</label>\
                        <input required type="number" step=".01" min="0" class="form-control tri-inp trimounth" data-parent="4" name="subfour[]">\
                    </div>\
                    <div class="col-1 text-center">\
                        <label for="activity" style="display:block;">Acciones</label>\
                        <button type="button" class="btn btn-danger btn-delete-subaction"><i class="fa fa-trash-alt"></i></button> \
                    </div>\
                </div>'
                );
                
            });

            $(document).on("click", ".btn-delete-subaction", function(){
                var _this = $(this),
                    val_sub = 0,
                    index = 0;
                _this.parents(".form-row").fadeOut("slow", function(){
                    $(this).remove();
                    _this.parents(".form-row").find(".trimounth").each(function(i){
                        index = i + 1;
                        val_sub = ($(".trimounth-parent-"+index).val()) - (Number($(this).val()));
                        $(".trimounth-parent-"+index).val(val_sub)
                    });
                    val_sub = 0;
                    $(".trimounth-input").each(function(i){
                        val_sub += Number($(this).val());
                    });
                    $(".total-input").val(val_sub);
                    var trs = $("#actions-poa").find(".form-row");
                    if(trs.length == 0)
                    {
                        $(".trimounth-input").removeAttr("readonly");
                    }
                });

            });


        });
    </script>
@endsection