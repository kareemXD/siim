@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class="row justify-content-center mb-3">
    <h3>Asignación de las MIR</h3>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif
<div class="row justify-content-center">
    <form action="{{ route('assign.mir') }}" method="POST" class="col-12 col-xl-8">
        @csrf
        @if (config('implan.times.planning'))
            <div class="form-group row">
                <label for="action" class="col-md-5 col-form-label text-md-right">Acción a Realizar</label>
                <div class="col-md-6 ">
                    <select class="form-control {{ $errors->has('direction') ? ' is-invalid' : '' }}" id="action">
                        <option value="1" selected>Asignar Indicadores</option>
                        <option value="2">Cambiar Estatus a Indicador</option>
                    </select>
                </div>
            </div>
        @endif
        <div class="form-group row direction-combo">
            <label for="direction" class="col-md-5 col-form-label text-md-right">Seleccione una Dirección</label>
            <div class="col-md-6 ">
                <select class="form-control select2 {{ $errors->has('direction') ? ' is-invalid' : '' }}" name="direction" required>
                    <option value="" selected>Seleccione una dirección</option>
                    @foreach ($directions as $direction)
                        <option value="{{ $direction->id }}">{{ $direction->nombre }}</option>
                    @endforeach
                </select>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        @if ($errors->has('password'))
            <div class="alert alert-danger text-center" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </div>
        @endif
        <div class="accordion" id="muncipal-plan">
            @foreach ($programs as $program)
                <div class="card ml-5">
                    <div class="card-header" >
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#program{{ $program->id}}" aria-expanded="true" aria-controls="collapseOne">
                                {{ $program->nombre }}
                            </button>
                        </h5>
                    </div>
                    <div id="program{{ $program->id}}" class="collapse" data-parent="#muncipal-plan">
                        @foreach ($types as $type)
                            <div class="card ml-5">
                                <div class="card-header" >
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#type{{ $type->id}}" aria-expanded="true" aria-controls="collapseOne">
                                            {{ $type->nombre }}
                                        </button>
                                    </h5>
                                </div>
                                <div id="type{{ $type->id}}" class="collapse" data-parent="#program{{ $program->id}}">
                                    @foreach ($program->indicators()->where("mir_indicador_nivel_id", $type->id)->get() as $indicator)
                                        <div class="card ml-5">
                                            <div class="card-header">
                                                <h5 class="mb-0" style="overflow-x:hidden;">
                                                    @if ($indicator->status == 0)
                                                        <input type="checkbox" name="indicators[]" class="mr-1 inp-indicator" value="{{ $indicator->id }}" >
                                                        {{-- <input type="hidden" name="periods[]" value="{{ config('implan.periods.previous') }}" > --}}
                                                    @else
                                                        <span class="badge badge-success">Terminda en {{ $indicator->finished_at }}</span>
                                                    @endif
                                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#indicator{{ $indicator->id}}" aria-expanded="false" aria-controls="collapseTwo">
                                                        <label style="width:100px;">{{ $indicator->nombre }}</label>
                                                    </button>
                                                </h5>
                                            </div>
                                            <div id="indicator{{ $indicator->id}}" class="collapse" data-parent="#type{{ $type->id }}">
                                                <div class="card-body">
                                                    <h5 class="card-title text-center">Asignaciones</h5>
                                                    <table class="table text-center">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col">Direccion</th>
                                                                <th scope="col">Accciones</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @if (count($indicator->directions) > 0)
                                                                @foreach ($indicator->directions()->wherePivot("period", config('implan.periods.previous'))->get() as $direction)
                                                                    <tr>
                                                                        <td>{{ $direction->nombre }}</td>
                                                                        <td>
                                                                            <button type="button" class="btn btn-danger btn-desasignar" data-line="{{ $indicator->id }}" data-direction="{{ $direction->id }}" data-period="{{ config('implan.periods.previous') }}">
                                                                                <i class="fas fa-trash-alt mr-2"></i> Desasignar
                                                                            </button>
                                                                        </td>
                                                                    </tr>
                                                                @endforeach
                                                            @else
                                                                <tr>
                                                                    <td colspan="2">Ninguna Asignación</td>
                                                                </tr>
                                                            @endif
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row justify-content-center mt-3">
            <button type="submit" class="btn btn-primary mr-2 mb-2"><i class="fas fa-save mr-2"></i> Guardar</button>
        </div>
    </form>
</div>
@endsection

@section('modals')
    
@endsection

@section('js')
    <script>
        $(function(){
            // $(document).on("change", ".inp-indicator", function(){
            //     var _this = $(this);
            //     if(this.checked) {
            //         _this.parent().find("input[name='periods[]']").attr("checked", true);
            //     }else{
            //         _this.parent().find("input[name='periods[]']").removeAttr("checked");
            //     }
            // });
            $(document).on("click", ".btn-desasignar", function(){
                var line = $(this).data("line"),
                    direction = $(this).data("direction"),
                    period = $(this).data("period"),
                    token = "{{ csrf_token() }}";

                var _this = $(this);

                $.confirm({
                    title: '¿ Estas Seguro (a) ?',
                    content: 'Se desasignara la indicator a la dependecia.',
                    buttons: {
                        Confirmar: {
                            text: 'Confirmar',
                            btnClass: 'btn btn-primary btn-confirm',
                            action: function(){
                                $.ajax({
                                    url: "{{ route('dettach.mir') }}",
                                    type: "POST",
                                    data: {
                                        _token: token,
                                        line: line,
                                        period: period,
                                        direction: direction
                                    },
                                }).done(function(result){
                                    _this.parents("tr").fadeOut("slow");
                                });
                            }
                        },
                        Cancelar: {
                            text: 'Cancelar',
                            btnClass: 'btn btn-secondary btn-confirm',
                        }
                    }
                });
                
                
            });
        });
    </script>
@endsection