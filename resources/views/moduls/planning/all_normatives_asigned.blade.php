@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class="row justify-content-center mb-3">
    <h3>Normativa Asignada a todas las Direcciones</h3>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif
<div class="row justify-content-center">
    <div class="col-12 col-xl-8">
        @foreach ($directions as $direction)
            <div class="accordion" id="direction{{ $direction->id }}">
                <div class="card">
                    <div class="card-header" >
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#normative{{ $direction->id }}" aria-expanded="true" aria-controls="collapseOne">
                                {{ $direction->nombre }}
                            </button>
                        </h5>
                    </div>
                    <div id="normative{{ $direction->id }}" class="collapse" data-parent="#direction{{ $direction->id }}">
                        <div class="card-body">
                            <div class="accordion" id="muncipal-plan">
                                <div class="card">
                                    <div class="card-header" >
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#lineActions" aria-expanded="true" aria-controls="collapseOne">
                                                Plan Municipal
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="lineActions" class="collapse" data-parent="#muncipal-plan">
                                        <div class="card-body">
                                            <table class="table dataTable">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Linea de Acción</th>
                                                        <th scope="col">Estatus</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($direction->actionLines()->wherePivot("period", config('implan.periods.previous'))->get() as $actionLines)
                                                    {{-- @php
                                                        dd($actionLines);
                                                    @endphp --}}
                                                        @if ($actionLines->pivot->status == 0)
                                                            <tr class="line-action-pm" data-id="{{ $actionLines->id }}">
                                                                <td>{{ $actionLines->punto}}. {{ $actionLines->nombre}}</td>
                                                                <td class="bg-warning text-white">Sin asignar</td>
                                                            </tr>
                                                        @elseif($actionLines->pivot->status == 1)
                                                            <tr class="action-accept" data-id="{{ $actionLines->id }}">
                                                                <td>{{ $actionLines->punto}}. {{ $actionLines->nombre}}</td>
                                                                <td class="bg-success text-white">Asignada</td>
                                                            </tr>
                                                        @else
                                                            <tr class="action-reject" data-id="{{ $actionLines->id }}">
                                                                <td>{{ $actionLines->punto}}. {{ $actionLines->nombre}}</td>
                                                                <td class="bg-danger text-white">Rechazada</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="accordion" id="inafed">
                                <div class="card">
                                    <div class="card-header" >
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#indicators" aria-expanded="true" aria-controls="collapseOne">
                                                INAFED
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="indicators" class="collapse" data-parent="#inafed">
                                        <div class="card-body">
                                            <table class="table dataTable">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Linea de Acción</th>
                                                        <th scope="col">Estatus</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($direction->infIndicators()->wherePivot("period", config('implan.periods.previous'))->get() as $indicator)
                                                        @if ($indicator->pivot->status == 0)
                                                            <tr class="indicator-inf" data-id="{{ $indicator->id }}">
                                                                <td>{{ $indicator->punto}}. {{ $indicator->nombre}}</td>
                                                                <td class="bg-warning text-white">Sin asignar</td>
                                                            </tr>
                                                        @elseif($indicator->pivot->status == 1)
                                                            <tr class="indicator-accept-inf" data-id="{{ $indicator->id }}">
                                                                <td>{{ $indicator->punto}}. {{ $indicator->nombre}}</td>
                                                                <td class="bg-success text-white">Asignada</td>
                                                            </tr>
                                                        @else
                                                            <tr class="indicator-reject-inf" data-id="{{ $indicator->id }}">
                                                                <td>{{ $indicator->punto}}. {{ $indicator->nombre}}</td>
                                                                <td class="bg-danger text-white">Rechazada</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="accordion" id="schedule">
                                <div class="card">
                                    <div class="card-header" >
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#metas" aria-expanded="true" aria-controls="collapseOne">
                                                Agenda 2030
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="metas" class="collapse" data-parent="#schedule">
                                        <div class="card-body">
                                            <table class="table dataTable">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Metas</th>
                                                        <th scope="col">Estatus</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($direction->scheMeta()->wherePivot("period", config('implan.periods.previous'))->get() as $meta)
                                                        @if ($meta->pivot->estatus == 0)
                                                            <tr class="meta-sche" data-id="{{ $meta->id }}">
                                                                <td>{{ $meta->punto}}. {{ $meta->nombre}}</td>
                                                                <td class="bg-warning text-white">Sin asignar</td>
                                                            </tr>
                                                        @elseif($meta->pivot->estatus == 1)
                                                            <tr class="meta-accept-sche" data-id="{{ $meta->id }}">
                                                                <td>{{ $meta->punto}}. {{ $meta->nombre}}</td>
                                                                <td class="bg-success text-white">Asignada</td>
                                                            </tr>
                                                        @else
                                                            <tr class="meta-reject-sche" data-id="{{ $meta->id }}">
                                                                <td>{{ $meta->punto}}. {{ $meta->nombre}}</td>
                                                                <td class="bg-danger text-white">Rechazada</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="accordion" id="mir">
                                <div class="card">
                                    <div class="card-header" >
                                        <h5 class="mb-0">
                                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#mir-indicator" aria-expanded="true" aria-controls="collapseOne">
                                                MIR
                                            </button>
                                        </h5>
                                    </div>
                                    <div id="mir-indicator" class="collapse" data-parent="#mir">
                                        <div class="card-body">
                                            <table class="table dataTable">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">Indicador</th>
                                                        <th scope="col">Estatus</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($direction->mirIndicators()->wherePivot("period", config('implan.periods.previous'))->get() as $indicator)
                                                        @if ($indicator->pivot->status == 0)
                                                            <tr class="indicator-mir" data-direction="{{ $direction->id }}" data-id="{{ $indicator->id }}">
                                                                <td>{{ $indicator->punto.". ".$indicator->nombre}}</td>
                                                                <td class="bg-warning text-white" data-mir="{{ $indicator->id }}">Sin asignar</td>
                                                            </tr>
                                                        @elseif($indicator->pivot->status == 1)
                                                            <tr class="indicator-accept-mir" data-id="{{ $indicator->id }}">
                                                                <td>{{ $indicator->punto.". ".$indicator->nombre}}</td>
                                                                <td class="bg-success text-white">Asignada</td>
                                                            </tr>
                                                        @else
                                                            <tr class="indicator-reject-mir" data-id="{{ $indicator->id }}">
                                                                <td>{{ $indicator->punto.". ".$indicator->nombre}}</td>
                                                                <td class="bg-danger text-white">Rechazada</td>
                                                            </tr>
                                                        @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
    
@endsection

@section('modals')
    {{-- Modal for Municipal plan --}}
    @include('partials.modals.normative.modal_action_line')
    @include('partials.modals.normative.modal_action_line_accepted')
    @include('partials.modals.normative.modal_action_line_rejected')
    @include('partials.modals.normative.modal_reject_action_line')
    {{-- Modal for INAFED --}}
    @include('partials.modals.normative.modal_inf_indicator')
    @include('partials.modals.normative.modal_inf_indicator_accepted')
    @include('partials.modals.normative.modal_inf_indicator_rejected')
    @include('partials.modals.normative.modal_reject_inf_indicator')
    {{-- Modal for Schedule 2030 --}}
    @include('partials.modals.normative.modal_sche_meta')
    @include('partials.modals.normative.modal_reject_sche_meta')
    @include('partials.modals.normative.modal_sche_meta_rejected')
    @include('partials.modals.normative.modal_sche_meta_accepted')
    {{-- Modal for MIR´s --}}
    @include('partials.modals.normative.modal_mir_indicator')
    @include('partials.modals.normative.modal_reject_mir_indicator')
    @include('partials.modals.normative.modal_mir_indicator_rejected')
    @include('partials.modals.normative.modal_mir_indicator_accepted')
@endsection

@section('js')
<script>
    $(function(){
        /*************** Events for Municipal Plan ****************/
        $(document).on('dblclick', '.line-action-pm', function(){
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.pm.action.line.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                var modal = $("#modal-action-line");
                modal.find('[name="id_action_line"]').val(result.id);
                modal.find("#normative").val(result.normative.nombre);
                modal.find("#axe").val(result.axe.punto+". "+result.axe.nombre);
                modal.find("#objective").val(result.objective.punto+". "+result.objective.nombre);
                modal.find("#strategy").val(result.strategy.punto+". "+result.strategy.nombre);
                modal.find("#action-line").val(result.punto+". "+result.nombre);
                modal.find(".id-action-line").data("id", result.id);
                modal.modal("show");
            });
        });
        
        $(document).on('dblclick', '.action-accept', function(){
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.pm.action.line.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                var modal = $("#modal-action-line-accepted");
                modal.find("#accept-normative").val(result.normative.nombre);
                modal.find("#accept-axe").val(result.axe.punto+". "+result.axe.nombre);
                modal.find("#accept-objective").val(result.objective.punto+". "+result.objective.nombre);
                modal.find("#accept-strategy").val(result.strategy.punto+". "+result.strategy.nombre);
                modal.find("#accept-action-line").val(result.punto+". "+result.nombre);
                modal.modal("show");
            });
        });
        
        $(document).on('dblclick', '.action-reject', function(){
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.pm.action.line.reject.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                var modal = $("#modal-action-line-rejected");
                modal.find("#reject-normative").val(result.normative.nombre);
                modal.find("#reject-axe").val(result.axe.punto+". "+result.axe.nombre);
                modal.find("#reject-objective").val(result.objective.punto+". "+result.objective.nombre);
                modal.find("#reject-strategy").val(result.strategy.punto+". "+result.strategy.nombre);
                modal.find("#reject-action-line").val(result.punto+". "+result.nombre);
                modal.find("#reject-description").val(result.reject.descripcion);
                modal.modal("show");
            });
        });
        
        /*************** Events for INAFED ****************/

        $(document).on('dblclick', '.indicator-inf', function(){
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.inf.indicator.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                var modal = $("#modal-inf-indicatior");
                modal.find("#inf-normative").val(result.normative.nombre);
                modal.find("#inf-schedule").val(result.schedule.punto+". "+result.schedule.nombre);
                modal.find("#inf-axe").val(result.axe.punto+". "+result.axe.nombre);
                modal.find("#inf-topic").val(result.topic.punto+". "+result.topic.nombre);
                modal.find("#inf-indicator").val(result.punto+". "+result.nombre);
                modal.find(".id-inf-indicator").data("id", result.id);
                modal.modal("show");
            });
        });
        
        $(document).on('dblclick', '.indicator-accept-inf', function(){
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.inf.indicator.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                var modal = $("#modal-inf-indicatior-accepted");
                modal.find("#accept-inf-normative").val(result.normative.nombre);
                modal.find("#accept-inf-schedule").val(result.schedule.punto+". "+result.schedule.nombre);
                modal.find("#accept-inf-axe").val(result.axe.punto+". "+result.axe.nombre);
                modal.find("#accept-inf-topic").val(result.topic.punto+". "+result.topic.nombre);
                modal.find("#accept-inf-indicator").val(result.punto+". "+result.nombre);
                modal.modal("show");
            });
        });
        
        $(document).on('dblclick', '.indicator-reject-inf', function(){
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.inf.indicator.reject.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                var modal = $("#modal-inf-indicatior-rejected");
                modal.find("#reject-inf-normative").val(result.normative.nombre);
                modal.find("#reject-inf-schedule").val(result.schedule.punto+". "+result.schedule.nombre);
                modal.find("#reject-inf-axe").val(result.axe.punto+". "+result.axe.nombre);
                modal.find("#reject-inf-topic").val(result.topic.punto+". "+result.topic.nombre);
                modal.find("#reject-inf-indicator").val(result.punto+". "+result.nombre);
                modal.find("#reject-description-inf").val(result.reject.descripcion);
                modal.modal("show");
            });
        });

        /*************** Events for Schedule 2030 ****************/

        $(document).on('dblclick', '.meta-sche', function(){
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.sche.meta.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                console.log(result);
                var modal = $("#modal-sche-meta");
                modal.find("#sche-normative").val(result.normative.nombre);
                modal.find("#sche-objective").val(result.objective.punto+". "+result.objective.nombre);
                modal.find("#sche-meta").val(result.punto+". "+result.nombre);
                modal.find(".id-sche-indicator").data("id", result.id);
                modal.modal("show");
            });
        });

        $(document).on('dblclick', '.meta-accept-sche', function(){
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.sche.meta.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                var modal = $("#modal-sche-meta-accepted");
                modal.find("#accept-sche-normative").val(result.normative.nombre);
                modal.find("#accept-sche-objective").val(result.objective.punto+". "+result.objective.nombre);
                modal.find("#accept-sche-meta").val(result.punto+". "+result.nombre);
                modal.modal("show");
            });
        });

        $(document).on('dblclick', '.meta-reject-sche', function(){
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.sche.meta.reject.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                var modal = $("#modal-sche-meta-rejected");
                modal.find("#reject-sche-normative").val(result.normative.nombre);
                modal.find("#reject-sche-objective").val(result.objective.punto+". "+result.objective.nombre);
                modal.find("#reject-sche-meta").val(result.punto+". "+result.nombre);
                modal.find("#reject-description-sche").val(result.reject.descripcion);
                modal.modal("show");
            });
        });
    
        /*************** Events for MIR´s ****************/

        $(document).on('dblclick', '.indicator-mir', function(){
            var id = $(this).data("id"),
                direction = $(this).data("direction"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.mir.indicator.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                console.log(result);
                var modal = $("#modal-mir-indicator");

                modal.find("#mir-normative").val(result.normative.nombre);
                modal.find("#direction").val(direction);
                modal.find("#mir-program").val(result.program.nombre);
                modal.find("#mir-level").val(result.level.nombre);
                modal.find("#mir-operation").val(result.formula);
                modal.find("#mir-indicator").val(result.nombre);
                modal.find(".id-mir-indicator").data("id", result.id);
                modal.modal("show");
            });
        });

        $(document).on('dblclick', '.indicator-accept-mir', function(){
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.mir.indicator.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                var modal = $("#modal-mir-indicator-accepted");
                modal.find("#accept-mir-normative").val(result.normative.nombre);
                modal.find("#accept-mir-program").val(result.program.nombre);
                modal.find("#accept-mir-level").val(result.level.nombre);
                modal.find("#accept-mir-operation").val(result.formula);
                modal.find("#accept-mir-indicator").val(result.nombre);
                modal.modal("show");
            });
        });

        $(document).on('dblclick', '.indicator-reject-mir', function(){
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                url: "{{ route('get.mir.indicator.reject.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    id: id,
                },
            }).done(function(result){
                var modal = $("#modal-mir-indicator-rejected");
                modal.find("#reject-mir-normative").val(result.normative.nombre);
                modal.find("#reject-mir-program").val(result.program.nombre);
                modal.find("#reject-mir-level").val(result.level.nombre);
                modal.find("#reject-mir-operation").val(result.formula);
                modal.find("#reject-mir-indicator").val(result.nombre);
                modal.find("#reject-description-mir").val(result.reject.descripcion);
                modal.modal("show");
            });
        });
    });
</script>
@endsection