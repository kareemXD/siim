@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
    <div class="text-center mb-3">
        <h3>Actividades del Programa</h3>
        <h4>( {{ $program->nombre }} )</h4>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary text-center" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    <div class="row justify-content-end">
        <a href="{{ route('new.action.poa', $program->id) }}" class="btn btn-primary mr-3 mb-2"><i class="fas fa-plus-circle mr-2"></i> Agregar Actividad</a>
    </div>
    <div class="row mt-3">
        <div class="col-12">
            <table class="table dataTable">
                <thead>
                    <tr>
                        <th scope="col">Actividad</th>
                        <th scope="col">Meta Anual</th>
                        <th scope="col">1° Trimeste</th>
                        <th scope="col">2° Trimeste</th>
                        <th scope="col">3° Trimeste</th>
                        <th scope="col">4° Trimeste</th>
                        <th scope="col">Acciones</th>
                    </tr>
                </thead>
                <tbody id="actions-poa">
                    @foreach ($program->actions as $action)
                        <tr data-toggle="collapse" data-target="#collapse{{ $action->id }}" data-id="{{ $action->id }}">
                            <td class="col-6">{{ $action->actividad }}</td>
                            <td class="col-1">{{ $action->meta_anual }}</td>
                            <td class="col-1">{{ $action->trimestre_1 }}</td>
                            <td class="col-1">{{ $action->trimestre_2 }}</td>
                            <td class="col-1">{{ $action->trimestre_3 }}</td>
                            <td class="col-1">{{ $action->trimestre_4 }}</td>
                            <td class="col-1">
                                <a href="{{ route('edit.action.poa', $action->id) }}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Editar">
                                    <i class="fas fa-edit"></i>
                                </a>
                                <button type="button" class="btn btn-danger btn-delete-action" data-action="{{ $action->id }}" data-toggle="tooltip" data-placement="top" title="Eliminar">
                                    <i class="fas fa-trash-alt"></i>
                                </button>
                            </td>
                        </tr>
                        <tr class="no-hover son{{ $action->id }}">
                            <td colspan="6" style="padding:initial;">
                                <div id="collapse{{ $action->id }}" class="collapse ml-5" >
                                    <table class="table">
                                        <tr class="no-hover">
                                            <th scope="col">Subactividades</th>
                                            <th scope="col">Meta Anual</th>
                                            <th scope="col">1° Trimeste</th>
                                            <th scope="col">2° Trimeste</th>
                                            <th scope="col">3° Trimeste</th>
                                            <th scope="col">4° Trimeste</th>
                                        </tr>
                                        @if (count($action->subactions) > 0)
                                            @foreach ($action->subactions as $subaction)
                                                <tr>
                                                    <td class="col-7">{{ $subaction->actividad }}</td>
                                                    <td class="col-1">{{ $subaction->meta_anual }}</td>
                                                    <td class="col-1">{{ $subaction->trimestre_1 }}</td>
                                                    <td class="col-1">{{ $subaction->trimestre_2 }}</td>
                                                    <td class="col-1">{{ $subaction->trimestre_3 }}</td>
                                                    <td class="col-1">{{ $subaction->trimestre_4 }}</td>
                                                </tr>
                                            @endforeach
                                        @else
                                            <tr>
                                                <td colspan="6" class="text-center">Ninguna Subactividad Registrada</td>
                                            </tr>
                                        @endif
                                    </table>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
@endsection

@section('modals')

@endsection

@section('js')
<script>
    $(function(){
        $(document).on("click", ".btn-delete-action", function(){
            var action = $(this).data("action"),
                token = "{{ csrf_token() }}";
    
            var _this = $(this);
            $.confirm({
                title: '¿ Estas Seguro (a) ?',
                content: 'Se eliminara la actividad.',
                buttons: {
                    Confirmar: {
                        text: 'Confirmar',
                        btnClass: 'btn btn-primary btn-confirm',
                        action: function(){
                            $.ajax({
                                url: "{{ route('delete.action.poa') }}",
                                type: "POST",
                                data: {
                                    _token: token,
                                    action: action
                                },
                            }).done(function(result){
                                console.log(result);
                                $(".son"+action).fadeOut("slow");
                                _this.parents("tr").fadeOut("slow");
                            });
                        }
                    },
                    Cancelar: {
                        text: 'Cancelar',
                        btnClass: 'btn btn-secondary btn-confirm',
                    }
                }
            });
        });
    });
    </script>
@endsection