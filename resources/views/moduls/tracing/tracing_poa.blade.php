@extends('layouts.index')

@section('title') Avance del POA @endsection

@section('css')
    
@endsection

@section('content')
    <div class=" mb-3 text-center">
        <h3>Avance de POA</h3>
        <h4>( {{ Auth::user()->profile->position->direction->nombre }} )</h4>
        <h5>( <b>{{ $trimester["label"] }}</b> )</h5>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary text-center" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    <div class="row mt-3">
        <div class="col-12">
            <table class="table dataTable">
                <thead>
                    <tr>
                        <th scope="col" >Área o Proyecto Específico</th>
                        <th scope="col" >Objetivo</th>
                    </tr>
                </thead>
                <tbody id="actions-poa">
                    @php
                        if(config('implan.times.planning'))
                        {
                            $period = config('implan.periods.previous');
                        }else {
                            $period = config('implan.periods.actual');
                        }
                    @endphp
                    @foreach (Auth::user()->profile->position->direction->programs()->where("period", $period)->get() as $program)
                        <tr class="edit-program" data-toggle="collapse" data-target="#collapse{{ $program->id }}">
                            <td>{{ $program->nombre }}</td>
                            <td>{{ $program->descripcion }}</td>
                        </tr>
                        <tr class="no-hover son{{ $program->id }}" >
                            <td colspan="6" style="padding:initial;">
                                <div id="collapse{{ $program->id }}" class="collapse ml-5" >
                                    <table class="table table-bordered ">
                                        <thead class="no-hover">
                                            <tr>
                                                <th scope="col" style="text-align:center;" rowspan="2">Actividad</th>
                                                <th scope="col" style="text-align:center;" rowspan="2">U. de M.</th>
                                                @if ($trimester["number"] == 1)
                                                    <th scope="col" style="text-align:center;" colspan="5">1° Trimeste</th>
                                                @elseif ($trimester["number"] == 2)
                                                    <th scope="col" style="text-align:center;" colspan="5">2° Trimeste</th>
                                                @elseif ($trimester["number"] == 3)
                                                    <th scope="col" style="text-align:center;" colspan="5">3° Trimeste</th>
                                                @elseif ($trimester["number"] == 4)
                                                    <th scope="col" style="text-align:center;" colspan="5">4° Trimeste</th>
                                                @endif
                                                <th scope="col" style="text-align:center;" colspan="3">Meta Anual</th>
                                            </tr>
                                            <tr>
                                                <th scope="col" style="text-align:center;">P</th>
                                                <th scope="col" style="text-align:center;">R</th>
                                                @foreach ($trimester["mounths"] as $mounth)
                                                    <th scope="col" style="text-align:center;">{{ $mounth }}</th>
                                                @endforeach
                                                <th scope="col" style="text-align:center;">P</th>
                                                <th scope="col" style="text-align:center;">R</th>
                                                <th scope="col" style="text-align:center;">Avance</th>
                                            </tr>
                                        </thead>
                                        <tbody id="actions-poa">
                                            @if (count($program->actions) > 0)
                                                @foreach ($program->actions as $action)
                                                    <tr data-toggle="collapse" data-target="#collapse-action{{ $action->id }}" class="update-action" data-id="{{ $action->id }}">
                                                        <td class="">{{ $action->actividad }}</td>
                                                        <td class="">{{ $action->unidad_medida }}</td>
                                                        @if ($trimester["number"] == 1)
                                                            <td class="wd-3">{{ fmod($action->trimestre_1, 1) !== 0.00 ? $action->trimestre_1 : intval($action->trimestre_1) }}</td>
                                                            <td class="wd-3">{{ fmod($action->trimestre_1_r, 1) !== 0.00 ? $action->trimestre_1_r : intval($action->trimestre_1_r) }}</td>

                                                            <td class="wd-5">{{ fmod($action->enero, 1) !== 0.00 ? $action->enero : intval($action->enero) }}</td>
                                                            <td class="wd-5">{{ fmod($action->febrero, 1) !== 0.00 ? $action->febrero : intval($action->febrero) }}</td>
                                                            <td class="wd-5">{{ fmod($action->marzo, 1) !== 0.00 ? $action->marzo : intval($action->marzo) }}</td>
                                                        @elseif ($trimester["number"] == 2)
                                                            <td class="wd-3">{{ fmod($action->trimestre_2, 1) !== 0.00 ? $action->trimestre_2 : intval($action->trimestre_2) }}</td>
                                                            <td class="wd-3">{{ fmod($action->trimestre_2_r, 1) !== 0.00 ? $action->trimestre_2_r : intval($action->trimestre_2_r) }}</td>

                                                            <td class="wd-5">{{ fmod($action->abril, 1) !== 0.00 ? $action->abril : intval($action->abril) }}</td>
                                                            <td class="wd-5">{{ fmod($action->mayo, 1) !== 0.00 ? $action->mayo : intval($action->mayo) }}</td>
                                                            <td class="wd-5">{{ fmod($action->junio, 1) !== 0.00 ? $action->junio : intval($action->junio) }}</td>
                                                        @elseif ($trimester["number"] == 3)
                                                            <td class="wd-3">{{ fmod($action->trimestre_3, 1) !== 0.00 ? $action->trimestre_3 : intval($action->trimestre_3) }}</td>
                                                            <td class="wd-3">{{ fmod($action->trimestre_3_p, 1) !== 0.00 ? $action->trimestre_3_p : intval($action->trimestre_3_p) }}</td>

                                                            <td class="wd-5">{{ fmod($action->julio, 1) !== 0.00 ? $action->julio : intval($action->julio) }}</td>
                                                            <td class="wd-5">{{ fmod($action->agosto, 1) !== 0.00 ? $action->agosto : intval($action->agosto) }}</td>
                                                            <td class="wd-5">{{ fmod($action->septiembre, 1) !== 0.00 ? $action->septiembre : intval($action->septiembre) }}</td>
                                                        @elseif ($trimester["number"] == 4 )
                                                            <td class="wd-3">{{ fmod($action->trimestre_4, 1) !== 0.00 ? $action->trimestre_4 : intval($action->trimestre_4) }}</td>
                                                            <td class="wd-3">{{ fmod($action->trimestre_4_r, 1) !== 0.00 ? $action->trimestre_4_r : intval($action->trimestre_4_r) }}</td>

                                                            <td class="wd-5">{{ fmod($action->octubre, 1) !== 0.00 ? $action->octubre : intval($action->octubre) }}</td>
                                                            <td class="wd-5">{{ fmod($action->noviembre, 1) !== 0.00 ? $action->noviembre : intval($action->noviembre) }}</td>
                                                            <td class="wd-5">{{ fmod($action->diciembre, 1) !== 0.00 ? $action->diciembre : intval($action->diciembre) }}</td>
                                                        @endif
                                                        
                                                        <td class="wd-5">{{ fmod($action->meta_anual, 1) !== 0.00 ? $action->meta_anual : intval($action->meta_anual) }}</td>
                                                        <td class="wd-5">{{ fmod($action->meta_anual_r, 1) !== 0.00 ? $action->meta_anual_r : intval($action->meta_anual_r) }}</td>
                                                        <td class="wd-5 {{ $action->getAdvanceColor() }}">{{ $action->getAdvance() }}%</td>
                                                    </tr>
                                                    @if (count($action->subactions) > 0)
                                                        <tr class="no-hover">
                                                            <td colspan="9" style="padding:initial;">
                                                                <div id="collapse-action{{ $action->id }}" class="collapse ml-5" >
                                                                    <table class="table">
                                                                        <thead>
                                                                            <tr>
                                                                                <th scope="col" style="text-align:center;" rowspan="2">Subactividad</th>
                                                                                @if ($trimester["number"] == 1)
                                                                                    <th scope="col" style="text-align:center;" colspan="5">1° Trimeste</th>
                                                                                @elseif ($trimester["number"] == 2)
                                                                                    <th scope="col" style="text-align:center;" colspan="5">2° Trimeste</th>
                                                                                @elseif ($trimester["number"] == 3)
                                                                                    <th scope="col" style="text-align:center;" colspan="5">3° Trimeste</th>
                                                                                @elseif ($trimester["number"] == 4)
                                                                                    <th scope="col" style="text-align:center;" colspan="5">4° Trimeste</th>
                                                                                @endif
                                                                                <th scope="col" style="text-align:center;" colspan="3">Meta Anual</th>
                                                                            </tr>
                                                                            <tr>
                                                                                <th scope="col" style="text-align:center;">P</th>
                                                                                <th scope="col" style="text-align:center;">R</th>
                                                                                @foreach ($trimester["mounths"] as $mounth)
                                                                                    <th scope="col" style="text-align:center;">{{ $mounth }}</th>
                                                                                @endforeach
                                                                                <th scope="col" style="text-align:center;">P</th>
                                                                                <th scope="col" style="text-align:center;">R</th>
                                                                                <th scope="col" style="text-align:center;">Avance</th>
                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>
                                                                            
                                                                            @foreach ($action->subactions as $subaction)
                                                                                <tr data-toggle="collapse" data-target="#collapse-subaction{{ $subaction->id }}">
                                                                                    <td class="col-6">{{ $subaction->actividad }}</td>
                                                                                    
                                                                                    @if ($trimester["number"] == 1)
                                                                                        <td class="wd-3">{{ fmod($subaction->trimestre_1, 1) !== 0.00 ? $subaction->trimestre_1 : intval($subaction->trimestre_1) }}</td>
                                                                                        <td class="wd-3">{{ fmod($subaction->trimestre_1_r, 1) !== 0.00 ? $subaction->trimestre_1_r : intval($subaction->trimestre_1_r) }}</td>

                                                                                        <td class="wd-5">{{ fmod($subaction->enero, 1) !== 0.00 ? $subaction->enero : intval($subaction->enero) }}</td>
                                                                                        <td class="wd-5">{{ fmod($subaction->febrero, 1) !== 0.00 ? $subaction->febrero : intval($subaction->febrero) }}</td>
                                                                                        <td class="wd-5">{{ fmod($subaction->marzo, 1) !== 0.00 ? $subaction->marzo : intval($subaction->marzo) }}</td>
                                                                                    @elseif ($trimester["number"] == 2)
                                                                                        <td class="wd-3">{{ fmod($subaction->trimestre_2, 1) !== 0.00 ? $subaction->trimestre_2 : intval($subaction->trimestre_2) }}</td>
                                                                                        <td class="wd-3">{{ fmod($subaction->trimestre_2_r, 1) !== 0.00 ? $subaction->trimestre_2_r : intval($subaction->trimestre_2_r) }}</td>

                                                                                        <td class="wd-5">{{ fmod($subaction->abril, 1) !== 0.00 ? $subaction->abril : intval($subaction->abril) }}</td>
                                                                                        <td class="wd-5">{{ fmod($subaction->mayo, 1) !== 0.00 ? $subaction->mayo : intval($subaction->mayo) }}</td>
                                                                                        <td class="wd-5">{{ fmod($subaction->junio, 1) !== 0.00 ? $subaction->junio : intval($subaction->junio) }}</td>
                                                                                    @elseif ($trimester["number"] == 3)
                                                                                        <td class="wd-3">{{ fmod($subaction->trimestre_3, 1) !== 0.00 ? $subaction->trimestre_3 : intval($subaction->trimestre_3) }}</td>
                                                                                        <td class="wd-3">{{ fmod($subaction->trimestre_3_p, 1) !== 0.00 ? $subaction->trimestre_3_p : intval($subaction->trimestre_3_p) }}</td>

                                                                                        <td class="wd-5">{{ fmod($subaction->julio, 1) !== 0.00 ? $subaction->julio : intval($subaction->julio) }}</td>
                                                                                        <td class="wd-5">{{ fmod($subaction->agosto, 1) !== 0.00 ? $subaction->agosto : intval($subaction->agosto) }}</td>
                                                                                        <td class="wd-5">{{ fmod($subaction->septiembre, 1) !== 0.00 ? $subaction->septiembre : intval($subaction->septiembre) }}</td>
                                                                                    @elseif ($trimester["number"] == 4)
                                                                                        <td class="wd-3">{{ fmod($subaction->trimestre_4, 1) !== 0.00 ? $subaction->trimestre_4 : intval($subaction->trimestre_4) }}</td>
                                                                                        <td class="wd-3">{{ fmod($subaction->trimestre_4_r, 1) !== 0.00 ? $subaction->trimestre_4_r : intval($subaction->trimestre_4_r) }}</td>

                                                                                        <td class="wd-5">{{ fmod($subaction->octubre, 1) !== 0.00 ? $subaction->octubre : intval($subaction->octubre) }}</td>
                                                                                        <td class="wd-5">{{ fmod($subaction->noviembre, 1) !== 0.00 ? $subaction->noviembre : intval($subaction->noviembre) }}</td>
                                                                                        <td class="wd-5">{{ fmod($subaction->diciembre, 1) !== 0.00 ? $subaction->diciembre : intval($subaction->diciembre) }}</td>
                                                                                    @endif
                                                                                    <td class="col-1">{{ fmod($subaction->meta_anual, 1) !== 0.00 ? $subaction->meta_anual : intval($subaction->meta_anual) }}</td>
                                                                                    <td class="col-1">{{ fmod($subaction->meta_anual_r, 1) !== 0.00 ? $subaction->meta_anual_r : intval($subaction->meta_anual_r) }}</td>
                                                                                    <td class="col-1 {{ $subaction->getAdvanceColor() }}">{{ $subaction->getAdvance() }}%</td>
                                                                                </tr>
                                                                            @endforeach
                                                                        </tbody>
                                                                    </table>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="6" class="text-center">Ninguna Actividad Registrada</td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    
@endsection

@section('modals')
    @include('partials.modals.tracing.modal_update_action_goal')
@endsection

@section('js')
<script>
$(function(){
    var trimestre = "";
    @if ($trimester["number"] == 1)
        trimestre = '<th style="text-align:center;" colspan="5">1° Trimeste</th>';
    @elseif ($trimester["number"] == 2)
        trimestre = '<th style="text-align:center;" colspan="5">2° Trimeste</th>';
    @elseif ($trimester["number"] == 3)
        trimestre = '<th style="text-align:center;" colspan="5">3° Trimeste</th>';
    @elseif ($trimester["number"] == 4)
        trimestre = '<th style="text-align:center;" colspan="5">4° Trimeste</th>';
    @endif

    var meses = "";

    @foreach ($trimester["mounths"] as $mounth)
        meses += '<th style="text-align:center;">{{ $mounth }}</th>';
    @endforeach

    $(document).on("dblclick", ".update-action", function(){
        var action = $(this).data("id"),
            token = "{{ csrf_token() }}";

            $.ajax({
                url: "{{ route('get.action.poa.by.id') }}",
                type: "POST",
                data: {
                    _token: token,
                    action: action
                },
            }).done(function(result){
                var modal = $("#modal-update-action-goal");
                modal.find("#action_id").val(result.id);
                modal.find("#action").val(result.actividad);
                modal.find("#udem").val(result.unidad_medida);
                modal.find("#anual-p").val(result.meta_anual);
                modal.find("#anual-r").val(result.meta_anual_r);
                modal.find("#anual-r-initial").val(result.meta_anual_r);
                
                @if ($trimester["number"] == 1)
                    modal.find("#Enero").val(result.enero);
                    modal.find("#Febrero").val(result.febrero);
                    modal.find("#Marzo").val(result.marzo);

                    modal.find("#trimestre-p").val(result.trimestre_1);
                    modal.find("#trimestre-r").val(result.trimestre_1_r);

                    modal.find("#Enero").removeAttr('readonly');
                    modal.find("#Febrero").removeAttr('readonly');
                    modal.find("#Marzo").removeAttr('readonly');

                @elseif ($trimester["number"] == 2)
                    modal.find("#Abril").val(result.abril);
                    modal.find("#Mayo").val(result.mayo);
                    modal.find("#Junio").val(result.junio);

                    modal.find("#trimestre-p").val(result.trimestre_2);
                    modal.find("#trimestre-r").val(result.trimestre_2_r);

                    modal.find("#Abril").removeAttr('readonly');
                    modal.find("#Mayo").removeAttr('readonly');
                    modal.find("#Junio").removeAttr('readonly');

                @elseif ($trimester["number"] == 3)
                    modal.find("#Julio").val(result.julio);
                    modal.find("#Agosto").val(result.agosto);
                    modal.find("#Septiembre").val(result.septiembre);

                    modal.find("#trimestre-p").val(result.trimestre_3);
                    modal.find("#trimestre-r").val(result.trimestre_3_p);
                    modal.find("#Julio").removeAttr('readonly');
                    modal.find("#Agosto").removeAttr('readonly');
                    modal.find("#Septiembre").removeAttr('readonly');
                
                @elseif ($trimester["number"] == 4)
                    modal.find("#Octubre").val(result.octubre);
                    modal.find("#Noviembre").val(result.noviembre);
                    modal.find("#Diciembre").val(result.diciembre);

                    modal.find("#trimestre-p").val(result.trimestre_4);
                    modal.find("#trimestre-r").val(result.trimestre_4_r);
                    
                    modal.find("#Octubre").removeAttr('readonly');
                    modal.find("#Noviembre").removeAttr('readonly');
                    modal.find("#Diciembre").removeAttr('readonly');
                @endif
                modal.find("#subactions").empty();
                if(result.sub.length > 0)
                {
                    result.sub.forEach(element => {
                        var sub_month = '';
                        var sub_trimonth = "";
                        @if ($trimester["number"] == 1)
                            sub_month += '<td class="wd-14"><input required type="number" step=".01" min="0" class="form-control sub_mounth" data-parent="Enero" name="sub_enero[]" value="'+ element.enero +'"></td>';
                            sub_month += '<td class="wd-14"><input required type="number" step=".01" min="0" class="form-control sub_mounth" data-parent="Febrero" name="sub_febrero[]" value="'+ element.febrero +'"></td>';
                            sub_month += '<td class="wd-14"><input required type="number" step=".01" min="0" class="form-control sub_mounth" data-parent="Marzo" name="sub_marzo[]" value="'+ element.marzo +'"></td>';

                            sub_trimonth += '<td class="wd-14"><input disabled type="number" step=".01" min="0" class="form-control" value="'+element.trimestre_1+'"></td>\
                                            <td class="wd-14"><input disabled type="number" step=".01" min="0" class="sub-trimestre-r form-control" value="'+element.trimestre_1_r+'"></td>';
                            
                            modal.find("#Enero").attr('readonly', true);
                            modal.find("#Febrero").attr('readonly', true);
                            modal.find("#Marzo").attr('readonly', true);
                        @elseif ($trimester["number"] == 2)
                            sub_month += '<td class="wd-14"><input required type="number" step=".01" min="0" class="form-control sub_mounth" data-parent="Abril" name="sub_abril[]" value="'+ element.abril +'"></td>';
                            sub_month += '<td class="wd-14"><input required type="number" step=".01" min="0" class="form-control sub_mounth" data-parent="Mayo" name="sub_mayo[]" value="'+ element.mayo +'"></td>';
                            sub_month += '<td class="wd-14"><input required type="number" step=".01" min="0" class="form-control sub_mounth" data-parent="Junio" name="sub_junio[]" value="'+ element.junio +'"></td>';

                            sub_trimonth += '<td class="wd-14"><input disabled type="number" step=".01" min="0" class="form-control" value="'+element.trimestre_2+'"></td>\
                                            <td class="wd-14"><input disabled type="number" step=".01" min="0" class="sub-trimestre-r form-control" value="'+element.trimestre_2_r+'"></td>';

                            modal.find("#Abril").attr('readonly', true);
                            modal.find("#Mayo").attr('readonly', true);
                            modal.find("#Junio").attr('readonly', true);
                        @elseif ($trimester["number"] == 3)
                            sub_month += '<td class="wd-14"><input required type="number" step=".01" min="0" class="form-control sub_mounth" data-parent="Julio" name="sub_julio[]" value="'+ element.julio +'"></td>';
                            sub_month += '<td class="wd-14"><input required type="number" step=".01" min="0" class="form-control sub_mounth" data-parent="Agosto" name="sub_agosto[]" value="'+ element.agosto +'"></td>';
                            sub_month += '<td class="wd-14"><input required type="number" step=".01" min="0" class="form-control sub_mounth" data-parent="Septiembre" name="sub_septiembre[]" value="'+ element.septiembre +'"></td>';

                            sub_trimonth += '<td class="wd-14"><input disabled type="number" step=".01" min="0" class="form-control" value="'+element.trimestre_3+'"></td>\
                                            <td class="wd-14"><input disabled type="number" step=".01" min="0" class="sub-trimestre-r form-control" value="'+element.trimestre_3_p+'"></td>';
                            
                            modal.find("#Julio").attr('readonly', true);
                            modal.find("#Agosto").attr('readonly', true);
                            modal.find("#Septiembre").attr('readonly', true);
                        @elseif ($trimester["number"] == 4)
                            sub_month += '<td class="wd-14"><input required type="number" step=".01" min="0" class="form-control sub_mounth" data-parent="Octubre" name="sub_octubre[]" value="'+ element.octubre +'"></td>';
                            sub_month += '<td class="wd-14"><input required type="number" step=".01" min="0" class="form-control sub_mounth" data-parent="Noviembre" name="sub_noviembre[]" value="'+ element.noviembre +'"></td>';
                            sub_month += '<td class="wd-14"><input required type="number" step=".01" min="0" class="form-control sub_mounth" data-parent="Diciembre" name="sub_diciembre[]" value="'+ element.diciembre +'"></td>';

                            sub_trimonth += '<td class="wd-14"><input disabled type="number" step=".01" min="0" class="form-control" value="'+element.trimestre_4+'"></td>\
                                            <td class="wd-14"><input disabled type="number" step=".01" min="0" class="sub-trimestre-r form-control" value="'+element.trimestre_4_r+'"></td>';
                        
                            modal.find("#Octubre").attr('readonly', true);
                            modal.find("#Noviembre").attr('readonly', true);
                            modal.find("#Diciembre").attr('readonly', true);
                        @endif

                        modal.find("#subactions").append('<div class="form-row">\
                                <input name="subactions[]" type="hidden" value="'+element.id+'">\
                                <div class="form-group col">\
                                    <label for="action">Subactividad</label>\
                                    <input disabled type="text" class="form-control" value="'+element.actividad+'">\
                                </div>\
                            </div>\
                            <div class="form-row">\
                                <table class="table table-sm">\
                                    <thead class="no-hover">\
                                        <tr>\
                                            <th style="text-align:center;" colspan="2">Meta Anual</th>\
                                            '+trimestre+'\
                                        </tr>\
                                        <tr>\
                                            <th style="text-align:center;">P</th>\
                                            <th style="text-align:center;">R</th>\
                                            '+meses+'\
                                            <th style="text-align:center;">P</th>\
                                            <th style="text-align:center;">R</th>\
                                        </tr>\
                                    </thead>\
                                    <tbody id="actions-poa">\
                                        <tr>\
                                            <td class="wd-14"><input id="anual-p" disabled type="number" step=".01" min="0" class="form-control" value="'+element.meta_anual+'"></td>\
                                            <td class="wd-14">\
                                                <input class="sub-old-value" disabled type="hidden" >\
                                                <input disabled type="number" step=".01" min="0" class="form-control sub-anual-r" value="'+element.meta_anual_r+'">\
                                            </td>\
                                            '+ sub_month +'\
                                            '+ sub_trimonth +'\
                                        </tr>\
                                    </tbody>\
                                </table>\
                            </div>'
                        );
                    });
                }
                modal.modal("show");
            });
        // var _this = $(this);
        // $.confirm({
        //     title: '¿ Estas Seguro (a) ?',
        //     content: 'Se eliminara el programa y todas las actividades que lo compongan.',
        //     buttons: {
        //         Confirmar: {
        //             text: 'Confirmar',
        //             btnClass: 'btn btn-primary btn-confirm',
        //             action: function(){
        //                 $.ajax({
        //                     url: "{{ route('delete.program') }}",
        //                     type: "POST",
        //                     data: {
        //                         _token: token,
        //                         program: program
        //                     },
        //                 }).done(function(result){
        //                     console.log(result);
        //                     $(".son"+program).fadeOut("slow");
        //                     _this.parents("tr").fadeOut("slow");
        //                 });
        //             }
        //         },
        //         Cancelar: {
        //             text: 'Cancelar',
        //             btnClass: 'btn btn-secondary btn-confirm',
        //         }
        //     }
        // });
    });
});
</script>
@endsection