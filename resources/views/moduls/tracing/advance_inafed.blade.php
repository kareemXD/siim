@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class="row justify-content-center mb-3">
    <h3>Avance de INAFED por Dirección</h3>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif
<div class="row justify-content-center">
    <div class="col-12 col-xl-8">
        <table class="table dataTable">
            <thead>
                <tr>
                    <th scope="col">Direccion</th>
                    <th scope="col">POA</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($directions as $direction)
                    @if(empty($direction->padre))
                        
                        @if (count($direction->subdirections) > 0)
                            <tr data-toggle="collapse" data-target="#collapse{{ $direction->id }}">
                                <td>
                                    {{ $direction->nombre }}
                                </td>
                                <td>
                                    <a href="{{ route('print.inafed.advance', $direction->id) }}" target="_BLANK" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Generar Avance"><i class="fas fa-file-pdf"></i></a>
                                </td>
                            </tr>
                            <tr class="no-hover" >
                                <td style="padding:initial;">
                                    <div id="collapse{{ $direction->id }}" class="collapse ml-5" >
                                        <table class="table table-bordered ">
                                            <thead class="no-hover">
                                                <tr>
                                                    <th>Regidor</th>
                                                    <th>POA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($direction->subdirections as $subdirections)
                                                    <tr>
                                                        <td>
                                                            {{ $subdirections->nombre }}
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('print.inafed.advance', $subdirections->id) }}" target="_BLANK" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Generar Avance"><i class="fas fa-file-pdf"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td>
                                    {{ $direction->nombre }}
                                </td>
                                <td>
                                    <a href="{{ route('print.inafed.advance', $direction->id) }}" target="_BLANK" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Generar Avance"><i class="fas fa-file-pdf"></i></a>
                                </td>
                            </tr>
                        @endif
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>
    
@endsection

@section('modals')

@endsection

@section('js')
@endsection