@extends('layouts.index')

@section('title') Actividades Mesuales @endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
@endsection

@section('content')
    <div class="row justify-content-center">
        <h3>Informe Mensual</h3>
    </div>
    @if(session()->has('response'))
        @if(session('response')["status"] == true)
            <div class="alert alert-primary" role="alert">
                {{ session("response")["message"] }}
            </div>
        @else
            <div class="alert alert-danger" role="alert">
                {{ session("response")["message"] }}
            </div>
        @endif
    @endif
    <div class="row justify-content-end">
        <button type="button" class="btn btn-primary mr-3 mb-2" data-toggle="modal" data-target="#modal-add-monthly-activity"><i class="fas fa-plus-circle mr-2"></i> Agregar Actividades</button>
    </div>
    <div class="table-responsive">
        <table class="table dataTable table-sm">
            <thead>
                <tr>
                    <th scope="col">Año</th>
                    <th scope="col">Mes</th>
                    <th scope="col">Localidad(es)</th>
                    <th scope="col">Nombre o Tema de la Actividad Relevante</th>
                    <th scope="col">Monto</th>
                    <th scope="col">Beneficiarios</th>
                </tr>
            </thead>
            <tbody>
                @if (count($direction->activities) > 0)
                    @foreach ($direction->activities()->orderBy('year', 'ASC')->orderBy('mes', 'ASC')->get() as $activity)
                        <tr class="tr-row" data-id="{{ $activity->id }}">
                            <td>{{ $activity->year }}</td>
                            <td>{{ $meses_nor[$activity->mes - 1] }}</td>
                            <td>
                                @foreach ($activity->locations as $location)
                                {{ $location->nombre }},         
                                @endforeach
                            </td>
                            <td>{{ $activity->accion }}</td>
                            <td>$ {{ $activity->monto }}</td>
                            <td>{{ $activity->beneficiarios_h }} Hombre, {{ $activity->beneficiarios_m }} Mujeres</td>
                        </tr>
                    @endforeach
                @endif
                
            </tbody>
        </table>
    </div>
@endsection

@section('modals')
    @include('partials.modals.tracing.modal_add_monthly_activity')
    @include('partials.modals.tracing.modal_view_monthly_activity')
@endsection

@section('js')
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script>
        $(function(){
            
            $(".dataTable").DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });

            $(document).on("dblclick", ".tr-row", function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.monthly.activity.by.id') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id,
                    },
                }).done(function(result){
                    console.log(result);

                    var modal = $("#modal-view-monthly-activity");
                    modal.find("#view-mounth").val(result.mes+"-"+result.year);
                    modal.find("#view-location").val(result.location);
                    $(".select2").select2({
                        theme: "bootstrap"
                    });
                    modal.find("#view-location").val(result.localidad_id);
                    modal.find("#view-name2").val(result.accion);
                    modal.find("#view-beneficiaries-men").val(result.beneficiarios_h);
                    modal.find("#view-beneficiaries-women").val(result.beneficiarios_m);
                    modal.find("#view-amount").val(result.monto);
                    modal.find("#view-description").val(result.descripcion);
                    $(".div-evidences").find("ul").empty();
                    result.inf_evidences.forEach(element => {
                        $(".div-evidences").find("ul").append('<li class="download-file" data-id="'+element.id+'">\
                            <img src="'+element.storage+'" >\
                        </li>');
                    });
                    
                    modal.modal("show");
                });
            });
            
            $(document).on("dblclick", ".download-file", function(){
                var id = $(this).data("id");
                window.open('/seguimiento/descargar/evidencia/'+id, '_blank');
            });
            
        });
    </script>
@endsection