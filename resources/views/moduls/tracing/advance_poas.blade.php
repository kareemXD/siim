@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class="row justify-content-center mb-3">
    <h3>Avance de POA por Dirección</h3>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif
<form id="form-advance" method="get" action="{{ route('view.poa.advance') }}" class="row justify-content-center">
    @csrf
    @can('write_advance_poas')
        <div class="col-12 col-xl-8">
            <div class="form-row">
                <div class="form-group col">
                    <label for="trimester">Trimestre</label>
                    <input type="hidden" name="direction">
                    <select class="form-control  select2" name="trimester">
                        <option value="">Seleccione Trimestre</option>
                        <option value="1">1° Trimestre</option>
                        <option value="2">2° Trimestre</option>
                        <option value="3">3° Trimestre</option>
                        <option value="4">4° Trimestre</option>
                    </select>
                </div>
            </div> 
        </div> 
    @endcan
    <div class="col-12 col-xl-8">
        {{-- <table class="table dataTable">
            <thead>
                <tr>
                    <th scope="col">Direccion</th>
                    <th scope="col">POA</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($directions as $direction)
                <tr>
                    <td>
                        <a class="@can('write_advance_poas') btn-direction @endcan" href="#!" data-direction="{{ $direction->id }}" >{{ $direction->nombre }}</a>
                    </td>
                    <td>
                        <a href="{{ route('print.poa.advance', $direction->id) }}" target="_BLANK" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Generar POA"><i class="fas fa-file-pdf"></i></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table> --}}
        <table class="table dataTable">
            <thead>
                <tr>
                    <th scope="col">Direccion</th>
                    <th scope="col">POA</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($directions as $direction)
                    @if(empty($direction->padre))
                        
                        @if (count($direction->subdirections) > 0)
                            <tr data-toggle="collapse" data-target="#collapse{{ $direction->id }}">
                                <td>
                                    <a class="@can('write_advance_poas') btn-direction @endcan" href="#!" data-direction="{{ $direction->id }}" >{{ $direction->nombre }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('print.poa.advance', $direction->id) }}" target="_BLANK" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Generar POA"><i class="fas fa-file-pdf"></i></a>
                                </td>
                            </tr>
                            <tr class="no-hover" >
                                <td style="padding:initial;">
                                    <div id="collapse{{ $direction->id }}" class="collapse ml-5" >
                                        <table class="table table-bordered ">
                                            <thead class="no-hover">
                                                <tr>
                                                    <th>Regidor</th>
                                                    <th>POA</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($direction->subdirections as $subdirections)
                                                    <tr>
                                                        <td>
                                                            <a class="@can('write_advance_poas') btn-direction @endcan" href="#!" data-direction="{{ $subdirections->id }}" >{{ $subdirections->nombre }}</a>
                                                        </td>
                                                        <td>
                                                            <a href="{{ route('print.poa.advance', $subdirections->id) }}" target="_BLANK" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Generar POA"><i class="fas fa-file-pdf"></i></a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        @else
                            <tr>
                                <td>
                                    <a class="@can('write_advance_poas') btn-direction @endcan" href="#!" data-direction="{{ $direction->id }}" >{{ $direction->nombre }}</a>
                                </td>
                                <td>
                                    <a href="{{ route('print.poa.advance', $direction->id) }}" target="_BLANK" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Generar POA"><i class="fas fa-file-pdf"></i></a>
                                </td>
                            </tr>
                        @endif
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
</form>
    
@endsection

@section('modals')

@endsection

@section('js')
<script>
    $(function(){
        $(document).on("click", ".btn-direction", function(){
            var direction = $(this).data("direction");

            $("[name='direction']").val(direction);

            $("#form-advance").submit();
        });
    });
</script>
    
@endsection