@extends('layouts.index')

@section('title') Registro @endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/dropzone2/dropzone.css') }}">
@endsection

@section('content')
    <div class="row justify-content-center">
        <h3>Registro</h3>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary" role="alert">
            {{ session("alert") }}
        </div>
    @endif
        <div class="container">
            <div class="alert alert-danger message" style="display:none;" role="alert"></div>
            <div class="row">
                <div class="col-xl-12">
                    <form style="display:none;" action="{{route('store.shape') }}"
                        class="dropzone"
                        id="my-awesome-dropzone">
                        {{-- <input type="file" name="file"  > --}}
                        @csrf
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <label for="localidades">Seleccione una localidad</label>
                    <select class="form-control localidad" name="localidad" id="localidades">
                         <option value="" selected>Seleccione una localidad</option>
                        @foreach ($localidades as $localidad)
                            <option value="{{$localidad->shape}}">{{$localidad->nombre}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-xl-2">
                    <label for="">Adjunte Shape hidraulico</label>
                    <input type="file" name="hidrualica"  class="file2 hidraulica" id="">
                </div>
                
                <div class="col-xl-2">
                    <label for="">Adjunte Shape sanitario</label>
                    <input type="file" name="sanitaria" class="file2 sanitaria"  id="">
                </div>
                <div class="col-xl-2">
                    <label for="">Adjunte Shape Predio</label>
                    <input type="file" name="predios"  class="file2 predios" id="">
                </div>
                <div class="col-xl-2">
                    <label for="">Adjunte Shape Poligono</label>
                    <input type="file" name="poligonos"  class="file2 poligono" id="">
                </div>
                <div class="col-xl-2 mt-4">
                    <label for="">Adjunte Shape Pozos</label>
                    <input type="file" name="pozos"  class="file2 pozos" id="">
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt-5">
            <button type="button" class="btn btn-primary mr-2 mb-2 saveFiles"><i class="fas fa-save mr-2"></i> Guardar</button>
        </div>

        {{-- <div id="map"></div> --}}
@endsection

@section('modals')
@endsection

@section('js')
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script src="{{ asset('assets/plugins/dropzone2/dropzone.js') }}"></script>
    <script src="{{ asset('assets/plugins/validate/validate.js') }}"></script>
    <script src="{{ asset('assets/plugins/validate/methods.js') }}"></script>
<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>

</body>
</html>
<script>
    $('.file2').simpleFilePreview();

    Dropzone.autoDiscover = false;
    var token = $('input[name=_token]').val();

    // A quick way setup
    var myDropzone = new Dropzone("#my-awesome-dropzone", {
        // Setup chunking
        chunking: true,
        method: "POST",
        maxFilesize: 400000000,
        chunkSize: 400000000,
        timeout: 180000,
        // If true, the individual chunks of a file are being uploaded simultaneously.
        parallelChunkUploads: true,
        autoProcessQueue: false,
        parallelUploads: 10
    });

    // Append token to the request - required for web routes
    myDropzone.on('sending', function (file, xhr, formData) {
        $('.wrapper-spinner-initial').fadeIn('fast');
        formData.append("_token", token);
        formData.append("hidraulica",$('.hidraulica')[0].files[0]);
        formData.append("sanitaria", $('.sanitaria')[0].files[0]);
        formData.append("poligono", $('.poligono')[0].files[0]);
        formData.append("predios", $('.predios')[0].files[0]);
        formData.append("pozos", $('.pozos')[0].files[0]);
        formData.append("localidad", $('.localidad').val());
    });

    myDropzone.on('success', function (data, response) {
        // $('.wrapper-spinner-initial').fadeOut('fast');
        // if(response == false)
        // {
        //     $(".message").html("Algunos de los archivos shapes se encuentran dañados");
        // }
        // else
        // {
        //     location.href = '{{route("oromapas")}}';
        // }
        if(response == "1")
        {
            window.location.reload()
        }
        if(response == "2")
        {
            window.location.reload()
        }
        if(response == "3")
        {
            location.href = '{{route("oromapas")}}';
        }
        // console.log(response);
        
        // location.href = '{{route("oromapas")}}';
    });

    myDropzone.on("totaluploadprogress", function(progress) {
        $(".progress-bar").width(progress + '%');
    });

    $('.saveFiles').click(function (e) {
        var localidad =  $('.localidad').val();
        if(localidad != "" )
        {
            jQuery.validator.setDefaults({
                debug: true,
                success: "valid"
            });
            var form = $('.dropzone');
            form.validate();
            if (form.valid() == true) { 
                if (myDropzone.getQueuedFiles().length > 0) {
                    myDropzone.processQueue();  
                } else {                     
                    myDropzone._uploadData([{ upload: { filename: '' } }], [{ filename: '', name: '', data: new Blob() }]);
                }                                    
            }      
        }
        else
        {
            $('.message').html('Debe seleccionar una localidad');
            $('.message').show();
        }
    });
</script>
@endsection
