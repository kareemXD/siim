@extends('layouts.index')

@section('title') Oromapas @endsection

@section('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/leaflet.css') }}" />
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.css"/> --}}
<link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/pm/pm.css') }}" />
<link rel="stylesheet" href="{{ asset('/assets/plugins/leaflet/groups/dist/leaflet.groupedlayercontrol.min.css') }}" />
<style>
  .map {
        height: 650px;
        width: 100%;
      }
  .sanitariaMap {
        height: 600px;
        width: 100%;
      }
      .switch {
  font-size: 1rem;
  position: relative;
}
.switch input {
  position: absolute;
  height: 1px;
  width: 1px;
  background: none;
  border: 0;
  clip: rect(0 0 0 0);
  clip-path: inset(50%);
  overflow: hidden;
  padding: 0;
}
.switch input + label {
  position: relative;
  min-width: calc(calc(2.375rem * .8) * 2);
  border-radius: calc(2.375rem * .8);
  height: calc(2.375rem * .8);
  line-height: calc(2.375rem * .8);
  display: inline-block;
  cursor: pointer;
  outline: none;
  user-select: none;
  vertical-align: middle;
  text-indent: calc(calc(calc(2.375rem * .8) * 2) + .5rem);
}
.switch input + label::before,
.switch input + label::after {
  content: '';
  position: absolute;
  top: 0;
  left: 0;
  width: calc(calc(2.375rem * .8) * 2);
  bottom: 0;
  display: block;
}
.switch input + label::before {
  right: 0;
  background-color: #dee2e6;
  border-radius: calc(2.375rem * .8);
  transition: 0.2s all;
}
.switch input + label::after {
  top: 2px;
  left: 2px;
  width: calc(calc(2.375rem * .8) - calc(2px * 2));
  height: calc(calc(2.375rem * .8) - calc(2px * 2));
  border-radius: 50%;
  background-color: white;
  transition: 0.2s all;
}
.switch input:checked + label::before {
  background-color: #08d;
}
.switch input:checked + label::after {
  margin-left: calc(2.375rem * .8);
}
.switch input:focus + label::before {
  outline: none;
  box-shadow: 0 0 0 0.2rem rgba(0, 136, 221, 0.25);
}
.switch input:disabled + label {
  color: #868e96;
  cursor: not-allowed;
}
.switch input:disabled + label::before {
  background-color: #e9ecef;
}
.switch.switch-sm {
  font-size: 0.875rem;
}
.switch.switch-sm input + label {
  min-width: calc(calc(1.9375rem * .8) * 2);
  height: calc(1.9375rem * .8);
  line-height: calc(1.9375rem * .8);
  text-indent: calc(calc(calc(1.9375rem * .8) * 2) + .5rem);
}
.switch.switch-sm input + label::before {
  width: calc(calc(1.9375rem * .8) * 2);
}
.switch.switch-sm input + label::after {
  width: calc(calc(1.9375rem * .8) - calc(2px * 2));
  height: calc(calc(1.9375rem * .8) - calc(2px * 2));
}
.switch.switch-sm input:checked + label::after {
  margin-left: calc(1.9375rem * .8);
}
.switch.switch-lg {
  font-size: 1.25rem;
}
.switch.switch-lg input + label {
  min-width: calc(calc(3rem * .8) * 2);
  height: calc(3rem * .8);
  line-height: calc(3rem * .8);
  text-indent: calc(calc(calc(3rem * .8) * 2) + .5rem);
}
.switch.switch-lg input + label::before {
  width: calc(calc(3rem * .8) * 2);
}
.switch.switch-lg input + label::after {
  width: calc(calc(3rem * .8) - calc(2px * 2));
  height: calc(calc(3rem * .8) - calc(2px * 2));
}
.switch.switch-lg input:checked + label::after {
  margin-left: calc(3rem * .8);
}
.switch + .switch {
  margin-left: 1rem;
}
</style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <h3>Control de oromapas</h3>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary" role="alert">
            {{ session("alert") }}
            <?php Session::forget('alert')?>
        </div>
    @endif
    <div class="alert alert-danger alerta" style="display:none;" role="alert">

    </div>
    <div class="row justify-content-end">
        @can("write_oromapas_son")
            <a href="{{ route('store.shape.view') }}" class="btn btn-primary mr-3 mb-2"><i class="fas fa-upload"></i> Subir Shapes</a>
            <button data-toggle="modal" data-target="#modal-update-credit" class="btn btn-success mr-3 mb-2"><i class="fas fa-money-bill-wave"></i> Actualizar Saldos</button>
        @endcan
    </div>
    <div id="map" class="map"></div> 
    </div>
    
    
    
@endsection

@section('modals')
    @include('partials.modals.oromapas.modal_edit')
    @include('partials.modals.oromapas.modal_update_credit')
@endsection

@section('js')
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/leaflet.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/wms/src/leaflet.wms.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/WFS/src/WFS.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/utm.js') }}"></script>
    <script>
        var cartoIp = '{{ env("CartoWeb_IP") }}';
        var target = "";
    </script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/leaflet.draw/0.4.2/leaflet.draw.js"></script> --}}
    <script src="{{ asset('/assets/plugins/leaflet/pm/pm.js') }}"></script>
    <script src="{{ asset('/assets/plugins/leaflet/groups/dist/leaflet.groupedlayercontrol.min.js') }}"></script>
    <script>
        $(function () {
            // L.drawLocal.draw.handlers.polyline.tooltip.start = 'Seleccione poligono a dividir';
            var map = L.map('map',{scrollWheelZoom:true, minZoom: 12}).setView([20.74689, -105.39425], 12);
            var id = "";
            var click = true;
            var popup = true;

            var ortofoto = L.tileLayer(cartoIp + '/geoserver/gwc/service/tms/1.0.0/BahiaBanderas:Vuelo_Bajo_Ortofoto@EPSG:900913@png/{z}/{x}/{y}.png', {
                tms: true,
                minZoom: 12,
                maxZoom: 23,
                attribution: '&copy; IMPLAN - Ortofoto Bahia de Banderas 2019 | E.E.G.P.'
            }).addTo(map);

            var street = L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors | E.E.G.P.'
            });

            var baseMaps = {
                "<i class='fas fa-plane mr-1 ml-1'></i> Ortofoto BadeBa": ortofoto,
                "<i class='fas fa-map mr-1 ml-1'></i> OpenStreetMap": street
            };

            var oromapas_predios_aguamilpa = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_aguamilpa",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_bucerias = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_bucerias",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_el_coatante = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_el_coatante",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_el_colomo = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_el_colomo",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_el_guamuchil = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_el_guamuchil",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_el_porvenir = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_el_porvenir",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_fortuna_de_vallejo = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_fortuna_de_vallejo",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_higuera_blanca = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_higuera_blanca",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_jarretaderas = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_jarretaderas",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_la_cruz_de_huanacaxtle = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_la_cruz_de_huanacaxtle",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_las_lomas = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_las_lomas",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_lo_de_marcos = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_lo_de_marcos",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_los_sauces = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_los_sauces",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_mezcales = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_mezcales",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_pondoroque = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_pondoroque",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_punta_mita = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_punta_mita",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_san_francisco = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_san_francisco",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_san_ignacio = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_san_ignacio",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_san_jose_del_valle = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_san_jose_del_valle",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_san_juan_de_abajo = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_san_juan_de_abajo",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_san_quintin = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_san_quintin",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_san_vicente = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_san_vicente",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_sayulita = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_sayulita",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_sta_rosa_tapachula = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_sta_rosa_tapachula",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_predios_valle_banderas = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_predios_valle_banderas",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_hidraulica = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_hidraulica",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_sanitaria = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_sanitaria",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var oromapas_pozos = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:oromapas_pozos",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            
            var curvas = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:CN_1M",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });

            var curvas5 = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:CN_5M",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
           
            var calles = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:calle_geom",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            
            var terraceria = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:terraceria",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            
            var cueposAgua = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:cuerpos_agua",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var escurrimientos = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:escurrimientos",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var escurrimientosP = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:escurrimiento_principal",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var canales = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:canal",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var arroyos = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:arroyos",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var talud = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:talud",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });
            var corona = L.tileLayer.wms(cartoIp + "/geoserver/BahiaBanderas/wms", {
                layers: "OI.BahiaBanderas:corona",//nombre de la capa (ver get capabilities)
                format: 'image/png',
                transparent: true,
                minZoom: 12,
                maxZoom: 23,
                version: '1.3.0',//wms version (ver get capabilities)
            });

            var overlayMaps = {
                "Localidades": {
                    "Aguamilpa": oromapas_predios_aguamilpa,
                    "Bucerias": oromapas_predios_bucerias,
                    "El Coatante": oromapas_predios_el_coatante,
                    "El Colomo": oromapas_predios_el_colomo,
                    "El Guamuchil": oromapas_predios_el_guamuchil,
                    "El Porvenir": oromapas_predios_el_porvenir,
                    "Fortuna de Vallejo": oromapas_predios_fortuna_de_vallejo,
                    "Higuera Blanca": oromapas_predios_higuera_blanca,
                    "Jarretaderas": oromapas_predios_jarretaderas,
                    "La Cruz de Huanacaxtle": oromapas_predios_la_cruz_de_huanacaxtle,
                    "Las Lomas": oromapas_predios_las_lomas,
                    "Lo de Marcos": oromapas_predios_lo_de_marcos,
                    "Los Sauces": oromapas_predios_los_sauces,
                    "Mezcales": oromapas_predios_mezcales,
                    "Pondoroque": oromapas_predios_pondoroque,
                    "Punta Mita": oromapas_predios_punta_mita,
                    "San Francisco": oromapas_predios_san_francisco,
                    "San Ignacio": oromapas_predios_san_ignacio,
                    "San Jose del Valle": oromapas_predios_san_jose_del_valle,
                    "San Juan de Abajo": oromapas_predios_san_juan_de_abajo,
                    "San Quintin": oromapas_predios_san_quintin, 
                    "San Vicente": oromapas_predios_san_vicente,
                    "Sayulita": oromapas_predios_sayulita,
                    "Santa Rosa Tapachula": oromapas_predios_sta_rosa_tapachula,
                    "Valle Banderas": oromapas_predios_valle_banderas,
                },
                "Red Hidraulica": {
                    "Red Hidraulica": oromapas_hidraulica,
                },
                "Red Sanitaria": {
                    "Red Sanitaria": oromapas_sanitaria,
                },
                "Pozos de Visita": {
                    "Pozos de Visita": oromapas_pozos,
                },
                "Altimetria": {
                    "Curvas de Nivel 1M": curvas,
                    "Curvas de Nivel 5M": curvas5,
                },
                "Planimetria": {
                    "Calles": calles,
                    "Terraceria": terraceria,
                    "Cuerpo de Agua": cueposAgua,
                    "Escurrimientos Principales": escurrimientosP,
                    "Escurrimientos": escurrimientos,
                    "Canales": canales,
                    "Arroyos": arroyos,
                    "Talud": talud,
                    "Corona": corona,
                }
            };
            var options2 = {collapsed: false, groupsCollapsable: true, groupsExpandedClass: "fas fa-angle-down", groupsCollapsedClass: "fas fa-angle-right"};
            
            layerControl = L.control.groupedLayers(baseMaps, overlayMaps, options2).addTo(map);
            
            // var wmsLayer = L.tileLayer.wms('http://187.188.190.48:8080/geoserver/BahiaBanderas/ows?', {
            //     layers: 'BahiaBanderas:Vuelo_Bajo_Ortofoto'
            // }).addTo(map);
            
            // var pnoa = L.tileLayer.wms("http://187.188.190.48:8080/geoserver/BahiaBanderas/wms", {
            //     layers: "OI.BahiaBanderas:"+mapa,//nombre de la capa (ver get capabilities)
            //     format: 'image/png',
            //     transparent: true,
            //     minZoom: 12,
            //     maxZoom: 23,
            //     version: '1.3.0',//wms version (ver get capabilities)
            // }).addTo(map);

            // var overlayMaps = {
            //     "Localidad": pnoa
            // };
            // layerControl = L.control.layers(baseMaps, overlayMaps,{collapsed:false}).addTo(map);

            map.pm.addControls({
                position: 'topleft',
                drawMarker: false,
                drawCircleMarker: false,
                drawPolyline: false,
                drawRectangle: false,
                drawPolygon: true,
                drawCircle: false,
                cutPolygon: true,
                dragMode: false,
                editMode: false
            });

            map.addEventListener('click', function(e){ onMapClick(e);});
           
            function onMapClick(e, mapa = ""){
                var latlngStr = '('+ e.latlng.lat.toFixed(3) + ", " + e.latlng.lng.toFixed(3)+')';
                var BBOX = map.getBounds()._southWest.lng + ", " + map.getBounds()._southWest.lat + ", " + map.getBounds()._northEast.lng + ", " + map.getBounds()._northEast.lat;
                var WIDTH = map.getSize().x;
                var HEIGHT = map.getSize().y;   
                var X = map.layerPointToContainerPoint(e.layerPoint).x.toFixed(0);
                var Y = map.layerPointToContainerPoint(e.layerPoint).y.toFixed(0);
                var layers = e.sourceTarget._layers;
                for(var layer in layers)
                {
                    if(layers[layer].options.layers)
                    {
                        switch ((layers[layer].options.layers.split('OI.BahiaBanderas:'))[1]) {
                            case 'oromapas_hidraulica':
                                target = "#hidraulica";
                                break;
                            case 'oromapas_sanitaria':
                                target = "#sanitaria";
                                break;
                            case 'oromapas_pozos':
                                target = "#pozo";
                                break;
                            default:
                                target = "#predios";
                                break;
                        }
                        shape = (layers[layer].options.layers.split('OI.BahiaBanderas:'))[1];
                        URL = cartoIp+'/geoserver/BahiaBanderas/wms?request=GetFeatureInfo&service=WMS&version=1.1.1&info_format=application/json&exceptions=application/vnd.ogc.se_xml&layers=OI.'+layers[layer].options.layers+'&styles=&srs=EPSG%3A4326&format=image%2Fpng&bbox='+ BBOX + '&width='+ WIDTH +'&height='+ HEIGHT + '&query_layers=OI.'+layers[layer].options.layers+'&feature_count=20&x='+ X +'&y='+ Y;
                        $.ajax({
                            type: 'POST',
                            url: URL,
                            dataType : 'json',
                            success: function (response) {
                                var json = L.geoJson(response, {
                                    onEachFeature: onEachFeature
                                }).addTo(map);
                            },
                            error: function (req, status, error) {
                            }
                        });
                        
                    }
                }
                function onEachFeature(feature, layer) {
                    if (feature.properties && popup) {
                        
                        var strCompleta=new String(feature.id);
                        var arrPartes=strCompleta.split(".");
                        var id=arrPartes[1];
                        if(target == "#predios")
                        {
                            var popup1 = L.popup().setLatLng([e.latlng.lat, e.latlng.lng]).setContent("\
                                <table class='table table-sm'>\
                                    <thead>\
                                        <tr>\
                                            <th scope='col'>Propiedad</th>\
                                            <th scope='col'>Valor</th>\
                                        </tr>\
                                    </thead>\
                                    <tbody>\
                                        <tr>\
                                            <th scope='row'>Contrato</th>\
                                            <td>"+feature.properties.contrato+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Calle</th>\
                                            <td>"+feature.properties.calle+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Numero</th>\
                                            <td>"+feature.properties.numero+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Entre Calle 1</th>\
                                            <td>"+feature.properties.entreCalle1+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Entre Calle 2</th>\
                                            <td>"+feature.properties.entreCalle2+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Colonia</th>\
                                            <td>"+feature.properties.colonia+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Localidad</th>\
                                            <td>"+feature.properties.localidad+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Tipo de servicio</th>\
                                            <td>"+feature.properties.tipoServicio+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Giro</th>\
                                            <td>"+feature.properties.giro+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Tiene Agua</th>\
                                            <td>"+feature.properties.tieneAgua+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Tiene Drenaje</th>\
                                            <td>"+feature.properties.tieneDrenaje+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Medidor Serie</th>\
                                            <td>"+feature.properties.medidorSerie+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Saldo</th>\
                                            <td>$"+feature.properties.saldo+"</td>\
                                        </tr>\
                                    </tbody>\
                                </table> <button class='btn btn-primary btn-block detallePredios' data-id="+id+"><i class='fas fa-edit'></i></button> ").openOn(map);
                        }     
                        if(target == "#sanitaria")
                        {
                            var popup1 = L.popup().setLatLng([e.latlng.lat, e.latlng.lng]).setContent("\
                                <table class='table table-sm'>\
                                    <thead>\
                                        <tr>\
                                            <th scope='col'>Propiedad</th>\
                                            <th scope='col'>Valor</th>\
                                        </tr>\
                                    </thead>\
                                    <tbody>\
                                        <tr>\
                                            <th scope='row'>Layer</th>\
                                            <td>"+feature.properties.layer+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Pendiente</th>\
                                            <td>"+feature.properties.pendiente+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Longitud</th>\
                                            <td>"+feature.properties.longitud+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Diametro</th>\
                                            <td>"+feature.properties.diametro+"</td>\
                                        </tr>\
                                    </tbody>\
                                </table> <button class='btn btn-primary btn-block detalleSanitarias' data-id="+id+"><i class='fas fa-edit'></i></button> ").openOn(map);
                        }     
                        if(target == "#hidraulica")
                        {
                            var popup1 = L.popup().setLatLng([e.latlng.lat, e.latlng.lng]).setContent("\
                                <table class='table table-sm'>\
                                    <thead>\
                                        <tr>\
                                            <th scope='col'>Propiedad</th>\
                                            <th scope='col'>Valor</th>\
                                        </tr>\
                                    </thead>\
                                    <tbody>\
                                        <tr>\
                                            <th scope='row'>Layer</th>\
                                            <td>"+feature.properties.layer+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Nivel Raz</th>\
                                            <td>"+feature.properties.nivel_raz+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>LongiNivel Arras</th>\
                                            <td>"+feature.properties.nivel_arras+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Diferencia</th>\
                                            <td>"+feature.properties.diferencia+"</td>\
                                        </tr>\
                                    </tbody>\
                                </table> <button class='btn btn-primary btn-block detalleHidraulicas' data-id="+id+"><i class='fas fa-edit'></i></button> ").openOn(map);
                        }     
                        if(target == "#pozo")
                        {
                            var popup1 = L.popup().setLatLng([e.latlng.lat, e.latlng.lng]).setContent("\
                                <table class='table table-sm'>\
                                    <thead>\
                                        <tr>\
                                            <th scope='col'>Propiedad</th>\
                                            <th scope='col'>Valor</th>\
                                        </tr>\
                                    </thead>\
                                    <tbody>\
                                        <tr>\
                                            <th scope='row'>Layer</th>\
                                            <td>"+feature.properties.layer+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Pendiente</th>\
                                            <td>"+feature.properties.pendiente+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Longitud</th>\
                                            <td>"+feature.properties.longitud+"</td>\
                                        </tr>\
                                        <tr>\
                                            <th scope='row'>Diametro</th>\
                                            <td>"+feature.properties.diametro+"</td>\
                                        </tr>\
                                    </tbody>\
                                </table> <button class='btn btn-primary btn-block detallePozos' data-id="+id+"><i class='fas fa-edit'></i></button> ").openOn(map);
                        }     
                        if(target == "#poligonos")   
                        {
                            var popup1 = L.popup().setLatLng([e.latlng.lat, e.latlng.lng]).setContent("\
                                <table class='table table-sm'>\
                                    <thead>\
                                        <tr>\
                                            <th scope='col'>Propiedad</th>\
                                            <th scope='col'>Valor</th>\
                                        </tr>\
                                    </thead>\
                                    <tbody>\
                                        <tr>\
                                            <th scope='row'>Poligono</th>\
                                            <td>"+feature.properties.nombre+"</td>\
                                        </tr>\
                                    </tbody>\
                                </table> <button class='btn btn-primary btn-block detallePredios' data-id="+id+"><i class='fas fa-edit'></i></button> ").openOn(map);
                        }     
                    }
                }
            }

            // mapa("oromapas_predios_jarretaderas");
            // var shape = "oromapas_predios_jarretaderas";

            
            $('.file').simpleFilePreview();

            
            //Modal para editar predios
            $(document).on("click",".detallePredios",function (e) { 
                e.preventDefault();
                var id = $(this).data('id'),
                    token = "{{ csrf_token() }}";
                var modal = $("#modal-edit-predio");
                $.ajax({
                    type: "POST",
                    url: "{{ route('get.predio.by.id') }}",
                    data: {
                        _token: token,
                        id:id,
                        shape:shape
                    },
                    success: function (data) {
                        modal.find('#contrato').val(data.contrato);
                        modal.find('#calle').val(data.calle);
                        modal.find('#numero').val(data.numero);
                        modal.find('#calle_1').val(data.entreCalle1);                    
                        modal.find('#calle_2').val(data.entreCalle2);                    
                        modal.find('#colonia').val(data.colonia);
                        modal.find('#localidad').val(data.localidad);
                        modal.find('#tipoServicio').val(data.tipoServicio);
                        modal.find('#giro').val(data.giro);
                        if(data.tieneAgua == "Si")
                        {
                            $('#switch-agua').prop('checked', true);
                        }
                        else
                        {
                            $('#switch-agua').prop('checked', false);
                        }

                        if(data.tieneDrenaje == 'Si')
                        {
                            $('#switch-drenaje').prop('checked', true);
                        }
                        else
                        {
                            $('#switch-drenaje').prop('checked', false);
                        }
                        // modal.find('#agua').val(data.tieneAgua);
                        // modal.find('#drenaje').val(data.tieneDrenaje);
                        modal.find('#medidorSerie').val(data.medidorSerie);
                        modal.find('#predio').val(data.id);
                        modal.modal('show');
                        map.closePopup();
                    }
                });
            });    
            $(document).on("click",".detalleHidraulicas",function (e) { 
                e.preventDefault();
                var id = $(this).data('id'),
                    token = "{{ csrf_token() }}";
                var modal = $("#modal-edit-hidraulica");
                $.ajax({
                    type: "POST",
                    url: "{{ route('get.hidraulica.by.id') }}",
                    data: {
                        _token: token,
                        id:id,
                    },
                    success: function (data) {
                        modal.find('#layer').val(data.layer);
                        modal.find('#longitud').val(data.longitud);
                        modal.find('#diametro').val(data.diametro);
                        modal.find('#pendiente').val(data.pendiente);
                        modal.find('#hidraulica').val(data.id);
                        modal.modal('show');
                    }
                });
               
            });    

            $(document).on("click",".detallePozos",function (e) { 
                e.preventDefault();
                var id = $(this).data('id'),
                    token = "{{ csrf_token() }}";
                var modal = $("#modal-edit-pozo");
                $.ajax({
                    type: "POST",
                    url: "{{ route('get.pozo.by.id') }}",
                    data: {
                        _token: token,
                        id:id,
                    },
                    success: function (data) {
                        modal.find('#layer_p').val(data.layer);
                        modal.find('#nivel_raz').val(data.nivel_raz);
                        modal.find('#nivel_arras').val(data.nivel_arras);
                        modal.find('#diferencia').val(data.diferencia);
                        modal.find('#pozo').val(data.id);
                        modal.modal('show');
                    }
                });
            });    

            $(document).on("click",".detalleSanitarias",function (e) { 
                e.preventDefault();
                var id = $(this).data('id'),
                    token = "{{ csrf_token() }}";
                var modal = $("#modal-edit-sanitaria");
                $.ajax({
                    type: "POST",
                    url: "{{ route('get.sanitaria.by.id') }}",
                    data: {
                        _token: token,
                        id:id,
                    },
                    success: function (data) {
                        modal.find('#layer_s').val(data.layer);
                        modal.find('#longitud_s').val(data.longitud);
                        modal.find('#diametro_s').val(data.diametro);
                        modal.find('#pendiente_s').val(data.pendiente);
                        modal.find('#sanitaria').val(data.id);
                        modal.modal('show');
                    }
                });
            });    

            
            /**********************************************/
                    /*Ajax para editar de cada modal*/
            /**********************************************/
            $(document).on("submit","#frPredio",function (e) {
                e.preventDefault();
                var fr = $(this).serialize()+"&localidad2="+shape;
                var modal = $("#modal-edit-predio");

                modal.find('input[type=checkbox]').each(function() {     
                    // if (!this.checked) {
                    //     fr += '&'+this.name+'=false';
                    // }
                    if ($(this).is(":checked")) {
                        fr += '&'+this.name+'=true';
                    }
                    else
                    {
                        fr += '&'+this.name+'=false';
                    }
                });
                $.post("{{ route('edit.predio.by.id') }}", fr,
                    function (data) {
                         var id = modal.find('#predio').val();
                        $('#predio-'+id).html(
                                        '<td>'+data.contrato+'</td>'+
                                        '<td>'+data.calle+'</td>'+
                                        '<td>'+data.numero+'</td>'+
                                        '<td>'+data.entreCalle1+'</td>'+
                                        '<td>'+data.entreCalle2+'</td>'+
                                        '<td>'+data.colonia+'</td>'+
                                        '<td>'+data.localidad+'</td>'+
                                        '<td>'+data.tipoServicio+'</td>'+
                                        '<td>'+data.giro+'</td>'+
                                        '<td>'+data.tieneAgua+'</td>'+
                                        '<td>'+data.tieneDrenaje+'</td>'+
                                        '<td>'+data.medidorSerie+'</td>'+
                                        '<td><button class="btn btn-primary detallePredios" data-id='+data.id+'><i class="fas fa-edit"></i></button></td>'
                                      );
                        modal.modal("hide");
                        map._onResize();             
                    },
                );
            });
            $(document).on("submit","#frHidraulica",function (e) {
                e.preventDefault();
                var fr = $(this).serialize();
                var modal = $("#modal-edit-hidraulica");
                $.post("{{ route('edit.hidraulica.by.id') }}", fr,
                    function (data) {
                        var id = modal.find('#hidraulica').val();
                        $('#hidraulica-'+id).html(
                                        '<td>'+data.layer+'</td>'+
                                        '<td>'+data.longitud+'</td>'+
                                        '<td>'+data.pendiente+'</td>'+
                                        '<td>'+data.diametro+'</td>'+
                                        '<td><button class="btn btn-primary detalleHidraulicas" data-id='+data.id+'><i class="fas fa-edit"></i></button></td>'
                                      );
                        modal.modal("hide");              
                    },
                );
            });
            $(document).on("submit","#frPozo",function (e) {
                e.preventDefault();
                var fr = $(this).serialize();
                var modal = $("#modal-edit-pozo");
                $.post("{{ route('edit.pozo.by.id') }}", fr,
                    function (data) {
                        var id = modal.find('#pozo').val();
                        $('#pozo-'+id).html(
                                        '<td>'+data.layer+'</td>'+
                                        '<td>'+data.nivel_raz+'</td>'+
                                        '<td>'+data.nivel_arras+'</td>'+
                                        '<td>'+data.diferencia+'</td>'+
                                        '<td><button class="btn btn-primary detalleHidraulicas" data-id='+data.id+'><i class="fas fa-edit"></i></button></td>'
                                      );
                        modal.modal("hide");              
                    },
                );
            });

            $(document).on("submit","#frSanitaria",function (e) {
                e.preventDefault();
                var fr = $(this).serialize();
                var modal = $("#modal-edit-sanitaria");
                $.post("{{ route('edit.sanitaria.by.id') }}", fr,
                    function (data) {
                        var id = modal.find('#sanitaria').val();
                        $('#sanitaria-'+id).html(
                                        '<td>'+data.layer+'</td>'+
                                        '<td>'+data.longitud+'</td>'+
                                        '<td>'+data.pendiente+'</td>'+
                                        '<td>'+data.diametro+'</td>'+
                                        '<td><button class="btn btn-primary detalleSanitarias" data-id='+data.id+'><i class="fas fa-edit"></i></button></td>'
                                      );
                        modal.modal("hide");              
                    },
                );
            });

          
            /*********************************************************************************************************/
            /********************************Script para leaflet*****************************************************/
            /********************************************************************************************************/
            
            
            

            var obt = new Array();
            var obt2 = new Array();
            var i = 0;
            var opcion = "";
            $(document).on('click', '.leaflet-pm-icon-cut', function (e) {
                e.preventDefault();
                map.pm.disableDraw('Cut');
                opcion = "cortar";
                map.pm.enableDraw('Cut');
            });

            $(document).on('click', '.leaflet-pm-icon-polygon', function (e) {
                map.pm.disableDraw('Polygon');
                e.preventDefault();
                opcion = "crear";
                map.pm.enableDraw('Polygon');
            });
             
           
                map.on('pm:drawstart', ({ workingLayer }) => {
                    console.log(workingLayer);
                    workingLayer.on('pm:vertexadded', e => {
                        console.log(e);
                    });
                    if(click && opcion == 'cortar')
                    {

                        $.confirm({
                            title: 'Seleccione poligono a modificar',
                            content: 'Por favor realize click sobre el poligono a cortar',
                            buttons: {
                                Confirmar: {
                                    text: 'Ok',
                                    btnClass: 'btn btn-primary btn-confirm',
                                    action: function()
                                    {
                                        map.addEventListener('click', eventocortar);
                                    }
                                },
                                Cancelar: {
                                    text: 'Cancelar',
                                    btnClass: 'btn btn-secondary btn-confirm',
                                    action: function()
                                    {
                                        click = true;
                                        opcion = "";
                                        map.pm.disableDraw('Polygon');
                                    }
                                }
                            }
                        });
                    }
                    else if(opcion == 'cortar')
                    {
                        workingLayer.on('pm:vertexadded', e => {
                            obt[i] = new Array();
                            obt[i]["x"] =  e.marker.getLatLng().utm().x;
                            obt[i]["y"] =  e.marker.getLatLng().utm().y;
                            i++;
                        });

                    }

                    if(opcion == "crear")
                    {
                        workingLayer.on('pm:vertexadded', e => {
                            console.log(e);
                            obt2[i] = new Array();
                            obt2[i]["x"] =  e.marker.getLatLng().utm().x;
                            obt2[i]["y"] =  e.marker.getLatLng().utm().y;
                            i++;
                        });
                    }
                });

            var eventocortar = function(e){ 
                if(click)
                {
                    map.pm.Draw.Cut._removeLastVertex();
                    marker = L.marker();
                    e.target.closePopup();
                    layerSelected(e, shape);
                    click = false;
                    popup = false;
                    cut = false;
                    map.pm.enableDraw('Cut');
                    map.removeEventListener('click', eventocortar)
                }
            };
            
            var parametros = {};
            var eliminar = "";
            $(document).on('click', '.leaflet-pm-icon-delete' ,function () {
                if(eliminar){
                    eliminar = false;
                    map.removeEventListener('click', eventoeliminar);
                    map.addEventListener('click', function(e){ onMapClick(e);});
                }else{
                    eliminar = true;
                    map.addEventListener('click', eventoeliminar);
                }
                console.log(eliminar,popup,click);
            });
          
            var eventoeliminar = function(e){ 
                popup = false;
                layerSelected(e, shape);
                if(eliminar && id != "")
                {
                    $.confirm({
                        title: 'Eliminar poligono',
                        content: 'Esta seguro que desea eliminar este poligono?',
                        buttons: {
                            Confirmar: {
                                text: 'Ok',
                                btnClass: 'btn btn-primary btn-confirm',
                                action: function()
                                {
                                    $.ajax({
                                        type: "POST",
                                        url: "{{route('delete.polygon')}}",
                                        data: {
                                            _token:"{{ csrf_token() }}",
                                            id:id,
                                            shape:shape,
                                        },
                                        success: function (data) {
                                            eliminar = false;
                                            id = "";
                                            popup = true;
                                            click = true;
                                            // mapaNew(shape, true);
                                            var _this = $(".leaflet-pm-icon-delete").parents('.button-container');
                                            _this.removeClass('active');
                                            map.removeEventListener('click', eventoeliminar);
                                            map.addEventListener('click', function(e){ onMapClick(e);});
                                        }
                                    });
                                }
                            },
                            Cancelar: {
                                text: 'Cancelar',
                                btnClass: 'btn btn-secondary btn-confirm',
                                action: function()
                                {
                                    eliminar = false;
                                    id = "";
                                    popup = true;
                                    click = true;
                                    var _this = $(".leaflet-pm-icon-delete").parents('.button-container');
                                    _this.removeClass('active');
                                    map.removeEventListener('click', eventoeliminar);
                                    map.addEventListener('click', function(e){ onMapClick(e);});
                                }
                            }
                        }
                    });
                }
                console.log(eliminar,popup,click);
            };
            
            $(document).on('click', '.action-cancel', function (e) {
                e.preventDefault();
                parametros = {};
                id = "";
                opcion = "";
                obt = [];
                click = true;
            });
            $(document).on('click', '.action-finish', function (e) {
                e.preventDefault();
                if(opcion == "cortar")
                {
                    drawEnd();
                    click = true;
                    popup = true;
                    opcion = "";
                    i = 0;
                }
               
            });

            function drawEnd()
            {
                if(obt.length != 0)
                {
                    $.each(obt, function (i, value) { 
                        
                        parametros[i] = 
                        {
                            x: value.x,
                            y: value.y
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "{{ route('cut.polygon') }}",
                        dataType: "JSON",
                        data: 
                        {
                            _token:"{{ csrf_token() }}",
                            id:id,
                            parametros:parametros,
                            shape: shape
                        },
                    success: function (data) {
                        // parametros = {};
                        // id = "";
                        // click = true;
                        // mapaNew(shape, true);
                    }
                    });
                }
            }
            map.on('pm:create', function(e){
                console.log(e);
                obt2.push(obt2[0]);
                var modal = $("#modal-new-predio");
                modal.modal("show");
            });
            $(document).on('submit', "#frNewPredio" ,function (e) {
                e.preventDefault();
                var modal = $("#modal-new-predio");
                console.log(shape);
                if(obt2.length != 0)
                {
                    $.each(obt2, function (i, value) { 
                        parametros[i] = 
                        {
                            x: value["x"],
                            y: value["y"]
                        };
                    });
                    var fr = $(this).serialize() + "&parametros=" + JSON.stringify(parametros) +"&shape=" + shape + "&_token=" + "{{ csrf_token() }}" ;
                    $.ajax({
                        type: "POST",
                        url: "{{ route('create.polygon') }}",
                        data:fr,
                        success: function (data) {
                            parametros = {};
                            id = "";
                            click = true;
                            i = 0;
                            // mapaNew(shape, true);
                            modal.modal("hide");
                            // location.reload();
                        },
                        error: function (error) {
                        }
                    });
                }      
            });

            function layerSelected(e, mapa)
            {
                var latlngStr = '('+ e.latlng.lat.toFixed(3) + ", " + e.latlng.lng.toFixed(3)+')';
                var BBOX = map.getBounds()._southWest.lng + ", " + map.getBounds()._southWest.lat + ", " + map.getBounds()._northEast.lng + ", " + map.getBounds()._northEast.lat;
                var WIDTH = map.getSize().x;
                var HEIGHT = map.getSize().y;   
                var X = map.layerPointToContainerPoint(e.layerPoint).x.toFixed(0);
                var Y = map.layerPointToContainerPoint(e.layerPoint).y.toFixed(0);
                URL = 'http://187.188.190.48:8080/geoserver/BahiaBanderas/wms?request=GetFeatureInfo&service=WMS&version=1.1.1&info_format=application/json&exceptions=application/vnd.ogc.se_xml&layers=OI.BahiaBanderas:'+mapa+'&styles=&srs=EPSG%3A4326&format=image%2Fpng&bbox='+ BBOX + '&width='+ WIDTH +'&height='+ HEIGHT + '&query_layers=OI.BahiaBanderas:'+mapa+'&feature_count=20&x='+ X +'&y='+ Y;
                
                function onEachFeature(feature, layer){
                    var strCompleta=new String(feature.id);
                    var arrPartes=strCompleta.split(".");
                    id=arrPartes[1];
                }

                $.ajax({
                    type: 'POST',
                    url: URL,
                    dataType : 'json',
                    success: function (response) {

                        var json = L.geoJson(response,{
                            onEachFeature:onEachFeature
                        }).addTo(map);
                    },
                    error: function (req, status, error) {
                    }
                });    
            }

            function isEmpty(obj) {
                for(var key in obj) {
                    if(obj.hasOwnProperty(key))
                        return false;
                }
                return true;
            }
            
        });
    </script>
@endsection