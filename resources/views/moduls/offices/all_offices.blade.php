@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('assets/plugins/preview/simpleFilePreview.css') }}">
  
   
@endsection

@section('content')
<div class="row justify-content-center mb-3">
    <h1>Oficios</h1>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<div class="row justify-content-center">
    <div class="col-12 col-xl-12">
    @can('write_offices')
        <div class="row justify-content-end mb-2">
            <button type="button" class="btn btn-primary mr-3" data-toggle="modal" data-target="#modal-new-offices"><i class="fas fa-plus mr-2"></i>Nuevo Oficio Entrante </button>
            <button type="button" class="btn btn-primary mr-3" data-toggle="modal" data-target="#modal-new-offices-extern"><i class="fas fa-plus mr-2"></i>Nuevo Oficio Salida</button>
        </div>
    @else
        <div class="row justify-content-end mb-2">
            <button type="button" class="btn btn-primary mr-3" data-toggle="modal" data-target="#modal-new-offices-extern"><i class="fas fa-plus mr-2"></i>Nuevo Oficio</button>
        </div>    
    @endcan
        <div class="nav nav-tabs justify-content-center filtro" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active tabs-item" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Oficios de entrada</a>
            <a class="nav-item nav-link tabs-item" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Oficios de salida</a>
        </div>

        <div class="tab-content mt-2" id="nav-tabContent">
            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">  
                <table class="table dataTable">
                    <thead>
                        <tr>
                            <th scope="col">No. Oficio</th>
                            <th scope="col">Emisor</th>
                            <th scope="col">Dependencia</th>
                            <th scope="col">Responsable</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Estatus</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @can("write_offices")    
                            @if(isset($oficios))
                                @foreach ($oficios as $oficio)
                                    @if($oficio->id_padre == null && $oficio->type == 0 || $oficio->type == 0 && $oficio->estatus == 4 )
                                        <tr>
                                            <td>{{$oficio->no_oficio}}</td>
                                            <td>{{$oficio->emisor}}</td>
                                            <td>{{$oficio->dependencia}}</td>
                                            @foreach ($oficio->responsable()->wherePivot('type', 0)->get() as $responsable)
                                            <td>{{$responsable->nombre}}</td>
                                            @endforeach
                                            @if($oficio->estatus == 0 || $oficio->estatus == 4)
                                                <td><span style="display:none;">{{ Carbon\Carbon::parse($oficio->fecha_emision)->format('Ymd') }}</span>{{ Carbon\Carbon::parse($oficio->fecha_emision)->format('d/m/Y') }}</td>
                                            @else
                                                <td><span style="display:none;">{{ Carbon\Carbon::parse($oficio->fecha_recepcion)->format('Ymd') }}</span>{{ Carbon\Carbon::parse($oficio->fecha_recepcion)->format('d/m/Y') }}</td>
                                            @endif
                                            @if($oficio->type == 0)
                                                <td>{{ config('implan.offices.types.internal') }}</td>
                                            @elseif($oficio->type == 1)
                                                <td>{{ config('implan.offices.types.external') }}</td>
                                            @elseif($oficio->type == 2)
                                                <td>{{ config('implan.offices.types.answer') }}</td>
                                            @endif
                                        
                                            @if ($oficio->estatus == 1)
                                                <td align="center" class="bg-danger text-white">{{ config('implan.offices.status.not_checking') }}</td>
                                            @elseif($oficio->estatus == 2)
                                                <td align="center" class="bg-warning text-white">{{ config('implan.offices.status.waiting_answer') }}</td>
                                            @elseif($oficio->estatus == 3)
                                                <td align="center" class="bg-success text-white">{{ config('implan.offices.status.complete') }}</td>
                                            @elseif($oficio->estatus == 4)
                                                <td align="center" class="bg-danger text-white">{{ config('implan.offices.status.cancelled') }}</td> 
                                            @elseif($oficio->estatus == 0)
                                                <td align="center" class="bg-warning text-white">{{ config('implan.offices.status.waiting_office') }}</td>    
                                            @endif
                                            <td align="center">
                                                <button class="btn btn-primary detalles" data-id="{{$oficio->id}}"><i class="fas fa-edit"></i></button>
                                                @if($oficio->estatus != 3 && $oficio->estatus != 4) 
                                                    <button class="btn btn-danger delete" data-id="{{$oficio->id}}"><i class="fas fa-trash"></i></button>
                                                @endif
                                                @if($oficio->estatus == 4)
                                                    <button class="btn btn-success restore" data-id="{{$oficio->id}}"><i class="fas fa-check"></i></button>
                                                @endif
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            @endif
                        @else
                            @foreach (Auth::user()->profile->position->offices as $oficio)
                                @if($oficio->id_padre == null && $oficio->type == 0 )
                                    <tr>
                                        <td>{{$oficio->no_oficio}}</td>
                                        <td>{{$oficio->emisor}}</td>
                                        <td>{{$oficio->dependencia}}</td>
                                        @foreach ($oficio->responsable()->wherePivot('type', 0)->get() as $responsable)
                                        <td>{{$responsable->nombre}}</td>
                                        @endforeach
                                        @if($oficio->estatus == 0 || $oficio->estatus == 4)
                                            <td><span style="display:none;">{{ Carbon\Carbon::parse($oficio->fecha_emision)->format('Ymd') }}</span>{{ Carbon\Carbon::parse($oficio->fecha_emision)->format('d/m/Y') }}</td>
                                        @else
                                            <td><span style="display:none;">{{ Carbon\Carbon::parse($oficio->fecha_recepcion)->format('Ymd') }}</span>{{ Carbon\Carbon::parse($oficio->fecha_recepcion)->format('d/m/Y') }}</td>
                                        @endif                   
                                        @if($oficio->type == 0)
                                            <td>{{ config('implan.offices.types.internal') }}</td>
                                        @elseif($oficio->type == 1)
                                            <td>{{ config('implan.offices.types.external') }}</td>
                                        @elseif($oficio->type == 2)
                                        <td>{{ config('implan.offices.types.answer') }}</td>
                                        @endif
                                    
                                        @if ($oficio->estatus == 1)
                                            <td align="center" class="bg-danger text-white">{{ config('implan.offices.status.not_checking') }}</td>
                                        @elseif($oficio->estatus == 2)
                                            <td align="center" class="bg-warning text-white">{{ config('implan.offices.status.waiting_answer') }}</td>
                                        @elseif($oficio->estatus == 4)
                                            <td align="center" class="bg-danger text-white">{{ config('implan.offices.status.cancelled') }}</td>
                                        @elseif($oficio->estatus == 3)
                                            <td align="center" class="bg-success text-white">{{ config('implan.offices.status.complete') }}</td>
                                        @elseif($oficio->estatus == 0)
                                        <td align="center" class="bg-warning text-white">{{ config('implan.offices.status.waiting_office') }}</td>        
                                        @endif
                                        <td align="center"><button class="btn btn-primary detalles" data-id="{{$oficio->id}}"><i class="fas fa-edit"></i></button></td>
                                    </tr>
                                @endif                        
                            @endforeach
                        @endcan
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                <table class="table dataTable">
                        <thead>
                            <tr>
                                <th scope="col">No. Oficio</th>
                                <th scope="col">Receptor</th>
                                <th scope="col">Dependencia Receptora</th>
                                <th scope="col">Emisor</th>
                                <th scope="col">Fecha</th>
                                <th scope="col">Tipo</th>
                                <th scope="col">Estatus</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @can("write_offices")    
                                @if(isset($oficios))
                                    @foreach ($oficios as $oficio)
                                        @if($oficio->id_padre == null && $oficio->type == 1 || $oficio->type == 2 && $oficio->estatus == 0 || $oficio->type == 2 && $oficio->estatus == 4 )
                                            <tr>
                                                <td>{{$oficio->no_oficio}}</td>
                                                <td>{{$oficio->emisor}}</td> 
                                                <td>{{$oficio->dependencia}}</td>
                                                @foreach ($oficio->responsable()->wherePivot('type', 0)->get() as $responsable)
                                                <td>{{$responsable->nombre}}</td>
                                                @endforeach
                                                @if($oficio->estatus == 0 || $oficio->estatus == 4)
                                                    <td><span style="display:none;">{{ Carbon\Carbon::parse($oficio->fecha_emision)->format('Ymd') }}</span>{{ Carbon\Carbon::parse($oficio->fecha_emision)->format('d/m/Y') }}</td>
                                                @else
                                                    <td><span style="display:none;">{{ Carbon\Carbon::parse($oficio->fecha_recepcion)->format('Ymd') }}</span>{{ Carbon\Carbon::parse($oficio->fecha_recepcion)->format('d/m/Y') }}</td>
                                                @endif
                                                @if($oficio->type == 0)
                                                    <td>{{ config('implan.offices.types.internal') }}</td>
                                                @elseif($oficio->type == 1)
                                                    <td>{{ config('implan.offices.types.external') }}</td>
                                                @elseif($oficio->type == 2)
                                                    <td>{{ config('implan.offices.types.answer') }}</td>
                                                @endif
                                            
                                                @if ($oficio->estatus == 1)
                                                    <td align="center" class="bg-danger text-white">{{ config('implan.offices.status.not_checking') }}</td>
                                                @elseif($oficio->estatus == 2)
                                                    <td align="center" class="bg-warning text-white">{{ config('implan.offices.status.waiting_answer') }}</td>
                                                @elseif($oficio->estatus == 3)
                                                    <td align="center" class="bg-success text-white">{{ config('implan.offices.status.complete') }}</td>
                                                @elseif($oficio->estatus == 4)
                                                    <td align="center" class="bg-danger text-white">{{ config('implan.offices.status.cancelled') }}</td> 
                                                @elseif($oficio->estatus == 0)
                                                    <td align="center" class="bg-warning text-white">{{ config('implan.offices.status.waiting_office') }}</td>    
                                                @endif
                                                <td align="center">
                                                    <button class="btn btn-primary detalles" data-id="{{$oficio->id}}"><i class="fas fa-edit"></i></button>
                                                    @if($oficio->estatus != 3 && $oficio->estatus != 4) 
                                                        <button class="btn btn-danger delete" data-id="{{$oficio->id}}"><i class="fas fa-trash"></i></button>
                                                    @endif
                                                    @if($oficio->estatus == 4)
                                                        <button class="btn btn-success restore" data-id="{{$oficio->id}}"><i class="fas fa-check"></i></button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endif
                            @else
                                @foreach (Auth::user()->profile->position->offices as $oficio)
                                    @if($oficio->id_padre == null && $oficio->type == 1  || $oficio->type == 2 && $oficio->estatus == 0)
                                        <tr>
                                            <td>{{$oficio->no_oficio}}</td>
                                            <td>{{$oficio->emisor}}</td>
                                            <td>{{$oficio->dependencia}}</td>
                                            @foreach ($oficio->responsable()->wherePivot('type', 0)->get() as $responsable)
                                            <td>{{$responsable->nombre}}</td>
                                            @endforeach
                                            @if($oficio->estatus == 0 || $oficio->estatus == 4)
                                                <td><span style="display:none;">{{ Carbon\Carbon::parse($oficio->fecha_emision)->format('Ymd') }}</span>{{ Carbon\Carbon::parse($oficio->fecha_emision)->format('d/m/Y') }}</td>
                                            @else
                                                <td><span style="display:none;">{{ Carbon\Carbon::parse($oficio->fecha_recepcion)->format('Ymd') }}</span>{{ Carbon\Carbon::parse($oficio->fecha_recepcion)->format('d/m/Y') }}</td>
                                            @endif                   
                                            @if($oficio->type == 0)
                                                <td>{{ config('implan.offices.types.internal') }}</td>
                                            @elseif($oficio->type == 1)
                                                <td>{{ config('implan.offices.types.external') }}</td>
                                            @elseif($oficio->type == 2)
                                            <td>{{ config('implan.offices.types.answer') }}</td>
                                            @endif
                                        
                                            @if ($oficio->estatus == 1)
                                                <td align="center" class="bg-danger text-white">{{ config('implan.offices.status.not_checking') }}</td>
                                            @elseif($oficio->estatus == 2)
                                                <td align="center" class="bg-warning text-white">{{ config('implan.offices.status.waiting_answer') }}</td>
                                            @elseif($oficio->estatus == 4)
                                                <td align="center" class="bg-danger text-white">{{ config('implan.offices.status.cancelled') }}</td>
                                            @elseif($oficio->estatus == 3)
                                                <td align="center" class="bg-success text-white">{{ config('implan.offices.status.complete') }}</td>
                                            @elseif($oficio->estatus == 0)
                                            <td align="center" class="bg-warning text-white">{{ config('implan.offices.status.waiting_office') }}</td>        
                                            @endif
                                            <td align="center"><button class="btn btn-primary detalles" data-id="{{$oficio->id}}"><i class="fas fa-edit"></i></button></td>
                                        </tr>
                                    @endif                        
                                @endforeach
                            @endcan
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
    
@endsection

@section('modals')
    @include('partials.modals.office.modal_new_offices')
@endsection

@section('js')
    <script src="{{ asset('assets/plugins/preview/jquery.simpleFilePreview.js') }}"></script>
    <script>
        $(document).ready(function () {
            $("#storeAnswer").hide();
            $(".answerFile").hide();
            $('.messageNotification').hide();
            $('.file-size').hide();
            /*Condicion para revisar si la variable $details contiene informacion y desplegar el modal
              con la informacion del oficio que se envia por la URL*/
            @if(!empty($details))
                var modal2 = $("#modal-details-offices");
                modal2.find('#no_oficio').val("{{$details->no_oficio}}");
                modal2.find('#emisor').val('{{$details->emisor}}');
                modal2.find('#fecha_emision').val('{{$details->fecha_emision}}');
                modal2.find('#asunto').val('{{$details->asunto}}');                    
                modal2.find('#responsable').val('{{$details->encargado->nombre}}');
                modal2.find('#fecha_recepcion').val('{{$details->fecha_recepcion}}');
                modal2.find('#dependencia').val('{{$details->dependencia}}');
                $('.oficio').data('id', '{{$details->id}}');
                @if($details->fecha_respuesta)
            
                    $('.fechaRespuesta').html('<label for="">Fecha limite respuesta</label>\
                        <input class="form-control btn-warning" type="date" name="fecha_recepcion" disabled value="{{$details->fecha_respuesta}}" >    ');
                
                @else
                
                    $('.fechaRespuesta').html('');
                @endif                              
                @if($details->withCopy != null)                
                    $('#WithCopy').html('<div class="row mb-2">\
                        <div class="col-xl-12">\
                                <label for="">Con copia</label>\
                                <input class="form-control" type="text" name="Copy" id="Copy">\
                        </div>\
                    </div>\
                    ');
                    var copias = "";
                    @foreach($details->withCopy as $copia)
                        copias += '{{$copia->nombre}}, '  
                    @endforeach                 
                   
                    copias = copias.slice(0, -2);
                    modal2.find("#Copy").val(copias);
                @endif
                @if($details->estatus == 1)
                        $('.fileReaded').data('id', '{{$details->id}}');
                        $('.fileReaded').data('status', 2); 
                 @endif                       

                @if($details->estatus == 1 && $details->respuesta == 0)
                    $('.fileReaded').data('id', '{{$details->id}}');
                    $('.fileReaded').data('status', 3); 
                @endif
                $("#modal-details-offices :input").prop('disabled', true);
                $("#modal-details-offices button").prop('disabled', false);
                $("#storeAnswer").hide();
                $(".answerFile").hide();  
                $(".saveFile").hide();
                @if(Auth::user()->profile->position->id == $details->encargado->id)
                    
                @else
                   $('.fileReaded').hide();
                @endif  
                modal2.modal('show');
                
                
            @endif
        });
        //Ajax para obtener informacion del oficio y desplegar el modal
       $(document).on('click','.detalles', function () {
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{ route('get.office.by.id') }}",
                data: {
                    _token: token,
                    id:id,
                },           
                success: function (data) 
                {
                   
                    var modal = $("#modal-details-offices");
                    modal.find('#no_oficio').val(data.no_oficio);
                    modal.find('#emisor').val(data.emisor);
                    modal.find('#fecha_emision').val(data.fecha_emision);
                    modal.find('#asunto').val(data.asunto);                    
                    modal.find('#responsable').val(data.encargado.nombre);                    
                    modal.find('#fecha_recepcion').val(data.fecha_recepcion);
                    modal.find('#dependencia').val(data.dependencia);
                    $('.oficio').data('id', data.id);
                    if(data.withCopy.lenght != 0)
                    {
                        $('#WithCopy').html('<div class="row mb-2">\
                            <div class="col-xl-12">\
                                    <label for="">Con copia</label>\
                                    <input class="form-control" type="text" name="Copy" id="Copy">\
                            </div>\
                        </div>\
                        ');
                        var copias = "";
                        $.each(data.withCopy, function (i, value) { 
                             copias += value.nombre + ', ';
                        });
                        copias = copias.slice(0, -2);
                        modal.find("#Copy").val(copias);
                    }
                   
                                 
                    $("#modal-details-offices :input").prop('disabled', true);

                    @can("write_offices")   
                        //En caso de que el oficio que se despliegue su informacion sea el que tiene el mismo usuario logeado de encargado
                        //Ocurriran los eventos dentro del switch, LA VARIABLE data.type SE REFIERA AL TIPO DE OFICIO QUE ES 1-ENVIADO O 0-RECIBIDO
                        if('{{Auth::user()->profile->position->id}}' == data.encargado.id)
                        {
                            switch (data.estatus) 
                            {

                                case 1:
                                    $(function () {
                                        $("#storeAnswer").hide();
                                        $(".saveFile").hide();
                                        $(".answerFile").hide();
                                        $(".fileReaded").show();
                                        $('.fileReaded').data('id', data.id);
                                        $('.fileReaded').data('status', 2);
                                        if( data.respuesta == 0)
                                        {
                                            $('.fileReaded').data('status', 3); 
                                        } 
                                    });
                                    
                                    break;

                                case 2:
                                    $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');
                                    $(".fileReaded").hide();
                                    $(".saveFile").hide();
                                    //$("#storeAnswer :input").prop('readonly', false);
                                    $("#storeAnswer").hide();
                                    //$(".answerFile").show();
                                    $('.respuesta').append('<div class="form-group col officeAnswer">\
                                                                <label>Adjuntar oficio respuesta</label>\
                                                                <input type="file" id="oficioRespuesta2" name="oficio"/>\
                                                                <input type="hidden" name="id_padre" value='+data.id+'>\
                                                            </div>'                                                    
                                                            );
                                    $('#oficioRespuesta2').simpleFilePreview();        
                                    //$("#storeAnswer :input").prop('disabled', false); 
                                    break;

                                case 0:
                                    if (data.type == 1) 
                                    {
                                        $(function () {
                                            $("#storeAnswer").hide();
                                            $(".saveFile").show();
                                            $('.oficio').hide();
                                            $('.answerFile').hide();
                                            $('.fileReaded').hide();
                                            $(".adjuntar").html('<label for="oficio">Adjuntar Oficio</label>\
                                                                <input type="file" id="oficios2" name="oficio"/>\
                                                                <input type="hidden" name="id" value="'+data.id+'">\
                                                                ');
                                            $('.fecha_entrega').html('<label id="date" for="">Fecha Entrega</label>'
                                                                    +'<input class="form-control" type="date" id="fecha_entrega" name="fecha_recepcion" required>'        
                                                                    );                     
                                            $('#oficios2').simpleFilePreview();
                                            $('.type').val(1);
                                            $(".storeFile :input").prop('disabled', false); 
                                            $(".storeFile :input").prop('readonly', true);
                                            $("#fecha_entrega").prop('readonly', false);
                                            $("#emisor").prop('readonly', false);   
                                        });
                                    }
                                    if (data.type == 2) 
                                    {
                                        $(function () {
                                            $("#storeAnswer").hide();
                                            $(".saveFile").show();
                                            $('.oficio').hide();
                                            $('.answerFile').hide();
                                            $('.fileReaded').hide();
                                            $(".adjuntar").html('<label for="oficio">Adjuntar Oficio</label>\
                                                                <input type="file" id="oficios2" name="oficio"/>\
                                                                <input type="hidden" name="id" value="'+data.id+'">\
                                                                ');
                                            $('.fecha_entrega').html('<label id="date" for="">Fecha Entrega</label>'
                                                                    +'<input class="form-control" type="date" id="fecha_entrega" name="fecha_recepcion" required>'        
                                                                    );
                                            $('.type').val(2);                                             
                                            $('#oficios2').simpleFilePreview();
                                            $(".storeFile :input").prop('disabled', false); 
                                            $(".storeFile :input").prop('readonly', true);
                                            $("#fecha_entrega").prop('readonly', false);
                                             
                                        });
                                    }
                                    break;

                                case 3:
                                    if (data.type == 1)
                                    {
                                        $(function () {
                                            $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');
                                            $("#storeAnswer").hide();
                                            $(".saveFile").hide();
                                            $('.fecha_entrega').html('<label id="date" for="">Fecha Entrega</label>'
                                                                        +'<input class="form-control" type="date" id="fecha_recepcion"  value="'+data.fecha_recepcion+'" disabled name="fecha_recepcion">'        
                                                                        );
                                        });
                                         
                                    }
                                    if (data.type == 0)
                                    {
                                        $(function () {
                                            $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');
                                            $("#storeAnswer").hide();
                                            $(".saveFile").hide(); 
                                        });
                                    }
                                      
                                    break;
                                case 4:
                                    $(function () {
                                        $("#storeAnswer").hide();
                                        $(".answerFile").hide();
                                        $(".adjuntar").html('');
                                        $(".saveFile").hide();
                                        $(".fileReaded").hide();
                                    });
                                    break;    
                                default:
                                break;
                            }

                            if(data.respuesta == 0)
                            {
                                $("#storeAnswer").hide();
                            }     
                        }
                        else 
                        {
                        //Caso contrario de que el oficio no sea de su responsabilidad 
                            $(".fileReaded").hide();
                            $("#storeAnswer").hide();
                            $(".answerFile").hide();                            
                            $(".saveFile").hide();

                            switch (data.estatus) 
                            {
                                case 0:
                                 if (data.type == 1) 
                                {
                                    $(function () {
                                        $('.adjuntar').html('');
                                        $('.fecha_entrega').html('<label id="date" for="">Fecha Entrega</label>'
                                                                    +'<input class="form-control" type="date" id="fecha_entrega" disabled name="fecha_entrega">'        
                                                                    );
                                                                    
                                    });
                                 }
                                 if(data.type == 2)
                                 {
                                    $(function () {
                                        $('.adjuntar').html('');  
                                    });
                                 }
                                    break;

                                case 3:
                                 if (data.type == 1) 
                                 {
                                     $(function () {
                                        $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');
                                        $('.fecha_entrega').html('<label id="date" for="">Fecha Entrega</label>'
                                                                    +'<input class="form-control" type="date" id="fecha_recepcion"  value="'+data.fecha_recepcion+'" disabled name="fecha_recepcion">'        
                                                                    );
                                     });
                                 }
                                 if(data.type == 0)
                                 {
                                    $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');
                                 }
                                    break;
                                case 2:
                                    $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');
                                    $('#storeAnswer').hide();
                                    break; 

                                case 4:
                                    $(function () {
                                        $("#storeAnswer").hide();
                                        $(".answerFile").hide();
                                        $(".adjuntar").html('');
                                        $(".saveFile").hide();
                                        $(".fileReaded").hide();
                                    });
                                    break;           
                            
                                default:
                                    
                                break;
                            }
                        }

                        //Validacion para insertar el boton de notificar que el usuario que se le asigno el oficio 
                        //a superado su fecha limite de respuesta para atender el oficio
                        if(data.fecha_respuesta)
                        {
                            var now = new Date('{{Carbon\Carbon::now()}}');
                            var fechaRes = new Date(data.fecha_respuesta);
                            if(now.getTime() > fechaRes.getTime() && data.estatus != 3 )
                            {
                                $(".notification").html("<button data-id='"+data.id+"' data-res='"+data.encargado.id+"' class='btn btn-danger alertNotification'>Enviar Recordatorio</button>");            
                            }
                            else
                            {
                                $(".notification").html("");
                            }
                        }
                        else
                        {
                            $(".notification").html("");
                        }
                    @else

                        $(".notification").html("");
                        //En caso de que el usuraio logeado revise un oficio que no sea de su cargo se ocultaran 
                        //las siguientes clases, en dado caso que el oficio este terminado se mostrara el form que tendra los datos del archivo respuesta     
                        if('{{Auth::user()->profile->position->id}}' != data.encargado.id)
                        {
                           
                            $(".fileReaded").hide();                            
                            $(".saveFile").hide();
                            $(".answerFile").hide();
                            $("#storeAnswer").hide();
                            $('#oficios2').hide();  

                            switch (data.estatus) 
                            {
                                case 0:
                                    if (data.type == 1) 
                                    {
                                        $(function () {
                                            $('.adjuntar').html("");
                                            $('.fecha_entrega').html('<label id="date" for="">Fecha Entrega</label>'
                                                                        +'<input class="form-control" disabled type="date" id="fecha_entrega" disabled name="fecha_entrega">'        
                                                                        );
                                        });
                                    }
                                    break;
                                 case 1:
                                    $(function () {
                                        $(".fileReaded").hide();                            
                                        $(".saveFile").hide();
                                        $(".answerFile").hide();
                                        $("#storeAnswer").hide();
                                        $('#oficios2').hide(); 
                                    });
                                    break;
                                case 3:
                                 $("#storeAnswer").show(); 
                                 if (data.type == 1) 
                                 {
                                     $(function () {
                                        $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');
                                        $('.fecha_entrega').html('<label id="date" for="">Fecha Entrega</label>'
                                                                    +'<input class="form-control" type="date" id="fecha_recepcion"  value="'+data.fecha_recepcion+'" disabled name="fecha_recepcion">'        
                                                                    );
                                     });
                                     $("#storeAnswer").hide();
                                 }
                                 $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');
                                    break;
                                case 2:
                                    
                                    $(function () {
                                        $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');
                                        $(".fileReaded").hide();                            
                                        $(".saveFile").hide();
                                        $(".answerFile").hide();
                                        $("#storeAnswer").hide();
                                        $('#oficios2').hide(); 
                                    });
                                    break; 
                                    
                                case 4:
                                $(function () {
                                    $("#storeAnswer").hide();
                                    $(".answerFile").hide();
                                    $(".adjuntar").html('');
                                    $(".saveFile").hide();
                                    $(".fileReaded").hide();
                                });
                                    break;        
                            
                                default:
                                break;
                            }
                        }
                        else
                        {
                            //Eventos que pasaran en diferentes tipos de estatus que se encuentre el oficio en caso de ser un usuario de no escritura
                            switch (data.estatus) 
                            {
                                case 1:
                                    $(function () {
                                        $("#storeAnswer").hide();
                                        $(".answerFile").hide();
                                        $(".fileReaded").show();
                                        $('.fileReaded').data('id', data.id);
                                        $('.fileReaded').data('status', 2);
                                        $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');

                                        if (data.respuesta == 0)
                                        {
                                            $('.fileReaded').data('status', 3); 
                                        } 
                                    });
                                    
                                    break;

                                case 2:
                                    $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> '); 
                                    $("#storeAnswer :input").prop('readonly', false);
                                    break;

                                case 3:
                                    if (data.type == 1) 
                                    {
                                        $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');
                                        $("#storeAnswer").hide();
                                        $(".saveFile").hide(); 
                                    }

                                    if (data.type == 0) 
                                    {
                                        $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');                                
                                    }
                                    break;    

                                case 0:
                                    if (data.type == 1) 
                                    {
                                        $(function () {
                                            $("#storeAnswer").hide();
                                            $(".saveFile").show();
                                            $('.oficio').hide();
                                            $('.answerFile').hide();
                                            $('.fileReaded').hide();
                                            $(".adjuntar").html(' <label for="oficio">Adjuntar Oficio</label>\
                                                                <input type="file" id="oficios2" name="oficio"/>\
                                                                <input type="hidden" name="id" value="'+data.id+'">\
                                                                ');
                                            $('.fecha_entrega').html('<label id="date" for="">Fecha Entrega</label>'
                                                                    +'<input class="form-control" type="date" id="fecha_entrega" name="fecha_recepcion" required>'        
                                                                    );
                                            $('.type').val(1);                          
                                            $('#oficios2').simpleFilePreview();
                                            $(".storeFile :input").prop('disabled', false); 
                                            $(".storeFile :input").prop('readonly', true);
                                            $("#fecha_entrega").prop('readonly', false);
                                            $("#emisor").prop('readonly', false);
                                        });
                                    }

                                    if (data.type == 2) 
                                    {
                                        $(function () {
                                            $("#storeAnswer").hide();
                                            $(".saveFile").show();
                                            $('.oficio').hide();
                                            $('.answerFile').hide();
                                            $('.fileReaded').hide();
                                            $(".adjuntar").html('<label for="oficio">Adjuntar Oficio</label>\
                                                                <input type="file" id="oficios2" name="oficio"/>\
                                                                <input type="hidden" name="id" value="'+data.id+'">\
                                                                ');
                                            $('.fecha_entrega').html('<label id="date" for="">Fecha Entrega</label>'
                                                                    +'<input class="form-control" type="date" id="fecha_entrega" name="fecha_recepcion" required>'        
                                                                    );
                                            $('.type').val(2);                                             
                                            $('#oficios2').simpleFilePreview();
                                            $(".storeFile :input").prop('disabled', false); 
                                            $(".storeFile :input").prop('readonly', true);
                                            $("#fecha_entrega").prop('readonly', false);    
                                        });
                                    }
                                    break;
                                case 4:
                                $(function () {
                                    $("#storeAnswer").hide();
                                    $(".answerFile").hide();
                                    $(".adjuntar").html('');
                                    $(".saveFile").hide();
                                    $(".fileReaded").hide();
                                });
                                    break;        
                            
                                default:
                                    break;
                            }

                        }

                       
                        //Condicion que valida en caso de que el oficio requiera una respuesta                                                           
                        if(data.respuesta == 1)
                        {                                                   
                            $('.respuesta').append('<div class="form-group col officeAnswer">\
                                                        <label>Adjuntar oficio respuesta</label>\
                                                        <input type="file" id="oficioRespuesta" name="oficio"/>\
                                                        <input type="hidden" name="id_padre" value='+data.id+'>\
                                                    </div>'                                                    
                                                    );                    
                            $('#oficioRespuesta').simpleFilePreview();
                            //$(".answerFile").show();
                            $(".fileReaded").hide();
                            $(".saveFile").hide();
                            $("#storeAnswer :input").prop('disabled', false); 
                            //$("#storeAnswer").show();                                                                                
                        }                   
                        else if(data.respuesta == 0)
                        {
                            $(".saveFile").hide();
                            $('.respuesta').html("");
                            $("#storeAnswer").hide();
                            $(".fileReaded").show();
                            $('.fileReaded').data('id', data.id);                        
                        }
                    @endcan

                    //Condiciones para cambiar el texto de etiquetas dependiendo si el oficio es de entrada o de salida
                    if(data.type == 1)
                    {
                        $("#lbemisor").html('Dirigido a');
                        $("#lbdate").html('Fecha Entrega');
                    }
                    else if(data.type == 0)
                    {
                            $("#lbemisor").html('Emisor');
                            $("#lbdate").html('Fecha Recepcion');
                            $('.fecha_entrega').html('<label id="date" for="">Fecha recepcion</label>'
                                +'<input class="form-control" type="date" id="fecha_entrega" name="fecha_recepcion" disabled value='+data.fecha_recepcion+'>'        
                                );     
                    }

                    //Condicion para verificar si el ajax contiene los datos del oficio respuesta y mostrarlos
                    if(data.resp)
                    {   
                        modal.find('#no_oficio_r').val(data.response.no_oficio);
                        modal.find('#emisor_r').val(data.response.emisor);
                        modal.find('#fecha_emision_r').val(data.response.fecha_emision);
                        modal.find('#asunto_r').val(data.response.asunto);                    
                        modal.find('#fecha_recepcion_r').val(data.response.fecha_recepcion);
                        modal.find('#dependencia_r').val(data.response.dependencia);
                        modal.find(":input").prop('disabled', true);
                        $(function () {
                            $(".respuesta").html("<button data-id='"+data.response.id+"' class='btn btn-success oficio'>Descargar Oficio</button>");    
                            if(data.estatus == 3)
                            {
                                $("#storeAnswer").show();                            
                            }                    
                        });
                    }
                    else
                    {
                        @can("write_offices") 
                        
                        @endcan    
                        $("#storeAnswer").trigger('reset');
                    }


                    $("#modal-details-offices button").prop('disabled', false);
                    if(data.estatus == 3)
                    {
                        $(".fileReaded").hide();
                        $(".answerFile").hide();
                    }
                    if(data.fecha_respuesta)
                    {
                        $('.fechaRespuesta').html('<label for="">Fecha limite respuesta</label>\
                            <input class="form-control btn-warning" type="date" name="fecha_recepcion" disabled value='+data.fecha_respuesta+' >    ');
                    }
                    else
                    {
                        $('.fechaRespuesta').html('');
                    }

                    if(data.estatus == 1)
                    {
                        $('.adjuntar').html(' <a href="#" data-id='+data.id+' class="btn btn-primary oficio">Descargar Oficio</a> ');
                    }

                    // if (data.estatus == 4) 
                    // {
                    //     $("#storeAnswer").hide();
                    //     $(".answerFile").hide();
                    //     $(".adjuntar").hide();
                    //     $(".saveFile").hide();
                    // }

                    modal.modal('show');
                }
            });
        });
        
       //Evento para descargar oficio tanto entrada, salida o respuesta
       $(document).on("click", ".oficio", function(e){
           e.preventDefault();
            var id = $(this).data("id");
            window.open('/soporte/descargar/oficio/'+id, '_blank');
        });

        //Evento para limpiar clases cuando el modal se cierre
        $(document).on("hide.bs.modal", '#modal-details-offices', function (e) {
            $('.respuesta').html("");
            $('#WithCopy').html("");
        });

        //Evento para enviar formulario de archivo de respuesta 
        $(document).on("click", '.answerFile', function (e) {
            e.preventDefault();
          $('#storeAnswer').submit();
        });

        //Ajax para cambiar estatus de oficio
        $(document).on("click", '.fileReaded', function (e) {
            e.preventDefault();
            var id = $(this).data("id"),
            estatus = $(this).data('status'),            
            token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{route('change.status.by.id')}}",
                data: {
                    _token:token,
                    id:id,
                    estatus:estatus
                },
                success: function (data) {
                    window.location.href = "/soporte/offices";
                }
            });
          
        });
 
            $(".dataTable").DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                order: [ [4, 'desc']]
            });

            $("#attach").prop('disabled', true);
            $('#attach2').prop('disabled',true);
        //Evento para boton de guardar el archivo de salida una vez que su no de oficio fue generado
        $(document).on('click', '.saveFile', function (e) {
           // e.preventDefault();
           if($('.storeFile').validate())
           {
             $('.storeFile').submit();
           }
        });
        //Ajax para enviar alerta de que se debe revisar 
        $(document).on('click', '.alertNotification', function (e) {
            e.preventDefault();
            var id = $(this).data('id');
            var encargado = $(this).data('res');
            var token = "{{ csrf_token() }}";
            $.ajax({
                type: "POST",
                url: "{{route('alert.notification')}}",
                data:{
                    id:id,
                    encargado:encargado,
                    _token:token
                },
                success: function (data) {
                    $('.messageNotification').html(data);
                    $('.messageNotification').show();
                    $(".messageNotification").delay(4000).slideUp(200, function() {
                        $(this).hide();
                    });
                }
            });
        });

        $(document).on('submit','.NewOffice',function(e){
            e.preventDefault();
            var fileSize = $('input[type="file"]').get(0).files[0].size;

            if(fileSize > 5000000){
                    $('.file-size').html('El archivo no debe superar los 5MB');
                    $('.file-size').show();
                    $(".file-size").delay(4000).slideUp(200, function() {
                    $(this).hide();
                });
            }
            else
            {
                $(this)[0].submit();
            }
        });

        $(document).on('submit','.storeFile',function(e){
            e.preventDefault();
            var fileSize = $('#oficios2').get(0).files[0].size;

            if(fileSize > 5000000){
                    $('.file-size').html('El archivo no debe superar los 5MB');
                    $('.file-size').show();
                    $(".file-size").delay(4000).slideUp(200, function() {
                    $(this).hide();
                });
            }
            else
            {
                $(this)[0].submit();
            }
        });

        $(document).on('click','.delete',function(e){
            e.preventDefault();
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";

            $.confirm({
                title: '¿ Estas Seguro (a) ?',
                content: 'Se cancelara el oficio.',
                buttons: {
                    Confirmar: {
                        text: 'Confirmar',
                        btnClass: 'btn btn-primary btn-confirm',
                        action: function(){
                            $.ajax({
                                url: "{{ route('delete.office.by.id') }}",
                                type: "POST",
                                data: {
                                    _token: token,
                                    id: id
                                },
                            }).done(function(result){
                                window.location.href = "/soporte/offices";
                            });
                        }
                    },
                    Cancelar: {
                        text: 'Cancelar',
                        btnClass: 'btn btn-secondary btn-confirm',
                    }
                }
            });
            
        });

        $(document).on('click','.restore',function(e){
            e.preventDefault();
            var id = $(this).data("id"),
                token = "{{ csrf_token() }}";

            $.confirm({
                title: '¿ Estas Seguro (a) ?',
                content: 'Se restaurara el oficio.',
                buttons: {
                    Confirmar: {
                        text: 'Confirmar',
                        btnClass: 'btn btn-primary btn-confirm',
                        action: function(){
                            $.ajax({
                                url: "{{ route('restore.office.by.id') }}",
                                type: "POST",
                                data: {
                                    _token: token,
                                    id: id
                                },
                            }).done(function(result){
                                window.location.href = "/soporte/offices";
                            });
                        }
                    },
                    Cancelar: {
                        text: 'Cancelar',
                        btnClass: 'btn btn-secondary btn-confirm',
                    }
                }
            });
            
        });

        $('#nav-home-tab a').on('click', function (e) {
            e.preventDefault()
            $(this).tab('show')
        })
    

           
    </script>    
@endsection