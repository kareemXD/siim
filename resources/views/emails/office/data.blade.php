<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Correo de Contacto</title>
    <style>
        .btn-primary { 
            margin: 10px;
            border-radius: 10px;
            background-color: #0062A0; 
            color: #fff;
            border-color: #fff;
        } 
        
        .blue-text{
            color: #0062A0;
        }
        .white-text{
            color: #fff;
        }
        .white-text a{
            color: #fff;
        }
        .center{
            text-align: center;
        }
        .logo{
            text-align: center !important;
            padding: 30px 0px !important;
            background-color: #fff !important;
        }
        
        .footer{
            text-align: center !important;
            padding: 10px 0px !important;
            background-color: #fff !important;
        }
        .white{
            background-color: #fff !important;
        }
    </style>
</head>
<body>
    <table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
        {{-- <tr>
            <td class="white logo">
                <img src="http://www.implanbadeba.gob.mx/assets/images/logo-implan.png" alt="Logo IMPLAN" width="50%">
            </td>
        </tr> --}}
        <tr style="background-color: #13629D; background-size:cover;">
            <td>
                @if($data->asunto == 'alerta')
                    <h1  class="center white-text">Notificacion de oficio</h1>
                    <p class="white-text center" >Alerta de aviso para atencion al oficio: {{ $data->no_oficio }} </p>  
                @else               
                    <h1  class="center white-text">Notificacion de oficio</h1>
                    @if($data->cc == 0)
                        <p class="white-text center" >Se le ha asignado el oficio: {{ $data->no_oficio }} </p>   
                    @elseif($data->cc == 1)
                        <p class="white-text center">Se le ha asignado la copia de asignacion del oficio: {{ $data->no_oficio }} </p>                    
                    @endif
                    <p class="white-text center">Con el asunto: {{ $data->asunto }} </p>
                    <p  class="white-text center" ><a class="btn-primary" href="{{route('check.office.by.id', $data->id)}}">Revisar Oficio</a></p>
                @endif                
            </td>
        </tr>
    </table>
</body>
</html> 
