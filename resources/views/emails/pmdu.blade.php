<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Correo de Contacto</title>
    <style>
        .btn-primary { 
            margin: 10px;
            border-radius: 10px;
            background-color: #0062A0; 
            color: #fff;
            border-color: #fff;
        } 
        
        .blue-text{
            color: #0062A0;
        }
        .white-text{
            color: #fff;
        }
        .white-text a{
            color: #fff;
        }
        .center{
            text-align: center;
        }
        .justify{
            text-align: justify;
        }
        .logo{
            text-align: center !important;
            padding: 30px 0px !important;
            background-color: #fff !important;
        }
        
        .footer{
            text-align: center !important;
            padding: 10px 0px !important;
            background-color: #fff !important;
        }
        .white{
            background-color: #fff !important;
        }
    </style>
</head>
<body>
    <table align="center" border="1" cellpadding="0" cellspacing="0" width="600">
        <tr style="background-color: #13629D; background-size:cover;">
            <td>
                <h1  class="justify white-text">Notificación de respuesta de observaciones</h1>
                <p class="white-text justify" >Notificación de aviso para atención a las observaciones: {{ $data->folio }} </p>  
            
                <p class="white-text justify">Buen día, <b>{{ $data->nombre." ".$data->apellidos }},</b><br><br>
                    Con la finalidad de dar respuesta a las observaciones realizadas al 
                    Plan Municipal de Desarrollo Urbano (PMDU) de Bahía de Banderas, le informamos por favor asistir a las oficinas del 
                    Instituto Municipal de Planeación de Bahía de Banderas (IMPLAN) por su oficio de respuesta.
                </p>
                {{-- <h1 class="justify white-text">Tus Observaciones</h1>
                <p  class="white-text justify" >{{ $data->observaciones }}</p>
                <h1 class="justify white-text">Respuesta</h1>
                <p  class="white-text justify" >{{ $data->respuesta }}</p> --}}
            </td>
        </tr>
    </table>
</body>
</html> 
