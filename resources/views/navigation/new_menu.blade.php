@extends('layouts.index')

@section('title') Nuevo Menu @endsection

@section('css')
    
@endsection

@section('content')
    <div class="row justify-content-center">
        <h3>Nuevo Menu</h3>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    
    <form action="{{ route('store.menu') }}" method="POST">
        @csrf
        <div class="row justify-content-center">
            <fieldset class="col-xl-8 col-sm-12 fieldset">
                <legend class="legend">Informacion del Menu:</legend>
                <div class="form-row">
                    <div class="form-group col">
                        <label for="name2">Nombre</label>
                        <input required type="text" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" id="name2" name="name" value="{{ old('name') }}">
                        @if ($errors->has('name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="alias">Alias</label>
                        <input required type="text" class="form-control {{ $errors->has('alias') ? ' is-invalid' : '' }}" id="alias" name="alias" value="{{ old('alias') }}">
                        @if ($errors->has('alias'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('alias') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col">
                        <label for="url">Ruta</label>
                        <input type="text" class="form-control {{ $errors->has('url') ? ' is-invalid' : '' }}" id="url" name="url" value="{{ old('url') }}">
                        @if ($errors->has('url'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('url') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-sm-6">
                        <label for="position">Posición</label>
                        <input required type="number" class="form-control {{ $errors->has('position') ? ' is-invalid' : '' }}" id="position" name="position" value="{{ old('position') }}">
                        @if ($errors->has('position'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('position') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-sm-5">
                        <label for="icon">Icono</label>
                        <input required type="text" class="form-control {{ $errors->has('icon') ? ' is-invalid' : '' }}" id="icon" name="icon" value="{{ old('icon') }}">
                        @if ($errors->has('icon'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('icon') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-sm-1 justify-content-center align-items-center" style="display:flex;">
                        <i class="fa-2x icon-viewer"></i>
                    </div>
                </div>
            </fieldset>
            <fieldset class="col-xl-8 col-sm-12 fieldset">
                <legend class="legend">Submenus:</legend>
                <div class="row justify-content-end">
                    <button type="button" class="btn btn-primary mr-2 mb-2 add-submenu"><i class="fas fa-plus-circle mr-2"></i> Añadir Submenu</button>
                </div>
                <div class="row justify-content-center mb-4" id="submenus-container">
                    
                </div>
            </fieldset>
        </div>
        <div class="row justify-content-center mt-3">
            <button type="submit" class="btn btn-primary mr-2 mb-2"><i class="fas fa-save mr-2"></i> Guardar</button>
        </div>
    </form>
    
@endsection

@section('modals')

@endsection

@section('js')
    <script>
        $(function(){
            $("#users").addClass('active');
            $(document).on("focusout", "#icon", function(){
                $(".icon-viewer").addClass($(this).val());
            });

            $(document).on("click", ".add-submenu", function(){
                $("#url").attr("disabled", true);
                $("#submenus-container").append('<fieldset class="col-sm-11 fieldset submenu">\
                        <legend class="legend">Informacion del Submenu:</legend>\
                        <button type="button" class="btn btn-danger circle btn-delete"><i class="fas fa-times-circle"></i></button>\
                        <div class="form-row">\
                            <div class="form-group col">\
                                <label for="subname2">Nombre</label>\
                                <input required type="text" class="form-control" id="subname" name="subname[]">\
                            </div>\
                            <div class="form-group col">\
                                <label for="suburl">Ruta</label>\
                                <input required type="text" class="form-control" id="suburl" name="suburl[]">\
                            </div>\
                        </div>\
                        <div class="form-row">\
                            <div class="form-group col-sm-6">\
                                <label for="subalias">Alias</label>\
                                <input required type="text" class="form-control" id="subalias" name="subalias[]">\
                            </div>\
                            <div class="form-group col-sm-6">\
                                <label for="subposition">Posición</label>\
                                <input required type="number" class="form-control" id="subposition" name="subposition[]">\
                            </div>\
                        </div>\
                    </fieldset>');
            });

            $(document).on("click", ".btn-delete", function(){
                $(this).parent("fieldset").remove();

                if($(".submenu").length == 0)
                {
                    $("#url").removeAttr("disabled");
                }
            });
        });
    </script>
@endsection