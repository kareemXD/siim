@extends('layouts.index')

@section('title') Usuarios @endsection

@section('css')
    
@endsection

@section('content')
    <div class="row justify-content-center">
        <h3>Usuarios</h3>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    @can('write_users')
    <div class="row justify-content-end mb-2">
        <button type="button" class="btn btn-primary mr-3" data-toggle="modal" data-target="#modal-new-user"><i class="fas fa-user-plus mr-2"></i> Crear Usuario</button>
    </div>
    @endcan
    <div class="table-responsive">
        <table class="table dataTable">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Correo</th>
                    <th scope="col">Puesto</th>
                    <th scope="col">Dirección</th>
                    <th scope="col">Rol</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @if (count($users) > 0)
                    @foreach ($users as $user)
                        <tr class="edit-user" data-id="{{ $user->id }}">
                            <td scope="row">{{ $user->id }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->profile->position->nombre }}</td>
                            <td>{{ $user->profile->position->direction->abreviacion }}</td>
                            <td>{{ $user->getRoleNames()[0] }}</td>
                            <td class="center"><button type="button" class="btn btn-danger delete-user" data-user="{{ $user->id }}"><i class="fas fa-trash-alt"></i></button></td>
                        </tr>
                    @endforeach
                @endif
                
            </tbody>
        </table>
    </div>
@endsection

@section('modals')
    @can('write_users')
        @include('partials.modals.modal_new_user')
    @endcan
    @include('partials.modals.modal_update_user')
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js"></script>
    <script>
        $(function(){
            $("#users").addClass('active');

            $(".dataTable").DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'pdfHtml5'
                ]
            });
            
            $(document).on('dblclick', '.edit-user', function(){
                var id = $(this).data("id"),
                    token = "{{ csrf_token() }}";
                $.ajax({
                    url: "{{ route('get.user.data') }}",
                    type: "POST",
                    data: {
                        _token: token,
                        id: id
                    },
                }).done(function(result){
                    var modal = $("#modal-update-user");
                    modal.find('#id').val(result.id);
                    modal.find('.name').val(result.profile.nombre);
                    modal.find('.ape_pat').val(result.profile.apellido_pat);
                    modal.find('.ape_mat').val(result.profile.apellido_mat);
                    modal.find('.email').val(result.email);
                    modal.find('.position').val(result.profile.puesto_id).trigger('change');
                    modal.find('.role').val(result.role).trigger('change');

                    $(".select2").select2();
                    modal.modal("show");
                });
            });

            $(document).on("click", ".delete-user", function(){
                var id = $(this).data("user"),
                    token = "{{ csrf_token() }}";

                var _this = $(this);
                $.confirm({
                    title: '¿ Estas Seguro (a) ?',
                    content: 'Se desasignara la meta a la dependecia.',
                    buttons: {
                        Confirmar: {
                            text: 'Confirmar',
                            btnClass: 'btn btn-primary btn-confirm',
                            action: function(){
                                $.ajax({
                                    url: "{{ route('deactivate.user') }}",
                                    type: "POST",
                                    data: {
                                        _token: token,
                                        id: id,
                                    },
                                }).done(function(result){
                                    _this.parents("tr").fadeOut("slow");
                                });
                            }
                        },
                        Cancelar: {
                            text: 'Cancelar',
                            btnClass: 'btn btn-secondary btn-confirm',
                        }
                    }
                });
            });
        });
    </script>
@endsection