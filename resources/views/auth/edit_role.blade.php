@extends('layouts.index')

@section('title') Roles @endsection

@section('css')
    
@endsection

@section('content')
    <div class="row justify-content-center">
        <h3>Roles</h3>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary text-center" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    @can('write_roles')    
    <form action="{{ route('update.role') }}" method="POST">
        @csrf
        <input type="hidden" name="role_id" value="{{ $rol->id }}">
        <div class="row justify-content-center">
            <fieldset class="col-xl-8 col-sm-12 fieldset">
                <legend class="legend">Nombre del Rol</legend>
                <div class="form-group">
                    <input type="text" class="form-control" id="rol-name" name="name" value="{{ $rol->name }}" required>
                </div>
            </fieldset>
        </div>
        <div class="row justify-content-center">
            <fieldset class="col-xl-8 col-sm-12 fieldset">
                <legend class="legend">Secciones</legend>
                @foreach ($menus as $menu)
                    <ul class='list-group'>
                        
                        @if (count($menu->submenus) > 0)
                            <li class='list-group-item'>
                                <input type='checkbox'  data-son='{{ $menu->id }}' class="parent" id="menu{{ $menu->id }}"> <label for="menu{{ $menu->id }}" class="mb-0"><i class="fa-fw {{ $menu->icono }} mr-3 ml-2 "></i> {{ $menu->nombre }}</label>
                            </li>
                            <ul class='list-group list2'>
                                @foreach ($menu->submenus as $submenu)
                                    @if (in_array($submenu->id, $rol->ids))
                                        <li class='list-group-item'>
                                            <div class='row'>
                                                <div class='col-sm-6'>
                                                    <input type='checkbox'  checked id="submenu{{ $submenu->id }}" data-parent='{{ $menu->id }}' class="mr-3 son"  name='sections[]' value='{{ $submenu->id }}'><label for="submenu{{ $submenu->id }}" class="mb-0">{{ $submenu->nombre }}</label>
                                                </div>
                                                <div class='col-sm-6'>
                                                    <select class='form-control' data-parentinput='{{ $menu->id }}' name='permissions[]' style='height: initial; display: none;'>
                                                        <option value='' disabled> Seleccionar Permisos</option>
                                                        @if ($rolelv->hasPermissionTo("read_".$submenu->alias))
                                                            <option value='1' selected> Lectura </option> 
                                                        @else
                                                            <option value='1'> Lectura </option> 
                                                        @endif
                                                        
                                                        @if ($rolelv->hasPermissionTo("write_".$submenu->alias))
                                                            <option value='2' selected > Escritura </option> 
                                                        @else
                                                            <option value='2' > Escritura </option> 
                                                        @endif
                                                    </select>
                                                </div> 
                                            </div> 
                                        </li>
                                    @else
                                        <li class='list-group-item'>
                                            <div class='row'>
                                                <div class='col-sm-6'>
                                                    <input type='checkbox'  id="submenu{{ $submenu->id }}" data-parent='{{ $menu->id }}' class="mr-3 son"  name='sections[]' value='{{ $submenu->id }}'><label for="submenu{{ $submenu->id }}" class="mb-0">{{ $submenu->nombre }}</label>
                                                </div>
                                                <div class='col-sm-6'>
                                                    <select class='form-control' data-parentinput='{{ $menu->id }}' name='permissions[]' disabled style='height: initial; display: none;'>
                                                        <option value=''> Seleccionar Permisos</option>
                                                        <option value='1' > Lectura </option> 
                                                        <option value='2' > Escritura </option> 
                                                    </select>
                                                </div> 
                                            </div> 
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @else
                            @if (in_array($menu->id, $rol->ids))
                                <li class='list-group-item'>
                                    <div class='row'>
                                        <div class='col-sm-6'>
                                            <input type='checkbox'  checked data-son='{{ $menu->id }}' class="parent" id="menu{{ $menu->id }}" name='sections[]' value='{{ $menu->id }}'> <label for="menu{{ $menu->id }}" class="mb-0"><i class="fa-fw {{ $menu->icono }} mr-3 ml-2"></i> {{ $menu->nombre }}</label>
                                        </div>
                                        <div class='col-sm-6'>
                                            <select class='form-control' data-parentinput='{{ $menu->id }}' name='permissions[]' style='height: initial; display: none;'>
                                                <option value='' disabled> Seleccionar Permisos</option>
                                                @if ($rolelv->hasPermissionTo("read_".$menu->alias))
                                                    <option value='1' selected> Lectura </option> 
                                                @else
                                                    <option value='1'> Lectura </option> 
                                                @endif
                                                
                                                @if ($rolelv->hasPermissionTo("write_".$menu->alias))
                                                    <option value='2' selected > Escritura </option> 
                                                @else
                                                    <option value='2' > Escritura </option> 
                                                @endif 
                                            </select>
                                        </div> 
                                    </div> 
                                </li>
                            @else
                                <li class='list-group-item'>
                                    <div class='row'>
                                        <div class='col-sm-6'>
                                            <input type='checkbox'  data-son='{{ $menu->id }}' class="parent" id="menu{{ $menu->id }}" name='sections[]' value='{{ $menu->id }}'> <label for="menu{{ $menu->id }}" class="mb-0"><i class="fa-fw {{ $menu->icono }} mr-3 ml-2"></i> {{ $menu->nombre }}</label>
                                        </div>
                                        <div class='col-sm-6'>
                                            <select class='form-control' data-parentinput='{{ $menu->id }}' name='permissions[]' style='height: initial; display: none;'>
                                                <option value='' disabled selected> Seleccionar Permisos</option>
                                                <option value='1' > Lectura </option> 
                                                <option value='2' > Escritura </option> 
                                            </select>
                                        </div> 
                                    </div> 
                                </li>
                            @endif
                        @endif
                    </ul>
                @endforeach
            </fieldset>
        </div>
        <div class="row justify-content-center mt-3">
            <button type="submit" id="btnSubmit" disabled class="btn btn-primary mr-2 mb-2"><i class="fas fa-save mr-2"></i> Guardar</button>
        </div>
    </form>
    @else
        <input type="hidden" name="role_id" value="{{ $rol->id }}">
        <div class="row justify-content-center">
            <fieldset class="col-xl-8 col-sm-12 fieldset">
                <legend class="legend">Nombre del Rol</legend>
                <div class="form-group">
                    <input disabled type="text" class="form-control" id="rol-name" name="name" value="{{ $rol->name }}" required>
                </div>
            </fieldset>
        </div>
        <div class="row justify-content-center">
            <fieldset class="col-xl-8 col-sm-12 fieldset">
                <legend class="legend">Secciones</legend>
                @foreach ($menus as $menu)
                    <ul class='list-group'>
                        
                        @if (count($menu->submenus) > 0)
                            <li class='list-group-item'>
                                <input disabled type='checkbox'  data-son='{{ $menu->id }}' class="parent" id="menu{{ $menu->id }}"> <label for="menu{{ $menu->id }}" class="mb-0"><i class="fa-fw {{ $menu->icono }} mr-3 ml-2 "></i> {{ $menu->nombre }}</label>
                            </li>
                            <ul class='list-group list2'>
                                @foreach ($menu->submenus as $submenu)
                                    @if (in_array($submenu->id, $rol->ids))
                                        <li class='list-group-item'>
                                            <div class='row'>
                                                <div class='col-sm-6'>
                                                    <input disabled type='checkbox'  checked id="submenu{{ $submenu->id }}" data-parent='{{ $menu->id }}' class="mr-3 son"  name='sections[]' value='{{ $submenu->id }}'><label for="submenu{{ $submenu->id }}" class="mb-0">{{ $submenu->nombre }}</label>
                                                </div>
                                                <div class='col-sm-6'>
                                                    <select disabled class='form-control' data-parentinput='{{ $menu->id }}' name='permissions[]' style='height: initial; display: none;'>
                                                        <option value='' disabled> Seleccionar Permisos</option>
                                                        @if ($rolelv->hasPermissionTo("read_".$submenu->alias))
                                                            <option value='1' selected> Lectura </option> 
                                                        @else
                                                            <option value='1'> Lectura </option> 
                                                        @endif
                                                        
                                                        @if ($rolelv->hasPermissionTo("write_".$submenu->alias))
                                                            <option value='2' selected > Escritura </option> 
                                                        @else
                                                            <option value='2' > Escritura </option> 
                                                        @endif
                                                    </select>
                                                </div> 
                                            </div> 
                                        </li>
                                    @else
                                        <li class='list-group-item'>
                                            <div class='row'>
                                                <div class='col-sm-6'>
                                                    <input disabled type='checkbox'  id="submenu{{ $submenu->id }}" data-parent='{{ $menu->id }}' class="mr-3 son"  name='sections[]' value='{{ $submenu->id }}'><label for="submenu{{ $submenu->id }}" class="mb-0">{{ $submenu->nombre }}</label>
                                                </div>
                                                <div class='col-sm-6'>
                                                    <select disabled class='form-control' data-parentinput='{{ $menu->id }}' name='permissions[]' style='height: initial; display: none;'>
                                                        <option value=''> Seleccionar Permisos</option>
                                                        <option value='1' > Lectura </option> 
                                                        <option value='2' > Escritura </option> 
                                                    </select>
                                                </div> 
                                            </div> 
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @else
                            @if (in_array($menu->id, $rol->ids))
                                <li class='list-group-item'>
                                    <div class='row'>
                                        <div class='col-sm-6'>
                                            <input disabled type='checkbox'  checked data-son='{{ $menu->id }}' class="parent" id="menu{{ $menu->id }}" name='sections[]' value='{{ $menu->id }}'> <label for="menu{{ $menu->id }}" class="mb-0"><i class="fa-fw {{ $menu->icono }} mr-3 ml-2"></i> {{ $menu->nombre }}</label>
                                        </div>
                                        <div class='col-sm-6'>
                                            <select disabled class='form-control' data-parentinput='{{ $menu->id }}' name='permissions[]' style='height: initial; display: none;'>
                                                <option value='' disabled> Seleccionar Permisos</option>
                                                @if ($rolelv->hasPermissionTo("read_".$menu->alias))
                                                    <option value='1' selected> Lectura </option> 
                                                @else
                                                    <option value='1'> Lectura </option> 
                                                @endif
                                                
                                                @if ($rolelv->hasPermissionTo("write_".$menu->alias))
                                                    <option value='2' selected > Escritura </option> 
                                                @else
                                                    <option value='2' > Escritura </option> 
                                                @endif 
                                            </select>
                                        </div> 
                                    </div> 
                                </li>
                            @else
                                <li class='list-group-item'>
                                    <div class='row'>
                                        <div class='col-sm-6'>
                                            <input disabled type='checkbox'  data-son='{{ $menu->id }}' class="parent" id="menu{{ $menu->id }}" name='sections[]' value='{{ $menu->id }}'> <label for="menu{{ $menu->id }}" class="mb-0"><i class="fa-fw {{ $menu->icono }} mr-3 ml-2"></i> {{ $menu->nombre }}</label>
                                        </div>
                                        <div class='col-sm-6'>
                                            <select disabled class='form-control' disabled data-parentinput='{{ $menu->id }}' name='permissions[]' style='height: initial; display: none;'>
                                                <option value='' disabled selected> Seleccionar Permisos</option>
                                                <option value='1' > Lectura </option> 
                                                <option value='2' > Escritura </option> 
                                            </select>
                                        </div> 
                                    </div> 
                                </li>
                            @endif
                        @endif
                    </ul>
                @endforeach
            </fieldset>
        </div>
    @endcan
@endsection

@section('js')
    <script>
        $(function(){
            $("#users").addClass('active');
            
		    $('.parent').on('change', function(){
			    var section_id = $(this).data('son');
			    if($(this).is(':checked'))
			    {
		            $('input[data-parent="'+section_id+'"]').prop('checked', true);
		            $('[data-parentInput="'+section_id+'"]').css('display', 'initial');
                    $('[data-parentInput="'+section_id+'"]').removeAttr('disabled');
		    	    $('#btnSubmit').removeAttr('disabled');
		    	    $(this).parents('ul').find('[data-parentInput="'+section_id+'"]').attr('required', true)
		        } else
		        {
		            $('input[data-parent="'+section_id+'"]').prop('checked', false);
		            $('[data-parentInput="'+section_id+'"]').css('display', 'none');
		            $(this).parents('ul').find('[data-parentInput="'+section_id+'"]').removeAttr('required')
		    	    $(this).parents('ul').find('[data-parentInput="'+section_id+'"]').val('');
		        }
		    });

		    $('.son').on('change', function(){
			    var section_id = $(this).data('parent');
			    if($(this).is(':checked'))
			    {
				    $('#btnSubmit').removeAttr('disabled');
		            $('input[data-son="'+section_id+'"]').prop('checked', true);
                    $('[data-parentInput="'+section_id+'"]').removeAttr('disabled');
		            $(this).parents('li').find('[data-parentInput="'+section_id+'"]').css('display', 'initial');
		    	    $(this).parents('li').find('[data-parentInput="'+section_id+'"]').attr('required', true)
		        }else
		        {
		    	    $(this).parents('li').find('[data-parentInput="'+section_id+'"]').val('');
		    	    $(this).parents('li').find('[data-parentInput="'+section_id+'"]').removeAttr('required')
		            $(this).parents('li').find('[data-parentInput="'+section_id+'"]').css('display', 'none');
		    	    var checkeds = $('.son:checked').each();
		    	    if (checkeds.length == 0)
		    	    {
			            $('input[data-son="'+section_id+'"]').prop('checked', false);
		    	    }

		        }
		    });
		    $('input[type="checkbox"]').on('change', function(){
			    var inputs = $('input[type="checkbox"]:checked').each();
                
			    if (inputs.length == 0)
	    	    {
	    		    $('#btnSubmit').attr('disabled', true);
	    	    }
		    });
            
            var inputs = $('input[type="checkbox"]:checked').each(function(){
                if($(this).hasClass("parent"))
                {
                    var section_id = $(this).data('son');
                    $('input[data-parent="'+section_id+'"]').prop('checked', true);
		            $('[data-parentInput="'+section_id+'"]').css('display', 'initial');
		    	    $('#btnSubmit').removeAttr('disabled');
		    	    $(this).parents('ul').find('[data-parentInput="'+section_id+'"]').attr('required', true)
                }

                if ($(this).hasClass("son")) 
                {
                    var section_id = $(this).data('parent');
                    $('#btnSubmit').removeAttr('disabled');
		            $('input[data-son="'+section_id+'"]').prop('checked', true);
		            $(this).parents('li').find('[data-parentInput="'+section_id+'"]').css('display', 'initial');
		    	    $(this).parents('li').find('[data-parentInput="'+section_id+'"]').attr('required', true)
                }
            });
        });
    </script>
@endsection