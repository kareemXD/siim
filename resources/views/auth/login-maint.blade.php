<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Matenimiento</title>
    {{-- Styles --}}
    <link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/login.css') }}">
    {{-- End Styles --}}
</head>
<body>
    <div class="wrapper d-flex">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-4 col-10 col-md-3">
                    <img src="{{ asset('assets/images/mantenimiento.png') }}" width="100%" alt="Logo IMPLAN">
                </div>
            </div>
        </div>
    </div>
</body>
</html>