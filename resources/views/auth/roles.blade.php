@extends('layouts.index')

@section('title') Roles @endsection

@section('css')
    
@endsection

@section('content')
    <div class="row justify-content-center">
        <h3>Roles</h3>
    </div>
    @if(session()->has('alert'))
        <div class="alert alert-primary text-center" role="alert">
            {{ session("alert") }}
        </div>
    @endif
    @can('write_roles')
        <div class="row justify-content-end mb-2">
            <a href="{{ route('new.role') }}" class="btn btn-primary mr-3" ><i class="fas fa-user-plus mr-2"></i> Crear Rol</a>
        </div>
    @endcan
    <div class="table-responsive">
        <table class="table dataTable">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nombre</th>
                </tr>
            </thead>
            <tbody>
                @if (count($roles) > 0)
                    @foreach ($roles as $role)
                        <tr class="edit-role" data-id="{{ $role->id }}">
                            <td scope="row">{{ $role->id }}</td>
                            <td>{{ $role->name }}</td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
@endsection

@section('js')
    <script>
        $(function(){
            $("#users").addClass('active');
            
            $(".dataTable").DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
            
            $(document).on('dblclick', '.edit-role', function(){
                var id = $(this).data("id");
                window.location.href = "/autenticacion/editar/rol/"+id;
            });
        });
    </script>
@endsection