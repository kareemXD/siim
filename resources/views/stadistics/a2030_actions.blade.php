@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class="title-page row justify-content-center mb-3">
    <h3>Lineas no Programadas Agenda 2030</h3>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif
<form method="POST" action="{{ route('stadistics.a2030.action.lines') }}" class="row justify-content-center">
    @csrf
    <div class="col-12 col-xl-8">
        <div class="form-row">
            <div class="form-group col">
                <label for="trimester">Trimestre</label>
                <select class="form-control  select2" name="trimester">
                    <option value="">Anual</option>
                    <option value="1">1° Trimestre</option>
                    <option value="2">2° Trimestre</option>
                    <option value="3">3° Trimestre</option>
                    <option value="4">4° Trimestre</option>
                </select>
            </div>
        </div>
        <div class="justify-content-center row">
            <div class="col-3 mb-2">
                <button type="submit" class="btn btn-primary btn-block" value="">Generear <i class="ml-2 fa fa-chart-bar"></i></button>
            </div>
            <div class="col-3 mb-2">
                <a href="{{ route('stadistics.a2030.action.lines.print') }}" target="_BLANK" class="btn btn-primary btn-block" >Imprimir <i class="ml-2 fa fa-chart-bar"></i></a>
            </div>
        </div>
    </div>
</form>

<div class="row justify-content-center">
    <div id="data-table" class="col-12 col-xl-10">
        <table class="table  table-bordered dataTable">
            <thead>
                <tr>
                    <th>Objetivo</th>
                    <th>Linea de Accion</th>
                    <th>Linea de Accion</th>
                    <th>Area Responsable</th>
                    <th>Accion</th>
                    <th>Avance</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($stadistics as $key => $data)
                    @if ($data["type"] == "No Prog.")
                        <tr>
                            <td>{{ $data["objective"] }}</td>
                            <td>{{ $data["point"] }}</td>
                            <td>{{ $data["name"] }}</td>
                            <td></td>
                            <td></td>
                            <td>No Prog.</td>
                        </tr>
                    @else
                        @php
                            // $rows = count($data["actions"]);
                        @endphp
                            {{-- @if ($rows == 1)
                                <tr>
                                    <td rowspan="{{ $rows }}">{{ $data["axe"] }}</td>
                                    <td rowspan="{{ $rows }}">{{ $data["point"] }}</td>
                                    <td rowspan="{{ $rows }}">{{ $data["name"] }}</td>
                                    <td>{{ $data["actions"][0]["resp"] }}</td>
                                    <td>{{ $data["actions"][0]["name"] }}</td>
                                    <td class="bg-red">{{ $data["actions"][0]["type"] }}</td>
                                </tr>
                            @endif
                        @if ($rows > 1)
                            <tr>
                                <td rowspan="{{ $rows + 1 }}">{{ $data["axe"] }}</td>
                                <td rowspan="{{ $rows + 1 }}">{{ $data["point"] }}</td>
                                <td rowspan="{{ $rows + 1 }}">{{ $data["name"] }}</td>
                            </tr> --}}
                            @foreach ($data["actions"] as $action)
                            <tr>
                                <td>{{ $data["objective"] }}</td>
                                <td>{{ $data["point"] }}</td>
                                <td>{{ $data["name"] }}</td>
                                <td>{{ $action["resp"] }}</td>
                                <td>{{ $action["name"] }}</td>
                                <td class="bg-red">{{ $action["type"] }}</td>
                            </tr>
                            @endforeach
                        {{-- @endif --}}
                    @endif
                    
                @endforeach
            </tbody>
        </table>
    </div>
</div>
    
@endsection

@section('modals')

@endsection

@section('js')
<script>
    $(function(){
        $(".dataTable").DataTable({
                language: {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Filas",
                    "infoEmpty": "Mostrando 0 de 0 Entradas",
                    "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Entradas",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                },
                dom: 'Bfrtip',
                buttons: [
                    'excelHtml5',
                    'pdfHtml5'
                ]
            });
    });
</script>
@endsection