@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class="title-page row justify-content-center mb-3">
    <h3>Generar Estadisticas</h3>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif
<form method="POST" action="{{ route('stadistics.actions.pmd2.custom') }}" class="row justify-content-center">
    @csrf
    <div class="col-12 col-xl-8">
        <div class="form-row">
            <div class="form-group col">
                <label for="axe">Eje(s)</label>
                <select required class="form-control select2" name="axe[]" multiple="multiple">
                    <option value="">Todos</option>
                    @foreach ($axes as $key => $axe)
                        <option value="{{ $axe->id }}">{{ $axe->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col">
                <label for="direction">Dirección(es)</label>
                <select required class="form-control  select2" name="direction[]" multiple="multiple">
                    <option value="">Todas</option>
                    @foreach ($directions->where('padre', "") as $direction)
                        <option value="{{ $direction->id }}">{{ $direction->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col">
                <label for="trimester">Trimestre</label>
                <select class="form-control  select2" name="trimester">
                    <option value="">Anual</option>
                    <option value="1">1° Trimestre</option>
                    <option value="2">2° Trimestre</option>
                    <option value="3">3° Trimestre</option>
                    <option value="4">4° Trimestre</option>
                </select>
            </div>
        </div>
        <div class="justify-content-center row">
            <div class="col-3 mb-2">
                <button type="submit" class="btn btn-primary btn-block" value="">Generear <i class="ml-2 fa fa-chart-bar"></i></button>
            </div>
            <div class="col-3 mb-2">
                <button id="print-stadistics-btn" type="button" class="btn btn-primary btn-block" value="">Imprimir <i class="ml-2 fa fa-chart-bar"></i></button>
            </div>
        </div>
    </div>
</form>
<form id="print-stadistics" target="_BLANK" action="{{ route('stadistics.actions.pmd2.custom.print') }}" method="post">
    @csrf
    <input type="hidden" name="image">
</form>
<div class="row justify-content-center">
    <div id="data-table" class="col-12 col-xl-10">
        <table class="table  table-bordered">
            <thead>
                <tr>
                    <th rowspan="2">Direccion</th>
                    @foreach ($stadistics["axes"] as $axe)
                        <th scope="col" colspan="5">{{ $axe }}</th>
                    @endforeach
                </tr>
                <tr>
                    @foreach ($stadistics["axes"] as $axe)
                        <th class="rotate"><div><span>100% Concluidas</span></div></th>
                        <th class="rotate"><div><span>No Concluidas</span></div></th>
                        <th class="rotate"><div><span>No Atendida</span></div></th>
                        <th class="rotate"><div><span>No Prog</span></div></th>
                        <th class="rotate"><div><span>Total</span></div></th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @php
                    $axes = $stadistics["axes"]; 
                    unset($stadistics["axes"]);
                @endphp
                @foreach ($stadistics as $key => $data)
                    <tr>
                        <td>{{ $data["name"] }}</td>
                        @foreach ($data["axes"] as $axe)
                        
                            <td class="rotate"><div><span>{{ round($axe["finish"], 1) }}</span></div></td>
                            <td class="rotate"><div><span>{{ round($axe["process"], 1) }}</span></div></td>
                            <td class="rotate"><div><span>{{ round($axe["null"], 1) }}</span></div></td>
                            <td class="rotate"><div><span>{{ round($axe["empty"], 1) }}</span></div></td>
                            <td class="rotate"><div><span>{{ round($axe["total"], 1) }}</span></div></td>
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-12 col-xl-10">
        <canvas id="myChart" ></canvas>
    </div>
</div>
    
@endsection

@section('modals')

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
    <script src="{{ asset('assets/plugins/chart.js/Chart.js') }}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.1/Chart.min.js"></script> --}}
    <script>
        var labels = [];
        var finish = [];
        var process2 = [];
        var nulls = [];
        var empty = [];
        var total = [];

        @forEach($axes as $key => $axe)
            labels.push("{{ $axe }}");
        @endforeach
        
        @php
            $data = end($stadistics);
        @endphp

        @foreach ($data["axes"] as $axe)
            finish.push("{{ round($axe['finish'], 1) }}");
            process2.push("{{ round($axe['process'], 1) }}");
            nulls.push("{{ round($axe['null'], 1) }}");
            empty.push("{{ round($axe['empty'], 1) }}");
            total.push("{{ round($axe['total'], 1) }}");
        @endforeach

		var barChartData = {
			labels: labels,
			datasets: [{
				label: '100 % Concluidas',
				backgroundColor: "#66bb6a",
				stack: 'Stack 0',
				data: finish
			}, {
				label: 'No Concluidas',
				backgroundColor: "#fbc02d",
				stack: 'Stack 1',
				data: process2
			}, {
				label: 'No Atendidadas',
				backgroundColor: "#ef5350",
				stack: 'Stack 2',
				data: nulls
			}, {
				label: 'No Programadas',
				backgroundColor: "#9e9e9e",
				stack: 'Stack 3',
				data: empty
			},{
				label: 'Total',
				backgroundColor: "#13629D",
				stack: 'Stack 4',
				data: total
			}]

		};
        
		window.onload = function() {
			var char = document.getElementById('myChart').getContext('2d');
			window.myBar = new Chart(char, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Grafica Acciones PMD'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					},
                    animation: {
                        onComplete: function () {
                            var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                            ctx.font = Chart.helpers.fontString(16, "bold", Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];                            
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                });
                            });
                        }
                    }
				}
			});
		};
        $(function(){
            
            $(document).on("click", "#print-stadistics-btn", function(){
            var newCanvas = document.querySelector("#myChart");
            var newCanvasImg = newCanvas.toDataURL("image/png", 1.0);
            $("input[name='image']").val(newCanvasImg);
            $("#print-stadistics").submit();
            //     var newCanvas = document.querySelector("#myChart");
            //     // //create image from dummy canvas
            //     var newCanvasImg = newCanvas.toDataURL("image/png", 1.0);
            //     // //creates PDF from img
            //     // var doc = new jsPDF('landscape');
                
            //     // doc.addHTML(document.body,function() {
            //     //     doc.addPage();  
            //     //     doc.addImage(newCanvasImg, 'PNG', 10, 10, 280, 150 );
            //     //     // doc.save('web.pdf');
            //     //     doc.save('Reporte-Acciones-PMD.pdf');
            //     // });
            //     $(".navbar").hide();
            //     $(".sidebar").hide();
            //     $(".btn-back").hide();
            //     $(".title-page").hide();
            //     $(".sticky-footer").hide();
            //     $("#myChart").hide();
            //     $("form").hide();
            //     $("#data-table").show();
            //     $("#data-table").css("font-size", "10px");
            //     $("#data-table").append("<img src='"+newCanvasImg+"' />");

            //     var css = '@page { size: landscape; } ';
            //     head = document.head || document.getElementsByTagName('head')[0],
            //     style = document.createElement('style');

            //     style.type = 'text/css';
            //     style.media = 'print';

            //     if (style.styleSheet){
            //     style.styleSheet.cssText = css;
            //     } else {
            //     style.appendChild(document.createTextNode(css));
            //     }

            //     head.appendChild(style);
            //     window.print();
            });
        });

	</script>
@endsection