@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class="row justify-content-center mb-3">
    <h3>Asignación del Plan Municipal</h3>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif
<div class="row justify-content-center">
    <div class="col-xl-8 col-12">
        <div class="accordion" id="muncipal-plan">
            @foreach ($axes as $axe)
                <div class="card">
                    <div class="card-header" >
                        <h5 class="mb-0">
                            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#axe{{ $axe->id}}" aria-expanded="true" aria-controls="collapseOne">
                                {{ $axe->punto}}. {{ $axe->nombre}}
                            </button>
                        </h5>
                    </div>
                    <div id="axe{{ $axe->id}}" class="collapse" data-parent="#muncipal-plan">
                        @foreach ($axe->objectives as $objective)
                            <div class="card ml-5">
                                <div class="card-header">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#objective{{ $objective->id}}" aria-expanded="false" aria-controls="collapseTwo">
                                            {{ $objective->punto}}. {{ $objective->nombre}}
                                        </button>
                                    </h5>
                                </div>
                                <div id="objective{{ $objective->id}}" class="collapse" data-parent="#axe{{ $axe->id}}">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col">Estrategia</th>
                                                <th scope="col">LA Totales</th>
                                                <th scope="col">LA Prog</th>
                                                <th scope="col">LA Prog %</th>
                                                <th scope="col">% de Avance</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($objective->strategies as $strategy)
                                                <tr>
                                                    <td>{{ $strategy->nombre }}</td>
                                                    <td>{{ count($strategy->actionLines) }}</td>
                                                    <td>{{ $strategy->scheduled() }}</td>
                                                    <td>{{ $strategy->scheduledPercentage() }} %</td>
                                                    <td>{{ $strategy->annualAdvancePercentage() }} %</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @foreach ($objective->strategies as $strategy)
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>

@endsection

@section('modals')
@endsection

@section('js')
    <script>
        $(function(){
            $(document).on("click", ".btn-desasignar", function(){
                var line = $(this).data("line"),
                    direction = $(this).data("direction"),
                    token = "{{ csrf_token() }}";

                var _this = $(this);
                $.confirm({
                    title: '¿ Estas Seguro (a) ?',
                    content: 'Se desasignara la meta a la dependecia.',
                    buttons: {
                        Confirmar: {
                            text: 'Confirmar',
                            btnClass: 'btn btn-primary btn-confirm',
                            action: function(){
                                $.ajax({
                                    url: "{{ route('dettach.municipal.plan') }}",
                                    type: "POST",
                                    data: {
                                        _token: token,
                                        line: line,
                                        direction: direction
                                    },
                                }).done(function(result){
                                    _this.parents("tr").fadeOut("slow");
                                });
                            }
                        },
                        Cancelar: {
                            text: 'Cancelar',
                            btnClass: 'btn btn-secondary btn-confirm',
                        }
                    }
                });
            });
        });
    </script>
@endsection