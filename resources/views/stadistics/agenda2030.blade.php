@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class="title-page row justify-content-center mb-3">
    <h3>Generar Estadisticas de Agenda 2030</h3>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif
<form method="POST" action="{{ route('stadistics.agenda2030') }}" class="row justify-content-center">
    @csrf
    <div class="col-12 col-xl-8">
        <div class="form-row">
            <div class="form-group col">
                <label for="trimester">Trimestre</label>
                <select class="form-control  select2" name="trimester">
                    <option value="">Anual</option>
                    <option value="1">1° Trimestre</option>
                    <option value="2">2° Trimestre</option>
                    <option value="3">3° Trimestre</option>
                    <option value="4">4° Trimestre</option>
                </select>
            </div>
        </div>
        <div class="justify-content-center row">
            <div class="col-3 mb-2">
                <button type="submit" class="btn btn-primary btn-block" value="">Generear <i class="ml-2 fa fa-chart-bar"></i></button>
            </div>
            <div class="col-3 mb-2">
                <button id="print-stadistics-btn" type="button" class="btn btn-primary btn-block" value="">Imprimir <i class="ml-2 fa fa-chart-bar"></i></button>
            </div>
        </div>
    </div>
</form>
<form id="print-stadistics" target="_BLANK" action="{{ route('stadistics.agenda2030.print') }}" method="post">
    @csrf
    <input type="hidden" name="image">
</form>
<div class="row justify-content-center">
    <div id="data-table" class="col-12 col-xl-10">
        <table class="table  table-bordered">
            <thead>
                <tr>
                    <th>Eje</th>
                    <th>Total de LA del PMD</th>
                    <th># de LA con Trabajos</th>
                    <th># de LA sin Trabajos</th>
                    <th>Atendidas al 100%</th>
                    <th>%</th>
                    <th>No logro el 100%</th>
                    <th>%</th>
                    <th>No Atendidas</th>
                    <th>%</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($stadistics as $key => $data)
                    <tr>
                        <td>{{ $data["name"] }}</td>
                        <td>{{ $data["la_total"] }}</td>
                        <td>{{ $data["la_worked"] }}</td>
                        <td>{{ $data["la_not_worked"] }}</td>
                        <td>{{ $data["la_finished"] }}</td>
                        <td>{{ $data["la_finished_per"] }}</td>
                        <td>{{ $data["la_process"] }}</td>
                        <td>{{ $data["la_process_per"] }}</td>
                        <td>{{ $data["la_null"] }}</td>
                        <td>{{ $data["la_null_per"] }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-12 col-xl-10">
        <canvas id="myChart" ></canvas>
    </div>
</div>
    
@endsection

@section('modals')

@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.debug.js"></script>
    <script src="{{ asset('assets/plugins/chart.js/Chart.js') }}"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/1.0.1/Chart.min.js"></script> --}}
    <script>
        var labels = [];
        var finish = [];
        var process2 = [];
        var nulls = [];
        var empty = [];
        var total = [];

        @forEach($stadistics as $key => $axe)
            labels.push("{{ $axe['alias'] }}");
            finish.push("{{ $axe['la_finished'] }}");
            process2.push("{{ $axe['la_process'] }}");
            nulls.push("{{ $axe['la_null'] }}");
            empty.push("{{ $axe['la_not_worked'] }}");
            total.push("{{ $axe['la_total'] }}");
        @endforeach

		var barChartData = {
			labels: labels,
			datasets: [{
				label: '100 % Concluidas',
				backgroundColor: "#66bb6a",
				stack: 'Stack 0',
				data: finish
			}, {
				label: 'No Concluidas',
				backgroundColor: "#fbc02d",
				stack: 'Stack 1',
				data: process2
			}, {
				label: 'No Atendidadas',
				backgroundColor: "#ef5350",
				stack: 'Stack 2',
				data: nulls
			}, {
				label: 'No Programadas',
				backgroundColor: "#9e9e9e",
				stack: 'Stack 3',
				data: empty
			},{
				label: 'Total',
				backgroundColor: "#13629D",
				stack: 'Stack 4',
				data: total
			}]

		};
        
		window.onload = function() {
			var char = document.getElementById('myChart').getContext('2d');
			window.myBar = new Chart(char, {
				type: 'bar',
				data: barChartData,
				options: {
					title: {
						display: true,
						text: 'Grafica Indicadores INAFED'
					},
					tooltips: {
						mode: 'index',
						intersect: false
					},
					responsive: true,
					scales: {
						xAxes: [{
							stacked: true,
						}],
						yAxes: [{
							stacked: true
						}]
					},
                    animation: {
                        onComplete: function () {
                            var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                            ctx.font = Chart.helpers.fontString(16, "bold", Chart.defaults.global.defaultFontFamily);
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];                            
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                });
                            });
                        }
                    }
				}
			});
		};
        $(function(){
            
            $(document).on("click", "#print-stadistics-btn", function(){
                var newCanvas = document.querySelector("#myChart");
                var newCanvasImg = newCanvas.toDataURL("image/png", 1.0);
                $("input[name='image']").val(newCanvasImg);
                $("#print-stadistics").submit();
            });
        });

	</script>
@endsection