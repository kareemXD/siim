@extends('layouts.index')

@section('title') Panel de Control @endsection

@section('css')
    
@endsection

@section('content')
<div class="text-center mb-3">
    <h3>Configracion de Datos</h3>
    <h4>( {{ Auth::user()->profile->position->direction->nombre }} )</h4>
</div>
@if(session()->has('alert'))
    <div class="alert alert-primary text-center" role="alert">
        {{ session("alert") }}
    </div>
@endif

<div class="container">
    <form method="POST" action="{{ route('update.metadata.direction') }}"  >
        @csrf
        <div class="form-group mt-5">
            <label for="{{ config('implan.metakeys.budget') }}">Presupuesto Estimado</label>
            <?php $budget = $metadata->where("meta_llave", config("implan.metakeys.budget"))->first() ?> 
            <input type="text" name="{{ config('implan.metakeys.budget') }}" class="form-control" name="{{ config('implan.metakeys.budget') }}" value="{{  !empty($budget) ? $budget->valor : '' }}" placeholder="Presupuesto Estimado">
        </div>
        <div class="form-group mt-5">
            <label for="objective">Objetivos de la Dependencia</label>
            <?php $description = $metadata->where("meta_llave", config("implan.metakeys.description"))->first() ?> 
            <textarea name="{{ config('implan.metakeys.description') }}" class="form-control" id="objective" rows="4" placeholder="Breve descripción de los objetivos de la dependencia">{{  !empty($description) ? $description->valor : "" }}</textarea>
        </div>
        <div class="row justify-content-center mt-3">
            <button type="submit" class="btn btn-primary mr-2 mb-2"><i class="fas fa-save mr-2"></i> Guardar</button>
        </div>
    </form>
</div>
@endsection

@section('modals')
    
@endsection

@section('js')

@endsection