
CREATE TABLE public.programas_mir
(
    id serial NOT NULL,
    nombre character varying COLLATE pg_catalog."default",
    objetivo character varying COLLATE pg_catalog."default",
    objetivo_fin text COLLATE pg_catalog."default",
    objetivo_prop text COLLATE pg_catalog."default",
    updated_at timestamp without time zone,
    created_at timestamp without time zone,
    clave character varying(20) COLLATE pg_catalog."default",
    presupuesto numeric,
    CONSTRAINT programas_mir_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.programas_mir
    OWNER to postgres;

CREATE TABLE public.mir_areas
(
    id serial NOT NULL ,
    nombre character varying COLLATE pg_catalog."default",
    descripcion text COLLATE pg_catalog."default",
    direccion_id integer,
    periodo integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    programa_id integer,
    CONSTRAINT mir_areas_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE public.mir_areas
    OWNER to postgres;

CREATE TABLE public.mir_componentes
(
    id serial NOT NULL ,
    actividad text COLLATE pg_catalog."default",
    unidad_medida character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    meta_anual numeric(11,2) DEFAULT NULL::numeric,
    meta_anual_r numeric(11,2) DEFAULT 0.00,
    trimestre_1 numeric(11,2) DEFAULT NULL::numeric,
    trimestre_2 numeric(11,2) DEFAULT NULL::numeric,
    trimestre_3 numeric(11,2) DEFAULT NULL::numeric,
    trimestre_4 numeric(11,2) DEFAULT NULL::numeric,
    puesto_id integer NOT NULL,
    enero numeric(11,2) DEFAULT 0.00,
    febrero numeric(11,2) DEFAULT 0.00,
    marzo numeric(11,2) DEFAULT 0.00,
    trimestre_1_r numeric(11,2) DEFAULT 0.00,
    abril numeric(11,2) DEFAULT 0.00,
    mayo numeric(11,2) DEFAULT 0.00,
    junio numeric(11,2) DEFAULT 0.00,
    trimestre_2_r numeric(11,2) DEFAULT 0.00,
    julio numeric(11,2) DEFAULT 0.00,
    agosto numeric(11,2) DEFAULT 0.00,
    septiembre numeric(11,2) DEFAULT 0.00,
    trimestre_3_p numeric(11,2) DEFAULT 0.00,
    octubre numeric(11,2) DEFAULT 0.00,
    noviembre numeric(11,2) DEFAULT 0.00,
    diciembre numeric(11,2) DEFAULT 0.00,
    trimestre_4_r numeric(11,2) DEFAULT 0.00,
    beneficiarios numeric(11,0) DEFAULT 0,
    valor smallint DEFAULT '0'::smallint,
    tipo smallint DEFAULT '0'::smallint,
    orientacion smallint DEFAULT '0'::smallint,
    dimension character varying(30) COLLATE pg_catalog."default",
    formula text COLLATE pg_catalog."default",
    objetivo text COLLATE pg_catalog."default",
    periodo_verificacion character varying COLLATE pg_catalog."default",
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone,
    direction_id integer,
    periodo numeric,
    area_id integer,
    CONSTRAINT mir_componentes_pkey PRIMARY KEY (id),
    CONSTRAINT fk_mir_componentes_puestos1_idx FOREIGN KEY (puesto_id)
        REFERENCES public.puestos (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.mir_componentes
    OWNER to postgres;

COMMENT ON COLUMN public.mir_componentes.valor
    IS '0 = Absoluto, 1 = Relativo';

CREATE TABLE public.acciones_componente
(
    id serial NOT NULL ,
    actividad text COLLATE pg_catalog."default",
    unidad_medida character varying(255) COLLATE pg_catalog."default" DEFAULT NULL::character varying,
    meta_anual numeric(11,2) DEFAULT NULL::numeric,
    meta_anual_r numeric(11,2) DEFAULT 0.00,
    trimestre_1 numeric(11,2) DEFAULT NULL::numeric,
    trimestre_2 numeric(11,2) DEFAULT NULL::numeric,
    trimestre_3 numeric(11,2) DEFAULT NULL::numeric,
    trimestre_4 numeric(11,2) DEFAULT NULL::numeric,
    componente_id integer NOT NULL,
    puesto_id integer NOT NULL,
    enero numeric(11,2) DEFAULT 0.00,
    febrero numeric(11,2) DEFAULT 0.00,
    marzo numeric(11,2) DEFAULT 0.00,
    trimestre_1_r numeric(11,2) DEFAULT 0.00,
    abril numeric(11,2) DEFAULT 0.00,
    mayo numeric(11,2) DEFAULT 0.00,
    junio numeric(11,2) DEFAULT 0.00,
    trimestre_2_r numeric(11,2) DEFAULT 0.00,
    julio numeric(11,2) DEFAULT 0.00,
    agosto numeric(11,2) DEFAULT 0.00,
    septiembre numeric(11,2) DEFAULT 0.00,
    trimestre_3_p numeric(11,2) DEFAULT 0.00,
    octubre numeric(11,2) DEFAULT 0.00,
    noviembre numeric(11,2) DEFAULT 0.00,
    diciembre numeric(11,2) DEFAULT 0.00,
    trimestre_4_r numeric(11,2) DEFAULT 0.00,
    beneficiarios numeric(11,0) DEFAULT 0,
    valor smallint DEFAULT '0'::smallint,
    tipo smallint DEFAULT '0'::smallint,
    orientacion smallint DEFAULT '0'::smallint,
    dimension character varying(30) COLLATE pg_catalog."default",
    formula text COLLATE pg_catalog."default",
    objetivo text COLLATE pg_catalog."default",
    padre smallint,
    created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    updated_at timestamp without time zone,
    periodo_verificacion character varying COLLATE pg_catalog."default",
    CONSTRAINT acciones_componente_pkey PRIMARY KEY (id),
    CONSTRAINT fk_acciones_componente_mir_componentes_id FOREIGN KEY (componente_id)
        REFERENCES public.mir_componentes (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_acciones_componente_puestos1_idx FOREIGN KEY (puesto_id)
        REFERENCES public.puestos (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.acciones_componente
    OWNER to postgres;

COMMENT ON COLUMN public.acciones_componente.valor
    IS '0 = Absoluto, 1 = Relativo';

COMMENT ON COLUMN public.acciones_componente.tipo
    IS '0 = Numero Entero, 1 = Numero Decimal';

COMMENT ON COLUMN public.acciones_componente.orientacion
    IS '0 = positivo, 1= negativo, 2 = neutro';

CREATE TABLE public.age_meta_has_mir_componente
(
    age_metas_id integer,
    mir_componente_id integer,
    CONSTRAINT fk_age_metas_has_accion_componente_age_metas1_idx FOREIGN KEY (age_metas_id)
        REFERENCES public.age_metas (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_age_metas_has_mir_componente_acciones_poa1_idx FOREIGN KEY (mir_componente_id)
        REFERENCES public.mir_componentes (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.age_meta_has_mir_componente
    OWNER to postgres;

CREATE TABLE public.inf_indicador_has_mir_componente
(
    inf_indicadores_id integer,
    mir_componente_id integer,
    CONSTRAINT fk_inf_indicadores_has_accion_componente_inf_indicadores1_idx FOREIGN KEY (inf_indicadores_id)
        REFERENCES public.inf_indicadores (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_inf_indicadores_has_mir_componente_acciones_poa1_idx FOREIGN KEY (mir_componente_id)
        REFERENCES public.mir_componentes (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.inf_indicador_has_mir_componente
    OWNER to postgres;

CREATE TABLE public.pm_linea_accion_has_mir_componente
(
    pm_lineas_accion_id integer,
    mir_componente_id integer,
    CONSTRAINT fk_pm_lineas_accion_has_accion_componente_pm_lineas_accion1_idx FOREIGN KEY (pm_lineas_accion_id)
        REFERENCES public.pm_lineas_accion (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_pm_lineas_accion_has_mir_componente_acciones_poa1_idx FOREIGN KEY (mir_componente_id)
        REFERENCES public.mir_componentes (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.pm_linea_accion_has_mir_componente
    OWNER to postgres;

CREATE TABLE public.age_meta_has_accion_componente
(
    age_metas_id integer,
    accion_componente_id integer,
    CONSTRAINT fk_age_metas_has_accion_componente_acciones_poa1_idx FOREIGN KEY (accion_componente_id)
        REFERENCES public.acciones_componente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_age_metas_has_accion_componente_age_metas1_idx FOREIGN KEY (age_metas_id)
        REFERENCES public.age_metas (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.age_meta_has_accion_componente
    OWNER to postgres;

CREATE TABLE public.inf_indicador_has_accion_componente
(
    inf_indicadores_id integer,
    accion_componente_id integer,
    CONSTRAINT fk_inf_indicadores_has_accion_componente_acciones_poa1_idx FOREIGN KEY (accion_componente_id)
        REFERENCES public.acciones_componente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_inf_indicadores_has_accion_componente_inf_indicadores1_idx FOREIGN KEY (inf_indicadores_id)
        REFERENCES public.inf_indicadores (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.inf_indicador_has_accion_componente
    OWNER to postgres;

CREATE TABLE public.pm_linea_accion_has_accion_componente
(
    pm_lineas_accion_id integer,
    accion_componente_id integer,
    CONSTRAINT fk_pm_lineas_accion_has_accion_componente_acciones_poa1_idx FOREIGN KEY (accion_componente_id)
        REFERENCES public.acciones_componente (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_pm_lineas_accion_has_accion_componente_pm_lineas_accion1_idx FOREIGN KEY (pm_lineas_accion_id)
        REFERENCES public.pm_lineas_accion (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)

TABLESPACE pg_default;

ALTER TABLE public.pm_linea_accion_has_accion_componente
    OWNER to postgres;