-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-11-2018 a las 18:38:40
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `implan`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acciones_poa`
--

CREATE TABLE `acciones_poa` (
  `id` int(11) NOT NULL,
  `actividad` varchar(255) DEFAULT NULL,
  `unidad_medida` varchar(50) DEFAULT NULL,
  `meta_anual` int(11) DEFAULT NULL,
  `trimestre_1` int(11) DEFAULT NULL,
  `trimestre_2` int(11) DEFAULT NULL,
  `trimestre_3` int(11) DEFAULT NULL,
  `trimestre_4` int(11) DEFAULT NULL,
  `programas_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `age_metas`
--

CREATE TABLE `age_metas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `punto` varchar(20) DEFAULT NULL,
  `descripcion` text,
  `age_objetivo_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `age_metas`
--

INSERT INTO `age_metas` (`id`, `nombre`, `punto`, `descripcion`, `age_objetivo_id`, `created_at`, `updated_at`) VALUES
(1, 'Erradicar para todas las personas y en todo el mundo la pobreza extrema', '1.1', 'De aquí a 2030, erradicar para todas las personas y en todo el mundo la pobreza extrema (actualmente se considera que sufren pobreza extrema las personas que viven con menos de 1,25 dólares de los Estados Unidos al día)', 1, NULL, NULL),
(2, 'Reducir al menos a la mitad la proporción de hombres, mujeres y niños de todas las edades que viven en la pobreza en todas sus dimensiones con arreglo a las definiciones nacionales', '1.2', 'De aquí a 2030, reducir al menos a la mitad la proporción de hombres, mujeres y niños de todas las edades que viven en la pobreza en todas sus dimensiones con arreglo a las definiciones nacionales', 1, NULL, NULL),
(3, 'Implementar a nivel nacional sistemas y medidas apropiados de protección social para todos.', '1.3', 'Implementar a nivel nacional sistemas y medidas apropiados de protección social para todos, incluidos niveles mínimos, y, de aquí a 2030, lograr una amplia cobertura de las personas pobres y vulnerables', 1, NULL, NULL),
(4, 'Garantizar que todos los hombres y mujeres, en particular los pobres y los vulnerables, tengan los mismos derechos a los recursos económicos y acceso a los servicios básicos', '1.4', 'De aquí a 2030, garantizar que todos los hombres y mujeres, en particular los pobres y los vulnerables, tengan los mismos derechos a los recursos económicos y acceso a los servicios básicos, la propiedad y el control de la tierra y otros bienes, la herencia, los recursos naturales, las nuevas tecnologías apropiadas y los servicios financieros, incluida la microfinanciación', 1, NULL, NULL),
(5, 'Fomentar la resiliencia de los pobres y las personas que se encuentran en situaciones de vulnerabilidad y reducir su exposición y vulnerabilidad a los fenómenos extremos relacionados con el clima y otras perturbaciones y desastres económicos, sociales y a', '1.5', 'De aquí a 2030, fomentar la resiliencia de los pobres y las personas que se\r\nencuentran en situaciones de vulnerabilidad y reducir su exposición y vulnerabilidad\r\na los fenómenos extremos relacionados con el clima y otras perturbaciones y\r\ndesastres económicos, sociales y ambientales', 1, NULL, NULL),
(6, 'Garantizar una movilización significativa de recursos procedentes de diversas fuentes.', '1.A', 'Garantizar una movilización significativa de recursos procedentes de diversas fuentes, incluso mediante la mejora de la cooperación para el desarrollo, a fin de proporcionar medios suficientes y previsibles a los países en desarrollo, en particular los países menos adelantados, para que implementen programas y políticas encaminados a poner fin a la pobreza en todas sus dimensiones', 1, NULL, NULL),
(7, 'Crear marcos normativos sólidos en los planos nacional, regional e internacional, sobre la base de estrategias de desarrollo en favor de los pobres', '1.B', 'Crear marcos normativos sólidos en los planos nacional, regional e internacional, sobre la base de estrategias de desarrollo en favor de los pobres que tengan en cuenta las cuestiones de género, a fin de apoyar la inversión acelerada en medidas para erradicar la pobreza', 1, NULL, NULL),
(24, 'De aquí a 2030, poner fin al hambre y asegurar el acceso de todas las personas', '2.1 ', 'De aquí a 2030, poner fin al hambre y asegurar el acceso de todas las personas,\r\nen particular los pobres y las personas en situaciones de vulnerabilidad, incluidos\r\nlos niños menores de 1 año, a una alimentación sana, nutritiva y suficiente\r\ndurante todo el año\r\n', 2, NULL, NULL),
(25, 'De aquí a 2030, poner fin a todas las formas de malnutrición, incluso logrando, a más tardar en 2025', '2.2 ', 'De aquí a 2030, poner fin a todas las formas de malnutrición, incluso logrando,\r\na más tardar en 2025, las metas convenidas internacionalmente sobre el retraso\r\ndel crecimiento y la emaciación de los niños menores de 5 años, y abordar\r\nlas necesidades de nutrición de las adolescentes, las mujeres embarazadas y\r\nlactantes y las personas de edad\r\n', 2, NULL, NULL),
(26, 'De aquí a 2030, duplicar la productividad agrícola y los ingresos de los productores\r\nde alimentos en pequeña escala', '2.3 ', 'De aquí a 2030, duplicar la productividad agrícola y los ingresos de los productores\r\nde alimentos en pequeña escala, en particular las mujeres, los pueblos indígenas,\r\nlos agricultores familiares, los ganaderos y los pescadores, entre otras cosas\r\nmediante un acceso seguro y equitativo a las tierras, a otros recursos e insumos\r\nde producción y a los conocimientos, los servicios fnancieros, los mercados y\r\nlas oportunidades para añadir valor y obtener empleos no agrícolas', 2, NULL, NULL),
(27, 'De aquí a 2030, asegurar la sostenibilidad de los sistemas de producción de\r\nalimentos y aplicar prácticas agrícolas resilientes', '2.4 ', 'De aquí a 2030, asegurar la sostenibilidad de los sistemas de producción de\r\nalimentos y aplicar prácticas agrícolas resilientes que aumenten la productividad\r\ny la producción, contribuyan al mantenimiento de los ecosistemas, fortalezcan\r\nla capacidad de adaptación al cambio climático, los fenómenos meteorológicos\r\nextremos, las sequías, las inundaciones y otros desastres, y mejoren progresivamente\r\nla calidad de la tierra y el suelo\r\n', 2, NULL, NULL),
(28, 'De aquí a 2020, mantener la diversidad genética de las semillas, las plantas\r\ncultivadas y los animales de granja y domesticados', '2.5 ', 'De aquí a 2020, mantener la diversidad genética de las semillas, las plantas\r\ncultivadas y los animales de granja y domesticados y sus correspondientes\r\nespecies silvestres, entre otras cosas mediante una buena gestión y diversifcación\r\nde los bancos de semillas y plantas a nivel nacional, regional e internacional,\r\ny promover el acceso a los benefcios que se deriven de la utilización de los\r\nrecursos genéticos y los conocimientos tradicionales conexos y su distribución\r\njusta y equitativa, según lo convenido internacionalmente\r\n', 2, NULL, NULL),
(29, 'Aumentar, incluso mediante una mayor cooperación internacional, las inversiones\r\nen infraestructura rural', '2.a ', 'Aumentar, incluso mediante una mayor cooperación internacional, las inversiones\r\nen infraestructura rural, investigación y servicios de extensión agrícola, desarrollo\r\ntecnológico y bancos de genes de plantas y ganado a fn de mejorar la capacidad\r\nde producción agropecuaria en los países en desarrollo, particularmente en los\r\npaíses menos adelantados\r\n', 2, NULL, NULL),
(30, 'Corregir y prevenir las restricciones y distorsiones comercia', '2.b ', 'Corregir y prevenir las restricciones y distorsiones comerciales en los mercados\r\nagropecuarios mundiales, incluso mediante la eliminación paralela de todas\r\nlas formas de subvención a las exportaciones agrícolas y todas las medidas\r\nde exportación con efectos equivalentes, de conformidad con el mandato de la\r\nRonda de Doha para el Desarrollo\r\n', 2, NULL, NULL),
(31, 'Adoptar medidas para asegurar el buen funcionamiento de los mercados de\r\nproductos básicos alimentarios y sus derivados', '2.c ', 'Adoptar medidas para asegurar el buen funcionamiento de los mercados de\r\nproductos básicos alimentarios y sus derivados y facilitar el acceso oportuno\r\na la información sobre los mercados, incluso sobre las reservas de alimentos,\r\na fn de ayudar a limitar la extrema volatilidad de los precios de los alimentos', 2, NULL, NULL),
(32, 'De aquí a 2030, reducir la tasa mundial de mortalidad materna a menos de\r\n70 por cada 100.000 nacidos vivos', '3.1 ', 'De aquí a 2030, reducir la tasa mundial de mortalidad materna a menos de\r\n70 por cada 100.000 nacidos vivos\r\n', 3, NULL, NULL),
(33, 'De aquí a 2030, poner fn a las muertes evitables de recién nacidos y de niños\r\nmenores de 5 años', '3.2 ', 'De aquí a 2030, poner fn a las muertes evitables de recién nacidos y de niños\r\nmenores de 5 años, logrando que todos los países intenten reducir la mortalidad\r\nneonatal al menos a 12 por cada 1.000 nacidos vivos y la mortalidad de los\r\nniños menores de 5 años al menos a 25 por cada 1.000 nacidos vivos\r\n', 3, NULL, NULL),
(34, 'De aquí a 2030, poner fn a las epidemias del SIDA, la tuberculosis', '3.3 ', 'De aquí a 2030, poner fn a las epidemias del SIDA, la tuberculosis, la malaria y las\r\nenfermedades tropicales desatendidas y combatir la hepatitis, las enfermedades\r\ntransmitidas por el agua y otras enfermedades transmisibles\r\n', 3, NULL, NULL),
(35, 'De aquí a 2030, reducir en un tercio la mortalidad prematura por enfermedades\r\nno transmisibles', '3.4 ', 'De aquí a 2030, reducir en un tercio la mortalidad prematura por enfermedades\r\nno transmisibles mediante su prevención y tratamiento, y promover la salud\r\nmental y el bienestar\r\n', 3, NULL, NULL),
(36, 'Fortalecer la prevención y el tratamiento del abuso de sustancias adictivas', '3.5 ', 'Fortalecer la prevención y el tratamiento del abuso de sustancias adictivas,\r\nincluido el uso indebido de estupefacientes y el consumo nocivo de alcohol\r\n', 3, NULL, NULL),
(37, 'De aquí a 2020, reducir a la mitad el número de muertes y lesiones causadas\r\npor accidentes de tráfco en el mundo', '3.6 ', 'De aquí a 2020, reducir a la mitad el número de muertes y lesiones causadas\r\npor accidentes de tráfco en el mundo\r\n', 3, NULL, NULL),
(38, 'De aquí a 2030, garantizar el acceso universal a los servicios de salud\r\nsexual y reproductiva', '3.7 ', 'De aquí a 2030, garantizar el acceso universal a los servicios de salud\r\nsexual y reproductiva, incluidos los de planificación familiar, información\r\ny educación, y la integración de la salud reproductiva en las estrategias y\r\nlos programas nacionales', 3, NULL, NULL),
(39, 'Lograr la cobertura sanitaria universal, incluida la protección contra los riesgos\r\nfnancieros', '3.8 ', 'Lograr la cobertura sanitaria universal, incluida la protección contra los riesgos\r\nfnancieros, el acceso a servicios de salud esenciales de calidad y el acceso a\r\nmedicamentos y vacunas inocuos, efcaces, asequibles y de calidad para todos\r\n', 3, NULL, NULL),
(40, 'De aquí a 2030, reducir considerablemente el número de muertes', '3.9 ', 'De aquí a 2030, reducir considerablemente el número de muertes y enfermedades\r\ncausadas por productos químicos peligrosos y por la polución y contaminación\r\ndel aire, el agua y el suelo\r\n', 3, NULL, NULL),
(41, 'Fortalecer la aplicación del Convenio Marco de la Organización Mundial', '3.a ', 'Fortalecer la aplicación del Convenio Marco de la Organización Mundial de la\r\nSalud para el Control del Tabaco en todos los países, según proceda\r\n', 3, NULL, NULL),
(42, 'Apoyar las actividades de investigación y desarrollo de vacunas y medicamentos\r\ncontra las enfermedades transmisibles', '3.b ', 'Apoyar las actividades de investigación y desarrollo de vacunas y medicamentos\r\ncontra las enfermedades transmisibles y no transmisibles que afectan\r\nprimordialmente a los países en desarrollo y facilitar el acceso a medicamentos\r\ny vacunas esenciales asequibles de conformidad con la Declaración relativa\r\nal Acuerdo sobre los Aspectos de los Derechos de Propiedad Intelectual\r\nRelacionados con el Comercio y la Salud Pública, en la que se afrma el derecho\r\nde los países en desarrollo a utilizar al máximo las disposiciones del Acuerdo\r\nsobre los Aspectos de los Derechos de Propiedad Intelectual Relacionados\r\ncon el Comercio respecto a la fexibilidad para proteger la salud pública y, en\r\nparticular, proporcionar acceso a los medicamentos para todos\r\n', 3, NULL, NULL),
(43, 'Aumentar considerablemente la financiación de la salud y la contratación, el\r\nperfeccionamiento,', '3.c ', 'Aumentar considerablemente la fnanciación de la salud y la contratación, el\r\nperfeccionamiento, la capacitación y la retención del personal sanitario en los\r\npaíses en desarrollo, especialmente en los países menos adelantados y los\r\npequeños Estados insulares en desarrollo\r\n', 3, NULL, NULL),
(44, 'Reforzar la capacidad de todos los países, en particular los países en desarrollo', '3.d ', 'Reforzar la capacidad de todos los países, en particular los países en desarrollo,\r\nen materia de alerta temprana, reducción de riesgos y gestión de los riesgos\r\npara la salud nacional y mundial', 3, NULL, NULL),
(45, 'De aquí a 2030, asegurar que todas las niñas y todos los niños terminen la\r\nenseñanza primaria y secundaria', '4.1 ', 'De aquí a 2030, asegurar que todas las niñas y todos los niños terminen la\r\nenseñanza primaria y secundaria, que ha de ser gratuita, equitativa y de calidad\r\ny producir resultados de aprendizaje pertinentes y efectivos\r\n', 4, NULL, NULL),
(46, 'De aquí a 2030, asegurar que todas las niñas y todos los niños tengan acceso a\r\nservicios de atención', '4.2 ', 'De aquí a 2030, asegurar que todas las niñas y todos los niños tengan acceso a\r\nservicios de atención y desarrollo en la primera infancia y educación preescolar\r\nde calidad, a fn de que estén preparados para la enseñanza primaria\r\n', 4, NULL, NULL),
(47, 'De aquí a 2030, asegurar el acceso igualitario de todos los hombres y las\r\nmujeres a una formación técnica', '4.3 ', 'De aquí a 2030, asegurar el acceso igualitario de todos los hombres y las\r\nmujeres a una formación técnica, profesional y superior de calidad, incluida la enseñanza universitaria\r\n', 4, NULL, NULL),
(48, 'De aquí a 2030, aumentar considerablemente el número de jóvenes y adultos que tienen las competencias necesarias', '4.4 ', 'De aquí a 2030, aumentar considerablemente el número de jóvenes y adultos\r\nque tienen las competencias necesarias, en particular técnicas y profesionales,\r\npara acceder al empleo, el trabajo decente y el emprendimiento\r\n', 4, NULL, NULL),
(49, 'De aquí a 2030, eliminar las disparidades de género en la educación y asegurar el\r\nacceso igualitario a todos los niveles', '4.5 ', 'De aquí a 2030, eliminar las disparidades de género en la educación y asegurar el\r\nacceso igualitario a todos los niveles de la enseñanza y la formación profesional\r\npara las personas vulnerables, incluidas las personas con discapacidad, los\r\npueblos indígenas y los niños en situaciones de vulnerabilidad\r\n', 4, NULL, NULL),
(50, 'De aquí a 2030, asegurar que todos los jóvenes y una proporción considerable\r\nde los adultos, tengan\r\nnociones elementales de aritmética', '4.6 ', 'De aquí a 2030, asegurar que todos los jóvenes y una proporción considerable\r\nde los adultos, tanto hombres como mujeres, estén alfabetizados y tengan\r\nnociones elementales de aritmética\r\n', 4, NULL, NULL),
(51, 'De aquí a 2030, asegurar que todos los alumnos adquieran los conocimientos\r\nteóricos y prácticos necesarios para promover el desarrollo sostenible', '4.7', 'De aquí a 2030, asegurar que todos los alumnos adquieran los conocimientos\r\nteóricos y prácticos necesarios para promover el desarrollo sostenible, entre otras cosas mediante la educación para el desarrollo sostenible y los estilos de\r\nvida sostenibles, los derechos humanos, la igualdad de género, la promoción\r\nde una cultura de paz y no violencia, la ciudadanía mundial y la valoración de\r\nla diversidad cultural y la contribución de la cultura al desarrollo sostenible\r\n', 4, NULL, NULL),
(52, 'Construir y adecuar instalaciones educativas que tengan en cuenta las necesidades\r\nde los niños', '4.a ', 'Construir y adecuar instalaciones educativas que tengan en cuenta las necesidades\r\nde los niños y las personas con discapacidad y las diferencias de género, y que\r\nofrezcan entornos de aprendizaje seguros, no violentos, inclusivos y efcaces\r\npara todos\r\n', 4, NULL, NULL),
(53, 'De aquí a 2020, aumentar considerablemente a nivel mundial el número de\r\nbecas disponibles para los países en desarrollo', '4.b ', 'De aquí a 2020, aumentar considerablemente a nivel mundial el número de\r\nbecas disponibles para los países en desarrollo, en particular los países menos\r\nadelantados, los pequeños Estados insulares en desarrollo y los países africanos,\r\na fn de que sus estudiantes puedan matricularse en programas de enseñanza\r\nsuperior, incluidos programas de formación profesional y programas técnicos,\r\ncientífcos, de ingeniería y de tecnología de la información y las comunicaciones,\r\nde países desarrollados y otros países en desarrollo\r\n', 4, NULL, NULL),
(54, 'De aquí a 2030, aumentar considerablemente la oferta de docentes califcados', '4.c ', 'De aquí a 2030, aumentar considerablemente la oferta de docentes califcados,\r\nincluso mediante la cooperación internacional para la formación de docentes\r\nen los países en desarrollo, especialmente los países menos adelantados y los\r\npequeños Estados insulares en desarrollo', 4, NULL, NULL),
(55, 'Poner fn a todas las formas de discriminación', '5.1 ', 'Poner fn a todas las formas de discriminación contra todas las mujeres y las\r\nniñas en todo el mundo\r\n', 5, NULL, NULL),
(56, 'Eliminar todas las formas de violencia contra todas las mujeres y las niñas', '5.2 ', 'Eliminar todas las formas de violencia contra todas las mujeres y las niñas en\r\nlos ámbitos público y privado, incluidas la trata y la explotación sexual y otros\r\ntipos de explotación\r\n', 5, NULL, NULL),
(57, 'Eliminar todas las prácticas nocivas, como el matrimonio infantil', '5.3 ', 'Eliminar todas las prácticas nocivas, como el matrimonio infantil, precoz y\r\nforzado y la mutilación genital femenina\r\n', 5, NULL, NULL),
(58, 'Reconocer y valorar los cuidados y el trabajo doméstico no remunerados\r\nmediante servicios públicos, ', '5.4 ', 'Reconocer y valorar los cuidados y el trabajo doméstico no remunerados\r\nmediante servicios públicos, infraestructuras y políticas de protección social,\r\ny promoviendo la responsabilidad compartida en el hogar y la familia, según\r\nproceda en cada país\r\n', 5, NULL, NULL),
(59, 'Asegurar la participación plena y efectiva de las mujeres', '5.5 ', 'Asegurar la participación plena y efectiva de las mujeres y la igualdad de\r\noportunidades de liderazgo a todos los niveles decisorios en la vida política,\r\neconómica y pública\r\n', 5, NULL, NULL),
(60, 'Asegurar el acceso universal a la salud sexual y reproductiva y los derechos\r\nreproductivos', '5.6 ', 'Asegurar el acceso universal a la salud sexual y reproductiva y los derechos\r\nreproductivos según lo acordado de conformidad con el Programa de Acción\r\nde la Conferencia Internacional sobre la Población y el Desarrollo, la Plataforma\r\nde Acción de Beijing y los documentos fnales de sus conferencias de examen', 5, NULL, NULL),
(61, 'Emprender reformas que otorguen a las mujeres igualdad de derechos a los\r\nrecursos económicos', '5.a ', 'Emprender reformas que otorguen a las mujeres igualdad de derechos a los\r\nrecursos económicos, así como acceso a la propiedad y al control de la tierra\r\ny otros tipos de bienes, los servicios fnancieros, la herencia y los recursos\r\nnaturales, de conformidad con las leyes nacionales\r\n', 5, NULL, NULL),
(62, 'Mejorar el uso de la tecnología instrumental, en particular la tecnología de\r\nla información ', '5.b ', 'Mejorar el uso de la tecnología instrumental, en particular la tecnología de\r\nla información y las comunicaciones, para promover el empoderamiento de\r\nlas mujeres\r\n', 5, NULL, NULL),
(63, 'Aprobar y fortalecer políticas acertadas y leyes aplicables para promover la\r\nigualdad de género', '5.c ', 'Aprobar y fortalecer políticas acertadas y leyes aplicables para promover la\r\nigualdad de género y el empoderamiento de todas las mujeres y las niñas a\r\ntodos los niveles', 5, NULL, NULL),
(72, 'Acceso universal y equitativo al agua potable', '6.1 ', 'De aquí a 2030, lograr el acceso universal y equitativo al agua potable a un\r\nprecio asequible para todos\r\n', 6, NULL, NULL),
(73, 'Acceso a servicios de saneamiento e higiene adecuados', '6.2', 'De aquí a 2030, lograr el acceso a servicios de saneamiento e higiene adecuados\r\ny equitativos para todos y poner fin a la defecación al aire libre, prestando\r\nespecial atención a las necesidades de las mujeres y las niñas y las personas\r\nen situaciones de vulnerabilidad\r\n', 6, NULL, NULL),
(74, 'Mejorar la calidad del agua reduciendo la contaminación', '6.3 ', 'De aquí a 2030, mejorar la calidad del agua reduciendo la contaminación,\r\neliminando el vertimiento y minimizando la emisión de productos químicos y\r\nmateriales peligrosos, reduciendo a la mitad el porcentaje de aguas residuales\r\nsin tratar y aumentando considerablemente el reciclado y la reutilización sin\r\nriesgos a nivel mundial\r\n', 6, NULL, NULL),
(75, 'Aumentar considerablemente el uso eficiente de los recursos\r\nhídricos', '6.4 ', 'De aquí a 2030, aumentar considerablemente el uso eficiente de los recursos\r\nhídricos en todos los sectores y asegurar la sostenibilidad de la extracción y el\r\nabastecimiento de agua dulce para hacer frente a la escasez de agua y reducir\r\nconsiderablemente el número de personas que sufren falta de agua\r\n', 6, NULL, NULL),
(76, 'Implementar la gestión integrada de los recursos hídricos', '6.5 ', 'De aquí a 2030, implementar la gestión integrada de los recursos hídricos a\r\ntodos los niveles, incluso mediante la cooperación transfronteriza, según proceda\r\n', 6, NULL, NULL),
(77, 'Proteger y restablecer los ecosistemas relacionados con\r\nel agua', '6.6 ', 'De aquí a 2020, proteger y restablecer los ecosistemas relacionados con\r\nel agua, incluidos los bosques, las montañas, los humedales, los ríos, los\r\nacuíferos y los lagos', 6, NULL, NULL),
(78, 'Ampliar la cooperación internacional y el apoyo prestado a los\r\npaíses en desarrollo para la creación de capacidad en actividades y programas\r\nrelativos al agua y el saneamiento', '6.a ', 'De aquí a 2030, ampliar la cooperación internacional y el apoyo prestado a los\r\npaíses en desarrollo para la creación de capacidad en actividades y programas\r\nrelativos al agua y el saneamiento, como los de captación de agua, desalinización,\r\nuso eficiente de los recursos hídricos, tratamiento de aguas residuales, reciclado\r\ny tecnologías de reutilización\r\n', 6, NULL, NULL),
(79, 'Apoyar y fortalecer la gestión del agua y el saneamiento', '6.b ', 'Apoyar y fortalecer la participación de las comunidades locales en la mejora de\r\nla gestión del agua y el saneamiento', 6, NULL, NULL),
(80, 'Garantizar el acceso universal a servicios energéticos asequibles', '7.1 ', 'De aquí a 2030, garantizar el acceso universal a servicios energéticos asequibles,\r\nfiables y modernos\r\n', 7, NULL, NULL),
(81, 'Aumentar considerablemente la proporción de energía renovable', '7.2 ', 'De aquí a 2030, aumentar considerablemente la proporción de energía renovable\r\nen el conjunto de fuentes energéticas\r\n', 7, NULL, NULL),
(82, 'Duplicar la eficiencia energética', '7.3 ', 'De aquí a 2030, duplicar la tasa mundial de mejora de la eficiencia energética\r\n', 7, NULL, NULL),
(83, 'Aumentar la cooperación internacional para facilitar el acceso\r\na la investigación y la tecnología relativas a la energía limpia', '7.a ', 'De aquí a 2030, aumentar la cooperación internacional para facilitar el acceso\r\na la investigación y la tecnología relativas a la energía limpia, incluidas las\r\nfuentes renovables, la eficiencia energética y las tecnologías avanzadas y menos\r\ncontaminantes de combustibles fósiles, y promover la inversión en infraestructura\r\nenergética y tecnologías limpias\r\n', 7, NULL, NULL),
(84, 'Ampliar la infraestructura y mejorar la tecnología para prestar\r\nservicios energéticos modernos y sostenibles', '7.b ', 'De aquí a 2030, ampliar la infraestructura y mejorar la tecnología para prestar\r\nservicios energéticos modernos y sostenibles para todos en los países en\r\ndesarrollo, en particular los países menos adelantados, los pequeños Estados\r\ninsulares en desarrollo y los países en desarrollo sin litoral, en consonancia con\r\nsus respectivos programas de apoyo', 7, NULL, NULL),
(85, 'Mantener el crecimiento económico per capita', '8.1 ', 'Mantener el crecimiento económico per capita de conformidad con las\r\ncircunstancias nacionales y, en particular, un crecimiento del producto interno\r\nbruto de al menos el 7% anual en los países menos adelantados\r\n', 8, NULL, NULL),
(86, 'Lograr niveles más elevados de productividad económica', '8.2 ', 'Lograr niveles más elevados de productividad económica mediante la diversificación,\r\nla modernización tecnológica y la innovación, entre otras cosas centrándose en\r\nlos sectores con gran valor añadido y un uso intensivo de la mano de obra\r\n', 8, NULL, NULL),
(87, 'Promover políticas orientadas al desarrollo que apoyen las actividades\r\nproductivas', '8.3 ', 'Promover políticas orientadas al desarrollo que apoyen las actividades\r\nproductivas, la creación de puestos de trabajo decentes, el emprendimiento,\r\nla creatividad y la innovación, y fomentar la formalización y el crecimiento de\r\nlas microempresas y las pequeñas y medianas empresas, incluso mediante\r\nel acceso a servicios financieros\r\n', 8, NULL, NULL),
(88, 'Mejorar progresivamente la producción y el consumo eficientes\r\nde los recursos mundiales', '8.4 ', 'Mejorar progresivamente, de aquí a 2030, la producción y el consumo eficientes\r\nde los recursos mundiales y procurar desvincular el crecimiento económico de\r\nla degradación del medio ambiente, conforme al Marco Decenal de Programas\r\nsobre Modalidades de Consumo y Producción Sostenibles, empezando por los\r\npaíses desarrollados\r\n', 8, NULL, NULL),
(89, 'Lograr el empleo pleno y productivo y el trabajo decente para\r\ntodas las mujeres y los hombres', '8.5 ', 'De aquí a 2030, lograr el empleo pleno y productivo y el trabajo decente para\r\ntodas las mujeres y los hombres, incluidos los jóvenes y las personas con\r\ndiscapacidad, así como la igualdad de remuneración por trabajo de igual valor\r\n', 8, NULL, NULL),
(90, 'Reducir considerablemente la proporción de jóvenes que no están empleados', '8.6 ', 'De aquí a 2020, reducir considerablemente la proporción de jóvenes que no están empleados y no cursan estudios ni reciben capacitación\r\n', 8, NULL, NULL),
(91, 'Adoptar medidas inmediatas y eficaces para erradicar el trabajo forzoso', '8.7 ', 'Adoptar medidas inmediatas y eficaces para erradicar el trabajo forzoso, poner\r\nfin a las formas contemporáneas de esclavitud y la trata de personas y asegurar\r\nla prohibición y eliminación de las peores formas de trabajo infantil, incluidos\r\nel reclutamiento y la utilización de niños soldados, y, de aquí a 2025, poner fin\r\nal trabajo infantil en todas sus formas\r\n', 8, NULL, NULL),
(92, NULL, NULL, '8.8 Proteger los derechos laborales y promover un entorno de trabajo seguro y sin\r\nriesgos para todos los trabajadores, incluidos los trabajadores migrantes, en\r\nparticular las mujeres migrantes y las personas con empleos precarios\r\n', 8, NULL, NULL),
(93, 'Elaborar y poner en práctica políticas encaminadas a promover\r\nun turismo sostenible', '8.9 ', 'De aquí a 2030, elaborar y poner en práctica políticas encaminadas a promover\r\nun turismo sostenible que cree puestos de trabajo y promueva la cultura y los\r\nproductos locales\r\n', 8, NULL, NULL),
(94, 'Fortalecer la capacidad de las instituciones financieras nacionales', '8.10 ', 'Fortalecer la capacidad de las instituciones financieras nacionales para\r\nfomentar y ampliar el acceso a los servicios bancarios, financieros y de\r\nseguros para todos\r\n', 8, NULL, NULL),
(95, 'Aumentar el apoyo a la iniciativa de ayuda para el comercio en los países en\r\ndesarrollo', '8.a ', 'Aumentar el apoyo a la iniciativa de ayuda para el comercio en los países en\r\ndesarrollo, en particular los países menos adelantados, incluso mediante el Marco\r\nIntegrado Mejorado para la Asistencia Técnica a los Países Menos Adelantados\r\nen Materia de Comercio\r\n', 8, NULL, NULL),
(96, 'Desarrollar y poner en marcha una estrategia mundial para el\r\nempleo de los jóvenes', '8.b ', 'De aquí a 2020, desarrollar y poner en marcha una estrategia mundial para el\r\nempleo de los jóvenes y aplicar el Pacto Mundial para el Empleo de la Organización\r\nInternacional del Trabajo', 8, NULL, NULL),
(97, 'Desarrollar infraestructuras fiables, sostenibles, resilientes y de calidad, incluidas\r\ninfraestructuras regionales y transfronterizas', '9.1 ', 'Desarrollar infraestructuras fiables, sostenibles, resilientes y de calidad, incluidas\r\ninfraestructuras regionales y transfronterizas, para apoyar el desarrollo económico\r\ny el bienestar humano, haciendo especial hincapié en el acceso asequible y\r\nequitativo para todos\r\n', 9, NULL, NULL),
(98, 'Promover una industrialización inclusiva y sostenible', '9.2 ', 'Promover una industrialización inclusiva y sostenible y, de aquí a 2030, aumentar\r\nsignificativamente la contribución de la industria al empleo y al producto interno\r\nbruto, de acuerdo con las circunstancias nacionales, y duplicar esa contribución\r\nen los países menos adelantados\r\n', 9, NULL, NULL),
(99, 'Aumentar el acceso de las pequeñas industrias y otras empresas, particularmente\r\nen los países en desarrollo', '9.3 ', 'Aumentar el acceso de las pequeñas industrias y otras empresas, particularmente\r\nen los países en desarrollo, a los servicios financieros, incluidos créditos\r\nasequibles, y su integración en las cadenas de valor y los mercados\r\n', 9, NULL, NULL),
(100, 'Modernizar la infraestructura y reconvertir las industrias para\r\nque sean sostenibles', '9.4 ', 'De aquí a 2030, modernizar la infraestructura y reconvertir las industrias para\r\nque sean sostenibles, utilizando los recursos con mayor eficacia y promoviendo\r\nla adopción de tecnologías y procesos industriales limpios y ambientalmente\r\nracionales, y logrando que todos los países tomen medidas de acuerdo con sus\r\ncapacidades respectivas\r\n', 9, NULL, NULL),
(101, 'Aumentar la investigación científica y mejorar la capacidad tecnológica de los\r\nsectores industriales de todos los países', '9.5 ', 'Aumentar la investigación científica y mejorar la capacidad tecnológica de los\r\nsectores industriales de todos los países, en particular los países en desarrollo, entre otras cosas fomentando la innovación y aumentando considerablemente,\r\nde aquí a 2030, el número de personas que trabajan en investigación y desarrollo\r\npor millón de habitantes y los gastos de los sectores público y privado en\r\ninvestigación y desarrollo\r\n', 9, NULL, NULL),
(102, 'Facilitar el desarrollo de infraestructuras sostenibles y resilientes en los países\r\nen desarrollo', '9.a ', 'Facilitar el desarrollo de infraestructuras sostenibles y resilientes en los países\r\nen desarrollo mediante un mayor apoyo financiero, tecnológico y técnico a los\r\npaíses africanos, los países menos adelantados, los países en desarrollo sin\r\nlitoral y los pequeños Estados insulares en desarrollo\r\n', 9, NULL, NULL),
(103, 'Apoyar el desarrollo de tecnologías, la investigación y la innovación nacionales\r\nen los países en desarrollo', '9.b ', 'Apoyar el desarrollo de tecnologías, la investigación y la innovación nacionales\r\nen los países en desarrollo, incluso garantizando un entorno normativo propicio\r\na la diversificación industrial y la adición de valor a los productos básicos, entre otras cosas\r\n', 9, NULL, NULL),
(104, 'Aumentar significativamente el acceso a la tecnología de la información y las\r\ncomunicaciones', '9.c ', 'Aumentar significativamente el acceso a la tecnología de la información y las\r\ncomunicaciones y esforzarse por proporcionar acceso universal y asequible a\r\nInternet en los países menos adelantados de aquí a 2020', 9, NULL, NULL),
(105, 'Lograr progresivamente y mantener el crecimiento de los ingresos', '10.1 ', 'De aquí a 2030, lograr progresivamente y mantener el crecimiento de los ingresos\r\ndel 40% más pobre de la población a una tasa superior a la media nacional\r\n', 10, NULL, NULL),
(106, 'Potenciar y promover la inclusión social, económica y política\r\nde todas las personas', '10.2 ', 'De aquí a 2030, potenciar y promover la inclusión social, económica y política\r\nde todas las personas, independientemente de su edad, sexo, discapacidad,\r\nraza, etnia, origen, religión o situación económica u otra condición\r\n', 10, NULL, NULL),
(107, 'Garantizar la igualdad de oportunidades y reducir la desigualdad de resultados', '10.3 ', 'Garantizar la igualdad de oportunidades y reducir la desigualdad de resultados,\r\nincluso eliminando las leyes, políticas y prácticas discriminatorias y promoviendo\r\nlegislaciones, políticas y medidas adecuadas a ese respecto\r\n', 10, NULL, NULL),
(108, 'Adoptar políticas, especialmente fiscales, salariales y de protección social', '10.4 ', 'Adoptar políticas, especialmente fiscales, salariales y de protección social, y\r\nlograr progresivamente una mayor igualdad\r\n', 10, NULL, NULL),
(109, 'Mejorar la reglamentación y vigilancia', '10.5 ', 'Mejorar la reglamentación y vigilancia de las instituciones y los mercados\r\nfinancieros mundiales y fortalecer la aplicación de esos reglamentos\r\n', 10, NULL, NULL),
(110, 'Asegurar una mayor representación e intervención de los países en desarrollo\r\nen las decisiones adoptadas por las instituciones económicas y financieras', '10.6 ', 'Asegurar una mayor representación e intervención de los países en desarrollo\r\nen las decisiones adoptadas por las instituciones económicas y financieras\r\ninternacionales para aumentar la eficacia, fiabilidad, rendición de cuentas y\r\nlegitimidad de esas instituciones\r\n', 10, NULL, NULL),
(111, 'Facilitar la migración y la movilidad ordenadas, seguras, regulares y responsables de las personas', '10.7 ', 'Facilitar la migración y la movilidad ordenadas, seguras, regulares y responsables\r\nde las personas, incluso mediante la aplicación de políticas migratorias planificadas\r\ny bien gestionadas\r\n', 10, NULL, NULL),
(112, 'Aplicar el principio del trato especial y diferenciado para los países en desarrollo', '10.a ', 'Aplicar el principio del trato especial y diferenciado para los países en desarrollo,\r\nen particular los países menos adelantados, de conformidad con los acuerdos\r\nde la Organización Mundial del Comercio\r\n', 10, NULL, NULL),
(113, 'Fomentar la asistencia oficial para el desarrollo y las corrientes financieras,\r\nincluida la inversión extranjera directa', '10.b ', 'Fomentar la asistencia oficial para el desarrollo y las corrientes financieras,\r\nincluida la inversión extranjera directa, para los Estados con mayores necesidades,\r\nen particular los países menos adelantados, los países africanos, los pequeños\r\nEstados insulares en desarrollo y los países en desarrollo sin litoral, en consonancia\r\ncon sus planes y programas nacionales\r\n', 10, NULL, NULL),
(114, 'Reducir a menos del 3% los costos de transacción de las\r\nremesas de los migrantes', '10.c ', 'De aquí a 2030, reducir a menos del 3% los costos de transacción de las\r\nremesas de los migrantes y eliminar los corredores de remesas con un costo\r\nsuperior al 5%', 10, NULL, NULL),
(115, 'Asegurar el acceso de todas las personas a viviendas y servicios\r\nbásicos adecuados', '11.1 ', 'De aquí a 2030, asegurar el acceso de todas las personas a viviendas y servicios\r\nbásicos adecuados, seguros y asequibles y mejorar los barrios marginales\r\n', 11, NULL, NULL),
(116, 'Proporcionar acceso a sistemas de transporte seguros,\r\nasequibles, accesibles y sostenibles', '11.2 ', 'De aquí a 2030, proporcionar acceso a sistemas de transporte seguros,\r\nasequibles, accesibles y sostenibles para todos y mejorar la seguridad vial, en\r\nparticular mediante la ampliación del transporte público, prestando especial\r\natención a las necesidades de las personas en situación de vulnerabilidad, las\r\nmujeres, los niños, las personas con discapacidad y las personas de edad\r\n', 11, NULL, NULL),
(117, 'Aumentar la urbanización inclusiva y sostenible y la capacidad\r\npara la planificación y la gestión participativas', '11.3 ', 'De aquí a 2030, aumentar la urbanización inclusiva y sostenible y la capacidad\r\npara la planificación y la gestión participativas, integradas y sostenibles de los asentamientos humanos en todos los países\r\n', 11, NULL, NULL),
(118, 'Redoblar los esfuerzos para proteger y salvaguardar el patrimonio cultural', '11.4 ', 'Redoblar los esfuerzos para proteger y salvaguardar el patrimonio cultural y\r\nnatural del mundo\r\n', 11, NULL, NULL),
(119, 'Reducir significativamente el número de muertes causadas por\r\nlos desastres', '11.5 ', 'De aquí a 2030, reducir significativamente el número de muertes causadas por\r\nlos desastres, incluidos los relacionados con el agua, y de personas afectadas por\r\nellos, y reducir considerablemente las pérdidas económicas directas provocadas\r\npor los desastres en comparación con el producto interno bruto mundial, haciendo\r\nespecial hincapié en la protección de los pobres y las personas en situaciones\r\nde vulnerabilidad\r\n', 11, NULL, NULL),
(120, 'Reducir el impacto ambiental negativo per capita de las ciudades', '11.6 ', 'De aquí a 2030, reducir el impacto ambiental negativo per capita de las ciudades,\r\nincluso prestando especial atención a la calidad del aire y la gestión de los\r\ndesechos municipales y de otro tipo\r\n', 11, NULL, NULL),
(121, 'Proporcionar acceso universal a zonas verdes y espacios\r\npúblicos seguros', '11.7 ', 'De aquí a 2030, proporcionar acceso universal a zonas verdes y espacios\r\npúblicos seguros, inclusivos y accesibles, en particular para las mujeres y los\r\nniños, las personas de edad y las personas con discapacidad\r\n', 11, NULL, NULL),
(122, 'Apoyar los vínculos económicos, sociales y ambientales positivos entre las\r\nzonas urbanas', '11.a ', 'Apoyar los vínculos económicos, sociales y ambientales positivos entre las\r\nzonas urbanas, periurbanas y rurales fortaleciendo la planificación del desarrollo\r\nnacional y regional\r\n', 11, NULL, NULL),
(123, 'Aumentar considerablemente el número de ciudades y asentamientos\r\nhumanos', '11.b ', 'De aquí a 2020, aumentar considerablemente el número de ciudades y asentamientos\r\nhumanos que adoptan e implementan políticas y planes integrados para promover\r\nla inclusión, el uso eficiente de los recursos, la mitigación del cambio climático\r\ny la adaptación a él y la resiliencia ante los desastres, y desarrollar y poner en\r\npráctica, en consonancia con el Marco de Sendai para la Reducción del Riesgo\r\nde Desastres 2015-2030, la gestión integral de los riesgos de desastre a todos\r\nlos niveles\r\n', 11, NULL, NULL),
(124, 'Proporcionar apoyo a los países menos adelantados', '11.c ', 'Proporcionar apoyo a los países menos adelantados, incluso mediante asistencia\r\nfinanciera y técnica, para que puedan construir edificios sostenibles y resilientes\r\nutilizando materiales locales', 11, NULL, NULL),
(125, 'Aplicar el Marco Decenal de Programas sobre Modalidades de Consumo y\r\nProducción Sostenibles', '12.1 ', 'Aplicar el Marco Decenal de Programas sobre Modalidades de Consumo y\r\nProducción Sostenibles, con la participación de todos los países y bajo el liderazgo de los países desarrollados, teniendo en cuenta el grado de desarrollo\r\ny las capacidades de los países en desarrollo\r\n', 12, NULL, NULL),
(126, 'Lograr la gestión sostenible', '12.2 ', 'De aquí a 2030, lograr la gestión sostenible y el uso eficiente de los recursos\r\nnaturales\r\n', 12, NULL, NULL),
(127, 'Reducir a la mitad el desperdicio de alimentos per capita mundial', '12.3 ', 'De aquí a 2030, reducir a la mitad el desperdicio de alimentos per capita mundial\r\nen la venta al por menor y a nivel de los consumidores y reducir las pérdidas\r\nde alimentos en las cadenas de producción y suministro, incluidas las pérdidas\r\nposteriores a la cosecha\r\n', 12, NULL, NULL),
(128, 'Lograr la gestión ecológicamente racional de los productos\r\nquímicos y de todos los desechos a lo largo de su ciclo de vida', '12.4 ', 'De aquí a 2020, lograr la gestión ecológicamente racional de los productos\r\nquímicos y de todos los desechos a lo largo de su ciclo de vida, de conformidad\r\ncon los marcos internacionales convenidos, y reducir significativamente su\r\nliberación a la atmósfera, el agua y el suelo a fin de minimizar sus efectos\r\nadversos en la salud humana y el medio ambiente\r\n', 12, NULL, NULL),
(129, 'Reducir considerablemente la generación de desechos', '12.5 ', 'De aquí a 2030, reducir considerablemente la generación de desechos mediante\r\nactividades de prevención, reducción, reciclado y reutilización\r\n', 12, NULL, NULL),
(130, 'Alentar a las empresas, en especial las grandes empresas y las empresas\r\ntransnacionales', '12.6 ', 'Alentar a las empresas, en especial las grandes empresas y las empresas\r\ntransnacionales, a que adopten prácticas sostenibles e incorporen información\r\nsobre la sostenibilidad en su ciclo de presentación de informes\r\n', 12, NULL, NULL),
(131, 'Promover prácticas de adquisición pública que sean sostenibles', '12.7 ', 'Promover prácticas de adquisición pública que sean sostenibles, de conformidad\r\ncon las políticas y prioridades nacionales\r\n', 12, NULL, NULL),
(132, 'Asegurar que las personas de todo el mundo tengan la información\r\ny los conocimientos pertinentes', '12.8 ', 'De aquí a 2030, asegurar que las personas de todo el mundo tengan la información\r\ny los conocimientos pertinentes para el desarrollo sostenible y los estilos de\r\nvida en armonía con la naturaleza\r\n', 12, NULL, NULL),
(133, 'Ayudar a los países en desarrollo a fortalecer su capacidad científica y tecnológica', '12.a ', 'Ayudar a los países en desarrollo a fortalecer su capacidad científica y tecnológica\r\npara avanzar hacia modalidades de consumo y producción más sostenibles\r\n', 12, NULL, NULL),
(134, 'Elaborar y aplicar instrumentos para vigilar los efectos en el desarrollo sostenible', '12.b ', 'Elaborar y aplicar instrumentos para vigilar los efectos en el desarrollo sostenible,\r\na fin de lograr un turismo sostenible que cree puestos de trabajo y promueva\r\nla cultura y los productos locales\r\n', 12, NULL, NULL),
(135, 'Racionalizar los subsidios ineficientes a los combustibles fósiles que fomentan\r\nel consumo antieconómico', '12.c ', 'Racionalizar los subsidios ineficientes a los combustibles fósiles que fomentan\r\nel consumo antieconómico eliminando las distorsiones del mercado, de acuerdo\r\ncon las circunstancias nacionales, incluso mediante la reestructuración de los\r\nsistemas tributarios y la eliminación gradual de los subsidios perjudiciales,\r\ncuando existan, para reflejar su impacto ambiental, teniendo plenamente en\r\ncuenta las necesidades y condiciones específicas de los países en desarrollo\r\ny minimizando los posibles efectos adversos en su desarrollo, de manera que\r\nse proteja a los pobres y a las comunidades afectadas', 12, NULL, NULL),
(136, 'Fortalecer la resiliencia y la capacidad de adaptación a los riesgos relacionados\r\ncon el clima', '13.1 ', 'Fortalecer la resiliencia y la capacidad de adaptación a los riesgos relacionados\r\ncon el clima y los desastres naturales en todos los países\r\n', 13, NULL, NULL),
(137, 'Incorporar medidas relativas al cambio climático', '13.2 ', 'Incorporar medidas relativas al cambio climático en las políticas, estrategias y\r\nplanes nacionales\r\n', 13, NULL, NULL),
(138, 'Mejorar la educación, la sensibilización y la capacidad humana e institucional', '13.3 ', 'Mejorar la educación, la sensibilización y la capacidad humana e institucional\r\nrespecto de la mitigación del cambio climático, la adaptación a él, la reducción\r\nde sus efectos y la alerta temprana\r\n', 13, NULL, NULL),
(139, 'Cumplir el compromiso de los países desarrollados que son partes en la\r\nConvención Marco de las Naciones Unidas sobre el Cambio Climático', '13.a ', 'Cumplir el compromiso de los países desarrollados que son partes en la\r\nConvención Marco de las Naciones Unidas sobre el Cambio Climático de lograr\r\npara el año 2020 el objetivo de movilizar conjuntamente 100.000 millones de\r\ndólares anuales procedentes de todas las fuentes a fin de atender las necesidades\r\nde los países en desarrollo respecto de la adopción de medidas concretas de\r\nmitigación y la transparencia de su aplicación, y poner en pleno funcionamiento\r\nel Fondo Verde para el Clima capitalizándolo lo antes posible\r\n', 13, NULL, NULL),
(140, 'Promover mecanismos para aumentar la capacidad para la planificación y gestión\r\neficaces en relación con el cambio climático', '13.b ', 'Promover mecanismos para aumentar la capacidad para la planificación y gestión\r\neficaces en relación con el cambio climático en los países menos adelantados\r\ny los pequeños Estados insulares en desarrollo, haciendo particular hincapié\r\nen las mujeres, los jóvenes y las comunidades locales y marginadas', 13, NULL, NULL),
(141, 'Prevenir y reducir significativamente la contaminación marina\r\nde todo tipo', '14.1 ', 'De aquí a 2025, prevenir y reducir significativamente la contaminación marina\r\nde todo tipo, en particular la producida por actividades realizadas en tierra,\r\nincluidos los detritos marinos y la polución por nutrientes\r\n', 14, NULL, NULL),
(142, 'Gestionar y proteger sosteniblemente los ecosistemas marinos\r\ny costeros', '14.2 ', 'De aquí a 2020, gestionar y proteger sosteniblemente los ecosistemas marinos\r\ny costeros para evitar efectos adversos importantes, incluso fortaleciendo su\r\nresiliencia, y adoptar medidas para restaurarlos a fin de restablecer la salud y\r\nla productividad de los océanos\r\n', 14, NULL, NULL),
(143, 'Minimizar y abordar los efectos de la acidificación de los océanos', '14.3 ', 'Minimizar y abordar los efectos de la acidificación de los océanos, incluso\r\nmediante una mayor cooperación científica a todos los niveles\r\n', 14, NULL, NULL),
(144, 'Reglamentar eficazmente la explotación pesquera y poner\r\nfin a la pesca excesiva', '14.4', 'De aquí a 2020, reglamentar eficazmente la explotación pesquera y poner\r\nfin a la pesca excesiva, la pesca ilegal, no declarada y no reglamentada y las\r\nprácticas pesqueras destructivas, y aplicar planes de gestión con fundamento\r\ncientífico a fin de restablecer las poblaciones de peces en el plazo más breve\r\nposible, al menos alcanzando niveles que puedan producir el máximo rendimiento\r\nsostenible de acuerdo con sus características biológicas\r\n', 14, NULL, NULL),
(145, 'Conservar al menos el 10% de las zonas costeras y marinas', '14.5 ', 'De aquí a 2020, conservar al menos el 10% de las zonas costeras y marinas,\r\nde conformidad con las leyes nacionales y el derecho internacional y sobre la\r\nbase de la mejor información científica disponible\r\n', 14, NULL, NULL),
(146, 'Prohibir ciertas formas de subvenciones a la pesca que\r\ncontribuyen a la sobrecapacidad y la pesca excesiva', '14.6 ', 'De aquí a 2020, prohibir ciertas formas de subvenciones a la pesca que\r\ncontribuyen a la sobrecapacidad y la pesca excesiva, eliminar las subvenciones\r\nque contribuyen a la pesca ilegal, no declarada y no reglamentada y abstenerse\r\nde introducir nuevas subvenciones de esa índole, reconociendo que la negociación\r\nsobre las subvenciones a la pesca en el marco de la Organización Mundial del\r\nComercio debe incluir un trato especial y diferenciado, apropiado y efectivo para\r\nlos países en desarrollo y los países menos adelantados\r\n', 14, NULL, NULL),
(147, 'Aumentar los beneficios económicos que los pequeños Estados\r\ninsulares en desarrollo y los países menos adelantados', '14.7 ', 'De aquí a 2030, aumentar los beneficios económicos que los pequeños Estados\r\ninsulares en desarrollo y los países menos adelantados obtienen del uso sostenible\r\nde los recursos marinos, en particular mediante la gestión sostenible de la\r\npesca, la acuicultura y el turismo\r\n', 14, NULL, NULL),
(148, 'Aumentar los conocimientos científicos, desarrollar la capacidad de investigación\r\ny transferir tecnología marina', '14.a ', 'Aumentar los conocimientos científicos, desarrollar la capacidad de investigación\r\ny transferir tecnología marina, teniendo en cuenta los Criterios y Directrices\r\npara la Transferencia de Tecnología Marina de la Comisión Oceanográfica\r\nIntergubernamental, a fin de mejorar la salud de los océanos y potenciar\r\nla contribución de la biodiversidad marina al desarrollo de los países en\r\ndesarrollo, en particular los pequeños Estados insulares en desarrollo y los\r\npaíses menos adelantados\r\n', 14, NULL, NULL),
(149, 'Facilitar el acceso a los recursos marinos', '14.b ', 'Facilitar el acceso de los pescadores artesanales a los recursos marinos y\r\nlos mercados\r\n', 14, NULL, NULL),
(150, 'Mejorar la conservación y el uso sostenible de los océanos y sus recursos', '14.c ', 'Mejorar la conservación y el uso sostenible de los océanos y sus recursos\r\naplicando el derecho internacional reflejado en la Convención de las Naciones\r\nUnidas sobre el Derecho del Mar, que constituye el marco jurídico para la\r\nconservación y la utilización sostenible de los océanos y sus recursos, como\r\nse recuerda en el párrafo 158 del documento “El futuro que queremos”', 14, NULL, NULL),
(151, 'Asegurar la conservación, el restablecimiento y el uso sostenible\r\nde los ecosistemas terrestres y los ecosistemas interiores de agua dulce', '15.1 ', 'De aquí a 2020, asegurar la conservación, el restablecimiento y el uso sostenible\r\nde los ecosistemas terrestres y los ecosistemas interiores de agua dulce y sus\r\nservicios, en particular los bosques, los humedales, las montañas y las zonas\r\náridas, en consonancia con las obligaciones contraídas en virtud de acuerdos\r\ninternacionales\r\n', 15, NULL, NULL);
INSERT INTO `age_metas` (`id`, `nombre`, `punto`, `descripcion`, `age_objetivo_id`, `created_at`, `updated_at`) VALUES
(152, 'Promover la puesta en práctica de la gestión sostenible de\r\ntodos los tipos de bosques', '15.2 ', 'De aquí a 2020, promover la puesta en práctica de la gestión sostenible de\r\ntodos los tipos de bosques, detener la deforestación, recuperar los bosques\r\ndegradados y aumentar considerablemente la forestación y la reforestación a\r\nnivel mundial\r\n', 15, NULL, NULL),
(153, 'Luchar contra la desertificación, rehabilitar las tierras y los\r\nsuelos degradados', '15.3 ', 'De aquí a 2030, luchar contra la desertificación, rehabilitar las tierras y los\r\nsuelos degradados, incluidas las tierras afectadas por la desertificación, la\r\nsequía y las inundaciones, y procurar lograr un mundo con efecto neutro en la\r\ndegradación del suelo\r\n', 15, NULL, NULL),
(154, 'Conservación de los ecosistemas montañosos,\r\nincluida su diversidad biológica', '15.4 ', 'De aquí a 2030, asegurar la conservación de los ecosistemas montañosos,\r\nincluida su diversidad biológica, a fin de mejorar su capacidad de proporcionar\r\nbeneficios esenciales para el desarrollo sostenible\r\n', 15, NULL, NULL),
(155, 'Adoptar medidas urgentes y significativas para reducir la degradación de los\r\nhábitats naturales', '15.5 ', 'Adoptar medidas urgentes y significativas para reducir la degradación de los\r\nhábitats naturales, detener la pérdida de biodiversidad y, de aquí a 2020, proteger\r\nlas especies amenazadas y evitar su extinción\r\n', 15, NULL, NULL),
(156, 'Promover la participación justa y equitativa en los beneficios derivados de la\r\nutilización de los recursos genéticos', '15.6 ', 'Promover la participación justa y equitativa en los beneficios derivados de la utilización de los recursos genéticos y promover el acceso adecuado a esos\r\nrecursos, según lo convenido internacionalmente\r\n', 15, NULL, NULL),
(157, 'Adoptar medidas urgentes para poner fin a la caza furtiva y el tráfico de especies\r\nprotegidas de flora y fauna', '15.7 ', 'Adoptar medidas urgentes para poner fin a la caza furtiva y el tráfico de especies\r\nprotegidas de flora y fauna y abordar tanto la demanda como la oferta de\r\nproductos ilegales de flora y fauna silvestres\r\n', 15, NULL, NULL),
(158, 'Adoptar medidas para prevenir la introducción de especies\r\nexóticas invasoras', '15.8 ', 'De aquí a 2020, adoptar medidas para prevenir la introducción de especies\r\nexóticas invasoras y reducir significativamente sus efectos en los ecosistemas\r\nterrestres y acuáticos y controlar o erradicar las especies prioritarias\r\n', 15, NULL, NULL),
(159, 'Integrar los valores de los ecosistemas y la biodiversidad en\r\nla planificación, los procesos de desarrollo', '15.9 ', 'De aquí a 2020, integrar los valores de los ecosistemas y la biodiversidad en\r\nla planificación, los procesos de desarrollo, las estrategias de reducción de la\r\npobreza y la contabilidad nacionales y locales\r\n', 15, NULL, NULL),
(160, 'Movilizar y aumentar significativamente los recursos financieros', '15.a', 'Movilizar y aumentar significativamente los recursos financieros procedentes de\r\ntodas las fuentes para conservar y utilizar de forma sostenible la biodiversidad\r\ny los ecosistemas\r\n', 15, NULL, NULL),
(161, 'Movilizar recursos considerables de todas las fuentes y a todos los niveles para\r\nfinanciar la gestión forestal sostenible', '15.b ', 'Movilizar recursos considerables de todas las fuentes y a todos los niveles para\r\nfinanciar la gestión forestal sostenible y proporcionar incentivos adecuados a\r\nlos países en desarrollo para que promuevan dicha gestión, en particular con\r\nmiras a la conservación y la reforestación\r\n', 15, NULL, NULL),
(162, 'Aumentar el apoyo mundial a la lucha contra la caza furtiva y el tráfico de\r\nespecies protegidas', '15.c ', 'Aumentar el apoyo mundial a la lucha contra la caza furtiva y el tráfico de\r\nespecies protegidas, incluso aumentando la capacidad de las comunidades\r\nlocales para perseguir oportunidades de subsistencia sostenibles', 15, NULL, NULL),
(175, 'Reducir significativamente todas las formas de violencia', '16.1 ', 'Reducir significativamente todas las formas de violencia y las correspondientes\r\ntasas de mortalidad en todo el mundo\r\n', 16, NULL, NULL),
(176, 'Poner fin al maltrato contra los niños', '16.2 ', 'Poner fin al maltrato, la explotación, la trata y todas las formas de violencia y\r\ntortura contra los niños\r\n', 16, NULL, NULL),
(177, 'Promover el estado de derecho en los planos nacional e internacional', '16.3 ', 'Promover el estado de derecho en los planos nacional e internacional y garantizar\r\nla igualdad de acceso a la justicia para todos\r\n', 16, NULL, NULL),
(178, 'Reducir significativamente las corrientes financieras y de armas\r\nilícitas', '16.4 ', 'De aquí a 2030, reducir significativamente las corrientes financieras y de armas\r\nilícitas, fortalecer la recuperación y devolución de los activos robados y luchar\r\ncontra todas las formas de delincuencia organizada\r\n', 16, NULL, NULL),
(179, 'Reducir considerablemente la corrupción', '16.5 ', 'Reducir considerablemente la corrupción y el soborno en todas sus formas\r\n', 16, NULL, NULL),
(180, 'Crear a todos los niveles instituciones eficaces y transparentes', '16.6 ', 'Crear a todos los niveles instituciones eficaces y transparentes que\r\nrindan cuentas\r\n', 16, NULL, NULL),
(181, 'Garantizar la adopción en todos los niveles de decisiones inclusivas, participativas\r\ny representativas', '16.7 ', 'Garantizar la adopción en todos los niveles de decisiones inclusivas, participativas\r\ny representativas que respondan a las necesidades\r\n', 16, NULL, NULL),
(182, 'Ampliar y fortalecer la participación de los países en desarrollo', '16.8 ', 'Ampliar y fortalecer la participación de los países en desarrollo en las instituciones\r\nde gobernanza mundial\r\n', 16, NULL, NULL),
(183, 'Proporcionar acceso a una identidad jurídica para todos', '16.9 ', 'De aquí a 2030, proporcionar acceso a una identidad jurídica para todos, en\r\nparticular mediante el registro de nacimientos\r\n', 16, NULL, NULL),
(184, 'Garantizar el acceso público a la información y proteger las libertades fundamentales', '16.10 ', 'Garantizar el acceso público a la información y proteger las libertades fundamentales,\r\nde conformidad con las leyes nacionales y los acuerdos internacionales\r\n', 16, NULL, NULL),
(185, 'Fortalecer las instituciones nacionales pertinentes', '16.a ', 'Fortalecer las instituciones nacionales pertinentes, incluso mediante la\r\ncooperación internacional, para crear a todos los niveles, particularmente en\r\nlos países en desarrollo, la capacidad de prevenir la violencia y combatir el\r\nterrorismo y la delincuencia\r\n', 16, NULL, NULL),
(186, 'Promover y aplicar leyes y políticas no discriminatorias', '16.b ', 'Promover y aplicar leyes y políticas no discriminatorias en favor del\r\ndesarrollo sostenible', 16, NULL, NULL),
(206, 'Fortalecer la movilización de recursos internos', '17.1 ', 'Fortalecer la movilización de recursos internos, incluso mediante la prestación de\r\napoyo internacional a los países en desarrollo, con el fin de mejorar la capacidad\r\nnacional para recaudar ingresos fiscales y de otra índole\r\n', 17, NULL, NULL),
(207, 'Velar por que los países desarrollados cumplan plenamente sus compromisos\r\nen relación con la asistencia oficial para el desarrollo', '17.2 ', 'Velar por que los países desarrollados cumplan plenamente sus compromisos\r\nen relación con la asistencia oficial para el desarrollo, incluido el compromiso de\r\nnumerosos países desarrollados de alcanzar el objetivo de destinar el 0,7% del\r\ningreso nacional bruto a la asistencia oficial para el desarrollo de los países en\r\ndesarrollo y entre el 0,15% y el 0,20% del ingreso nacional bruto a la asistencia\r\noficial para el desarrollo de los países menos adelantados; se alienta a los\r\nproveedores de asistencia oficial para el desarrollo a que consideren la posibilidad\r\nde fijar una meta para destinar al menos el 0,20% del ingreso nacional bruto\r\na la asistencia oficial para el desarrollo de los países menos adelantados\r\n', 17, NULL, NULL),
(208, 'Movilizar recursos financieros', '17.3', 'Movilizar recursos financieros adicionales de múltiples fuentes para los países\r\nen desarrollo\r\n', 17, NULL, NULL),
(209, 'Ayudar a los países en desarrollo a lograr la sostenibilidad de la deuda', '17.4 ', 'Ayudar a los países en desarrollo a lograr la sostenibilidad de la deuda a\r\nlargo plazo con políticas coordinadas orientadas a fomentar la financiación,\r\nel alivio y la reestructuración de la deuda, según proceda, y hacer frente a\r\nla deuda externa de los países pobres muy endeudados a fin de reducir el\r\nendeudamiento excesivo\r\n', 17, NULL, NULL),
(210, 'Adoptar y aplicar sistemas de promoción de las inversiones', '17.5 ', 'Adoptar y aplicar sistemas de promoción de las inversiones en favor de los\r\npaíses menos adelantados\r\n\r\n', 17, NULL, NULL),
(211, 'Mejorar la cooperación regional e internacional Norte-Sur, Sur-Sur y triangular\r\nen materia de ciencia, tecnología e innovación', '17.6 ', 'Mejorar la cooperación regional e internacional Norte-Sur, Sur-Sur y triangular\r\nen materia de ciencia, tecnología e innovación y el acceso a estas, y aumentar el\r\nintercambio de conocimientos en condiciones mutuamente convenidas, incluso\r\nmejorando la coordinación entre los mecanismos existentes, en particular a nivel\r\nde las Naciones Unidas, y mediante un mecanismo mundial de facilitación de\r\nla tecnología\r\n', 17, NULL, NULL),
(212, 'Promover el desarrollo de tecnologías ecológicamente racionales y su transferencia', '17.7 ', 'Promover el desarrollo de tecnologías ecológicamente racionales y su transferencia,\r\ndivulgación y difusión a los países en desarrollo en condiciones favorables,\r\nincluso en condiciones concesionarias y preferenciales, según lo convenido de\r\nmutuo acuerdo\r\n', 17, NULL, NULL),
(213, 'Poner en pleno funcionamiento el banco de tecnología y el mecanismo de apoyo a la creación de capacidad en materia de ciencia', '17.8 ', 'Poner en pleno funcionamiento, a más tardar en 2017, el banco de tecnología\r\ny el mecanismo de apoyo a la creación de capacidad en materia de ciencia,\r\ntecnología e innovación para los países menos adelantados y aumentar la\r\nutilización de tecnologías instrumentales, en particular la tecnología de la\r\ninformación y las comunicaciones\r\nCreación de capacidad\r\n', 17, NULL, NULL),
(214, 'Aumentar el apoyo internacional para realizar actividades de creación de\r\ncapacidad eficaces y específicas', '17.9 ', 'Aumentar el apoyo internacional para realizar actividades de creación de\r\ncapacidad eficaces y específicas en los países en desarrollo a fin de respaldar\r\nlos planes nacionales de implementación de todos los Objetivos de Desarrollo\r\nSostenible, incluso mediante la cooperación Norte-Sur, Sur-Sur y triangular\r\nComercio\r\n', 17, NULL, NULL),
(215, 'Promover un sistema de comercio multilateral universal', '17.10', 'Promover un sistema de comercio multilateral universal, basado en normas,\r\nabierto, no discriminatorio y equitativo en el marco de la Organización Mundial\r\ndel Comercio, incluso mediante la conclusión de las negociaciones en el marco\r\ndel Programa de Doha para el Desarrollo\r\n', 17, NULL, NULL),
(216, 'Aumentar significativamente las exportaciones de los países en desarrollo', '17.11 ', 'Aumentar significativamente las exportaciones de los países en desarrollo, en particular con miras a duplicar la participación de los países menos adelantados\r\nen las exportaciones mundiales de aquí a 2020\r\n', 17, NULL, NULL),
(217, 'Lograr la consecución oportuna del acceso a los mercados libre de derechos\r\ny contingentes de manera duradera', '17.12 ', 'Lograr la consecución oportuna del acceso a los mercados libre de derechos\r\ny contingentes de manera duradera para todos los países menos adelantados,\r\nconforme a las decisiones de la Organización Mundial del Comercio, incluso\r\nvelando por que las normas de origen preferenciales aplicables a las importaciones\r\nde los países menos adelantados sean transparentes y sencillas y contribuyan\r\na facilitar el acceso a los mercados\r\nCuestiones sistémicas\r\nCoherencia normativa e institucional\r\n', 17, NULL, NULL),
(218, 'Aumentar la estabilidad macroeconómica mundial', '17.13 ', 'Aumentar la estabilidad macroeconómica mundial, incluso mediante la coordinación\r\ny coherencia de las políticas\r\n', 17, NULL, NULL),
(219, 'Mejorar la coherencia de las políticas para el desarrollo sostenible\r\n', '17.14', 'Mejorar la coherencia de las políticas para el desarrollo sostenible\r\n', 17, NULL, NULL),
(220, 'Respetar el margen normativo y el liderazgo de cada país', '17.15 ', 'Respetar el margen normativo y el liderazgo de cada país para establecer y\r\naplicar políticas de erradicación de la pobreza y desarrollo sostenible\r\nAlianzas entre múltiples interesados\r\n', 17, NULL, NULL),
(221, 'Mejorar la Alianza Mundial para el Desarrollo Sostenible', '17.16 ', 'Mejorar la Alianza Mundial para el Desarrollo Sostenible, complementada por\r\nalianzas entre múltiples interesados que movilicen e intercambien conocimientos,\r\nespecialización, tecnología y recursos financieros, a fin de apoyar el logro de\r\nlos Objetivos de Desarrollo Sostenible en todos los países, particularmente los\r\npaíses en desarrollo\r\n', 17, NULL, NULL),
(222, 'Fomentar y promover la constitución de alianzas eficaces en las esferas\r\npública', '17.17 ', 'Fomentar y promover la constitución de alianzas eficaces en las esferas\r\npública, público-privada y de la sociedad civil, aprovechando la experiencia y\r\nlas estrategias de obtención de recursos de las alianzas\r\nDatos, supervisión y rendición de cuentas\r\n', 17, NULL, NULL),
(223, 'Mejorar el apoyo a la creación de capacidad prestado a los países\r\nen desarrollo', '17.18 ', 'De aquí a 2020, mejorar el apoyo a la creación de capacidad prestado a los países\r\nen desarrollo, incluidos los países menos adelantados y los pequeños Estados\r\ninsulares en desarrollo, para aumentar significativamente la disponibilidad de\r\ndatos oportunos, fiables y de gran calidad desglosados por ingresos, sexo, edad,\r\nraza, origen étnico, estatus migratorio, discapacidad, ubicación geográfica y\r\notras características pertinentes en los contextos nacionales\r\n', 17, NULL, NULL),
(224, 'Aprovechar las iniciativas existentes para elaborar indicadores que\r\npermitan medir los progresos', '17.19 ', 'De aquí a 2030, aprovechar las iniciativas existentes para elaborar indicadores que\r\npermitan medir los progresos en materia de desarrollo sostenible y complementen\r\nel producto interno bruto, y apoyar la creación de capacidad estadística en los\r\npaíses en desarrollo', 17, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `age_metas_rechazadas`
--

CREATE TABLE `age_metas_rechazadas` (
  `id` int(11) NOT NULL,
  `descripcion` text,
  `original_nombre` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `age_meta_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `age_meta_has_acciones_poa`
--

CREATE TABLE `age_meta_has_acciones_poa` (
  `age_metas_id` int(11) NOT NULL,
  `acciones_poa_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `age_meta_has_direccion`
--

CREATE TABLE `age_meta_has_direccion` (
  `age_metas_id` int(11) NOT NULL,
  `direcciones_id` int(11) NOT NULL,
  `puestos_id` int(11) DEFAULT NULL,
  `estatus` tinyint(4) DEFAULT NULL COMMENT '0 = Sin Asignar\n\n1 = Acptada\n\n2 = Rechazada'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `age_objetivos`
--

CREATE TABLE `age_objetivos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `punto` varchar(20) DEFAULT NULL,
  `descripcion` text,
  `normativa_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `age_objetivos`
--

INSERT INTO `age_objetivos` (`id`, `nombre`, `punto`, `descripcion`, `normativa_id`, `created_at`, `updated_at`) VALUES
(1, 'Fin de la Pobreza', '1', 'Poner fin a la pobreza en todas sus formas en todo el mundo', 3, NULL, NULL),
(2, 'Hambre Cero', '2', 'Poner fin al hambre, lograr la seguridad alimentaria y la mejora de la nutrición y promover la agricultura sostenible', 3, NULL, NULL),
(3, 'Salud y Bienestar', '3', 'Garantizar una vida sana y promover el bienestar de todos a todas las edades', 3, NULL, NULL),
(4, 'Educación de Calidad', '4', 'Garantizar una educación inclusiva y equitativa de calidad y promover oportunidades de aprendizaje permanente para todos', 3, NULL, NULL),
(5, 'Igualdad de Género', '5', 'Lograr la igualdad de género y empoderar a todas las mujeres y las niñas', 3, NULL, NULL),
(6, 'Agua Limpia y Saneamiento', '6', 'Garantizar la disponibilidad y la gestión sostenible del agua y el saneamiento para todos', 3, NULL, NULL),
(7, 'Energía Asequible y no Contaminante', '7', 'Garantizar el acceso a una energía asequible, fiable, sostenible y moderna para todos', 3, NULL, NULL),
(8, 'Trabajo Decente y Crecimiento Económico', '8', 'Promover el crecimiento económico sostenido, inclusivo y sostenible, el empleo pleno y productivo y el trabajo decente para todos', 3, NULL, NULL),
(9, 'Industria, Innovación e Infraestructura', '9', 'Construir infraestructuras resilientes, promover la industrialización inclusiva y sostenible y fomentar la innovación', 3, NULL, NULL),
(10, 'Reducción de las Desigualdades', '10', 'Reducir la desigualdad en los países y entre ellos', 3, NULL, NULL),
(11, 'Ciudades y Comunidades Sostenibles', '11', 'Lograr que las ciudades y los asentamientos humanos sean inclusivos, seguros, resilientes y sostenibles', 3, NULL, NULL),
(12, 'Producción y Consumo Responsable', '12', 'Garantizar modalidades de consumo y producción sostenibles', 3, NULL, NULL),
(13, 'Acción por el Clima', '13', 'Adoptar medidas urgentes para combatir el cambio climático y sus efectos', 3, NULL, NULL),
(14, 'Vida Submarina', '14', 'Conservar y utilizar sosteniblemente los océanos, los mares y los recursos marinos para el desarrollo sostenible', 3, NULL, NULL),
(15, 'Vida de Ecosistemas Terrestres', '15', 'Proteger, restablecer y promover el uso sostenible de los ecosistemas terrestres, gestionar sosteniblemente los bosques, luchar contra la desertificación, detener e invertir la degradación de las tierras y detener la pérdida de biodiversidad', 3, NULL, NULL),
(16, 'Paz, Justicia e Instituciones Sólidas', '16', 'Promover sociedades pacíficas e inclusivas para el desarrollo sostenible, facilitar el acceso a la justicia para todos y construir a todos los niveles instituciones eficaces e inclusivas que rindan cuentas', 3, NULL, NULL),
(17, 'Alianzas para Lograr los Objetivos', '17', 'Fortalecer los medios de implementación y revitalizar la Alianza Mundial para el Desarrollo Sostenible', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE `bitacora` (
  `id` int(11) NOT NULL,
  `action` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `users_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones`
--

CREATE TABLE `direcciones` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `abreviacion` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `direcciones`
--

INSERT INTO `direcciones` (`id`, `nombre`, `abreviacion`, `created_at`, `updated_at`) VALUES
(1, 'Instituto Municipal de Planeación de Bahía de Banderas', 'IMPLAN', '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(2, 'Sistema de Justicia Administrativa Municipal', 'Sistema de Justicia Administrativa Municipal', NULL, NULL),
(3, 'Despacho de Presidencia', 'Despacho de Presidencia', NULL, NULL),
(4, 'Unidad Municipal de Protección Civil', 'Unidad Municipal de Protección Civil', NULL, NULL),
(5, 'Secretaría del Ayuntamiento', 'Secretaría del Ayuntamiento', NULL, NULL),
(6, 'Tesorería', 'Tesorería', NULL, NULL),
(7, 'Contraloría', 'Contraloría', NULL, NULL),
(8, 'Oficialía Mayor Administrativa', 'Oficialía Mayor Administrativa', NULL, NULL),
(9, 'Padrón y licencias', 'Padrón y licencias', NULL, NULL),
(10, 'Seguridad Pública y Transito', 'Seguridad Pública y Transito', NULL, NULL),
(11, 'Ordenamiento Territorial Desarrollo Urbano y Medio Ambiente', 'Ordenamiento Territorial Desarrollo Urbano y Medio', NULL, NULL),
(12, 'Servicios Públicos', 'Servicios Públicos', NULL, NULL),
(13, 'Obras Públicas', 'Obras Públicas', NULL, NULL),
(14, 'Turismo y Desarrollo Económico', 'Turismo y Desarrollo Económico', NULL, NULL),
(15, 'Desarrollo Y Bienestar Social', 'Desarrollo Y Bienestar Social', NULL, NULL),
(16, 'Dirección Jurídica', 'Dirección Jurídica', NULL, NULL),
(17, 'Sistema Municipal DIF', 'Sistema Municipal DIF', NULL, NULL),
(18, 'Comisión Municipal de Derechos Humanos', 'CMDH', NULL, NULL),
(19, 'Organismo Operador Municipal de Agua Potable Alcantarillado y Saneamiento', 'OROMAPAS', NULL, NULL),
(20, 'Coordinación de Gabinete', 'Coordinación de Gabinete', NULL, NULL),
(21, 'Comunicación Estratégica y  RP', 'Comunicación Estratégica y  RP', NULL, NULL),
(22, 'Contacto y Gestión Ciudadana', 'Contacto y Gestión Ciudadana', NULL, NULL),
(23, 'Despacho de Presidencia', 'Despacho de Presidencia', NULL, NULL),
(24, 'Unidad Municipal de Protección Civil', 'Unidad Municipal de Protección Civil', NULL, NULL),
(25, 'Secretaría del Ayuntamiento', 'Secretaría del Ayuntamiento', NULL, NULL),
(26, 'Tesorería', 'Tesorería', NULL, NULL),
(27, 'Contraloría', 'Contraloría', NULL, NULL),
(28, 'Oficialía Mayor Administrativa', 'Oficialía Mayor Administrativa', NULL, NULL),
(29, 'Padrón y licencias', 'Padrón y licencias', NULL, NULL),
(30, 'Seguridad Pública y Transito', 'Seguridad Pública y Transito', NULL, NULL),
(31, 'Ordenamiento Territorial Desarrollo Urbano y Medio Ambiente', 'Ordenamiento Territorial Desarrollo Urbano y Medio', NULL, NULL),
(32, 'Servicios Públicos', 'Servicios Públicos', NULL, NULL),
(33, 'Obras Públicas', 'Obras Públicas', NULL, NULL),
(34, 'Turismo y Desarrollo Económico', 'Turismo y Desarrollo Económico', NULL, NULL),
(35, 'Desarrollo Y Bienestar Social', 'Desarrollo Y Bienestar Social', NULL, NULL),
(36, 'Dirección Jurídica', 'Dirección Jurídica', NULL, NULL),
(37, 'Sistema Municipal DIF', 'Sistema Municipal DIF', NULL, NULL),
(38, 'Comisión Municipal de Derechos Humanos', 'CMDH', NULL, NULL),
(39, 'Organismo Operador Municipal de Agua Potable Alcantarillado y Saneamiento', 'OROMAPAS', NULL, NULL),
(40, 'Coordinación de Gabinete', 'Coordinación de Gabinete', NULL, NULL),
(41, 'Comunicación Estratégica y  RP', 'Comunicación Estratégica y  RP', NULL, NULL),
(42, 'Contacto y Gestión Ciudadana', 'Contacto y Gestión Ciudadana', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones_has_mir_indicadores`
--

CREATE TABLE `direcciones_has_mir_indicadores` (
  `direcciones_id` int(11) NOT NULL,
  `mir_indicadores_id` int(11) NOT NULL,
  `puestos_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `direcciones_has_pm_linea_accion`
--

CREATE TABLE `direcciones_has_pm_linea_accion` (
  `direccion_id` int(11) NOT NULL,
  `pm_linea_accion_id` int(11) NOT NULL,
  `puesto_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '0 = Sin Asignar\n\n1 = Acptada\n\n2 = Rechazada'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `direcciones_has_pm_linea_accion`
--

INSERT INTO `direcciones_has_pm_linea_accion` (`direccion_id`, `pm_linea_accion_id`, `puesto_id`, `status`) VALUES
(1, 572, NULL, 0),
(1, 573, NULL, 0),
(1, 575, NULL, 0),
(1, 576, NULL, 0),
(1, 577, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estatus`
--

CREATE TABLE `estatus` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `folios`
--

CREATE TABLE `folios` (
  `id` int(11) NOT NULL,
  `folio` varchar(45) DEFAULT NULL,
  `descripcion` text,
  `users_id` int(10) UNSIGNED NOT NULL,
  `prioridad_id` int(11) NOT NULL,
  `estatus_id` int(11) NOT NULL,
  `comentario` text,
  `fecha_termino` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inf_agendas`
--

CREATE TABLE `inf_agendas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `punto` varchar(20) DEFAULT NULL,
  `normativa_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inf_agendas`
--

INSERT INTO `inf_agendas` (`id`, `nombre`, `descripcion`, `punto`, `normativa_id`, `created_at`, `updated_at`) VALUES
(1, 'SECCIÓN A', 'Agenda Básica para el Desarrollo Municipal', 'A', 2, NULL, NULL),
(2, 'SECCIÓN B', 'La sección ampliada (B) del programa Agenda para el Desarrollo Municipal se ocupa de temas que, en sentido\r\nestricto, no constituyen funciones de los municipios, pero en los que se han involucrado para contribuir con\r\nla Federación y los estados en el logro de objetivos de desarrollo integral para sus habitantes. La sección está\r\nintegrada por tres ejes temáticos, cada uno de los cuales se subdivide en los rubros o temas que se enuncian a\r\ncontinuación.', 'B', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inf_ejes`
--

CREATE TABLE `inf_ejes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `punto` varchar(20) DEFAULT NULL,
  `objetivo` text,
  `inf_agenda_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inf_ejes`
--

INSERT INTO `inf_ejes` (`id`, `nombre`, `punto`, `objetivo`, `inf_agenda_id`, `created_at`, `updated_at`) VALUES
(1, 'Desarrollo Territorial', 'A.1', 'Regular los usos y aprovechamientos del suelo en los centros de población del municipio, con el fin de utilizar y aprovechar el territorio de manera ordenada y sustentable.', 1, NULL, NULL),
(2, 'Servicios Públicos', 'A.2', 'El eje de Servicios Públicos se desglosa en 11 temas: calles, agua potable, drenaje y alcantarillado,\r\naguas residuales, limpia, residuos sólidos, parques y jardines, alumbrado público, mercados públicos,\r\npanteones y rastro.', 1, NULL, NULL),
(3, 'Seguridad Pública', 'A.3', 'El eje de Seguridad Pública está compuesto por los temas de prevención social de la violencia y la\r\ndelincuencia; policía preventiva, seguridad pública y tránsito.', 1, NULL, NULL),
(4, 'Desarrollo Institucional', 'A.4. ', 'Desarrollo Institucional atiende los temas de organización; planeación y control\r\ninterno; capacitación; tecnologías de la información; transparencia y acceso a la información pública;\r\narmonización contable; ingresos; egresos, Y deuda.', 1, NULL, NULL),
(5, 'Desarrollo Económico', 'B.1', 'El eje temático de Desarrollo Económico contiene los rubros de empleo, transporte público, conectividad,\r\ncomercio y servicios; industria, agricultura, ganadería, pesca y turismo.', 2, NULL, NULL),
(6, 'Desarrollo Social', 'B.2', 'El eje de Desarrollo Social engloba los temas de pobreza, educación, salud, vivienda, grupos vulnerables,\r\nigualdad de género, juventud, deporte y recreación; así como patrimonio cultural.', 2, NULL, NULL),
(7, 'Desarrollo Ambiental', 'B.3', 'El eje de Desarrollo Ambiental está compuesto por el rubro de medio ambiente.', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inf_indicadores`
--

CREATE TABLE `inf_indicadores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `punto` varchar(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '0 = No Asignado, 1 = Asignado',
  `inf_tema_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inf_indicadores`
--

INSERT INTO `inf_indicadores` (`id`, `nombre`, `descripcion`, `punto`, `status`, `inf_tema_id`, `created_at`, `updated_at`) VALUES
(1, 'Marco normativo para la planeación urbana.', 'El municipio cuenta con disposiciones normativas para la planeación de sus centros de población.', 'A.1.1.1', 0, 1, NULL, NULL),
(2, 'Instancia responsable de la planeación urbana.', 'El municipio cuenta con una instancia encargada de la planeación urbana.', 'A.1.1.2', 0, 1, NULL, NULL),
(3, 'Diagnóstico de desarrollo urbano.', 'El municipio cuenta con un diagnóstico de la situación de los asentamientos humanos y centros de población en su territorio.', 'A.1.1.3', 0, 1, NULL, NULL),
(4, 'Recursos humanos capacitados para la planeación urbana.', 'VEl municipio cuenta con los recursos humanos debidamente capacitados para la planeación de los asentamientos humanos y centros de población.', 'A.1.1.4', 0, 1, NULL, NULL),
(5, 'Plan o programa municipal de desarrollo urbano.', 'El municipio cuenta con un plan o programa municipal de desarrollo urbano publicado e inscrito en el Registro Público de la Propiedad.', 'A.1.1.5', 0, 1, NULL, NULL),
(6, 'Procedimiento para la emisión de licencias de construcción.', 'VEl municipio cuenta con un procedimiento enfocado a regular y mejorar la eficiencia en la emisión de licencias de construccion.', 'A.1.1.6', 0, 1, NULL, NULL),
(7, 'Coordinación para la planeación urbana.', 'El municipio cuenta con instrumentos de coordinación vigentes en materia de planeación urbana.', 'A.1.1.7', 0, 1, NULL, NULL),
(8, 'Tasa de abatimiento del uso o aprovechamiento del territorio no apto para asentamientos humanos.', '[(Extensión territorial (km²) en asentamientos humanos con un uso o aprovechamiento en zonas no aptas en el año evaluado / Extensión territorial (km²) en asentamientos humanos con un uso o aprovechamiento en zonas no aptas en el año previo al evaluado)-1] * -100A', 'A.1.1.8', 0, 1, NULL, NULL),
(9, 'Marco normativo para el Ordenamiento Ecológico.', 'El municipio cuenta con un Reglamento Interno o una disposición normativa para el Ordenamiento Ecológico.', 'A.1.2.1', 0, 2, NULL, NULL),
(10, 'Instancia responsable del Ordenamiento Ecológico.', 'El municipio cuenta con un comité de ordenamiento ecológico conformado por los sectores interesados.', 'A.1.2.2', 0, 2, NULL, NULL),
(11, 'Diagnóstico de Ordenamiento Ecológico.', 'El municipio cuenta con un diagnóstico que identifica las aptitudes por sector y analiza los conflictos ambientales en su territorio.', 'A.1.2.3', 0, 2, NULL, NULL),
(12, 'Recursos humanos capacitados para el Ordenamiento Ecológico.', 'El municipio cuenta con los recursos humanos debidamente capacitados para el Ordenamiento Ecológico.', 'A.1.2.4', 0, 2, NULL, NULL),
(13, 'Programa de Ordenamiento Ecológico.', 'El municipio cuenta con un Programa de Ordenamiento Ecológico, local o regional, publicado de acuerdo con la legislación respectiva.', 'A.1.2.5', 0, 2, NULL, NULL),
(14, 'Acciones para la implementación delOrdenamiento Ecológico.', 'El municipio realiza acciones que contribuyen a la implementación del ordenamiento ecológico, local o regional, publicado de acuerdo con la legislaciónrespectiva.', 'A.1.2.6', 0, 2, NULL, NULL),
(15, 'Coordinación para el Ordenamiento Ecológico.', 'El municipio cuenta con mecanismos de coordinación vigentes en materia de Ordenamiento Ecológico.', 'A.1.2.7', 0, 2, NULL, NULL),
(16, 'Tasa de uso y aprovechamiento del suelo en zonas aptas.', '[(Extensión territorial (km²) de las unidades de gestión\r\nambiental con un uso o aprovechamiento del suelo en zonas\r\naptas en el año evaluado / Extensión territorial (km²) de las\r\nunidades de gestión ambiental con un uso o aprovechamiento\r\ndel suelo en zonas aptas del año previo al evaluado)-1]*-100', 'A.1.2.8', 0, 2, NULL, NULL),
(17, 'Marco normativo de protección civil', 'El municipio cuenta con un reglamento de protección civil.', 'A.1.3.1', 0, 3, NULL, NULL),
(18, 'Marco normativo en materia de\r\nconstrucciones.', 'El municipio cuenta con un reglamento de\r\nconstrucción.', 'A.1.3.2', 0, 3, NULL, NULL),
(19, 'Instancias responsables de la protección civil.', 'El municipio cuenta con una Unidad de protección civil y con un Consejo municipal de protección civil formalmente instalado y que opera regularmente.', 'A.1.3.3', 0, 3, NULL, NULL),
(20, 'Atlas municipal de riesgos. ', 'El municipio cuenta con un atlas de riesgos. \r\n', 'A.1.3.4', 0, 3, NULL, NULL),
(21, 'Recursos humanos capacitados en\r\nmateria de protección civil.', 'El municipio cuenta con los recursos humanos capacitados para realizar las actividades de protección civil.', 'A.1.3.5', 0, 3, NULL, NULL),
(22, 'Acciones para prevenir los riesgos.', 'El municipio realiza acciones que contribuyen a la prevención de riesgos en su territorio. ', 'A.1.3.6', 0, 3, NULL, NULL),
(23, 'Programa municipal de protección civil.', ' El municipio cuenta con un Programa de Protección\r\nCivil; para proteger, asistir y prevenir a la población\r\nen caso de una contingencia o desastre natural. ', 'A.1.3.7', 0, 3, NULL, NULL),
(24, 'Tasa de crecimiento de asentamientos humanos en zonas de riesgo.', '(( Extensión territorial (km2) de zonas de riesgo ocupadas por\r\nasentamientos humanos en el año evaluado - Extensión\r\nterritorial (Km2) de zonas de riesgo ocupadas por\r\nasentamientos humanos en el año previo al evaluado) /\r\nExtensión territorial (Km2) de zonas de riesgo ocupadas por\r\nasentamientos humanos en el año previo al evaluado) * 100', 'A.1.3.8', 0, 3, NULL, NULL),
(28, 'Marco normativo para la construcción y mantenimiento de calles.', 'El municipio cuenta con un reglamento para la construcción y mantenimiento de calles.', 'A.2.1.1', 0, 26, NULL, NULL),
(29, 'Instancia responsable de la construcción y mantenimiento de calles.', 'El municipio cuenta con una instancia responsable de la construcción y mantenimiento de calles.', 'A.2.1.2', 0, 26, NULL, NULL),
(30, 'Diagnostico de construcción y mantenimiento de calles', 'El municipio cuenta con un diagnostico en materia de construcción y mantenimiento de calles.', 'A.2.1.3', 0, 26, NULL, NULL),
(31, 'Programa operativo de construcción y mantenimiento de calles.', 'El municipio cuenta con un programa operativo en materia de construcción y mantenimiento de calles.', 'A.2.1.4', 0, 26, NULL, NULL),
(32, 'Tasa de abatimiento de calles sin revestimiento.', '((Metros lineales de calles sin revestimiento en el año evaluado – Metros lineales de calles sin revestimiento en el año previo al evaluado) / Metros lineales sin revestimiento en el año previo al evaluado) * -100', 'A.2.1.5', 0, 26, NULL, NULL),
(33, 'Cobertura de mantenimiento de calles.', '(Metros lineales de calles que recibieron mantenimiento / Metros lineales de calles que requerían mantenimiento en el año evaluado) * 100', 'A.2.1.6', 0, 26, NULL, NULL),
(34, 'Satisfacción ciudadana de la construcción de calles.', '(Número de encuestados que dicen estar satisfechos con las obras de pavimentación / Total de encuestados que dicen contar con el servicio) *100', 'A.2.1.7', 0, 26, NULL, NULL),
(35, 'Satisfacción ciudadana del mantenimiento de calles.', '(Número de encuestados que dicen estar satisfechos con las obras de mantenimiento / Total de encuestados que dicen contar con el servicio) * 100', 'A.2.1.8', 0, 26, NULL, NULL),
(36, 'Marco normativo del servicio de agua potable.', 'El municipio cuenta con un reglamento en materia de agua potable.', 'A.2.2.1', 0, 27, NULL, NULL),
(37, 'Instancia responsable del servicio de agua potable.', 'El municipio cuenta con una instancia responsable del servicio de agua potable.', 'A.2.2.2', 0, 27, NULL, NULL),
(38, 'Diagnóstico del servicio de agua potable.', 'El municipio cuenta con un diagnóstico del servicio de agua potable.', 'A.2.2.3', 0, 27, NULL, NULL),
(39, 'Programa de operación y administración del servicio de agua potable.', 'El municipio cuenta con un programa de operación y administración del servicio de agua potable.', 'A.2.2.4', 0, 27, NULL, NULL),
(40, 'Tasa de abatimiento de la carencia de servicio de agua potable en las viviendas.', '((Viviendas sin toma de agua potable en el año evaluado - Viviendas sin toma de agua potable en el año previo al evaluado) / Viviendas sin toma de agua potable en el año previo al evaluado) * -100', 'A.2.2.5', 0, 27, NULL, NULL),
(41, 'Autonomía financiera del sistema de agua potable.', 'Total de ingresos por derechos de agua potable del año evaluado / Costo total del sistema de agua potable del año evaluado', 'A.2.2.6', 0, 27, NULL, NULL),
(42, 'Satisfacción ciudadana del servicio de agua potable.', '(Número de encuestados que dicen estar satisfechos con el servicio de agua potable / Total de encuestados que dicen contar con el servicio) * 100', 'A.2.2.7', 0, 27, NULL, NULL),
(43, 'Marco normativo en materia de drenaje y alcantarillado.', 'El municipio cuenta con un reglamento en materia de drenaje y alcantarillado.', 'A.2.3.1', 0, 28, NULL, NULL),
(44, 'Instancia responsable del servicio de drenaje y alcantarillado.', 'El municipio cuenta con una instancia responsable del servicio de drenaje y alcantarillado.', 'A.2.3.2', 0, 28, NULL, NULL),
(45, 'Diagnóstico del servicio de drenaje y alcantarillado.', 'El municipio cuenta con un diagnóstico del servicio de drenaje y alcantarillado.', 'A.2.3.3', 0, 28, NULL, NULL),
(46, 'Programa de operación y administración del servicio de drenaje y alcantarillado.', 'El municipio cuenta con un programa de operación y administración del servicio de drenaje y alcantarillado.', 'A.2.3.4', 0, 28, NULL, NULL),
(47, 'Tasa de abatimiento del déficit del servicio de drenaje en viviendas particulares.', '((Viviendas sin drenaje en el año evaluado – Viviendas sin drenaje en el año previo al evaluado) / Viviendas sin drenaje en el año previo al evaluado) * –100', 'A.2.3.5', 0, 28, NULL, NULL),
(48, 'Tasa de abatimiento del déficit del servicio de alcantarillado en arterias viales.', '((Tramos de calle sin alcantarillado en el año evaluado – Tramos de calle sin alcantarillado en el año previo al evaluado) / Tramos de calle sin alcantarillado en el año previo al evaluado) * -100', 'A.2.3.6', 0, 28, NULL, NULL),
(49, 'Satisfacción ciudadana del servicio de drenaje.', '(Numero de encuestados que dicen estar satisfechos con el servicio de drenaje / Total de encuestados que dicen contar con el servicio) * 100', 'A.2.3.7', 0, 28, NULL, NULL),
(50, 'Satisfacción ciudadana del servicio de alcantarillado.', '(Número de encuestados que dicen estar satisfechos con el servicio de alcantarillado / Total de encuestados que dicen contar con el servicio) * 100', 'A.2.3.8', 0, 28, NULL, NULL),
(51, 'Normativa para el tratamiento y descarga de aguas residuales.', 'El municipio cuenta con disposiciones normativas en materia de tratamiento y descarga de agua.', 'A.2.4.1', 0, 29, NULL, NULL),
(52, 'Instancia responsable del tratamiento y descarga de aguas residuales.', 'El municipio cuenta con una instancia responsable del tratamiento y descarga de aguas residuales.', 'A.2.4.2', 0, 29, NULL, NULL),
(53, 'Diagnóstico del tratamiento y descarga de aguas residuales.', 'El municipio cuenta con un diagnóstico del tratamiento y descarga de aguas residuales.', 'A.2.4.3', 0, 29, NULL, NULL),
(54, 'Programa para el tratamiento y descarga de aguas residuales.', 'El municipio cuenta con un programa para el tratamiento y descarga de aguas residuales.', 'A.2.4.4', 0, 29, NULL, NULL),
(55, 'Porcentaje de agua tratada.', '(Metros cúbicos (m3) de agua tratada en el año evaluado / Metros cúbicos (m3) totales de aguas residuales en el año evaluado) * 100', 'A.2.4.5', 0, 29, NULL, NULL),
(56, 'Marco normativo para el servicio de limpia.', 'El Municipio cuenta con un marco normativo para la prestación del servicio de limpia.', 'A.2.5.1', 0, 30, NULL, NULL),
(57, 'Instancia responsable del servicio de limpia.', 'El municipio cuenta con una instancia responsable del servicio de limpia.', 'A.2.5.2', 0, 30, NULL, NULL),
(58, 'Diagnóstico municipal del servicio de limpia.', 'El municipio cuenta con un diagnóstico del servicio de limpia.', 'A.2.5.3', 0, 30, NULL, NULL),
(59, 'Programa de limpia.', 'El municipio cuenta con un programa operativo para el servicio de limpia.', 'A.2.5.4', 0, 30, NULL, NULL),
(60, 'Cobertura de mobiliario para la prestación del servicio de limpia (botes de basura).', '(Espacios públicos que cuentan con al menos un bote de basura en el año evaluado / Total de espacios públicos en el municipio en el año evaluado)*100', 'A.2.5.5', 0, 30, NULL, NULL),
(61, 'Cobertura del servicio de limpia en vialidades y espacios públicos.', '(Tramos de calles y espacios públicos con servicio continuo de limpia en el año evaluado / Total de tramos de calles y espacios públicos del municipio en el año evaluado) * 100', 'A.2.5.6', 0, 30, NULL, NULL),
(62, 'Satisfacción ciudadana del servicio de limpia.', '(Número de encuestados que dicen estar satisfechos con el servicio de limpia / Total de encuestados que dicen contar con el servicio) * 100', 'A.2.5.7', 0, 30, NULL, NULL),
(63, 'Marco normativo en materia de residuos sólidos.', 'El municipio cuenta con un reglamento en materia de residuos sólidos.', 'A.2.6.1', 0, 31, NULL, NULL),
(64, 'Instancia responsable de la gestión integral de los residuos sólidos.', 'El municipio cuenta con una instancia responsable de la gestión integral de los residuos sólidos.', 'A.2.6.2', 0, 31, NULL, NULL),
(65, 'Diagnóstico en materia de gestión integral de los residuos sólidos.', 'Diagnóstico de la gestión integral de los residuos sólidos.', 'A.2.6.3', 0, 31, NULL, NULL),
(66, 'Sitio de disposición final de los residuos sólidos municipales.', 'El municipio deposita los residuos sólidos en un sitio que cumple con todos los requerimientos de la NOM-083-SEMARNAT-2003.', 'A.2.6.4', 0, 31, NULL, NULL),
(67, 'Programa para la gestión integral de residuos sólidos.', 'El municipio cuenta con un programa operativo para la gestión integral de los residuos sólidos.', 'A.2.6.5', 0, 31, NULL, NULL),
(68, 'Cobertura del servicio de recolección de residuos sólidos.', '(Viviendas particulares habitadas que reciben el servicio de recolección de residuos en el año evaluado / Total de viviendas particulares habitadas del municipio en el año evaluado) * 100', 'A.2.6.6', 0, 31, NULL, NULL),
(69, 'Porcentaje de residuos sólidos dispuestos conforme a la NOM-083-SEMARNAT-2003.', '(Toneladas de residuos sólidos dispuestas en un sitio que cumple lo dispuesto por la NOM-083-SEMARNAT-2003 en el año evaluado / Total de toneladas de residuos sólidos recolectadas en el año evaluado) * 100', 'A.2.6.7', 0, 31, NULL, NULL),
(70, 'Satisfacción ciudadana del servicio de recolección de residuos sólidos.', '(Número de encuestados que dicen estar satisfechos con la recolección de residuos sólidos / Total de encuestados que dicen contar con el servicio) * 100', 'A.2.6.8', 0, 31, NULL, NULL),
(71, 'Marco normativo del servicio de parques y jardines.', 'El municipio cuenta con disposiciones normativas en materia de parques y jardines.', 'A.2.7.1', 0, 32, NULL, NULL),
(72, 'Instancia responsable del servicio de parques y jardines.', 'El municipio cuenta con una instancia responsable del servicio de parques y jardines.', 'A.2.7.2', 0, 32, NULL, NULL),
(73, 'Diagnóstico del servicio de parques y jardines.', 'El municipio cuenta con un diagnóstico del servicio de parques y jardines.', 'A.2.7.3', 0, 32, NULL, NULL),
(74, 'Programa operativo del servicio de parques y jardines.', 'El municipio cuenta con un programa operativo para la construcción y mantenimiento de parques y jardines.', 'A.2.7.4', 0, 32, NULL, NULL),
(75, 'Tasa de crecimiento anual del índice de áreas verdes y recreativas per cápita.', '((Áreas verdes y recreativas per cápita en el año evaluado - Áreas verdes y recreativas per cápita en el año previo al evaluado) / Áreas verdes y recreativas per cápita en el año previo al evaluado) * 100', 'A.2.7.5', 0, 32, NULL, NULL),
(76, 'Satisfacción ciudadana del servicio de áreas verdes y recreativas.', '(Número de encuestados que dicen estar satisfechos con las áreas verdes y recreativas / Total de encuestados que dicen hacer uso del servicio) * 100', 'A.2.7.6', 0, 32, NULL, NULL),
(77, 'Marco normativo del servicio de alumbrado público.', 'El municipio cuenta con disposiciones normativas en materia del servicio de alumbrado público.', 'A.2.8.1', 0, 33, NULL, NULL),
(78, 'Instancia responsable del servicio de alumbrado público.', 'El municipio cuenta con una instancia responsable del servicio de alumbrado público.', 'A.2.8.2', 0, 33, NULL, NULL),
(79, 'Diagnóstico del servicio de alumbrado público.', 'El municipio cuenta con un diagnóstico del servicio de alumbrado público.', 'A.2.8.3', 0, 33, NULL, NULL),
(80, 'Programa operativo del servicio de alumbrado público.', 'El municipio cuenta con un programa operativo para el servicio de alumbrado público.', 'A.2.8.4', 0, 33, NULL, NULL),
(81, 'Cobertura en el servicio de alumbrado público.', '(Tramos de calles y espacios públicos que disponen del servicio de alumbrado público con luminarias en buenas condiciones en el año evaluado / Tramos de calles y espacios públicos del municipio en el año evaluado)*100', 'A.2.8.5', 0, 33, NULL, NULL),
(82, 'Abatimiento del costo promedio por luminaria.', '((Costo promedio por luminaria del año evaluado – Costo promedio por luminaria en el año previo al evaluado) / Costo promedio por luminaria en el año previo al evaluado) * -100', 'A.2.8.6', 0, 33, NULL, NULL),
(83, 'Satisfacción ciudadana del servicio de alumbrado público.', '(Número de encuestados que dicen estar satisfechos con el servicio de alumbrado público / Total de encuestados que dicen contar con el servicio) * 100', 'A.2.8.7', 0, 33, NULL, NULL),
(84, 'Marco normativo para mercados públicos.', 'El municipio cuenta con un marco normativo en materia de mercados públicos.', 'A.2.9.1', 0, 34, NULL, NULL),
(85, 'Instancia responsable del servicio de mercados públicos.', 'El municipio cuenta con una instancia responsable del servicio de mercados públicos.', 'A.2.9.2', 0, 34, NULL, NULL),
(86, 'Diagnóstico de mercados públicos.', 'El municipio cuenta con un diagnóstico en materia de mercados públicos.', 'A.2.9.3', 0, 34, NULL, NULL),
(87, 'Programa de mercados públicos.', 'El municipio cuenta con un programa operativo en materia de mercados públicos.', 'A.2.9.4 ', 0, 34, NULL, NULL),
(88, 'Cobertura en el servicio de mercados públicos per cápita.', '(Total de mercados públicos municipales en el año evaluado / Total de habitantes en el año evaluado)* 100,000', 'A.2.9.5', 0, 34, NULL, NULL),
(89, 'Satisfacción ciudadana del servicio de mercados públicos.', '(Número de encuestados que dicen estar satisfechos con los mercados públicos / Total de encuestados que dicen hacer uso del servicio) * 100', 'A.2.9.6', 0, 34, NULL, NULL),
(90, 'Marco normativo del servicio de panteones.', 'El municipio cuenta con disposiciones normativas en materia de panteones.', 'A.2.10.1', 0, 35, NULL, NULL),
(91, 'Instancia responsable del servicio de panteones.', 'El municipio cuenta con una instancia responsable del servicio de panteones.', 'A.2.10.2', 0, 35, NULL, NULL),
(92, 'Diagnóstico del servicio de panteones.', 'El municipio cuenta con un diagnóstico del servicio de panteones.', 'A.2.10.3', 0, 35, NULL, NULL),
(93, 'Programa de operación y administración del servicio de panteones.', 'El municipio cuenta con un programa de operación y administración del servicio de panteones.', 'A.2.10.4', 0, 35, NULL, NULL),
(94, 'Cobertura en el servicio de panteones.', 'Número de espacios disponibles para depósito de restos humanos registrados por el municipio en el año evaluado / Demanda potencial de espacios para el depósito de restos humanos', 'A.2.10.5', 0, 35, NULL, NULL),
(95, 'Satisfacción ciudadana del servicio de panteones.', '(Número de encuestados que dicen estar satisfechos con el número de panteones / Número total de encuestados que dicen hacer uso del servicio) * 100', 'A.2.10.6', 0, 35, NULL, NULL),
(96, 'Marco normativo del servicio de rastro.', 'El municipio cuenta con disposiciones normativas en materia de rastro.', 'A.2.11.1', 0, 36, NULL, NULL),
(97, 'Instancia responsable del servicio de rastro.', 'El municipio cuenta con una instancia responsable del servicio de rastro.', 'A.2.11.2', 0, 36, NULL, NULL),
(98, 'Diagnóstico del servicio de rastro.', 'El municipio cuenta con un diagnóstico del servicio de rastro.', 'A.2.11.3', 0, 36, NULL, NULL),
(99, 'Infraestructura y equipamiento para la prestación del servicio de rastro.', 'El municipio utiliza y/o cuenta con la infraestructura y equipamiento adecuados para la prestación del servicio de rastro.', 'A.2.11.4', 0, 36, NULL, NULL),
(100, 'Programa de operación y administración del servicio de rastro.', 'El municipio cuenta con un programa de operación y administración del servicio de rastro.', 'A.2.11.5', 0, 36, NULL, NULL),
(101, 'Cobertura del servicio de rastro.', '(Número de cabezas de ganado sacrificado en rastros en el año evaluado / Total de cabezas de ganado sacrificado en el año evaluado) * 100', 'A.2.11.6', 0, 36, NULL, NULL),
(102, 'Marco normativo de la Instancia Local para la prevención social de la violencia y la delincuencia.', 'El municipio cuenta con disposiciones normativas para la Prevención Social de la Violencia y la Delincuencia.', 'A.3.1.1', 0, 37, NULL, NULL),
(103, 'Instancia responsable de la prevención social de la violencia y la delincuencia.', 'El municipio cuenta con una instancia responsable de la prevención social de la violencia y la delincuencia.', 'A.3.1.2. ', 0, 37, NULL, NULL),
(104, 'Diagnóstico de prevención social de la violencia y la delincuencia.', 'El municipio cuenta con un diagnóstico de prevención social de la violencia y la delincuencia que identifica factores de riesgo en su territorio.', 'A.3.1.3. ', 0, 37, NULL, NULL),
(105, 'Recursos humanos capacitados en materia de prevención social de la violencia y la delincuencia.', 'El municipio cuenta con los recursos humanos debidamente capacitados para realizar las actividades de prevención social de la violencia y la delincuencia.', 'A.3.1.4. ', 0, 37, NULL, NULL),
(106, 'Programa de prevención social de la violencia y la delincuencia.', 'El municipio cuenta con un programa de prevención social de la violencia y la delincuencia.', 'A.3.1.5. ', 0, 37, NULL, NULL),
(107, 'Acciones para fomentar la participación ciudadana en la prevención social de la violencia y la delincuencia.', 'El municipio implementa acciones que fomentan la participación ciudadana en la prevención social de la violencia y la delincuencia.', 'A.3.1.6. ', 0, 37, NULL, NULL),
(108, 'Marco normativo de policía preventiva.', 'El municipio cuenta con un marco normativo en materia de policía preventiva.', 'A.3.2.1. ', 0, 38, NULL, NULL),
(109, 'Instancia responsable del desempeño de la función de policía preventiva.', 'El municipio cuenta con una instancia responsable de la función de Policía Preventiva.', 'A.3.2.2. ', 0, 38, NULL, NULL),
(110, 'Diagnóstico de policía preventiva.', 'El municipio cuenta con un diagnóstico completo y actualizado en materia de policía preventiva.', 'A.3.2.3. ', 0, 38, NULL, NULL),
(111, 'Programa de operación y administración de la policía preventiva municipal.', 'El municipio cuenta con un programa de operación y administración del servicio de policía preventiva municipal.', 'A.3.2.4. ', 0, 38, NULL, NULL),
(112, 'Tasa de crecimiento anual del índice de policías operativos por cada 1,000 habitantes.', '((Número de policías operativos por cada 1000 habitantes en el año evaluado - Número de policías operativos por cada 1000 habitantes en el año previo al evaluado) / Número de policías operativos por cada 1,000 habitantes en el año previo al evaluado) * 100', 'A.3.2.5. ', 0, 38, NULL, NULL),
(113, 'Marco normativo de seguridad pública.', 'El municipio cuenta con un marco normativo en materia de seguridad pública.', 'A.3.3.1. ', 0, 39, NULL, NULL),
(114, 'Instancia responsable del desempeño de la función de seguridad pública.', 'El municipio cuenta con una instancia responsable de la Seguridad Pública y órganos colegiados para conocer y resolver las controversias en materia de la Carrera Policial y el Régimen Disciplinario.', 'A.3.3.2. ', 0, 39, NULL, NULL),
(115, 'Diagnóstico de seguridad pública.', 'El municipio cuenta con un diagnóstico completo y actualizado en materia de seguridad pública, que contempla los factores sociales, culturales, económicos y urbanos.', 'A.3.3.3. ', 0, 39, NULL, NULL),
(116, 'Programa de seguridad pública.', 'El municipio cuenta con un programa de seguridad pública.', 'A.3.3.4. ', 0, 39, NULL, NULL),
(117, 'Tasa de abatimiento de la incidencia delictiva.', '', 'A.3.3.5. ', 0, 39, NULL, NULL),
(118, 'Marco normativo de tránsito.', 'El municipio cuenta con un marco normativo en materia de tránsito.', 'A.3.4.1. ', 0, 40, NULL, NULL),
(119, 'Instancia responsable de la función de tránsito.', 'El municipio cuenta con una instancia responsable de la función de tránsito.', 'A.3.4.2.', 0, 40, NULL, NULL),
(120, 'Diagnóstico de tránsito.', 'El municipio cuenta con un diagnóstico completo y actualizado en materia de tránsito.', 'A.3.4.3. ', 0, 40, NULL, NULL),
(121, 'Programa de operación y administración del tránsito municipal.', 'El municipio cuenta con un programa de operación y administración del tránsito municipal.', 'A.3.4.4. ', 0, 40, NULL, NULL),
(122, 'Tasa de abatimiento del índice de siniestralidad (accidentes) de tránsito.', '((Índice de siniestralidad en el año evaluado - índice de siniestralidad en el año previo al evaluado) / índice de siniestralidad en el año previo al evaluado) * -100', 'A.3.4.5. ', 0, 40, NULL, NULL),
(123, 'Marco normativo en materia de organización de la Administración Pública Municipal (APM).', 'El municipio cuenta con un marco normativo adecuado para llevar a cabo las tareas derivadas de sus atribuciones constitucionales.', 'A.4.1.1. ', 0, 50, NULL, NULL),
(124, 'Diagnóstico de organización de la Administración Pública Municipal.', 'El municipio cuenta con un diagnóstico de la organización de la Administración Pública Municipal.', 'A.4.1.2. ', 0, 50, NULL, NULL),
(125, 'Programa municipal tendiente a redimensionar la estructura organizacional y los tabuladores salariales, adecuados a las necesidades de la función pública municipal.', 'El municipio cuenta con un programa municipal enfocado a optimizar el uso de los recursos públicos, redimensionar su estructura organizacional y establecer tabuladores salariales adecuados a las necesidades de la función pública municipal.', 'A.4.1.3. ', 0, 50, NULL, NULL),
(126, 'Porcentaje de dependencias municipales en función del “organigrama óptimo”.', '(Número de dependencias que conforman la administración municipal / Número de dependencias contempladas en el “organigrama óptimo”) * 100', 'A.4.1.4. ', 0, 50, NULL, NULL),
(127, 'Personal municipal total por cada 1,000 habitantes.', '(Número de personal total / Población total) * 1000', 'A.4.1.5. ', 0, 50, NULL, NULL),
(128, 'Relación entre el sueldo recibido y el sueldo óptimo en función de la población.', 'Salario neto mensual recibido por el presidente municipal / Salario óptimo en función del tamaño poblacional', 'A.4.1.6. ', 0, 50, NULL, NULL),
(129, 'Porcentaje de puestos de mando medio y superior de la administración pública municipal ocupados por mujeres.', '(Puestos de mando medio y superior ocupados por mujeres en el año evaluado / Total de puestos de mando medio y superior de la APM en el año evaluado) * 100', 'A.4.1.7. ', 0, 50, NULL, NULL),
(130, 'Marco legal para la planeación y el control interno.', 'El municipio cuenta con disposiciones normativas en materia de planeación y control interno.', 'A.4.2.1. ', 0, 51, NULL, NULL),
(131, 'Instancia responsable de evaluar el cumplimiento del Plan Municipal de Desarrollo.', 'El municipio cuenta con una instancia responsable de evaluar el cumplimiento del Plan Municipal de Desarrollo.', 'A.4.2.2. ', 0, 51, NULL, NULL),
(132, 'Instancia responsable del control interno.', 'El municipio cuenta con una Contraloría Municipal o instancia responsable del control interno de la administración municipal.', 'A.4.2.3. ', 0, 51, NULL, NULL),
(133, 'Plan Municipal de Desarrollo.', 'El municipio cuenta con un Plan Municipal de Desarrollo debidamente publicado, que integra los elementos mínimos que le permiten contribuir al Sistema Nacional de Planeación y constituirse en el documento rector de la planeación municipal.', 'A.4.2.4. ', 0, 51, NULL, NULL),
(134, 'Programa de Control Interno.', 'El municipio cuenta con un programa de control interno de la administración municipal.', 'A.4.2.5. ', 0, 51, NULL, NULL),
(135, 'Índice de planeación y evaluación municipal.', '(X1*25) + (X2*15) + (X3*20) + (X4*20) + (X5*10) + (X6*10)', 'A.4.2.6. ', 0, 51, NULL, NULL),
(136, 'Diagnóstico de capacitación del personal de la administración pública municipal.', 'El municipio cuenta con un diagnóstico de capacitación del personal de la administración pública municipal.', 'A.4.3.1. ', 0, 52, NULL, NULL),
(137, 'Programa de capacitación para el personal de la administración pública municipal.', 'El municipio cuenta con un programa de capacitación para personal de la administración pública municipal.', 'A.4.3.2. ', 0, 52, NULL, NULL),
(138, 'Personal capacitado.', '(Personal de la administración pública municipal capacitado durante el año / Total de personal de la administración pública municipal) * 100', 'A.4.3.3. ', 0, 50, NULL, NULL),
(139, 'Diagnóstico del uso de las TIC’s en la Administración Pública Municipal.', 'El municipio cuenta con un diagnóstico del uso de las TIC’s en la Administración Pública Municipal.', 'A.4.4.1. ', 0, 53, NULL, NULL),
(140, 'Programa para impulsar el uso de las TIC´s en el desempeño institucional de la APM, así como en la realización de trámites y servicios ofrecidos a la población.', 'El municipio cuenta con un programa para impulsar el uso de las Tecnologías de la Información y la Comunicación (TIC´s) en el desempeño Institucional de la Administración Pública Municipal, así como en la realización de trámites y servicios ofrecidos a la población.', 'A.4.4.2. ', 0, 53, NULL, NULL),
(141, 'Índice de Gobierno Electrónico.', '((Número total de trámites y servicios de la APM con servicios informativos en la página web / Total de trámites y servicios de la APM) * (0.17) + (Número total de trámites y servicios de la APM con servicios interactivos en la página web / Total de trámites y servicios de la APM) * (0.33) + (Número total de trámites y servicios de la APM con servicios transaccionales en la página web / Total de trámites y servicios de la APM) * (0.5)) * 100', 'A.4.4.3. ', 0, 53, NULL, NULL),
(142, 'Tasa de crecimiento anual del índice de equipo de cómputo por cada 100 empleados.', '((Equipo de cómputo por cada 100 empleados con funciones administrativas en el año evaluado - Equipo de cómputo por cada 100 empleados con funciones administrativas en el año previo al evaluado) / Equipo de cómputo por cada 100 empleados con funciones administrativas en el año previo al evaluado) * 100', 'A.4.4.4. ', 0, 53, NULL, NULL),
(143, 'Marco normativo en materia de transparencia y acceso a la información pública.', 'El municipio cuenta con un reglamento en materia de transparencia y acceso a la información pública.', 'A.4.5.1. ', 0, 54, NULL, NULL),
(144, 'Instancia responsable de transparencia y acceso a la información pública.', 'El municipio cuenta con una unidad de transparencia y acceso a la información pública.', 'A.4.5.2. ', 0, 54, NULL, NULL),
(145, 'Diagnóstico de transparencia y acceso a la información pública.', 'El municipio cuenta con un diagnóstico de transparencia y acceso a la información pública.', 'A.4.5.3. ', 0, 54, NULL, NULL),
(146, 'Recursos para garantizar la transparencia y el acceso a la información pública.', 'El municipio cuenta con los recursos necesarios para garantizar la transparencia y el acceso a la información pública.', 'A.4.5.4. ', 0, 54, NULL, NULL),
(147, 'Programa de transparencia y acceso a la información pública.', 'El municipio cuenta con un programa operativo para garantizar la transparencia y el acceso a la información pública.', 'A.4.5.5. ', 0, 54, NULL, NULL),
(148, 'Coordinación en materia de transparencia y acceso a la información pública.', 'El municipio cuenta con mecanismos de coordinación con instancias estatales o federales competentes en materia de transparencia y acceso a la información pública.', 'A.4.5.6. ', 0, 54, NULL, NULL),
(149, 'Eficacia en la atención de solicitudes de acceso a la información.', '(Solicitudes de acceso a la información recurridas ante el órgano garante del derecho y falladas en contra del municipio / Total de solicitudes de información presentadas) * 100', 'A.4.5.7. ', 0, 54, NULL, NULL),
(150, 'Cumplimiento de obligaciones de transparencia.', '(Número de obligaciones de transparencia disponibles y actualizadas / Total de obligaciones de transparencia establecidas en la legislación) * 100', 'A.4.5.8. ', 0, 54, NULL, NULL),
(151, 'Marco normativo en materia de armonización contable.', 'El municipio cuenta con disposiciones normativas materia de armonización contable.', 'A.4.6.1. ', 0, 55, NULL, NULL),
(152, 'Instancia responsable de la armonización contable.', 'El municipio cuenta con una unidad competente en materia de contabilidad gubernamental, como parte de la administración pública municipal.', 'A.4.6.2. ', 0, 55, NULL, NULL),
(153, 'Diagnóstico de armonización contable.', 'El municipio cuenta con un diagnóstico que le permite identificar el grado de cumplimiento en materia de armonización contable.', 'A.4.6.3. ', 0, 55, NULL, NULL),
(154, 'Recursos para la armonización contable.', 'El municipio cuenta con el personal capacitado y los recursos tecnológicos necesarios para la aplicación del proceso de armonización contable.', 'A.4.6.4. ', 0, 55, NULL, NULL),
(155, 'Programa de armonización contable.', 'El municipio cuenta con un programa operativo en materia de armonización contable.', 'A.4.6.5. ', 0, 55, NULL, NULL),
(156, 'Coordinación en materia de armonización contable.', 'El municipio cuenta con mecanismos de coordinación con instancias estatales o federales competentes en materia de armonización contable.', 'A.4.6.6. ', 0, 55, NULL, NULL),
(157, 'Cumplimiento de obligaciones de armonización contable.', '(Número de obligaciones de armonización contable que se cumplen / Total de obligaciones de armonización contable establecidas en la legislación) * 100', 'A.4.6.7. ', 0, 55, NULL, NULL),
(158, 'Marco normativo de los ingresos', 'El municipio cuenta con disposiciones normativas en materia de ingresos.', 'A.4.7.1. ', 0, 56, NULL, NULL),
(159, 'Marco normativo en materia de catastro.', 'El municipio cuenta con disposiciones normativas en materia de catastro.', 'A.4.7.2. ', 0, 56, NULL, NULL),
(160, 'Diagnóstico de ingresos propios.', 'El municipio cuenta con una estrategia que le permita incrementar los ingresos propios.', 'A.4.7.3. ', 0, 56, NULL, NULL),
(161, 'Estrategia para incrementar los ingresos propios.', 'El municipio cuenta con una estrategia que le permita incrementar los ingresos propios.', 'A.4.7.4. ', 0, 56, NULL, NULL),
(162, 'Sistema de información catastral.', 'El municipio cuenta con un sistema de información catastral que le permite mantener actualizado su padrón de catastro.', 'A.4.7.5. ', 0, 56, NULL, NULL),
(163, 'Tasa de crecimiento real anual de la recaudación del impuesto predial.', '((Monto real del impuesto predial recaudado por el municipio en el año evaluado - Monto real del impuesto predial recaudado por el municipio en el año previo al evaluado) / Monto real del impuesto predial recaudado por el municipio en el año previo al evaluado) * 100', 'A.4.7.6. ', 0, 56, NULL, NULL),
(164, 'Tasa de crecimiento real anual de la recaudación por derecho de agua.', '((Monto real del derecho de agua recaudado por el municipio en el año evaluado - Monto real del derecho de agua recaudado por el municipio en el año previo al evaluado) / Monto real del derecho de agua recaudado por el municipio en el año previo al evaluado) * 100', 'A.4.7.7. ', 0, 56, NULL, NULL),
(165, 'Tasa de crecimiento real anual de la recaudación de otros ingresos propios.', '((Otros ingresos propios reales recaudados por el municipio en el año evaluado - Otros ingresos propios reales recaudados por el municipio en el año previo al evaluado) / Otros ingresos propios reales recaudados por el municipio en el año previo al evaluado)*100', 'A.4.7.8. ', 0, 56, NULL, NULL),
(166, 'Incremento de los recursos obtenidos por gestión de programas estatales o federales.', '((Monto por gestión de recursos estatales y federales en el año evaluado - Monto por gestión de recursos estatales y federales del año previo al evaluado) / Monto por gestión de recursos estatales y federales del año previo al evaluado) * 100', 'A.4.7.9. ', 0, 56, NULL, NULL),
(167, 'Marco normativo de los egresos.', 'El municipio cuenta con disposiciones normativas en materia de egresos.', 'A.4.8.1. ', 0, 57, NULL, NULL),
(168, 'Costo de operación.', '(Gasto corriente / Egresos totales) * 100', 'A.4.8.2. ', 0, 57, NULL, NULL),
(169, 'Proporción de los Adeudos de Ejercicios Fiscales Anteriores, respecto al ingreso total.', '(Monto total anual destinado al pago de ADEFAS en el año evaluado / Ingreso total anual en el año evaluado) *100', 'A.4.8.3. ', 0, 57, NULL, NULL),
(170, 'Porcentaje de participaciones destinadas a bienes y servicios públicos municipales.', '(Monto total de participaciones destinadas a bienes y servicios públicos en el año evaluado/ Monto total de participaciones en el año evaluado)* 100', 'A.4.8.4. ', 0, 57, NULL, NULL),
(171, 'Porcentaje de aportaciones destinadas a bienes y servicios públicos municipales.', '(Monto total de FAFMDTDF destinado a bienes y servicios públicos en el año evaluado/ Monto total del FAFMDTDF en el año evaluado) * 100', 'A.4.8.5. ', 0, 57, NULL, NULL),
(172, 'Balance presupuestario sostenible.', 'Ingresos totales en el año evaluado - Gastos totales del año evaluado (excepto amortización de la deuda)', 'A.4.8.6. ', 0, 57, NULL, NULL),
(173, 'Diagnóstico de Deuda.', 'El municipio cuenta con un diagnóstico de su deuda.', 'A.4.9.1. ', 0, 58, NULL, NULL),
(174, 'Programa para minimizar el peso de la deuda pública en los ingresos municipales.', 'El municipio cuenta con un programa enfocado a minimizar el peso de la deuda pública en los ingresos municipales y a privilegiar el financiamiento público sobre el privado.', 'A.4.9.2. ', 0, 58, NULL, NULL),
(175, 'Contratación de deuda pública y obligaciones.', 'El municipio cumple las obligaciones de financiamiento que la ley establece.', 'A.4.9.3. ', 0, 58, NULL, NULL),
(176, 'Nivel de endeudamiento municipal.', 'Endeudamiento Sostenible = 3\r\nEndeudamiento en Observación = 2\r\nEndeudamiento Elevado = 1', 'A.4.9.4. ', 0, 58, NULL, NULL),
(177, 'Instancia responsable de la capacitación y promoción del empleo.', 'El municipio cuenta con una instancia de desarrollo económico, que entre sus responsabilidades tiene la de promover la capacitación y el empleo.', 'B.1.1.1. ', 0, 59, NULL, NULL),
(178, 'Diagnóstico de capacitación y promoción del empleo.', 'El municipio cuenta con un diagnóstico sobre las condiciones del empleo y la capacitación para el trabajo entre su población.', 'B.1.1.2. ', 0, 59, NULL, NULL),
(179, 'Programa operativo para la capacitación y promoción del empleo.', 'El municipio cuenta con un programa enfocado a promover el empleo y la capacitación para el trabajo entre su población.', 'B.1.1.3. ', 0, 59, NULL, NULL),
(180, 'Coordinación para promover el empleo y la capacitación.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias para promover el empleo y la capacitación para el trabajo entre su población.', 'B.1.1.4. ', 0, 59, NULL, NULL),
(181, 'Marco normativo en materia de transporte público.', 'El municipio cuenta con disposiciones normativas en materia de transporte público.', 'B.1.2.1. ', 0, 60, NULL, NULL),
(182, 'Diagnóstico en materia de transporte público.', 'El municipio cuenta con un diagnóstico en materia de transporte público en su territorio.', 'B.1.2.2. ', 0, 60, NULL, NULL),
(183, 'Programa de mejora del transporte público.', 'El municipio cuenta con un programa o tiene conocimiento de un programa enfocado a mejorar el servicio de transporte público.', 'B.1.2.3. ', 0, 60, NULL, NULL),
(184, 'Coordinación para mejorar el transporte público.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias, enfocado a mejorar el transporte público.', 'B.1.2.4. ', 0, 60, NULL, NULL),
(185, 'Diagnóstico en materia de conectividad.', 'El municipio cuenta con un diagnóstico en materia de conectividad.', 'B.1.3.1. ', 0, 61, NULL, NULL),
(186, 'Programa en materia de conectividad.', 'El municipio cuenta con un programa en materia de conectividad, enfocado a reducir la brecha digital.', 'B.1.3.2. ', 0, 61, NULL, NULL),
(187, 'Coordinación para mejorar la conectividad.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias, enfocado a mejorar la conectividad.', 'B.1.3.3 ', 0, 61, NULL, NULL),
(188, 'Porcentaje de sitios y espacios públicos conectados a internet.', '(Número de sitios y espacios públicos con conexión a internet en el año evaluado / Total de sitios y espacios públicos en el municipio en el año evaluado) * 100', 'B.1.3.4. ', 0, 61, NULL, NULL),
(189, 'Marco normativo en materia de comercio y servicios.', 'El municipio cuenta con disposiciones normativas enfocadas a facilitar la apertura de unidades económicas en su municipio.', 'B.1.4.1. ', 0, 62, NULL, NULL),
(190, 'Instancia responsable de la mejora regulatoria.', 'El municipio cuenta con una instancia responsable de la mejora regulatoria para la apertura de unidades económicas.', 'B.1.4.2. ', 0, 62, NULL, NULL),
(191, 'Diagnóstico de la mejora regulatoria.', 'El municipio cuenta con un diagnóstico acerca de la apertura de unidades económicas.', 'B.1.4.3. ', 0, 62, NULL, NULL),
(192, 'Acciones para la mejora regulatoria.', 'El municipio cuenta con acciones encaminadas a la mejora regulatoria.', 'B.1.4.4. ', 0, 62, NULL, NULL),
(193, 'Coordinación para la mejora regulatoria.', 'El municipio cuenta con un convenio vigente para la mejora regulatoria.', 'B.1.4.5. ', 0, 62, NULL, NULL),
(194, 'Atracción y retención de inversión en el sector comercio y servicios.', '((Número de unidades de comercio y servicios existentes en el año evaluado - Número de unidades de comercio y servicios existentes en el año previo al evaluado) / Número de unidades de comercio y servicio existentes en el año previo al evaluado) * 100', 'B.1.4.6. ', 0, 62, NULL, NULL),
(195, 'Marco normativo en materia de industria.', 'El municipio cuenta con disposiciones normativas en materia de industria.', 'B.1.5.1. ', 0, 63, NULL, NULL),
(196, 'Instancia responsable de promover la industria.', 'El municipio cuenta con una instancia de desarrollo económico que entre sus responsabilidades tiene las de fomentar la industria.', 'B.1.5.2. ', 0, 63, NULL, NULL),
(197, 'Diagnóstico de la industria.', 'El municipio cuenta con un diagnóstico sobre la situación de la industria.', 'B.1.5.3. ', 0, 63, NULL, NULL),
(198, 'Programa de fomento de la industria.', 'El municipio cuenta con un programa enfocado a promover la industria.', 'B.1.5.4. ', 0, 63, NULL, NULL),
(199, 'Coordinación para promover la inversión en el sector industrial.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias para promover la inversión en el sector industrial.', 'B.1.5.5. ', 0, 63, NULL, NULL),
(200, 'Atracción y retención de inversión en el sector industrial.', '((Número de unidades industriales existentes en el año evaluado - Número de unidades industriales existentes en el año previo al evaluado) / Número de unidades industriales existentes en el año previo al evaluado) * 100', 'B.1.5.6. ', 0, 63, NULL, NULL),
(201, 'Marco normativo en materia de agricultura.', 'El municipio cuenta con un reglamento del consejo municipal para el desarrollo rural sustentable.', 'B.1.6.1', 0, 64, NULL, NULL),
(202, 'Instancia responsable de fomentar actividades en materia de agricultura.', 'El municipio cuenta con una instancia que funciona y fomenta la agricultura como actividad económica que se desarrolla en su territorio.', 'B.1.6.2. ', 0, 64, NULL, NULL),
(203, 'Diagnóstico de agricultura.', 'El municipio cuenta con un diagnóstico de la agricultura como actividad económica que se desarrolla en su territorio.', 'B.1.6.3. ', 0, 64, NULL, NULL),
(204, 'Programa de fomento de la agricultura como actividad económica.', 'El municipio cuenta con un programa enfocado a fomentar la agricultura como actividad económica que se desarrolla en su territorio.', 'B.1.6.4. ', 0, 64, NULL, NULL),
(205, 'Coordinación para promover la agricultura.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias para promover y fomentar la agricultura como actividad económica que se desarrolla en su territorio.', 'B.1.6.5. ', 0, 64, NULL, NULL),
(206, 'Atracción y retención de inversión del sector de la agricultura con la finalidad de incrementar la producción.', '((Número de unidades económicas dedicadas a la agricultura existentes en el año evaluado - Número de unidades económicas dedicadas a la agricultura existentes en el año previo al evaluado) / Número de unidades económicas dedicadas a la agricultura existentes en el año previo al evaluado) * 100', 'B.1.6.6. ', 0, 64, NULL, NULL),
(207, 'Marco normativo en materia de ganadería.', 'El municipio cuenta con un reglamento del consejo municipal para el desarrollo rural sustentable.', 'B.1.7.1. ', 0, 65, NULL, NULL),
(208, 'Instancia responsable de fomentar actividades en materia de ganadería.', 'El municipio cuenta con una instancia que funciona y fomenta la ganadería como actividad económica que se desarrolla en su territorio.', 'B.1.7.2. ', 0, 65, NULL, NULL),
(209, 'Diagnóstico de ganadería.', 'El municipio cuenta con un diagnóstico de la actividad ganadera que se desarrolla en su territorio.', 'B.1.7.3. ', 0, 65, NULL, NULL),
(210, 'Programa de fomento de la ganadería como actividad económica.', '', 'B.1.7.4. ', 0, 65, NULL, NULL),
(211, 'Coordinación para promover la ganadería.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias para promover y fomentar la ganadería como actividad económica que se desarrolla en su territorio.', 'B.1.7.5. ', 0, 65, NULL, NULL),
(212, 'Atracción y retención de inversión del sector ganadero con la finalidad de incrementar la producción.', '((Número de unidades económicas dedicadas a la ganadería existentes en el año evaluado - Número de unidades económicas dedicadas a la ganadería existentes en el año previo al evaluado) / Número de unidades económicas dedicadas a la ganadería existentes en el año previo al evaluado) * 100', 'B.1.7.6. ', 0, 65, NULL, NULL),
(213, 'Marco normativo en materia de pesca.', 'El municipio cuenta con un reglamento del consejo municipal para el desarrollo rural sustentable o una disposición normativa para la pesca como actividad económica.', 'B.1.8.1. ', 0, 66, NULL, NULL),
(214, 'Instancia responsable de fomentar actividades en materia de pesca.', 'El municipio cuenta con una instancia que funciona y fomenta la pesca como actividad económica que se desarrolla en su territorio.', 'B.1.8.2. ', 0, 66, NULL, NULL),
(215, 'Diagnóstico de pesca.', 'El municipio cuenta con un diagnóstico de la actividad pesquera que se desarrolla en su territorio.', 'B.1.8.3. ', 0, 66, NULL, NULL),
(216, 'Programa de fomento de la pesca como actividad económica.', 'El municipio cuenta con un programa enfocado a fomentar la pesca como actividad económica que se desarrolla en su territorio.', 'B.1.8.4. ', 0, 66, NULL, NULL),
(217, 'Coordinación para promover la pesca.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias para promover y fomentar la pesca como actividad económica que se desarrolla en su territorio.', 'B.1.8.5. ', 0, 66, NULL, NULL),
(218, 'Atracción y retención de inversión del sector pesquero con la finalidad de incrementar la producción.', '((Número de unidades económicas dedicadas a la pesca existentes en el año evaluado - Número de unidades económicas dedicadas a la pesca existentes en el año previo al evaluado) / Número de unidades económicas dedicadas a la pesca existentes en el año previo al evaluado) * 100', 'B.1.8.6. ', 0, 66, NULL, NULL),
(219, 'Marco normativo en materia de turismo.', 'El municipio cuenta con disposiciones normativas en materia de turismo.', 'B.1.9.1. ', 0, 67, NULL, NULL),
(220, 'Instancia responsable de fomento del turismo.', 'El municipio cuenta con una instancia de desarrollo económico, que entre sus responsabilidades tiene las de fomentar la actividad turística en su territorio.', 'B.1.9.2. ', 0, 67, NULL, NULL),
(221, 'Diagnóstico en materia de turismo', 'El municipio cuenta con un diagnóstico actualizado en materia de turismo en su territorio.', 'B.1.9.3. ', 0, 67, NULL, NULL),
(222, 'Programa de fomento del turismo.', NULL, 'B.1.9.4. ', 0, 67, NULL, NULL),
(223, 'Coordinación para promover el turismo.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias para promover y aprovechar de manera sustentable los atractivos turísticos de su territorio.', 'B.1.9.5. ', 0, 67, NULL, NULL),
(224, 'Flujo de turistas en el municipio.', NULL, 'B.1.9.6. ', 0, 67, NULL, NULL),
(225, 'Marco normativo en materia forestal.', 'El municipio cuenta con disposiciones normativas en materia forestal.', 'B.1.9.1. ', 0, 68, NULL, NULL),
(226, 'Instancia responsable de fomentar actividades en materia forestal.', 'El municipio cuenta con una instancia que funciona y fomenta al sector forestal como actividad económica que se desarrolla en su territorio.', 'B.1.10.2. ', 0, 68, NULL, NULL),
(227, 'Diagnóstico del sector forestal.', 'El municipio cuenta con un diagnóstico de la actividad forestal que se desarrolla en su territorio.', 'B.1.10.3. ', 0, 68, NULL, NULL),
(228, 'Programa de fomento del sector forestal como actividad económica.', 'El municipio cuenta con un programa enfocado a fomentar al sector forestal como actividad económica que se desarrolla en su territorio.', 'B.1.10.4. ', 0, 68, NULL, NULL);
INSERT INTO `inf_indicadores` (`id`, `nombre`, `descripcion`, `punto`, `status`, `inf_tema_id`, `created_at`, `updated_at`) VALUES
(229, 'Coordinación para promover al sectorforestal.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias para promover y fomentar al sector forestal como actividad económica que se desarrolla en su territorio.', 'B.1.10.5. ', 0, 68, NULL, NULL),
(230, 'Atracción y retención de inversión del sector forestal con la finalidad de incrementar la producción.', '(Número de unidades económicas existentes en el añoevaluado - Número de unidades económicas integradas al sector forestal existentes en el año previo al evaluado / Número de unidades económicas integradas al sector forestal existentes en el año previo al evaluado) * 100', 'B.1.10.6. ', 0, 68, NULL, NULL),
(231, 'Instancia responsable del combate a la pobreza.', 'El municipio cuenta con una instancia de desarrollo social que entre sus responsabilidades tiene la de diseñar e instrumentar acciones para el combate a la pobreza.', 'B.2.1.1. ', 0, 78, NULL, NULL),
(232, 'Diagnóstico de la situación de pobreza en el municipio.', 'El municipio cuenta con un diagnóstico de la situación de pobreza y rezago social de su población.', 'B.2.1.2. ', 0, 78, NULL, NULL),
(233, 'Programa para el combate a la pobreza.', 'El municipio cuenta con un programa para la atención de su población en condiciones de pobreza.', 'B.2.1.3. ', 0, 78, NULL, NULL),
(234, 'Coordinación para el combate a la pobreza.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias para impulsar el combate a la pobreza.', 'B.2.1.4. ', 0, 78, NULL, NULL),
(235, 'Instancia responsable de promover la educación básica.', 'El municipio cuenta con una instancia de desarrollo social que entre sus responsabilidades tiene la de diseñar e instrumentar acciones para promover la educación básica.', 'B.2.2.1. ', 0, 79, NULL, NULL),
(236, 'Diagnóstico de educación básica.', 'El municipio cuenta con un diagnóstico en materia deeducación básica.', 'B.2.2.2. ', 0, 79, NULL, NULL),
(237, 'Programa municipal de educación básica.', 'El municipio cuenta con un programa para promover la educación básica.', 'B.2.2.3. ', 0, 79, NULL, NULL),
(238, 'Coordinación para promover la educación básica en el municipio.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias para promover la educación básica.', 'B.2.2.4. ', 0, 79, NULL, NULL),
(239, 'Instancia responsable de promover la salud.', 'El municipio cuenta con una instancia que contribuye a promover la salud de su población.', 'B.2.3.1. ', 0, 80, NULL, NULL),
(240, 'Diagnóstico en materia de salud.', 'El municipio cuenta con un diagnóstico en materia de salud.', 'B.2.3.2. ', 0, 80, NULL, NULL),
(241, 'Programa municipal de salud.', 'El municipio cuenta con un programa enfocado a impulsar la atención y promoción de la salud.', 'B.2.3.3. ', 0, 80, NULL, NULL),
(242, 'Coordinación para garantizar el derecho a la protección de la salud.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias para garantizar el derecho a la protección de la salud.', 'B.2.3.4. ', 0, 80, NULL, NULL),
(243, 'Instancia responsable de promover la vivienda.', 'El municipio cuenta con una instancia de desarrollo social que entre sus responsabilidades tiene la de promover la adquisición, construcción o mejora de la vivienda.', 'B.2.4.1. ', 0, 81, NULL, NULL),
(244, 'Diagnóstico de la situación de la vivienda en el municipio.', 'El municipio cuenta con un diagnóstico sobre la situación de la vivienda en su territorio.', 'B.2.4.2. ', 0, 81, NULL, NULL),
(245, 'Programa municipal de vivienda.', 'El municipio cuenta con un programa enfocado a promover el acceso y mejora de la vivienda.', 'B.2.4.3. ', 0, 81, NULL, NULL),
(246, 'Coordinación enfocada a impulsar acciones para satisfacer la demanda de vivienda digna.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias, enfocado a impulsar acciones para satisfacer la demanda de vivienda digna.', 'B.2.4.4. ', 0, 81, NULL, NULL),
(247, 'Instancia responsable de la atención de grupos vulnerables.', 'El municipio cuenta con una instancia de desarrollo social, que entre sus responsabilidades tiene la de atender a grupos vulnerables.', 'B.2.5.1. ', 0, 82, NULL, NULL),
(248, 'Diagnóstico de grupos vulnerables.', 'El municipio cuenta con un diagnostico que identifica las condiciones de la población en situación de vulnerabilidad social.', 'B.2.5.2. ', 0, 82, NULL, NULL),
(249, 'Programa para la atención de grupos vulnerables.', 'El municipio cuenta con un programa enfocado a la atención de los grupos vulnerables.', 'B.2.5.3. ', 0, 82, NULL, NULL),
(250, 'Coordinación para la atención de grupos vulnerables.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias, para la atención de grupos vulnerables.', 'B.2.5.4. ', 0, 82, NULL, NULL),
(251, 'Marco normativo en materia de igualdad de género.', 'El municipio cuenta con disposiciones normativas en materia de igualdad de género.', 'B.2.6.1. ', 0, 83, NULL, NULL),
(252, 'Instancia responsable de la promoción de la igualdad de género.', 'El municipio cuenta con una instancia responsable de promover la igualdad de género en las políticas públicas municipales.', 'B.2.6.2. ', 0, 83, NULL, NULL),
(253, 'Diagnóstico sobre la igualdad de género.', 'El municipio cuenta con un diagnóstico sobre la igualdad de género.', 'B.2.6.3. ', 0, 83, NULL, NULL),
(254, 'Programa para la promoción de la igualdad de género.', 'El municipio cuenta con un programa enfocado a la promoción de la igualdad de género.', 'B.2.6.4. ', 0, 83, NULL, NULL),
(255, 'Coordinación para la promoción de la igualdad de género.', 'El municipio cuenta por lo menos con un convenio vigente con otra instancia, para la promoción de la igualdad de género en las políticas públicas municipales.', 'B.2.6.5. ', 0, 83, NULL, NULL),
(256, 'Instancia responsable de atención a la juventud.', 'El municipio cuenta con una instancia de desarrollo social responsable de atender a la juventud', 'B.2.7.1. ', 0, 84, NULL, NULL),
(257, 'Diagnóstico sobre juventud.', 'El municipio cuenta con un diagnóstico en materia de juventud.', 'B.2.7.2. ', 0, 84, NULL, NULL),
(258, 'Programa municipal de atención a la juventud.', 'El municipio cuenta con un programa enfocado a la atención de la juventud.', 'B.2.7.3. ', 0, 84, NULL, NULL),
(259, 'Coordinación para la atención de la juventud.', 'El municipio cuenta por lo menos con un convenio vigente enfocado a la atención de la juventud.', 'B.2.7.4. ', 0, 84, NULL, NULL),
(260, 'Instancia responsable de la promoción del deporte y la recreación.', 'El municipio cuenta con una instancia de desarrollo social responsable de promover el deporte y la recreación.', 'B.2.8.1. ', 0, 85, NULL, NULL),
(261, 'Diagnóstico sobre deporte y recreación', 'El municipio cuenta con un diagnóstico en materia de deporte y recreación.', 'B.2.8.2. ', 0, 85, NULL, NULL),
(262, 'Programa municipal de promoción del deporte y la recreación.', 'El municipio cuenta con un programa enfocado a la promoción del deporte y la recreación, para contribuir a mejorar la salud y la convivencia social', 'B.2.8.3. ', 0, 85, NULL, NULL),
(263, 'Coordinación para la promoción del deporte y la recreación.', 'El municipio cuenta por lo menos con un convenio vigente enfocado a la promoción del deporte y la recreación, para contribuir a mejorar la salud y la convivencia social.', 'B.2.8.4. ', 0, 85, NULL, NULL),
(264, 'Marco normativo en materia de patrimonio cultural.', 'El municipio cuenta con disposiciones normativas en materia de patrimonio cultural.', 'B.2.9.1. ', 0, 86, NULL, NULL),
(265, 'Instancia responsable del patrimonio cultural.', 'El municipio cuenta con una instancia de desarrollo social que entre sus responsabilidades tiene la de diseñar e instrumentar acciones para preservar y promover el patrimonio cultural.', 'B.2.9.2. ', 0, 86, NULL, NULL),
(266, 'Diagnóstico del patrimonio cultural.', 'El municipio cuenta con un diagnóstico en materia de patrimonio cultural.', 'B.2.9.3. ', 0, 86, NULL, NULL),
(267, 'Programa de preservación y promoción del patrimonio cultural.', 'El municipio cuenta con un programa para preservar y promover el patrimonio cultural.', 'B.2.9.4. ', 0, 86, NULL, NULL),
(268, 'Coordinación en materia de patrimonio cultural.', 'El municipio cuenta por lo menos con un convenio vigente con otras instancias para preservar y promover el patrimonio cultural.', 'B.2.9.5. ', 0, 86, NULL, NULL),
(269, 'Marco normativo para el cuidado del medio ambiente.', 'El municipio cuenta con disposiciones normativas en materia de medio ambiente.', 'B.3.1.1 ', 0, 87, NULL, NULL),
(270, 'Instancia responsable del cuidado del medio ambiente.', 'El municipio cuenta con una instancia responsable del cuidado del medio ambiente.', 'B.3.1.2. ', 0, 87, NULL, NULL),
(271, 'Diagnóstico del medio ambiente.', 'El municipio cuenta con un diagnóstico en materia de medio ambiente, que incluye información de sus recursos naturales (aire, agua, suelo, flora y fauna) y de las fuentes de energía sustentable.', 'B.3.1.3. ', 0, 87, NULL, NULL),
(272, 'Programa para el cuidado del medio ambiente.', 'El municipio cuenta con un programa para el cuidado de medio ambiente, enfocado a promover el aprovechamiento sustentable de la energía, así como la preservación o restauración de sus recursos naturales: aire, agua, suelo, flora y fauna.', 'B.3.1.4. ', 0, 87, NULL, NULL),
(273, 'Coordinación para el cuidado del medio ambiente.', 'El municipio cuenta por lo menos con un convenio vigente con una instancia para promover el cuidado del medio ambiente, así como para la protección y la preservación del aire, agua, suelo, flora y fauna y el aprovechamiento sustentable de la energía.', 'B.3.1.5. ', 0, 87, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inf_indicadores_rechazados`
--

CREATE TABLE `inf_indicadores_rechazados` (
  `id` int(11) NOT NULL,
  `descripcion` text,
  `original_nombre` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `inf_indicador_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inf_indicador_has_accion_poa`
--

CREATE TABLE `inf_indicador_has_accion_poa` (
  `inf_indicadores_id` int(11) NOT NULL,
  `acciones_poa_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inf_indicador_has_direcciones`
--

CREATE TABLE `inf_indicador_has_direcciones` (
  `inf_indicador_id` int(11) NOT NULL,
  `direccion_id` int(11) NOT NULL,
  `puesto_id` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL COMMENT '0 = Sin Asignar\n\n1 = Acptada\n\n2 = Rechazada'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inf_indicador_has_direcciones`
--

INSERT INTO `inf_indicador_has_direcciones` (`inf_indicador_id`, `direccion_id`, `puesto_id`, `status`) VALUES
(133, 1, NULL, NULL),
(135, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inf_temas`
--

CREATE TABLE `inf_temas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `objetivo` text,
  `punto` varchar(20) DEFAULT NULL,
  `inf_eje_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `inf_temas`
--

INSERT INTO `inf_temas` (`id`, `nombre`, `objetivo`, `punto`, `inf_eje_id`, `created_at`, `updated_at`) VALUES
(1, 'Planeación Urbana', 'Regular los usos y aprovechamientos del suelo en los centros de población del municipio, con el fin de utilizar y aprovechar el territorio de manera ordenada y sustentable.', 'A.1.1', 1, NULL, NULL),
(2, 'Ordenamiento Ecológico', 'Regular o inducir los usos y aprovechamientos del suelo con el fin de lograr la protección, preservación y aprovechamiento sustentable de los recursos naturales.', 'A.1.2', 1, NULL, NULL),
(3, 'Protección Civil', 'Disminuir, tendiente a erradicar, los asentamientos humanos en zonas de riesgo, así como proteger,\r\nasistir y prevenir a la población en caso de una contingencia o desastre natural.', 'A.1.3 ', 1, NULL, NULL),
(26, 'Calles', 'Abatir el déficit de arterias viales y mantener en condiciones óptimas las arterias existentes en el sistema vial, para impulsar la movilidad y comunicación terrestre de la población.', 'A.2.1 ', 2, NULL, NULL),
(27, 'Agua potable', 'Abatir el déficit en el servicio de agua potable en viviendas particulares.', 'A.2.2 ', 2, NULL, NULL),
(28, 'Drenaje y alcantarillado', 'Abatir el déficit en el servicio de drenaje en viviendas particulares y alcantarillado en arterias viales para la conducción de aguas residuales y pluviales.', 'A.2.3 ', 2, NULL, NULL),
(29, 'Aguas residuales', 'Garantizar la concentración y tratamiento de las aguas residuales para su debida utilización.', 'A.2.4 ', 2, NULL, NULL),
(30, 'Limpia', 'Garantizar la cobertura y continuidad del servicio de limpia con el fin de mantener vialidades y espacios públicos libres de residuos.', 'A.2.5', 2, NULL, NULL),
(31, 'Residuos sólidos (recolección, traslado, tratamiento y disposición final)', 'Abatir el déficit en la prestación del servicio de recolección de los residuos sólidos, así como garantizar el traslado, tratamiento y disposición final de los mismos con apego a la normatividad.', 'A.2.6 ', 2, NULL, NULL),
(32, 'Parques y jardines\r\n', 'Abatir el déficit y dar mantenimiento adecuado a los espacios públicos destinados a la convivencia social y a la recreación.', 'A.2.7 ', 2, NULL, NULL),
(33, 'Alumbrado público', 'Abatir el déficit y dar mantenimiento adecuado a la red de alumbrado público.', 'A.2.8 ', 2, NULL, NULL),
(34, 'Mercados públicos', 'Abatir el déficit y dar mantenimiento adecuado a los espacios públicos destinados al abasto de artículos básicos.', 'A.2.9 ', 2, NULL, NULL),
(35, 'Panteones', 'Abatir el déficit y dar mantenimiento adecuado a los espacios públicos destinados a restos humanos.', 'A.2.10 ', 2, NULL, NULL),
(36, 'Rastro', 'Fomentar que el mayor número de sacrificios de ganado en el municipio se realice en rastros, en condiciones de sanidad e higiene.', 'A.2.11 ', 2, NULL, NULL),
(37, 'Prevención Social de la Violencia y la Delincuencia', 'Reducir la vulnerabilidad a la violencia y a la delincuencia de los grupos poblacionales másexpuestos, atendiendo los factores de riesgo y fortaleciendo la protección, la sensibilización,el reconocimiento y la educación para identificar situaciones de violencia y formas de enfrentarla.', 'A.3.1', 3, NULL, NULL),
(38, 'Policía Preventiva', 'Contar con un cuerpo profesional de policía para la prevención del delito, acorde al tamaño poblacional.', 'A.3.2', 3, NULL, NULL),
(39, 'Seguridad Pública', 'Abatir la incidencia de delitos del fuero común en el municipio, en forma coordinada con el estado y la federación.', 'A.3.3', 3, NULL, NULL),
(40, 'Tránsito', 'Reducir la siniestralidad de tránsito en el municipio, mediante un adecuado funcionamiento de las arterias viales y del flujo vehicular.', 'A.3.4', 3, NULL, NULL),
(50, 'Organización', 'Redimensionar la estructura organizacional hasta niveles óptimos del número de dependencias, personal y tabuladores salariales adecuados a las necesidades de la función pública.', 'A.4.1', 4, NULL, NULL),
(51, 'Planeación y Control Interno', 'Contar con un instrumento de planeación y procesos que promuevan la consecución de las metas establecidas, que respalde a las autoridades municipales en la toma de decisiones encaminadas a lograr los objetivos institucionales.', 'A.4.2. ', 4, NULL, NULL),
(52, 'Capacitación', 'Impulsar el desarrollo de las capacidades y habilidades del personal de la administración pública municipal.', 'A.4.3', 4, NULL, NULL),
(53, 'Tecnologías de la Información', 'Impulsar el uso de las tecnologías y la comunicación (TIC´s) en el desempeño institucional de la Administración Pública Municipal, así como en la realización de trámites y servicios ofrecidos a la población.', 'A.4.4', 4, NULL, NULL),
(54, 'Transparencia y Acceso a la Información Pública', 'Garantizar la transparencia y el acceso a la información pública para la ciudadanía.', 'A.4.5', 4, NULL, NULL),
(55, 'Armonización Contable', 'Garantizar que el municipio cumpla con los lineamientos en materia de contabilidad gubernamental y emisión de información financiera, para una adecuada rendición de cuentas a la ciudadanía.', 'A.4.6', 4, NULL, NULL),
(56, 'Ingresos', 'Incentivar el manejo sostenible de las finanzas públicas municipales, impulsando las bases para el logro de balances presupuestarios sostenibles, deudas sostenibles y el uso eciente de los recursos públicos.', 'A.4.7', 4, NULL, NULL),
(57, 'Egresos', 'Promover un ejercicio del gasto público responsable, eficaz, eficiente y transparente que promueva condiciones de bienestar para la población.', 'A.4.8. ', 4, NULL, NULL),
(58, 'Deuda', 'Minimizar el peso de la deuda pública en los ingresos municipales.', 'A.4.9. ', 4, NULL, NULL),
(59, 'Empleo', 'Incrementar el empleo formal en el municipio a través de la coordinación con el estado y la federación en la creación y aprovechamiento de las fuentes de trabajo', 'B.1.1', 5, NULL, NULL),
(60, 'Transporte Público', 'Mejorar el transporte público al interior del municipio mediante una efectiva coordinación con el gobierno del estado.', 'B.1.2', 5, NULL, NULL),
(61, 'Conectividad', 'Contribuir a la reducción de la brecha digital, mediante la provisión de acceso a internet en los sitios y espacios públicos existentes en el municipio', 'B.1.3', 5, NULL, NULL),
(62, 'Comercio y Servicios', 'Atraer y retener inversión en el sector comercial y de servicios en el municipio, mediante programas municipales de mejora regulatoria, ordenamiento y promoción comercial y de servicios locales, en coordinación con los distintos órdenes de gobierno.', 'B.1.4', 5, NULL, NULL),
(63, 'Industria', 'Atraer y retener inversión en el sector industrial en el municipio, mediante programas municipales de ordenamiento y promoción, en coordinación con los distintos órdenes de gobierno.', 'B.1.5', 5, NULL, NULL),
(64, 'Agricultura', 'Atraer y retener inversión para agricultura, mediante programas municipales de productividad, aprovechamiento sustentable y promoción comercial de productos locales, en coordinación con los distintos órdenes de gobierno.', 'B.1.6', 5, NULL, NULL),
(65, 'Ganadería', 'Atraer y retener inversión para ganadería, mediante programas municipales de productividad, aprovechamiento sustentable y promoción comercial de productos locales, en coordinación con los distintos órdenes de gobierno.', 'B.1.7', 5, NULL, NULL),
(66, 'Pesca', 'Atraer y retener inversión para pesca, mediante programas municipales de productividad, aprovechamiento sustentable y promoción comercial de productos locales, en coordinación con los distintos órdenes de gobierno.', 'B.1.8', 5, NULL, NULL),
(67, 'Turismo', 'Incrementar la actividad turística en el municipio mediante programas de promoción y aprovechamiento sustentable de sus atractivos turísticos.', 'B.1.9. ', 5, NULL, NULL),
(68, 'Forestal', 'Atraer y retener inversión para la conservación, protección, restauración, producción,ordenación, el cultivo, manejo y aprovechamiento de los ecosistemas forestales en el municipio, en coordinación con los distintos órdenes de gobierno.', 'B.1.10', 5, NULL, NULL),
(78, 'Pobreza', 'Contribuir a disminuir la pobreza mediante el financiamiento de servicios públicos, obras, acciones e inversiones que beneficien directamente a la población en esa condición, mediante la colaboración en programas federales y estatales de desarrollo social y comunitario.', 'B.2.1. ', 6, NULL, NULL),
(79, 'Educación', 'Contribuir a elevar la calidad y cobertura de la educación básica en el municipio, en coordinación con otros órdenes de gobierno.', 'B.2.2', 6, NULL, NULL),
(80, 'Salud', 'Garantizar el derecho a la protección de la salud mediante una mayor inversión en infraestructura básica y en acciones de promoción de la salud', 'B.2.3', 6, NULL, NULL),
(81, 'Vivienda', 'Satisfacer la demanda de vivienda digna de la población municipal, impulsando los desarrollos habitacionales de interés social, programas de mejoramiento de la vivienda y lotes con servicios, en coordinación con las autoridades estatales y federales competentes en la materia.', 'B.2.4', 6, NULL, NULL),
(82, 'Grupos Vulnerables', 'Contribuir al mejoramiento de las condiciones de vida de la población en situación de vulnerabilidad social y propiciar la equidad en el acceso a las oportunidades de desarrollo.', 'B.2.5', 6, NULL, NULL),
(83, 'Igualdad de Género', 'Promover la igualdad de género como estrategia transversal en las políticas públicas municipales, para contribuir al acceso equitativo de oportunidades de desarrollo.', 'B.2.6', 6, NULL, NULL),
(84, 'Juventud', 'Impulsar la implementación de programas y acciones para la atención de las necesidades especificas de la población joven del municipio.', 'B.2.7', 6, NULL, NULL),
(85, 'Deporte y Recreación', 'Impulsar la implementación de programas y acciones para la creación de espacios públicos destinados a actividades físicas y lúdicas.', 'B.2.8', 6, NULL, NULL),
(86, 'Patrimonio cultural', 'Preservar el patrimonio cultural del municipio y realizar acciones de promoción de la cultura.', 'B.2.9', 6, NULL, NULL),
(87, 'Medio Ambiente', 'Promover el aprovechamiento sustentable de la energía y la preservación o, en su caso, la restauración de los recursos naturales (aire, agua, suelo, flora y fauna) a cargo del municipio, a fin de garantizar, en concurrencia con los otros órdenes de gobierno, un medio ambiente sano.', 'B.3.1', 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `icono` varchar(255) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `alias` varchar(100) DEFAULT NULL,
  `padre` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menus`
--

INSERT INTO `menus` (`id`, `nombre`, `url`, `icono`, `posicion`, `alias`, `padre`, `created_at`, `updated_at`) VALUES
(1, 'Usuarios', NULL, 'fas fa-users', 4, 'auth', NULL, '2018-09-19 16:05:08', '2018-10-17 15:51:27'),
(2, 'Usuarios', 'users', NULL, 1, 'users', 1, '2018-09-19 16:05:08', '2018-09-19 16:05:08'),
(4, 'Roles', 'roles', NULL, 2, 'roles', 1, '2018-09-19 16:06:17', '2018-09-19 16:06:17'),
(5, 'Panel de Control', 'dashboard', 'fas fa-tachometer-alt', 1, 'dashboard', NULL, '2018-09-19 16:06:41', '2018-09-19 16:06:41'),
(6, 'Navegación', NULL, 'fab fa-elementor', 3, 'nav', NULL, '2018-09-19 16:07:58', '2018-09-19 16:07:58'),
(7, 'Menu', 'menus', NULL, 1, 'menus', 6, '2018-09-19 16:07:58', '2018-09-19 16:07:58'),
(8, 'Soporte', 'support', 'fas fa-life-ring', 2, 'support', NULL, '2018-09-19 22:11:24', '2018-09-19 22:11:24'),
(9, 'Planeación', NULL, 'fas fa-archive', 3, 'planning', NULL, '2018-09-20 18:18:29', '2018-09-20 18:18:29'),
(10, 'Plan Municipal', 'municipal.plan', NULL, 1, 'municipal_plan', 9, '2018-09-20 18:18:29', '2018-09-20 18:18:29'),
(11, 'Mis lineas de acción asignadas', 'municipal.plan.client', NULL, 3, 'municipal_plan_client', 9, '2018-10-11 17:24:01', '2018-10-17 15:53:23'),
(12, 'Crear POA', 'make.poa', NULL, 4, 'make_poa', 9, '2018-10-14 16:43:14', '2018-10-17 15:53:23'),
(13, 'INAFED', 'inafed', NULL, 2, 'inafed', 9, '2018-10-17 15:53:24', '2018-10-17 15:53:24'),
(14, 'Agenda 2030', 'schedule', NULL, 2, 'schedule', 9, '2018-11-09 19:34:55', '2018-11-09 19:34:55'),
(15, 'Configuración', NULL, 'fas fa-cogs', 10, 'config', NULL, '2018-11-12 15:34:59', '2018-11-12 15:34:59'),
(16, 'Direccion', 'metadata.direction', NULL, 1, 'metadata_direction', 15, '2018-11-12 15:34:59', '2018-11-12 15:34:59'),
(17, 'Reportes', NULL, 'fas fa-file-invoice', 3, 'reports', NULL, '2018-11-12 16:43:39', '2018-11-12 16:43:39'),
(18, 'Generar Poa', 'generate.poa', NULL, 1, 'generate_poa', 17, '2018-11-12 16:43:39', '2018-11-12 16:43:39'),
(19, 'MIR', 'mir', NULL, 4, 'mir', 9, '2018-11-14 16:26:10', '2018-11-14 16:26:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metadata_direction`
--

CREATE TABLE `metadata_direction` (
  `id` int(11) NOT NULL,
  `meta_llave` varchar(30) DEFAULT NULL,
  `valor` text,
  `direccion_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `metadata_direction`
--

INSERT INTO `metadata_direction` (`id`, `meta_llave`, `valor`, `direccion_id`, `created_at`, `updated_at`) VALUES
(1, 'presupuesto', '15,000,000', 1, '2018-11-21 15:43:15', '2018-11-21 15:43:15'),
(2, 'descripcion', 'La atención a las necesidades del municipio y sus habitantes, que mejoren la calidad de vida, permitan el desarrollo socio-cultural y económico en armonía con la preservación del medio ambiente.\r\nLa construcción y conducción de un gobierno ciudadano, en el que la planeación operativa del gobierno; permita que previo acuerdo del cuerpo edilicio, que genere una reorganización de la administración pública municipal, para mejorar la prestación de servicios públicos, disfrutar de una ciudad segura e impulse el desarrollo económico y social del Municipio.', 1, '2018-11-21 15:43:15', '2018-11-21 15:43:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mir_idicadores_rechazados`
--

CREATE TABLE `mir_idicadores_rechazados` (
  `id` int(11) NOT NULL,
  `descripcion` text,
  `original_nombre` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `mir_indicador_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mir_indicadores`
--

CREATE TABLE `mir_indicadores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(250) DEFAULT NULL,
  `objetivo` text,
  `formula` text,
  `mir_tipo_indicador_id` int(11) NOT NULL,
  `mir_indicador_nivel_id` int(11) NOT NULL,
  `punto` varchar(30) DEFAULT NULL,
  `mir_programa_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mir_indicadores`
--

INSERT INTO `mir_indicadores` (`id`, `nombre`, `objetivo`, `formula`, `mir_tipo_indicador_id`, `mir_indicador_nivel_id`, `punto`, `mir_programa_id`, `created_at`, `updated_at`) VALUES
(1, 'Variación porcentual en la disminución de la delincuencia\r\n', 'Contribuir a la disminución de la delincuencia y  percepción de  inseguridad mediante acciones de seguridad  pública y movilidad ágil y segura\r\n', '((Disminución de los actos delictivos en el año actual / Disminución de los actos delictivos en el año anterior)-1)*100\r\n', 1, 1, '1', 1, NULL, NULL),
(2, 'Variación porcentual en la disminución de la inseguridad pública\r\n', 'La población se beneficia con la disminución de la inseguridad pública.\r\n', '((Disminución de denuncias públicas en el año actual / Disminución de denuncias públicas en el año anterior)-)*100\r\n', 1, 2, '2', 1, NULL, NULL),
(3, 'Inversión en programa de prevención por cada mil habitantes\r\n', 'Fortalecimiento de la política de prevención del delito con medios propios y de  otras instituciones de seguridad pública\r\n', 'IPP/ (PT/1000). IPP= Inversión en programas de prevención, PT= Población total en el municipio\r\n', 1, 3, '3', 1, NULL, NULL),
(4, 'Acciones de prevención en comunidad, ciudadanía en general\r\n', 'Generación de una política de prevención que atienda a la población en general\r\n', '(APR/APP)*100. APR= Actividades de prevención en comunidad realizadas, APP= Acciones de prevención en comunidad programadas\r\n', 1, 4, '4', 1, NULL, NULL),
(5, 'Accidentes viales por cada 1000 vehículos\r\n', 'Reducción de los accidentes en las vialidades\r\n', 'AV/ (TV/1000). AV= Accidentes viales, TV=Total de vehículos registrados en el municipio en el que se distinga vehiculos accidentados foráneos y locales /Sexo conductor / Evaluar formula para número de accidentes / accidentes base\r\n', 1, 3, '5', 1, NULL, NULL),
(6, 'Campañas de difusión de normas viales\r\n', 'Conocimiento de las normas viales por parte de la población xele', '(Boletines informativos de las normas viales distribuídos / Boletines informativos de las normas viales programados)*100 spots, medios impresos(tiraje); trípticos\r\n', 1, 4, '6', 1, NULL, NULL),
(7, 'Tasa de crecimiento anual del índice de policías operativos por cada 1,000 habitantes.\r\n', 'Los estándares internacionales sugieren que con más operativos en la policía preventiva, los delitos disminuirán por lo que es necesario cuidar la relación de policías con respecto a la población\r\n', '((Número de policías operativos por cada 1000 habitantes en el año evaluado - Número de policías operativos por cada 1000 habitantes en el año previo al evaluado) / Número de policías operativos por cada 1,000 habitantes en el año previo al evaluado) * 100\r\n', 1, 1, '1', 2, NULL, NULL),
(8, 'Proporción en aumento de monto de inversión en actualización y mejora de tecnológicos, informáticos,analíticos, métodos y sistemas\r\n', 'La automatización de proceso en la Dirección de seguridad y tránsito brindará la posibilidad de mejora en todos sus servicios\r\n', '(ITE/TPS)*100. ITE=Inversión total  en recursos tecnológicos, informáticos,analíticos, métodos  y sistemas\r\n', 1, 2, '2', 2, NULL, NULL),
(9, 'Quejas contra la corporación de seguridad y tránsito respecto del total de quejas contra el gobierno municipal.\r\n', 'El sentido de pertenencia entre el personal de seguridad pública y tránsito es incentivado.\r\n', 'TQS/TQG. TQS= Total de quejas contra la corporación de seguridad y tránsito,TQG= Total de quejas contra el gobierno municipal y su administración pública.\r\n', 1, 3, '3', 1, NULL, NULL),
(10, 'Apoyos familiares e incentivos para los agentes de seguridad y tránsito.\r\n', 'Apoyos familiares e incentivos por buen desempeño para el agente de seguridad y tránsito.\r\n', '(AFI/TP)*100. Apoyos familiares e incentivos para agentes de seguridad y tránsito entregados o gestionados, TP= Total de elementos de seguridad y tránsito.\r\n', 1, 4, '4', 2, NULL, NULL),
(11, 'Variación porcentual de queja por violación a los derechos humanos.\r\n', 'Contribuir a asegurar una vida digna mediante las quejas por violación a los derechos humanos.\r\n', '((Quejas por violación a los derechos humanos presentadas en el año actual / Quejas por violación a los derechos humanos presentadas en el año anterior)-1)*100\r\n', 1, 1, '1', 3, NULL, NULL),
(12, 'Variación porcentual de personas protegidas por violación a los derechos humanos.\r\n', 'La población municipal, esta protegida de acciones u omisiones violatorias de derechos humanos.\r\n', '((Número de personas atendidas por violación a los derechos humanos en el año actual / Número de personas atendidas por violación a los derechos humanos en el año anterior)-)*100\r\n\r\n', 1, 2, '2', 3, NULL, NULL),
(13, 'Cobertura de capacitaciones en materia de derechos humanos.\r\n', 'Capacitar a la población en general en materia de derechos humanos y respecto a la dignidad humana.\r\n', '(Número de personas asistentes a las capacitaciones / Población municipal)*100\r\n', 1, 3, '3', 3, NULL, NULL),
(14, 'Porcentaje de campañas de información.\r\n', 'Campañas de información en materia de derechos humanos.', '(Campañas de información realizadas / Campañas de información programadas)*100\r\n', 1, 4, '4', 3, NULL, NULL),
(15, 'Variación porcentual en la cultura de la protección civil.\r\n', 'Contribuir a la cultura de la protección civil mediante la prevención ante la ocurrencia de fenómenos antropogénicos y propios de la naturaleza.\r\n', '((Eventos de promoción de la cultura de protección civil realizadas en el año actual/eventos de promoción de protección civil realizados en el año anterior)-1)*100\r\n', 1, 1, '1', 4, NULL, NULL),
(16, 'Variación porcentual en la promoción de la organización de la ciudadania en materia de protección civil.\r\n', 'Contribuir a la cultura de la protección civil mediante la prevención ante la ocurrencia de fenómenos antropogénicos y propios de la naturaleza.\r\nPromover la organización de la ciudadania en materia de protección civil para enfrentar la ocurrencia de fenómenos perturbadores.\r\n', '((Brigadas de protección civil conformadas en el año actual/Brigadas de protección civil conformadas en el año anterior)-1)*100\r\n', 1, 2, '2', 4, NULL, NULL),
(17, 'Porcentaje en la población capacitada en la prevención de riesgos \r\n', 'Población capacitada en la prevención de riesgos. \r\n', '(Eventos de capacitación dirigidos a la ciudadanía en materia de protección civil realizados/Eventos de capacitación dirigidos a la ciudadanía en materia de protección civil programados)*100\r\n', 1, 2, '3', 4, NULL, NULL),
(18, 'Variación porcentual en la planeación urbana y territorial.\r\n', 'Contribuir al fortalecimiento de la política territorial a través de actividades de incorporación ordenada y planificada del suelo al desarrollo urbano.\r\n', '((Proyectos de planeación urbana y territorial concluidos en el año actual/ Proyectos de planeación urbana y territorial concluidos en el año anterior)-1)*100 \r\n', 1, 1, '1', 5, NULL, NULL),
(19, 'Variación porcentual en la política territorial municipal.\r\n', 'La planeación de la política territorial municipal cumple con el objeto del Sistema de Planeación Democrática\r\n', '((Sesiones de planeación para la política territorial municipal efectuadas en el año actual/ Sesiones de planeación para la política territorial municipal efectuadas en el año anterior)-1)*100 \r\n', 1, 2, '2', 5, NULL, NULL),
(20, 'Porcentaje de Procesos Terminado \r\n', 'Regular con eficiencia la dinámica urbana mediante la mejora de las oportunidades para la regularización\r\n', '(Número de Regularizaciones Realizadas/ Número de Regularizaciones Programados)*100\r\n', 1, 3, '3', 5, NULL, NULL),
(21, 'Pocentaje en los barridos de campo, para identificar asentamientos irregulares.\r\n', 'Barridos de campo para identificar asentamiento irregulares.\r\n', '(Barridos de campo realizados para identificar asentamientos humanos irregulares / Barridos de campo programados para identificar asentamientos humanos irregulares)*100\r\n', 1, 4, '4', 5, NULL, NULL),
(22, 'Porcentaje en las jornadas de educación ambiental\r\n', 'Jornadas de educación y cultura ambiental desarrolladas.\r\n', '(Jornadas de educación ambiental impartidas/Jornadas de educación ambiental programadas)*100\r\n', 1, 3, '5', 5, NULL, NULL),
(23, 'Porcentaje en los cursos y talleres de educación ambiental realizados\r\n', 'Realización de cursos y talleres de educación ambiental\r\n', '(Cursos y talleres de educación ambiental realizados/ cursos y talleres de educación ambiental programados)*100\r\n', 1, 4, '6', 5, NULL, NULL),
(24, 'Porcentaje de dormitorios\r\n', 'Dormitorio (pieza) a grupos vulnerables con carencia de calidad y espacios de la vivienda entregados\r\n', '(Dormitorios entregados / Dormitorios proramados)*100\r\n', 1, 3, '7', 5, NULL, NULL),
(25, 'Porcentaje de construcción de pisos firmes.\r\n', 'Construcción de pisos firmes a grupos vulnerables con carencia de calidad y espacios de la vivienda entregados.\r\n', '(Construcción de pisos firmes entregados / Construcción de pisos firmes programados)*100\r\n', 1, 4, '8', 5, NULL, NULL),
(26, 'Variación porcentual en el manejo sustentable del agua potable.\r\n', 'Contribuir al manejo sustentable del agua potable mediante la conservación de las fuentes de abastecimiento.\r\n', '((Manejo sustentable del agua potable en el año actual / Manejo sustentable del agua potable en el año anterior)-1)*100\r\n', 1, 1, '1', 6, NULL, NULL),
(27, 'Variación porcentual en los estandares de calidad en el agua potable. Cumplimiento de la norma en la producción de agua potable.', 'Los estándares de calidad en el agua potable  se cumplen para suministrar a la población.', '((Estandares de calidad en el agua potable en el año actual / Estandares de calidad en el agua potable en el año anterior)-1*100. (Horas de servicio continuo año actual / Horas de servicio continuo año anterior)*100\r\n\r\n\r\n((Resultados de los estudios de laboratorio externo de la principal cuenca de suministro de agua potable en el año actual / Resultados de los estudios de laboratorio externo del principal cuenca suministro de agua potable en el año anterior)-*100. (Número de pruebas con resultados satisfactorios / Número de pruebas realizadas)\r\n', 1, 2, '2', 6, NULL, NULL),
(28, 'Porcentaje en distribución de agua potable', 'Distribución de agua potable para el suministro de la población abastecida.\r\n', '(Mts3 distribuidos de agua potable/Población beneficiada)*100\r\n', 1, 3, '3', 6, NULL, NULL),
(29, 'Porcentaje en el abastecimiento de agua potable a la población.\r\n', 'Suministro de agua potable a la población abastecido.\r\n', '(Suministro de agua potable a la población / Suministro de agua potable solicitada)*100\r\n', 1, 4, '4', 6, NULL, NULL),
(30, 'Variación porcentual de infraestructura urbana\r\n', 'Contribuir al desarrollo del ordenamiento territorial del municipio mediante la infraestructura urbana para mejorar la calidad de vida de los habitantes.\r\n', '((Infraestructura urbana desarrollada en el año actual/ Infraestructura urbana desarrollada en el año anterior)-1)*100\r\n', 1, 1, '1', 7, NULL, NULL),
(31, '\"Variación porcentual de\r\nmantenimientos o ampliación a la infraestructura urbana\r\n\"\r\n', 'La administración pública municipal realiza acciones de mantenimiento y/o ampliación a la infraestructura urbana.\r\n', '((Mantenimientos o ampliación a la infraestructura urbana en el año actual/ Mantenimientos o ampliación a la infraestructura urbana en el año anterior)-1)*100\r\n', 1, 2, '2', 7, NULL, NULL),
(32, 'Porcentaje de jornadas para recolección\r\n', 'Programación de la recolección de residuos sólidos urbanos realizada\r\n', '(Faenas para la recolección de residuos sólidos\r\nurbanos/Faenas programadas para la recolección\r\nde residuos sólidos urbanos)*100', 1, 3, '3', 7, NULL, NULL),
(33, 'Porcentaje de las rutas de recolección de residuos sólidos urbanos municipales\r\n', 'Mapeo de las rutas de recolección de residuos sólidos urbanos municipales.\r\n', '(Rutas de recolección de residuos sólidos urbanos municipales realizadas/rutas de recolección de residuos sólidos urbanos municipales programados)*100\r\n', 1, 4, '4', 7, NULL, NULL),
(34, '\"Porcentaje de\r\ninstalación e Insumos para el equipamiento de la infraestructura de alumbrado público, realizado.\r\n\"', 'Insumos e instalación para el equipamiento de la infraestructura de lumbrado público, realizado.\r\n', '(Insumos e instalación del equipamiento de la infraestructura de alumbrado realizado /mantenimiento del equipamiento de la infraestructura de alumbrado programado)*100.\r\n', 1, 3, '5', 7, NULL, NULL),
(35, '\"Porcentaje de inspecciones\r\nfísicas de las\r\nobras de electrificación\r\n\"\r\n', 'Registro de inspecciones fisicas para control y evaluación de las obras de electrificación para beneficio d ela población municipal.\r\n', '\"(Inspecciones físicas de las obras de electrificación realizadas / Inspecciones físicas de las obras de\r\nElectrificación programadas)*100\r\n\"\r\n', 1, 4, '6', 7, NULL, NULL),
(36, '\"Porcentaje de vialidades,\r\nguarniciones y\r\nbanquetas\r\n\"\r\n', 'Mantenimiento de vialidades, banquetas y guarniciones del municipio.\r\n', '\"(Vialidades, guarniciones y banquetas rehabilitadas\r\n/Vialidades, guarniciones y banquetas programadas) * 100\r\n\"\r\n', 1, 3, '7', 7, NULL, NULL),
(37, '\"Limpieza de imagen\r\nurbana (grafiti)\r\n\r\n\r\n\"\r\n', 'Limpieza de imagen urbana (Grafiti)\r\n', '(Metros cuadrados realizados / Metros cuadrados Programados)* 100\r\n', 1, 4, '8', 7, NULL, NULL),
(38, 'Variación porcentual de infraestructura urbana\r\n', 'Contribuir al desarrollo del Ordenamiento Territorial del Municipio mediante la infaestructura urbana para mejorar la calidad de vida de los habitantes.\r\n', '((Infraestructura urbana desarrollada en el año actual/ Infraestructura urbana desarrollada en el año anterior)-1)*100\r\n', 1, 1, '1', 8, NULL, NULL),
(39, '\"Variación porcentual de\r\nmantenimientos o ampliación a la infraestructura urbana\r\n\"\r\n', 'La Administración Pública Municipal realiza acciones de mantenimiento y/o ampliación a la infraestructura urbana.\r\n', '((Mantenimientos o ampliación a la infraestructura urbana en el año actual/ Mantenimientos o ampliación a la infraestructura urbana en el año anterior)-1)*100\r\n', 1, 2, '2', 8, NULL, NULL),
(40, 'Porcentaje de Pavimentación de calles.\r\n', 'Pavimentación de calles.\r\n', 'Porcentaje de Pavimentación de calles.\r\n', 1, 3, '3', 8, NULL, NULL),
(41, 'Porcentaje de banquetas y guarniciones rehabilitadas.\r\n', 'Rehabilitación de banquetas y guarniciones.\r\n', 'Porcentaje de banquetas y guarniciones rehabilitadas.\r\n', 1, 4, '4', 8, NULL, NULL),
(42, 'Variación porcentual de infraestructura urbana en las zonas de atención prioritaria \r\n', 'El gobierno federal provee recursos para el desarrollo de la infraestructura;  en zonas de alta priridad \r\n', '((Infraestructura urbana en las zonas de atención prioritaria desarrollada en el año actual/ Infraestructura urbana en las zonas de atención prioritaria desarrollada en el año anterior)-1)*100\r\n', 1, 1, '1', 9, NULL, NULL),
(43, '\"Variación porcentual de\r\nmantenimientos o ampliación a la infraestructura urbana en las zonas de atención prioritaria.\r\n\"\r\n', 'Se amplía las obras de mantenimiento en xonas de amplia prioridad\r\n', '((Mantenimientos o ampliación a la infraestructura urbana en las zonas de atención prioritaria en el año actual/ Mantenimientos o ampliación a la infraestructura urbana  en las zonas de atención prioritaria en el año anterior)-1)*100\r\n', 1, 2, '2', 9, NULL, NULL),
(44, 'Porcentaje en los niveles de infraestructura para el tratamiento de aguas residuales.\r\n', 'Infraestructura para el tratamiento de aguas residuales construida.\r\n', '(infraestructura para el tratamiento de aguas residuales construida/infraestructura para el tratamiento de aguas residuales programada)*100\r\n', 1, 3, '3', 9, NULL, NULL),
(45, 'Porcentaje en la inspección de la infraestructura para el tratamiento de aguas residuales.\r\n', 'Inspección de l infraestructura para el tratamiento de aguas residuales.\r\n', '(Inspección de l infraestructura para el tratamiento de aguas residuales realizadas/Inspección de l infraestructura para el tratamiento de aguas residuales programadas)*100\r\n', 1, 4, '4', 9, NULL, NULL),
(46, 'Variación porcentual de la derrama económica turística en el municipio\r\n', 'Contribuir al fortalecimiento del desarrollo económico del municipio a través de esquemas de colaboración y corresponsabilidad en materia de inversión destinada al aprovechamiento del potencial turístico y de la oferta de productos turísticos competitivos.\r\n', '((Ingresos de carácter turístico en el año actual/ Ingresos de carácter turístico en el año anterior)-1)*100\r\n', 1, 1, '1', 10, NULL, NULL),
(47, 'Variación porcentual en la infraestructura turística municipal\r\n', 'Las undades económicas turísticas municipales cuentan con esquemas de colaboración y corresponsabilidad que dinamizan y optimizan la oferta turística.\r\n', '((Unidades económicas de carácter turístico en funciones en el presente año/ Unidades económicas de carácter turístico en funciones en el año anterior\r\n', 1, 2, '2', 10, NULL, NULL),
(48, 'Porcentaje de Mipymes beneficiadas\r\n', 'Gestión de apoyos federales económicos y/o en especie de Mi pymes\r\n', 'Mipymes beneficiadas/Mipymes vinculadas \r\n', 1, 3, '3', 10, NULL, NULL),
(49, 'Porcentaje de Mipymes vinculadas \r\n', 'Vincular a las Mi pymes a los apoyos Federales.\r\n', '\"(Vinculación de Mipymes realizado /Vinculación de Mipymes programado)x100\r\n\"\r\n', 1, 4, '4', 10, NULL, NULL),
(50, '\"Empresas que se\r\nregistran por primera\r\nvez\r\n\"\r\n', 'Mecanismo para dar de alta a las empresas que ofertan sus vacantes y en todo momento fomentar la capacitación.\r\n', '\"(Total de Empresas nuevas registradas/Total de empresas nuevas programadas)*100\r\n\"\r\n', 1, 3, '5', 10, NULL, NULL),
(51, 'Solicitantes contratados\r\n', 'Personas enviadas contratadas.\r\n', '\"(Total de solicitantes enviados contratados/ Total\r\nde solicitantes registrados enviados) *100\r\n\"\r\n', 1, 4, '6', 10, NULL, NULL),
(52, '\"Porcentaje de Apoyos\r\nProgramados\r\n\"\r\n', 'Promocionar en comunidades y ejidos los programas y apoyos existentes por parte de la Federación, Estado y Muncipio dirigidos al sector.\r\n', '(Total de apoyos entregados / Número de apoyos programados) * 100\r\n', 1, 3, '7', 10, NULL, NULL),
(53, 'Porcentaje de programas aplicados\r\n', 'Promocionar en comunidades y ejidos los programas y apoyos existentes por parte de la Federación hacia el sector rural y agropecuario.\r\n', '\"(Total de programas ofrecidos/ Número de\r\nprogramas operados)*100\r\n\r\n\"\r\n', 1, 4, '8', 10, NULL, NULL),
(54, '\"Porcentaje de Unidades\r\nEconómicas\r\nRegularizadas\r\n\"\r\n', 'Contribuir a la competitividad del sector empresarial del municipio.\r\n', '\"( Número de Unidades Económicas Regularizadas\r\n/ Número de Trámites Programados ) *100\r\n\"\r\n', 1, 1, '1', 11, NULL, NULL),
(55, 'Porcentaje de mejoras regulatorias\r\n', 'Optimizar los procesos de mejora regulatoria para la obtención y renovación de la Licencia de Funcionamiento.\r\n', '(Procesos de mejora regulatoria aplicada /Número de procesos totales) *100\r\n', 1, 2, '2', 11, NULL, NULL),
(56, '\"Porcentaje de licencias\r\nde funcionamiento\r\nexpedidas\r\n\"\r\n', 'Expedición de licencias de cualquier actividad comercial, indutrial y de servicios.\r\n', '(Número de Licencia Expedidas en Tiempo/Número de Trámites Ingresados) *100\r\n', 1, 3, '3', 11, NULL, NULL),
(57, '\"Porcentaje de\r\nrevalidación de licencias\r\nde funcionamiento\r\n\"\r\n', 'Licencias de funcionamiento revalidación.\r\n', '\"(Cantidad de licencias de revalidación\r\nexpedidas/total de tramites de revalidación\r\ningresados)* 100\r\n\"', 1, 4, '4', 11, NULL, NULL),
(58, '\"Instituciones\r\nEducativas Eficientes\r\n\"\r\n', 'Contribuir a la calidad de vida mediante una adecuada Educación básica.\r\n', '\"(Servicios de educación básica realizados/\r\nServicios de educación básica programados)*100\r\n\"\r\n', 1, 1, '1', 12, NULL, NULL),
(59, 'Porcentaje de servicios entregados\r\n', 'La comunidad escolar de educación básica cuenta con apoyos para mejorar los servicios educativos en la educación básica.\r\n', '\"(Apoyos entregados /\r\nApoyos programados)*100\r\n\"\r\n', 1, 2, '2', 12, NULL, NULL),
(60, '\"Porcentaje de\r\nEfectividad\r\nCultural Municipal\"', 'Actividades culturales y artísticas otorgadas al público en general.\r\n', '\"(Eventos Culturales y\r\nArtísticos realizados /\r\nEventos Culturales y\r\nArtísticos programados)*100\r\n\"\r\n', 1, 3, '3', 12, NULL, NULL),
(61, '\"Porcentaje de\r\nvida cultural en días naturales\r\n\"\r\n', 'Elaboración de un programa cultural y artístico.\r\n', '\"(Días calendario coneventos culturales y artísticos programados /Días Calendario Naturales)*100 \r\n\"\r\n', 2, 4, '4', 12, NULL, NULL),
(62, '\"Porcentaje en la gestión para\r\npromover la\r\npráctica deportiva.\r\n\"\r\n', 'Gestión para promover la práctica deportiva realizada.\r\n', '\"(Eventos de promoción de la  práctica deportiva realizados\r\n/ Eventos de promoción de la práctica deportiva programados)*100\r\n\"\r\n', 1, 2, '5', 12, NULL, NULL),
(63, 'Porcentaje en la organización de eventos deportivos.\r\n', 'Organización de eventos deportivos, por ramas de actividad deportiva.\r\n', '\"(Eventos deportivos  realizados/Eventos deportivos programados)*100\r\n\"\r\n', 1, 4, '5', 12, NULL, NULL),
(64, 'Porcentaje en los sistemas difundidos de prevención de la salud\r\n', 'Sistemas difundidos de prevención de la salud aplicados.\r\n', '(campañas de promoción de  la salud realizadas/campañas de promoción de  la salud programadas)*100\r\n', 1, 3, '6', 12, NULL, NULL),
(65, 'Porcentaje en las jornadas para la prevención de la salud.\r\n', 'Realización de jornadas para la prevención de la salud, en zonas marginadas.\r\n', '(Jornadas para la prevención de la salud realizadas / Jornadas para la prevención de la salud programadas)*100\r\n', 1, 4, '7', 12, NULL, NULL),
(66, 'Variación porcentual de la población femenina capacitada para el trabajo.\r\n', 'Capacitación de la mujer para el trabajo realizada.\r\n', '((Mujeres en edad productiva capacitadas en el presente semestre / Mujeres en edad productiva capacitadas en el semestre anterior)-1)*100\r\n', 1, 3, '8', 12, NULL, NULL),
(67, 'Variación porcentual de mujeres capacitadas en áreas productivas.\r\n', 'Impartición de cursos de formación para el trabajo en distintas áreas productivas.\r\n', '(Mujeres que reciben capacitación para el trabajo / Total de mujeres solicitantes de cursos de capacitación para el trabajo)*100\r\n', 1, 4, '9', 12, NULL, NULL),
(68, 'Porcentaje en el otorgamiento de becas educativas.\r\n', 'Becas educativas otorgadas.\r\n', '(Becas educativas otorgadas / Becas educativas programadas)*100\r\n', 1, 3, '10', 12, NULL, NULL),
(69, 'Porcentaje de cumplimiento de publicación de convocatoria.\r\n', 'Emisión de convocatoria de becas.\r\n', '(Convocatorias publicadas / Convocatorias programadas)*100\r\n', 1, 4, '11', 12, NULL, NULL),
(70, 'Variación porcentual del impacto a la atención a familias vulnerables.\r\n', 'Contribuir a incrementar la cobertura de familias vulnerables y/o sujetas a asistencia social municipal a través de programas de integración familiar.\r\n', '((Familias vulnerables atendidas a través de programas de asistencia social en el año actual / Familias vulnerables atendidas a través de programas de asistencia social en el año anterior)-1)*100\r\n', 1, 1, '1', 13, NULL, NULL),
(71, 'Variación porcentual de atención al sector de familias vulnerables.\r\n', 'Las familias vulnerables cuentan con la atención necesaria para manejar situaciones adversas y de riesgo psicosocial.\r\n', '((Población total beneficiada con programas de atención a la familia en el año actual / Total de la población municipal  beneficiada con programas de atención a la familia en el año anterior)-1)*100\r\n', 1, 2, '2', 13, NULL, NULL),
(72, 'Variación porcentual de atención al sector de personas con discapacidad.\r\n', 'Las personas con discapacidad cuentan con programas de asistencia social que favorecen su integración a la sociedad.\r\n', '((Personas con discapacidad atendidas en el año actual / Personas con discapacidad atendidas en el año anterior)-1)*100\r\n', 1, 3, '3', 13, NULL, NULL),
(73, 'Porcentaje en la gestión de empleo a personas con discapacidad.\r\n', 'Gestión de empleo a personas con discapacidad.\r\n', '(Personas con discapacidad a las que se les gestionó un empleo / Total de personas con discapacidad que solicitan empleo)*100\r\n', 1, 4, '4', 13, NULL, NULL),
(74, 'Porcentaje de población infantil beneficiada con desayunos escolares.\r\n', 'Desayunos gestionados otorgados.\r\n', '(Población infantil beneficiada con desayunos escolares / Total población infantil nivel preescolar y escolar de educación pública)*100\r\n', 1, 3, '5', 13, NULL, NULL),
(75, 'Porcentaje de escuelas beneficiarias de raciones alimentarias.\r\n', 'Integración de padrón de escuelas beneficiarias del programa de raciones alimentarias.\r\n', '(Escuelas beneficiarias del programa de raciones alimentarias / Total de escuelas de nivel escolar y preescolar)*100\r\n', 1, 4, '6', 13, NULL, NULL),
(76, 'Porcentaje de adultos mayores en bolsa de trabajo.\r\n', 'Creación de bolsa de trabajo par adultos mayores.\r\n', '(Adultos mayores registrados en la bolsa de trabajo en el presente semestre / Adultos mayores registrados en la bolsa de trabajo en el semestre anterior)-1*100\r\n', 1, 4, '7', 13, NULL, NULL),
(77, 'Porcentaje de jóvenes sensibilizados.\r\n', 'Acciones educativas para la prevención de la violencia y las adicciones.\r\n', '(Jóvenes ue asistieron a las acciones educativas de prevención / Total de jóvenes en el municipio)\r\n', 1, 3, '8', 13, NULL, NULL),
(78, 'Porcentaje de participación en la campaña de sensibilización.', 'Acciones de sensibilizzación contra el consumo excesivo de alcohol.', '(Escuelas que recibieron sensibilización en el presente semestre / Escuelas que recibieron sensibilización en el semestre pasado)*100\r\n', 1, 4, '9', 13, NULL, NULL),
(79, 'Variación porcentual en la difusión de la información gubernamental en los medios de comunicación y sectores social.\r\n', 'Contribuir a la difusión de la información gubernamental en los medios de comunicación y sectores sociales, mediante la difusión de las acciones de gobierno.\r\n', '((Acciones de difusión de información gubernamental en los medios de comunicación realizados en el año actual / Acciones de difusión de información gubernamental en los medios de comunicación efectuados en el año anterior)-1)*100\r\n', 1, 1, '1', 14, NULL, NULL),
(80, 'Variación porcentual en la comunicación pública y fortalecimiento informativo a los habitantes del municipio.\r\n', 'La comunicación pública y fortalecimiento informativo a los habitantes del municipio se realiza por los canales convecionales de infromación gubernamental.\r\n', '((Eventos para fortalecer la comunicación e información pública dirigida a los habitantes del municipio realizados en el año actual / Eventos para fortalecer la comunicación e información pública dirigida a los habitantes del municipio realizados en el año anterior)-1)*100\r\n', 1, 2, '2', 14, NULL, NULL),
(81, 'Porcentaje en los planes y programas d acción gubernamental para instancias de gobierno y la sociedad.\r\n', 'Los planes y programas de acción gubernamental para instancias de Gobierno y la sociedad difundidos.\r\n', '(Planes y programas de acción gubernamental difundidos / Total de planes y programas de acción gubernamental)*100\r\n', 1, 3, '3', 14, NULL, NULL),
(82, 'Pocentaje en el cumplmmiento de la distribución de boletines informativos.\r\n', 'Distribución de los boletines informativos, con las acciones de gobierno.\r\n', '(Boletines informativos difundidos  / Boletines informativos programados para difusión)*100\r\n', 1, 4, '4', 14, NULL, NULL),
(83, 'Índice de respuesta a demandas ciudadanas para la construcción de las políticas públicas municipales.\r\n', 'Demandas ciudadanas para la construcción de las políticas públicas municipales registradas.\r\n', '(Demandas ciudadanas para la construcción de las políticas públicas municipales atendidas / Demandas ciudadanas para la construcción de las políticas públicas municipales registradas en foros de consulta del PMD)*100\r\n', 1, 3, '5', 14, NULL, NULL),
(84, 'Porcentaje en las políticas públicas municipales.\r\n', 'Políticas públicas municipales desarrolladas.\r\n', '(Políticas públicas municipales realizadas / Políticas públicas municipales programadas)*100\r\n', 1, 4, '6', 14, NULL, NULL),
(85, 'Variación porcentual en el comportamiento del Índice de Transparencia en la información.\r\n', 'Contribuir al cumplimiento de la obligación de acceso a la información a través de la consolidación de grupos de participación social y escrutinio público.\r\n', '((Índice de transparencia del año actual / Índice de transparencia año anterior)-1)*100\r\n', 1, 1, '1', 15, NULL, NULL),
(86, 'Porcentaje de cumplimiento en la obligación de transparencia.\r\n', 'Laciudadania recibe atención puntual y oportuna en las solicitudes interpuestas en materia de transparencia y acceso a la información.\r\n', '((Procedimientos de inconformidad interpuestos en el año actual / Procedimientos de inconformidad interpuestos en el año anterior)-1)*100\r\n', 1, 2, '2', 15, NULL, NULL),
(87, 'Porcentaje en asesorías jurídicas impartidas.\r\n', 'Asesorías Jurídicas impartidas.\r\n', '(Asesorías Jurídicas impartidas / Asesorías Jurídicas programadas)*100\r\n', 1, 3, '3', 15, NULL, NULL),
(88, 'Porcentaje en las asesorías de mediación y conciliación de diferendos entre las partes en conflicto.\r\n\r\n', 'Los asesores jurídicos calificados tramitan la defensa de asuntos jurídicos.\r\n\r\n', '(Asesorías de mediación de diferendos entre las partes en conflicto realizadas / Asesorías de mediación de diferendos entre las partes en conflicto programadas)*100\r\n', 1, 4, '4', 15, NULL, NULL),
(89, 'Porcentaje de las demandas en contra de la Adinistración Pública Municipal.\r\n', 'Demandas en contra de la Administración Pública Municipal obtenidas atendidas.\r\n', '(Demandas en contra de la Administración Pública Municipal atendidas / Demandas en contra de la Administración Pública Municipal presentadas)*100\r\n', 1, 3, '5', 15, NULL, NULL),
(90, 'Porentaje en la tramitación de asuntos jurídicos.\r\n', 'Tramitación de asuntos jurídicos, en los tribunales competentes.\r\n', '(Tramitación de asuntos jurídicos realizados / Tramitación de asuntos jurídicos programados)*100\r\n', 1, 4, '6', 15, NULL, NULL),
(91, 'Porcentaje en el cumplimiento de respuesta a solicitudes de información.\r\n', 'Solicitudes de información a través de módulos de información recibidas.\r\n', '(Solicitudes de transparencia y acceso a la información solventadas / Solicitudes de transparencia y acceso a la información recibidas)*100\r\n', 1, 3, '7', 15, NULL, NULL),
(92, 'Porcentaje de Unidades administratias en demanda de solicitudes.\r\n', 'Solicitudes tornadas a las diversas áreas administrativas.\r\n', '(Unidades admnistrativas con solicitud de acceso a la información interpuesta / Total de Unidades Administrativas Municipales)*100\r\n', 1, 4, '8', 15, NULL, NULL),
(93, 'Variación porcentual en la actualización tecnológica municipal.\r\n', 'Contribuir a eficientar la gestión y administración gubernamental a través de la actualización de TIC´ s que contribuyan al impulso de un gobierno digital.\r\n', '((TIC´s instrumentadas en el año actual / TIC´s instrumentadas en el año anterior)-1)*100\r\n', 1, 1, '1', 16, NULL, NULL),
(94, 'Porcentaje de avance en la prestación de Servicios Electrónicos.\r\n', 'Los servidores públicos municipales cuentan TIC´s que mejoran la operación y distribución de la información brindada a la ciudadanía y la prestación de servicos electrónicos.\r\n', '(Total de trámites municipales en linea / Total de trámites municipales por subir a la web)*100\r\n', 1, 2, '2', 16, NULL, NULL),
(95, 'Porcentaje de avance en la mejora procedimental.\r\n', 'Procedimientos simplificados para la prestación de servicios electrónicos instaurados.\r\n', '(Total de procedimientos simplificados enfocados a la prestación de servicios electrónicos / Total de procedimientos identificados de servicios electrónicos )*100\r\n', 1, 3, '3', 16, NULL, NULL),
(96, 'Porcentaje de avance en la Adaptación Procedimental.\r\n', 'Adaptación del procedimiento presencial hacia procedimientos remotos.\r\n', '(Procedimientos adecuados / Procedimientos programados)*100\r\n', 1, 4, '4', 16, NULL, NULL),
(97, 'Variación porcentual de Elaboración de Manuales de organización y procedimientos de la Administración Pública Municipal.\r\n\r\n', 'Programa integral de desarrollo Institucional que da certeza y mejora los servicios que se ofrecen a la ciudadanía.\r\n\r\n', '((Manuales de Organización elaborados en el año actual / Manuales de Organización elaborados en el año anterior)-1)*100\r\n', 1, 3, '5', 16, NULL, NULL),
(98, 'Porcentaje de elaboración de Manuales de Organización.\r\n', 'Elaboración de Manuales de Organización.\r\n', '(Manuales de Organización elaborados / Manuales de Organización programados)*100\r\n', 1, 4, '6', 16, NULL, NULL),
(99, 'Porcentaje de elaboración de Manuales de procedimientos.\r\n', 'Elaboración de Manuales de procedimientos.\r\n', '(Manuales de Procedimientos elaborados / Manuales de Procedimientos programados)*100\r\n', 1, 4, '7', 16, NULL, NULL),
(100, 'Variación porcentual de la observación y seguimiento al cumplimiento del marco normativo.\r\n', 'Contribuir al fortalecimiento de la vocación del servicio ético y profesional de la función pública a través del seguimiento y observación al cumplimiento del marco normativo institucional.\r\n', '((Expedientes de observación y seguimiento al cumplimiento del marco normativo institucional concluidos en el año actual / Expedientes de observación y seguimiento al cumplimiento del marco normativo institucional concluidos en el año anterior)-1)*100\r\n', 1, 1, '1', 17, NULL, NULL),
(101, 'Variación porcentual de quejas y/o denuncias hacia servidores públicos.\r\n', 'Los servidores públicos desarrollan eficazmente la función pública y ética en el municipio en base a quejas y/o denuncias.\r\n', '((Quejas y/o  denuncias hacia servidores públicos presentadas en el año actual / Quejas y/o  denuncias hacia servidores públicos presentadas en el año anterior)-1)*100 \r\n', 1, 2, '2', 17, NULL, NULL),
(102, 'Porcentaje de campañas de información de las obligaciones.\r\n', 'Campañas de información de las obligaciones de los servidores públicos realizadas.\r\n', '(Campañas de información de las obligaciones de los servidores públicos realizadas / Campañas de información de las obligaciones de los servidores públicos programadas)*100\r\n', 1, 3, '3', 17, NULL, NULL),
(103, 'Porcentaje de carteles informativos.\r\n', 'Elaboración de carteles informativos.\r\n', '(Carteles informativos elaborados / Carteles informativos requeridos)*100\r\n', 1, 4, '4', 17, NULL, NULL),
(104, 'Variación porcentual en el incremento en los ingresos municipales.\r\n', 'Contribuir a fortalecer la estructura del ingreso municipal a través de un programa de Fortalecimiento Recaudatorio que incremente los ingresos propios municipales.\r\n', '((Ingresos municipales sin considerar financiamiento del año actual / Ingresos municipales sin considerar financiamiento del año anterior)-1)*100\r\n', 1, 1, '1', 18, NULL, NULL),
(105, 'Variación porcentual en el fortalecimiento de los ingresos propios municipales.\r\n', 'Los ingresos propios municipales incrementan su estructura porcentual con respecto al total de los ingresos municipales.\r\n', '((Recursos propios del municipio sin considerar participaciones del año actual / Recursos propios del municipio sin considerar participaciones del año anterior)-1)*100 \r\n', 1, 2, '2', 18, NULL, NULL),
(106, 'Variación porcentual en el fortalecimiento de la recaudación corriente.\r\n', 'Programas de Fortalecimiento a la Recaudación corriente aplicado.\r\n', '((Recaudación corriente obtenida en el primer trimestre del año actual / Recaudación corriente obtenida en el primer trimestre del año anterior)-1)*100\r\n', 1, 3, '3', 18, NULL, NULL),
(107, 'Porcentaje de jornadas especiales de recaudación (descuentos por pago anticipado o en intereses moratorios)\r\n', 'Jornadas especiales e instauración de cajas móviles en los meses de alta recaudación.\r\n\r\n', '(Total de jornadas especiales de recaudación realizadas / Total de jornadas especiales de recaudación programadas)*100\r\n', 1, 4, '4', 18, NULL, NULL),
(108, 'Porcentaje de servicios de recaudación de pagos vía electrónica.\r\n', 'Jornadas especiales e instauración de cajas móviles en los meses de alta recaudación.\r\n\r\n', '(Total de cajas móviles instaladas / Total de cajas móviles programadas)*100\r\n', 1, 4, '5', 18, NULL, NULL),
(109, 'Actualización catastral\r\n', 'Actualización de los valores catastrales y factores de incremento en el sistema de cobro.\r\n', '((Predios actualizados en el presente ejercicio fiscal / Predios actualizados en el año anterior))-1*100\r\n', 1, 4, '6', 18, NULL, NULL),
(110, 'Proporción del monto de adeudos por incumplimiento del pago con respecto a los egresos municipales.\r\n', 'Contribuir al saneamiento financiero municipal a través de un programa de cumplimiento del rezago de obligaciones por concepto de servicios definidos.\r\n', '(Monto total de adeudos por concepto de rezago de cumplimiento de obligaciones por servicios y financiamiento proporcionados / Total de egresos municipales)*100\r\n', 1, 1, '1', 19, NULL, NULL),
(111, 'Monto de los acreedores a regulizar con respecto al total de ingresos.\r\n', 'Las finanzas municipales cumplen el compromiso del rezago de obligaciones financieras.\r\n', '(Monto definido de rezago por pago a acreedores sujetos a regularización / Total de ingresos Municipales del presente ejercicio)*100\r\n', 1, 2, '2', 19, NULL, NULL),
(112, 'Amortización del cumplimiento con respecto al total de ingresos municipales.\r\n', 'Convenios para el reconocimiento, establecimiento del cumplimiento y amortización de la deuda, gestionados.\r\n', '(Monto establecido de amortización para el pago del rezago por incumplimiento de pago por servicios proporcionados / Total de ingresos municipales)*100\r\n', 1, 3, '3', 19, NULL, NULL),
(113, 'Proporción de la diferencia de montos con respecto al ingreso.\r\n', 'Conciliación del rezago.\r\n', '((Monto de rezago por incumplimiento de pago de servicios proporcionados registrados por el Ayuntamiento - Monto de rezago por incumplimiento registrado por el acreedor) / Total de ingresos Municipales del presente ejercicio)*100\r\n', 1, 4, '4', 19, NULL, NULL),
(114, 'Porcentaje de evaluaciones al Plan de Desarrollo Municipal.\r\n', 'Contribuir a mejorar los procesos de planeación, presupuestación y evaluación, mediante las evaluaciones al Plan de Desarrollo Municipal.\r\n', '(Evaluaciones al Plan de Desarrollo Municipal efectuadas en el año / Evaluaciones al Plan de Desarrollo Municipal programadas)*100\r\n', 1, 1, '1', 20, NULL, NULL),
(115, 'Variación porcentual de indicadores estratégicos y de gestión.\r\n', 'El proyecto PBR presenta los indicadores estratégicos y de gestión para su revisión y redirección.\r\n', '((Número de indicadores estratégicos y de gestión implementados en año actual / Número de indicadores estratégicos y de gestión implementados en el año anterior)-1)*100\r\n', 1, 2, '2', 20, NULL, NULL),
(116, 'Porcentaje de orientaciones y asesorías brindados a la estructura municipal.\r\n', 'Orientaciones y asesorías en materia del Sistema de Evaluación del Desempeño Municipal.\r\n', '(Número de orientaciones y asesorías otorgadas /Número de orientaciones y asesorías programadas)*100\r\n', 1, 3, '3', 20, NULL, NULL),
(117, 'Porcentaje del cumplimiento del llenado de formatos del PbRM. \r\n', 'Formulación del presupuesto con base en resultados.\r\n', '(Formatos el PbRM requisitados/ Total de formatos del PbRM requeridos)*100\r\n', 1, 4, '4', 20, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mir_indicador_has_acciones_poa`
--

CREATE TABLE `mir_indicador_has_acciones_poa` (
  `mir_indicadores_id` int(11) NOT NULL,
  `acciones_poa_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mir_indicador_niveles`
--

CREATE TABLE `mir_indicador_niveles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mir_indicador_niveles`
--

INSERT INTO `mir_indicador_niveles` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Fin', NULL, NULL),
(2, 'Propósito', NULL, NULL),
(3, 'Componente', NULL, NULL),
(4, 'Actividad', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mir_programas`
--

CREATE TABLE `mir_programas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `punto` varchar(30) DEFAULT NULL,
  `objetivo` text,
  `normativa_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mir_programas`
--

INSERT INTO `mir_programas` (`id`, `nombre`, `punto`, `objetivo`, `normativa_id`, `created_at`, `updated_at`) VALUES
(1, 'Escudo Bahía', 'Pp-E001', 'Disminuir la delincuencia y accidentes viales fortaleciendo las actividades de prevención  y dando seguimiento a las actividades institucionales programadas para que de igual forma se mejore la percepción de inseguridad en la ciudadanía.', 4, NULL, NULL),
(2, 'Seguridad Confiable y Moderna', 'Pp-E002', 'Aplicar los recursos federales para ampliar el número de policías y la inversión que requieren para proveer de la seguridad de los badebadenses necesitan', 4, NULL, NULL),
(3, 'Respeto a los Derechos Humanos', 'Pp- E003', 'Capacitar a la población en temas de derechos humanos para preveer de violaciones de sus derechos;  por otro lado, es necesario atender las quejas y proteger a las personas', 4, NULL, NULL),
(4, 'Cultura de Protección civil', 'Pp-E004', 'Realizar todo los requerido para mitigar el impacto de los desastres naturales  en la población a través de una cultura de protección civil fuerte', 4, NULL, NULL),
(5, 'Desarrollo Urbano Sustentable y Resiliente', 'Pp-E005', 'Desarrollar proyectos de orden territoral y ecológico requeridos así como proveer de vivienda a la sociedad', 4, NULL, NULL),
(6, 'Agua de Calidad para Todos ', 'Pp-E006', 'Destinar los recursos humanos, técnicos y financieros para la administración eficiente del agua', 4, NULL, NULL),
(7, 'Nuestra Imagen Urbana Recobra Nuestro Sentido', 'Pp-E007', 'Mejorar la imagen urbana del Municipio a través de la recolección eficiente de los residuos sólidos, limpieza y mantenimiento de espacios públicos y ampliación del alumbrado', 4, NULL, NULL),
(8, 'Territorio Competitivo E innovador', 'Pp-K008', 'Desarrollar la infraestructura planeada en tiempo y forma', 4, NULL, NULL),
(9, 'Infraestructura para el rezago social/Fondo III', 'Pp-K009', 'Desarrollar infraestructura con los los fondos federales en los términos y tiempo señalados', 4, NULL, NULL),
(10, 'Territorio Rentable para la Inversión y Turismo', 'Pp-F010', 'Fomentar el turismo a través de acciones que fortalezcan el destino; así como la diversificación  de la economía y fomento al empleo para el desarrollo económico local', 4, NULL, NULL),
(11, 'Regulación y Oportunidad de Negocios', 'Pp-F011', 'Mejorar los procesos  de registro y regulación de licencias de negocios', 4, NULL, NULL),
(12, 'Calidad de Vida Solidaria E incluyente ', 'Pp-E012', 'Atender en forma oportuna con calidad y calidez, los programas de apoyo a la pobreza, mujeres, jóvenes, educación,  cultura y deporte', 4, NULL, NULL),
(13, 'Todos merecemos estar bien', 'Pp-E013', 'Generar programas y acciones para apoyar a grupos vulnerables', 4, NULL, NULL),
(14, 'Gobierno  innovador ', 'Pp-E014', NULL, 4, NULL, NULL),
(15, 'Cultura de la Legalidad ', 'Pp-E015', NULL, 4, NULL, NULL),
(16, 'Bahía Moderno y eficiente', 'Pp-M016', NULL, 4, NULL, NULL),
(17, 'Cero Tolerancia', 'Pp-O017', NULL, 4, NULL, NULL),
(18, 'Ponte al Corriente ', 'Pp-E018', NULL, 4, NULL, NULL),
(19, 'Autonomía Financiera (Deuda)', 'D.019 ', NULL, 4, NULL, NULL),
(20, 'Juntos contruimos el futuro', 'Pp-P020', NULL, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mir_tipos_indicador`
--

CREATE TABLE `mir_tipos_indicador` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mir_tipos_indicador`
--

INSERT INTO `mir_tipos_indicador` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Accion', NULL, NULL),
(2, 'Datos', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_id`, `model_type`) VALUES
(1, 1, 'App\\User'),
(6, 3, 'App\\User'),
(4, 2, 'App\\User');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `normativas`
--

CREATE TABLE `normativas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) DEFAULT NULL,
  `descripcion` text,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `normativas`
--

INSERT INTO `normativas` (`id`, `nombre`, `descripcion`, `created_at`, `updated_at`) VALUES
(1, 'Plan Municipal', NULL, '2018-09-20 05:00:00', '2018-09-20 05:00:00'),
(2, 'INAFED', NULL, '2018-09-20 05:00:00', '2018-09-20 05:00:00'),
(3, 'Agenda 2030', 'Agenda 2030 y los Objetivos de Desarrollo Sostenible', NULL, NULL),
(4, 'MIR', 'Matriz de Indicadores de Resultados', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notificaciones`
--

CREATE TABLE `notificaciones` (
  `id` int(11) NOT NULL,
  `contenido` text,
  `estatus` tinyint(4) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'estatus\n\n0 => no Visto\n1 => Visto',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE `perfiles` (
  `id` int(11) NOT NULL,
  `nombre` varchar(80) DEFAULT NULL,
  `apellido_pat` varchar(50) DEFAULT NULL,
  `apellido_mat` varchar(50) DEFAULT NULL,
  `puesto_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`id`, `nombre`, `apellido_pat`, `apellido_mat`, `puesto_id`, `created_at`, `updated_at`) VALUES
(1, 'Edson Enrique', 'Garcia', 'Perez', 2, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(2, 'Juan Carlos', 'Carrillo', 'Contreras', 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(4, 'Luis', 'Hernandez', 'Larios', 3, '2018-09-19 20:42:02', '2018-09-19 21:15:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'read_users', 'web', '2018-09-19 16:05:08', '2018-09-19 16:05:08'),
(2, 'write_users', 'web', '2018-09-19 16:05:08', '2018-09-19 16:05:08'),
(5, 'read_roles', 'web', '2018-09-19 16:06:17', '2018-09-19 16:06:17'),
(6, 'write_roles', 'web', '2018-09-19 16:06:17', '2018-09-19 16:06:17'),
(7, 'read_dashboard', 'web', '2018-09-19 16:06:41', '2018-09-19 16:06:41'),
(8, 'write_dashboard', 'web', '2018-09-19 16:06:41', '2018-09-19 16:06:41'),
(9, 'read_menus', 'web', '2018-09-19 16:07:58', '2018-09-19 16:07:58'),
(10, 'write_menus', 'web', '2018-09-19 16:07:58', '2018-09-19 16:07:58'),
(11, 'read_support', 'web', '2018-09-19 22:11:24', '2018-09-19 22:11:24'),
(12, 'write_support', 'web', '2018-09-19 22:11:24', '2018-09-19 22:11:24'),
(13, 'read_municipal_plan', 'web', '2018-09-20 18:18:29', '2018-09-20 18:18:29'),
(14, 'write_municipal_plan', 'web', '2018-09-20 18:18:29', '2018-09-20 18:18:29'),
(15, 'read_municipal_plan_client', 'web', '2018-10-11 17:24:01', '2018-10-11 17:24:01'),
(16, 'write_municipal_plan_client', 'web', '2018-10-11 17:24:01', '2018-10-11 17:24:01'),
(17, 'read_make_poa', 'web', '2018-10-14 16:43:15', '2018-10-14 16:43:15'),
(18, 'write_make_poa', 'web', '2018-10-14 16:43:15', '2018-10-14 16:43:15'),
(19, 'read_inafed', 'web', '2018-10-17 15:53:24', '2018-10-17 15:53:24'),
(20, 'write_inafed', 'web', '2018-10-17 15:53:24', '2018-10-17 15:53:24'),
(21, 'read_schedule', 'web', '2018-11-09 19:34:55', '2018-11-09 19:34:55'),
(22, 'write_schedule', 'web', '2018-11-09 19:34:55', '2018-11-09 19:34:55'),
(23, 'read_metadata_direction', 'web', '2018-11-12 15:34:59', '2018-11-12 15:34:59'),
(24, 'write_metadata_direction', 'web', '2018-11-12 15:34:59', '2018-11-12 15:34:59'),
(25, 'read_generate_poa', 'web', '2018-11-12 16:43:39', '2018-11-12 16:43:39'),
(26, 'write_generate_poa', 'web', '2018-11-12 16:43:39', '2018-11-12 16:43:39'),
(27, 'read_mir', 'web', '2018-11-14 16:26:10', '2018-11-14 16:26:10'),
(28, 'write_mir', 'web', '2018-11-14 16:26:10', '2018-11-14 16:26:10');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pm_ejes`
--

CREATE TABLE `pm_ejes` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `objetivo` text,
  `punto` varchar(20) DEFAULT NULL,
  `normativa_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pm_ejes`
--

INSERT INTO `pm_ejes` (`id`, `nombre`, `objetivo`, `punto`, `normativa_id`, `created_at`, `updated_at`) VALUES
(1, 'Juntos por la Seguridad', NULL, '1', 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(2, 'Juntos por el Desarrollo Sustentable', NULL, '2', 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(3, 'Juntos por la Competitividad Integral\r\n', NULL, '3', 1, NULL, NULL),
(4, 'Juntos por la Calidad de vida Incluyente y Solidaria', NULL, '4', 1, NULL, NULL),
(5, 'Juntos por la Gobernanza\r\n', NULL, '5', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pm_estrategias`
--

CREATE TABLE `pm_estrategias` (
  `id` int(11) NOT NULL,
  `nombre` text,
  `punto` varchar(20) DEFAULT NULL,
  `descripccion` text,
  `pm_objetivo_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pm_estrategias`
--

INSERT INTO `pm_estrategias` (`id`, `nombre`, `punto`, `descripccion`, `pm_objetivo_id`, `created_at`, `updated_at`) VALUES
(1, 'Seguridad Cercana\r\n', '1.1.1', 'Seguridad cercana respetuosa de los derechos humanos, mantener el orden y la paz en todo el municipio y garantizar efectivo ejercicio de los derechos humanos relacionados con la prevención, el control de la violencia y el delito a propios y visitantes, mediante la implementación de políticas publicas integrales, que contemplen acciones participativas, operativas, normativas y preventivas.', 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(2, 'Legalidad en justicia municipal/D.H\r\n', '1.1.2', 'Mejorar la de justicia municipal procurando\r\nla armonía social con apego al Estado de Derecho.', 1, NULL, NULL),
(3, 'Seguridad preventiva y operativa\r\n', '1.1.3', 'Seguridad preventiva y operativa, fortalecer la política de Prevención del delito\r\npara poder incidir en la transformación del tejido social.', 1, NULL, NULL),
(4, 'Seguridad  confiable\r\n', '1.1.4', 'Seguridad Confiable, Elevar los índices de confianza y satisfacción ciudadana\r\nen la seguridad pública, mediante el fortalecimiento de la corporación de Seguridad Pública y Tránsito\r\npara generar alto compromiso en el combate a lainseguridad y delincuencia.', 1, NULL, NULL),
(5, 'Seguridad moderna\r\n', '1.1.5', 'Seguridad Moderna, Incrementar los resultados adecuados en materia de disminución\r\nde índices delictivos a través del uso de herramientas informáticas y tecnológicas.', 1, NULL, NULL),
(6, 'Seguridad Coordinada\r\n', '1.1.6', 'Seguridad coordinada, Generar mecanismos de comunicación y coordinación\r\nefectivas y seguras, dentro y fuera de la corporación municipal.', 1, NULL, NULL),
(7, 'Seguridad, prioridad de todos\r\n', '1.1.7 ', 'La seguridad es prioridad de todos, Priorizar la seguridad pública como eje\r\nestratégico de la población, administración y el presupuesto municipal a través de la captación de\r\nfondos públicos y/o de particulares.', 1, NULL, NULL),
(8, 'Movilidad para todos\r\n', '1.2.1 ', 'Movilidad para todos, promover acciones para lograr la movilidad de visitantes\r\ny habitantes en forma rápida, segura y con el menor costo económico, social y ambiental', 2, NULL, NULL),
(9, 'Seguridad vial\r\n', '1.2.2 ', 'Seguridad vial, reducir los índices de siniestralidad vial en el municipio', 2, NULL, NULL),
(10, 'Cultura de protección civil\r\n', '1.3.1 ', 'Cultura de protección civil. Impulsar una cultura de protección civil en conjunto\r\ncon los demás niveles de gobierno, actores sociales y privados.', 3, NULL, NULL),
(11, 'Respuesta oportuna en emergencias y desastres\r\n', '1.3.2 ', 'Respuesta oportuna. Fortalecer la capacidad de respuesta para la atención\r\nde emergencias y desastres por la presencia de fenómenos perturbadores en el municipio, proporcionando\r\nauxilio de manera eficaz y eficiente a la población afectada.', 3, NULL, NULL),
(12, 'Gestión  oportuna en situación de riesgos\r\n', '1.3.3 ', 'Gestión oportuna en situación de riesgo. Aumentar la capacidad del municipio\r\nen preparación para casos de desastre, a fin de dar una respuesta eficaz y “reconstruir mejor”\r\nen los ámbitos de la recuperación, la rehabilitación y la reconstrucción', 3, NULL, NULL),
(13, 'Estructura sólida de protección civil\r\n', '1.3.4 ', 'Estructura sólida y coordinada, Contar con una estructura organizada,\r\nbasada en procedimientos claros de coordinación para la Gestión Integral de Riesgo de Desastre (GIRD)\r\ny riesgo en el municipio.', 3, NULL, NULL),
(14, 'Ciudad Consolidada Sostenible\r\n', '2.1.1 ', 'CIUDAD CONSOLIDADAD SOSTENIBLE CON GESTION URBANISTCA ÁGIL Y\r\nTRANSPARENTE. Actualizar democráticamente la reglamentación municipal en materia de desarrollo\r\nurbano, para lograr un crecimiento ordenado, sustentado en principios inclusivos, integrales, seguros,\r\nsustentables, resilientes, mediante la implementación de procesos de gestión municipal ágil, transparente\r\ne integral, que además permitan abatir la corrupción en la materia.', 4, NULL, NULL),
(15, 'Desarrollo urbano sustentable y resiliente\r\n', '2.1.2 ', 'DESARROLLO URBANO SUSTENTABLE Y RESCILENTE. Implementar el\r\nprograma de desarrollo social, económico, y urbano, acorde con el plan municipal integral de gestión\r\nambiental, en el que se contemplen acciones en los siguientes ejes: gestión integral del agua\r\ny residuos sólidos, promoción de sistemas energéticos eficientes, movilidad integral sostenible, entre\r\notras, con la finalidad de realizar un manejo sostenible de los recursos naturales bajo principios de\r\nconservación, protección y restauración de los ecosistemas municipales', 4, NULL, NULL),
(16, 'Agua de Calidad para todos\r\n', '2.1.3', 'Agua de calidad para todos, Priorizar el vital líquido como recurso estratégico\r\npara la calidad de vida y competitividad, Establecer el programa integral y participativo para\r\nmejorar, incrementar y proteger las fuentes de abastecimiento de agua y ampliar la infraestructura\r\npara garantizar la cobertura de dotación de agua de calidad a todas las familias del municipio y\r\nlograr su posterior aprovechamiento.', 4, NULL, NULL),
(17, 'Manejo de residuos sólidos\r\n', '2.1.4 ', 'El manejo integral de residuos sólidos, mejora la salud de todos, consolidar\r\ny mejorar el sistema de recolección de residuos sólidos, con visión integral, sustentable, participativa\r\ny rescilente, asegurando la correcta disposición final y minimizando el impacto ambiental y a\r\nla salud de los ciudadanos.', 4, NULL, NULL),
(18, 'Modernización y ampliación del alumbrado público\r\n', '2.1.5 ', 'Modernización y ampliación del alumbrado público para seguridad,\r\nAmpliar y modernizar el sistema de alumbrado público de la ciudad bajo principios de eficiencia\r\nenergética y como política trasversal para garantizar la seguridad de las familias en el municipio.', 4, NULL, NULL),
(19, 'Movilidad integral y sustentable\r\n', '2.1.6 ', 'Movilidad integral y sustentable, Realizar los estudios, obras, acciones y\r\ngestiones con todos los actores públicos y sociales para lograr la conectividad integral en el municipio,\r\nmediante la implementación de acciones planeación para la consolidación y cohesión de ciudad,\r\nasí como la planeación de los sistemas multimodales, sustentables, modernos, eficientes, y accesibles\r\ny la construcción y mejoramiento de infraestructura de comunicación que facilite la movilidad\r\nmultimodal, accesible y segura como una política trasversal para garantizar la competitividad, el\r\ndesarrollo sustentable y la seguridad', 4, NULL, NULL),
(20, 'Bahía digital\r\n', '2.1.7 ', 'Bahía digital, Como política trasversal con la competitividad y educación\r\nse propone la conectividad o acceso, al uso y apropiación de la tecnología para la información\r\ncomo elemento para elevar la calidad de vida y acceso a oportunidades de los jóvenes y\r\nfamilias del municipio.', 4, NULL, NULL),
(21, 'Nuestra imagen urbana\r\n', '2.1.8 ', 'Nuestra imagen urbana, recobra su significado, Lograr que en la organización\r\nde ciudad que se plantee en la planeación urbana, además de contemplar los elementos\r\npara el bienestar de la población con perspectiva estética y funcional, se incorporen los aspecto\r\nsociales y humano propios de la identidad y arraigo de la zona, a partir de los cuales cada barrio,\r\ncolonia, calle, toma significado e importancia para los habitantes, en armonía con el contexto genera,\r\nconvirtiéndose en un elementos de valor representativo de la esencia de la población', 4, NULL, NULL),
(22, 'Vivienda\r\n', '2.1.9 ', 'Vivienda, Lograr que el derecho a la vivienda en el municipio, se ejerza\r\nen forma incluyente, segura, sustentable, digna, accesible y con servicios públicos, como política\r\ntrasversal se garantiza la seguridad y bienestar de las familias', 4, NULL, NULL),
(23, 'Productos cárnicos\r\n', '2.1.10 ', 'Productos cárnicos saludables, garantizar que el consumo de carne en\r\nel municipio que se sacrifica en el rastro, cumpla con las normas de calidad, sanidad y humanitarias.', 4, NULL, NULL),
(24, 'Territorio Competitivo e Innovador\r\n', '3.1.1', 'Territorio competitivo e innovador, Priorización en aplicación del presupuesto\r\nen los factores que inciden en del desarrollo económico, en forma trasversal y sustentable tales\r\ncomo: obras públicas, infraestructura, seguridad, movilidad integral y multimodal, conectividad, desarrollo\r\nurbano consolidado y protección en la tenencia de la tierra, de forma incluyente, dado que la\r\ncalidad de dichos servicios incrementa los atractivos de la localidad para la inversión privada.', 5, NULL, NULL),
(25, 'Territorio rentable para la inversión\r\n', '3.1.2 ', 'Territorio rentable para la inversión, promocionar y fortalecer la imagen del\r\nMunicipio de Bahía de Banderas como un lugar rentable y seguro para la inversión nacional e internacional,\r\npara lograr el desarrollo integral y sustentable', 5, NULL, NULL),
(26, 'Encadenamiento productivo\r\n', '3.1.4 ', 'Encadenamiento productivo para la competitividad, vincular al tejido productivo,\r\ncomercial y empresarial a través de redes y cadenas productivas.', 5, NULL, NULL),
(27, 'Más empleos mejor remunerados\r\n', '3.1.5 ', 'Más empleos mejor remunerados, Elevar la calidad del capital humano, mejorando\r\nla productividad, la capacitación y la certificación de las competencias laborales, a través de\r\nlas políticas públicas que permitan determinar las necesidades del empresariado local en conjunto\r\ncon las universidades en cuanto a las competencias necesarias para mejorar la competitividad del\r\nmunicipio y mejorar las condiciones laborales', 5, NULL, NULL),
(28, 'Oportunidad de negocio\r\n', '3.1.6 ', 'Oportunidad de negocio, hacer de Bahía de Banderas un municipio atractivo y\r\nrentable para la inversión.', 5, NULL, NULL),
(29, 'Diversificación de la economía\r\n', '3.1.7 ', 'Diversificamos la economía con el desarrollo agrícola, pesquero y ganadero\r\ncompetitivo y sustentable, reactivar las actividades agropecuaria, pesquera y de ganadería y\r\nfomentar la inserción en procesos productivos y de comercialización de sus productos para generar\r\nel desarrollo integral del municipio, sustentado en un sector agroalimentario productivo, competitivo,\r\nrentable y sustentable', 5, NULL, NULL),
(30, 'Destino turístico seguro\r\n', '3.1.8 ', 'Destino turístico seguro, atractivo, confiable y sustentable, consolidar a\r\nBahía de Banderas, como un destino de preferencia a nivel nacional e internacional.', 5, NULL, NULL),
(31, 'Destino turístico atractivo y confiable\r\n', '3.1.9 ', 'Destino turístico atractivo, confiable y sustentable, consolidar a Bahía de\r\nBanderas, como un destino de preferencia a nivel nacional e internacional.', 5, NULL, NULL),
(32, 'Recuperando nuestra identidad\r\n', '3.1.10', 'Recuperando nuestra identidad, reconocer que la imagen y demás elementos\r\nurbanos, proporcionan la identidad de la ciudad, por lo que se propone realizar acciones de regeneración,\r\nembellecimiento y sacar a valor en forma sustentable, para fortalecer las economías de cada\r\ndelegación', 5, NULL, NULL),
(33, 'Comercio y abasto\r\n', '3.1.11 ', 'Comercio y Abasto, Acercar a la población de Bahía a fuentes de abasto,\r\nconfiables, ordenadas y de fácil acceso, que mejoren su calidad de vida.', 5, NULL, NULL),
(34, 'Infraestructura y movilidad integral\r\n', '3.1.12 ', 'Infraestructura y movilidad integral, multimodal, sustentable como base de\r\nla competitividad, fortalecer y ampliar la infraestructura, servicios y red vial de forma planeada y\r\nordenada sustentable y resiliente, para atraer la inversión y desarrollo económico.', 5, NULL, NULL),
(35, 'Infraestructura para el desarrollo\r\n', '4.1.1 ', 'Infraestructura para el desarrollo humano, dotar de infraestructura necesaria\r\npara la prestación de los servicios asistenciales en forma eficiente, equitativa, digna oportuna', 6, NULL, NULL),
(36, 'Bahía activo\r\n', '4.1.2', 'Bahía Actívate, fortalecer el deporte, fomentar la salud, física y mental de\r\nla población', 6, NULL, NULL),
(39, 'Bahía Sano\r\n', '4.1.3', 'Bahía Sano, Aumentar la disponibilidad de servicios de salud en el municipio,\r\npara la población abierta, a través de unidades de atención médica móviles y fijas.', 6, NULL, NULL),
(40, 'En Bahía promovemos la cultura\r\n', '4.1.4  ', 'En Bahía promovemos la cultura, lograr que Bahía sea un referente de cultura\r\ny arte a nivel nacional e internacional y fomentar el turismo cultural.', 6, NULL, NULL),
(41, 'Por nuestras mujeres\r\n', '4.1.5 ', 'Por nuestras mujeres, lograr que las mujeres del municipio se desarrollen\r\nen un ambiente libre de violencia y de equidad, que les permita el desarrollo integral de sus capacidades,\r\ncomo pilar de las familias de Bahía de Banderas, para que logren oportunidades de mejorar\r\nsu calidad de vida y la de sus familias', 6, NULL, NULL),
(42, 'Todos merecemos estar bien\r\n', '4.1.6 ', 'Todos merecemos estar bien, generar condiciones de inclusión e igualdad\r\nde oportunidades para toda la población logre elevar su validad de vida', 6, NULL, NULL),
(43, 'Alimentación', '4.1.7 ', 'Alimentación, Disminuir el número de personas que no tienen acceso a la\r\nalimentación en el municipio', 6, NULL, NULL),
(44, 'Educación\r\n', '4.1.8 ', 'Para el Desarrollo competitivo le apostamos a la educación, favorecer\r\nuna educación de vanguardia en Bahía de Banderas, que nos permita consolidarnos como municipio\r\npróspero, sustentable y competitivo', 6, NULL, NULL),
(45, 'Bahía joven\r\n', '4.1.9 ', 'BAHÍA JOVEN, gestionar y facilitar herramientas necesarias para garantizar el\r\ndesarrollo integral de la Juventud de Bahía de Banderas, fortaleciendo y fomentando la organización\r\ny participación social, la educación y capacitación permanente, desarrollo de las expresiones deportivas,\r\nculturales y artísticas, el cuidado de su salud, promoviendo acciones para impulsar el emprendurismo\r\ne inserción laboral, para el mejoramiento de las condiciones de vida de su calidad de vida\r\ny bienestar de la juventud.', 6, NULL, NULL),
(46, 'Protección de raíces indígenas\r\n', '4.1.10 ', 'Protegiendo nuestras raíces indígenas, Mejorar la calidad y el nivel de\r\nvida de las familias indígenas del municipio.', 6, NULL, NULL),
(47, 'Adultos mayores\r\n', '4.1.11 ', 'Adultos Mayores, Fortalecer e implementar una cultura de respeto y trato\r\ndigno a los adultos mayores; y brindar herramientas p0ara su inclusión social y laboral en forma\r\nH. X Ayuntamiento 2017 - 2021 . Bahía de Banderas . Plan de Desarrollo 2017 - 2021\r\n175\r\ndigna', 6, NULL, NULL),
(48, 'Cultura de la legalidad\r\n', '5.1 ', 'Cultura de la legalidad, Consolidarnos como un gobierno íntegro, en el que\r\nlas acciones de la gestión municipal son respetuosas y se ajustan las normas vigentes, así como,\r\nconcientizar sobre la necesaria corresponsabilidad ciudadana y abatir la corrupción para fortalecer\r\nla confianza ciudadana.', 7, NULL, NULL),
(49, 'Gestión municipal con respeto a las DH\r\n', '5.1.2 ', 'Tus derechos humanos son primero, lograr que la gestión municipal en todos\r\nlos ámbitos se realice en respeto de los derechos humanos de los ciudadanos.', 7, NULL, NULL),
(50, 'Bahía Transparente\r\n', '5.1.3 ', 'Bahía transparente, generar y actualizar la información pública relativa a la gestión\r\nmunicipal, en forma ordenada y establecer los canales sencillos y óptimos para el acceso a la\r\ninformación y rendición de cuentas a la ciudadanía', 7, NULL, NULL),
(51, 'Bahía cerca de ti\r\n', '5.1.4 ', 'En Bahía estamos cerca de ti., Elevar los índices de participación ciudadana, a\r\ntravés de instrumentos y herramientas que permitan a los ciudadanos conocer, fiscalizar, controlar,\r\ninvolucrarse en las acciones del gobierno municipal.', 7, NULL, NULL),
(52, 'Estrategia Bahía Moderno y digital\r\n', '5.1.5 ', 'Estrategia Bahía Moderno y digital, mejorar la calidad de la gestión pública\r\nmunicipal mediante la modernización de procesos, instrumentos de servicios municipales, así como,\r\naprovechamiento de las TICS, para elevar la calidad y oportunidad en la prestación de los servicios\r\npúblicos y conformar el sistema de información e indicadores sobre el desarrollo municipal, como\r\ninstrumentos de fortalecimiento de transparencia y rendición de cuentas.', 7, NULL, NULL),
(53, 'Menos política, más resultados\r\n', '5.1.6 ', 'Menos política, más resultados. Establecer mecanismos de manejo del gasto y la\r\ninversión, que garanticen la optimización de recursos y manejo adecuado de deuda pública para el\r\ndesarrollo de la ciudad y permitan el monitoreo y evaluación ciudadana.', 7, NULL, NULL),
(54, 'Estrategia ponte al corriente\r\n', '5.1.7 ', 'Estrategia Ponte al corriente, con tu participación avanzamos, Consolidar los\r\nmecanismos que permiten ampliar la recaudación fiscal, en un marco de transparencia, respeto a\r\nlos derechos del contribuyente y modernización.', 7, NULL, NULL),
(55, 'Estrategia Observatorio ciudadano\r\n', '5.1.8 ', 'Estrategia Observatorio ciudadano, Fortalecer la participación ciudadana en escrutinio\r\nciudadano sobre la gestión de la administración pública y difundir resultados, como medio\r\npara recuperar la confianza ciudadana y garantizar la eficiencia y efectividad en el ejercicio.', 7, NULL, NULL),
(56, 'Juntos construyendo el futuro\r\n', '5.1.9', 'Juntos construyendo el futuro fortalecer y estrechar las relaciones del municipio\r\ncon la sociedad civil, y entre la cabecera y las comunidades, conformando un gobierno cercano a\r\nla gente que genere confianza y coparticipación.', 7, NULL, NULL),
(57, 'Cero tolerancias', '5.1.10', 'Cero tolerancias, promover entre los servidores públicos y sociedad en general, la\r\nparticipación activa y corresponsabilidad para abatir la corrupción y abuso de poder en la gestión\r\nmunicipal', 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pm_lineas_accion`
--

CREATE TABLE `pm_lineas_accion` (
  `id` int(11) NOT NULL,
  `nombre` text,
  `punto` varchar(20) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0' COMMENT '0 = No Asignado, 1 = Asignado',
  `pm_estrategia_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pm_lineas_accion`
--

INSERT INTO `pm_lineas_accion` (`id`, `nombre`, `punto`, `status`, `pm_estrategia_id`, `created_at`, `updated_at`) VALUES
(1, 'Instalar el sistema de vídeo vigilancia en lugares y puntos estratégicos', '1.1.1.1', 0, 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(2, 'Fortalecer el modelo de agilidad para apoyo a emergencias mediante el centro de mando.', '1.1.1.2', 0, 1, '2018-09-20 05:00:00', '2018-09-20 05:00:00'),
(3, 'Coordinar operativos con instancias federales y estatales para el programa Respuesta Oportuna.', '1.1.1.3', 0, 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(4, 'Vigilar que se cumplan los ordenamientos de policía y buen gobierno en el municipio', '1.1.1.4', 0, 1, '2018-09-20 05:00:00', '2018-09-20 05:00:00'),
(5, 'Gestionar y destinar recursos federales y estatales para mejorar el servicio de seguridad pública municipal.', '1.1.1.5', 0, 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(6, 'Mantener una coordinación intra e interinstitucional con instituciones policiales municipales,estatales o federales para las comunicaciones.', '1.1.1.6', 0, 1, '2018-09-20 05:00:00', '2018-09-20 05:00:00'),
(10, 'Lograr la reducción de tiempos de respuesta a reportes ciudadanos.', '1.1.1.7', 0, 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(11, 'Incrementar las acciones de cercanía con la ciudadanía, el sector comercial y turístico, así como organizaciones civiles.', '1.1.1.8', 0, 1, '2018-09-20 05:00:00', '2018-09-20 05:00:00'),
(12, 'Implementar la denuncia telefónica, con atención y electrónica bilingüe.', '1.1.1.9', 0, 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(13, 'Restructuración, menos policías para administrar y más para vigilancia\r\n', '1.1.1.10', 0, 1, '2018-09-20 05:00:00', '2018-09-20 05:00:00'),
(14, 'Seguridad en tu casa y negocio, seguro contra robo y alarma vecinal\r\n\r\n', '1.1.1.11', 0, 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(15, 'Fortalecer la operatividad e instalar del Consejo Ciudadano de Seguridad.', '1.1.1.12', 0, 1, '2018-09-20 05:00:00', '2018-09-20 05:00:00'),
(16, 'Promover la cultura de la paz, favoreciendo la solución alterna de conflictos', '1.1.2.1 ', 0, 2, NULL, NULL),
(17, 'Garantizar el acceso a la información', '1.1.2.2', 0, 2, NULL, NULL),
(18, 'Ajustar la reglamentación municipal al estado de derecho con estricto respeto a los derechos\r\nhumanos', '1.1.2.3 ', 0, 2, NULL, NULL),
(19, 'En coordinación con autoridades federales y locales, garantizar la asesoría jurídica y en su caso\r\npatrocinio a personas vulnerables en asuntos jurídicos', '1.1.2.4 ', 0, 2, NULL, NULL),
(20, 'Garantizar el irrestricto respeto de los derechos humanos a la población y visitantes en todas\r\nlas actuaciones y gestiones de las autoridades municipales', '1.1.2.5 ', 0, 2, NULL, NULL),
(21, 'Convenir alianzas con la sociedad civil organizada para la promoción de los derechos humanos\r\ny el bienestar social.', '1.1.2.6 ', 0, 2, NULL, NULL),
(22, 'Capacitar y asesorar a la población en materia de derechos humanos.', '1.1.2.7 ', 0, 2, NULL, NULL),
(23, 'Atender y resolver quejas por presuntas violaciones a los derechos humanos.', '1.1.2.8 ', 0, 2, NULL, NULL),
(24, 'Difundir los Derechos Humanos y el reconocimiento de la dignidad', '1.1.2.9 ', 0, 2, NULL, NULL),
(25, 'Impulsar la política de prevención que atienda a la población en general.', '1.1.3.1 ', 0, 3, NULL, NULL),
(26, 'Recuperar los espacios públicos para la prevención de los delitos', '1.1.3.2 ', 0, 3, NULL, NULL),
(27, 'Identificar predios baldíos que representen riesgo para la salud y seguridad de las personas,\r\npara su limpieza y patrullaje.', '1.1.3.3 ', 0, 3, NULL, NULL),
(28, 'Establecer programas para reducir la tasa de desempleo y elevar ingresos per cápita, para\r\nbrindar oportunidades a todos y reducir los índices de robo en el municipio', '1.1.3.4 ', 0, 3, NULL, NULL),
(29, 'Ampliación y mantenimiento del Alumbrado público para tu seguridad', '1.1.3.5 ', 0, 3, NULL, NULL),
(30, 'Promover el uso responsable del Internet para prevenir delitos cibernéticos y asociados.', '1.1.3.6 ', 0, 3, NULL, NULL),
(31, 'Fomentar una cultura de no violencia todas las escuelas del municipio.', '1.1.3.7 ', 0, 3, NULL, NULL),
(32, 'Forjar una alianza con la ciudadanía contra la violencia y delincuencia con apoyo del Consejo\r\nCiudadano de Seguridad.', '1.1.3.8 ', 0, 3, NULL, NULL),
(33, 'Implementar en coordinación con la sociedad e instituciones educativas el programa de seguridad\r\nciudadana y vigilancia vecinal', '1.1.3.9 ', 0, 3, NULL, NULL),
(34, 'Implementar un sistema de atención integral a víctimas del delito y especialmente de género,\r\nviolencia doméstica y ataques sexuales.', '1.1.3.10 ', 0, 3, NULL, NULL),
(35, 'Atención de grupos de riesgo', '1.1.3.11 ', 0, 3, NULL, NULL),
(36, 'Establecer el sistema de prevención del delito a través de la cultura, el deporte y la recreación.', '1.1.3.12 ', 0, 3, NULL, NULL),
(37, 'Implementar cursos de autodefensa parajóvenes ymujeres del municipio', '1.1.3.13', 0, 3, NULL, NULL),
(38, 'Atención a jóvenes involucrados enasuntos de violencia y consumo de drogas.', '1.1.3.14 ', 0, 3, NULL, NULL),
(39, 'Vigilar el cumplimiento de los mecanismos de control de confianza de los elementos de la\r\ncorporación', '1.1.4.1 ', 0, 4, NULL, NULL),
(40, 'Establecer sistemas e indicadores de evaluación sistemática de desempeño policial', '1.1.4.2 ', 0, 4, NULL, NULL),
(41, 'Establecer el sistema de control y evaluación externa de la dirección mediante la instalación\r\ndel observatorio ciudadano.', '1.1.4.3 ', 0, 4, NULL, NULL),
(42, 'Ajustar la actuación de la corporación al respeto de los derechos humanos y a los principios\r\nde legalidad', '1.1.4.4 ', 0, 4, NULL, NULL),
(43, 'Establecer sistema de capacitación y profesionalización de los elementos de la corporación y\r\nespecialmente sobre su intervención en el sistema de justicia penal', '1.1.4.5 ', 0, 4, NULL, NULL),
(44, 'Generar programa de estímulos e incentivos por buen desempeño policial y contribuir a mejorar\r\nlas condiciones de trabajo de los elementos de la corporación.', '1.1.4.6 ', 0, 4, NULL, NULL),
(45, 'Inhibir las malas prácticas tales como impunidad, corrupción, violencia innecesaria yarbitrariedad\r\nen las acciones, en el cuerpo de seguridad', '1.1.4.7 ', 0, 4, NULL, NULL),
(46, 'Contribuir a reducir el índice de criminalidad en el municipio', '1.1.4.8 ', 0, 4, NULL, NULL),
(47, 'Incentivar el sentido de pertenencia entre el personal de la corporación para contribuir a la\r\nfortaleza de la corporación y el compromiso de sus miembros.', '1.1.4.9 ', 0, 4, NULL, NULL),
(48, 'Implementar el programa de Cuidado de la Salud para el personal de la corporación', '1.1.4.10 ', 0, 4, NULL, NULL),
(49, 'Impulsar un programa de acondicionamiento físico de los elementos de la corporación.', '1.1.4.11 ', 0, 4, NULL, NULL),
(50, 'Modernizar y equipar a los cuerpos policiales.', '1.1.4.12 ', 0, 4, NULL, NULL),
(51, 'Impulsar un programa de apoyo a la familia de los elementos de la corporación', '1.1.4.13 ', 0, 4, NULL, NULL),
(52, 'Aplicar, mediante sanciones eficaces, la política de Cero Tolerancia contra la corrupción y\r\nmal desempeño.', '1.1.4.14 ', 0, 4, NULL, NULL),
(53, 'Implementar sistemas informáticos, cartográficos, estadísticos, gráficos y los que se estimen\r\nnecesarios y viables.', '1.1.5.1 ', 0, 5, NULL, NULL),
(54, 'Actualizar y mejorar el equipo de cómputo de las áreas de inteligencia policial.', '1.1.5.2 ', 0, 5, NULL, NULL),
(55, 'Fortalecer y Modernizar la Unidad de Análisis, Sistema de Información Operativa e Inteligencia\r\nque proporcione los datos e información necesaria para guiar el despliegue de la Policía.\r\n', '1.1.5.3 ', 0, 5, NULL, NULL),
(56, 'Implementar el Mando estratégico para la acción policial reactiva y preventiva.', '1.1.5.4 ', 0, 5, NULL, NULL),
(57, 'Automatizar la información estadística y de criminalidad en el municipio para potenciar su\r\naprovechamiento en la toma de decisiones.\r\n', '1.1.5.5 ', 0, 5, NULL, NULL),
(58, 'Generación y explotación de la inteligencia policial a través de los informes homologados', '1.1.5.6 ', 0, 5, NULL, NULL),
(59, 'Realizar estudios y mediciones de la victimización y percepción de la inseguridad\r\n', '1.1.5.7 ', 0, 5, NULL, NULL),
(60, 'Incorporar recursos tecnológicos, informáticos, analíticos, métodos y sistemas a cargo de Seguridad\r\nPública y Tránsito para contribuir a garantizar la seguridad y reducir la criminalidad en el\r\nmunicipio.', '1.1.5.8 ', 0, 5, NULL, NULL),
(61, 'Mantener una coordinación intra e interinstitucional con instituciones policiales municipales,\r\nestatales o federales para las comunicaciones.\r\n', '1.1.6.1 ', 0, 6, NULL, NULL),
(62, 'Establecer sistemas de control de reportes, incidencias y colaboraciones.', '1.1.6.2 ', 0, 6, NULL, NULL),
(63, 'Implementar acciones de coordinación con la sociedad civil organizada, comités, instituciones\r\nacadémicas y sociedad en general programas de vigilancia y denuncia ciudadana.\r\n', '1.1.6.3 ', 0, 6, NULL, NULL),
(64, 'Fortalecer el sistema de radiocomunicaciones para atender oportunamente las incidencias que\r\nse presenten.', '1.1.6.4', 0, 6, NULL, NULL),
(65, 'Implementar en coordinación con el sistema educativo y padres de familia programa de vigilancia\r\nescolar.\r\n', '1.1.6.5 ', 0, 6, NULL, NULL),
(66, 'Lograr la coordinación con el sector hotelero en la prevención del delito a través de las áreas\r\nde seguridad y recursos humanos, para atención, reacción y capacitación sobre incidencia delictiva\r\nen zona turística', '1.1.6.6', 0, 6, NULL, NULL),
(67, 'Colaborar con las autoridades estatales y federales en la seguridad de la zona federal marítimo\r\nterrestre\r\n', '1.1.6.7', 0, 6, NULL, NULL),
(68, 'Generar la interconexión integral de frecuencias entre la corporación policiaca y las áreas de\r\nseguridad del sector empresarial para reacción de contingencias de seguridad', '1.1.6.8', 0, 6, NULL, NULL),
(69, 'Instrumentar y operar la Policía Turística Municipal bilingüe.\r\n', '1.1.6.9', 0, 6, NULL, NULL),
(70, 'Implementar el sistema de patrullaje por demarcación y/o cuadrantes para lograr que la ciudadanía\r\nse identifique con la seguridad en su zona y a su vez esta lo evalúe.', '1.1.6.10 ', 0, 6, NULL, NULL),
(71, 'Orientar estratégicamente y en base a resultados el recurso financiero para atender con\r\neficiencia las prioridades en materia de seguridad\r\n', '1.1.7.1 ', 0, 7, NULL, NULL),
(72, 'Generar el Programa de Seguridad Ciudadana con visión trasversal y perspectiva de género', '1.1.7.2 ', 0, 7, NULL, NULL),
(73, 'Lograr la reducción de congestión vial, en horarios, zonas y temporadas críticas\r\n', '1.2.1.1 ', 0, 8, NULL, NULL),
(74, 'Realizar acciones para recuperar las vialidades para incentivar el transporte no motorizado', '1.2.1.2 ', 0, 8, NULL, NULL),
(75, 'En coordinación con las autoridades locales, mejorar la accesibilidad a los servicios de movilidad\r\nen el municipio de toda la población y visitantes\r\n', '1.2.1.3 ', 0, 8, NULL, NULL),
(76, 'Implementar operativos para mejorar la fluidez vehicular y reducción de accidentes en el municipio', '1.2.1.4 ', 0, 8, NULL, NULL),
(77, 'Difundir entre la población la normatividad aplicable en materia de tránsito.\r\n', '1.2.1.5 ', 0, 8, NULL, NULL),
(78, 'Proporcionar auxilio vial, a las personas que lo requieran, para agilizar el tránsito vehicular', '1.2.1.6 ', 0, 8, NULL, NULL),
(79, 'Realizar estudios y proyectos viables, para mejorar el tránsito y fluidez en el territorio.\r\n', '1.2.1.7 ', 0, 8, NULL, NULL),
(80, 'Actualizar la normatividad en la materia', '1.2.1.8 ', 0, 8, NULL, NULL),
(81, 'Generar un sistema integral, multimodal accesible y sustentable de movilidad que logre la interconexión\r\nen todo el municipio\r\n', '1.2.1.9 ', 0, 8, NULL, NULL),
(82, 'Realizar las acciones de balizamiento necesarias para lograr la circulación segura', '1.2.1.10 ', 0, 8, NULL, NULL),
(83, 'Incentivar el uso seguro de la motocicleta como medio de transporte en el municipio\r\n', '1.2.1.11 ', 0, 8, NULL, NULL),
(84, 'Formular e implementar el plan para la movilidad de ciclistas', '1.2.1.12 ', 0, 8, NULL, NULL),
(85, 'Promover la cultura de la movilización segura en el municipio\r\n', '1.2.1.13 ', 0, 8, NULL, NULL),
(86, 'En Coordinación con las autoridades competentes promover la dignificación de los paradores\r\ndel transporte público', '1.2.1.14 ', 0, 8, NULL, NULL),
(87, 'Mejorar los programas de carga y descarga, así como de estacionamientos en el municipio,\r\npara agilizar el flujo vial y dar prioridad a peatones y ciclistas\r\n', '1.2.1.15 ', 0, 8, NULL, NULL),
(88, 'Promover la implantación de plataformas sobre los derechos de los pasajeros en el transporte\r\npúblico urbano', '1.2.1.16', 0, 8, NULL, NULL),
(89, 'Contribuir a lograr la conectividad/permeabilidad de la red viaria.\r\n', '1.2.1.17 ', 0, 8, NULL, NULL),
(90, 'Impulsar programas para desincentivar el uso de vehículo particular', '1.2.1.18', 0, 8, NULL, NULL),
(91, 'Garantizar la accesibilidad a los servicios de movilidad en los nuevos desarrollos\r\n', '1.2.1.19 ', 0, 8, NULL, NULL),
(92, 'Promover la ampliación de la cobertura de vialidades en el municipio', '1.2.1.20 ', 0, 8, NULL, NULL),
(93, 'Gestionar las acciones correctivas de la carpeta asfáltica y caminos rurales\r\n', '1.2.1.21 ', 0, 8, NULL, NULL),
(94, 'Ampliar la cobertura de pavimentación en el municipio', '1.2.1.22 ', 0, 8, NULL, NULL),
(95, 'Gestionar tecnología constructiva de vanguardia para minimizar las afectaciones en el tránsito\r\nvehicular y peatonal de la ciudad\r\n', '1.2.1.23', 0, 8, NULL, NULL),
(96, 'Colaborar con las dependencias de la Administración Pública Federal, Estatal y Municipal, en\r\nla planeación y elaboración de estudios viales', '1.2.1.24 ', 0, 8, NULL, NULL),
(97, 'Buscar estrategias de apoyo para movilidad peatonal, no motorizada y motorizada con los\r\nsectores y autoridades estatales para las familias vulnerables, estudiantes y adultos mayores.', '1.2.1.25 ', 0, 8, NULL, NULL),
(98, 'Realizar campañas prevención de accidentes viales relacionadas con el alcohol\r\n', '1.2.2.1 ', 0, 9, NULL, NULL),
(99, 'Realizar campañas viales reducción de accidentes motocicletas', '1.2.2.2 ', 0, 9, NULL, NULL),
(100, 'Proteger al peatón en cruceros peligrosos, mediante acciones preventivas y correctivas\r\n', '1.2.2.3 ', 0, 9, NULL, NULL),
(101, 'Generar la colaboración con autoridades Estatales para coadyuvar en el control, inspección y\r\nvigilancia del transporte público', '1.2.2.4', 0, 9, NULL, NULL),
(102, 'Fomentar la cultura de respeto de los derechos de las personas con capacidades diferentes\r\n', '1.2.2.5 ', 0, 9, NULL, NULL),
(103, 'Fortalecer la cultura de circulación vehicular en forma ajustada a los lineamientos en materia\r\nde control vehicular', '1.2.2.6 ', 0, 9, NULL, NULL),
(104, 'Asesorar y apoyar la integración y funcionamiento de los Comités Ciudadanos de Protección\r\nCivil\r\n', '1.3.1.1', 0, 10, NULL, NULL),
(105, 'Capacitar a la ciudadanía en materia Gestión Integral de Riesgos de Desastres.', '1.3.1.2 ', 0, 10, NULL, NULL),
(106, 'Llevar a cabo simulacros en el sector público, privado y social para la prevención y previsión\r\nde desastres.\r\n', '1.3.1.3 ', 0, 10, NULL, NULL),
(107, 'Impulsar y fomentar programas de educación ambiental como una estrategia para disminuir la\r\ngeneración de residuos y apoyar el reciclaje.', '1.3.1.4 ', 0, 10, NULL, NULL),
(108, 'Generar convenios para impulsar programas, campañas de información y actividades sociales\r\nrelacionadas con la protección civil y gestión integral de riesgos de desastres, en escuelas, empresas\r\ncon apoyo de organizaciones civiles y de otros niveles de gobierno (estatal y federal).\r\n', '1.3.1.5 ', 0, 10, NULL, NULL),
(109, 'Incorporar a todos los sectores en las acciones de prevención, mitigación y daños negativos\r\nante fenómenos naturales.', '1.3.1.6 ', 0, 10, NULL, NULL),
(110, 'Fortalecer información a través de campañas de difusión prevención y reducción de riesgos a\r\nla sociedad.\r\n', '1.3.1.7 ', 0, 10, NULL, NULL),
(111, 'Promover información para la prevención, gestión integral de riesgos y resiliencia en centros de\r\neducación básica, media y media superior.', '1.3.1.8 ', 0, 10, NULL, NULL),
(112, 'Incluir a la población residente en zonas vulnerables en programas o acciones de prevención\r\nante ocurrencia de desastres.\r\n', '1.3.1.9 ', 0, 10, NULL, NULL),
(113, 'Capacitar a estudiantes, servidores públicos, capital humano del sector económico y hoteles\r\ndel municipio y sociedad en general en primeros auxilios y prevención de incendios', '1.3.1.10 ', 0, 10, NULL, NULL),
(114, 'Vincularse con las demás áreas municipales y estatales para prevenir la instalación de comercios\r\ny obras que representen un riesgo y seguridad de las personas.', '1.3.1.11', 0, 10, NULL, NULL),
(115, 'Implementar un protocolo de seguimiento telefónico de emergencias que contribuya a la reducción\r\nde daños.\r\n\r\n', '1.3.2.1 ', 0, 11, NULL, NULL),
(116, 'Monitorear los fenómenos perturbadores que afecten a la ciudadanía.', '1.3.2.2 ', 0, 11, NULL, NULL),
(117, 'Brindar atención pre hospitalaria a las personas que lo requieran.\r\n', '1.3.2.3 ', 0, 11, NULL, NULL),
(118, 'Dar atención oportuna a la población ante situaciones de emergencias y desastres.', '1.3.2.4 ', 0, 11, NULL, NULL),
(119, 'Dotar de herramienta, equipo y vehículos a los cuerpos de atención a emergencias.\r\n', '1.3.2.5 ', 0, 11, NULL, NULL),
(120, 'Ampliar la cobertura de atención', '1.3.2.6 ', 0, 11, NULL, NULL),
(121, 'Elaborar un programa de vinculación entre las instituciones involucradas, para mejorar la capacidad\r\nde respuesta.\r\n', '1.3.2.7 ', 0, 11, NULL, NULL),
(122, 'Celebrar convenios para habilitar refugios temporales ante la ocurrencia de hechos catastróficos.', '1.3.2.8 ', 0, 11, NULL, NULL),
(123, 'Elaborar y mantener actualizado los manuales de reacción y el Manual Operativo de Refugios\r\nTemporales Municipales.\r\n', '1.3.2.9 ', 0, 11, NULL, NULL),
(124, 'Informar a la ciudadanía sobre las zonas de seguridad y rutas de evacuación en caso de riesgo o emergencia', '1.3.2.10 ', 0, 11, NULL, NULL),
(125, 'Actualización de los factores de riesgo a las instancias de Gobierno en materia de Protección Civil.\r\n', '1.3.3.1 ', 0, 12, NULL, NULL),
(126, 'Actualizar el atlas de riesgo.', '1.3.3.2 ', 0, 12, NULL, NULL),
(127, 'Identificación y dictamen de riesgos.\r\n', '1.3.3.3 ', 0, 12, NULL, NULL),
(128, 'Realizar funciones de inspección y vigilancia para prevenir riesgos en el municipio', '1.3.3.4 ', 0, 12, NULL, NULL),
(129, 'Realizar dictámenes de riesgo para centros de atención y cuidado infantil.\r\n', '1.3.3.5 ', 0, 12, NULL, NULL),
(130, 'Realizar verificaciones de seguridad a empresas, industrias y comercios.', '1.3.3.6 ', 0, 12, NULL, NULL),
(131, 'Gestionar recursos Federales y Estatales para llevar a cabo proyectos de prevención y mitigación\r\nde riesgos.\r\n', '1.3.3.7 ', 0, 12, NULL, NULL),
(132, 'Elaborar planes integrales y protocolos de Planes de Contingencias para cada tipo de fenómeno\r\nperturbador identificado en el territorio municipal considerando Subprogramas de Prevención, Auxilio\r\ny Recuperación', '1.3.3.8 ', 0, 12, NULL, NULL),
(133, 'Promover programas de profesionalización y capacitación, al personal que trabaja de manera\r\ndirecta en atención a emergencias o desastres.\r\n', '1.3.3.9 ', 0, 12, NULL, NULL),
(134, 'Brindar capacitación a la comunidad en general y a los integrantes de los grupos voluntarios,\r\na cerca de las acciones a realizar en caso de emergencia.', '1.3.3.10 ', 0, 12, NULL, NULL),
(135, 'Propiciar la elaboración de un marco legal que apoye la reducción de riesgos de desastres al\r\nnivel local para la promoción de obras de prevención, adaptación y mitigación.\r\n', '1.3.4.1 ', 0, 13, NULL, NULL),
(136, 'Generar la colaboración con el sector hotelero, empresarial y social para potenciar los recursos\r\nque garanticen la prevención y reacción oportuna en caso de contingencia.', '1.3.4.2 ', 0, 13, NULL, NULL),
(137, 'Impulsar el proceso de planeación integral, mediante la actualización del Plan de Desarrollo\r\nUrbano del Municipio en forma democrática y con visión de futuro, bajo los principios de sustentabilidad,\r\nresidencia e integridad, con apoyo del IMPLAN y Consejo consultivo Municipal de Desarrollo\r\nUrbano\r\n', '2.1.1.1 ', 0, 14, NULL, NULL),
(138, 'Frenar el proceso anárquico y disperso de crecimiento de las localidades urbanas, e incrementar\r\nla inversión en infraestructura priorizando que ésta sea inclusiva, congruente con el ambiente\r\nque dé lugar a espacios públicos y viviendas acorde con las necesidades de la población, con altos\r\nniveles de conectividad multimodal y seguridad.', '2.1.1.2 ', 0, 14, NULL, NULL),
(139, 'Promover normas, lineamientos, incentivos y desincentivos que permitan avanzar hacia ciudades\r\nmás consolidadas y compactas.\r\n', '2.1.1.3 ', 0, 14, NULL, NULL),
(140, 'Elaborar los Planes Parcial es de Reordenamiento Urbano e Incorporación Territorial', '2.1.1.4 ', 0, 14, NULL, NULL),
(141, 'Elaborar las carpetas técnicas para la regeneración de las zonas susceptibles de Recuperación\r\nurbana o re densificación\r\n', '2.1.1.5 ', 0, 14, NULL, NULL),
(142, 'Garantizar la planeación sustentable, integral, rescilente con visión de futuro mediante el fortalecimiento\r\ndel Consejo Consultivo Municipal de Desarrollo Urbano y consolidación del IMPLAN', '2.1.1.6 ', 0, 14, NULL, NULL),
(143, 'Generar un proyecto preventivo y correctivo para regularizar los asentamientos irregulares y\r\nevitar su proliferación\r\n', '2.1.1.7 ', 0, 14, NULL, NULL),
(144, 'Conformar el Plan Maestro Municipal Desarrollo de Infraestructura Urbana, Conservación y\r\nRescate del Patrimonio natural, paisajístico, cultural e Histórico del municipio.', '2.1.1.8 ', 0, 14, NULL, NULL),
(145, 'Definir los límites de crecimiento de asentamientos humanos y establecer criterios sostenibles\r\nde ocupación del territorio y administrar en forma planificada el uso y ocupación del suelo, en forma\r\ntal que se mejoren las condiciones sociales y ambientales de todo el municipio.\r\n', '2.1.1.9 ', 0, 14, NULL, NULL),
(146, 'Implementar el programa de actualización del sistema catastral del municipio', '1.1.9 ', 0, 14, NULL, NULL),
(147, 'Establecer programas de planeación y ejecución que favorezcan que los asentamientos humanos\r\nsean inclusivos, seguros, resilientes y sustentables\r\n', '1.1.10 ', 0, 14, NULL, NULL),
(148, 'Fortalecer la gestión de reservas territoriales carácter federal, estatal y municipal para satisfacer\r\nla necesidad de suelo urbano en el mediano y largo plazo, donde se ubicarán los nuevos equipamientos\r\ny vivienda para los diferentes estratos socioeconómicos, así como la promoción habitacional,\r\nla construcción del equipamiento urbano necesario y la dotación de servicios públicos tomando en\r\ncuenta las necesidades de la población y su distribución en el espacio que permitan incrementar la\r\ncalidad de vida', '1.1.11 ', 0, 14, NULL, NULL),
(149, 'Lograr una adecuada distribución de la población en el territorio municipal, que permita equilibrar\r\nlas cargas y beneficios del desarrollo, atendiendo principalmente a las regiones de alta marginación\r\n', '2.1.1.12 ', 0, 14, NULL, NULL),
(150, 'Implementar un programa de reordenamiento de las delegaciones municipales que atiendan la\r\nproblemática que se vive en la zona rural del municipio', '2.1.1.13 ', 0, 14, NULL, NULL),
(151, 'Implementar el programa de regularización de construcciones\r\n', '2.1.1.14 ', 0, 14, NULL, NULL),
(152, 'Gestionar la regularización y tenencia de la tierra para los habitantes del municipio', '2.1.1.15 ', 0, 14, NULL, NULL),
(153, 'Gestionar la adquisición de reservas territoriales para el desarrollo de vivienda social.\r\n', '2.1.1.16 ', 0, 14, NULL, NULL),
(154, 'Actualizar el marco normativo en materia de desarrollo urbano municipal acorde a la política\r\nde sustentabilidad, democratización, integralidad y resliencia, para elevar los estándares de calidad,\r\ndimensión, durabilidad, localización de la vivienda y accesibilidad a transporte.', '2.1.1.17 ', 0, 14, NULL, NULL),
(155, 'Reforzar el área de inspección con la ampliación de la cobertura, vigilancia y la aplicación de\r\nla normatividad en función de que permita una mejoría sustancial del control urbano.', '2.1.1.18 ', 0, 14, NULL, NULL),
(156, 'Implementar un sistema de monitoreo que busque normalizar el crecimiento de las zona\r\ns carentes de servicios públicos e irregulares', '2.1.1.19 ', 0, 14, NULL, NULL),
(157, 'Instalar y operar la ventanilla única encargada del proceso ágil, transparente e integral de\r\nexpedición de licencias, permisos, autorizaciones, y constancias, como medida para abatir la corrupción\r\ny promover el desarrollo ordenado\r\n2.1.1.21 Mejorar la calidad y cobertura de los equipamientos y servicios públicos a toda la población,\r\ntales como agua potable y saneamiento de las aguas residuales, energía eléctrica y gestión integral\r\nde los residuos sólidos urbanos', '2.1.1.20 ', 0, 14, NULL, NULL),
(158, 'Mejorar la calidad y cobertura de los equipamientos y servicios públicos a toda la población,\r\ntales como agua potable y saneamiento de las aguas residuales, energía eléctrica y gestión integral\r\nde los residuos sólidos urbanos', '2.1.1.21 ', 0, 14, NULL, NULL),
(159, 'Hacer de Bahía un municipio próspero que mejora la calidad de vida de su población, con\r\nel suministro eficiente el suelo urbano y promueve su ocupación compacta, de tal forma que la concentración\r\nde las actividades económicas, sociales y culturales, garantice la movilidad, conectividad y prestación de servicios eficiente.\r\n', '2.1.1.22 ', 0, 14, NULL, NULL),
(160, 'Promover acciones para dotar de viviendas dignas y/o mejoramiento de e las ya existentes y\r\nregularizar la propiedad de viviendas ya existentes.', '2.1.1.23 ', 0, 14, NULL, NULL),
(161, '2.1.1.24. Colaborar en el programa de actualización del sistema catastral del municipio.', '2.1.1.24', 0, 14, NULL, NULL),
(162, 'Contribuir a la protección, preservación, y restauración del ecosistema y la biodiversidad en\r\nconjunto con la integración de todos los sectores de la sociedad en el desarrollo sustentable, mediante\r\ngestión para la conformación del plan de manejo ambiental para Bahía de Banderas\r\n', '2.1.2.1 ', 0, 15, NULL, NULL),
(163, 'Establecer el programa de prácticas sostenibles en el Municipio, tales como; movilidad sostenible,\r\necoturismo, uso de eco tecnologías en el medio rural y urbano, recuperación y aprovechamiento\r\nde ecosistemas municipales para mitigar y prevenir la pérdida de la biodiversidad', '2.1.2.2 ', 0, 15, NULL, NULL),
(164, 'Realizar acciones y gestiones para detener los procesos de deterioro de los ecosistemas, así\r\ncomo, poner en marcha proyectos de restauración ecológica en el territorio municipal\r\n', '2.1.2.3 ', 0, 15, NULL, NULL),
(165, 'Promover una agenda de resiliencia y gestión de riesgos en todo el municipio', '2.1.2.4 ', 0, 15, NULL, NULL),
(166, 'Impulsar la educación y cultura ambiental\r\n', '2.1.2.5 ', 0, 15, NULL, NULL),
(167, 'Implementar un programa de reforestación del Municipio con vegetación endémica y nativa.', '2.1.2.6 ', 0, 15, NULL, NULL),
(170, 'Incrementar los espacios verdes en la zona urbana del Municipio\r\n', '2.1.2.7 ', 0, 15, NULL, NULL),
(171, 'Coadyuvar con las autoridades competentes en la integración y difusión de campañas y ejecución\r\nde proyectos de conservación, mantenimiento, regeneración, aprovechamiento sustentable y\r\ncertificación de las playas del municipio y zonas naturales protegidas como la sierra de Vallejo y las\r\nMarietas', '2.1.2.8 ', 0, 15, NULL, NULL),
(172, 'Coadyuvar con las autoridades competentes en la difusión de campañas y ejecución de proyectos\r\nde saneamiento de los ríos y cuencas, especialmente la Cuenca Alimentadora Distrito de Riego No. 43 y declaración de zona protegidas a la Sierra Vallejo y Laguna de Quelele', '2.1.2.9 ', 0, 15, NULL, NULL),
(173, 'Promover acciones en coordinación con autoridades estatales y federales y sociedad civil organizada\r\na fin de proteger las áreas con alto valor ambiental para la ciudad y aprovechar los baldíos\r\nurbanos con fines ambientales, deportivos', '2.1.2.10 ', 0, 15, NULL, NULL),
(174, 'Gestionar acciones para rehabilitar y conectar para sacar a valor los diferentes ámbitos de\r\ninterés ambiental mediante un sistema de corredores verdes, que cuenten con vías blandas para peatones\r\no bicicleta\r\n', '2.1.2.11 ', 0, 15, NULL, NULL),
(175, 'Proponer políticas de ahorro de agua, luz y papel, dentro de los edificios públicos municipales', '2.1.2.12 ', 0, 15, NULL, NULL),
(176, 'Gestionar ante las autoridades estatales, federales y los las municipales de los municipios\r\ncolindantes las acciones necesarias para lograr la metropolización formal para impulsar proyectos\r\nconjuntos en beneficio del desarrollo regional\r\n', '2.1.2.13 ', 0, 15, NULL, NULL),
(177, 'Impulsar el Proyecto Santuario de la Iguana para su aprovechamiento turístico en forma sustentable', '2.1.2.14 ', 0, 15, NULL, NULL),
(178, 'Realizar proyectos y acciones de rehabilitación, mantenimiento y cuidado de las fuentes de\r\nabastecimiento de agua en el municipio.\r\n', '2.1.3.1 ', 0, 16, NULL, NULL),
(179, 'Gestionar la realización de estudios para el programa de perforación y equipamiento de nuevas\r\nfuentes de abastecimiento de agua potable (pozos profundos)\r\n', '2.1.3.2 ', 0, 16, NULL, NULL),
(180, 'Desarrollar los programas y acciones para modificar las tendencias de degradación del recurso\r\nhídrico con enfoque de participación en la planeación y gestión articuladora e integradora, respondiendo\r\nal enfoque eco sistémico de planificación del recurso\r\n', '2.1.3.3 ', 0, 16, NULL, NULL),
(181, 'Lograr el diseño participativo y prioritario del programa de monitoreo, rehabilitación y mantenimiento\r\nintegral de la infraestructura de la red de conducción de agua, para disminuir el problema\r\nde fugas y tomas clandestinas del vital líquido.\r\n', '2.1.3.4 ', 0, 16, NULL, NULL),
(182, 'Desarrollar el proyecto integral de manejo de micro cuencas, agua y saneamiento, en las que\r\nse contemplen cuando menos acciones para la gestión de letrina e inodoro ecológico, cosecha del\r\nagua, filtros para aguas domiciliadas, reducir la vulnerabilidad de micro cuencas y garantizar agua de\r\ncalidad en la comunidad.\r\n', '2.1.3.5 ', 0, 16, NULL, NULL),
(183, 'Incrementar la calidad de agua que se abastece en el municipio\r\n', '2.1.3.6 ', 0, 16, NULL, NULL),
(184, 'Implementar un programa de macro medición, sectorización y potabilización el servicio de agua,\r\nque permitan un mejor manejo del recurso\r\n', '2.1.3.7 ', 0, 16, NULL, NULL),
(185, 'Modernizar la infraestructura para eficientizar el servicio del OROMAPAS\r\n', '2.1.3.8 ', 0, 16, NULL, NULL),
(186, 'Promover la concientización ciudadana sobre el cuidado del agua como recurso finito y valor\r\nreal.', '2.1.3.9 ', 0, 16, NULL, NULL),
(187, 'Garantizar el acceso de agua potabilizada a la población escolar del municipio y zonas marginadas\r\n', '2.1.3.10 ', 0, 16, NULL, NULL),
(188, 'Reducir la contaminación del agua, por actividades sociales, comerciales y de la industria,\r\nmediante una óptima vigilancia y promover la limpieza de ríos y canales\r\n', '2.1.3.11 ', 0, 16, NULL, NULL),
(189, 'Garantizar el auto sustentabilidad del OROMAPAS mediante eficiencia en el manejo de recursos,\r\nla actualización de padrón y reducción de cartera vencida\r\n', '2.1.3.12 ', 0, 16, NULL, NULL),
(190, 'Coordinarse con autoridades municipales, estatales y federales para asegurar que los nuevos\r\ndesarrollos urbanos y turísticos cuenten con infraestructura para el tratamiento y reutilización de\r\naguas residuales\r\n', '2.1.3.13 ', 0, 16, NULL, NULL),
(191, 'Gestionar ante las autoridades municipales, estatales y federas las acciones para proteger la\r\ndegradación y contaminación de los cuerpos hídricos, entubamiento de los canales de desagüe que\r\natraviesan toda la localidad de Valle Dorado así como el desazolve del vaso de captación\r\n', '2.1.3.14 ', 0, 16, NULL, NULL),
(192, 'Coordinar un estudio hídrico que contemple un mapeo de cuerpos de agua y lograr la certificación\r\nde huella hídrica, encauzamiento del arroyo el Caloso, desazolve de los vasos de captación\r\n', '2.1.3.15 ', 0, 16, NULL, NULL),
(193, 'Mediante Programa de Desazolve proporcionar mantenimiento preventivo a las líneas de drenaje\r\ny alcantarillado con atención oportuna de los reportes ciudadanos\r\n', '2.1.3.16 ', 0, 16, NULL, NULL),
(194, 'Aumentar el porcentaje de aguas tratadas en el municipio, mediante el aprovechamiento al\r\n100% de su capacidad en forma eficiente las plantas de tratamiento de aguas\r\n', '2.1.3.17 ', 0, 16, NULL, NULL),
(195, 'Proyecto de instalación de colectores de aguas pluviales\r\n', '2.1.3.18 ', 0, 16, NULL, NULL),
(196, 'Consolidar y mantener el tratamiento de aguas residuales y lograr los mecanismos de aprovechamiento', '2.1.3.19 ', 0, 16, NULL, NULL),
(197, 'Formular e implementar el programa Maestro Para el manejo integral de residuos solidos\r\n', '2.1.4.1 ', 0, 17, NULL, NULL),
(198, 'Realizar campañas de concientización ciudadana sobre la importancia de su participación en\r\nlas políticas de reducir, reciclar, reutilizar y cuidar que la disposición final de los residuos sólidos se\r\nrealice correctamente, generando la sinergia para la colaboración en la denuncia y limpieza de basureros\r\nclandestinos\r\n', '2.1.4.2 ', 0, 17, NULL, NULL),
(199, 'Gestionar con las instancias competentes que la disposición final de los residuos sólidos en el\r\nmunicipio se ajuste a las normas en la materia (predio los Brasiles). De acuerdo a la norma\r\n', '2.1.4.3 ', 0, 17, NULL, NULL),
(200, 'Realizar las gestiones necesarias para lograr el aprovechamiento relleno sanitario región Compostela\r\n', '2.1.4.4 ', 0, 17, NULL, NULL),
(201, 'Implementar en coordinación con las autoridades municipales y sociedad en general, las Jornadas\r\nde limpieza y descacharrizaciòn en tu colonia\r\n', '2.1.4.5 ', 0, 17, NULL, NULL),
(202, 'Eficientar la cobertura y la frecuencia del servicio de limpia y barrido y recolección de desechos\r\nen espacios públicos\r\n', '2.1.4.6 ', 0, 17, NULL, NULL),
(203, 'Incentivar el Programa de Recolección diferenciada de los residuos sólidos urbanos a efecto de\r\ncontribuir a la conservación del medio ambiente', '2.1.4.7 ', 0, 17, NULL, NULL),
(204, 'Levantar inventario generadores de residuos de manejo especial y hacer los reportes correspondientes\r\nen la materia al Gobierno Estatal', '2.1.4.8 ', 0, 17, NULL, NULL),
(211, 'Ampliar la red de alumbrado público, bajo los criterios de concentración ciudadana y reducción\r\nde índice delictivo, en zonas de conflicto, áreas públicas, partes y espacios de esparcimiento y\r\nrecreación', '2.1.5.1 ', 0, 18, NULL, NULL),
(212, 'Avanzar hacia la modernización del sistema de alumbrado público mediante el aprovechamiento\r\nde tecnologías más eficientes y energía alternativa\r\n', '2.1.5.2 ', 0, 18, NULL, NULL),
(213, 'Establecer el programa de poda de árboles para una mejor iluminación y limpieza de lotes\r\nbaldíos para la seguridad\r\n', '2.1.5.3 ', 0, 18, NULL, NULL),
(214, 'Atender oportunamente todos los reportes de alumbrado público\r\n', '2.1.5.4 ', 0, 18, NULL, NULL),
(215, 'Proponer estudios y acciones para reducir el consumo de energía eléctrica en alumbrado público\r\n', '2.1.5.5', 0, 18, NULL, NULL),
(216, 'Gestionar ante las autoridades competentes la ampliación de la cobertura a comunidades que\r\ncarezcan de energía eléctrica', '2.1.5.6 ', 0, 18, NULL, NULL),
(233, 'Gestionar con las autoridades estatales y sociedad civil organizada, la conformación de la\r\nagenda de movilidad sustentable multimodal (, bicicleta, peatón, transporte público y privado a fin de\r\noptimizar los traslados, reducir sus costos (económicos y ambientales) e impulsar la productividad de\r\nla ciudad.\r\n', '2.1.6.1', 0, 19, NULL, NULL),
(234, 'Gestionar con las autoridades estatales y sociedad civil organizada la consolidación del modelo\r\ndel Desarrollo de transporte sustentable\r\n', '2.1.6.2 ', 0, 19, NULL, NULL),
(235, 'Dignificar los paradores y áreas de espera del transporte público de la Ciudad.\r\n', '2.1.6.3 ', 0, 19, NULL, NULL),
(236, 'Atender el rezago de la conectividad peatonal y movilidad segura de ciclistas, adultos mayores,\r\npersonas con capacidades diferentes en el Municipio\r\n', '2.1.6.4 ', 0, 19, NULL, NULL),
(237, 'Establecer criterios y lineamientos que garanticen el uso adecuado de las banquetas del Municipio\r\n', '2.1.6.5 ', 0, 19, NULL, NULL),
(238, 'Implementar acciones planeación para la consolidación y cohesión de ciudad, así como la planeación de los sistemas multimodales, sustentables, modernos, eficientes, y accesibles\r\n', '2.1.6.6 ', 0, 19, NULL, NULL),
(239, 'Gestionar con las autoridades competentes e iniciativa privada la construcción y mejoramiento\r\nde infraestructura de comunicación que facilite la movilidad multimodal, accesible y segura como una\r\npolítica trasversal para garantizar la competitividad, el desarrollo sustentable y la seguridad\r\n', '2.1.6.7', 0, 19, NULL, NULL),
(240, 'Establecer un programa para administrar, gestionar y mantener la infraestructura vial existente\r\nen óptimas condiciones y que permitan la movilidad de más personas y bienes, priorizando el uso de\r\nla vialidad para transporte público, peatones y ciclistas, para lo cual se Priorizará en el presupuesto\r\nde obra pública para mejoramiento de vialidades', '2.1.6.8 ', 0, 19, NULL, NULL),
(241, 'Realizar los estudios, obras, acciones y gestiones con todos los actores públicos y sociales\r\npara lograr la conectividad integral en el municipio\r\n', '2.1.7.1 ', 0, 20, NULL, NULL),
(242, 'Promover y gestionar ante las autoridades competentes y el sector privado la ampliación de\r\nla cobertura y accesibilidad de la población en todo el municipio de TICS (tecnologías de la información\r\ny comunicación) con énfasis en las viviendas de menor ingreso, espacios públicos, escuelas y\r\nunidades productivas', '2.1.7.2 ', 0, 20, NULL, NULL),
(243, 'Establecer el programa de rehabilitación y rescate de la imagen urbana, compatible con la\r\nedificación tradicional y características propias de la identidad de cada zona en el municipio\r\n', '2.1.8.1 ', 0, 21, NULL, NULL),
(244, 'Mejorar de la imagen y los espacios urbanos, mediante la creación y recuperación de calles,\r\npaseos, plazas; dinamizando actividades, introducir más vegetación y racionalizar la relación entre\r\nlos diferentes modos de movilidad generando nuevos ámbitos urbanos de fusión diversos y atractivos\r\n', '2.1.8.2 ', 0, 21, NULL, NULL),
(245, 'Dar mantenimiento, rehabilitación y embellecimiento de camellones, áreas verdes y arboladas\r\nen espacios públicos, parques, plazas y jardines municipales.\r\n', '2.1.8.3 ', 0, 21, NULL, NULL),
(246, 'Reconocer y difundir entre la población el valor y necesidad de apropiación de la imagen urbana\r\ncomo elemento de bienestar\r\n', '2.1.8.4 ', 0, 21, NULL, NULL),
(247, 'Establecer las bases normativas y técnicas para la integración del concepto de imagen urbana\r\ncomo parte del ordenamiento territorial.\r\n', '2.1.8.5 ', 0, 21, NULL, NULL),
(248, 'Realizar una revisión de los programas de desarrollo urbano, para integrar la imagen urbana\r\n', '2.1.8.6 ', 0, 21, NULL, NULL),
(249, 'Integrar el concepto de imagen urbana en la elaboración de los proyectos de introducción y\r\nmejoramiento de los servicios y equipamiento urbano.\r\n', '2.1.8.7', 0, 21, NULL, NULL),
(250, 'Fortalecer los espacios públicos, a partir de sus posibilidades reales de sustentabilidad económica\r\ny ecológica.\r\n', '2.1.8.8 ', 0, 21, NULL, NULL),
(251, 'Regular la instalación desordenada de anuncios y demás elementos en vía pública del municipio\r\n', '2.1.8.9 ', 0, 21, NULL, NULL),
(252, 'Fortalecer la política de prohibir la instalación de anuncios espectaculares en las vialidades\r\nprimarias de la ciudad\r\n', '2.1.8.9 ', 0, 21, NULL, NULL),
(253, 'Realizar estudios para lograr que la dotación de mobiliario urbano sea adecuado a la función\r\ny carácter de cada zona del municipio', '2.1.8.10', 0, 21, NULL, NULL),
(254, 'Gestionar ante las autoridades estatales y federales subsidios para construcción de vivienda\r\npara las personas que menos tienen, en condiciones de conectividad, acceso a servicios, empleo y\r\ncalidad de vida, diversificando las acciones en materia de vivienda, mediante programas de construcción,\r\nautoconstrucción y mejoramiento, atendiendo los rezagos existentes\r\n', '2.1.9.1 ', 0, 22, NULL, NULL),
(255, 'Gestionar ante las autoridades estatales y federales subsidios para la rehabilitación de la vivienda\r\nde zonas marginadas\r\nInstrumentar políticas y programas que incentiven la ocupación de viviendas que han sido abandonadas', '2.1.9.2 ', 0, 22, NULL, NULL),
(256, 'Emprender programas de regeneración urbana, relocalización, regularización, ampliación y mejoramiento\r\nde vivienda en asentamientos informales, barrios y colonias deterioradas\r\n', '2.1.9.3 ', 0, 22, NULL, NULL),
(257, 'Incorporar políticas en la planeación urbana y en los programas habitacionales, que garanticen\r\npara la nueva vivienda una localización dentro del tejido urbano o en su periferia inmediata, en zonas\r\nadecuadas, sin riesgos y con la mínima afectación ambiental.\r\n', '2.1.9.4 ', 0, 22, NULL, NULL),
(258, 'Gestionar ante las autoridades del estado y federales recursos a fin de abatir el rezago en\r\nmateria de vivienda en el municipio que genera el hacinamiento u ocupación de zonas peligrosas,\r\nasí como para ampliar la durabilidad de los materiales de construcción asociados a las condiciones\r\ngeográficas. Brindar apoyos para el mejoramiento de la vivienda a grupos vulnerables con carencia de\r\ncalidad y espacios en vivienda.\r\n', '2.1.9.5 ', 0, 22, NULL, NULL),
(259, 'Gestionar la incorporación de habitantes de Bahía de Banderas a programas federales y estatales,\r\nque atiendan la carencia por calidad y espacios de la vivienda\r\n', '1.9.6 ', 0, 22, NULL, NULL),
(260, 'Gestionar la realización de obras de electrificación para viviendas en zonas marginadas.\r\n', '2.1.9.7', 0, 22, NULL, NULL),
(261, 'Gestionar y/o crear reservas territoriales en el municipio para contribuir al desarrollo urbano\r\nordenado y sustentable, con servicios integrales.\r\n', '2.1.9.8 ', 0, 22, NULL, NULL),
(262, 'Simplificar y agilizar los trámites para la creación de colonias populares, fraccionamientos de\r\ninterés social y social-progresivo.\r\n', '2.1.9.9 ', 0, 22, NULL, NULL),
(263, 'Simplificar y agilizar los trámites para la construcción de viviendas.\r\n', '2.1.9.10 ', 0, 22, NULL, NULL),
(264, 'Facilitar el acceso a la vivienda digna mediante diversos esquemas y programas.\r\n', '2.1.9.11 ', 0, 22, NULL, NULL),
(265, 'Coordinarse con las instituciones, dependencias y organismos descentralizados de los tres\r\nórdenes de gobierno, así como con organismos privados de asistencia social en materia de vivienda\r\ncon el propósito de impulsar programas de vivienda de interés social.\r\n', '2.1.9.12 ', 0, 22, NULL, NULL),
(266, 'Promover la certeza jurídica de la propiedad de la vivienda a través de la regularización y\r\nescrituración\r\n', '2.1.9.13 ', 0, 22, NULL, NULL),
(267, 'Promover la escrituración de lotes y viviendas que no cuentan con certeza jurídica.', '1.9.14 ', 0, 22, NULL, NULL),
(268, 'Realizar la matanza con apego a las normas de calidad, sanidad y humanitaria de ganado\r\nvacuno, porcino, ovino y aviar para el abasto de la población\r\n', '2.1.10.1 ', 0, 23, NULL, NULL),
(269, 'Realizar los procedimientos de planeación, organización y administración en la recepción,\r\ndesembarque, arreo, insensibilización, sangrado, eviscerado, pelado, numerado, lavado y rajado del\r\nganado, bajo una constante inspección sanitaria, el pesaje y estibado del mismo, de acuerdo a las\r\nnormas de control para que el consumo de la carne sea de calidad para la población.\r\n', '2.1.10.2 ', 0, 23, NULL, NULL),
(270, 'Reducción de tiempos en hacer la entrega de canal\r\n', '2.1.10.3 ', 0, 23, NULL, NULL),
(271, 'Cuidar la cadena de frio en el reparto de carnes', '2.1.10.4 ', 0, 23, NULL, NULL),
(272, 'Promover el desarrollo económico integral, diversificado y sustentable\r\n', '3.1.1.1 ', 0, 24, NULL, NULL),
(273, 'Lograr la modernización y simplificación administrativa\r\n', '3.1.1.2 ', 0, 24, NULL, NULL),
(274, 'Ampliar y mejorar la calidad de los servicios públicos y la infraestructura municipal\r\n', '3.1.1.3 ', 0, 24, NULL, NULL),
(275, 'Promover la mejora regulatoria dentro del gobierno municipal que facilite la operación de las\r\nempresas', '3.1.1.4 ', 0, 24, NULL, NULL),
(276, 'Optimizar la Ventanilla Única mediante la mejora regulatoria.\r\n', '3.1.1.5 ', 0, 24, NULL, NULL),
(277, 'Reducir el tiempo de aperturas de empresas y negocios\r\n', '3.1.1.6 ', 0, 24, NULL, NULL),
(278, 'Construcción de vialidades primarias y secundarias', '3.1.1.7 ', 0, 24, NULL, NULL),
(279, 'Promover la inversión privada en las vocaciones productivas del Municipio\r\n', '3.1.2.1 ', 0, 25, NULL, NULL),
(280, 'Generar la promoción público privada del desarrollo económico\r\n', '3.1.2.2 ', 0, 25, NULL, NULL),
(281, 'Gestionar la presencia del sector financiero involucrado con las estrategias de desarrollo\r\nterritorial en el municipio\r\n', '3.1.3.3 ', 0, 25, NULL, NULL),
(282, 'Generar un sistema de contribuciones municipales, y el marco jurídico apropiados para el\r\nfomento del desarrollo económico\r\n', '3.1.3.4 ', 0, 25, NULL, NULL),
(283, 'Difundir a nivel nacional e internacional los criterios de rentabilidad de inversión en el Municipio\r\n', '3.1.3.5 ', 0, 25, NULL, NULL),
(284, 'Establecer acuerdos participativos de todos los sectores a favor del desarrollo municipal, que\r\ntrasciendan las gestiones municipales\r\n', '3.1.3.6 ', 0, 25, NULL, NULL),
(285, 'Promocionar las actividades económicas y turísticas del Municipio\r\n', '3.1.3.7 ', 0, 25, NULL, NULL),
(286, 'Generar la plataforma del territorio competitivo, en la que se contenga información dinámica\r\ny actualizada sobre oportunidades de desarrollo que le permita a inversores tomar decisión de\r\ninvertir en el municipio.\r\n', '3.1.3.8 ', 0, 25, NULL, NULL),
(287, 'Fomentar la concertación público privada en la competitividad territorial\r\n', '3.1.3.9 ', 0, 25, NULL, NULL),
(288, 'Fortalecer la promoción del Municipio a nivel nacional e internacional, atendiendo foros nacionales\r\ne internacionales para atraer mayor inversión y generar más y mejores empleos.\r\n', '3.1.3.10 ', 0, 25, NULL, NULL),
(289, 'Generar incentivos para atraer nuevas inversiones en la industria de la transformación de\r\nproductos primarios.', '3.1.3.11 ', 0, 25, NULL, NULL),
(290, 'Incentiva y apoyar para la asociación de pequeños productores de las diferentes actividades\r\neconómicas en las delegaciones del Municipio\r\n', '3.1.4.1', 0, 26, NULL, NULL),
(291, 'Gestionar con la autoridad locales y federal, así como, entidades privadas apoyos y programas\r\npara la producción, transformación y comercialización de los productos pesqueros, agrícolas y\r\nganaderos del municipio\r\n', '3.1.4.2 ', 0, 26, NULL, NULL),
(292, 'Difundir y fomentar los beneficios del consumo de productos locales, haciendo partícipes a\r\nlos habitantes de las comunidades rurales del Municipio\r\n', '3.1.4.3 ', 0, 26, NULL, NULL),
(293, 'Impulsar y promocionar la oferta de productos locales en mercados regionales, locales, nacionales\r\ne internacionales para el posicionamiento de sus productos', '3.1.4.4 ', 0, 26, NULL, NULL),
(294, 'Promover la capacitación para la introducción de innovaciones en los sistemas productivos\r\n', '3.1.4.5 ', 0, 26, NULL, NULL),
(295, 'Crear mayores oportunidades de mercado para los sectores agrarios, pesqueros y ganaderos\r\n', '3.1.4.6 ', 0, 26, NULL, NULL),
(296, 'Incentivar acciones de capacitación y asesoría para que los productores de los sectores\r\nagrarios, pesquero y ganadero\r\n', '3.1.4.7 ', 0, 26, NULL, NULL),
(297, 'Incrementar la participación de las empresas locales en la red de proveeduría de empresas\r\ndel sector turístico\r\n', '3.1.4.8 ', 0, 26, NULL, NULL),
(298, 'Promover la creación de mercados y espacios públicos para la exposición y venta de artesanías\r\nen la zona urbana y comunidades rurales del Municipio', '3.1.4.9 ', 0, 26, NULL, NULL),
(299, 'Fomentar la capacitación para el empleo de personas que buscan insertarse en el mercado\r\nlaboral formal y fortalecer la vinculación con el sector productivo para la articulación entre oferentes\r\ny demandantes de trabajo que permita elevar la colocación\r\n', '3.1.5.1', 0, 27, NULL, NULL),
(300, 'Impulsar la orientación y vinculación laboral de los buscadores de empleo de acuerdo con\r\nsu perfil y expectativas laborales\r\n', '3.1.5.2 ', 0, 27, NULL, NULL),
(301, 'Desarrollar sistemas electrónicos y medios de difusión impresos amigables y accesibles para\r\nlos que solicitan empleo\r\n', '3.1.5.3 ', 0, 27, NULL, NULL),
(302, 'Promover ante el sector empresarial cultura de inclusión laboral y gestionar acuerdos para la\r\ncontratación de personal e impulsar la colocación de jóvenes y personas con capacidades diferentes\r\ny adultos mayores\r\n', '3.1.5.4 ', 0, 27, NULL, NULL),
(303, 'Establecer alianzas estratégicas entre empresas empleadoras y las autoridades estatales en la\r\nmateria para la realización de ferias del empleo\r\n', '3.1.5.5 ', 0, 27, NULL, NULL),
(304, 'Gestionar con las autoridades locales la implementación de campañas de alfabetización y a\r\ncapacitación para el autoempleo y el trabajo\r\n', '3.1.5.6 ', 0, 27, NULL, NULL),
(305, 'Brindar asesoría y capacitación para que los sectores económicos, accedan a infraestructura\r\ny tecnología de desarrollo productivo\r\n', '3.1.5.7 ', 0, 27, NULL, NULL),
(306, 'Gestionar ante autoridades estatales, federales e iniciativa privada becas de capacitación\r\npara el trabajo.\r\n', '3.1.5.8 ', 0, 27, NULL, NULL),
(307, 'Establecer esquemas de vinculación con el sector educativo y empresarial para la formación\r\nde técnicos y profesionistas de acuerdo a las necesidades del mercado laboral para su contratación\r\n', '3.1.5.9 ', 0, 27, NULL, NULL);
INSERT INTO `pm_lineas_accion` (`id`, `nombre`, `punto`, `status`, `pm_estrategia_id`, `created_at`, `updated_at`) VALUES
(308, 'Promover con las autoridades locales y federales el programa de certificación de las competencias,\r\nlaborales\r\n', '3.1.5.10 ', 0, 27, NULL, NULL),
(309, 'Gestionar con el Gobierno Local y Federal un programa de empleo temporal que atienda a\r\nlas zonas de atención prioritaria\r\n', '3.1.5.11 ', 0, 27, NULL, NULL),
(310, 'Capacitar a los estudiantes, jóvenes y personas interesadas en materia financiera, atención\r\nal cliente, e innovación\r\n', '3.1.5.12 ', 0, 27, NULL, NULL),
(311, 'Fomentar las prácticas de Responsabilidad Social Empresarial en los diversos sectores y\r\ncomunidades del Municipio\r\n', '3.1.5.13 ', 0, 27, NULL, NULL),
(312, 'Coordinar capacitaciones en temas de negocios y emprendimiento a las\r\n', '3.1.5.14 ', 0, 27, NULL, NULL),
(313, 'Atención oportuna a micro y pequeños empresarios a través de acciones de capacitación, asistencia técnica.\r\n', '3.1.5.15 ', 0, 27, NULL, NULL),
(314, 'Vincular el financiamiento público y privado a micro y pequeños empresarios\r\n', '3.1.5.16 ', 0, 27, NULL, NULL),
(315, 'Difundir y ejecutar los programas estatales y federales enfocados al empleo.\r\n', '3.1.5.17 ', 0, 27, NULL, NULL),
(316, 'Hacer promoción de los servicios municipales de empleo a través de las redes sociales y el\r\nportal de internet del municipio.\r\n', '3.1.5.18 ', 0, 27, NULL, NULL),
(317, 'Actualizar el Portal Municipal de Empleo permanentemente.\r\n', '3.1.5.19 ', 0, 27, NULL, NULL),
(318, 'Beneficiar mediante el programa de empleo temporal a personas en condiciones de vulnerabilidad', '3.1.5.20 ', 0, 27, NULL, NULL),
(319, 'Simplificar la apertura de empresas locales en la administración municipal\r\n', '3.1.6.1 ', 0, 28, NULL, NULL),
(320, 'Regularización de comercios y servicios en el municipio\r\n', '3.1.6.2 ', 0, 28, NULL, NULL),
(321, 'Gestionar proyectos productivos en las delegaciones municipales.\r\n', '3.1.6.3 ', 0, 28, NULL, NULL),
(322, 'Impulsar el emprendurismo y apoyar en gestión para el financiamiento de proyectos productivos\r\ny de desarrollo\r\n', '3.1.6.4 ', 0, 28, NULL, NULL),
(323, 'Mejorar los espacios públicos e imagen rural y urbana, en coordinación con la sociedad y\r\nempresarios, para su potenciación económica y turística\r\n', '3.1.6.5 ', 0, 28, NULL, NULL),
(324, 'Fomento a las microempresas y pequeñas y medianas empresas\r\n', '3.1.6.6', 0, 28, NULL, NULL),
(325, 'Gestionar apoyo y capacitación a grupos vulnerables para emprender actividades productivas\r\n', '3.1.6.7 ', 0, 28, NULL, NULL),
(326, 'Apoyar y fomentar la formalización de las actividades económicas\r\n', '3.1.6.8 ', 0, 28, NULL, NULL),
(327, 'Gestionar con las instancias de gobierno y las cámaras y organizaciones privadas la creación\r\nde incubadoras de micro empresas\r\n', '3.1.6.9', 0, 29, NULL, NULL),
(328, 'Promover el fortalecimiento de las redes de cooperación municipales\r\n', '3.1.6.10 ', 0, 28, NULL, NULL),
(329, 'Apoyar la generación de proyectos productivos de pequeña y mediana inversión para grupos\r\nvulnerables como son: mujeres, jóvenes, adultos mayores, indígenas, capacidades especiales, entre\r\notras; contribuyendo a la equidad social en las oportunidades de autoempleo.', '3.1.6.11 ', 0, 28, NULL, NULL),
(330, 'Promover el desarrollo de actividades agropecuarias, ganaderas y pesqueras de alta rentabilidad\r\n', '3.1.7.1 ', 0, 29, NULL, NULL),
(331, 'Impulsar el desarrollo y aprovechamiento de mercados para incrementar la competitividad de los sectores\r\n', '3.1.7.2 ', 0, 29, NULL, NULL),
(332, 'Promover la inversión pública y privada en las actividades en dichos sectores que permitan\r\nuna mayor integración, generando un incremento en el valor económico\r\n', '3.1.7.3 ', 0, 29, NULL, NULL),
(333, 'Promover la inversión pública y privada para el desarrollo de proyectos estratégicos agropecuarios,\r\npesqueros\r\n', '3.1.7.4', 0, 29, NULL, NULL),
(334, 'Impulsar la competitividad a través de la capacitación a los productores de herramientas y\r\nhabilidades administrativas y técnicas para impulsar la cultura empresarial\r\n', '3.1.7.5 ', 0, 29, NULL, NULL),
(335, 'Gestionar la reparación y modernización de canales dañados\r\n', '3.1.7.6', 0, 29, NULL, NULL),
(336, 'Gestionar entre la C.N-C las acciones necesarias para mantener en óptimo funcionamiento la\r\ninfraestructura de cada uno de los distintos distritos de riego\r\n', '3.1.7.7 ', 0, 29, NULL, NULL),
(337, 'Colaborar en la integración de proyectos productivos competitivos que les permitan ingresar\r\no mejorar su posición en los mercados nacional e internacional\r\n', '3.1.7.8 ', 0, 29, NULL, NULL),
(338, 'Gestionar de infraestructura necesaria para hacer rentable al campo y atraer inversiones en\r\nactividades agrícolas, pecuarias y de trasformación de productos primarios.\r\n', '3.1.7.9 ', 0, 29, NULL, NULL),
(339, 'Concientizar a los productores el manejo y cuidado de los recursos y medio ambiente para\r\nel desarrollo de sus actividades económicas con visión de sustentabilidad\r\n', '3.1.7.10 ', 0, 29, NULL, NULL),
(340, 'Colaborar con el sistema de seguimiento y control de los sectores y lograr el involucramiento\r\nsocial\r\n', '3.1.7.11 ', 0, 29, NULL, NULL),
(341, 'Colaborar en la conformación de las bases de información que facilite la toma de decisiones\r\na las autoridades competentes\r\n', '3.1.7.12 ', 0, 29, NULL, NULL),
(342, 'Apoyar en la gestión de recursos para los sectores productivos\r\n', '3.1.7.13 ', 0, 29, NULL, NULL),
(343, 'Apoyar campañas de formalización de las organizaciones de cada sector e Impulsar programas\r\npara emprendedores', '3.1.7.14 ', 0, 29, NULL, NULL),
(344, 'Gestionar con los gobiernos del Estado y Federal, así como con el sector empresarial y sociedad\r\ncivil organizada el fortalecimiento sustentable e impulso al proyecto turístico Riviera Nayarit.\r\n', '3.1.8.1 ', 0, 30, NULL, NULL),
(345, 'Impulsar el Proyecto turístico Sierra Vallejo.\r\n', '3.1.8.2 ', 0, 30, NULL, NULL),
(346, 'Promover la cultura de atención y orientación oportuna al turista.\r\n', '3.1.8.3 ', 0, 30, NULL, NULL),
(347, 'Promover entre las autoridades locales y federales, así como iniciativa privada acciones para\r\ngarantizar la internación y desplazamiento seguro y eficiente de los turistas en el municipio.\r\n', '3.1.8.4 ', 0, 30, NULL, NULL),
(348, 'Fortalecer a Bahía de Banderas como un destino seguro.\r\n', '3.1.8.5 ', 0, 30, NULL, NULL),
(349, 'El coordinación con todos los actores del sector turístico y sociedad establecer e implementa\r\nlos protocolos de atención oportuna al turista.\r\n', '3.1.8.6 ', 0, 30, NULL, NULL),
(350, 'Promover en las escuelas cursos relativos a la importancia del turismo en nuestra economía\r\ny estrategias ciudadanas para fortalecer el sector.\r\n', '3.1.8.7 ', 0, 30, NULL, NULL),
(351, 'Impulsar el desarrollo de la infraestructura turística en todas las regiones para para fortalecer\r\nlas vocaciones de cada una.\r\n', '3.1.8.8 ', 0, 30, NULL, NULL),
(352, 'Diversificar y consolidar la oferta turísticaconcertando con las empresas de bienes y servicios\r\nturísticos en el municipio, Promover turismo de Convenciones.\r\n', '3.1.8.9 ', 0, 30, NULL, NULL),
(353, 'Ampliar la promoción de los lugares turísticos de Bahía de Banderas\r\n', '3.1.8.10 ', 0, 30, NULL, NULL),
(354, 'promover turismo de convenciones.\r\n', '3.1.8.11 ', 0, 30, NULL, NULL),
(355, 'Impulsar y establecer en conjunto con la comunidad de Valle de Banderas para promover la\r\nconsolidación como pueblo mágico.', '3.1.8.12 ', 0, 30, NULL, NULL),
(356, 'Incrementar la promoción turística con apoyo de la marca Riviera Nayarit, fortalecer la identidad\r\nmunicipal para la vinculación turística municipal con la participación de hoteles y empresas de\r\nbienes y servicios turísticos.\r\n', '3.1.9.1 ', 0, 31, NULL, NULL),
(357, 'Realizar eventos de carácter promocional y de tradición en los diferentes lugares con atractivos\r\nturísticos del municipio.\r\n', '3.1.9.2 ', 0, 31, NULL, NULL),
(358, 'Organizar Ferias y exposiciones de carácter comercial para la exposición y promoción de los\r\nempresarios locales y que promuevan la visita y consumo de ciudadanos nacionales e internacionales.\r\n', '3.1.9.3 ', 0, 31, NULL, NULL),
(359, 'Promover entre los prestadores de servicios la importancia de incrementar la calidad de los\r\nservicios de recorridos turísticos y vincularlos a las empresas hoteleras establecidas en el municipio.\r\n', '3.1.9.4 ', 0, 31, NULL, NULL),
(360, 'Promover la artesanía local con la Feria Artesanal Anual dentro y fuera del municipio.\r\n', '3.1.9.5 ', 0, 31, NULL, NULL),
(361, 'Vincular a las empresas de bienes y servicios turísticos con certificaciones que permitan a\r\nBahía de Banderas colocarse en un mejor posicionamiento en la materia.\r\n', '3.1.9.6 ', 0, 31, NULL, NULL),
(362, 'Difusión y promoción de nuestra riqueza natural, urbana, gastronómica y cultural, para diversificar\r\nel turismo.\r\n', '3.1.9.7 ', 0, 31, NULL, NULL),
(363, 'Actualizar el padrón de unidades y zonas económicas turísticas.\r\n', '3.1.9.8 ', 0, 31, NULL, NULL),
(364, 'Elaborar un programa de parques temáticos', '3.1.9.10 ', 0, 31, NULL, NULL),
(365, 'Implementar y en su caso gestionar con autoridades competentes una adecuada reglamentación\r\nde imagen urbana y protección al patrimonio cultural y natural.\r\n', '3.1.10.1 ', 0, 32, NULL, NULL),
(366, 'Recuperación de espacios públicos y la creación de zonas de valor que incrementen los servicios\r\nturísticos.\r\n', '3.1.10.2 ', 0, 32, NULL, NULL),
(367, 'Rescate de los sitios turísticos e imagen urbana municipal.\r\n', '3.1.10.3 ', 0, 32, NULL, NULL),
(368, 'Retomar el arraigo del municipio mediante la creación de espacios públicos que brinden\r\nidentidad y mejoren la estructura urbana a través de la planeación integral del municipio.', '3.1.10.4 ', 0, 32, NULL, NULL),
(369, 'Brindar mantenimiento y reparación de mobiliario urbano.', '3.1.10.5 ', 0, 32, NULL, NULL),
(370, 'Mejorar las condiciones operativas y de recaudación de los tianguis y del comercio en vía\r\npública.\r\n', '3.1.11.1 ', 0, 33, NULL, NULL),
(371, 'Regular el comercio en vía pública.\r\n', '3.1.11.2 ', 0, 33, NULL, NULL),
(372, 'Actualizar el padróncausantespúblicos, tianguis y comercio en vía pública.\r\n', '3.1.11.3 ', 0, 33, NULL, NULL),
(373, 'Ampliar la recaudación de pago de derechos derivados de las actividades comerciales.\r\n', '3.1.11.4 ', 0, 33, NULL, NULL),
(374, 'Promover la instalación del mercado de abastos municipal.', '3.1.11.5', 0, 33, NULL, NULL),
(375, 'En forma trasversal coordinadamente con las autoridades municipales, planear, ejecutar, ampliar,\r\nmodernizar la cobertura de infraestructura, servicios y vías de movilidad en el municipio, para\r\npromover el desarrollo económico.\r\n', '3.1.12.1', 0, 34, NULL, NULL),
(376, 'Fortalecer, mejorar y ampliar la red vial, generando estructuras y alternativas que faciliten el\r\ndesplazamiento de las diferentes zonas con el resto del municipio.\r\n', '3.1.12.2 ', 0, 34, NULL, NULL),
(377, 'Modernizar las vialidades para reducir tiempos, costos y emisiones contaminantes.\r\n', '3.1.12.3 ', 0, 34, NULL, NULL),
(378, 'Gestionar la creación de circuitos zonales de transporte que interactúen con circuitos regionales\r\npara mejorar la movilidad interna.\r\n', '3.1.12.4 ', 0, 34, NULL, NULL),
(379, 'Generar la interacción de los diversos modos de transporte que nos permita crear un sistema\r\nintegral de movilidad para un municipio accesible y conectado garantizando a los usuarios un\r\nservicio ágil y de calidad.\r\n', '3.1.12.5 ', 0, 34, NULL, NULL),
(380, 'Construir corredores urbanos a través de ciclo vías y vías peatonales.', '3.1.12.6 ', 0, 34, NULL, NULL),
(381, 'Rehabilitación de los campos de fútbol y canchas de básquetbol municipal.\r\n', '4.1.1.1 ', 0, 35, NULL, NULL),
(382, 'Rehabilitación, equipamiento e impulso a la unidad deportiva municipal.\r\n', '4.1.1.2 ', 0, 35, NULL, NULL),
(383, 'Rehabilitación, mantenimiento y equipamiento de las casas de cultura y bibliotecas municipales.\r\n', '4.1.1.3 ', 0, 35, NULL, NULL),
(384, 'Construcción de las unidades de atención médica DIF.', '4.1.1.4 ', 0, 35, NULL, NULL),
(385, 'Construcción mantenimiento de la clínica dental DIF municipal.\r\n', '4.1.1.5 ', 0, 35, NULL, NULL),
(386, 'Rehabilitación, mantenimiento y equipamiento de la unidad de terapia física DIF municipal.\r\n', '4.1.1.6 ', 0, 35, NULL, NULL),
(387, 'Dignificación de las instalaciones de prestación de servicio de asistencia social municipal.\r\n', '4.1.1.7 ', 0, 35, NULL, NULL),
(388, 'Construcción y equipamiento de alberca olímpica municipal.\r\n', '4.1.1.8', 0, 35, NULL, NULL),
(389, 'Operación e instalación de nuevos centros de desarrollo comunitario municipales.\r\n', '4.1.1.9 ', 0, 35, NULL, NULL),
(390, 'Impulso al servicio médico comunitario y en tu colonia con apoyo de las universidades.\r\n', '4.1.1.10 ', 0, 35, NULL, NULL),
(391, 'Rehabilitación, equipamiento a instituciones de educación pública en el municipio en coordinación\r\ncon las autoridades estatales y federales.\r\n', '4.1.1.11 ', 0, 35, NULL, NULL),
(392, 'Rehabilitación, mantenimiento y embellecimiento de parques y jardines municipales.\r\n', '4.1.1.12 ', 0, 35, NULL, NULL),
(393, 'Rehabilitación, mantenimiento y embellecimiento de plazas y espacios públicos.\r\n', '4.1.1.13 ', 0, 35, NULL, NULL),
(394, 'Rescate de áreas verdes para potenciarlas como espacios deportivos y recreativos.\r\n', '4.1.1.14 ', 0, 35, NULL, NULL),
(395, 'Rehabilitación, mantenimiento y equipamiento de bibliotecas municipales.\r\n', '4.1.1.15 ', 0, 35, NULL, NULL),
(396, 'Limpieza y desazolve de canales.\r\n', '4.1.1.16 ', 0, 35, NULL, NULL),
(397, 'Entubamiento de canales que puedan representar foco de infección a las comunidades.\r\n', '4.1.1.17 ', 0, 35, NULL, NULL),
(398, 'Instalación de infraestructura para dotación de agua limpia en la escuela y colonia.\r\n', '4.1.1.18 ', 0, 35, NULL, NULL),
(399, 'Desarrollo de infraestructura y equipamiento urbano con visión de inclusión especialmente\r\na adultos mayores y personas con capacidades diferentes.\r\n', '4. 1.1.19 ', 0, 35, NULL, NULL),
(400, 'funcionamientos de la funeraria municipal.\r\n', '4. 1.1.9 ', 0, 35, NULL, NULL),
(401, 'Desarrollo y fomento de actividades recreativas, culturales, deportivas en espacios públicos.', '4.1.1.10. ', 0, 35, NULL, NULL),
(402, 'Semillero deportivo, conformación de escuelas deportivas.\r\n', '4.1.2.1', 0, 36, NULL, NULL),
(403, 'Actívate en tu colonia.\r\n', '4.1.2.2 ', 0, 36, NULL, NULL),
(404, 'Impulso la natación, con la operación alberca olímpica municipal.\r\n', '4.1.2.3', 0, 36, NULL, NULL),
(405, 'Promover la participación de las instituciones de educación superior del municipio en el fortalecimiento\r\nde la cultura y artes.\r\n.', '4.1.2.4 ', 0, 36, NULL, NULL),
(406, 'Desconcentrar las escuelas deportivas en las regiones del municipio.\r\n', '4.1.2.5 ', 0, 36, NULL, NULL),
(407, 'promover la realización de torneos, regatas y eventos deportivos nacionales internacionales\r\nen el municipio', '4.1.2.6 ', 0, 36, NULL, NULL),
(408, 'Brindar servicio dental a grupos marginados.\r\n', '4.1.3.1 ', 0, 39, NULL, NULL),
(409, 'Mejorar el servicio y atención de las clínicas del DIF municipales.\r\n', '4.1.3.2 ', 0, 39, NULL, NULL),
(410, 'Cubrir la demanda de medicamentos las clínicas del DIF.\r\n', '4.1.3.3 ', 0, 39, NULL, NULL),
(411, 'Unidad médica y dental móvil, para la salud en tu comunidad.\r\n', '4.1.3.4 ', 0, 39, NULL, NULL),
(412, 'Ampliar la cobertura de médicos en el municipio, con apoyo de las universidades de la región.\r\n', '4.1.3.5 ', 0, 39, NULL, NULL),
(413, 'Controlar la fauna callejera y las enfermedades transmitidas por vectores.\r\n', '4.1.3.6 ', 0, 39, NULL, NULL),
(414, 'Impulsar apoyo para que las Semanas Nacionales de Salud tengan mayor número de beneficiarios\r\nen el municipio.\r\n', '4.1.3.7 ', 0, 39, NULL, NULL),
(415, 'Promover con las autoridades estatales y federales campaña de detección del cáncer de\r\nmama y cèrvico uterino.\r\n', '4.1.3.8 ', 0, 39, NULL, NULL),
(416, 'Promover campañas de prevención de adicciones y enfermedades y promover la atención\r\noportuna\r\n', '4.1.3.9 ', 0, 39, NULL, NULL),
(417, 'Realizar campañas de prevención del suicidio.\r\n', '4.1.3.10 ', 0, 39, NULL, NULL),
(418, 'Brindar rehabilitación a personas con capacidades diferentes.\r\n', '4.1.3.11 ', 0, 39, NULL, NULL),
(419, 'Brindar atención psicológica a mujeres violentadas y grupos en situación de riesgo.\r\n', '4.1.3.12 ', 0, 39, NULL, NULL),
(420, 'Gestionar con organizaciones privadas y autoridades estatales y generales campañas de vacunación y esterilización de perros y gatos.\r\n', '4.1.3.13 ', 0, 39, NULL, NULL),
(421, 'En transversalización de acciones participar y gestionar en campañas de descacharrización y\r\nlimpieza de lotes baldíos.\r\n', '4.1.3.14 ', 0, 39, NULL, NULL),
(422, 'Tranversalización de acciones para acercar agua purificada a escuelas y comunidades.\r\n', '4.1.3.15 ', 0, 39, NULL, NULL),
(423, 'En transversalización, fomentar la práctica regular y sistemática del deporte, para disminuir\r\nla incidencia de enfermedades crónicos digestivos.\r\n', '4.1.3.16 ', 0, 39, NULL, NULL),
(424, 'Gestionar la incorporación de las familias al seguro popular.\r\n', '4.1.3.17 ', 0, 39, NULL, NULL),
(425, 'Instalar y fortalecer las acciones del Consejo Municipal de Salud.\r\n', '4.1.3.18 ', 0, 39, NULL, NULL),
(426, 'Contribuir a que Bahía de Banderas sea un Municipio cardio protegido', '4.1.3.19 ', 0, 39, NULL, NULL),
(427, 'Promover la participación de la sociedad en las actividades culturales del municipio.\r\n', '4.1.4.1 ', 0, 40, NULL, NULL),
(428, 'Institucionalizar y desconcentrar las actividades de la escuela de iniciación artística municipal.\r\n', '4.1.4.2 ', 0, 40, NULL, NULL),
(429, 'Promover la realización de eventos culturales, detalla nacional e internacional en el municipio.\r\n', '4.1.4.3 ', 0, 40, NULL, NULL),
(430, 'Integrar y promover el grupo de danza y baile folclórico municipal.\r\n', '4.1.4.4 ', 0, 40, NULL, NULL),
(431, 'Ampliar la oferta cultural para los adultos y adultos mayores en el municipio.\r\n', '4.1.4.5. ', 0, 40, NULL, NULL),
(432, 'Implementar el programa “Cultura Urbana”.\r\n', '4.1.4.6 ', 0, 40, NULL, NULL),
(433, 'Convertir a Bahía de Banderas en un municipio con oferta cultural para su población y visitantes.\r\n', '4.1.4.7 ', 0, 40, NULL, NULL),
(434, 'Difundir las acciones culturales en todos los medios de comunicación', '4.1.4.8 ', 0, 40, NULL, NULL),
(435, 'Institucionalizar el proceso de implementación de políticas públicas con perspectiva de género.\r\n', '4.1.5.1 ', 0, 41, NULL, NULL),
(436, 'Instalación y funcionamiento del Instituto Municipal de la Mujer.\r\n', '4.1.5.2 ', 0, 41, NULL, NULL),
(437, 'Promover la cultura de respeto de los derechos de las mujeres, niñas y adolescentes.\r\n', '4.1.5.3 ', 0, 41, NULL, NULL),
(438, 'Sensibilizar a servidores públicos en temas de violencia de género.\r\n', '4.1.5.4 ', 0, 41, NULL, NULL),
(439, 'Realizar talleres de prevención de la violencia en zonas vulnerables del municipio.\r\n', '4.1.5.5 ', 0, 41, NULL, NULL),
(440, 'Promover la capacitación y atención de las mujeres en situación de vulnerabilidad para su\r\nempoderamiento.\r\n', '4.1.5.6 ', 0, 41, NULL, NULL),
(441, 'Gestionar con las autoridades estatales, federales e iniciativa privada, así como la colaboración\r\ncomunitaria, para instar guarderías infantiles, que permitan el acceso a las mujeres al empleo.\r\n', '4.1.5.7 ', 0, 41, NULL, NULL),
(442, 'Atender, prevenir y contribuir a erradicar la violencia contra las Mujeres.\r\n', '4.1.5.8', 0, 41, NULL, NULL),
(443, 'Impartir cursos a mujeres de formación para el trabajo en distintas áreas productivas.\r\n', '4.1.5.9 ', 0, 41, NULL, NULL),
(444, 'Facilitar la inserción laboral de las mujeres.\r\n', '4.1.5.10 ', 0, 41, NULL, NULL),
(445, 'Crear redes productivas de mujeres en colonias con altos índices de violencia doméstica.\r\n', '4.1.5.11 ', 0, 41, NULL, NULL),
(446, 'Gestionar apoyos escolares a hijosde mujeres trabajadoras.\r\n', '4.1.5.12 ', 0, 41, NULL, NULL),
(447, 'Brindar asesoría jurídica y psicológica a mujeres vulnerables de manera gratuita.', '4.1.5.13 ', 0, 41, NULL, NULL),
(448, 'Promover oficios y prácticas educativas, y realizar actividades que permitan ampliar las ofertas\r\nla educación entre la población con capacidades diferentes, adultos mayores, grupos vulnerables\r\ny personas ubicadas en zonas marginadas o rurales para lograr su inclusión laboral y social.\r\n', '4.1.6.1 ', 0, 42, NULL, NULL),
(449, 'Facilitar la educación continua entre la población adulta, personas con capacidades diferentes\r\ny trabajadora del municipio.\r\n', '4.1.6.2 ', 0, 42, NULL, NULL),
(450, 'Implementar un programa de cursos y talleres de capacitación en artes y oficios en los Centros\r\nde Desarrollo Comunitario.\r\n', '4.1.6.3 ', 0, 42, NULL, NULL),
(451, 'Promover campañas de capacitación y atención de las mujeres en situación de vulnerabilidad\r\npara su empoderamiento.\r\n', '4.1.6.4 ', 0, 42, NULL, NULL),
(452, 'Gestionar con las instituciones de capacitación para el trabajo privadas la construcción de la\r\nescuela de costura.\r\n', '4.1.6.5 ', 0, 42, NULL, NULL),
(453, 'Fomentar la cultura de respeto de movilidad de las personas con capacidades diferentes', '4.1.6.6. ', 0, 42, NULL, NULL),
(454, 'Respeta tu lugar, Realizar convenios con empresas y comercios y campaña de concientización\r\ny sanción a quienes ocupen espacios destinados a personas con discapacidad.\r\n', '4.1.6.7.', 0, 42, NULL, NULL),
(455, 'Acreditar a personas con capacidades diferentes para hacer uso de los espacios destinados\r\npara ellos.\r\n', '4.1.6.8 ', 0, 42, NULL, NULL),
(456, 'Apoyar a la población vulnerable adulta mayor.\r\n', '4.1.6.9. ', 0, 42, NULL, NULL),
(457, 'Ofrecer asesoría legal a la población vulnerable.\r\n', '4.1.6.10 ', 0, 42, NULL, NULL),
(458, 'Bridar servicios funerarios a la población vulnerable.\r\n', '4.1.6.11 ', 0, 42, NULL, NULL),
(459, 'Reactivar la casa hogar y buscar la autosuficiencia.\r\n', '4.1.6.12 ', 0, 42, NULL, NULL),
(460, 'Fortalecer los servicios de rehabilitación e integración social de las personas con discapacidad\r\npromoviendo su desarrollo integral.\r\n', '4.1.6.13 ', 0, 42, NULL, NULL),
(461, 'Gestionar los programas sociales federales y estatales para elevar la calidad de vida de las\r\nfamilias del municipio.\r\n', '4.1.6.14 ', 0, 42, NULL, NULL),
(462, 'Involucrar a la sociedad en general en las acciones de gobierno para su bienestar a través\r\ndel padrón de jefe de manzana, colonia, sección, delegación, para gestionar los recursos y acciones\r\nnecesarias para su entono.\r\n', '4.1.6.15 ', 0, 42, NULL, NULL),
(463, 'Coadyuvar con las autoridades federales para la incorporación al Seguro popular a las personas\r\nque carezcan de servicios de salud', '4.1.6.16 ', 0, 42, NULL, NULL),
(464, 'Promover la cultura del huerto de traspatio, para abatir la pobreza alimentaria.\r\n', '4.1.7.1 ', 0, 43, NULL, NULL),
(465, 'Promover los desayunos escolares en zonas marginadas.\r\n', '4.1.7.2 ', 0, 43, NULL, NULL),
(466, 'Despensa para tu familia a grupos vulnerables.\r\n', '4.1.7.3 ', 0, 43, NULL, NULL),
(467, 'Curso sobre conservación de alimentos.\r\n', '4.1.7.4 ', 0, 43, NULL, NULL),
(468, 'Coordinar el funcionamiento de los comedores comunitarios.\r\n', '4.1.7.5 ', 0, 43, NULL, NULL),
(469, 'En coordinación con las instituciones educativas de la región, establecer el control nutricional\r\nen las zonas maginadas.\r\n', '4.1.7.6 ', 0, 43, NULL, NULL),
(470, 'En coordinación con la iniciativa privada y sociedad civil organizada, coordinar actividades.\r\n', '4.1.7.7 ', 0, 43, NULL, NULL),
(471, 'Conformar el banco de alimentos y entrega alimentaria en zonas marginadas.\r\n', '4.1.7.8 ', 0, 43, NULL, NULL),
(472, 'Gestionar con autoridades estatales, federales e iniciativa privada proyectos para entrega de\r\ntecnología ecológica en zonas rurales (estufas, colectores de agua, filtros de purificación de agua,\r\nboiler solar.\r\n', '4.1.7.9. ', 0, 43, NULL, NULL),
(473, 'Gestionar la incorporación de la población a los programas federales yestatales que atiendan\r\nla carencia por acceso a la alimentación.\r\n', '4.1.7.10. ', 0, 43, NULL, NULL),
(474, 'Gestionar la instalación de centros de distribución de leche LICONSA.\r\n', '4.1.7.11. ', 0, 43, NULL, NULL),
(475, 'Impulsar programas de combate a la desnutrición en coordinación con el Gobierno del Estado\r\nde Nayarit.\r\n', '4.1.7.12 ', 0, 43, NULL, NULL),
(476, 'Realizar pláticas y/o talleres en temas de desnutrición, sobrepeso y obesidad, a la población\r\nque lo solicite.', '4.1.7.13 ', 0, 43, NULL, NULL),
(477, 'Otorgar seguros, útiles, becas y apoyos escolares, con el fin de hacer de la educación la\r\nprincipal palanca del desarrollo social, la reducción de desigualdades y el progreso de las personas.\r\n', '4.1.8.1 ', 0, 44, NULL, NULL),
(478, 'Gestionar y destinar recursos para la ampliación, rehabilitación y mantenimiento de la infraestructura\r\nde los planteles de educación básica en el municipio.\r\n', '4. 1.8.2 ', 0, 44, NULL, NULL),
(479, 'Gestionar y destinar recursos para la regularización de los servicios básicos de planteles de educación básica (agua, luz y drenaje).', '4. 1.8.3 ', 0, 44, NULL, NULL),
(480, 'Implementar programas educativos suplementarios a través de las bibliotecas públicas y casas\r\nde la cultura, para coadyuvar a mejorar el rendimiento escolar en el municipio.\r\n', '4. 1.8.4 ', 0, 44, NULL, NULL),
(481, 'Gestionar ante las autoridades estatales y federales para que los centros educativos cuenten\r\ncon acceso a las tecnologías de la información.\r\n', '4. 1.8.5 ', 0, 44, NULL, NULL),
(482, 'Realizar la olimpiada de competencias educativas municipal.\r\n', '4. 1.8.6 ', 0, 44, NULL, NULL),
(483, 'Fomentar la lectura entre la población.\r\n', '4.1.8.7 ', 0, 44, NULL, NULL),
(484, 'Gestionar la creación de nuevos espacios educativos de educación media superior y superior,\r\npara que los jóvenes que habitan en el territorio municipal puedan cursar dicho nivel educativo.\r\n', '4.1.8.8 ', 0, 44, NULL, NULL),
(485, 'Disminuir el rezago educativo en los polígonos que comprenden las ZAP en el territorio\r\nmunicipal.\r\n', '4.1.8.9 ', 0, 44, NULL, NULL),
(486, 'Promover cursos de alfabetización en las AGEB de atención prioritaria en el territorio municipal.\r\n', '4.1.8.10 ', 0, 44, NULL, NULL),
(487, 'Promover cursos de educación secundaria a población que todavía no tenga el certificado\r\ncorrespondiente', '4.1.8.11 ', 0, 44, NULL, NULL),
(488, 'Lograr la concientización de nuestros jóvenes para prevenir, atender y\r\nErradicar las adicciones y malos hábitos, que deterioran su salud y relaciones personales.', '4.1.9.1 ', 0, 45, NULL, NULL),
(489, 'Radio joven, Crear una estación de radio por internet, operado por jóvenes.\r\n', '4.1.9.2 ', 0, 45, NULL, NULL),
(490, 'Promover cursos sobre el uso y aprovechamiento de la tecnología y blogueros, en sus actividades\r\nescolares y económicas.\r\n', '4.1.9.3 ', 0, 45, NULL, NULL),
(491, 'Realizar campañas de Prevención de embarazo adolescentes y cuidado de la salud.\r\n', '4.1.9.4 ', 0, 45, NULL, NULL),
(492, 'Identificar y rescatar a los menores en situación de calle en coordinación en coordinación\r\ncon las autoridades estatales y federales competentes.\r\n', '4.1.9.5 ', 0, 45, NULL, NULL),
(493, 'Realizar campañas tendientes aprevenir la violencia escolar.\r\n', '4.1.9.6 ', 0, 45, NULL, NULL),
(494, 'Impartir talleres educativos, de oficios y de prevención de la violencia.\r\n', '4.1.9.7 ', 0, 45, NULL, NULL),
(495, 'Brindar servicios de orientación psicológica.\r\n', '4.1.9.8 ', 0, 45, NULL, NULL),
(496, 'Gestionar apoyos y estímulos para jóvenes emprendedores.', '4.1.9.9 ', 0, 45, NULL, NULL),
(497, 'Apoyar la recuperación de espacios públicos, a través de actividades solidarias, comunitarias,\r\ndeportivas, culturales y productivas con jóvenes del municipio.\r\n', '4.1.9.10 ', 0, 45, NULL, NULL),
(498, 'Gestionar ante las autoridades competentes el incremento de opciones para que los jóvenes\r\ndel municipio puedan cursar el nivel educativo medio superior.\r\n', '4.1.9.11 ', 0, 45, NULL, NULL),
(499, 'Gestionar recursos para el otorgamiento de Becas a la juventud.\r\n', '4.1.9.12 ', 0, 45, NULL, NULL),
(500, 'Promover la salud de los jóvenes.\r\n', '4.1.9.13. ', 0, 45, NULL, NULL),
(501, 'Garantizar el respeto y ejercicio de los derechos de los niños, niñas y adolescentes del municipio\r\ny potenciar sus capacidades para mejorar su calidad de vida y la de sus familias.\r\n', '4.1.9.14 ', 0, 45, NULL, NULL),
(502, 'Realizar acciones para vincular a los jóvenes con el sector empresarial para que tengan\r\noportunidades de empleo.\r\n', '4.1.9.15 ', 0, 45, NULL, NULL),
(503, 'Contribuir al acceso y permanencia de la juventud en la educación.\r\n', '4.1.9.16 ', 0, 45, NULL, NULL),
(504, 'Colaborar para que en el municipio se imparta educación libre de violencia y con perspectiva\r\nde los derechos de las y los jóvenes.\r\n', '4.1.9.17 ', 0, 45, NULL, NULL),
(505, 'Apoyar en la transición de la vida escolar a la vida laboral de las y los jóvenes.\r\n', '4.1.9.18 ', 0, 45, NULL, NULL),
(506, 'Apoyar a los jóvenes en capacitación especializada, para elevar su capacidad de ingreso\r\n', '4.1.9.19 ', 0, 45, NULL, NULL),
(507, 'Gestionar para lograr el acceso a la vivienda propia de los jóvenes\r\n', '4.1.9.20 ', 0, 45, NULL, NULL),
(508, 'Apoyar a las madres jóvenes concluir sus estudios;\r\n', '4.1.9.21 ', 0, 45, NULL, NULL),
(509, 'Lograr la participación activa de los jóvenes en los temas sociales, comunitarios y públicos', '4.1.9.22 ', 0, 45, NULL, NULL),
(510, 'Apoyar a las comunidades indígenas que se encuentran habitando o de paso en el municipio.\r\n', '4.1.10.1. ', 0, 46, NULL, NULL),
(511, 'Promover y difundir la cultura de los pueblos Huicholes y Coras.', '4.1.10.2', 0, 46, NULL, NULL),
(512, 'Realizar acciones que promuevan la participación en actividades educativas, físicas, ocupacionales,\r\nproductivas y recreativas de los adultos mayores.\r\n', '4.1.11.1 ', 0, 47, NULL, NULL),
(513, 'Gestionar y otorgar apoyos a los adultos mayores.\r\n', '4.1.11.2 ', 0, 47, NULL, NULL),
(514, 'Gestionar un mayor acceso de los adultos mayores del municipio a los programas federales\r\ny estatales.\r\n', '4.1.11.3 ', 0, 47, NULL, NULL),
(515, 'Celebrar convenios de colaboración con empresas para generar oportunidades de desarrollo\r\ny empleo a los adultos mayores.\r\n', '4.1.11.4. ', 0, 47, NULL, NULL),
(516, 'Gestionar convenios de descuento en compras de productos básicos, farmacia y atención\r\nmédica.\r\n', '4.1.11.5 ', 0, 47, NULL, NULL),
(517, 'Gestionar recursos para Comedores comunitarios.', '4.1.11.6 ', 0, 47, NULL, NULL),
(518, 'vigilar que las personas que se integran a la gestión municipal como servidores públicos\r\ncuenten con el perfil adecuado para cada puesto y con vocación para el servicio.\r\n', '5.1.1.1 ', 0, 48, NULL, NULL),
(519, 'Consolidar a los órganos auxiliares (Consejos municipales) como organismos ciudadanizados\r\npara garantizar la participación ciudadana.\r\n', '5.1.1.2 ', 0, 48, NULL, NULL),
(520, 'Promover la mejora regulatoria, para ajustar el marco normativo realizad municipal, para generar\r\ncertidumbre, confianza a la ciudadanía y eficientar los procesos administrativos y de gestión.\r\n', '5.1.1.3 ', 0, 48, NULL, NULL),
(521, 'Elaborar los manuales de puestos, servicios y procedimientos del Municipio.\r\n', '5.1.1.4.', 0, 48, NULL, NULL),
(522, 'Impulsar un Programa de Modernización y Simplificación Administrativa en la gestión pública.', '5.1.1.5 ', 0, 48, NULL, NULL),
(523, 'Servicio público oportuno, con trato justo y equitativo.\r\n', '5.1.2.1. ', 0, 49, NULL, NULL),
(524, 'Elevar la profesionalización en el funcionamiento, organización y operación de los servidores\r\npúblicos del Municipio, a través de la inclusión permanente de sistemas de calidad.\r\n', '5.1.2.2 ', 0, 49, NULL, NULL),
(525, 'Implementar los mecanismos para asegurar la Justicia municipal.\r\n', '5.1.2.3 ', 0, 49, NULL, NULL),
(526, 'Capacitar a los servidores públicos municipales de las obligaciones legales y responsabilidades\r\na las que están sujetos en su actuación.\r\n', '5.1.2.4 ', 0, 49, NULL, NULL),
(527, 'Garantizar la libertad de expresión.', '5.1.2.5 ', 0, 49, NULL, NULL),
(528, 'Mejorar el sistema de fiscalización preventiva del ejercicio del gasto público.\r\n', '5.1.3. 1 ', 0, 50, NULL, NULL),
(529, 'Generar el proceso eficiente de compras consolidadas.\r\n', '5.1.3.2 ', 0, 50, NULL, NULL),
(530, 'Garantizar la transparencia y legalidad en los procesos de licitación.\r\n', '5.1.3.3 ', 0, 50, NULL, NULL),
(531, 'Establecer el sistema de control, seguimiento y resultados de la gestión del cabildo, mediante\r\nla publicación oportuna de acuerdos y resultados.\r\n', '5.1.3.4 ', 0, 50, NULL, NULL),
(532, 'Publicar oportunamente los acuerdos de Cabildo, Reglamentos y otras disposiciones administrativas\r\nde carácter general\r\n', '5.1.3.4 ', 0, 50, NULL, NULL),
(533, 'Garantizar el acceso a la información pública municipal.\r\n', '5.1.3.5 ', 0, 50, NULL, NULL),
(534, 'Actualizar permanentemente la información pública del portal electrónico del Ayuntamiento.\r\n', '5.1.3.6 ', 0, 50, NULL, NULL),
(535, 'Fomentar las buenas prácticas en los reportes de las áreas ejecutoras sobre el avance del\r\ngasto público.\r\n', '5.1.3.7 ', 0, 50, NULL, NULL),
(536, 'Incentivar la participación y corresponsabilidad ciudadana en las acciones de gobierno.', '5.1.3.8 ', 0, 50, NULL, NULL),
(537, 'Promover la integración y cohesión de la representación de manzana, colonia, sector, delegación.\r\n', '5.1.4.1. ', 0, 51, NULL, NULL),
(538, 'Incentivar a la sociedad civil organizada, especialistas, sector empresarial e instituciones educativas,\r\na participar en el diseño, implementación y evaluación de las políticas públicas municipales.\r\n', '5.1.4.2 ', 0, 51, NULL, NULL),
(539, 'Desconcentrar los servicios municipales, a fin de que en cada sector se cuente con una oficina\r\nde atención integral.\r\n', '5.1.4.3 ', 0, 51, NULL, NULL),
(540, 'Establecer el servicio telefónico y electrónico de reporte, denuncia y servicios a la ciudadanía.\r\n', '5.1.4.4 ', 0, 51, NULL, NULL),
(541, 'Bien y de Buenas en tu colonia.', '5.1.4.5 ', 0, 51, NULL, NULL),
(542, 'Modernizar el equipamiento tecnológico municipal.\r\n', '5.1.5.1 ', 0, 52, NULL, NULL),
(543, 'Desarrollar el sistema Informático Municipal para el mayor control del uso de los recursos y\r\nprestación de servicios.\r\n', '5.1.5.2 ', 0, 52, NULL, NULL),
(544, 'Modernizar los procesos administrativos, mediante la comunicación electrónica y digitalización,\r\npara reducción de tiempos de atención al ciudadano.\r\n', '5.1.5.3 ', 0, 52, NULL, NULL),
(545, 'Consolidar la comunicación pública y el fortalecimiento informativo de todas las acciones\r\ngubernamentales.\r\n', '5.1.5.4 ', 0, 52, NULL, NULL),
(546, 'Generar la base de información pública, sobre la gestión municipal, al acceso de la ciudadanía.\r\n', '5.1.5.5 ', 0, 52, NULL, NULL),
(547, 'Promover el uso de la TICS para la ampliación del catálogo de servicios en línea.\r\n', '5.1.5.6 ', 0, 52, NULL, NULL),
(548, 'Aprovechar el uso de las TICS para la recepción, tramite y seguimiento de denuncias ciudadanas.', '5.1.5.7 ', 0, 52, NULL, NULL),
(549, 'Gestión permanente para captar recursos del Gobierno Estatal, Federal y organismos nacionales\r\ne internacionales, público y privados.\r\n', '5.1.6.1 ', 0, 53, NULL, NULL),
(550, 'Optimización y asignación del gasto en base a resultados.\r\n', '5.1.6.2 ', 0, 53, NULL, NULL),
(551, 'Implementación del Sistema de Evaluación del Desempeño.\r\n', '5.1.6.3 ', 0, 53, NULL, NULL),
(552, 'Control del gasto.\r\n', '5.1.6.4 ', 0, 53, NULL, NULL),
(553, 'Política adecuada de pago de pasivo.\r\n', '5.1.6.5 ', 0, 53, NULL, NULL),
(554, 'Presupuesto de la gente.\r\n', '5.1.6.6 ', 0, 53, NULL, NULL),
(555, 'Establecimiento de proyectos participativos.\r\n', '5.1.6.7. ', 0, 53, NULL, NULL),
(556, 'Determinación con apoyo del IMPLAN el catálogo de proyectos estratégicos\r\n', '5.1.6.8 ', 0, 53, NULL, NULL),
(557, 'Informe trimestral de la aplicación del gasto y resultados.', '5.1.6.9 ', 0, 53, NULL, NULL),
(558, 'Mejorar el sistema financiero municipal.\r\n', '5.1.7.1 ', 0, 54, NULL, NULL),
(559, 'Fortalecer los mecanismos de captación y recaudación de ingresos.\r\n', '5.1.7.2 ', 0, 54, NULL, NULL),
(560, 'Proporcionar un mejor servicio y facilitar el pago a los contribuyentes evitando largas filas.\r\n', '5.1.7.3 ', 0, 54, NULL, NULL),
(561, 'Impulsar un programa integral de Fortalecimiento Hacendario e Institucional.\r\n', '5.1.7.4 ', 0, 54, NULL, NULL),
(562, 'Actualización del padrón de contribuyentes.\r\n', '5.1.7.5 ', 0, 54, NULL, NULL),
(563, 'Actualizar el catastro con valores unitarios reales y su base de contribuyentes.\r\n', '5.1.7.6 ', 0, 54, NULL, NULL),
(564, 'Actualizar el padrón de comercio.', '5.1.7.7 ', 0, 54, NULL, NULL),
(565, 'Atención, control, seguimiento y resultados oportunos a quejas.\r\n', '5.1.8.1 ', 0, 55, NULL, NULL),
(566, 'Atender y solventar observaciones ciudadanas propuestas.\r\n', '5.1.8.2 ', 0, 55, NULL, NULL),
(567, 'Integrar y difundir oportunamente la información contable presupuestal.\r\n', '5.1.8.3 ', 0, 55, NULL, NULL),
(568, 'Integrar de manera oportuna la Cuenta Pública.\r\n', '5.1.8.4 ', 0, 55, NULL, NULL),
(569, 'Administrar responsablemente los esquemas crediticios.\r\n', '5.1.8.5 ', 0, 55, NULL, NULL),
(570, 'Proteger el patrimonio municipal, el uso de los recursos públicos y prevenir actos de corrupción.\r\n', '5.1.8.6 ', 0, 55, NULL, NULL),
(571, 'Auditoria permanentes en áreas de alto riesgo.\r\n', '5.1.8.7 ', 0, 55, NULL, NULL),
(572, 'Conformación del programa de desarrollo institucional para dar certeza y mejorar los servicios al ciudadano.\r\n', '5.1.8.8. ', 0, 55, NULL, NULL),
(573, 'Diseñar e implementar el Sistema de Evaluación del Desempeño de las Gestión Municipal.', '5.1.8.9. ', 0, 55, NULL, NULL),
(574, 'Gestionar y dar respuesta efectiva a las solicitudes de los ciudadanos, mediante la atención,\r\ncanalización y vinculación con las dependencias y entidades paramunicipales.\r\n', '5.1.9.1 ', 0, 56, NULL, NULL),
(575, 'Consolidación del IMPLAN, como organismo ciudadanizado, de programación y orientación de\r\nla política pública municipal con visión de futuro.\r\n', '5.1.9.2 ', 0, 56, NULL, NULL),
(576, 'Planeación y toma de decisiones sobre el desarrollo urbano con la participación corresponsable\r\nde los ciudadanos y sus organizaciones.\r\n', '5.1.9.3 ', 0, 56, NULL, NULL),
(577, 'Diseñar e implementar el sistema de evaluación del desempeño, planeación, presupuestación\r\ny control de gasto.', '5.1.9.4. ', 0, 56, NULL, NULL),
(578, 'Diseño e implementación del programa de anticorrupción y ética en el servicio público.\r\n', '5.1.10.1', 0, 57, NULL, NULL),
(579, 'Denuncia ciudadana.\r\n', '5.1.10.2 ', 0, 57, NULL, NULL),
(580, 'Sistema de Evaluación del desempeño.', '5.1.10.3 ', 0, 57, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pm_lineas_accion_rechazadas`
--

CREATE TABLE `pm_lineas_accion_rechazadas` (
  `id` int(11) NOT NULL,
  `descripcion` text,
  `original_nombre` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `pm_linea_accion_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pm_linea_accion_has_accion_poa`
--

CREATE TABLE `pm_linea_accion_has_accion_poa` (
  `pm_lineas_accion_id` int(11) NOT NULL,
  `acciones_poa_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pm_objetivos`
--

CREATE TABLE `pm_objetivos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `punto` varchar(20) DEFAULT NULL,
  `descripccion` text,
  `pm_eje_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pm_objetivos`
--

INSERT INTO `pm_objetivos` (`id`, `nombre`, `punto`, `descripccion`, `pm_eje_id`, `created_at`, `updated_at`) VALUES
(1, 'Escudo Bahía', '1.1.', 'Generar un entorno de seguridad que permita el desarrollo de Bahia de Banderas protegiendo en forma oportuna y confiable a las personas, su patrimonio e integridad, mediante la modernizacion, profecionalización, humanización, evaluacion continua y coordinación de la corporación, dando prioridad a la participación ciudadana en la prevención del delito.', 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(2, 'Movilidad ágil, segura y sustentable\r\n', '1.2', 'Lograr que la movilidad de los habitantes y visitantes,\r\nasí como el transporte de personas, bienes y servicios en el municipio se realice de forma\r\nágil, segura y sustentable, con el menor costo desde unenfoque económico, social y ambiental, proporcionando\r\na la ciudadanía alternativas de desplazamiento más seguros sostenibles y eficaces,poniendo\r\na valor la defensa y respeto al medio ambiente, en equilibrio con la infraestructura vial y servicios de\r\nmovilidad que coadyuven a un mejor nivel de calidad de vida de los ciudadanos.', 1, NULL, NULL),
(3, 'Salvaguardar personas y sus bienes\r\n', '1.3', 'Salvaguardar la vida de las personas, sus bienes y su entorno,contribuyendo\r\na la reducción de los riesgos causados por fenómenos naturales, antrópicos, químicos, sanitarios,\r\nambientales y socio organizativos, y lograr disminuir la vulnerabilidad de la población ante desastres.', 1, NULL, NULL),
(4, 'Consolidar un municipio sostenible\r\n', '2.1 ', 'Consolidar a Bahía de Banderas, como un Municipio sostenible, Sustentable,\r\nresiliente, inclusivo, seguro y mejor conectado, mediante la conformación participativa de la agenda\r\nurbana Innovadora, integral con visión de futuro, que contemple estrategias para mejorar y ampliar\r\nla cobertura y calidad de los servicios públicos, la infraestructura, (vivienda, equipamiento, manejo\r\ny disposición final de residuos sólidos, agua potable y saneamiento, alumbrado, movilidad, ordenamiento\r\ndel uso de suelo e imagen urbana) como elementos propulsores del desarrollo económico,\r\nsocial y urbano, que inciden además en el mejoramiento de la calidad de vida de los habitantes.', 2, NULL, NULL),
(5, 'Consolidar un territorio competitivo e innovador', '3.1 ', 'Consolidar al Municipio de Bahía de Banderas, como un territorio competitivo e\r\ninnovador, en el que la infraestructura, movilidad multimodal, gobernanza, mejora regulatoria, sustentabilidad,\r\nseguridad, participación social, diversificación productiva, son entendidos y atendidos\r\ncomo factores fundamentales para para que el territorio por sí mismo se afiance como territorio\r\nrentable para la inversión y con diversidad de productos y servicios posicionados en el mercado\r\nnacional e internacional, generando así empleos mejor pagados y como consecuencia elevar la calidad\r\nde vida de toda la población', 3, NULL, NULL),
(6, 'Municipio próspero, humanista y solidario', '4.1 ', 'Consolidar un municipio próspero con sentido humanista y solidario, en el que prevalezca la inclusión y equidad como política trasversal y el fortalecimiento de los grupos vulnerables a través de la salud, educación, la cultura, deporte, valores como elementos de desarrollo y bienestar social.', 4, NULL, NULL),
(7, 'Consolidar una gestión pública municipal ordenada', '5.1 ', 'Consolidar una gestión pública municipal ordenada', 5, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `prioridad`
--

CREATE TABLE `prioridad` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programas`
--

CREATE TABLE `programas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `tema` varchar(45) DEFAULT NULL,
  `descripcion` text,
  `direccion_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proyectos`
--

CREATE TABLE `proyectos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `costo` int(11) DEFAULT NULL,
  `direccion_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `puestos`
--

CREATE TABLE `puestos` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `descripcion` text,
  `direccion_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `puestos`
--

INSERT INTO `puestos` (`id`, `nombre`, `descripcion`, `direccion_id`, `created_at`, `updated_at`) VALUES
(1, 'Subirector', 'Director del IMPLAN', 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(2, 'Informatica', 'Encargado de Sistemas del IMPLAN', 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00'),
(3, 'Hidrológico', 'Hidrológico del IMPLAN', 1, '2018-09-19 05:00:00', '2018-09-19 05:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Administrador', 'web', '2018-09-04 17:52:53', '2018-09-04 17:52:53'),
(2, 'Cliente', 'web', '2018-09-04 17:52:53', '2018-09-04 17:52:53'),
(4, 'Director', 'web', '2018-09-18 20:17:38', '2018-09-19 14:33:16'),
(6, 'Capturista', 'web', '2018-09-18 22:11:48', '2018-09-18 22:11:48');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles_has_menus`
--

CREATE TABLE `roles_has_menus` (
  `rol_id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles_has_menus`
--

INSERT INTO `roles_has_menus` (`rol_id`, `menu_id`) VALUES
(1, 2),
(1, 4),
(1, 5),
(1, 7),
(1, 8),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 16),
(1, 18),
(1, 19),
(4, 5),
(4, 11),
(4, 12),
(4, 16),
(4, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(2, 1),
(6, 1),
(8, 1),
(8, 4),
(10, 1),
(12, 1),
(14, 1),
(16, 1),
(16, 4),
(18, 1),
(18, 4),
(20, 1),
(22, 1),
(24, 1),
(24, 4),
(26, 1),
(26, 4),
(28, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `subacciones_poa`
--

CREATE TABLE `subacciones_poa` (
  `id` int(11) NOT NULL,
  `actividad` varchar(255) DEFAULT NULL,
  `meta_anual` int(11) DEFAULT NULL,
  `trimestre_1` int(11) DEFAULT NULL,
  `trimestre_2` int(11) DEFAULT NULL,
  `trimestre_3` int(11) DEFAULT NULL,
  `trimestre_4` int(11) DEFAULT NULL,
  `accion_poa_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `perfil_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `perfil_id`, `created_at`, `updated_at`) VALUES
(1, 'Edson Garcia', 'egarcia@implan.com', '$2y$10$TntpXwX365TrEJbJl8W2/u6EGlvg6AVyKcvTRWPMdy/zvZQrcwoGu', 't6GUFckHt7X4lX88ifWgLjp5rIiNNSaDgco66BS8EmOcQP61ZUw1VJBF4lwU', 1, '2018-09-04 17:50:57', '2018-09-04 17:50:57'),
(2, 'Juan Carlos Carrillo', 'jcarrillo@implan.com', '$2y$10$nEJGmeTy1x05K2IQa0ETwOdSUQvl.P44kQ1FWUy2CCjz0yaNe47km', 'aMSUp0mIHZYSNKBYY4xTd7nLN8ppRVxBwTdCPR468RXAAJAzknyYXYVAcfYY', 2, '2018-09-17 19:30:17', '2018-11-15 21:10:08'),
(3, 'Luis Hernandez', 'lhernandez@implan.com', '$2y$10$kUtdAv8MrHnVSYiyQkAxuOfy5kR8eFafem9E3oUyL.2VwnMJxBNYi', NULL, 4, '2018-09-19 20:42:02', '2018-09-19 21:15:18');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acciones_poa`
--
ALTER TABLE `acciones_poa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_acciones_poa_programas1_idx` (`programas_id`);

--
-- Indices de la tabla `age_metas`
--
ALTER TABLE `age_metas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_age_metas_age_objetivos1_idx` (`age_objetivo_id`);

--
-- Indices de la tabla `age_metas_rechazadas`
--
ALTER TABLE `age_metas_rechazadas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_pm_lineas_accion_rechazadas_users1_idx` (`user_id`),
  ADD KEY `fk_age_metas_rechazadas_age_metas1_idx` (`age_meta_id`);

--
-- Indices de la tabla `age_meta_has_acciones_poa`
--
ALTER TABLE `age_meta_has_acciones_poa`
  ADD PRIMARY KEY (`age_metas_id`,`acciones_poa_id`),
  ADD KEY `fk_age_metas_has_acciones_poa_acciones_poa1_idx` (`acciones_poa_id`),
  ADD KEY `fk_age_metas_has_acciones_poa_age_metas1_idx` (`age_metas_id`);

--
-- Indices de la tabla `age_meta_has_direccion`
--
ALTER TABLE `age_meta_has_direccion`
  ADD PRIMARY KEY (`age_metas_id`,`direcciones_id`),
  ADD KEY `fk_age_metas_has_direcciones_direcciones1_idx` (`direcciones_id`),
  ADD KEY `fk_age_metas_has_direcciones_age_metas1_idx` (`age_metas_id`),
  ADD KEY `fk_age_meta_has_direccion_puestos1_idx` (`puestos_id`);

--
-- Indices de la tabla `age_objetivos`
--
ALTER TABLE `age_objetivos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_age_objetivos_normativas1_idx` (`normativa_id`);

--
-- Indices de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bitacora_users1_idx` (`users_id`);

--
-- Indices de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `direcciones_has_mir_indicadores`
--
ALTER TABLE `direcciones_has_mir_indicadores`
  ADD PRIMARY KEY (`direcciones_id`,`mir_indicadores_id`),
  ADD KEY `fk_direcciones_has_mir_indicadores_mir_indicadores1_idx` (`mir_indicadores_id`),
  ADD KEY `fk_direcciones_has_mir_indicadores_direcciones1_idx` (`direcciones_id`),
  ADD KEY `fk_direcciones_has_mir_indicadores_puestos1_idx` (`puestos_id`);

--
-- Indices de la tabla `direcciones_has_pm_linea_accion`
--
ALTER TABLE `direcciones_has_pm_linea_accion`
  ADD PRIMARY KEY (`direccion_id`,`pm_linea_accion_id`),
  ADD UNIQUE KEY `pm_linea_accion_id` (`pm_linea_accion_id`),
  ADD KEY `fk_direcciones_has_pm_linea_accion_pm_linea_accion1_idx` (`pm_linea_accion_id`),
  ADD KEY `fk_direcciones_has_pm_linea_accion_direcciones1_idx` (`direccion_id`),
  ADD KEY `fk_direcciones_has_pm_linea_accion_puestos1_idx` (`puesto_id`);

--
-- Indices de la tabla `estatus`
--
ALTER TABLE `estatus`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `folios`
--
ALTER TABLE `folios`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_folios_users1_idx` (`users_id`),
  ADD KEY `fk_folios_prioridad1_idx` (`prioridad_id`),
  ADD KEY `fk_folios_estatus1_idx` (`estatus_id`);

--
-- Indices de la tabla `inf_agendas`
--
ALTER TABLE `inf_agendas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_inf_agendas_normativas1_idx` (`normativa_id`);

--
-- Indices de la tabla `inf_ejes`
--
ALTER TABLE `inf_ejes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_inf_ejes_inf_agendas1_idx` (`inf_agenda_id`);

--
-- Indices de la tabla `inf_indicadores`
--
ALTER TABLE `inf_indicadores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_inf_indicador_inf_tema1_idx` (`inf_tema_id`);

--
-- Indices de la tabla `inf_indicadores_rechazados`
--
ALTER TABLE `inf_indicadores_rechazados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_pm_lineas_accion_rechazadas_users1_idx` (`user_id`),
  ADD KEY `fk_inf_indicadores_rechazados_inf_indicadores1_idx` (`inf_indicador_id`);

--
-- Indices de la tabla `inf_indicador_has_accion_poa`
--
ALTER TABLE `inf_indicador_has_accion_poa`
  ADD PRIMARY KEY (`inf_indicadores_id`,`acciones_poa_id`),
  ADD KEY `fk_inf_indicadores_has_acciones_poa_acciones_poa1_idx` (`acciones_poa_id`),
  ADD KEY `fk_inf_indicadores_has_acciones_poa_inf_indicadores1_idx` (`inf_indicadores_id`);

--
-- Indices de la tabla `inf_indicador_has_direcciones`
--
ALTER TABLE `inf_indicador_has_direcciones`
  ADD PRIMARY KEY (`inf_indicador_id`,`direccion_id`),
  ADD KEY `fk_inf_indicador_has_direcciones_direcciones1_idx` (`direccion_id`),
  ADD KEY `fk_inf_indicador_has_direcciones_inf_indicador1_idx` (`inf_indicador_id`),
  ADD KEY `fk_inf_indicador_has_direcciones_puestos1_idx` (`puesto_id`);

--
-- Indices de la tabla `inf_temas`
--
ALTER TABLE `inf_temas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_inf_tema_inf_ejes1_idx` (`inf_eje_id`);

--
-- Indices de la tabla `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `metadata_direction`
--
ALTER TABLE `metadata_direction`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_metadata_direction_direcciones1_idx` (`direccion_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `mir_idicadores_rechazados`
--
ALTER TABLE `mir_idicadores_rechazados`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_pm_lineas_accion_rechazadas_users1_idx` (`user_id`),
  ADD KEY `fk_age_metas_rechazadas_copy1_mir_indicadores1_idx` (`mir_indicador_id`);

--
-- Indices de la tabla `mir_indicadores`
--
ALTER TABLE `mir_indicadores`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_mir_indicadores_mir_tipos_indicador1_idx` (`mir_tipo_indicador_id`),
  ADD KEY `fk_mir_indicadores_mir_indicador_niveles1_idx` (`mir_indicador_nivel_id`),
  ADD KEY `fk_mir_indicadores_mir_programas1_idx` (`mir_programa_id`);

--
-- Indices de la tabla `mir_indicador_has_acciones_poa`
--
ALTER TABLE `mir_indicador_has_acciones_poa`
  ADD PRIMARY KEY (`mir_indicadores_id`,`acciones_poa_id`),
  ADD KEY `fk_mir_indicadores_has_acciones_poa_acciones_poa1_idx` (`acciones_poa_id`),
  ADD KEY `fk_mir_indicadores_has_acciones_poa_mir_indicadores1_idx` (`mir_indicadores_id`);

--
-- Indices de la tabla `mir_indicador_niveles`
--
ALTER TABLE `mir_indicador_niveles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `mir_programas`
--
ALTER TABLE `mir_programas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_programa_presupuestario_normativas1_idx` (`normativa_id`);

--
-- Indices de la tabla `mir_tipos_indicador`
--
ALTER TABLE `mir_tipos_indicador`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `normativas`
--
ALTER TABLE `normativas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indices de la tabla `notificaciones`
--
ALTER TABLE `notificaciones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_notificaciones_users1_idx` (`user_id`);

--
-- Indices de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_perfil_puestos1_idx` (`puesto_id`);

--
-- Indices de la tabla `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pm_ejes`
--
ALTER TABLE `pm_ejes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_pm_ejes_normativas1_idx` (`normativa_id`);

--
-- Indices de la tabla `pm_estrategias`
--
ALTER TABLE `pm_estrategias`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_pm_estrategias_pm_objetivos1_idx` (`pm_objetivo_id`);

--
-- Indices de la tabla `pm_lineas_accion`
--
ALTER TABLE `pm_lineas_accion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_pm_linea_accion_pm_estrategias1_idx` (`pm_estrategia_id`);

--
-- Indices de la tabla `pm_lineas_accion_rechazadas`
--
ALTER TABLE `pm_lineas_accion_rechazadas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_pm_lineas_accion_rechazadas_users1_idx` (`user_id`),
  ADD KEY `fk_pm_lineas_accion_rechazadas_pm_lineas_accion1_idx` (`pm_linea_accion_id`);

--
-- Indices de la tabla `pm_linea_accion_has_accion_poa`
--
ALTER TABLE `pm_linea_accion_has_accion_poa`
  ADD PRIMARY KEY (`pm_lineas_accion_id`,`acciones_poa_id`),
  ADD KEY `fk_pm_lineas_accion_has_acciones_poa_acciones_poa1_idx` (`acciones_poa_id`),
  ADD KEY `fk_pm_lineas_accion_has_acciones_poa_pm_lineas_accion1_idx` (`pm_lineas_accion_id`);

--
-- Indices de la tabla `pm_objetivos`
--
ALTER TABLE `pm_objetivos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_pm_objetivos_pm_ejes1_idx` (`pm_eje_id`);

--
-- Indices de la tabla `prioridad`
--
ALTER TABLE `prioridad`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `programas`
--
ALTER TABLE `programas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_programas_direcciones1_idx` (`direccion_id`);

--
-- Indices de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_proyectos_direcciones1_idx` (`direccion_id`);

--
-- Indices de la tabla `puestos`
--
ALTER TABLE `puestos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_puestos_direcciones1_idx` (`direccion_id`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `roles_has_menus`
--
ALTER TABLE `roles_has_menus`
  ADD PRIMARY KEY (`rol_id`,`menu_id`),
  ADD KEY `fk_roles_has_menus_menus1_idx` (`menu_id`),
  ADD KEY `fk_roles_has_menus_roles1_idx` (`rol_id`);

--
-- Indices de la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indices de la tabla `subacciones_poa`
--
ALTER TABLE `subacciones_poa`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `fk_subacciones_poa_acciones_poa1_idx` (`accion_poa_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_users_perfil1_idx` (`perfil_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acciones_poa`
--
ALTER TABLE `acciones_poa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `age_metas`
--
ALTER TABLE `age_metas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;

--
-- AUTO_INCREMENT de la tabla `age_metas_rechazadas`
--
ALTER TABLE `age_metas_rechazadas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `age_objetivos`
--
ALTER TABLE `age_objetivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `bitacora`
--
ALTER TABLE `bitacora`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `direcciones`
--
ALTER TABLE `direcciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `estatus`
--
ALTER TABLE `estatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `folios`
--
ALTER TABLE `folios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inf_agendas`
--
ALTER TABLE `inf_agendas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `inf_ejes`
--
ALTER TABLE `inf_ejes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `inf_indicadores`
--
ALTER TABLE `inf_indicadores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=274;

--
-- AUTO_INCREMENT de la tabla `inf_indicadores_rechazados`
--
ALTER TABLE `inf_indicadores_rechazados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `inf_temas`
--
ALTER TABLE `inf_temas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT de la tabla `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT de la tabla `metadata_direction`
--
ALTER TABLE `metadata_direction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mir_idicadores_rechazados`
--
ALTER TABLE `mir_idicadores_rechazados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `mir_indicadores`
--
ALTER TABLE `mir_indicadores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT de la tabla `mir_indicador_niveles`
--
ALTER TABLE `mir_indicador_niveles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `mir_programas`
--
ALTER TABLE `mir_programas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `mir_tipos_indicador`
--
ALTER TABLE `mir_tipos_indicador`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `normativas`
--
ALTER TABLE `normativas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `notificaciones`
--
ALTER TABLE `notificaciones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `perfiles`
--
ALTER TABLE `perfiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `pm_ejes`
--
ALTER TABLE `pm_ejes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pm_estrategias`
--
ALTER TABLE `pm_estrategias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT de la tabla `pm_lineas_accion`
--
ALTER TABLE `pm_lineas_accion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=581;

--
-- AUTO_INCREMENT de la tabla `pm_lineas_accion_rechazadas`
--
ALTER TABLE `pm_lineas_accion_rechazadas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pm_objetivos`
--
ALTER TABLE `pm_objetivos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `prioridad`
--
ALTER TABLE `prioridad`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `programas`
--
ALTER TABLE `programas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `proyectos`
--
ALTER TABLE `proyectos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `puestos`
--
ALTER TABLE `puestos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `subacciones_poa`
--
ALTER TABLE `subacciones_poa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `acciones_poa`
--
ALTER TABLE `acciones_poa`
  ADD CONSTRAINT `fk_acciones_poa_programas1` FOREIGN KEY (`programas_id`) REFERENCES `programas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `age_metas`
--
ALTER TABLE `age_metas`
  ADD CONSTRAINT `fk_age_metas_age_objetivos1` FOREIGN KEY (`age_objetivo_id`) REFERENCES `age_objetivos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `age_metas_rechazadas`
--
ALTER TABLE `age_metas_rechazadas`
  ADD CONSTRAINT `fk_age_metas_rechazadas_age_metas1` FOREIGN KEY (`age_meta_id`) REFERENCES `age_metas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pm_lineas_accion_rechazadas_users100` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `age_meta_has_acciones_poa`
--
ALTER TABLE `age_meta_has_acciones_poa`
  ADD CONSTRAINT `fk_age_metas_has_acciones_poa_acciones_poa1` FOREIGN KEY (`acciones_poa_id`) REFERENCES `acciones_poa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_age_metas_has_acciones_poa_age_metas1` FOREIGN KEY (`age_metas_id`) REFERENCES `age_metas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `age_meta_has_direccion`
--
ALTER TABLE `age_meta_has_direccion`
  ADD CONSTRAINT `fk_age_meta_has_direccion_puestos1` FOREIGN KEY (`puestos_id`) REFERENCES `puestos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_age_metas_has_direcciones_age_metas1` FOREIGN KEY (`age_metas_id`) REFERENCES `age_metas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_age_metas_has_direcciones_direcciones1` FOREIGN KEY (`direcciones_id`) REFERENCES `direcciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `age_objetivos`
--
ALTER TABLE `age_objetivos`
  ADD CONSTRAINT `fk_age_objetivos_normativas1` FOREIGN KEY (`normativa_id`) REFERENCES `normativas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `bitacora`
--
ALTER TABLE `bitacora`
  ADD CONSTRAINT `fk_bitacora_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `direcciones_has_mir_indicadores`
--
ALTER TABLE `direcciones_has_mir_indicadores`
  ADD CONSTRAINT `fk_direcciones_has_mir_indicadores_direcciones1` FOREIGN KEY (`direcciones_id`) REFERENCES `direcciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_direcciones_has_mir_indicadores_mir_indicadores1` FOREIGN KEY (`mir_indicadores_id`) REFERENCES `mir_indicadores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_direcciones_has_mir_indicadores_puestos1` FOREIGN KEY (`puestos_id`) REFERENCES `puestos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `direcciones_has_pm_linea_accion`
--
ALTER TABLE `direcciones_has_pm_linea_accion`
  ADD CONSTRAINT `fk_direcciones_has_pm_linea_accion_direcciones1` FOREIGN KEY (`direccion_id`) REFERENCES `direcciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_direcciones_has_pm_linea_accion_pm_linea_accion1` FOREIGN KEY (`pm_linea_accion_id`) REFERENCES `pm_lineas_accion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_direcciones_has_pm_linea_accion_puestos1` FOREIGN KEY (`puesto_id`) REFERENCES `puestos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `folios`
--
ALTER TABLE `folios`
  ADD CONSTRAINT `fk_folios_estatus1` FOREIGN KEY (`estatus_id`) REFERENCES `estatus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_folios_prioridad1` FOREIGN KEY (`prioridad_id`) REFERENCES `prioridad` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_folios_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inf_agendas`
--
ALTER TABLE `inf_agendas`
  ADD CONSTRAINT `fk_inf_agendas_normativas1` FOREIGN KEY (`normativa_id`) REFERENCES `normativas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inf_ejes`
--
ALTER TABLE `inf_ejes`
  ADD CONSTRAINT `fk_inf_ejes_inf_agendas1` FOREIGN KEY (`inf_agenda_id`) REFERENCES `inf_agendas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inf_indicadores`
--
ALTER TABLE `inf_indicadores`
  ADD CONSTRAINT `fk_inf_indicador_inf_tema1` FOREIGN KEY (`inf_tema_id`) REFERENCES `inf_temas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inf_indicadores_rechazados`
--
ALTER TABLE `inf_indicadores_rechazados`
  ADD CONSTRAINT `fk_inf_indicadores_rechazados_inf_indicadores1` FOREIGN KEY (`inf_indicador_id`) REFERENCES `inf_indicadores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pm_lineas_accion_rechazadas_users10` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inf_indicador_has_accion_poa`
--
ALTER TABLE `inf_indicador_has_accion_poa`
  ADD CONSTRAINT `fk_inf_indicadores_has_acciones_poa_acciones_poa1` FOREIGN KEY (`acciones_poa_id`) REFERENCES `acciones_poa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inf_indicadores_has_acciones_poa_inf_indicadores1` FOREIGN KEY (`inf_indicadores_id`) REFERENCES `inf_indicadores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inf_indicador_has_direcciones`
--
ALTER TABLE `inf_indicador_has_direcciones`
  ADD CONSTRAINT `fk_inf_indicador_has_direcciones_direcciones1` FOREIGN KEY (`direccion_id`) REFERENCES `direcciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inf_indicador_has_direcciones_inf_indicador1` FOREIGN KEY (`inf_indicador_id`) REFERENCES `inf_indicadores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_inf_indicador_has_direcciones_puestos1` FOREIGN KEY (`puesto_id`) REFERENCES `puestos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `inf_temas`
--
ALTER TABLE `inf_temas`
  ADD CONSTRAINT `fk_inf_tema_inf_ejes1` FOREIGN KEY (`inf_eje_id`) REFERENCES `inf_ejes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `metadata_direction`
--
ALTER TABLE `metadata_direction`
  ADD CONSTRAINT `fk_metadata_direction_direcciones1` FOREIGN KEY (`direccion_id`) REFERENCES `direcciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `mir_idicadores_rechazados`
--
ALTER TABLE `mir_idicadores_rechazados`
  ADD CONSTRAINT `fk_age_metas_rechazadas_copy1_mir_indicadores1` FOREIGN KEY (`mir_indicador_id`) REFERENCES `mir_indicadores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pm_lineas_accion_rechazadas_users1000` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `mir_indicadores`
--
ALTER TABLE `mir_indicadores`
  ADD CONSTRAINT `fk_mir_indicadores_mir_indicador_niveles1` FOREIGN KEY (`mir_indicador_nivel_id`) REFERENCES `mir_indicador_niveles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mir_indicadores_mir_programas1` FOREIGN KEY (`mir_programa_id`) REFERENCES `mir_programas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mir_indicadores_mir_tipos_indicador1` FOREIGN KEY (`mir_tipo_indicador_id`) REFERENCES `mir_tipos_indicador` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `mir_indicador_has_acciones_poa`
--
ALTER TABLE `mir_indicador_has_acciones_poa`
  ADD CONSTRAINT `fk_mir_indicadores_has_acciones_poa_acciones_poa1` FOREIGN KEY (`acciones_poa_id`) REFERENCES `acciones_poa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_mir_indicadores_has_acciones_poa_mir_indicadores1` FOREIGN KEY (`mir_indicadores_id`) REFERENCES `mir_indicadores` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `mir_programas`
--
ALTER TABLE `mir_programas`
  ADD CONSTRAINT `fk_programa_presupuestario_normativas1` FOREIGN KEY (`normativa_id`) REFERENCES `normativas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `notificaciones`
--
ALTER TABLE `notificaciones`
  ADD CONSTRAINT `fk_notificaciones_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `perfiles`
--
ALTER TABLE `perfiles`
  ADD CONSTRAINT `fk_perfil_puestos1` FOREIGN KEY (`puesto_id`) REFERENCES `puestos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pm_ejes`
--
ALTER TABLE `pm_ejes`
  ADD CONSTRAINT `fk_pm_ejes_normativas1` FOREIGN KEY (`normativa_id`) REFERENCES `normativas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pm_estrategias`
--
ALTER TABLE `pm_estrategias`
  ADD CONSTRAINT `fk_pm_estrategias_pm_objetivos1` FOREIGN KEY (`pm_objetivo_id`) REFERENCES `pm_objetivos` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pm_lineas_accion`
--
ALTER TABLE `pm_lineas_accion`
  ADD CONSTRAINT `fk_pm_linea_accion_pm_estrategias1` FOREIGN KEY (`pm_estrategia_id`) REFERENCES `pm_estrategias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pm_lineas_accion_rechazadas`
--
ALTER TABLE `pm_lineas_accion_rechazadas`
  ADD CONSTRAINT `fk_pm_lineas_accion_rechazadas_pm_lineas_accion1` FOREIGN KEY (`pm_linea_accion_id`) REFERENCES `pm_lineas_accion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pm_lineas_accion_rechazadas_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pm_linea_accion_has_accion_poa`
--
ALTER TABLE `pm_linea_accion_has_accion_poa`
  ADD CONSTRAINT `fk_pm_lineas_accion_has_acciones_poa_acciones_poa1` FOREIGN KEY (`acciones_poa_id`) REFERENCES `acciones_poa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pm_lineas_accion_has_acciones_poa_pm_lineas_accion1` FOREIGN KEY (`pm_lineas_accion_id`) REFERENCES `pm_lineas_accion` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `pm_objetivos`
--
ALTER TABLE `pm_objetivos`
  ADD CONSTRAINT `fk_pm_objetivos_pm_ejes1` FOREIGN KEY (`pm_eje_id`) REFERENCES `pm_ejes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `programas`
--
ALTER TABLE `programas`
  ADD CONSTRAINT `fk_programas_direcciones1` FOREIGN KEY (`direccion_id`) REFERENCES `direcciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `proyectos`
--
ALTER TABLE `proyectos`
  ADD CONSTRAINT `fk_proyectos_direcciones1` FOREIGN KEY (`direccion_id`) REFERENCES `direcciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `puestos`
--
ALTER TABLE `puestos`
  ADD CONSTRAINT `fk_puestos_direcciones1` FOREIGN KEY (`direccion_id`) REFERENCES `direcciones` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `roles_has_menus`
--
ALTER TABLE `roles_has_menus`
  ADD CONSTRAINT `fk_roles_has_menus_menus1` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_roles_has_menus_roles1` FOREIGN KEY (`rol_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `subacciones_poa`
--
ALTER TABLE `subacciones_poa`
  ADD CONSTRAINT `fk_subacciones_poa_acciones_poa1` FOREIGN KEY (`accion_poa_id`) REFERENCES `acciones_poa` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `fk_users_perfil1` FOREIGN KEY (`perfil_id`) REFERENCES `perfiles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
