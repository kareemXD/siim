<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*********************************** Auth and Others Routes ***********************************/
Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@redirect')->middleware('checkstatus')->name('redirect');
Route::get('/redirect', 'HomeController@redirect')->middleware('checkstatus')->name('redirect');

Route::get('/editar/perfil', 'HomeController@editProfile')->middleware('auth')->name("edit.profile");

Route::get('/notificaciones', 'HomeController@notifications')->middleware('auth')->name("notifications");
Route::post('/actualizar/notificacion', 'HomeController@updateNotification')->middleware('auth')->name("update.notification");
/*********************************** End Auth and Others Routes ******************************/

/*********************************** All Routes ***********************************/
Route::middleware('auth')->group(function () {

    Route::namespace('Administration')->group(function () {
        //Dashboard
        Route::get('/panel-control', 'AdministrationController@dashboard')->middleware('checkProfile')->name('dashboard');
    });

    //Support
    Route::namespace('Support')->prefix('soporte')->group(function () {
        Route::get('/', 'SupportController@support')->middleware('checkProfile')->name('support');
        Route::post('/obtener/ticket/datos', 'SupportController@getTicketData')->name('get.ticket.data');
        Route::post('/cambiar/ticket/estatus', 'SupportController@changeTicketStatus')->name('change.ticket.status');
        Route::post('/terminar/ticket', 'SupportController@finishTicket')->name('finish.ticket');
        Route::post('/crear/ticket', 'SupportController@storeTicket')->name('store.ticket');

        //Offices Routes (yes.. inside of support)
        Route::get('/offices', 'OfficeController@index')->middleware('checkProfile')->name('offices');
        Route::post('/nuevo/oficio', 'OfficeController@store')->name('store.office.data');
        Route::post('/obtener/oficio', 'OfficeController@getOffice')->name('get.office.by.id');
        Route::get('/descargar/oficio/{id}', 'OfficeController@downloadOffice')->name('download.office.by.id');
        Route::get('/revisar/oficio/{id?}', 'OfficeController@checkOffice')->name('check.office.by.id');
        Route::post('/respuesta/oficio', 'OfficeController@storeAnswer')->name('store.answer.data');
        Route::post('/estatus/oficio', 'OfficeController@changeStatus')->name('change.status.by.id');
        Route::post('/alerta/oficio', 'OfficeController@alertNotification')->name('alert.notification');
        Route::post('/delete/oficio', 'OfficeController@deleteOffice')->name('delete.office.by.id');
        Route::post('/restore/oficio', 'OfficeController@restoreOffice')->name('restore.office.by.id');

    });


    // Authentication
    Route::namespace('Auth')->prefix('autenticacion')->group(function () {
        // Authentication -> Users (User Administration)
        Route::get('/usuarios', 'AuthController@users')->middleware('checkProfile')->name('users');
        Route::post('/obtener/datos/usuario', 'AuthController@getUserById')->name('get.user.data');
        Route::post('/nuevo/usuario', 'AuthController@newUser')->name('new.register');
        Route::post('/actualizar/perfil', 'AuthController@updateProfile')->middleware('auth')->name("update.profile");
        Route::post('/actualizar/perfil/usuario', 'AuthController@updateProfileClient')->middleware('auth')->name("update.profile.client");
        Route::post('/desactivar/usuario', 'AuthController@deactivateUser')->middleware('isAdmin')->name("deactivate.user");

        // Authentication -> Roles (Roles Administration)
        Route::get('/roles', 'AuthController@roles')->middleware('checkProfile')->name('roles');
        Route::get('/nuevo/rol', 'AuthController@newRole')->name('new.role');
        Route::post('/guardar/rol', 'AuthController@storeRole')->name('store.role');
        Route::get('/editar/rol/{id}', 'AuthController@editRole')->name('edit.role');
        Route::post('/actualizar/rol', 'AuthController@updateRole')->name('update.role');
    });

    //Navigation
    Route::namespace('Navigation')->prefix('navegacion')->group(function () {
        //Navigation -> Menus (Administration of Menus and Submenus)
        Route::get('/menus', 'NavigationController@menus')->middleware('checkProfile')->name('menus');
        Route::get('/nuevo/menu', 'NavigationController@newMenu')->name('new.menu');
        Route::post('/guardar/menu', 'NavigationController@storeMenu')->name('store.menu');
        Route::get('/editar/menu/{id}', 'NavigationController@editMenu')->name('edit.menu');
        Route::post('/actualizar/menu', 'NavigationController@updateMenu')->name('update.menu');
    });

    //Config
    Route::namespace('Configuration')->prefix('configuracion')->group(function () {
        //Config Direction Data
        Route::get('/datos/direccion', 'ConfigurationController@metadataDirection')->name('metadata.direction');
        Route::post('/actualizar/datos/direccion', 'ConfigurationController@updateMetadataDirection')->name('update.metadata.direction');

        //Asignar Apartados del Poa a Puestos
        Route::get('/poa', 'ConfigurationController@cofigPOA')->name('config.poa');

    });

    //Reports
    Route::namespace('Reports')->prefix('nuevos-reportes')->group(function () {
        //Config Direction Data
        Route::get('/generar/mir', 'NewReportController@generateMir')->name('generate.mir');
        Route::get('/generar/mir/direccion/{id}', 'NewReportController@generateMirDirection')->name('generate.mir.direction');
        Route::get('/generar/avance-mir/direccion/{id}', 'NewReportController@generateAdvanceMirDirection')->name('generate.advance.mir.direction');

    });

    //Reports
    Route::namespace('Reports')->prefix('resportes')->group(function () {
        //Config Direction Data
        Route::get('/generar/poa', 'ReportController@generatePOA')->name('generate.poa');
        Route::get('/generar/advance/poa', 'ReportController@generateAdvancePOA')->name('generate.advance.poa');
        Route::get('/generar/poa/direccion/{id}', 'ReportController@generatePOADirection')->name('print.poa.direction');
        Route::get('/generar/reporte/lineas-accion', 'ReportController@statusActionLines')->name('print.status.action');
        Route::get('/generar/reporte/avance/lineas-accion', 'ReportController@statusAdvanceActionLines')->name('print.status.advance.action');


        //Seccion de Segimiento del POA
        Route::get('/generar/avance/poa/direccion/{id}', 'ReportController@generatePoaAdvanceDirection')->name('print.poa.advance');

        //Seccion de Segimiento de INAFED y AGENDA 2030
        Route::get('/generar/avance/inafed/direccion/{id}', 'ReportController@generateInafedAdvanceDirection')->name('print.inafed.advance');
        Route::get('/generar/avance/agenda2030/direccion/{id}', 'ReportController@generateAgenda2030AdvanceDirection')->name('print.agenda2030.advance');
        Route::get('/generar/avance/mir/direccion/{id}', 'ReportController@generateMirAdvanceDirection')->name('print.mir.advance');

        //Reporte de Avance del Iforme Anual
        Route::get('/generar/informe-anual', 'ReportController@annualReport')->name('print.annual.report');
        Route::get('/generar/avance/informe/anual', 'ReportController@generateAnnualReport')->name('print.annual.advance');
    });

    //Stadistics
    Route::namespace('Reports')->prefix('estadisticas')->group(function () {
        //Config Direction Data
        //Reporte de Actividades del Plan Municipal
        Route::get('/actividades/pmd', 'StadisticsController@actionsPMD')->name('stadistics.actions.pmd');
        Route::post('/actividades/pmd/busqueda', 'StadisticsController@actionsPMDCustom')->name('stadistics.actions.pmd.custom');
        Route::post('/actividades/pmd/imprimir', 'StadisticsController@actionsPMDPDF')->name('stadistics.actions.pmd.custom.print');
        
        //Reporte de Actividades del Plan Municipal
        Route::get('/actividades/pmd2', 'StadisticsController@actionsPMD2')->name('stadistics.actions.pmd2');
        Route::post('/actividades/pmd2/busqueda', 'StadisticsController@actionsPMD2Custom')->name('stadistics.actions.pmd2.custom');
        Route::post('/actividades/pmd2/imprimir', 'StadisticsController@actionsPMD2PDF')->name('stadistics.actions.pmd2.custom.print');
        
        //Reporte de Actividades de INAFED
        Route::get('/actividades/adm', 'StadisticsController@actionsADM')->name('stadistics.actions.adm');
        Route::post('/actividades/adm/busqueda', 'StadisticsController@actionsADMCustom')->name('stadistics.actions.adm.custom');
        Route::post('/actividades/adm/imprimir', 'StadisticsController@actionsADMPDF')->name('stadistics.actions.adm.custom.print');
        
        //Reporte de Actividades de Agenda 2030
        Route::get('/actividades/a2030', 'StadisticsController@actionsA2030')->name('stadistics.actions.a2030');
        Route::post('/actividades/a2030/busqueda', 'StadisticsController@actionsA2030Custom')->name('stadistics.actions.a2030.custom');
        Route::post('/actividades/a2030/imprimir', 'StadisticsController@actionsA2030PDF')->name('stadistics.actions.a2030.custom.print');

        Route::get('/lineas/pmd', 'StadisticsController@linesPMD')->name('stadistics.lines.pmd');

        Route::any('/pmd', 'StadisticsController@pmd')->name('stadistics.pmd');
        Route::post('/pmd/imprimir', 'StadisticsController@pmdPDF')->name('stadistics.pmd.print');

        Route::any('/lineas-accion/pmd', 'StadisticsController@actionLinesPMD')->name('stadistics.pmd.action.lines');
        Route::get('/lineas-accion/pmd/imprimir', 'StadisticsController@actionLinesPMDPDF')->name('stadistics.pmd.action.lines.print');
        
        Route::any('/lineas-accion/adm', 'StadisticsController@actionLinesADM')->name('stadistics.adm.action.lines');
        Route::get('/lineas-accion/adm/imprimir', 'StadisticsController@actionLinesADMPDF')->name('stadistics.adm.action.lines.print');
        
        Route::any('/lineas-accion/a2030', 'StadisticsController@actionLinesA2030')->name('stadistics.a2030.action.lines');
        Route::get('/lineas-accion/a2030/imprimir', 'StadisticsController@actionLinesA2030PDF')->name('stadistics.a2030.action.lines.print');

        Route::any('/inafed', 'StadisticsController@inafed')->name('stadistics.inafed');
        Route::post('/inafed/imprimir', 'StadisticsController@inafedPDF')->name('stadistics.inafed.print');

        Route::any('/agenda2030', 'StadisticsController@agenda2030')->name('stadistics.agenda2030');
        Route::post('/agenda2030/imprimir', 'StadisticsController@agenda2030PDF')->name('stadistics.agenda2030.print');

    });

    //Web Page
    Route::namespace('WebSite')->prefix('pagina-web')->group(function(){
        Route::get('/noticias', 'WebSiteController@index')->name('notices');
        Route::get('/noticias/NuevaNoticia', 'WebSiteController@newNotice')->name('new.notice');
        Route::post('/noticias/AgregarNoticia', 'WebSiteController@storeNotice')->name('store.notice');
    });

    //Oromapas
    Route::namespace('Oromapas')->prefix('oromapas')->group(function(){
        Route::get('/', 'OromapasController@index')->name('oromapas');
        Route::get('/guardar-shape-view', 'OromapasController@registrarView')->name('store.shape.view');
        Route::post('/guardar-shape', 'OromapasController@registrar')->name('store.shape');
        Route::post('/obtener-predio', 'OromapasController@getPredio')->name('get.predio.by.id'); 
        Route::post('/obtener-hidraulica', 'OromapasController@getHidraulica')->name('get.hidraulica.by.id'); 
        Route::post('/obtener-pozo', 'OromapasController@getPozo')->name('get.pozo.by.id'); 
        Route::post('/obtener-sanitaria', 'OromapasController@getSanitaria')->name('get.sanitaria.by.id'); 
        Route::post('/editar-predio', 'OromapasController@editPredio')->name('edit.predio.by.id');
        Route::post('/editar-hidraulica', 'OromapasController@editHidraulica')->name('edit.hidraulica.by.id');
        Route::post('/editar-pozo', 'OromapasController@editPozo')->name('edit.pozo.by.id');
        Route::post('/editar-sanitaria', 'OromapasController@editSanitaria')->name('edit.sanitaria.by.id');
        Route::get('/obtener-predios-localidad', 'OromapasController@getPrediosLocalidad')->name('get.predios.by.localidad');
        Route::post('/actualizar-saldo', 'OromapasController@updateCredit')->name('update.credit');
        Route::any('/cortar-poligon', 'OromapasController@cutPolygon')->name('cut.polygon');
        Route::post('/eliminar-poligon', 'OromapasController@deletePolygon')->name('delete.polygon');
        Route::post('/crear-poligon', 'OromapasController@createPolygon')->name('create.polygon');
    });
    
    //Mapa
    Route::namespace('Administration')->prefix('mapa')->group(function(){
        Route::get('/', 'GeoController@index')->name('geo.index');
        Route::post('/registro', 'GeoController@registerMarker')->name('geo.register.marker');
        Route::post('/obtener', 'GeoController@getDataMarker')->name('geo.get.data');
        Route::post('/obtener/informacion/reportes', 'GeoController@getDataReports')->name('geo.get.data.report');
        Route::post('/actualizar', 'GeoController@updateMarker')->name('geo.update.marker');
        Route::get('/reporte', 'GeoController@viewReport')->name('geo.view.report');
        Route::post('/reporte/imprimir', 'GeoController@printReport')->name('geo.print.report');
        Route::post('/guardar/registro/filtro', 'GeoController@registerPersonFilter')->name('geo.register.filter.person');
        Route::post('/guardar/registro/dosificador', 'GeoController@registerDosificador')->name('geo.register.dosificador');
        Route::post('/guardar/registro/apoyo/hospital', 'GeoController@registerApoyoHospital')->name('geo.register.apoyo.hospital');
        Route::post('/guardar/registro/porciones', 'GeoController@registerPorciones')->name('geo.register.porciones');
        Route::post('/obtener/unidades', 'GeoController@getUnits')->name('geo.get.units');
        Route::post('/guardar/reporte', 'GeoController@registerReport')->name('geo.register.report');
    });
    
    //PMDU
    Route::namespace('Administration')->prefix('pmdu')->group(function(){
        Route::get('/observaciones', 'PmduController@observations')->name('pmdu.observations');
        Route::post('/informcion/observacion', 'PmduController@gatDataObservation')->name('get.data.observation.pmdu');
        Route::post('/responder/observaciones', 'PmduController@responseObservationPmdu')->name('response.observation.pmdu');
        Route::post('/firmar/observaciones', 'PmduController@signatureObservationPmdu')->name('signature.pdf.response');
        Route::post('/enviar/observaciones', 'PmduController@sendOficces')->name('send.all.ofices');
        Route::post('/store/observaciones', 'PmduController@addObservationPmdu')->name('add.observation.pmdu');
        Route::get('/respuesta/observaciones/{id}', 'PmduController@generatePdfResponse')->name('generate.pdf.response');
        Route::post('/exportar/observaciones', 'PmduController@exportToPdf')->name('export.to.pdf');
        Route::get('/estadisticas', 'PmduController@stadistics')->name('pmdu.stadistics');
        Route::get('/estadisticas/eje', 'PmduController@stadisticsPerAxe')->name('pmdu.stadistics.per.axe');
    });

    /*************************************************************************
     *
     *  Moduls
     *
    **************************************************************************/

    /*************************************************************************
     *  'Modulo de Planeacion', contiene las urls para acceder a las funciones de
     *  asignacion de Lineas de accion, creacion de programas, creacion de acciones,
     *  generar los poas de todas las direcciones.
     *************************************************************************/
    Route::namespace('Planning')->prefix('planeacion')->group(function () {
        //Routes for Assign action lines Municipal Plan
        Route::get('/plan-municipal', 'PlanningController@municipalPlan')->middleware('checkProfile')->name('municipal.plan');
        Route::post('/asignar/plan-municipal', 'PlanningController@assignMunicipalPlan')->name('assign.municipal.plan');
        Route::post('/terminar/plan-municipal', 'PlanningController@finishedMunicipalPlan')->name('finished.municipal.plan');
        Route::post('/desasignar/plan-municipal', 'PlanningController@dettachMunicipalPlan')->name('dettach.municipal.plan');

        Route::post('/obtener/linea-accion/plan-municipal', 'PlanningController@getPmActionLineById')->name('get.pm.action.line.by.id');
        Route::post('/obtener/linea-accion/rechazada/plan-municipal', 'PlanningController@getPmActionRejectLineById')->name('get.pm.action.line.reject.by.id');
        Route::post('/aceptar/linea-accion/plan-municipal', 'PlanningController@acceptPmActionLine')->name('accept.pm.action.line');
        Route::post('/rechazar/linea-accion/plan-municipal', 'PlanningController@rejectPmActionLine')->name('reject.pm.action.line');

        //Routes for Assign Indicators INAFED
        Route::get('/inafed', 'PlanningController@inafed')->middleware('checkProfile')->name('inafed');
        Route::post('/asignar/inafed', 'PlanningController@assignInafed')->name('assign.inafed');
        Route::post('/desasignar/inafed', 'PlanningController@dettachInafed')->name('dettach.inafed');

        Route::post('/obtener/indicador/inafed', 'PlanningController@getInfIndicatorById')->name('get.inf.indicator.by.id');
        Route::post('/obtener/indicador/rechazado/inafed', 'PlanningController@getInfIndicatorRejectLineById')->name('get.inf.indicator.reject.by.id');
        Route::post('/aceptar/indicador/inafed', 'PlanningController@acceptInfIndicator')->name('accept.inf.indicator');
        Route::post('/rechazar/indicador/inafed', 'PlanningController@rejectInfIndicator')->name('reject.inf.indicator');

        //Routes for Assign Indicators Schedule 2030
        Route::get('/agenda-2030', 'PlanningController@schedule')->middleware('checkProfile')->name('schedule');
        Route::post('/asignar/agenda-2030', 'PlanningController@assignSchedule')->name('assign.schedule');
        Route::post('/desasignar/agenda-2030', 'PlanningController@dettachSchedule')->name('dettach.schedule');

        Route::post('/obtener/meta/agenda-2030', 'PlanningController@getScheMetaById')->name('get.sche.meta.by.id');
        Route::post('/obtener/meta/rechazada/agenda-2030', 'PlanningController@getScheMetaRejectById')->name('get.sche.meta.reject.by.id');
        Route::post('/aceptar/meta/agenda-2030', 'PlanningController@acceptScheMeta')->name('accept.sche.meta');
        Route::post('/rechazar/meta/agenda-2030', 'PlanningController@rejectScheMeta')->name('reject.sche.meta');

        //Routes for Assign Indicators Mir
        Route::get('/mir', 'PlanningController@mir')->middleware('checkProfile')->name('mir');
        Route::post('/asignar/mir', 'PlanningController@assignMir')->name('assign.mir');
        Route::post('/desasignar/mir', 'PlanningController@dettachMir')->name('dettach.mir');

        Route::post('/obtener/meta/mir', 'PlanningController@getMirIndicatorById')->name('get.mir.indicator.by.id');
        Route::post('/obtener/indicador/rechazado/mir', 'PlanningController@getMirIndicatorRejectById')->name('get.mir.indicator.reject.by.id');
        Route::post('/aceptar/indicador/mir', 'PlanningController@acceptMirIndicator')->name('accept.mir.indicator');
        Route::post('/rechazar/indicador/mir', 'PlanningController@rejectMirIndicator')->name('reject.mir.indicator');

        //Routes for all normatives
        Route::get('/normativa/asignada', 'PlanningController@municipalPlanClient')->middleware('checkProfile')->name('municipal.plan.client');

        //POA
        Route::get('/poa', 'PlanningController@makePOA')->middleware('checkProfile')->name('make.poa');
        Route::post('/guardar/programa', 'PlanningController@storeProgram')->name('store.program');
        Route::get('/editar/programa/{id}', 'PlanningController@updateProgram')->name('update.program');
        Route::post('/eliminar/programa', 'PlanningController@deleteProgram')->name('delete.program');
        Route::get('/agregar/accion/{id}', 'PlanningController@newActionPoa')->name('new.action.poa');
        Route::post('/guardar/accion', 'PlanningController@storeActionPoa')->name('store.action.poa');
        Route::post('/editar/accion', 'PlanningController@updateActionPoa')->name('update.action.poa');
        Route::post('/eliminar/accion', 'PlanningController@deleteActionPoa')->name('delete.action.poa');
        Route::post('/obtener/lineas-accion', 'PlanningController@getActionLinesByNormative')->name('get.action.lines.by.normative');
        Route::get('/editar/accion/{id}', 'PlanningController@editActionPoa')->name('edit.action.poa');
        Route::get('/generar/poa', 'PlanningController@generateMyPoa')->name('generate.my.poa');

        //Administration POA
        Route::get('/todos/poa', 'PlanningController@allPOA')->middleware('checkProfile')->name('all.poa');
        Route::get('/ver/poa/{id}', 'PlanningController@viewPoaDirection')->name('view.poa.direction');
        //Route::get('/editar/programa/{id}', 'PlanningController@updateProgramDirection')->name('update.program.direction');
        Route::get('/agregar/accion/direccion/{id}', 'PlanningController@newActionPoaDirection')->name('new.action.poa.direction');
        Route::post('/obtener/direccion/lineas-accion', 'PlanningController@getActionLinesByNormativeDirection')->name('get.action.lines.by.normative.direction');

        //Assigned Normative
        Route::get('/normativa/asignada/direcciones', 'PlanningController@assignedNormative')->name('assigned.normative');



    });

    Route::namespace('Planning')->prefix('nueva-planeacion')->group(function () {
        //Routes for Assign action lines Municipal Plan
        Route::get('/plan-municipal', 'NewPlanningController@municipalPlan')->middleware('checkProfile')->name('new.municipal.plan');
        Route::post('/obtener/objetivos/pmd', 'NewPlanningController@getPmdObjectives')->name('new.get.pmd.objectives');
        Route::post('/asignar/plan-municipal', 'NewPlanningController@assignMunicipalPlan')->name('new.assign.municipal.plan');
        Route::post('/terminar/plan-municipal', 'NewPlanningController@finishedMunicipalPlan')->name('new.finished.municipal.plan');
        Route::post('/desasignar/plan-municipal', 'NewPlanningController@dettachMunicipalPlan')->name('new.dettach.municipal.plan');

        Route::post('/obtener/linea-accion/plan-municipal', 'NewPlanningController@getPmActionLineById')->name('new.get.pm.action.line.by.id');
        Route::post('/obtener/linea-accion/rechazada/plan-municipal', 'NewPlanningController@getPmActionRejectLineById')->name('new.get.pm.action.line.reject.by.id');
        Route::post('/aceptar/linea-accion/plan-municipal', 'NewPlanningController@acceptPmActionLine')->name('new.accept.pm.action.line');
        Route::post('/rechazar/linea-accion/plan-municipal', 'NewPlanningController@rejectPmActionLine')->name('new.reject.pm.action.line');

        //Routes for Assign Indicators INAFED
        Route::get('/inafed', 'NewPlanningController@inafed')->middleware('checkProfile')->name('new.inafed');
        Route::post('/asignar/inafed', 'NewPlanningController@assignInafed')->name('new.assign.inafed');
        Route::post('/desasignar/inafed', 'NewPlanningController@dettachInafed')->name('new.dettach.inafed');

        Route::post('/obtener/indicador/inafed', 'NewPlanningController@getInfIndicatorById')->name('new.get.inf.indicator.by.id');
        Route::post('/obtener/indicador/rechazado/inafed', 'NewPlanningController@getInfIndicatorRejectLineById')->name('new.get.inf.indicator.reject.by.id');
        Route::post('/aceptar/indicador/inafed', 'NewPlanningController@acceptInfIndicator')->name('new.accept.inf.indicator');
        Route::post('/rechazar/indicador/inafed', 'NewPlanningController@rejectInfIndicator')->name('new.reject.inf.indicator');

        //Routes for Assign Indicators Schedule 2030
        Route::get('/agenda-2030', 'NewPlanningController@schedule')->middleware('checkProfile')->name('new.schedule');
        Route::post('/asignar/agenda-2030', 'NewPlanningController@assignSchedule')->name('new.assign.schedule');
        Route::post('/desasignar/agenda-2030', 'NewPlanningController@dettachSchedule')->name('new.dettach.schedule');

        Route::post('/obtener/meta/agenda-2030', 'NewPlanningController@getScheMetaById')->name('new.get.sche.meta.by.id');
        Route::post('/obtener/meta/rechazada/agenda-2030', 'NewPlanningController@getScheMetaRejectById')->name('new.get.sche.meta.reject.by.id');
        Route::post('/aceptar/meta/agenda-2030', 'NewPlanningController@acceptScheMeta')->name('new.accept.sche.meta');
        Route::post('/rechazar/meta/agenda-2030', 'NewPlanningController@rejectScheMeta')->name('new.reject.sche.meta');

        //Routes for Assign Indicators Mir
        Route::get('/mir', 'NewPlanningController@mir')->middleware('checkProfile')->name('new.mir');
        Route::post('/guardar/mir', 'NewPlanningController@storeProgramMir')->name('new.store.program');
        Route::post('/obtener/mir', 'NewPlanningController@getProgramMir')->name('new.get.program.mir');
        Route::post('/actualizar/mir', 'NewPlanningController@updateProgramMir')->name('new.update.program');
        Route::post('/asignar/mir', 'NewPlanningController@assignMir')->name('new.assign.mir');
        Route::post('/desasignar/mir', 'NewPlanningController@dettachMir')->name('new.dettach.mir');

        Route::post('/obtener/meta/mir', 'NewPlanningController@getMirIndicatorById')->name('new.get.mir.indicator.by.id');
        Route::post('/obtener/indicador/rechazado/mir', 'NewPlanningController@getMirIndicatorRejectById')->name('new.get.mir.indicator.reject.by.id');
        Route::post('/aceptar/indicador/mir', 'NewPlanningController@acceptMirIndicator')->name('new.accept.mir.indicator');
        Route::post('/rechazar/indicador/mir', 'NewPlanningController@rejectMirIndicator')->name('new.reject.mir.indicator');

        //Routes for all normatives
        Route::get('/normativa/asignada', 'NewPlanningController@normativeAssignedDirection')->middleware('checkProfile')->name('new.normative.assigned.direction');

        //MIR Design
        Route::get('/diseño/mir', 'NewPlanningController@makeMirArea')->middleware('checkProfile')->name('new.mirs.design');
        Route::post('/guardar/area', 'NewPlanningController@storeArea')->name('new.store.area');
        Route::post('/editar/area', 'NewPlanningController@updateArea')->name('new.edit.area');
        Route::get('/actualizar/area/{id}', 'NewPlanningController@makeMir')->name('new.update.area');
        Route::post('/eliminar/arrea', 'NewPlanningController@deleteArea')->name('new.delete.area');

        Route::post('/guardar/componente', 'NewPlanningController@storeComponent')->name('new.store.component');
        Route::post('/actualizar/componente', 'NewPlanningController@editComponent')->name('new.edit.component');
        Route::post('/obtener/componente', 'NewPlanningController@getComponentById')->name('get.component.by.id');
        Route::get('/editar/componente/{id}', 'NewPlanningController@updateComponent')->name('new.update.component');
        Route::post('/eliminar/componente', 'NewPlanningController@deleteComponent')->name('new.delete.component');
        Route::get('/agregar/accion/{id}', 'NewPlanningController@newActionComponent')->name('new.action.component');
        Route::post('/guardar/accion', 'NewPlanningController@storeActionComponent')->name('new.store.action.component');
        Route::post('/editar/accion', 'NewPlanningController@updateActionComponent')->name('new.update.action.component');
        Route::post('/eliminar/accion', 'NewPlanningController@deleteActionComponent')->name('new.delete.action.component');
        Route::get('/editar/accion/{id}', 'NewPlanningController@editActionComponent')->name('new.edit.action.component');
        
        Route::post('/obtener/lineas-accion', 'NewPlanningController@getActionLinesByNormative')->name('new.get.action.lines.by.normative');
        Route::get('/generar/mir', 'NewPlanningController@generateMyMir')->name('new.generate.my.mir');

        //Administration POA
        Route::get('/todas/mir', 'NewPlanningController@allMir')->middleware('checkProfile')->name('new.all.mir');
        Route::get('/ver/poa/{id}', 'NewPlanningController@viewMirDirection')->name('view.mir.direction');
        //Route::get('/editar/programa/{id}', 'NewPlanningController@updateProgramDirection')->name('new.update.program.direction');
        Route::get('/agregar/accion/direccion/{id}', 'NewPlanningController@newActionMirDirection')->name('new.new.action.poa.direction');
        Route::post('/obtener/direccion/lineas-accion', 'NewPlanningController@getActionLinesByNormativeDirection')->name('new.get.action.lines.by.normative.direction');

        //Assigned Normative
        Route::get('/normativa/asignada/direcciones', 'NewPlanningController@assignedNormative')->name('new.normative.assigned');

  


    });

    /*************************************************************************
     *  'Modulo de Segimiento', Contiene todas las urls para la captura del
     *  avance trimestral de los poas, captura de actividades para el informe
     *  de gobierno.
     *************************************************************************/
    Route::namespace('Tracing')->prefix('seguimiento')->group(function () {

        //Rutas para la captura de actividades del informe anual.
        Route::get('/informe-anual', 'TracingController@monthlyReport')->middleware('checkProfile')->name('monthly.report');
        Route::post('/guardar/informe-anual', 'TracingController@storeMonthlyReport')->name('store.monthly.report');

        Route::post('/obtener/actividad/mensual', 'TracingController@getMonthlyActivityById')->name('get.monthly.activity.by.id');
        Route::any('/descargar/evidencia/{id}', 'TracingController@downloadEvidenceById')->name('download.evidence.by.id');

        //Rutas para la captura de actividades del informe anual.
        Route::get('/poa', 'TracingController@poa')->middleware('checkProfile')->name('poa');
        Route::post('/obtener/meta', 'TracingController@getActionPoaById')->name('get.action.poa.by.id');
        Route::post('/guardar/mestas', 'TracingController@storeActionGoals')->name('store.action.goals');

        //Rutas revisar el avance de los POA´s.
        Route::get('/avance/poas', 'TracingController@advancePoas')->middleware('checkProfile')->name('advance.poas');
        Route::get('/ver/avance/poa', 'TracingController@viewPoaAdvance')->name('view.poa.advance');

        //Rutas para generar avance de INAFED y AGENDA 2030
        Route::get('/avance/inafed', 'TracingController@advanceInafed')->middleware('checkProfile')->name('advance.inafed');
        Route::get('/avance/agenda2030', 'TracingController@advanceAgenda2030')->middleware('checkProfile')->name('advance.agenda2030');
        Route::get('/avance/mir', 'TracingController@advanceMir')->middleware('checkProfile')->name('advance.mir');
    });

    //nuevas rutas kareem
    Route::namespace('Tracing')->prefix('nuevo-seguimiento')->group(function(){
        Route::get('ver/avance/mirs', 'NewTracingController@avanceMirs')->name('new.advance.mirs');
        Route::get('ver/avance/mir', 'NewTracingController@viewMirAdvance')->name('new.mir.advance');

        Route::get('/avance/mir', 'NewTracingController@mirAdvance')->middleware('checkProfile')->name('new.mir.avance');

        Route::post('/obtener/meta', 'NewTracingController@getActionMirById')->name('get.action.mir.by.id');
        Route::post('/guardar/metas', 'NewTracingController@storeActionGoalsMir')->name('store.action.mir.goals');
        
    });

    //fin nuevo seguimiento trimestral kareem
});
/*********************************** End All Routes *******************************/

/*********************************** Secret Routes ***********************************/
Route::prefix('secret')->group(function () {
    Route::get('/crear-roles/{password?}', 'SecretController@createRoles');
    Route::get('/asignar-administrador/{password?}', 'SecretController@assignAdministrator');
    Route::get('/script/clean/pmd-actions', 'SecretController@cleanPmdActions');
    Route::get('/nuevo/period', 'SecretController@makeNewPeriod');
    Route::get('/clean/mirs/{password?}/{period}', 'SecretController@cleanMirs');
});
/*********************************** End Secret Routes *******************************/
