<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;

use Auth;
use App\Models\Notifications;
use App\Models\Menu;
use App\Models\Roledb;

use Spatie\Permission\Models\Role;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNewsNotifications()
    {
        return Notifications::where("user_id", Auth::user()->id)->where("estatus", 0)->get();
    }
    
    public function getAllNotifications()
    {
        return Notifications::where("user_id", Auth::user()->id)->get();
    }

    public function profile()
    {
        return $this->belongsTo('App\Models\Profile', 'perfil_id');
    }

    public function getMenu()
    {
        // if(session()->has('menu'))
        // {
        //     return session("menu");
        // }else {
            $user = Auth::user();
            $role_lv = Role::findByName($user->getRoleNames()[0]);
            $role = Roledb::find($role_lv->id);
            $navs = [];
            $parents = [];
            foreach($role->menus()->orderBy("posicion")->get() as $menu)
            {
                if(empty($menu->padre))
                {
                    $data_nav = [
                        'id' => $menu->id,
                        'name' => $menu->nombre,
                        'url' => $menu->url,
                        'icon' => $menu->icono,
                        'position' => $menu->posicion,
                        'alias' => $menu->alias,
                        'submenu' => []
                    ];
                    $navs[] = $data_nav;
                }else {
                    $parent = Menu::find($menu->padre);
                    if (!in_array($parent->id, $parents)) 
                    {
                        $parents[] = $parent->id;
                        $data_nav = [
                            'id' => $parent->id,
                            'name' => $parent->nombre,
                            'url' => $parent->url,
                            'icon' => $parent->icono,
                            'position' => $parent->posicion,
                            'alias' => $parent->alias,
                            'submenu' => []
                        ];
                        $navs[] = $data_nav;
                        foreach ($navs as $key => $nav) {
                            if($navs[$key]["id"] == $parent->id)
                            {
                                $data_nav = [
                                    'id' => $menu->id,
                                    'name' => $menu->nombre,
                                    'url' => $menu->url,
                                    'icon' => $menu->icono,
                                    'position' => $menu->posicion,
                                    'alias' => $menu->alias,
                                ];
                                $navs[$key]["submenu"][] = $data_nav;
                            }
                        }
                    }else {
                        foreach ($navs as $key => $nav) {
                            if($navs[$key]["id"] == $parent->id)
                            {
                                $data_nav = [
                                    'id' => $menu->id,
                                    'name' => $menu->nombre,
                                    'url' => $menu->url,
                                    'icon' => $menu->icono,
                                    'position' => $menu->posicion,
                                    'alias' => $menu->alias,
                                ];
                                $navs[$key]["submenu"][] = $data_nav;
                            }
                        }
                    }
                }
            }
            $position = array();
            foreach ($navs as $key => $nav)
            {
                $position[$key] = $nav['position'];
            }
            array_multisort($position, SORT_ASC, $navs);
            session(['menu' => $navs]);
            return $navs;
        // }
    }
}
