<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;
use Storage;
//Models
use App\User;

//PM -> Municipal Plan
use App\Models\PmAxe;
use App\Models\PmActionLine;
use App\Models\PmActionRejected;
//use App\Models\PmLineActionHasDirection;

//Inf -> INAFED
use App\Models\InfSchedule;
use App\Models\InfAxe;
use App\Models\InfIndicator;
use App\Models\InfIndicatorRejected;

//Inf -> Schedule 2030
use App\Models\AgeObjective;
use App\Models\AgeMeta;
use App\Models\AgeMetaRejected;

//Inf -> Mir
use App\Models\MirProgram;
use App\Models\MirIndicator;
use App\Models\MirIndicatorLevel;
use App\Models\MirIndicatorRejected;

//Others
use App\Models\Program;
use App\Models\Direction;
use App\Models\ActionPoa;
use App\Models\SubactionPoa;
use App\Models\Normative;
use App\Models\Position;

class PlanningRepository
{
	public function getAxes()
	{
		return PmAxe::all();
	}
	
	public function getAxesADM()
	{
		return InfAxe::orderBy("punto")->get();
	}
	
	public function getAxesA2030()
	{
		return AgeObjective::orderBy("punto")->get();
	}
	
	public function getSchedulesInafed()
	{
		return InfSchedule::all();
	}

	public function getObjectivesSchedule()
	{
		return AgeObjective::all();
	}
	
	public function getProgramsMir()
	{
		// return MirProgram::all();
		return MirProgram::where('periodo', config('implan.periods.previous'))->get();
	}
	
	public function getProgramsMirPeriods()
	{
		return MirProgram::select("periodo")->groupBy("periodo")->get();
	}
	
	public function getMirIdicatorsTypes()
	{
		return MirIndicatorLevel::all();
	}

	public function getNormatives()
	{
		return Normative::all();
	}

	public function getAllDirections()
	{
		return Direction::all();
	}
	
	public function getDirectionById($id)
	{
		return Direction::find($id);
	}

	public function getActionLinesByNormative($data)
	{
		$normative = Normative::find($data->normative);
		$direction = Direction::find($data->direction);
		
		switch ($normative->nombre) {
			case 'Plan Municipal':
				$normative = $direction->actionLines()->wherePivot('period', config('implan.periods.previous'))->wherePivot("status", 1)->get();
				break;
			case 'INAFED':
				$normative = $direction->infIndicators()->wherePivot('period', config('implan.periods.previous'))->wherePivot("status", 1)->get();
				break;
			case 'Agenda 2030':
				$normative = $direction->scheMeta()->wherePivot('period', config('implan.periods.previous'))->wherePivot("estatus", 1)->get();	
				break;
			case 'MIR':
				$normative = $direction->mirIndicators()->wherePivot('period', config('implan.periods.previous'))->wherePivot("status", 1)->get();	
				break;
		}
		return $normative;
	}

	public function assignActionLines($data)
	{
		$direction = Direction::find($data->direction);
		
		foreach ($data->actionlines as $key => $actionline) {
			$direction->actionLines()->attach($actionline, ["period" => config('implan.periods.previous')]);
		}
	}
	public function finishedActionLines($data)
	{
		
		foreach ($data->actionlines as $key => $actionline) {
			$line = PmActionLine::find($actionline);
			$line->finished_at = config('implan.periods.previous');
			$line->status = 1;
			$line->save();
		}
	}
	
	public function assignIndicators($data)
	{
		$direction = Direction::find($data->direction);
		
		foreach ($data->indicators as $key => $indicator) {
			$direction->infIndicators()->attach($indicator, ["period" => config('implan.periods.previous')]);
		}
	}

	public function finishedIndicators($data)
	{
		
		foreach ($data->indicators as $key => $actionline) {
			$line = InfIndicator::find($actionline);
			$line->finished_at = config('implan.periods.next');
			$line->status = 1;
			$line->save();
		}
	}
	
	public function assignSchedule($data)
	{
		$direction = Direction::find($data->direction);
		
		foreach ($data->metas as $key => $meta) {
			$direction->scheMeta()->attach($meta, ["period" => config('implan.periods.previous')]);
		}
	}

	public function finishedSchedule($data)
	{
		
		foreach ($data->metas as $key => $actionline) {
			$line = AgeMeta::find($actionline);
			$line->finished_at = config('implan.periods.next');
			$line->status = 1;
			$line->save();
		}
	}
	
	public function assignMir($data)
	{
		$direction = Direction::find($data->direction);
		
		foreach ($data->indicators as $key => $indicator) {
			$direction->mirIndicators()->attach($indicator, ["period" => config('implan.periods.previous')]);
		}
	}

	public function finishedMir($data)
	{
		
		foreach ($data->indicators as $key => $actionline) {
			$line = MirIndicator::find($actionline);
			$line->finished_at = config('implan.periods.next');
			$line->status = 1;
			$line->save();
		}
	}

	public function dettachActionLines($data)
	{
		
		$direction = Direction::find($data->direction);
		foreach ($direction->programs()->where('period', config('implan.periods.previous'))->get() as $p_key => $program) 
		{
			foreach ($program->actions as $a_key => $action) 
			{
				$action->pm_action_lines()->detach($data->line);
			}
		}
		$direction->actionLines()->wherePivot('period', config('implan.periods.previous'))->detach($data->line);
	}
	
	public function dettachInafed($data)
	{
		$direction = Direction::find($data->direction);
		foreach ($direction->programs()->where('period', config('implan.periods.previous'))->get() as $p_key => $program) 
		{
			foreach ($program->actions as $a_key => $action) 
			{
				$action->inf_action_lines()->detach($data->line);
			}
		}
		$direction->infIndicators()->wherePivot('period', config('implan.periods.previous'))->detach($data->line);
	}

	public function dettachSchedule($data)
	{
		$direction = Direction::find($data->direction);
		foreach ($direction->programs()->where('period', config('implan.periods.previous'))->get() as $p_key => $program) 
		{
			foreach ($program->actions as $a_key => $action) 
			{
				$action->sche_metas()->detach($data->line);
			}
		}
		$direction->scheMeta()->wherePivot('period', config('implan.periods.previous'))->detach($data->line);
	}
	
	public function dettachMir($data)
	{
		// dd($data->all());
		$direction = Direction::find($data->direction);
		foreach ($direction->programs()->where('period', $data->period)->get() as $p_key => $program) 
		{
			foreach ($program->actions as $a_key => $action) 
			{
				$action->mir_idicators()->detach($data->line);
			}
		}
		$direction->mirIndicators()->wherePivot('period', $data->period)->detach($data->line);
	}

	public function getPmActionLineById($data)
	{
		return PmActionLine::find($data->id);
	}
	
	public function getInfIndicatorById($data)
	{
		return InfIndicator::find($data->id);
	}
	
	public function getScheMetaById($data)
	{
		return AgeMeta::find($data->id);
	}
	
	public function getMirIndicatorById($data)
	{
		return MirIndicator::find($data->id);
	}

	public function getResposable($id)
	{
		return Position::find($id);
	}

	public function getPmActionRejectLineById($id)
	{
		return PmActionRejected::where("pm_linea_accion_id", $id)->first();
	}
	
	public function getInfIndicatorRejectLineById($id)
	{
		return InfIndicatorRejected::where("inf_indicador_id", $id)->first();
	}
	
	public function getScheMetaRejectById($id)
	{
		return AgeMetaRejected::where("age_meta_id", $id)->first();
	}
	
	public function getMirIndicatorRejectById($id)
	{
		return MirIndicatorRejected::where("mir_indicador_id", $id)->first();
	}

	public function acceptPmActionLine($data)
	{
		$actionline = PmActionLine::find($data->id);
		$actionline->directions()->wherePivot('period', config('implan.periods.previous'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["status" => 1]);
	}

	public function rejectPmActionLine($data)
	{
		$file = $data->file("file");
		$originalName = $file->getClientOriginalName();
		$ex = $file->getClientOriginalExtension();
		$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		while (Storage::disk('local')->exists('actions_rejecteds/'.$name.".".$ex)) 
		{
			$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		}
		
		if(Storage::disk('local')->putFileAs('actions_rejecteds/', $file, $name.".".$ex))
		{
			$action = new PmActionRejected;
			$action->descripcion = $data->description;
			$action->original_nombre = $originalName;
			$action->nombre = $name;
			$action->url = 'actions_rejecteds/';
			$action->user_id = Auth::user()->id;
			$action->pm_linea_accion_id = $data->id_action_line;
			$action->save();

			$actionline = PmActionLine::find($data->id_action_line);
			$actionline->directions()->wherePivot('period', config('implan.periods.previous'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["status" => 2]);
		}
	}

	public function acceptInfIndicator($data)
	{
		$indicator = InfIndicator::find($data->id);
		$indicator->directions()->wherePivot('period', config('implan.periods.previous'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["status" => 1]);
	}

	public function rejectInfIndicator($data)
	{
		$file = $data->file("file");
		$originalName = $file->getClientOriginalName();
		$ex = $file->getClientOriginalExtension();
		$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		while (Storage::disk('local')->exists('indicators_rejecteds/'.$name.".".$ex)) 
		{
			$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		}
		if(Storage::disk('local')->putFileAs('indicators_rejecteds/', $file, $name.".".$ex))
		{
			$action = new InfIndicatorRejected;
			$action->descripcion = $data->description;
			$action->original_nombre = $originalName;
			$action->nombre = $name;
			$action->url = 'indicators_rejecteds/';
			$action->user_id = Auth::user()->id;
			$action->inf_indicador_id = $data->id_inf_indicator;
			$action->save();

			$actionline = InfIndicator::find($data->id_inf_indicator);
			$actionline->directions()->wherePivot('period', config('implan.periods.previous'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["status" => 2]);
		}
	}

	public function acceptScheMeta($data)
	{
		$indicator = AgeMeta::find($data->id);
		$indicator->directions()->wherePivot('period', config('implan.periods.previous'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["estatus" => 1]);
	}
	
	public function rejectScheMeta($data)
	{
		$file = $data->file("file");
		$originalName = $file->getClientOriginalName();
		$ex = $file->getClientOriginalExtension();
		$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		while (Storage::disk('local')->exists('metas_rejecteds/'.$name.".".$ex)) 
		{
			$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		}
		if(Storage::disk('local')->putFileAs('metas_rejecteds/', $file, $name.".".$ex))
		{
			$action = new AgeMetaRejected;
			$action->descripcion = $data->description;
			$action->original_nombre = $originalName;
			$action->nombre = $name;
			$action->url = 'metas_rejecteds/';
			$action->user_id = Auth::user()->id;
			$action->age_meta_id = $data->id_sche_meta;
			$action->save();

			$actionline = AgeMeta::find($data->id_sche_meta);
			$actionline->directions()->wherePivot('period', config('implan.periods.previous'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["estatus" => 2]);
		}
	}
	
	public function acceptMirIndicator($data)
	{
		$indicator = MirIndicator::find($data->id);
		$indicator->directions()->wherePivot('period', config('implan.periods.previous'))->updateExistingPivot($data->direction, ["status" => 1]);
	}
	
	public function rejectMirIndicator($data)
	{
		$file = $data->file("file");
		$originalName = $file->getClientOriginalName();
		$ex = $file->getClientOriginalExtension();
		$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		while (Storage::disk('local')->exists('mir_idicators_rejecteds/'.$name.".".$ex)) 
		{
			$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		}
		if(Storage::disk('local')->putFileAs('mir_idicators_rejecteds/', $file, $name.".".$ex))
		{
			$action = new MirIndicatorRejected;
			$action->descripcion = $data->description;
			$action->original_nombre = $originalName;
			$action->nombre = $name;
			$action->url = 'mir_idicators_rejecteds/';
			$action->user_id = Auth::user()->id;
			$action->mir_indicador_id = $data->id_mir_indicator;
			$action->save();

			$actionline = MirIndicator::find($data->id_mir_indicator);
			$actionline->directions()->wherePivot('period', config('implan.periods.previous'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["status" => 2]);
		}
	}

	public function storeProgram($data)
	{
		$program = new Program;
		$program->nombre = $data->name;
		$program->descripcion = $data->description;
		$program->direccion_id = $data->direction_id;
		$program->period = config('implan.periods.previous');
		$program->save();
	}

	public function getProgramById($id)
	{
		return Program::find($id);
	}

	public function deleteProgram($data)
	{
		$program = Program::find($data->program);

		if(count($program->actions) > 0)
		{
			foreach ($program->actions as $key => $action) {
				//Eliminar las Subacciones
				if(count($action->subactions) > 0)
				{
					foreach ($action->subactions as $key => $subaction) {
						$subaction->delete();
					}
				}
				//Eliminar lineas de Accion asignadas a la acción
				if(!empty($action->pm_action_lines))
				{
					foreach ($action->pm_action_lines as $key => $actionline) {
						$action->pm_action_lines()->detach($actionline->id);
					}
				}
				
				if(!empty($action->inf_action_lines))
				{
					foreach ($action->inf_action_lines as $key => $actionline) {
						$action->inf_action_lines()->detach($actionline->id);
					}
				}
				
				if(!empty($action->sche_metas))
				{
					foreach ($action->sche_metas as $key => $actionline) {
						$action->sche_metas()->detach($actionline->id);
					}
				}
				
				if(!empty($action->mir_idicators))
				{
					foreach ($action->mir_idicators as $key => $actionline) {
						$action->mir_idicators()->detach($actionline->id);
					}
				}
				//Eliminar la acción
				$action->delete();
			}
		}

		if($program->delete())
		{
			return 1;
		}else
		{
			return 0;
		}
	}

	public function storeActionPoa($data)
	{
		$action = new ActionPoa;
		$action->actividad = $data->activity;
		$action->unidad_medida = $data->unity;
		$action->meta_anual = $data->year;
		$action->trimestre_1 = $data->first;
		$action->trimestre_2 = $data->second;
		$action->trimestre_3 = $data->thirth;
		$action->trimestre_4 = $data->four;
		$action->programas_id = $data->program;
		$action->tipo = $data->type;
		$action->valor = $data->value;
		$action->puesto_id = $data->responsable;
		$action->save();

		if(!empty($data->subactivity) > 0)
		{
			foreach ($data->subactivity as $key => $value) {
				$subaction = new SubactionPoa;
				$subaction->actividad = $data->subactivity[$key];
				$subaction->meta_anual = $data->subyear[$key];
				$subaction->trimestre_1 = $data->subfirst[$key];
				$subaction->trimestre_2 = $data->subsecond[$key];
				$subaction->trimestre_3 = $data->subthirth[$key];
				$subaction->trimestre_4 = $data->subfour[$key];
				$subaction->accion_poa_id = $action->id;
				$subaction->save();
			}
		}

		if(!empty($data->pm_normatives) > 0)
		{
			foreach ($data->pm_normatives as $key => $pm_normative) {
				$action->pm_action_lines()->attach($pm_normative);
			}
		}

		if(!empty($data->inf_normatives) > 0)
		{
			foreach ($data->inf_normatives as $key => $inf_normative) {
				$action->inf_action_lines()->attach($inf_normative);
			}
		}
		
		if(!empty($data->sche_normatives) > 0)
		{
			foreach ($data->sche_normatives as $key => $sche_normative) {
				$action->sche_metas()->attach($sche_normative);
			}
		}
		
		if(!empty($data->mir_normatives) > 0)
		{
			foreach ($data->mir_normatives as $key => $mir_normative) {
				$action->mir_idicators()->attach($mir_normative);
			}
		}

	}
	
	public function updateActionPoa($data)
	{
		$action = ActionPoa::find($data->id);
		$action->actividad = $data->activity;
		$action->unidad_medida = $data->unity;
		$action->meta_anual = $data->year;
		$action->trimestre_1 = $data->first;
		$action->trimestre_2 = $data->second;
		$action->trimestre_3 = $data->thirth;
		$action->trimestre_4 = $data->four;
		$action->programas_id = $data->program;
		$action->tipo = $data->type;
		$action->valor = $data->value;
		$action->puesto_id = $data->responsable;
		$action->save();

		if(!empty($action->subactions))
		{
			foreach ($action->subactions as $subaction) 
			{
				$subaction->delete();
			}
		}

		if(!empty($data->subactivity))
		{
			foreach ($data->subactivity as $key => $value) {
				$subaction = new SubactionPoa;
				$subaction->actividad = $data->subactivity[$key];
				$subaction->meta_anual = $data->subyear[$key];
				$subaction->trimestre_1 = $data->subfirst[$key];
				$subaction->trimestre_2 = $data->subsecond[$key];
				$subaction->trimestre_3 = $data->subthirth[$key];
				$subaction->trimestre_4 = $data->subfour[$key];
				$subaction->accion_poa_id = $action->id;
				$subaction->save();
			}
		}
		if(!empty($action->pm_action_lines))
		{
			foreach ($action->pm_action_lines as $key => $actionline) {
				$action->pm_action_lines()->detach($actionline->id);
			}
		}
		
		if(!empty($action->inf_action_lines))
		{
			foreach ($action->inf_action_lines as $key => $actionline) {
				$action->inf_action_lines()->detach($actionline->id);
			}
		}
		
		if(!empty($action->sche_metas))
		{
			foreach ($action->sche_metas as $key => $actionline) {
				$action->sche_metas()->detach($actionline->id);
			}
		}
		
		if(!empty($action->mir_idicators))
		{
			foreach ($action->mir_idicators as $key => $actionline) {
				$action->mir_idicators()->detach($actionline->id);
			}
		}

		if(!empty($data->pm_normatives))
		{
			foreach ($data->pm_normatives as $key => $pm_normative) {
				$action->pm_action_lines()->attach($pm_normative);
			}
		}

		if(!empty($data->inf_normatives))
		{
			foreach ($data->inf_normatives as $key => $inf_normative) {
				$action->inf_action_lines()->attach($inf_normative);
			}
		}
		if(!empty($data->sche_normatives))
		{
			foreach ($data->sche_normatives as $key => $inf_normative) {
				$action->sche_metas()->attach($inf_normative);
			}
		}
		
		if(!empty($data->mir_normatives))
		{
			foreach ($data->mir_normatives as $key => $mir_normative) {
				$action->mir_idicators()->attach($mir_normative);
			}
		}

	}

	public function deleteActionPoa($data)
	{
		$action = ActionPoa::find($data->action);

		//Eliminar las Subacciones
		if(count($action->subactions) > 0)
		{
			foreach ($action->subactions as $key => $subaction) {
				$subaction->delete();
			}
		}
		//Eliminar lineas de Accion asignadas a la acción
		if(!empty($action->pm_action_lines))
		{
			foreach ($action->pm_action_lines as $key => $actionline) {
				$action->pm_action_lines()->detach($actionline->id);
			}
		}
		
		if(!empty($action->inf_action_lines))
		{
			foreach ($action->inf_action_lines as $key => $actionline) {
				$action->inf_action_lines()->detach($actionline->id);
			}
		}
		
		if(!empty($action->sche_metas))
		{
			foreach ($action->sche_metas as $key => $actionline) {
				$action->sche_metas()->detach($actionline->id);
			}
		}
		
		if(!empty($action->mir_idicators))
		{
			foreach ($action->mir_idicators as $key => $actionline) {
				$action->mir_idicators()->detach($actionline->id);
			}
		}
		//Eliminar la acción
		if($action->delete())
		{
			return 1;
		}else
		{
			return 0;
		}
	}

	public function getActionPoaById($id)
	{
		return ActionPoa::find($id);
	}

	public function getDataMyPoa()
	{
		$data = [
			'pm' => Auth::user()->profile->position->direction->actionLines()->wherePivot("status", 1)->get(),
			'inf' => Auth::user()->profile->position->direction->infIndicators()->wherePivot("status", 1)->get()
		];
		return $data;
	}
}