<?php
namespace App\Repositories;

use Auth;

//Models
use App\User;
use App\Models\Ticket;
use App\Models\Priority;
use App\Models\Bitacora;
use App\Models\Notifications;

//Services
use Illuminate\Support\Facades\Hash;

class ClientRepository
{
	public function getActiveTicketsByUser()
	{
		return Ticket::whereIn('estatus_id', [1,2])->where("users_id", Auth::user()->id)->get();
	}
	
	public function getAllTicketsByUser()
	{
		return Ticket::where("users_id", Auth::user()->id)->get();
	}
	
	public function getTicketById($id)
	{
		$ticket =Ticket::find($id);
		$ticket->prioridad_txt = $ticket->priority->nombre;
		$ticket->prioridad_color = $ticket->priority->color;
		$ticket->estatus_txt = $ticket->status->nombre;
		$ticket->estatus_color = $ticket->status->color;
		$ticket->solicitante = $ticket->user->name;

		return $ticket;
	}
	
	// public function changeTicketStatus($id, $status)
	// {
	// 	$ticket =Ticket::find($id);
	// 	$ticket->estatus_id = $status;
	// 	$ticket->save();

		
		
	// 	return $ticket;
	// }
	
	public function getAllPriorities()
	{
		return Priority::all();
	}

	public function storeTicket($data)
	{
		$old_ticket = Ticket::orderBy('id', 'DESC')->first();
		if(empty($old_ticket))
		{
			$folio = "00001";
		}else
		{
			$arr = intval(explode("SOP-", $old_ticket->folio)[1]);
			$folio = str_pad($arr+1, 5, "0", STR_PAD_LEFT);
		}
		$ticket = new Ticket;
		$ticket->folio = "SOP-".$folio;
		$ticket->descripcion = $data->description;
		$ticket->prioridad_id = $data->priority;
		$ticket->estatus_id = 1;
		$ticket->users_id = Auth::user()->id;
		$ticket->save();

		$bitacora = new Bitacora;
		$bitacora->action = "El usuario ". Auth::user()->name . " registro un ticket con folio " . $ticket->folio . ', el dia '. $ticket->created_at;
		$bitacora->users_id = Auth::user()->id;
		$bitacora->save();

		$users = User::all();
		foreach ($users as $key => $user) {
			if($user->hasRole("Administrador"))
			{
				$notification = new Notifications;
				$notification->user_id = $user->id;
				$notification->contenido = "El usuario ". Auth::user()->name . " registro un ticket con folio " . $ticket->folio . ', el dia '. $ticket->created_at;
				$notification->estatus = 0;
				$notification->save();
			}
		}
		return "Se registro correctamente un ticket con folio ". $ticket->folio;
	}
}