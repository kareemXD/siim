<?php
namespace App\Repositories;

use Auth;

//Models
use App\User;
use App\Models\Ticket;
use App\Models\Bitacora;
use App\Models\Notifications;

//Services
use Illuminate\Support\Facades\Hash;

class HomeRepository
{
	public function updateUserName($user_id, $name)
	{
		$user = User::find($user_id);
		$user->name = $name;
		$user->save();
	}
	
	public function updateUser($user_id, $name, $password)
	{
		$user = User::find($user_id);
		$user->name = $name;
		$user->password = Hash::make($password);
		$user->save();
	}

	public function updateNotification($notification_id)
	{
		$notification = Notifications::find($notification_id);
		$notification->estatus = 1;
		$notification->save();

		return $notification;
	}
	
	public function getAllNotificationByAuhtUser()
	{
		$notification = Notifications::where('user_id', Auth::user()->id)->get();
		return $notification;
	}
}