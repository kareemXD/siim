<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Input;

//Models
use App\User;
use App\Models\DirectionMetadata;
use App\Models\Program;

//Lineas de Accion / Indicadores
use App\Models\Direction;
use App\Models\PmActionLine;
use App\Models\MirIndicator;
use App\Models\AgeMeta;
use App\Models\InfIndicator;

//Segimineto
use App\Models\Tracing\MonthlyActivity;
 

class ReportRepository
{
	public function getDataMyPoa($period)
	{
		return Program::where('period', $period)->where('direccion_id', Auth::user()->profile->position->direction->id)->get();
	}
	
	public function getDataPoaByDirection($id, $period)
	{
		return Program::where('period', $period)->where('direccion_id', $id)->get();
	}
	
	public function getDataInafedByDirection($id, $period)
	{
		$items = Program::where('period', $period)->where('direccion_id', $id)->get();
		return $items->filter(function ($program) {
			$flag = 0;
			foreach ($program->actions as $action) {
				if (count($action->inf_action_lines) > 0){
					$flag = 1;
				}
			}
			if($flag == 1){
				return true;
			}else{
				return false;
			}
		});
	}
	
	public function getDataAgenda2030ByDirection($id, $period)
	{
		$items = Program::where('period', $period)->where('direccion_id', $id)->get();
		return $items->filter(function ($program) {
			$flag = 0;
			foreach ($program->actions as $action) {
				if (count($action->sche_metas) > 0){
					$flag = 1;
				}
			}
			if($flag == 1){
				return true;
			}else{
				return false;
			}
		});
	}
	
	public function getDataMirByDirection($id, $period)
	{
		$items = Program::where('period', $period)->where('direccion_id', $id)->get();
		return $items->filter(function ($program) {
			$flag = 0;
			foreach ($program->actions as $action) {
				if (count($action->mir_idicators) > 0){
					$flag = 1;
				}
			}
			if($flag == 1){
				return true;
			}else{
				return false;
			}
		});
	}
	
	public function getDataPoaDirection($direction, $period)
	{
		return Program::where('period', $period)->where('direccion_id', $direction->id)->get();
	}

	public function statusActionLines()
	{
		$data = [
			'0' => [
				'nombre' => "PLAN MUNICIPAL DE DESARROLLO",
				'alias' => "pdm",
				'lines' => PmActionLine::orderBy("punto", "asc")->get()
			],
			'1' => [
				'nombre' => "AGENDA PARA EL DESARROLLO MUNICIPAL",
				'alias' => "adm",
				'lines' => InfIndicator::orderBy("punto", "asc")->get()
			],
			'2' => [
				'nombre' => "AGENDA 2030",
				'alias' => "a2030",
				'lines' => AgeMeta::orderBy("punto", "asc")->get()
			],
			'3' => [
				'nombre' => "PROGRAMAS PRESUPUESTARIO",
				'alias' => "mir",
				'lines' => MirIndicator::all()
			],
		];
		return $data;
	}

	public function getDirectionWithMonthlyActivities()
	{
		$directions = Direction::all();
		foreach ($directions as $key => $direction) {
			if (count($direction->activities) <= 0) 
			{
				$directions->forget($key);
			}
		}
		return $directions;
	}

	public function getDataAnnualReportAdvance()
	{
		$directions = Direction::all();
		$data = [];
		foreach ($directions as $key => $direction) {
			$data_d = [
				'name' => $direction->nombre,
				'meses' => [
					'9' => [
						'name' => "Octubre",
						'col' => "",
						'label' => "",
					],
					'10' => [
						'name' => "Noviembre",
						'col' => "",
						'label' => "",
					],
					'11' => [
						'name' => "Diciembre",
						'col' => "",
						'label' => "",
					],
					'0' => [
						'name' => "Enero",
						'col' => "",
						'label' => "",
					],
					'1' => [
						'name' => "Febrero",
						'col' => "",
						'label' => "",
					],
					'2' => [
						'name' => "Marzo",
						'col' => "",
						'label' => "",
					],
					'3' => [
						'name' => "Abril",
						'col' => "",
						'label' => "",
					],
					'4' => [
						'name' => "Mayo",
						'col' => "",
						'label' => "",
					],
					'5' => [
						'name' => "Junio",
						'col' => "",
						'label' => "",
					],
					'6' => [
						'name' => "Julio",
						'col' => "",
						'label' => "",
					],
					'7' => [
						'name' => "Agosto",
						'col' => "",
						'label' => "",
					],
					'8' => [
						'name' => "Septiembre",
						'col' => "",
						'label' => "",
					],
				],
			];
			for ($i=0; $i < 12; $i++) {
				$mes = $i + 1; 
				$count = $direction->activities()->where("mes", $mes)->count();
				if ($count > 0) {
					$data_d["meses"][$i]["col"] = "bg-green";
					$data_d["meses"][$i]["label"] = "Reportado";
				}else {
					$data_d["meses"][$i]["col"] = "bg-red";
					$data_d["meses"][$i]["label"] = "No Reportado";
				}
			}
			$data[] = $data_d;
		}
		return $data;
	}
}