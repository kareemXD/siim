<?php 
namespace App\Repositories;

use App\Models\Oromapas\Oromapas;
use App\Models\Oromapas\sanitaria;
use App\Models\Oromapas\predios;
use App\Models\Oromapas\poligonos;
use App\Models\Oromapas\Pozos;
use App\Models\Oromapas\Localidades;
use App\Models\Oromapas\padron;
use DB;
use Storage;
use Maatwebsite\Excel\Facades\Excel;



class OromapasRepository
{
    private $saldos = 0;
    private $datos = 0;
    
    public function getPredios()
    {
        return DB::connection("cartoWeb")->table("oromapas_predios_jarretaderas")->get();
    }

    public function getSanitarias()
    {
        return DB::connection("cartoWeb")->table("oromapas_sanitaria")->get();
    }

    public function getHidraulicas()
    {
        return DB::connection("cartoWeb")->table("oromapas_hidraulica")->get();
    }

    public function getPoligonos()
    {
        return DB::connection("cartoWeb")->table("oromapas_poligonos")->get();
    }

    public function getLocalidades()
    {
        return DB::connection("cartoWeb")->table("oromapas_localidades")->get();
    }
    public function getPozos()
    {
        return DB::connection("cartoWeb")->table("oromapas_pozos")->get();
    }

    public function getPredioById($data)
    {
        if(empty($data->shape))
        {
            return predios::find($data->id);
        }
        else 
        {
            return DB::connection('cartoWeb')->table($data->shape)->find($data->id);
        }
    }

    public function getHidraulicaById($id)
    {
        return Oromapas::find($id);
    }

    public function getPozoById($id)
    {
        return Pozos::find($id);
    }

    public function getSanitariaById($id)
    {
        return sanitaria::find($id);
    }

    public function getLocalidad($localidad)
    {
        return DB::connection('cartoWeb')->table($localidad)->get();
    }

    public function editPredio($data)
    {
        DB::connection("cartoWeb")->beginTransaction();
        try {
            if($data->agua == "true")
            {
                $data->agua = "Si";
            }
            else
            {
                $data->agua = "No";    
            }

            if($data->drenaje == "true")
            {
                $data->drenaje = "Si";
            }
            else
            {
                $data->drenaje = "No";    
            }
            DB::connection("cartoWeb")->table($data->localidad2)->where("id", "=", $data->id)->update([
                "contrato" => $data->contrato,
                "calle" => $data->calle,
                "numero" => $data->numero,
                "entreCalle1" => $data->calle_1,
                "entreCalle2" => $data->calle_2,
                "localidad" => $data->localidad,
                "tipoServicio" => $data->tipoServicio,
                "giro" => $data->giro,
                "tieneAgua" => $data->agua,
                "tieneDrenaje" => $data->drenaje,
                "medidorSerie" => $data->medidorSerie
            ]);
            $predio = DB::connection('cartoWeb')->table($data->localidad2)->where("id", '=' , $data->id)->first();
            DB::connection("cartoWeb")->commit();
            return $predio;
        } catch (Exception $e) {
            DB::connection("cartoWeb")->rollback();
            return $e->getMessage();
        }
    }
    
    public function editHidraulica($data)
    {
        DB::connection("cartoWeb")->beginTransaction();
        try {
            $hidraulica = Oromapas::find($data->id);
            $hidraulica->layer = $data->layer;
            $hidraulica->longitud = $data->longitud;
            $hidraulica->pendiente = $data->pendiente;
            $hidraulica->diametro = $data->diametro;
            $hidraulica->save();
            DB::connection("cartoWeb")->commit();
            return $hidraulica;
        } catch (Exception $e) {
            DB::connection("cartoWeb")->rollback();
            return $e->getMessage();
        }
    }

    public function editPozo($data)
    {
        DB::connection("cartoWeb")->beginTransaction();
        try {
            $pozo = Pozos::find($data->id);
            $pozo->layer = $data->layer;
            $pozo->nivel_raz = $data->nivel_raz;
            $pozo->nivel_arras = $data->nivel_arras;
            $pozo->diferencia = $data->diferencia;
            $pozo->save();
            DB::connection("cartoWeb")->commit();
            return $pozo;
        } catch (Exception $e) {
            DB::connection("cartoWeb")->rollback();
            return $e->getMessage();
        }
    }

    public function editSanitaria($data)
    {
        DB::connection("cartoWeb")->beginTransaction();
        try {
            $sanitaria = sanitaria::find($data->id);
            $sanitaria->layer = $data->layer;
            $sanitaria->longitud = $data->longitud;
            $sanitaria->pendiente = $data->pendiente;
            $sanitaria->diametro = $data->diametro;
            $sanitaria->save();
            DB::connection("cartoWeb")->commit();
            return $sanitaria;
        } catch (Exception $e) {
            DB::connection("cartoWeb")->rollback();
            return $e->getMessage();
        }
    }

    public function updateCredit($data)
    {
        $request = $data;
        $filename = $data->file->getClientOriginalName();
        $type = $data->file->getClientOriginalExtension();
        if(storage::disk('public')->putFileAs('saldos', $data->file, $filename))
        {  
            Excel::load(storage_path('app/public/saldos/'.$filename), function($reader) use ($request, $filename) 
            {
                $array = $reader->toArray();
               
                // $location = DB::connection("cartoWeb")->table("oromapas_localidades")->where("nombre", $request->localidad)->get();
                
                // dd($location);
                
                // $table = DB::connection("cartoWeb")->table($location->shape)->where("tieneAgua", "<>" ,"Indefinido")->get();
                // foreach ($table as $res) 
                // {
                //     $info = $this->is_in_array($array, 'contrato', $res->contrato);
                //     if($info)
                //     {  
                //         dd($info);

                //         if(!$info["saldoactual"])
                //         {
                //             $info["saldoactual"] = 0;
                //         }
                //         DB::connection("cartoWeb")->table($location->shape)->where("id", "=", $res->id )->update(["saldo" => $data["saldoactual"]]);
                //         $this->saldos++;
                //     }
                // }
               
                if($request->localidad != null)
                {
                    $predios = DB::connection("cartoWeb")->table($request->localidad)->where("tieneAgua", "<>" ,"Indefinido")->get();
                    foreach ($predios as $predio) {
                        $data = $this->is_in_array($array, 'contrato', $predio->contrato);
                        if($data)
                        {
                            // dd($predio, $data);
                            DB::connection("cartoWeb")->table($request->localidad)->where("id", "=", $predio->id )->update([
                                "calle" => isset($data["calle"]) ? $data["calle"] : 'Indefinido',
                                "numero" => isset($data["numero"]) ? $data["numero"] : 'Indefinido',
                                "entreCalle1" => isset($data["calle1"]) ? $data["calle1"] : 'Indefinido',
                                "entreCalle2" => isset($data["calle2"]) ? $data["calle2"] : 'Indefinido',
                                "colonia" => isset($data["colonia"]) ? $data["colonia"] : 'Indefinido',
                                "localidad" => isset($data["localidad"]) ? $data["localidad"] : 'Indefinido',
                                "tipoServicio" => isset($data["tiposervicio"]) ? $data["tiposervicio"] : 'Indefinido',
                                "giro" => isset($data["giro"]) ? $data["giro"] : 'Indefinido',
                                "tieneAgua" => isset($data["tieneagua"]) ? $data["tieneagua"] : 'Indefinido',
                                "tieneDrenaje" => isset($data["tienedrenaje"]) ? $data["tienedrenaje"] : 'Indefinido',
                                "medidorSerie" => isset($data["medidorserie"]) ? $data["medidorserie"] : 'Indefinido',
                                "saldo" => isset($data["saldoactual"]) ? $data["saldoactual"] : '0',
                            ]);
                            $this->datos++;
                        }
                    }
                }
                unlink(storage_path('app/public/saldos/'.$filename));
            });
            $return = [
                "saldos" => $this->saldos,
                "datos" => $this->datos
            ];
            return $return;
        }
    }

    function is_in_array($array, $key, $key_value)
    {
        $within_array = false;
        $data = false;
        foreach( $array as $k=>$v )
        {
            if(is_array($v))
            {
                $within_array = $this->is_in_array_2($v, $key, $key_value);
                if( $within_array == true )
                {
                    $data = $v;
                    break;
                }
            }
        }
        return $data;
    }
    
    function is_in_array_2($array, $key, $key_value)
    {
        $within_array = false;
        foreach( $array as $k=>$v )     
        {
            
            if( $v == $key_value && $k == $key )
            {
                $within_array = true;
                break;
            }
        }
        return $within_array;
    }

    public function cutPolygon($data)
    {
        $param = "";
        $sql = "SELECT ST_AsText((ST_Dump(ST_Split(polygon, line))).geom) As wkt
        FROM (";
        $sql .= "SELECT
            ST_GeomFromText('LINESTRING(";
        foreach ($data->parametros as $value) {
            $param .= $value["x"]. " "; 
            $param .= $value["y"].","; 
        }
        $param = substr($param, 0, -1);
        $sql.= $param.")',32613) As line,
        ST_GeomFromText((SELECT ST_AsText(geodata) FROM ".$data->shape." WHERE id = ".$data->id." ),32613) As polygon) As foo;";
        $result = DB::connection('cartoWeb')->select($sql);
        $origin = DB::connection('cartoWeb')->table($data->shape)->where('id', $data->id)->first();
        foreach ($result as $value)
        {
            $array[] = [
                'contrato' => $origin->contrato,
                'calle' => $origin->calle,
                'numero' => $origin->numero,
                'entreCalle1' => $origin->entreCalle1,
                'entreCalle2' => $origin->entreCalle2,
                'colonia' => $origin->colonia,
                'localidad' => $origin->localidad,
                'tipoServicio' => $origin->tipoServicio,
                'giro' => $origin->giro,
                'tieneAgua' => $origin->tieneAgua,
                'tieneDrenaje' => $origin->tieneDrenaje,
                'medidorSerie' => $origin->medidorSerie,
                'saldo' => $origin->saldo,
                'geodata' => $value->wkt,
            ];
        }
        DB::connection('cartoWeb')->table($data->shape)->where('id', $data->id)->delete();
        foreach ($array as $key => $value) 
        {
            DB::connection('cartoWeb')->table($data->shape)->insert($value);
        }
    }

    public function deletePolygon($data)
    {
        DB::connection('cartoWeb')->table($data->shape)->where('id', $data->id)->delete();
    }

    public function createPolygon($data)
    {
        if($data->agua == "true")
        {
            $data->agua = "Si";
        }
        else
        {
            $data->agua = "No";    
        }

        if($data->drenaje == "true")
        {
            $data->drenaje = "Si";
        }
        else
        {
            $data->drenaje = "No";    
        }
        $param = "";
        foreach (json_decode($data->parametros, true) as $value) {
            $param .= $value["x"]. " "; 
            $param .= $value["y"].", "; 
        }
        $param = substr($param, 0, -2);
        DB::connection('cartoWeb')->table($data->shape)->insert([
            "contrato" => $data->contrato,
            "calle" => $data->calle,
            "numero" => $data->numero,
            "entreCalle1" => $data->calle_1,
            "entreCalle2" => $data->calle_2,
            "localidad" => $data->localidad,
            "tipoServicio" => $data->tipoServicio,
            "giro" => $data->giro,
            "tieneAgua" => $data->agua,
            "tieneDrenaje" => $data->drenaje,
            "medidorSerie" => $data->medidorSerie,
            'geodata' =>  DB::raw("ST_GeomFromText('POLYGON((".$param."))',32613)"),
        ]);
    }
}