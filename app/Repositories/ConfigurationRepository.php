<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Input;

//Models
use App\User;
use App\Models\DirectionMetadata;
use App\Models\Direction;
use App\Models\Position;
 

class ConfigurationRepository
{
	public function getMetadataByDirectionId($id)
	{
		return DirectionMetadata::where("direccion_id", $id)->get();
	}

	public function validateKey($direction, $key)
	{
		$key = DirectionMetadata::where("direccion_id", $direction)->where("meta_llave", $key)->first();
		if(!empty($key))
		{
			return $key;
		}else {
			return false;
		}
	}

	public function updateMetadataDirection($data)
	{
		foreach($data->except('_token') as $key => $part)
		{
			if($metadata = $this->validateKey(Auth::user()->profile->position->direction->id, $key))
			{
				$metadata->valor = $part;
				$metadata->save();
			}else {
				$metadata = new DirectionMetadata;
				$metadata->direccion_id = Auth::user()->profile->position->direction->id;
				$metadata->meta_llave = $key;
				$metadata->valor = $part;
				$metadata->save();
			}
		
		}
	}

	public function getPositionsWithUserByAuthDirection()
	{
		$direction = Auth::user()->profile->position->direccion_id;

		$positions = Position::where('direccion_id', $direction)->get();
		foreach ($positions as $key => $position) {
			if (count($position->profile) == 0 ) 
			{
				unset($positions[$key]);
			}
		}
		return $positions;
	}
}