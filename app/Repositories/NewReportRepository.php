<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;
use Illuminate\Support\Facades\Input;

//Models
use App\User;
use App\Models\NewPlanning\MirComponent;
use App\Models\NewPlanning\MirAreas;
 

class NewReportRepository
{
	public function getDataMyMir($period)
	{
		return MirAreas::where('periodo', $period)->where('direccion_id', Auth::user()->profile->position->direction->id)->get();
	}

	public function getDataMirDirection($direction, $period)
	{
		return MirAreas::where('periodo', $period)->where('direccion_id', $direction->id)->get();
	}
}