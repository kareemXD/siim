<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;

use App\Models\PmAxe;
use App\Models\InfAxe;
use App\Models\AgeObjective;
use App\Models\PmActionLine;
use App\Models\InfIndicator;
use App\Models\AgeMeta;
use App\Models\Direction;

use App\Models\Program;
use App\Models\ActionPoa;

class SecretRepository
{
    public function cleanPmdActions()
    {
        $directions = Direction::all();
        $data = [
            "pmd" => 0,
            'adm' => 0,
            "a2030" => 0,
            "mir" => 0,
        ];
        foreach ($directions as $key => $direction) 
        {
            foreach ($direction->programs as $p_key => $program) 
            {
                foreach ($program->actions as $a_key => $action) 
                {
                    foreach ($action->pm_action_lines as $l_key => $line) 
                    {
                        $counter = $direction->actionLines()->where("id", $line->id)->count();
                        if($counter <= 0)
                        {
                            $action->pm_action_lines()->detach($line->id);
                            $data["pmd"]++;
                        }
                    }
                    foreach ($action->inf_action_lines as $l_key => $line) 
                    {
                        $counter = $direction->infIndicators()->where("id", $line->id)->count();
                        if($counter <= 0)
                        {
                            $action->inf_action_lines()->detach($line->id);
                            $data["adm"]++;
                        }
                    }
                    foreach ($action->sche_metas as $l_key => $line) 
                    {
                        $counter = $direction->scheMeta()->where("id", $line->id)->count();
                        if($counter <= 0)
                        {
                            $action->sche_metas()->detach($line->id);
                            $data["a2030"]++;
                        }
                    }
                    foreach ($action->mir_idicators as $l_key => $line) 
                    {
                        $counter = $direction->mirIndicators()->where("id", $line->id)->count();
                        if($counter <= 0)
                        {
                            $action->mir_idicators()->detach($line->id);
                            $data["mir"]++;
                        }
                    }
                }
            }
        }
        foreach ($data as $key => $value) {
            $data[$key] = "---> ".$value." elementos Corregidos <---";
        }
        return $data;
    }

    public function makeNewPeriod()
    {
        $period = 2020;
        $directions = Direction::all();
        foreach ($directions as $key => $direction) 
        {
            foreach ($direction->actionLines as $line) 
            {
                $direction->actionLines()->attach($line->id, ["status" => $line->pivot->status, "period" => $period]);
            }
            foreach ($direction->infIndicators as $line) 
            {

                $direction->infIndicators()->attach($line->id, ["status" => $line->pivot->status, "period" => $period]);
            }
            foreach ($direction->scheMeta as $line) 
            {

                $direction->scheMeta()->attach($line->id, ["estatus" => $line->pivot->estatus, "period" => $period]);
            }
            foreach ($direction->mirIndicators as $line) 
            {

                $direction->mirIndicators()->attach($line->id, ["status" => $line->pivot->status, "period" => $period]);
            }
            
            foreach ($direction->programs as $p_key => $program) 
            {
                $nProgram = New Program;
                
                $nProgram->nombre = $program->nombre;
                $nProgram->tema = $program->tema;
                $nProgram->descripcion = $program->descripcion;
                $nProgram->direccion_id = $program->direccion_id;
                $nProgram->period = $period;
                $nProgram->save();

                foreach ($program->actions as $a_key => $action) 
                {
                    $nAction = New ActionPoa;

                    $nAction->actividad = $action->actividad;
                    $nAction->unidad_medida = $action->unidad_medida;
                    $nAction->meta_anual = $action->meta_anual;
                    $nAction->meta_anual_r = 0;
                    $nAction->trimestre_1 = $action->trimestre_1;
                    $nAction->trimestre_2 = $action->trimestre_2;
                    $nAction->trimestre_3 = $action->trimestre_3;
                    $nAction->trimestre_4 = $action->trimestre_4;
                    $nAction->programas_id = $nProgram->id;
                    $nAction->puesto_id = $action->puesto_id;
                    $nAction->enero = 0;
                    $nAction->febrero = 0;
                    $nAction->marzo = 0;
                    $nAction->trimestre_1_r = 0;
                    $nAction->abril = 0;
                    $nAction->mayo = 0;
                    $nAction->junio = 0;
                    $nAction->trimestre_2_r = 0;
                    $nAction->julio = 0;
                    $nAction->agosto = 0;
                    $nAction->septiembre = 0;
                    $nAction->trimestre_3_p = 0;
                    $nAction->octubre = 0;
                    $nAction->noviembre = 0;
                    $nAction->diciembre = 0;
                    $nAction->trimestre_4_r = 0;
                    $nAction->valor = $action->valor;
                    $nAction->tipo = $action->tipo;
                    $nAction->orientacion = $action->orientacion;
                    $nAction->padre = $action->padre;
                    $nAction->save();

                    foreach ($action->pm_action_lines as $line) 
                    {
                        $nAction->pm_action_lines()->attach($line->id);
                    }
                    foreach ($action->inf_action_lines as $line) 
                    {
                        $nAction->inf_action_lines()->attach($line->id);
                    }
                    foreach ($action->sche_metas as $line) 
                    {
                        $nAction->sche_metas()->attach($line->id);
                    }
                    foreach ($action->mir_idicators as $line) 
                    {
                        $nAction->mir_idicators()->attach($line->id);
                    }
                }
            }
        }
        return true;
        
    }

    public function cleanMirs($period)
    {
        $directions = Direction::all();
        try {
            foreach ($directions as $key => $direction)  {
                foreach ($direction->programs()->where('period', $period)->get() as $p_key => $program) {
                    foreach ($program->actions as $a_key => $action) {
                        $action->mir_idicators()->detach();
                    }
                }
                $direction->mirIndicators()->wherePivot("period", $period)->detach();
            }
        } catch (\Throwable $th) {
            return false;
        }
        return true;
    }
}