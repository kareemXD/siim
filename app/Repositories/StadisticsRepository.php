<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;

use App\Models\PmAxe;
use App\Models\InfAxe;
use App\Models\AgeObjective;
use App\Models\PmActionLine;
use App\Models\InfIndicator;
use App\Models\AgeMeta;
use App\Models\Direction;
class StadisticsRepository
{
    public function getStadistics($directions = null, $axes = null, $trimester = null)
    {
        if(empty($directions))
        {
            // $directions = Direction::all;
            $directions = Direction::whereNull("padre")->get();
        }else {
            $directions = Direction::whereIn('id', $directions)->get();
        }
        $total = [
            'name' => "Total",
            'axes' => [],
        ];
        $array_axes = [];
        $data = [
            'axes' => [],
        ];
        if (empty($axes)) 
        {
            $data['axes'] = ["Eje 1", "Eje 2", "Eje 3", "Eje 4", "Eje 5", "Total"];
            for ($i=0; $i < 5; $i++) {
                $axe = $i + 1;
                $arr_axes = [
                    'axe-'.$axe => [
                        'label' => "Eje ".$axe,
                        'finish' => 0,
                        'process' => 0,
                        'null' => 0,
                        'empty' => 0,
                        'total' => 0,
                    ]
                ];
                $array_axes += $arr_axes;
            }
        }else {
            
            foreach ($axes as $key => $axe) {
                $data["axes"][] = "Eje-".$axe;
                $arr_axes = [
                    'axe-'.$axe => [
                        'label' => "Eje ".$axe,
                        'finish' => 0,
                        'process' => 0,
                        'null' => 0,
                        'empty' => 0,
                        'total' => 0,
                    ],
                ];
                $array_axes += $arr_axes;
            }
            $data["axes"][] = "Total";
        }
        $total_arr = [
            'total' => [
                'label' => "Total",
                'finish' => 0,
                'process' => 0,
                'null' => 0,
                'empty' => 0,
                'total' => 0,
            ]
        ];
        $array_axes += $total_arr;
        $total['axes'] = $array_axes;
        // dd($directions);
        foreach ($directions as $key => $direction) {
            $data_direction = [
                'id' => $direction->id,
                'name' => $direction->nombre,
                'axes' => [],
            ];
            $data_direction['axes'] = $array_axes;
            
            $programs = $direction->programs()->where("period", config('implan.periods.previous'))->get();
            foreach ($programs as $p_key => $program) 
            {
                $actions = $program->actions;

                foreach ($actions as $a_key => $action) {
                    $lines = $action->pm_action_lines;
                    $total_actions = count($lines);
                    if($total_actions > 0)
                    {
                        $fraction = 1 / $total_actions;
                        // dd($total_actions, $action);
                        switch ($trimester) {
                            case '1':
                                $goal = $action->trimestre_1;
                                $realize = $action->trimestre_1_r;
                                break;
                            case '2':
                                $goal = $action->trimestre_2;
                                $realize = $action->trimestre_2_r;
                                break;
                            case '3':
                                $goal = $action->trimestre_3;
                                $realize = $action->trimestre_3_p;
                                break;
                            case '4':
                                $goal = $action->trimestre_4;
                                $realize = $action->trimestre_4_r;
                                break;
                            default:
                                $goal = $action->meta_anual;
                                $realize = $action->meta_anual_r;
                                break;
                        }
                        foreach ($lines as $l_key => $line) {
                            switch ($line->strategy->objective->axe->punto) {
                                case '1':
                                    if(!empty($axes))
                                    {
                                        if(in_array("1", $axes))
                                        {
                                            $data_direction["axes"]["axe-1"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-1"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-1"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-1"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-1"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-1"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-1"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-1"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-1"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-1"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                            $data_direction["axes"]["axe-1"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-1"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-1"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-1"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-1"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-1"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-1"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-1"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-1"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-1"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                    }
                                    break;
                                case '2':
                                    if(!empty($axes))
                                    {
                                        if(in_array("2", $axes))
                                        {
                                            $data_direction["axes"]["axe-2"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-2"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-2"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-2"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-2"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-2"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-2"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-2"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-2"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-2"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-2"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-2"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-2"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-2"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-2"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-2"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-2"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-2"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-2"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-2"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case '3':
                                    if(!empty($axes))
                                    {
                                        if(in_array("3", $axes))
                                        {
                                            $data_direction["axes"]["axe-3"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-3"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-3"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-3"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-3"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-3"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-3"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-3"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-3"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-3"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-3"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-3"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-3"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-3"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-3"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-3"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-3"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-3"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-3"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-3"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case '4':
                                    if(!empty($axes))
                                    {
                                        if(in_array("4", $axes))
                                        {
                                            $data_direction["axes"]["axe-4"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-4"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-4"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-4"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-4"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-4"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-4"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-4"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-4"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-4"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-4"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-4"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-4"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-4"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-4"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-4"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-4"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-4"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-4"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-4"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case '5':
                                    if(!empty($axes))
                                    {
                                        if(in_array("5", $axes))
                                        {
                                            $data_direction["axes"]["axe-5"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-5"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-5"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-5"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-5"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-5"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-5"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-5"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-5"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-5"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-5"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-5"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-5"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-5"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-5"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-5"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-5"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-5"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-5"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-5"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            $data[] = $data_direction;
        }
        $data[] = $total;
        return $data;
    }

    public function getStadisticsPMD2($directions = null, $axes = null, $trimester = null)
    {
        if(empty($directions))
        {
            // $directions = Direction::all;
            $directions = Direction::whereNull("padre")->get();
        }else {
            $directions = Direction::whereIn('id', $directions)->get();
        }
        $total = [
            'name' => "Total",
            'axes' => [],
        ];
        $array_axes = [];
        $data = [
            'axes' => [],
        ];
        if (empty($axes)) 
        {
            $data['axes'] = ["Eje 1", "Eje 2", "Eje 3", "Eje 4", "Eje 5", "Total"];
            for ($i=0; $i < 5; $i++) {
                $axe = $i + 1;
                $arr_axes = [
                    'axe-'.$axe => [
                        'label' => "Eje ".$axe,
                        'finish' => 0,
                        'process' => 0,
                        'null' => 0,
                        'empty' => 0,
                        'total' => 0,
                    ]
                ];
                $array_axes += $arr_axes;
            }
        }else {
            
            foreach ($axes as $key => $axe) {
                $data["axes"][] = "Eje-".$axe;
                $arr_axes = [
                    'axe-'.$axe => [
                        'label' => "Eje ".$axe,
                        'finish' => 0,
                        'process' => 0,
                        'null' => 0,
                        'empty' => 0,
                        'total' => 0,
                    ],
                ];
                $array_axes += $arr_axes;
            }
            $data["axes"][] = "Total";
        }
        $total_arr = [
            'total' => [
                'label' => "Total",
                'finish' => 0,
                'process' => 0,
                'null' => 0,
                'empty' => 0,
                'total' => 0,
            ]
        ];
        $array_axes += $total_arr;
        $total['axes'] = $array_axes;
        // dd($directions);
        foreach ($directions as $key => $direction) {
            $data_direction = [
                'id' => $direction->id,
                'name' => $direction->nombre,
                'axes' => [],
            ];
            $data_direction['axes'] = $array_axes;
            
            $programs = $direction->programs()->where("period", config('implan.periods.previous'))->get();
            foreach ($programs as $p_key => $program) 
            {
                $actions = $program->actions;

                foreach ($actions as $a_key => $action) {
                    $lines = $action->pm_action_lines;
                    
                    switch ($trimester) {
                        case '1':
                            $goal = $action->trimestre_1;
                            $realize = $action->trimestre_1_r;
                            break;
                        case '2':
                            $goal = $action->trimestre_2;
                            $realize = $action->trimestre_2_r;
                            break;
                        case '3':
                            $goal = $action->trimestre_3;
                            $realize = $action->trimestre_3_p;
                            break;
                        case '4':
                            $goal = $action->trimestre_4;
                            $realize = $action->trimestre_4_r;
                            break;
                        default:
                            $goal = $action->meta_anual;
                            $realize = $action->meta_anual_r;
                            break;
                    }
                    foreach ($lines as $l_key => $line) {
                        switch ($line->strategy->objective->axe->punto) {
                            case '1':
                                if(!empty($axes))
                                {
                                    if(in_array("1", $axes))
                                    {
                                        $data_direction["axes"]["axe-1"]["total"] += 1;
                                        $data_direction["axes"]["total"]["total"] += 1;
                                        $total["axes"]["axe-1"]["total"] += 1;
                                        $total["axes"]["total"]["total"] += 1;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-1"]["process"] += 1;
                                                    $data_direction["axes"]["total"]["process"] += 1;
                                                    $total["axes"]["axe-1"]["process"] += 1;
                                                    $total["axes"]["total"]["process"] += 1;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-1"]["finish"] += 1;
                                                    $data_direction["axes"]["total"]["finish"] += 1;
                                                    $total["axes"]["axe-1"]["finish"] += 1;
                                                    $total["axes"]["total"]["finish"] += 1;
                                                }else {
                                                    $data_direction["axes"]["axe-1"]["null"] += 1;
                                                    $data_direction["axes"]["total"]["null"] += 1;
                                                    $total["axes"]["axe-1"]["null"] += 1;
                                                    $total["axes"]["total"]["null"] += 1;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-1"]["empty"] += 1;
                                            $data_direction["axes"]["total"]["empty"] += 1;
                                            $total["axes"]["axe-1"]["empty"] += 1;
                                            $total["axes"]["total"]["empty"] += 1;
                                        }
                                    }
                                }else {
                                        $data_direction["axes"]["axe-1"]["total"] += 1;
                                        $data_direction["axes"]["total"]["total"] += 1;
                                        $total["axes"]["axe-1"]["total"] += 1;
                                        $total["axes"]["total"]["total"] += 1;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-1"]["process"] += 1;
                                                    $data_direction["axes"]["total"]["process"] += 1;
                                                    $total["axes"]["axe-1"]["process"] += 1;
                                                    $total["axes"]["total"]["process"] += 1;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-1"]["finish"] += 1;
                                                    $data_direction["axes"]["total"]["finish"] += 1;
                                                    $total["axes"]["axe-1"]["finish"] += 1;
                                                    $total["axes"]["total"]["finish"] += 1;
                                                }else {
                                                    $data_direction["axes"]["axe-1"]["null"] += 1;
                                                    $data_direction["axes"]["total"]["null"] += 1;
                                                    $total["axes"]["axe-1"]["null"] += 1;
                                                    $total["axes"]["total"]["null"] += 1;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-1"]["empty"] += 1;
                                            $data_direction["axes"]["total"]["empty"] += 1;
                                            $total["axes"]["axe-1"]["empty"] += 1;
                                            $total["axes"]["total"]["empty"] += 1;
                                        }
                                }
                                break;
                            case '2':
                                if(!empty($axes))
                                {
                                    if(in_array("2", $axes))
                                    {
                                        $data_direction["axes"]["axe-2"]["total"] += 1;
                                        $data_direction["axes"]["total"]["total"] += 1;
                                        $total["axes"]["axe-2"]["total"] += 1;
                                        $total["axes"]["total"]["total"] += 1;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-2"]["process"] += 1;
                                                    $data_direction["axes"]["total"]["process"] += 1;
                                                    $total["axes"]["axe-2"]["process"] += 1;
                                                    $total["axes"]["total"]["process"] += 1;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-2"]["finish"] += 1;
                                                    $data_direction["axes"]["total"]["finish"] += 1;
                                                    $total["axes"]["axe-2"]["finish"] += 1;
                                                    $total["axes"]["total"]["finish"] += 1;
                                                }else {
                                                    $data_direction["axes"]["axe-2"]["null"] += 1;
                                                    $data_direction["axes"]["total"]["null"] += 1;
                                                    $total["axes"]["axe-2"]["null"] += 1;
                                                    $total["axes"]["total"]["null"] += 1;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-2"]["empty"] += 1;
                                            $data_direction["axes"]["total"]["empty"] += 1;
                                            $total["axes"]["axe-2"]["empty"] += 1;
                                            $total["axes"]["total"]["empty"] += 1;
                                        }
                                    }
                                }else {
                                    $data_direction["axes"]["axe-2"]["total"] += 1;
                                    $data_direction["axes"]["total"]["total"] += 1;
                                    $total["axes"]["axe-2"]["total"] += 1;
                                    $total["axes"]["total"]["total"] += 1;
                                    if($goal > 0)
                                    {
                                        $advance = ($realize * 100 ) / $goal;
                                        if($action->orientacion == 0)
                                        {
                                            if ($advance > 0 && $advance < 100) {
                                                $data_direction["axes"]["axe-2"]["process"] += 1;
                                                $data_direction["axes"]["total"]["process"] += 1;
                                                $total["axes"]["axe-2"]["process"] += 1;
                                                $total["axes"]["total"]["process"] += 1;
                                            } else if ($advance >= 100 ) {
                                                $data_direction["axes"]["axe-2"]["finish"] += 1;
                                                $data_direction["axes"]["total"]["finish"] += 1;
                                                $total["axes"]["axe-2"]["finish"] += 1;
                                                $total["axes"]["total"]["finish"] += 1;
                                            }else {
                                                $data_direction["axes"]["axe-2"]["null"] += 1;
                                                $data_direction["axes"]["total"]["null"] += 1;
                                                $total["axes"]["axe-2"]["null"] += 1;
                                                $total["axes"]["total"]["null"] += 1;
                                            }
                                        }
                                        else if($action->orientacion == 1)
                                        {
                                            dd($action);
                                        }
                                    }
                                    else {
                                        $data_direction["axes"]["axe-2"]["empty"] += 1;
                                        $data_direction["axes"]["total"]["empty"] += 1;
                                        $total["axes"]["axe-2"]["empty"] += 1;
                                        $total["axes"]["total"]["empty"] += 1;
                                    }
                                }
                                break;
                            case '3':
                                if(!empty($axes))
                                {
                                    if(in_array("3", $axes))
                                    {
                                        $data_direction["axes"]["axe-3"]["total"] += 1;
                                        $data_direction["axes"]["total"]["total"] += 1;
                                        $total["axes"]["axe-3"]["total"] += 1;
                                        $total["axes"]["total"]["total"] += 1;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-3"]["process"] += 1;
                                                    $data_direction["axes"]["total"]["process"] += 1;
                                                    $total["axes"]["axe-3"]["process"] += 1;
                                                    $total["axes"]["total"]["process"] += 1;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-3"]["finish"] += 1;
                                                    $data_direction["axes"]["total"]["finish"] += 1;
                                                    $total["axes"]["axe-3"]["finish"] += 1;
                                                    $total["axes"]["total"]["finish"] += 1;
                                                }else {
                                                    $data_direction["axes"]["axe-3"]["null"] += 1;
                                                    $data_direction["axes"]["total"]["null"] += 1;
                                                    $total["axes"]["axe-3"]["null"] += 1;
                                                    $total["axes"]["total"]["null"] += 1;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-3"]["empty"] += 1;
                                            $data_direction["axes"]["total"]["empty"] += 1;
                                            $total["axes"]["axe-3"]["empty"] += 1;
                                            $total["axes"]["total"]["empty"] += 1;
                                        }
                                    }
                                }else {
                                    $data_direction["axes"]["axe-3"]["total"] += 1;
                                    $data_direction["axes"]["total"]["total"] += 1;
                                    $total["axes"]["axe-3"]["total"] += 1;
                                    $total["axes"]["total"]["total"] += 1;
                                    if($goal > 0)
                                    {
                                        $advance = ($realize * 100 ) / $goal;
                                        if($action->orientacion == 0)
                                        {
                                            if ($advance > 0 && $advance < 100) {
                                                $data_direction["axes"]["axe-3"]["process"] += 1;
                                                $data_direction["axes"]["total"]["process"] += 1;
                                                $total["axes"]["axe-3"]["process"] += 1;
                                                $total["axes"]["total"]["process"] += 1;
                                            } else if ($advance >= 100 ) {
                                                $data_direction["axes"]["axe-3"]["finish"] += 1;
                                                $data_direction["axes"]["total"]["finish"] += 1;
                                                $total["axes"]["axe-3"]["finish"] += 1;
                                                $total["axes"]["total"]["finish"] += 1;
                                            }else {
                                                $data_direction["axes"]["axe-3"]["null"] += 1;
                                                $data_direction["axes"]["total"]["null"] += 1;
                                                $total["axes"]["axe-3"]["null"] += 1;
                                                $total["axes"]["total"]["null"] += 1;
                                            }
                                        }
                                        else if($action->orientacion == 1)
                                        {
                                            dd($action);
                                        }
                                    }
                                    else {
                                        $data_direction["axes"]["axe-3"]["empty"] += 1;
                                        $data_direction["axes"]["total"]["empty"] += 1;
                                        $total["axes"]["axe-3"]["empty"] += 1;
                                        $total["axes"]["total"]["empty"] += 1;
                                    }
                                }
                                break;
                            case '4':
                                if(!empty($axes))
                                {
                                    if(in_array("4", $axes))
                                    {
                                        $data_direction["axes"]["axe-4"]["total"] += 1;
                                        $data_direction["axes"]["total"]["total"] += 1;
                                        $total["axes"]["axe-4"]["total"] += 1;
                                        $total["axes"]["total"]["total"] += 1;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-4"]["process"] += 1;
                                                    $data_direction["axes"]["total"]["process"] += 1;
                                                    $total["axes"]["axe-4"]["process"] += 1;
                                                    $total["axes"]["total"]["process"] += 1;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-4"]["finish"] += 1;
                                                    $data_direction["axes"]["total"]["finish"] += 1;
                                                    $total["axes"]["axe-4"]["finish"] += 1;
                                                    $total["axes"]["total"]["finish"] += 1;
                                                }else {
                                                    $data_direction["axes"]["axe-4"]["null"] += 1;
                                                    $data_direction["axes"]["total"]["null"] += 1;
                                                    $total["axes"]["axe-4"]["null"] += 1;
                                                    $total["axes"]["total"]["null"] += 1;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-4"]["empty"] += 1;
                                            $data_direction["axes"]["total"]["empty"] += 1;
                                            $total["axes"]["axe-4"]["empty"] += 1;
                                            $total["axes"]["total"]["empty"] += 1;
                                        }
                                    }
                                }else {
                                    $data_direction["axes"]["axe-4"]["total"] += 1;
                                    $data_direction["axes"]["total"]["total"] += 1;
                                    $total["axes"]["axe-4"]["total"] += 1;
                                    $total["axes"]["total"]["total"] += 1;
                                    if($goal > 0)
                                    {
                                        $advance = ($realize * 100 ) / $goal;
                                        if($action->orientacion == 0)
                                        {
                                            if ($advance > 0 && $advance < 100) {
                                                $data_direction["axes"]["axe-4"]["process"] += 1;
                                                $data_direction["axes"]["total"]["process"] += 1;
                                                $total["axes"]["axe-4"]["process"] += 1;
                                                $total["axes"]["total"]["process"] += 1;
                                            } else if ($advance >= 100 ) {
                                                $data_direction["axes"]["axe-4"]["finish"] += 1;
                                                $data_direction["axes"]["total"]["finish"] += 1;
                                                $total["axes"]["axe-4"]["finish"] += 1;
                                                $total["axes"]["total"]["finish"] += 1;
                                            }else {
                                                $data_direction["axes"]["axe-4"]["null"] += 1;
                                                $data_direction["axes"]["total"]["null"] += 1;
                                                $total["axes"]["axe-4"]["null"] += 1;
                                                $total["axes"]["total"]["null"] += 1;
                                            }
                                        }
                                        else if($action->orientacion == 1)
                                        {
                                            dd($action);
                                        }
                                    }
                                    else {
                                        $data_direction["axes"]["axe-4"]["empty"] += 1;
                                        $data_direction["axes"]["total"]["empty"] += 1;
                                        $total["axes"]["axe-4"]["empty"] += 1;
                                        $total["axes"]["total"]["empty"] += 1;
                                    }
                                }
                                break;
                            case '5':
                                if(!empty($axes))
                                {
                                    if(in_array("5", $axes))
                                    {
                                        $data_direction["axes"]["axe-5"]["total"] += 1;
                                        $data_direction["axes"]["total"]["total"] += 1;
                                        $total["axes"]["axe-5"]["total"] += 1;
                                        $total["axes"]["total"]["total"] += 1;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-5"]["process"] += 1;
                                                    $data_direction["axes"]["total"]["process"] += 1;
                                                    $total["axes"]["axe-5"]["process"] += 1;
                                                    $total["axes"]["total"]["process"] += 1;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-5"]["finish"] += 1;
                                                    $data_direction["axes"]["total"]["finish"] += 1;
                                                    $total["axes"]["axe-5"]["finish"] += 1;
                                                    $total["axes"]["total"]["finish"] += 1;
                                                }else {
                                                    $data_direction["axes"]["axe-5"]["null"] += 1;
                                                    $data_direction["axes"]["total"]["null"] += 1;
                                                    $total["axes"]["axe-5"]["null"] += 1;
                                                    $total["axes"]["total"]["null"] += 1;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-5"]["empty"] += 1;
                                            $data_direction["axes"]["total"]["empty"] += 1;
                                            $total["axes"]["axe-5"]["empty"] += 1;
                                            $total["axes"]["total"]["empty"] += 1;
                                        }
                                    }
                                }else {
                                    $data_direction["axes"]["axe-5"]["total"] += 1;
                                    $data_direction["axes"]["total"]["total"] += 1;
                                    $total["axes"]["axe-5"]["total"] += 1;
                                    $total["axes"]["total"]["total"] += 1;
                                    if($goal > 0)
                                    {
                                        $advance = ($realize * 100 ) / $goal;
                                        if($action->orientacion == 0)
                                        {
                                            if ($advance > 0 && $advance < 100) {
                                                $data_direction["axes"]["axe-5"]["process"] += 1;
                                                $data_direction["axes"]["total"]["process"] += 1;
                                                $total["axes"]["axe-5"]["process"] += 1;
                                                $total["axes"]["total"]["process"] += 1;
                                            } else if ($advance >= 100 ) {
                                                $data_direction["axes"]["axe-5"]["finish"] += 1;
                                                $data_direction["axes"]["total"]["finish"] += 1;
                                                $total["axes"]["axe-5"]["finish"] += 1;
                                                $total["axes"]["total"]["finish"] += 1;
                                            }else {
                                                $data_direction["axes"]["axe-5"]["null"] += 1;
                                                $data_direction["axes"]["total"]["null"] += 1;
                                                $total["axes"]["axe-5"]["null"] += 1;
                                                $total["axes"]["total"]["null"] += 1;
                                            }
                                        }
                                        else if($action->orientacion == 1)
                                        {
                                            dd($action);
                                        }
                                    }
                                    else {
                                        $data_direction["axes"]["axe-5"]["empty"] += 1;
                                        $data_direction["axes"]["total"]["empty"] += 1;
                                        $total["axes"]["axe-5"]["empty"] += 1;
                                        $total["axes"]["total"]["empty"] += 1;
                                    }
                                }
                                break;
                        }
                    }
                }
            }
            $data[] = $data_direction;
        }
        $data[] = $total;
        return $data;
    }
    
    public function getStadisticsADM($directions = null, $axes = null, $trimester = null)
    {
        // dd($directions, $axes, $trimester);
        if(empty($directions))
        {
            // $directions = Direction::all;
            $directions = Direction::whereNull("padre")->get();
        }else {
            $directions = Direction::whereIn('id', $directions)->get();
        }
        $total = [
            'name' => "Total",
            'axes' => [],
        ];
        $array_axes = [];
        $data = [
            'axes' => [],
        ];
        if (empty($axes)) 
        {
            $data['axes'] = ["A.1", "A.2", "A.3", "A.4", "B.1", "B.2", "B.3", "C.1", "Total"];
            for ($i=0; $i < 8; $i++) {
                $axe = $i + 1;
                $arr_axes = [
                    'axe-'.$data['axes'][$i] => [
                        'label' => $data['axes'][$i],
                        'finish' => 0,
                        'process' => 0,
                        'null' => 0,
                        'empty' => 0,
                        'total' => 0,
                    ]
                ];
                $array_axes += $arr_axes;
            }
        }else {
            
            foreach ($axes as $key => $axe) {
                $data["axes"][] = $axe;
                $arr_axes = [
                    'axe-'.$axe => [
                        'label' => $axe,
                        'finish' => 0,
                        'process' => 0,
                        'null' => 0,
                        'empty' => 0,
                        'total' => 0,
                    ],
                ];
                $array_axes += $arr_axes;
            }
            $data["axes"][] = "Total";
        }
        $total_arr = [
            'total' => [
                'label' => "Total",
                'finish' => 0,
                'process' => 0,
                'null' => 0,
                'empty' => 0,
                'total' => 0,
            ]
        ];
        $array_axes += $total_arr;
        $total['axes'] = $array_axes;
        foreach ($directions as $key => $direction) {
            $data_direction = [
                'id' => $direction->id,
                'name' => $direction->nombre,
                'axes' => [],
            ];
            $data_direction['axes'] = $array_axes;
            // dd($data, $total,$data_direction);
            
            $programs = $direction->programs()->where("period", config('implan.periods.previous'))->get();
            foreach ($programs as $p_key => $program) 
            {
                $actions = $program->actions;

                foreach ($actions as $a_key => $action) {
                    $lines = $action->inf_action_lines;
                    $total_actions = count($lines);
                    if($total_actions > 0)
                    {
                        $fraction = 1 / $total_actions;
                        switch ($trimester) {
                            case '1':
                                $goal = $action->trimestre_1;
                                $realize = $action->trimestre_1_r;
                                break;
                            case '2':
                                $goal = $action->trimestre_2;
                                $realize = $action->trimestre_2_r;
                                break;
                            case '3':
                                $goal = $action->trimestre_3;
                                $realize = $action->trimestre_3_p;
                                break;
                            case '4':
                                $goal = $action->trimestre_4;
                                $realize = $action->trimestre_4_r;
                                break;
                            default:
                                $goal = $action->meta_anual;
                                $realize = $action->meta_anual_r;
                                break;
                        }
                        foreach ($lines as $l_key => $line) {
                            // dd($line->topic->axe->punto);
                            switch ($line->topic->axe->punto) {
                                case 'A.1':
                                    if(!empty($axes))
                                    {
                                        if(in_array("A.1", $axes))
                                        {
                                            $data_direction["axes"]["axe-A.1"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-A.1"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-A.1"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.1"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-A.1"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.1"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-A.1"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.1"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-A.1"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-A.1"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                            $data_direction["axes"]["axe-A.1"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-A.1"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-A.1"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.1"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-A.1"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.1"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-A.1"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.1"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-A.1"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-A.1"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                    }
                                    break;
                                case 'A.2':
                                    if(!empty($axes))
                                    {
                                        if(in_array("A.2", $axes))
                                        {
                                            $data_direction["axes"]["axe-A.2"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-A.2"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-A.2"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.2"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-A.2"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.2"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-A.2"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.2"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-A.2"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-A.2"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-A.2"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-A.2"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-A.2"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.2"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-A.2"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.2"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-A.2"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.2"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-A.2"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-A.2"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case 'A.3':
                                    if(!empty($axes))
                                    {
                                        if(in_array("A.3", $axes))
                                        {
                                            $data_direction["axes"]["axe-A.3"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-A.3"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-A.3"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.3"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-A.3"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.3"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-A.3"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.3"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-A.3"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-A.3"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-A.3"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-A.3"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-A.3"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.3"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-A.3"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.3"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-A.3"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.3"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-A.3"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-A.3"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case 'A.4':
                                    if(!empty($axes))
                                    {
                                        if(in_array("A.4", $axes))
                                        {
                                            $data_direction["axes"]["axe-A.4"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-A.4"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-A.4"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.4"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-A.4"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.4"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-A.4"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.4"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-A.4"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-A.4"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        // dd("entro");
                                        $data_direction["axes"]["axe-A.4"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-A.4"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-A.4"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.4"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-A.4"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.4"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-A.4"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.4"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-A.4"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-A.4"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case 'B.1':
                                    if(!empty($axes))
                                    {
                                        if(in_array("B.1", $axes))
                                        {
                                            $data_direction["axes"]["axe-B.1"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-B.1"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-B.1"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.1"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-B.1"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.1"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-B.1"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.1"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-B.1"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-B.1"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-B.1"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-B.1"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-B.1"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.1"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-B.1"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.1"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-B.1"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.1"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-B.1"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-B.1"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case 'B.2':
                                    if(!empty($axes))
                                    {
                                        if(in_array("B.2", $axes))
                                        {
                                            $data_direction["axes"]["axe-B.2"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-B.2"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-B.2"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.2"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-B.2"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.2"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-B.2"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.2"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-B.2"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-B.2"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-B.2"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-B.2"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-B.2"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.2"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-B.2"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.2"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-B.2"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.2"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-B.2"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-B.2"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case 'B.3':
                                    if(!empty($axes))
                                    {
                                        if(in_array("B.3", $axes))
                                        {
                                            $data_direction["axes"]["axe-B.3"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-B.3"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-B.3"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.3"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-B.3"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.3"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-B.3"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.3"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-B.3"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-B.3"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-B.3"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-B.3"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-B.3"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.3"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-B.3"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.3"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-B.3"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.3"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-B.3"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-B.3"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case 'C.1':
                                    if(!empty($axes))
                                    {
                                        if(in_array("C.1", $axes))
                                        {
                                            $data_direction["axes"]["axe-C.1"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-C.1"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-C.1"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-C.1"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-C.1"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-C.1"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-C.1"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-C.1"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-C.1"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-C.1"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-C.1"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-C.1"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-C.1"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-C.1"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-C.1"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-C.1"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-C.1"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-C.1"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-C.1"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-C.1"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            $data[] = $data_direction;
        }
        $data[] = $total;
        // dd($data);
        return $data;
    }
    
    public function getStadisticsA2030($directions = null, $axes = null, $trimester = null)
    {
        // dd($directions, $axes, $trimester);
        if(empty($directions))
        {
            // $directions = Direction::all;
            $directions = Direction::whereNull("padre")->get();
        }else {
            $directions = Direction::whereIn('id', $directions)->get();
        }
        $total = [
            'name' => "Total",
            'axes' => [],
        ];
        $array_axes = [];
        $data = [
            'axes' => [],
        ];
        if (empty($axes)) 
        {
            $data['axes'] = ["Eje 1","Eje 2", "Eje 3","Eje 4", "Eje 5","Eje 6", "Eje 7","Eje 8", "Eje 9","Eje 10", "Eje 11","Eje 12", "Eje 13","Eje 14", "Eje 15", "Eje 16","Eje 17", "Total"];
            for ($i=0; $i < 8; $i++) {
                $axe = $i + 1;
                $arr_axes = [
                    $data['axes'][$i] => [
                        'label' => $data['axes'][$i],
                        'finish' => 0,
                        'process' => 0,
                        'null' => 0,
                        'empty' => 0,
                        'total' => 0,
                    ]
                ];
                $array_axes += $arr_axes;
            }
        }else {
            
            foreach ($axes as $key => $axe) {
                $data["axes"][] = $axe;
                $arr_axes = [
                    $axe => [
                        'label' => $axe,
                        'finish' => 0,
                        'process' => 0,
                        'null' => 0,
                        'empty' => 0,
                        'total' => 0,
                    ],
                ];
                $array_axes += $arr_axes;
            }
            $data["axes"][] = "Total";
        }
        $total_arr = [
            'total' => [
                'label' => "Total",
                'finish' => 0,
                'process' => 0,
                'null' => 0,
                'empty' => 0,
                'total' => 0,
            ]
        ];
        $array_axes += $total_arr;
        $total['axes'] = $array_axes;
        foreach ($directions as $key => $direction) {
            $data_direction = [
                'id' => $direction->id,
                'name' => $direction->nombre,
                'axes' => [],
            ];
            $data_direction['axes'] = $array_axes;
            // dd($data, $total,$data_direction);
            
            $programs = $direction->programs()->where("period", config('implan.periods.previous'))->get();
            foreach ($programs as $p_key => $program) 
            {
                $actions = $program->actions;

                foreach ($actions as $a_key => $action) {
                    $lines = $action->sche_metas;
                    $total_actions = count($lines);
                    if($total_actions > 0)
                    {
                        $fraction = 1 / $total_actions;
                        switch ($trimester) {
                            case '1':
                                $goal = $action->trimestre_1;
                                $realize = $action->trimestre_1_r;
                                break;
                            case '2':
                                $goal = $action->trimestre_2;
                                $realize = $action->trimestre_2_r;
                                break;
                            case '3':
                                $goal = $action->trimestre_3;
                                $realize = $action->trimestre_3_p;
                                break;
                            case '4':
                                $goal = $action->trimestre_4;
                                $realize = $action->trimestre_4_r;
                                break;
                            default:
                                $goal = $action->meta_anual;
                                $realize = $action->meta_anual_r;
                                break;
                        }
                        foreach ($lines as $l_key => $line) {
                            // dd($line->topic->axe->punto);
                            switch ($line->topic->axe->punto) {
                                case 'A.1':
                                    if(!empty($axes))
                                    {
                                        if(in_array("A.1", $axes))
                                        {
                                            $data_direction["axes"]["axe-A.1"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-A.1"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-A.1"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.1"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-A.1"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.1"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-A.1"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.1"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-A.1"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-A.1"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                            $data_direction["axes"]["axe-A.1"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-A.1"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-A.1"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.1"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-A.1"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.1"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-A.1"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.1"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-A.1"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-A.1"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                    }
                                    break;
                                case 'A.2':
                                    if(!empty($axes))
                                    {
                                        if(in_array("A.2", $axes))
                                        {
                                            $data_direction["axes"]["axe-A.2"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-A.2"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-A.2"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.2"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-A.2"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.2"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-A.2"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.2"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-A.2"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-A.2"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-A.2"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-A.2"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-A.2"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.2"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-A.2"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.2"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-A.2"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.2"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-A.2"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-A.2"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case 'A.3':
                                    if(!empty($axes))
                                    {
                                        if(in_array("A.3", $axes))
                                        {
                                            $data_direction["axes"]["axe-A.3"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-A.3"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-A.3"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.3"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-A.3"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.3"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-A.3"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.3"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-A.3"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-A.3"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-A.3"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-A.3"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-A.3"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.3"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-A.3"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.3"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-A.3"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.3"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-A.3"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-A.3"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case 'A.4':
                                    if(!empty($axes))
                                    {
                                        if(in_array("A.4", $axes))
                                        {
                                            $data_direction["axes"]["axe-A.4"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-A.4"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-A.4"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.4"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-A.4"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.4"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-A.4"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-A.4"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-A.4"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-A.4"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        // dd("entro");
                                        $data_direction["axes"]["axe-A.4"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-A.4"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-A.4"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.4"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-A.4"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.4"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-A.4"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-A.4"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-A.4"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-A.4"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case 'B.1':
                                    if(!empty($axes))
                                    {
                                        if(in_array("B.1", $axes))
                                        {
                                            $data_direction["axes"]["axe-B.1"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-B.1"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-B.1"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.1"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-B.1"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.1"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-B.1"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.1"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-B.1"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-B.1"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-B.1"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-B.1"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-B.1"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.1"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-B.1"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.1"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-B.1"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.1"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-B.1"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-B.1"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case 'B.2':
                                    if(!empty($axes))
                                    {
                                        if(in_array("B.2", $axes))
                                        {
                                            $data_direction["axes"]["axe-B.2"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-B.2"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-B.2"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.2"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-B.2"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.2"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-B.2"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.2"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-B.2"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-B.2"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-B.2"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-B.2"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-B.2"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.2"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-B.2"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.2"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-B.2"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.2"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-B.2"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-B.2"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case 'B.3':
                                    if(!empty($axes))
                                    {
                                        if(in_array("B.3", $axes))
                                        {
                                            $data_direction["axes"]["axe-B.3"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-B.3"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-B.3"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.3"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-B.3"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.3"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-B.3"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-B.3"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-B.3"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-B.3"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-B.3"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-B.3"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-B.3"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.3"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-B.3"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.3"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-B.3"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-B.3"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-B.3"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-B.3"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                                case 'C.1':
                                    if(!empty($axes))
                                    {
                                        if(in_array("C.1", $axes))
                                        {
                                            $data_direction["axes"]["axe-C.1"]["total"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                            $total["axes"]["axe-C.1"]["total"] += 1 * $fraction;
                                            $total["axes"]["total"]["total"] += 1 * $fraction;
                                            if($goal > 0)
                                            {
                                                $advance = ($realize * 100 ) / $goal;
                                                if($action->orientacion == 0)
                                                {
                                                    if ($advance > 0 && $advance < 100) {
                                                        $data_direction["axes"]["axe-C.1"]["process"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                        $total["axes"]["axe-C.1"]["process"] += 1 * $fraction;
                                                        $total["axes"]["total"]["process"] += 1 * $fraction;
                                                    } else if ($advance >= 100 ) {
                                                        $data_direction["axes"]["axe-C.1"]["finish"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["axe-C.1"]["finish"] += 1 * $fraction;
                                                        $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                    }else {
                                                        $data_direction["axes"]["axe-C.1"]["null"] += 1 * $fraction;
                                                        $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                        $total["axes"]["axe-C.1"]["null"] += 1 * $fraction;
                                                        $total["axes"]["total"]["null"] += 1 * $fraction;
                                                    }
                                                }
                                                else if($action->orientacion == 1)
                                                {
                                                    dd($action);
                                                }
                                            }
                                            else {
                                                $data_direction["axes"]["axe-C.1"]["empty"] += 1 * $fraction;
                                                $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                                $total["axes"]["axe-C.1"]["empty"] += 1 * $fraction;
                                                $total["axes"]["total"]["empty"] += 1 * $fraction;
                                            }
                                        }
                                    }else {
                                        $data_direction["axes"]["axe-C.1"]["total"] += 1 * $fraction;
                                        $data_direction["axes"]["total"]["total"] += 1 * $fraction;
                                        $total["axes"]["axe-C.1"]["total"] += 1 * $fraction;
                                        $total["axes"]["total"]["total"] += 1 * $fraction;
                                        if($goal > 0)
                                        {
                                            $advance = ($realize * 100 ) / $goal;
                                            if($action->orientacion == 0)
                                            {
                                                if ($advance > 0 && $advance < 100) {
                                                    $data_direction["axes"]["axe-C.1"]["process"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["process"] += 1 * $fraction;
                                                    $total["axes"]["axe-C.1"]["process"] += 1 * $fraction;
                                                    $total["axes"]["total"]["process"] += 1 * $fraction;
                                                } else if ($advance >= 100 ) {
                                                    $data_direction["axes"]["axe-C.1"]["finish"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["axe-C.1"]["finish"] += 1 * $fraction;
                                                    $total["axes"]["total"]["finish"] += 1 * $fraction;
                                                }else {
                                                    $data_direction["axes"]["axe-C.1"]["null"] += 1 * $fraction;
                                                    $data_direction["axes"]["total"]["null"] += 1 * $fraction;
                                                    $total["axes"]["axe-C.1"]["null"] += 1 * $fraction;
                                                    $total["axes"]["total"]["null"] += 1 * $fraction;
                                                }
                                            }
                                            else if($action->orientacion == 1)
                                            {
                                                dd($action);
                                            }
                                        }
                                        else {
                                            $data_direction["axes"]["axe-C.1"]["empty"] += 1 * $fraction;
                                            $data_direction["axes"]["total"]["empty"] += 1 * $fraction;
                                            $total["axes"]["axe-C.1"]["empty"] += 1 * $fraction;
                                            $total["axes"]["total"]["empty"] += 1 * $fraction;
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }
            }
            $data[] = $data_direction;
        }
        $data[] = $total;
        // dd($data);
        return $data;
    }

    public function saveImage($image_base64)
    {
        $image = $image_base64;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = 'actionsPMD.png';
        $url = \File::put(public_path(). '/assets/graphics/' . $imageName, base64_decode($image));
    }
    
    public function saveImageInafed($image_base64)
    {
        $image = $image_base64;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = 'actionsInafed.png';
        $url = \File::put(public_path(). '/assets/graphics/' . $imageName, base64_decode($image));
    }
    
    public function saveImageAgenda2030($image_base64)
    {
        $image = $image_base64;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = 'actionsagenda2030.png';
        $url = \File::put(public_path(). '/assets/graphics/' . $imageName, base64_decode($image));
    }

    public function getStadisticsPMD($trimester = null)
    {
        $axes = PmAxe::all();
        $stadistics = [];
        foreach ($axes as $key => $axe) {

            $la_total = $axe->getLaTotal();
            $la_worked = $axe->getLaWorked($trimester);
            $la_not_worked = $la_total - $la_worked;
            $la_not_worked_per = round(($la_not_worked * 100) / $la_total, 2);
            $la_finished = $axe->getLaFinished($trimester);
            $la_finished_per = round(($la_finished * 100) / $la_total, 2);
            $la_process = $axe->getLaProcess($trimester);
            $la_process_per = round(($la_process * 100) / $la_total, 2);
            $la_null = $axe->getLaNull($trimester);
            $la_null_per = round(($la_null * 100) / $la_total, 2);

            $data = [
                'name' =>  trim($axe->nombre),
                'alias' =>  "Eje ". $axe->punto,
                'la_total' => $la_total,
                'la_worked' => $la_worked,
                'la_not_worked' => $la_not_worked,
                'la_not_worked_per' => $la_not_worked_per,
                'la_finished' => $la_finished,
                'la_finished_per' => $la_finished_per,
                'la_process' => $la_process,
                'la_process_per' => $la_process_per,
                'la_null' => $la_null,
                'la_null_per' => $la_null_per,
            ];
            $stadistics[]=$data;
        }
        // dd($stadistics);
        return $stadistics;
    }

    public function getStadisticsInafed($trimester = null)
    {
        $axes = InfAxe::all();
        $stadistics = [];
        foreach ($axes as $key => $axe) {

            $la_total = $axe->getLaTotal();
            $la_worked = $axe->getLaWorked($trimester);
            $la_not_worked = $la_total - $la_worked;
            if($la_worked > 0)
            {
                $la_finished = $axe->getLaFinished($trimester);
                $la_finished_per = round(($la_finished * 100) / $la_worked, 2);
                $la_process = $axe->getLaProcess($trimester);
                $la_process_per = round(($la_process * 100) / $la_worked, 2);
                $la_null = $axe->getLaNull($trimester);
                $la_null_per = round(($la_null * 100) / $la_worked, 2);
            }else {
                $la_finished = 0;
                $la_finished_per = 0;
                $la_process = 0;
                $la_process_per = 0;
                $la_null = 0;
                $la_null_per = 0;
            }

            $data = [
                'name' =>  trim($axe->nombre),
                'alias' =>  "Eje ". $axe->punto,
                'la_total' => $la_total,
                'la_worked' => $la_worked,
                'la_not_worked' => $la_not_worked,
                'la_finished' => $la_finished,
                'la_finished_per' => $la_finished_per,
                'la_process' => $la_process,
                'la_process_per' => $la_process_per,
                'la_null' => $la_null,
                'la_null_per' => $la_null_per,
            ];
            $stadistics[]=$data;
        }
        return $stadistics;
    }
    
    public function getStadisticsAgenda2030($trimester = null)
    {
        $axes = AgeObjective::all();
        $stadistics = [];
        foreach ($axes as $key => $axe) {

            $la_total = $axe->getLaTotal();
            $la_worked = $axe->getLaWorked($trimester);
            $la_not_worked = $la_total - $la_worked;
            if($la_worked > 0)
            {
                $la_finished = $axe->getLaFinished($trimester);
                $la_finished_per = round(($la_finished * 100) / $la_worked, 2);
                $la_process = $axe->getLaProcess($trimester);
                $la_process_per = round(($la_process * 100) / $la_worked, 2);
                $la_null = $axe->getLaNull($trimester);
                $la_null_per = round(($la_null * 100) / $la_worked, 2);
            }else {
                $la_finished = 0;
                $la_finished_per = 0;
                $la_process = 0;
                $la_process_per = 0;
                $la_null = 0;
                $la_null_per = 0;
            }

            $data = [
                'name' =>  trim($axe->nombre),
                'alias' =>  "Eje ". $axe->punto,
                'la_total' => $la_total,
                'la_worked' => $la_worked,
                'la_not_worked' => $la_not_worked,
                'la_finished' => $la_finished,
                'la_finished_per' => $la_finished_per,
                'la_process' => $la_process,
                'la_process_per' => $la_process_per,
                'la_null' => $la_null,
                'la_null_per' => $la_null_per,
            ];
            $stadistics[]=$data;
        }
        return $stadistics;
    }

    public function getStadisticsActionLinesPMD($trimester = null)
    {
        $lines = PmActionLine::all();

        $data = [];
        foreach ($lines as $key => $line) 
        {
            if(count($line->actions) == 0)
            {
                $data_line = [
                    'axe' => $line->strategy->objective->axe->punto.". ".$line->strategy->objective->axe->nombre,
                    'objective' => $line->strategy->objective->punto.". ".$line->strategy->objective->nombre,
                    'strategy' => $line->strategy->punto.". ".$line->strategy->nombre,
                    'point' => $line->punto,
                    'name' => $line->nombre,
                    'type' => "No Prog.",
                ];
                $data[] = $data_line;
            }else {
                $bandera = 0;
                $actions = [];
                foreach ($line->actions as $action) 
                {
                    // dd($action);
                    if($action->program->period == config('implan.periods.previous'))
                    {
                        switch ($trimester) {
                            case '1':
                                if($action->trimestre_1 > 0 && $action->trimestre_1_r == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                            case '2':
                                if($action->trimestre_2 > 0 && $action->trimestre_2_r == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                            case '3':
                                if($action->trimestre_3 > 0 && $action->trimestre_3_p == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                            case '4':
                                if($action->trimestre_4 > 0 && $action->trimestre_4_r == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                            
                            default:
                                if($action->meta_anual > 0 && $action->meta_anual_r == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                        }
                    }
                }

                $data_line = [
                    'axe' => $line->strategy->objective->axe->punto.". ".$line->strategy->objective->axe->nombre,
                    'objective' => $line->strategy->objective->punto.". ".$line->strategy->objective->nombre,
                    'strategy' => $line->strategy->punto.". ".$line->strategy->nombre,
                    'point' => $line->punto,
                    'name' => $line->nombre,
                    'type' => "0 %.",
                    'actions' => $actions,
                ];
                $data[] = $data_line;
            }
        }
        return $data;
    }
    
    public function getStadisticsActionLinesADM($trimester = null)
    {
        $lines = InfIndicator::all();

        $data = [];
        foreach ($lines as $key => $line) 
        {
            if(count($line->actions) == 0)
            {
                $data_line = [
                    'schedule' => $line->topic->axe->schedule->punto.". ".$line->topic->axe->schedule->nombre,
                    'axe' => $line->topic->axe->punto.". ".$line->topic->axe->nombre,
                    'topic' => $line->topic->punto.". ".$line->topic->nombre,
                    'point' => $line->punto,
                    'name' => $line->nombre,
                    'type' => "No Prog.",
                ];
                $data[] = $data_line;
            }else {
                $bandera = 0;
                $actions = [];
                foreach ($line->actions as $action) 
                {
                    if($action->program->period == config('implan.periods.previous'))
                    {
                        switch ($trimester) {
                            case '1':
                                if($action->trimestre_1 > 0 && $action->trimestre_1_r == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                            case '2':
                                if($action->trimestre_2 > 0 && $action->trimestre_2_r == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                            case '3':
                                if($action->trimestre_3 > 0 && $action->trimestre_3_p == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                            case '4':
                                if($action->trimestre_4 > 0 && $action->trimestre_4_r == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                            
                            default:
                                if($action->meta_anual > 0 && $action->meta_anual_r == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                        }
                    }   
                }

                $data_line = [
                    'schedule' => $line->topic->axe->schedule->punto.". ".$line->topic->axe->schedule->nombre,
                    'axe' => $line->topic->axe->punto.". ".$line->topic->axe->nombre,
                    'topic' => $line->topic->punto.". ".$line->topic->nombre,
                    'point' => $line->punto,
                    'name' => $line->nombre,
                    'type' => "0 %.",
                    'actions' => $actions,
                ];
                $data[] = $data_line;
            }
        }
        // dd($data);
        return $data;
    }
    
    public function getStadisticsActionLinesA2030($trimester = null)
    {
        $lines = AgeMeta::all();

        $data = [];
        foreach ($lines as $key => $line) 
        {
            if(count($line->actions) == 0)
            {
                $data_line = [
                    'objective' => $line->objective->punto.". ".$line->objective->nombre,
                    'point' => $line->punto,
                    'name' => $line->nombre,
                    'type' => "No Prog.",
                ];
                $data[] = $data_line;
            }else {
                $bandera = 0;
                $actions = [];
                foreach ($line->actions as $action) 
                {
                    if($action->program->period == config('implan.periods.previous'))
                    {
                        switch ($trimester) {
                            case '1':
                                if($action->trimestre_1 > 0 && $action->trimestre_1_r == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                            case '2':
                                if($action->trimestre_2 > 0 && $action->trimestre_2_r == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                            case '3':
                                if($action->trimestre_3 > 0 && $action->trimestre_3_p == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                            case '4':
                                if($action->trimestre_4 > 0 && $action->trimestre_4_r == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                            
                            default:
                                if($action->meta_anual > 0 && $action->meta_anual_r == 0)
                                {
                                    $bandera = 1;
                                    $data_action = [
                                        'resp' => $action->program->direction->nombre,
                                        'name' => $action->actividad,
                                        'type' => "0 %"
                                    ];
                                    $actions[] = $data_action;
                                }
                                break;
                        }
                    }
                }

                $data_line = [
                    'objective' => $line->objective->punto.". ".$line->objective->nombre,
                    'point' => $line->punto,
                    'name' => $line->nombre,
                    'type' => "0 %.",
                    'actions' => $actions,
                ];
                $data[] = $data_line;
            }
        }
        // dd($data);
        return $data;
    }
}