<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;
use Storage;
//Models
use App\User;
use App\Models\Location;
use App\Models\Tracing\MonthlyActivity;
use App\Models\Tracing\Evidence;

use App\Models\ActionPoa;
use App\Models\SubactionPoa;

use App\Repositories\PlanningRepository;

class TracingRepository
{
    public function getAllLocations()
    {
        return Location::all();
    }
    
    public function storeMonthlyReport($data)
    {
        $meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

        $activity = new MonthlyActivity;
        $activity->accion = $data->name;
        $activity->beneficiarios_h = $data->beneficiaries_men;
        $activity->beneficiarios_m = $data->beneficiaries_women;
        $activity->monto = $data->amount;
        $activity->descripcion = $data->description;
        $activity->mes = explode("-",$data->mounth)[0];
        $activity->year = explode("-",$data->mounth)[1];
        $activity->direccion_id = $data->direction;
        
        if($activity->save())
        {
            foreach ($data->location as $key => $location) 
            {
                $activity->locations()->attach($location);
            }
            
            $planningRepository = new PlanningRepository;

            if(isset($data->evidences))
            {
                foreach ($data->evidences as $key => $file) {
                    $originalName = $file->getClientOriginalName();
                    $ex = $file->getClientOriginalExtension();
                    $mime = $file->getClientMimeType();
                    $name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 30);
                    
                    $direction = $planningRepository->getDirectionById($data->direction);
                    $mounth = explode('-', $data->mounth)[0];
                    $year = explode('-', $data->mounth)[1];
                    while (Storage::disk('public')->exists('evidencias/'.$direction->abreviacion.'/'.$meses[$mounth - 1].'-'.$year.'/'.$name.".".$ex)) 
                    {
                        $name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 30);
                    }
                    
                    if(Storage::disk('public')->putFileAs('evidencias/'.$direction->abreviacion.'/'.$meses[$mounth - 1].'-'.$year.'/', $file, $name.".".$ex))
                    {
                        $evidence = new Evidence;
                        $evidence->nombre_original = $originalName;
                        $evidence->mime_type = $mime;
                        $evidence->nombre = $name;
                        $evidence->url = 'evidencias/'.$direction->abreviacion.'/'.$meses[$mounth - 1].'-'.$year.'/';
                        $evidence->extencion = $ex;
                        $evidence->actividad_mesual_id = $activity->id;
                        $evidence->save();
    
                    }else {
                        $response = [
                            'status' => false,
                            'message' => "Ooops!! Algo sucedió intente de nuevo."
                        ];
                    }
                }
            }
            
            $response = [
                'status' => true,
                'message' => "Actividad del mes de ".$meses[explode("-",$data->mounth)[0] - 1]." guadada con exito."
            ];
        }else {
            $response = [
                'status' => false,
                'message' => "Ooops!! Algo sucedió intente de nuevo."
            ];
        }
        return $response;
    }

    public function getMonthlyActivityById($id)
    {
        return MonthlyActivity::find($id);
    }
    
    public function getEvidenceById($id)
    {
        return Evidence::find($id);
    }

    public function getActualTrimester($trimestre = null)
    {
        $date = Carbon::now("America/Mexico_City");
        switch ($trimestre) {
            case '1':
                $now = Carbon::create($date->format("Y"), 02, 06, 12, 0, 0, 'America/Mexico_City');
                break;

            case '2':
                $now = Carbon::create($date->format("Y"), 05, 06, 12, 0, 0, 'America/Mexico_City');
                break;

            case '3':
                $now = Carbon::create($date->format("Y"), 8, 06, 12, 0, 0, 'America/Mexico_City');
                break;

            case '4':
                $now = Carbon::create($date->format("Y"), 12, 06, 12, 0, 0, 'America/Mexico_City');
                break;
            
            default:
                $now = Carbon::now("America/Mexico_City");
                // $now = Carbon::create($date->format("Y"), 04, 14, 12, 0, 0, 'America/Mexico_City');
                break;
        }
        $last = Carbon::create($now->format("Y"), 1, 0, 0, 0, 0, 'America/Mexico_City')->addDays(7);
        $tri_1 = Carbon::create($now->format("Y"), 3, 31, 23, 59, 59, 'America/Mexico_City')->addDays(7);
        $tri_2 = Carbon::create($now->format("Y"), 6, 30, 23, 59, 59, 'America/Mexico_City')->addDays(7);
        $tri_3 = Carbon::create($now->format("Y"), 9, 30, 23, 59, 59, 'America/Mexico_City')->addDays(7);
        $tri_4 = Carbon::create($now->format("Y"), 12, 31, 23, 59, 59, 'America/Mexico_City')->addDays(7);

        $current_year = (date("m") == "01" && date("d") < 7)? $now->format("Y") -1 : $now->format("Y");
        if((request()->trimester)){
            $current_year = (date("Y") - 1);
        }
        // dd($now, $tri_1, $tri_2, $tri_3, $tri_4);
        // $mounth = $now->isoFormat('DDD');
        if ($now <= $last ) {
            // dd("Trimestre 1: ", $tri_1);
           
            $trimestre = [
                'number' => 4,
                //'label' => "Cuarto Trimestre del ".$now->subYear()->format("Y"),
                'label' => "Cuarto Trimestre del ".$now->subYear()->format("Y"),
                'mounths' => ['Octubre','Noviembre','Diciembre']
            ];
        }else if ($now > $last && $now <= $tri_1) {
            // dd("Trimestre 1: ", $tri_1);
            $trimestre = [
                'number' => 1,
                'label' => "Primer Trimestre del ". $current_year,
                'mounths' => ['Enero','Febrero','Marzo']
            ];
        }else if ($now > $tri_1 && $now <= $tri_2) {
            // dd("Trimestre 2: ", $tri_2);
            $trimestre = [
                'number' => 2,
                'label' => "Segundo Trimestre del ". $current_year,
                'mounths' => ['Abril','Mayo','Junio']
            ];
        }else if ($now > $tri_2 && $now <= $tri_3) {
            // dd("Trimestre 3: ", $tri_3);
            $trimestre = [
                'number' => 3,
                'label' => "Tercer Trimestre del ". $current_year,
                'mounths' => ['Julio','Agosto','Septiembre']
            ];
        }else if ($now > $tri_3 && $now <= $tri_4) {
            // dd("Trimestre 4: ", $tri_4);
            $trimestre = [
                'number' => 4,
                'label' => "Cuarto Trimestre del ". $current_year,
                'mounths' => ['Octubre','Noviembre','Diciembre']
            ];
            
           
        }
        
        return $trimestre;
    }

    public function storeActionGoals($data)
    {
        $action = ActionPoa::find($data->action_id);
        
        $trimester = $this->getActualTrimester($data->trimester);
        
        if($action->padre > 0)
        {
            $padre = ActionPoa::find($action->padre);
            if ($trimester["number"] == 1){
                $enero = $data->Enero - $action->enero;
                $febrero = $data->Febrero - $action->febrero;
                $marzo = $data->Marzo - $action->marzo;

                $padre->enero += $enero;
                $padre->febrero += $febrero;
                $padre->marzo += $marzo;
                
                $already = $padre->enero + $padre->febrero + $padre->marzo;
                $padre->trimestre_1_r = $already;
            }elseif ($trimester["number"] == 2){
                $abril = $data->Abril - $action->abril;
                $mayo = $data->Mayo - $action->mayo;
                $junio = $data->Junio - $action->junio;

                $padre->abril += $abril;
                $padre->mayo += $mayo;
                $padre->junio += $junio;
                
                $already = $padre->abril + $padre->mayo + $padre->junio;
                $padre->trimestre_2_r = $already;
            }elseif ($trimester["number"] == 3){
                $julio = $data->Julio - $action->julio;
                $agosto = $data->Agosto - $action->agosto;
                $septiembre = $data->Septiembre - $action->septiembre;

                $padre->julio += $julio;
                $padre->agosto += $agosto;
                $padre->septiembre += $septiembre;
                
                $already = $padre->julio + $padre->agosto + $padre->septiembre;
                $padre->trimestre_3_p = $already;
            }elseif ($trimester["number"] == 4){
                $octubre = $data->Octubre - $action->octubre;
                $noviembre = $data->Noviembre - $action->noviembre;
                $diciembre = $data->Diciembre - $action->diciembre;

                $padre->octubre += $octubre;
                $padre->noviembre += $noviembre;
                $padre->diciembre += $diciembre;
                
                $already = $padre->octubre + $padre->noviembre + $padre->diciembre;
                $padre->trimestre_4_r = $already;
            }
    
            $annual_already = $padre->trimestre_1_r +$padre->trimestre_2_r + $padre->trimestre_3_p + $padre->trimestre_4_r;
            $padre->meta_anual_r = $annual_already;
            $padre->save();
        }

        if ($trimester["number"] == 1){

            $action->enero = $data->Enero;
            $action->febrero = $data->Febrero;
            $action->marzo = $data->Marzo;

            $already = $data->Enero + $data->Febrero + $data->Marzo;
            $action->trimestre_1_r = $already;
        }elseif ($trimester["number"] == 2){

            $action->abril = $data->Abril;
            $action->mayo = $data->Mayo;
            $action->junio = $data->Junio;

            $already = $data->Abril + $data->Mayo + $data->Junio;
            $action->trimestre_2_r = $already;
        }elseif ($trimester["number"] == 3){

            $action->julio = $data->Julio;
            $action->agosto = $data->Agosto;
            $action->septiembre = $data->Septiembre;

            $already = $data->Julio + $data->Agosto + $data->Septiembre;
            $action->trimestre_3_p = $already;
        }elseif ($trimester["number"] == 4){

            $action->octubre = $data->Octubre;
            $action->noviembre = $data->Noviembre;
            $action->diciembre = $data->Diciembre;
            
            $already = $data->Octubre + $data->Noviembre + $data->Diciembre;
            $action->trimestre_4_r = $already;
        }

        $annual_already = $action->trimestre_1_r +$action->trimestre_2_r + $action->trimestre_3_p + $action->trimestre_4_r;
        $action->meta_anual_r = $annual_already;
        $action->save();

        if(isset($data->subactions))
        {
            foreach ($data->subactions as $key => $subaction_id) 
            {
                $subaction = SubactionPoa::find($subaction_id);
                
                if ($trimester["number"] == 1){

                    $subaction->enero = $data->sub_enero[$key];
                    $subaction->febrero = $data->sub_febrero[$key];
                    $subaction->marzo = $data->sub_marzo[$key];
        
                    $already = $data->sub_enero[$key] + $data->sub_febrero[$key] + $data->sub_marzo[$key];
                    $subaction->trimestre_1_r = $already;
                }elseif ($trimester["number"] == 2){
        
                    $subaction->abril = $data->sub_abril[$key];
                    $subaction->mayo = $data->sub_mayo[$key];
                    $subaction->junio = $data->sub_junio[$key];
        
                    $already = $data->sub_abril[$key] + $data->sub_mayo[$key] + $data->sub_junio[$key];
                    $subaction->trimestre_2_r = $already;
                }elseif ($trimester["number"] == 3){
        
                    $subaction->julio = $data->sub_julio[$key];
                    $subaction->agosto = $data->sub_agosto[$key];
                    $subaction->septiembre = $data->sub_septiembre[$key];
        
                    $already = $data->sub_julio[$key] + $data->sub_agosto[$key] + $data->sub_septiembre[$key];
                    $subaction->trimestre_3_r = $already;
                }elseif ($trimester["number"] == 4){
        
                    $subaction->octubre = $data->sub_octubre[$key];
                    $subaction->noviembre = $data->sub_noviembre[$key];
                    $subaction->diciembre = $data->sub_diciembre[$key];
                    
                    $already = $data->sub_octubre[$key] + $data->sub_noviembre[$key] + $data->sub_diciembre[$key];
                    $subaction->trimestre_4_r = $already;
                }
        
                $annual_already = $subaction->trimestre_1_r +$subaction->trimestre_2_r + $subaction->trimestre_3_r + $subaction->trimestre_4_r;
                $subaction->meta_anual_r = $annual_already;
                $subaction->save();
            }
        }

        return $action;
    }
}