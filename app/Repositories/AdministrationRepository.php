<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;
use DB;
use Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\OfficeNotification;
use App\Mail\ReponsePmduNotification;
//Models
use App\User;
use App\Models\Ticket;
use App\Models\Bitacora;
use App\Models\Notifications;
use App\Models\Offices;
use App\Models\Position;
use App\Models\Priority;

//PMDU
use App\Models\Pmdu\ObservationPmdu;
use App\Models\Pmdu\PmduSection;
use App\Models\Pmdu\Need;
use App\Models\Pmdu\Action;
use App\Models\Pmdu\Sector;
use App\Models\Pmdu\Location;

//Services
use Illuminate\Support\Facades\Hash;

class AdministrationRepository
{
	public function getActiveTickets()
	{
		return Ticket::whereIn('estatus_id', [1,2])->get();
	}
	
	public function getAllTickets()
	{
		return Ticket::all();
	}
	
	public function getTicketById($id)
	{
		$ticket =Ticket::find($id);
		$ticket->prioridad_txt = $ticket->priority->nombre;
		$ticket->prioridad_color = $ticket->priority->color;
		$ticket->estatus_txt = $ticket->status->nombre;
		$ticket->estatus_color = $ticket->status->color;
		$ticket->solicitante = $ticket->user->name;

		return $ticket;
	}
	
	public function changeTicketStatus($id, $status)
	{
		$ticket =Ticket::find($id);
		$ticket->estatus_id = $status;
		$ticket->save();

		$bitacora = new Bitacora;
		$bitacora->action = "El usuario ". Auth::user()->name . " cambio el estatus del ticket con folio " . $ticket->folio . ', de "Pendiente" a "En Revisión"';
		$bitacora->users_id = Auth::user()->id;
		$bitacora->save();

		$notification = new Notifications;
		$notification->user_id = $ticket->users_id;
		$notification->contenido = "El usuario ". Auth::user()->name . " cambio el estatus del ticket con folio " . $ticket->folio . ', de "Pendiente" a "En Revisión"';
		$notification->estatus = 0;
		$notification->save();

		return $ticket;
	}
	
	public function finishTicket($id, $comment)
	{
		$ticket =Ticket::find($id);
		$ticket->estatus_id = 3;
		$ticket->comentario = $comment;
		$ticket->fecha_termino = Carbon::now("America/Mexico_City");
		$ticket->save();

		$bitacora = new Bitacora;
		$bitacora->action = "El usuario ". Auth::user()->name . " cambio el estatus del ticket con folio " . $ticket->folio . ', de "En Revisión" a "Terminado"';
		$bitacora->user_id = Auth::user()->id;
		$bitacora->direccion_id = Auth::user()->profile->position->direction->id;
		$bitacora->save();

		$notification = new Notifications;
		$notification->user_id = $ticket->users_id;
		$notification->contenido = "El usuario ". Auth::user()->name . " cambio el estatus del ticket con folio " . $ticket->folio . ', de "En Revisión" a "Terminado"';
		$notification->estatus = 0;
		$notification->save();

		return $ticket;
	}

	public function storeTicket($data)
	{
		
		$old_ticket = Ticket::orderBy('id', 'DESC')->first();
		if(empty($old_ticket))
		{
			$folio = "00001";
		}else
		{
			$arr = intval(explode("SOP-", $old_ticket->folio)[1]);
			$folio = str_pad($arr+1, 5, "0", STR_PAD_LEFT);
		}
		$ticket = new Ticket;
		$ticket->folio = "SOP-".$folio;
		$ticket->descripcion = $data->description;
		$ticket->prioridad_id = $data->priority;
		$ticket->estatus_id = 1;
		$ticket->users_id = Auth::user()->id;
		$ticket->save();

		$bitacora = new Bitacora;
		$bitacora->action = "El usuario ". Auth::user()->name . " registro un ticket con folio " . $ticket->folio . ', el dia '. $ticket->created_at;
		$bitacora->user_id = Auth::user()->id;
		$bitacora->direccion_id = Auth::user()->profile->position->direction->id;
		$bitacora->save();

		$users = User::all();
		foreach ($users as $key => $user) {
			if($user->hasRole("Administrador"))
			{
				$notification = new Notifications;
				$notification->user_id = $user->id;
				$notification->contenido = "El usuario ". Auth::user()->name . " registro un ticket con folio " . $ticket->folio . ', el dia '. $ticket->created_at;
				$notification->estatus = 0;
				$notification->save();
			}
		}
		return "Se registro correctamente un ticket con folio ". $ticket->folio;
	}

	public function getAllUsers()
	{
		return User::all();
	}

	public function newUser($data)
	{
		$user = new User;
		$user->name = $data->name;
		$user->email = $data->email;
		$user->password = Hash::make($data->password);
		$user->save();

		$user->assignRole("Cliente");
	}

	public function getUserById($id)
	{
		return User::find($id);
	}
	
	//Offices Functions
	public function getAllOffices()
	{
		return Offices::orderByDesc('fecha_recepcion')->get();
	}

	public function storeOffice($data)
	{ 	
		if($data->tipo == 0)
		{
			DB::beginTransaction();

			try {
				// Validate, then create if valid
				$office = new Offices;

				if($data->respuesta != null){
					$office->respuesta = 1;
				}
				else{
					$office->respuesta = 0;
				}

				$office->estatus = 1;		
				$office->emisor = $data->emisor;
				if($data->pmdu_field){
					$old = Offices::where("no_oficio", "LIKE", "PMDU-R-%")->orderBy('id', 'DESC')->first();
					if($old)
					{
						$number = explode("-R-", $old->no_oficio)[1];
						$office->no_oficio = "PMDU-R-".str_pad($number + 1, 4, "0", STR_PAD_LEFT);
					}else
					{
						$office->no_oficio = "PMDU-R-0001";
					}
				}else {
					$office->no_oficio = $data->no_oficio;
				}
				$office->fecha_emision = $data->fecha_emision;
				$office->asunto = $data->asunto;
				$office->fecha_recepcion = $data->fecha_recepcion;
				$office->fecha_respuesta = $data->fecha_respuesta;
				$office->dependencia = $data->dependencia;			
				$office->type = $data->tipo;
				$office->save();
				$data->id = $office->id;
				$originalName = $data->oficio->getClientOriginalName();
				$ex = $data->oficio->getClientOriginalExtension();
				$no_oficio = str_replace('/', '-', $data->no_oficio);
				Storage::disk('public')->putFileAs('oficios', $data->oficio, $no_oficio.'-'.$data->id.'.'.$ex);
				$data->cc = 0;
				if(!empty($data->responsable))
				{
					$puesto = Position::find($data->responsable);
					$correo = $puesto->profile[0]->user->email;			
					Mail::to($correo)->send(new OfficeNotification($data));
				}
				
				if (Mail::failures()) {
					
				}else {
					
				}
				// dd($data->userAttach);
				if(!empty($data->userAttach))
				{
					foreach ($data->userAttach as $value)
					{
						$office->responsable()->attach($value, ['type' => 1]);
						$data->cc = 1;
						$puesto = Position::find($value);
						$correo = $puesto->profile[0]->user->email;
						Mail::to($correo)->send(new OfficeNotification($data));     	
					}						
				}	
				
				$office->responsable()->attach($data->responsable, ['type' => 0]);
				
			}catch(ValidationException $e){
				// Rollback and then redirect
				// back to form with errors
				DB::rollback();
			}
			DB::commit();
		}
		else if($data->tipo == 1)
		{
			DB::beginTransaction();
			try {
				$oficio = Offices::where('no_oficio', '=', $data->no_oficio )->count();
				if($oficio > 0)
				{
					$ex = $data->oficio->getClientOriginalExtension();
					$data->no_oficio = str_replace('/', '-', $data->no_oficio);
					Storage::disk('public')->putFileAs('oficios', $data->oficio, $data->no_oficio.'-'.$data->id.'.'.$ex);
					$office = Offices::find($data->id);
					$office->emisor = $data->emisor;
					$office->estatus = 3;
					$office->fecha_recepcion = $data->fecha_recepcion;
					$office->save();
				}
				else 
				{
					$office = new Offices;
					$office->emisor = $data->emisor;
					$no_office = DB::table('oficios')->where(function($query){
							$query->where('type', '=', 1)->orWhere('type', '=', 2);
						})->whereRaw("extract(year from created_at) = ".date("Y")."")->orderBy('id', 'desc')->first();			
					$now = Carbon::now();
					if($no_office != null)
					{
						$numero = substr($no_office->no_oficio, 4, -5);
						$id = (int)$numero;
						$id = $id+1;
						$id = str_pad($id, 4, "0", STR_PAD_LEFT);
						$no_oficio = 'IMP/'.$id.'/'.$now->year;
					}
					else 
					{
						$no_oficio = 'IMP/0001/'.$now->year;
					}
					$office->no_oficio = $no_oficio;
					$office->fecha_emision = $data->fecha_emision;
					$office->asunto = $data->asunto;
					$office->fecha_recepcion = $data->fecha_recepcion;
					$office->dependencia = $data->dependencia;			
					$office->type = $data->tipo;
					$office->estatus = 0;
					$office->save();

					if(!empty($data->userAttach))
					{
						foreach ($data->userAttach as $value)
						{
							$office->responsable()->attach($value, ['type' => 1]);
							$data->cc = 1;
							$puesto = Position::find($value);
							$correo = $puesto->profile[0]->user->email;
							Mail::to($correo)->send(new OfficeNotification($data));     	
						}						
					}	

					$office->responsable()->attach(Auth::user()->profile->position->id, ['type' => 0]);
				}
			} catch (ValidationException $e) {
				DB::rollback();
			}
			DB::commit();
		}
		else if($data->tipo == 2)
		{
			DB::beginTransaction();
			try {
				$oficio = Offices::where('no_oficio', '=', $data->no_oficio )->count();
				if($oficio > 0)
				{
					$ex = $data->oficio->getClientOriginalExtension();
					$data->no_oficio = str_replace('/', '-', $data->no_oficio);
					Storage::disk('public')->putFileAs('oficios', $data->oficio, $data->no_oficio.'-'.$data->id.'.'.$ex);


					$office = Offices::find($data->id);
					$office->fecha_recepcion = $data->fecha_recepcion;
					$office->estatus = 3;
					$office->save();

					
					$officeFather = Offices::find($office->dad->id);
					$officeFather->estatus = 3;
					$officeFather->save();
				}
				else 
				{
					$office = new Offices;
					$office->emisor = $data->emisor;
					$no_office = DB::table('oficios')->where(function($query){
						$query->where('type', '=', 1)->orWhere('type', '=', 2);
					})->whereRaw("extract(year from created_at) = ".date("Y")."")->orderBy('id', 'desc')->first();			
					$now = Carbon::now();
					if($no_office != null)
					{
						$numero = substr($no_office->no_oficio, 4, -5);
						$id = (int)$numero;
						$id = $id+1;
						$id = str_pad($id, 4, "0", STR_PAD_LEFT);
						$no_oficio = 'IMP/'.$id.'/'.$now->year;
					}
					else 
					{
						$no_oficio = 'IMP/0001/'.$now->year;
						$id = "0001";
					}
					$office->no_oficio = $no_oficio;
					$office->fecha_emision = $data->fecha_emision;
					$office->asunto = $data->asunto;
					$office->fecha_recepcion = $data->fecha_recepcion;
					$office->dependencia = $data->dependencia;			
					$office->type = $data->tipo;
					$office->id_padre = $data->id_padre;
					$office->estatus = 0;
					$office->save();
					if(!empty($data->userAttach))
					{
						foreach ($data->userAttach as $value)
						{
							$office->responsable()->attach($value, ['type' => 1]);
							$data->cc = 1;
							$puesto = Position::find($value);
							$correo = $puesto->profile[0]->user->email;
							Mail::to($correo)->send(new OfficeNotification($data));     	
						}						
					}	
					$office->responsable()->attach(Auth::user()->profile->position->id, ['type' => 0]);
				}
			} catch (ValidationException $e) {
				DB::rollback();
			}
			DB::commit();
		}
	}

	public function getOfficeById($id)
	{
		return Offices::find($id);
	}

	public function changeStatusByid($data)
	{
		$office = Offices::find($data->id);
		$office->estatus = $data->estatus;
		$office->save();
	}

	public function getAllPriorities()
	{
		return Priority::all();
	}

	public function sendNotification($data)
	{
		$office = Offices::find($data->id);
		$data->no_oficio = $office->no_oficio;
		$puesto = Position::find($data->encargado);
		$data->asunto = "alerta";
		$correo = $puesto->profile[0]->user->email;
		Mail::to($correo)->send(new OfficeNotification($data));     	
	}

	public function statusDelete($data)
	{
		$office = Offices::find($data->id);
		$office->estatus = 4;
		$office->save();
	}
	public function statusRestore($data)
	{
		$office = Offices::find($data->id);
		
		if($office->type == 0 && $office->respuesta == false)
		{
			$office->estatus = 1;
		}
		else if($office->type == 0 && $office->respuesta != false)
		{
			$office->estatus = 2;
		}
		else if($office->type == 1 || $office->type == 2)
		{
			$office->estatus = 0;
		}
		$office->save();
	}

	/*************************************************************************************************************************
	* *
	* * Seccion para datos de Observaciones PMDU
	* *
	**************************************************************************************************************************/

	public function getAllObservations($filters = null){
		if($filters)
		{
			$observations =  ObservationPmdu::select("*");
			if(array_key_exists("tb_cuenta", $filters) && $filters["tb_cuenta"]){
				$observations->where("folio", "LIKE", $filters["tb_cuenta"]."%");
			}
			if(array_key_exists("tb_nombre", $filters) && $filters["tb_nombre"]){
				// dd($filters);
				$observations->where(DB::raw("nombre || ' ' || apellidos"), "ILIKE", "%".$filters["tb_nombre"]."%");
			}
			if(array_key_exists("tb_correo", $filters) && $filters["tb_correo"]){
				$observations->where("correo", "LIKE", $filters["tb_correo"]."%");
			}
			if(array_key_exists("tb_section", $filters) && $filters["tb_section"]){
				$observations->where("origen", "=", $filters["tb_section"]);
			}
			if(array_key_exists("tb_procedence", $filters) && $filters["tb_procedence"]){
				// if($filters["tb_procedence"] != "En Estudio")
				// {
					$search = "";
					if($filters["tb_procedence"] == "Comentario")
					{
						$search = "Sin Aporte";
					}else {
						$search = $filters["tb_procedence"];
					}
					$observations->where("procede", "=", $search);
				// }else{
				// 	$array = ["Procede", "Procede Parcialmente", "No Procede"];
				// 	$observations->whereIn("procede", $array);
				// }

			}
			return $observations->get();
		}else {
			return ObservationPmdu::all();
		}
	}

	public function getAllObservationsForSing($folios = null, $procedencia = null)
	{
		if($folios){
			return ObservationPmdu::whereIn("folio", $folios)->whereIn("procede", ["Sin Aporte", "Relativo a otra área"])->where("estatus", 1)->get();
		}else{
			if($procedencia){
				return ObservationPmdu::where("procede", $procedencia)->where("estatus", 1)->where("firmada", true)->get();
			}else{
				return ObservationPmdu::whereIn("procede", ["Sin Aporte", "Relativo a otra área"])->where("estatus", 1)->get();
			}
		}
	}
	
	public function gatDataObservation($id){
		return ObservationPmdu::find($id);
	}

	public function gatDatastadistics($type = null){
		if(!$type){
			$data = [
				'date' => "Total",
				'views' => 0,
				'answered' => 0,
				'not_answered' => 0,
			];
			$observations = ObservationPmdu::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))->groupBy('date')->get();
			foreach ($observations as $key => $observation) {
				$answered = ObservationPmdu::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))->where(DB::raw('DATE(created_at)'), $observation->date)->where("estatus", 1)->groupBy('date')->first();
				$not_answered = ObservationPmdu::select(DB::raw('DATE(created_at) as date'), DB::raw('count(*) as views'))->where(DB::raw('DATE(created_at)'), $observation->date)->where("estatus", 0)->groupBy('date')->first();
				// dd($not_answered);
				$observation->answered = (!empty($answered->views) ? $answered->views : 0);
				$observation->not_answered = (!empty($not_answered->views) ? $not_answered->views : 0);
				$data["views"] += $observation->views;
				$data["answered"] += (!empty($answered->views) ? $answered->views : 0);
				$data["not_answered"] += (!empty($not_answered->views) ? $not_answered->views : 0);
			}
			
			$observations->push($data);
		}elseif ($type = "axe") {
			$observations = [
				'I' =>[
					'title' => "I - Procesos de Planeación",
					'answered' => 0,
					'not_answered' => 0,
					'total' => 0
				],
				'II' =>[
					'title' => "II - Diagnóstico",
					'answered' => 0,
					'not_answered' => 0,
					'total' => 0
				],
				'III' =>[
					'title' => "III - Nivel Normativo",
					'answered' => 0,
					'not_answered' => 0,
					'total' => 0
				],
				'IV' =>[
					'title' => "IV - Nivel Estratégico",
					'answered' => 0,
					'not_answered' => 0,
					'total' => 0
				],
				'V' =>[
					'title' => "V - Nivel Programatico",
					'answered' => 0,
					'not_answered' => 0,
					'total' => 0
				],
				'VI' =>[
					'title' => "VI - Nivel Instrumental",
					'answered' => 0,
					'not_answered' => 0,
					'total' => 0
				],
				'Total' =>[
					'title' => "Total",
					'answered' => 0,
					'not_answered' => 0,
					'total' => 0
				],
			];
			$datas = ObservationPmdu::all();
			foreach ($datas as $key => $data) {
				if (count($data->sections) > 0) {
					$val = 1 / count($data->sections);
					foreach ($data->sections as $key => $section) {
						switch ($section->seccion) {
							case 'I - Procesos de Planeación':
								if($data->estatus == 0){
									$observations["I"]["not_answered"] += $val;
									$observations["Total"]["not_answered"] += $val;
								}else {
									$observations["I"]["answered"] += $val;
									$observations["Total"]["answered"] += $val;
								}
								$observations["I"]["total"] += $val;
								$observations["Total"]["total"] += $val;
								break;
							case 'II - Diagnóstico':
								if($data->estatus == 0){
									$observations["II"]["not_answered"] += $val;
									$observations["Total"]["not_answered"] += $val;
								}else {
									$observations["II"]["answered"] += $val;
									$observations["Total"]["answered"] += $val;
								}
								$observations["II"]["total"] += $val;
								$observations["Total"]["total"] += $val;
								break;
							case 'III - Nivel Normativo':
								if($data->estatus == 0){
									$observations["III"]["not_answered"] += $val;
									$observations["Total"]["not_answered"] += $val;
								}else {
									$observations["III"]["answered"] += $val;
									$observations["Total"]["answered"] += $val;
								}
								$observations["III"]["total"] += $val;
								$observations["Total"]["total"] += $val;
								break;
							case 'IV - Nivel Estratégico':
								if($data->estatus == 0){
									$observations["IV"]["not_answered"] += $val;
									$observations["Total"]["not_answered"] += $val;
								}else {
									$observations["IV"]["answered"] += $val;
									$observations["Total"]["answered"] += $val;
								}
								$observations["IV"]["total"] += $val;
								$observations["Total"]["total"] += $val;
								break;
							case 'V - Nivel Programatico':
								if($data->estatus == 0){
									$observations["V"]["not_answered"] += $val;
									$observations["Total"]["not_answered"] += $val;
								}else {
									$observations["V"]["answered"] += $val;
									$observations["Total"]["answered"] += $val;
								}
								$observations["V"]["total"] += $val;
								$observations["Total"]["total"] += $val;
								break;
							case 'VI - Nivel Instrumental':
								if($data->estatus == 0){
									$observations["VI"]["not_answered"] += $val;
									$observations["Total"]["not_answered"] += $val;
								}else {
									$observations["VI"]["answered"] += $val;
									$observations["Total"]["answered"] += $val;
								}
								$observations["VI"]["total"] += $val;
								$observations["Total"]["total"] += $val;
								break;
						}
					}
				}
			}
		}
		// dd($observations);
		return $observations;
	}

	public function responseObservationPmdu($data)
	{
		DB::beginTransaction();

		try {
			$observation = ObservationPmdu::find($data->id);
			if($data->response)
			{
				$observation->estatus = 1;
				$observation->respuesta = $data->response;
			}
			$observation->procede = $data->procede;
			$observation->copy = $data->copy;
			$observation->update();
			// dd($observation);}
			if($data->resend != "false")
			{
				if ($observation->correo) {
					switch ($observation->procede) {
						case 'Procede':
						case 'Procede Parcialmente':
						case "No Procede Rev":
						case "No Procede":
							Mail::to($observation->correo)->subject("Atención de Observaciones al PMDU")->send(new ReponsePmduNotification($observation));
							$observation->enviado = 1;
							break;
						case "Sin Aporte":
						case "Relativo a otra área":
							if($observation->firmada == true)
							{
								$file = storage_path('app/public/signatured')."/".$observation->folio."-Respuesta.pdf";
								Mail::send('emails.pmdu_responsed', ['data' => $observation], function($message) use ($file, $observation) {
									$file = storage_path('app/public/signatured')."/".$observation->folio."-Respuesta.pdf";
									$message->to($observation->correo);
									$message->subject("Atención de Observaciones al PMDU");
									$message->attach($file);
								},true);
								$observation->enviado = 1;
							}
							break;
					}
					$observation->update();
					if (Mail::failures()) {
						DB::rollback();
					}
				}
			}
		}catch(ValidationException $e){
			DB::rollback();
		}
		DB::commit();
	}

	public function getAllNeeds()
    {
        return Need::all();
    }

    public function getAllActions()
    {
        return Action::all();
    }

    public function getAllSectores()
    {
        return Sector::all();
    }

    public function getAllLocations()
    {
        return Location::all();
	}
	
	public function addObservationPmdu($data)
    {
		$poll = new ObservationPMDU;
        $poll->folio = $data->folio;
        $poll->origen = $data->origin;
        $poll->procede = $data->procedence;
        $poll->nombre = $data->name;
        $poll->apellidos = $data->lastname;
        $poll->edad = $data->age;
        $poll->sexo = $data->sex;
        $poll->telefono = $data->tel;
        $poll->correo = $data->email;
        $poll->direccion = $data->address;
        $poll->localidad = $data->community;
        $poll->escolaridad = $data->scholarship;
        $poll->ocupacion = ($data->occupation == "Otro")? $data->other_occupation : $data->occupation;
        $poll->observaciones = $data->observation;
        $poll->save();

		$sect = new PmduSection;
		$sect->id_observacion = $poll->id;
		$sect->seccion = $data->origin;
		$sect->save();
	}
	
	public function getLastOficeNumber()
	{
		$poll = ObservationPMDU::orderBy("no_respuesta", "DESC")->first();
		dd($poll);
	}

	/*************************************************************************************************************************
	* *
	* * Seccion para datos de Observaciones PMDU
	* *
	**************************************************************************************************************************/

	public function registerMarker($data)
	{
		// dd($data);
		DB::connection("cartoWeb")->beginTransaction();
        try {
			switch ($data->tipo) {
				case 'op-covid':
					DB::connection('cartoWeb')->table("dse_casos_covid")->insert([
						"localidad" => $data->covi_localidad,
						"estatus" => $data->covi_status,
						"nombre" => $data->covi_name,
						"edad" => $data->covi_age,
						"sexo" => $data->covi_sex,
						"observacio" => $data->covi_onservation,
						"lu_atencio" => $data->covi_place,
						"origen" => $data->covi_origin,
						"tipo" => $data->covi_tipo,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-beneficiarios':
					DB::connection('cartoWeb')->table("dse_beneficiarios_programa_alimenticio")->insert([
						"folio" => $data->ben_folio,
						"apellido_p" => $data->ben_name,
						"sector_eco" => $data->ben_sector,
						"clave_unic" => $data->ben_curp,
						"domicilio" => $data->ben_address,
						"cp" => $data->ben_cp,
						"localidad" => $data->ben_localidad,
						"celular" => $data->ben_phone,
						"numero_de" => $data->ben_ine,
						"califica_s" => ($data->ben_status == "Sí") ? 1 : 0,
						"califica_n" => ($data->ben_status == "No") ? 1 : 0,
						"observacio" => $data->ben_onservation,
						"lat" => $data->latitud,
						"lon" => $data->longitud,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-tiendas':
					DB::connection('cartoWeb')->table("dse_tiendas_participantes")->insert([
						"registro" => $data->tie_date,
						"licencia" => $data->tie_licencia,
						"propietari" => $data->tie_propietario,
						"domicilio" => $data->tie_address,
						"nombre" => $data->tie_negocio,
						"giro" => $data->tie_giro,
						"localidad" => $data->tie_localidad,
						"telefono" => $data->tie_phone,
						"calca_entr" => $data->tie_calca,
						"caretas" => $data->tie_caretas,
						"lat" => $data->latitud,
						"lon" => $data->longitud,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-delitos':
					DB::connection('cartoWeb')->table("sp_delitos")->insert([
						"reporte" => $data->del_reporte,
						"tipo_delit" => $data->del_delito,
						"localidad" => $data->del_localidad,
						"ubicacion" => $data->del_address,
						"fecha_regi" => $data->del_date,
						"hora_regis" => $data->del_hour,
						"latitud" => $data->latitud,
						"longitud" => $data->longitud,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-concentracion':
					DB::connection('cartoWeb')->table("sp_concentracion_personas")->insert([
						"motivo" => $data->con_motivo,
						"fecha" => $data->con_date,
						"tipo" => $data->con_delito,
						"ubicacion" => $data->con_address,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->x." ".$data->y.")', 32613)"),
					]);
					break;
				case 'op-transito':
					DB::connection('cartoWeb')->table("tran_incidentes")->insert([
						"folio" => $data->tran_folio,
						"fecha_regi" => $data->tran_date,
						"hora_regis" => $data->tran_hour,
						"ubicacion" => $data->tran_address,
						"localidad" => $data->tran_localidad,
						"tipo_incid" => $data->tran_incidente,
						"estatus_af" => $data->tran_afectado,
						"lat" => $data->latitud,
						"lon" => $data->longitud,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-filtros':
					DB::connection('cartoWeb')->table("dse_filtros_sanitarios")->insert([
						"filtro_loc" => $data->fil_localidad,
						"personal" => $data->fil_personal,
						"rep_matutino" => $data->fil_turno_mat,
						"rep_vespertino" => $data->fil_turno_ves,
						"rep_nocturno" => $data->fil_turno_noc,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-empresas':
					DB::connection('cartoWeb')->table("de_empresas")->insert([
						"nombre" => $data->emp_nombre,
						"giro_empre" => $data->emp_giro,
						"domicilio" => $data->emp_domicilio,
						"localidad" => $data->emp_localidad,
						"afili_ss" => $data->emp_ss,
						"num_trabaj" => $data->emp_empleados,
						"n_despidos" => $data->emp_despidos,
						"tipo_apoyo" => $data->emp_tipo,
						"num_apoyos" => $data->emp_apoyos,
						"num_vacant" => $data->emp_vacantes,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-actividades':
					DB::connection('cartoWeb')->table("ser_acciones")->insert([
						"tipo_servi" => $data->serv_tipo,
						"localidad" => $data->serv_localidad,
						"num_servic" => $data->serv_numero,
						"fecha" => $data->serv_fecha,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-consume':
					DB::connection('cartoWeb')->table("de_consume_local")->insert([
						"nombre" => $data->consu_nombre,
						"empleados" => $data->consu_numero,
						"tipo" => $data->consu_tipo,
						"localidad" => $data->consu_localidad,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-violencia':
					DB::connection('cartoWeb')->table("dse_violencia")->insert([
						"tipo_atenc" => $data->viol_tipo,
						"nom_ager" => $data->viol_nombre,
						"direccion" => $data->viol_direccion,
						"localidad" => $data->viol_localidad,
						"reincident" => $data->viol_reincidencia,
						"num_agredi" => $data->viol_numero,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-dosificadores':
					DB::connection('cartoWeb')->table("oromapas_dosificadores")->insert([
						"num_identi" => $data->dosi_numero,
						"localidad" => $data->dosi_localidad,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-orservicios':
					DB::connection('cartoWeb')->table("oromapas_servicios")->insert([
						"tipo_serv" => $data->orserv_tipo,
						"fecha" => $data->orserv_date,
						"direccion" => $data->orserv_direccion,
						"localidad" => $data->orserv_localidad,
						"observacio" => $data->orserv_observation,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-comites':
					DB::connection('cartoWeb')->table("dse_comites_salud")->insert([
						"representante" => $data->comite_represent,
						"localidad" => $data->comite_localidad,
						"no_reportes" => $data->comite_reports,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-consulta':
					DB::connection('cartoWeb')->table("dse_consultas_medicas")->insert([
						"nombre" => $data->consulta_nombre,
						"edad" => $data->consulta_edad,
						"sexo" => $data->consulta_sex,
						"localidad" => $data->consulta_localidad,
						"sintomas" => $data->consulta_sintomas,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-traslados':
					DB::connection('cartoWeb')->table("pc_traslados")->insert([
						"sexo" => $data->traslados_sex,
						"ocupacion" => $data->traslados_ocupacion,
						"derechoabiente" => $data->traslados_derecho,
						"lugar" => $data->traslados_lugar,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-comedores':
					DB::connection('cartoWeb')->table("dif_comedores")->insert([
						"nombre" => $data->comedores_nombre,
						"localidad" => $data->comedores_localidad,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-apoyos':
					DB::connection('cartoWeb')->table("dif_apoyos")->insert([
						"tipo" => $data->apoyo_tipo,
						"nombre" => $data->apoyo_name,
						"curp" => $data->apoyo_curp,
						"ine" => $data->apoyo_ine,
						"domicilio" => $data->apoyo_address,
						"localidad" => $data->apoyo_localidad,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-caretas':
					DB::connection('cartoWeb')->table("pl_caretas")->insert([
						"nombre" => $data->caretas_name,
						"tipo" => $data->caretas_tipo,
						"localidad" => $data->caretas_localidad,
						"no_caretas" => $data->caretas_numero,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-direcciones':
					DB::connection('cartoWeb')->table("gen_direcciones")->insert([
						"name" => $data->direcciones_name,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->x." ".$data->y.")', 32613)"),
					]);
					break;
			}
            DB::connection("cartoWeb")->commit();
            return true;
        } catch (Exception $e) {
            DB::connection("cartoWeb")->rollback();
            return false;
        }
	}
	
	public function updateMarker($data)
	{
		// dd($data);
		DB::connection("cartoWeb")->beginTransaction();
        try {
			$info = explode(".", $data->marker);
			switch ($data->type_marker) {
				case 'op-covid':
					DB::connection('cartoWeb')->table("dse_casos_covid")->where("id", $info[1])->update([
						"estatus" => $data->covi_status,
					]);
					break;
				
				case 'op-tiendas':
					DB::connection('cartoWeb')->table("dse_tiendas_participantes")->where("id", $info[1])->update([
						"caretas" => $data->tie_caretas,
					]);
					break;
				
				case 'op-filtros':
					DB::connection('cartoWeb')->table("dse_filtros_sanitarios")->where("id", $info[1])->update([
						"personal" => $data->fil_personal,
						"rep_matutino" => $data->fil_turno_mat,
						"rep_vespertino" => $data->fil_turno_ves,
						"rep_nocturno" => $data->fil_turno_noc,
					]);
					break;
				case 'op-hospitales':
					DB::connection('cartoWeb')->table("dse_hospitales")->where("id", $info[1])->update([
						"camas" => $data->hosp_camas,
					]);
					break;
				
				case 'op-cementerios':
					DB::connection('cartoWeb')->table("obras_cementerios")->where("id", $info[1])->update([
						"nombre" => $data->ceme_ubicacion,
						"espacio_di" => $data->ceme_espacios,
					]);
					break;
				case 'op-empresas':
					DB::connection('cartoWeb')->table("de_empresas")->where("id", $info[1])->update([
						"nombre" => $data->emp_nombre,
						"giro_empre" => $data->emp_giro,
						"domicilio" => $data->emp_domicilio,
						"localidad" => $data->emp_localidad,
						"afili_ss" => $data->emp_ss,
						"num_trabaj" => $data->emp_empleados,
						"n_despidos" => $data->emp_despidos,
						"tipo_apoyo" => $data->emp_tipo,
						"num_apoyos" => $data->emp_apoyos,
						"num_vacant" => $data->emp_vacantes,
					]);
					break;
				case 'op-actividades':
					DB::connection('cartoWeb')->table("ser_acciones")->where("id", $info[1])->update([
						"tipo_servi" => $data->serv_tipo,
						"localidad" => $data->serv_localidad,
						"num_servic" => $data->serv_numero,
						"fecha" => $data->serv_fecha,
					]);
					break;
				case 'op-consume':
					DB::connection('cartoWeb')->table("de_consume_local")->where("id", $info[1])->update([
						"nombre" => $data->consu_nombre,
						"empleados" => $data->consu_numero,
						"tipo" => $data->consu_tipo,
						"localidad" => $data->consu_localidad,
					]);
					break;
				case 'op-dosificadores':
					DB::connection('cartoWeb')->table("oromapas_dosificadores")->where("id", $info[1])->update([
						"num_identi" => $data->dosi_numero,
						"localidad" => $data->dosi_localidad,
					]);
					break;
				case 'op-orservicios':
					DB::connection('cartoWeb')->table("oromapas_servicios")->where("id", $info[1])->update([
						"tipo_serv" => $data->orserv_tipo,
						"fecha" => $data->orserv_date,
						"direccion" => $data->orserv_direccion,
						"localidad" => $data->orserv_localidad,
						"observacio" => $data->orserv_observation,
					]);
					break;
				case 'op-comites':
					DB::connection('cartoWeb')->table("dse_comites_salud")->where("id", $info[1])->update([
						"representante" => $data->comite_represent,
						"localidad" => $data->comite_localidad,
						"no_reportes" => $data->comite_reports,
					]);
					break;
				case 'op-consulta':
					DB::connection('cartoWeb')->table("dse_consultas_medicas")->where("id", $info[1])->update([
						"nombre" => $data->consulta_nombre,
						"edad" => $data->consulta_edad,
						"sexo" => $data->consulta_sex,
						"localidad" => $data->consulta_localidad,
						"sintomas" => $data->consulta_sintomas,
					]);
					break;
				case 'op-traslados':
					DB::connection('cartoWeb')->table("pc_traslados")->where("id", $info[1])->update([
						"sexo" => $data->traslados_sex,
						"ocupacion" => $data->traslados_ocupacion,
						"derechoabiente" => $data->traslados_derecho,
						"lugar" => $data->traslados_lugar,
					]);
					break;
				case 'op-comedores':
					DB::connection('cartoWeb')->table("dif_comedores")->where("id", $info[1])->update([
						"nombre" => $data->comedores_nombre,
						"localidad" => $data->comedores_localidad,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-caretas':
					DB::connection('cartoWeb')->table("pl_caretas")->where("id", $info[1])->update([
						"nombre" => $data->caretas_name,
						"tipo" => $data->caretas_tipo,
						"localidad" => $data->caretas_localidad,
						"no_caretas" => $data->caretas_numero,
						'geodata' =>  DB::raw("ST_GeomFromText('POINT(".$data->longitud." ".$data->latitud.")', 4326)"),
					]);
					break;
				case 'op-direcciones':
					DB::connection('cartoWeb')->table("gen_direcciones")->where("id", $info[1])->update([
						"name" => $data->direcciones_name,
					]);
					break;
			}
            DB::connection("cartoWeb")->commit();
            return true;
        } catch (Exception $e) {
            DB::connection("cartoWeb")->rollback();
            return false;
        }
	}

	public function getDataReport()
	{
		$data = [];
		$data["covid"] = DB::connection('cartoWeb')->table("dse_casos_covid")->count();
		$data["beneficiarios"] = DB::connection('cartoWeb')->table("dse_beneficiarios_programa_alimenticio")->count();
		$data["tiendas"] = DB::connection('cartoWeb')->table("dse_tiendas_participantes")->count();
		$data["delitos"] = DB::connection('cartoWeb')->table("sp_delitos")->count();
		$data["concentracion"] = DB::connection('cartoWeb')->table("sp_concentracion_personas")->count();
		$data["transito"] = DB::connection('cartoWeb')->table("tran_incidentes")->count();
		$data["filtros"] = DB::connection('cartoWeb')->table("dse_filtros_sanitarios")->count();
		$data["hospitales"] = DB::connection('cartoWeb')->table("dse_hospitales")->count();

		// dd($data);
		return $data;
	}
	
	public function getDataMarker($data)
	{
		// dd($data->all());
		$response = [];
		$info = explode(".", $data->id);
		$marker = DB::connection('cartoWeb')->table($info[0])->where("id", $info[1])->first();
		switch ($info[0]) {
			case 'dse_filtros_sanitarios':
				$response["filtro"] = $marker;
				$response["personas"] =  DB::connection('cartoWeb')->table("dse_filtro_personas")->where("filtro_id", $info[1])->get();
				return $response;
				break;
			case 'oromapas_dosificadores':
				$response["data"] = $marker;
				$response["items"] =  DB::connection('cartoWeb')->table("oromapas_dosificadores_revisiones")->where("id_doficificador", $info[1])->get();
				return $response;
				break;
			case 'dse_hospitales':
				$response["data"] = $marker;
				$response["items"] =  DB::connection('cartoWeb')->table("dse_hospitales_apoyos")->where("id_hospital", $info[1])->get();
				return $response;
				break;
			case 'dif_comedores':
				$response["data"] = $marker;
				$response["items"] =  DB::connection('cartoWeb')->table("dif_comedores_beneficiarios")->where("id_comedor", $info[1])->get();
				return $response;
				break;
			case 'gen_direcciones':
				$response["data"] = $marker;
				$response["items"] =  DB::connection('cartoWeb')->table("gen_reportes")->where("direccion_id", $info[1])->get();
				return $response;
				break;
			default:
				return $marker;
				break;
		}
		
	}

	public function getUnits()
	{
		return DB::connection('cartoWeb')->table("gen_reportes")->select("unidad")->distinct("unidad")->get();
	}

	public function registerPersonFilter($data)
	{
		// dd($data);
		DB::connection("cartoWeb")->beginTransaction();
        try {
			
			$result = DB::connection('cartoWeb')->table("dse_filtro_personas")->insertGetId([
				"filtro_id" => $data->filter_id,
				"personas" => $data->person_persons,
				"turno" => $data->person_turn,
				"fecha" => $data->person_date,
				"vehiculos" => $data->person_vehi,
				"locales" => $data->person_locals,
				"foraneos" => $data->person_foran,
				"retornos" => $data->person_returns,
			]);
			// dd($result);
			$person = DB::connection('cartoWeb')->table("dse_filtro_personas")->find($result);
			DB::connection("cartoWeb")->commit();
			return $person;
		} catch (Exception $e) {
			DB::connection("cartoWeb")->rollback();
			return false;
		}
	}
	
	public function registerDosificador($data)
	{
		// dd($data);
		DB::connection("cartoWeb")->beginTransaction();
        try {
			
			$result = DB::connection('cartoWeb')->table("oromapas_dosificadores_revisiones")->insertGetId([
				"id_doficificador" => $data->dosificador_id,
				"fecha" => $data->dosifi_date,
				"observaciones" => $data->dosifi_observation,
				"tipo" => $data->dosifi_tipo,
			]);
			// dd($result);
			$person = DB::connection('cartoWeb')->table("oromapas_dosificadores_revisiones")->find($result);
			DB::connection("cartoWeb")->commit();
			return $person;
		} catch (Exception $e) {
			DB::connection("cartoWeb")->rollback();
			return false;
		}
	}

	public function registerApoyoHospital($data)
	{
		// dd($data);
		DB::connection("cartoWeb")->beginTransaction();
        try {
			
			$result = DB::connection('cartoWeb')->table("dse_hospitales_apoyos")->insertGetId([
				"id_hospital" => $data->hospital_id,
				"fecha" => $data->hosp_date,
				"cantidad" => $data->hosp_cant,
				"tipo" => $data->hosp_tipo,
			]);
			// dd($result);
			$person = DB::connection('cartoWeb')->table("dse_hospitales_apoyos")->find($result);
			DB::connection("cartoWeb")->commit();
			return $person;
		} catch (Exception $e) {
			DB::connection("cartoWeb")->rollback();
			return false;
		}
	}
	
	public function registerPorciones($data)
	{
		// dd($data);
		DB::connection("cartoWeb")->beginTransaction();
        try {
			
			$result = DB::connection('cartoWeb')->table("dif_comedores_beneficiarios")->insertGetId([
				"id_comedor" => $data->comedor_id,
				"fecha" => $data->porcion_date,
				"porciones" => $data->porcion_cant,
			]);
			// dd($result);
			$person = DB::connection('cartoWeb')->table("dif_comedores_beneficiarios")->find($result);
			DB::connection("cartoWeb")->commit();
			return $person;
		} catch (Exception $e) {
			DB::connection("cartoWeb")->rollback();
			return false;
		}
	}
	
	public function registerReport($data)
	{
		// dd($data);
		DB::connection("cartoWeb")->beginTransaction();
        try {
			
			$result = DB::connection('cartoWeb')->table("gen_reportes")->insertGetId([
				"direccion_id" => $data->direction_id,
				"unidad" => isset($data->new_report_unidad) ? $data->new_report_unidad : $data->report_unidad,
				"cantidad" => $data->report_cantidad,
				"costo" => $data->report_cost,
				"fecha" => $data->report_date,
			]);

			// dd($result);
			$report = DB::connection('cartoWeb')->table("gen_reportes")->find($result);
			DB::connection("cartoWeb")->commit();
			return $report;
		} catch (Exception $e) {
			DB::connection("cartoWeb")->rollback();
			return false;
		}
	}

	public function saveImageMapa($image_base64)
    {
        $image = $image_base64;  // your base64 encoded
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $imageName = 'mapa.png';
        $url = \File::put(public_path(). '/assets/graphics/' . $imageName, base64_decode($image));
	}

	public function getAllLocationsCarto()
	{
		return DB::connection("cartoWeb")->table("localidad_geom")->get();
	}
	
	public function getGeoDataReports($request)
	{
		switch ($request->tipo) {
			case 'general':
				return true;
				break;
			case 'covid':
				$data = [];
				$data["positivos"] = DB::connection('cartoWeb')->table("dse_casos_covid")->where("estatus", "Positivo")->count();
				// $data["altas"] = DB::connection('cartoWeb')->table("dse_casos_covid")->where("estatus", "Alta")->count();
				$data["defunciones"] = DB::connection('cartoWeb')->table("dse_casos_covid")->where("estatus", "Defunción")->count();
				return $data;
				break;
			case 'filtros':
				$data = [];
				$filtros = DB::connection('cartoWeb')->table("dse_filtros_sanitarios")->get();
				foreach ($filtros as $key => $filtro) {
					$info = [
						"nombre" => $filtro->filtro_loc,
					];
					$info["retornos"] =  (DB::connection('cartoWeb')->table("dse_filtro_personas")->select(DB::raw("SUM(retornos) AS retornos"))->where("filtro_id", $filtro->id)->first())->retornos;
					$info["foraneos"] = ( DB::connection('cartoWeb')->table("dse_filtro_personas")->select(DB::raw("SUM(foraneos) AS foraneos"))->where("filtro_id", $filtro->id)->first())->foraneos;
					$data[] = $info;
				}
				// dd($data);
				return $data;
				break;
			case 'comedores':
				$data = [];
				$comedores = DB::connection('cartoWeb')->table("dif_comedores")->select("localidad", DB::raw("sum(porciones) as porciones"))
								->join('dif_comedores_beneficiarios', 'dif_comedores_beneficiarios.id_comedor', '=', 'dif_comedores.id')
								->groupBy('dif_comedores.id')->get();
				foreach ($comedores as $key => $comedor) {
					$info = [
						"nombre" => $comedor->localidad,
						"porciones" => $comedor->porciones
					];
					$data[] = $info;
				}
				// dd($data);
				return $data;
				break;
			case 'hospitales':
				$data = [];
				$hospitales = DB::connection('cartoWeb')->table("dse_hospitales")->get();
				foreach ($hospitales as $key => $hospital) {
					$info = [
						"nombre" => $hospital->nom_estab,
						"camas" => $hospital->camas
					];
					$data[] = $info;
				}
				// dd($data);
				return $data;
				break;
			
			case 'apoyos':
				$data = [];
				if($request->localidad){
					if($request->localidad == "BAHIA DE BANDERAS"){
						$data["despensas"] = DB::connection('cartoWeb')->table("dif_apoyos")->where("tipo", "DESPENSA")->count();
						$data["vales"] =  DB::connection('cartoWeb')->table("dse_beneficiarios_programa_alimenticio")->where("entrega", 1)->count();
						$data["vales2"] =  DB::connection('cartoWeb')->table("dse_beneficiarios_programa_alimenticio")->where("entrega", 2)->count();
						$data["economicos"] = DB::connection('cartoWeb')->table("dif_apoyos_economicos")->count();
					}else{
						$data["despensas"] = DB::connection('cartoWeb')->table("dif_apoyos")->where("tipo", "DESPENSA")->where("localidad", $request->localidad)->count();
						$data["vales"] =  DB::connection('cartoWeb')->table("dse_beneficiarios_programa_alimenticio")->where("localidad", $request->localidad)->where("entrega", 1)->count();
						$data["vales2"] =  DB::connection('cartoWeb')->table("dse_beneficiarios_programa_alimenticio")->where("localidad", $request->localidad)->where("entrega", 2)->count();
						$data["economicos"] = DB::connection('cartoWeb')->table("dif_apoyos_economicos")->where("localidad", $request->localidad)->count();
					}
					return $data;
				}else{
					// $localidades = DB::connection('cartoWeb')->table("dse_beneficiarios_programa_alimenticio")->select("localidad")->distinct("localidad")->get();
					$localidades = DB::connection("cartoWeb")->table("localidad_geom")->get();
					return $localidades;
				}
				break;
			
			default:
				# code...
				break;
		}
	}
}

