<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;
use Storage;
//Models
use App\User;

//PM -> Municipal Plan
use App\Models\PmAxe;
use App\Models\PmActionLine;
use App\Models\PmActionRejected;
use App\Models\PmObjective;
//use App\Models\PmLineActionHasDirection;

//Inf -> INAFED
use App\Models\InfSchedule;
use App\Models\InfAxe;
use App\Models\InfIndicator;
use App\Models\InfIndicatorRejected;

//Inf -> Schedule 2030
use App\Models\AgeObjective;
use App\Models\AgeMeta;
use App\Models\AgeMetaRejected;

//Inf -> Mir
use App\Models\MirProgram;
use App\Models\MirIndicator;
use App\Models\MirIndicatorLevel;
use App\Models\MirIndicatorRejected;

//Others
use App\Models\Program;
use App\Models\Direction;
use App\Models\ActionPoa;
use App\Models\SubactionPoa;
use App\Models\Normative;
use App\Models\Position;

//New Models
use App\Models\NewPlanning\MirAreas;
use App\Models\NewPlanning\Component;
use App\Models\NewPlanning\MirComponent;
use App\Models\NewPlanning\ProgramMir;
use App\Models\NewPlanning\ActionsComponent;


class NewPlanningRepository
{
	public function getAxes()
	{
		return PmAxe::all();
	}
	
	public function getAxesADM()
	{
		return InfAxe::orderBy("punto")->get();
	}
	
	public function getAxesA2030()
	{
		return AgeObjective::orderBy("punto")->get();
	}
	
	public function getSchedulesInafed()
	{
		return InfSchedule::all();
	}

	public function getObjectivesSchedule()
	{
		return AgeObjective::all();
	}
	
	// public function getProgramsMir()
	// {
	// 	// return MirProgram::all();
	// 	return MirProgram::where('periodo', config('implan.periods.actual'))->get();
	// }
	
	public function getProgramsMirPeriods()
	{
		return MirProgram::select("periodo")->groupBy("periodo")->get();
	}
	
	// public function getMirIdicatorsTypes()
	// {
	// 	return MirIndicatorLevel::all();
	// }

	public function getNormatives()
	{
		return Normative::all();
	}

	public function getAllDirections()
	{
		return Direction::all();
	}
	
	public function getDirectionById($id)
	{
		return Direction::find($id);
	}

	public function getActionLinesByNormative($data)
	{
		$normative = Normative::find($data->normative);
		$direction = Direction::find($data->direction);
		
		switch ($normative->id) {
			case 1:
				$normative = $direction->actionLines()->wherePivot('period', config('implan.periods.actual'))->wherePivot("status", 1)->get();
				break;
			case 2:
				$normative = $direction->infIndicators()->wherePivot('period', config('implan.periods.actual'))->wherePivot("status", 1)->get();
				break;
			case 3:
				$normative = $direction->scheMeta()->wherePivot('period', config('implan.periods.actual'))->wherePivot("estatus", 1)->get();	
				break;
			case 4:
				$normative = $direction->mirIndicators()->wherePivot('period', config('implan.periods.actual'))->wherePivot("status", 1)->get();	
				break;
		}
		return $normative;
	}

	public function assignActionLines($data)
	{
		$direction = Direction::find($data->direction);
		
		foreach ($data->actionlines as $key => $actionline) {
			$direction->actionLines()->attach($actionline, ["period" => config('implan.periods.actual'), "status" => 1]);
		}
	}
	public function finishedActionLines($data)
	{
		foreach ($data->actionlines as $key => $actionline) {
			$line = PmActionLine::find($actionline);
			$line->finished_at = config('implan.periods.actual');
			$line->status = 1;
			$line->save();
		}
	}
	
	public function assignIndicators($data)
	{
		$direction = Direction::find($data->direction);
		
		foreach ($data->indicators as $key => $indicator) {
			$direction->infIndicators()->attach($indicator, ["period" => config('implan.periods.actual'), "status" => 1]);
		}
	}

	public function finishedIndicators($data)
	{
		
		foreach ($data->indicators as $key => $actionline) {
			$line = InfIndicator::find($actionline);
			$line->finished_at = config('implan.periods.actual');
			$line->status = 1;
			$line->save();
		}
	}
	
	public function assignSchedule($data)
	{
		$direction = Direction::find($data->direction);
		
		foreach ($data->metas as $key => $meta) {
			$direction->scheMeta()->attach($meta, ["period" => config('implan.periods.actual'), "estatus" => 1]);
		}
	}

	public function finishedSchedule($data)
	{
		
		foreach ($data->metas as $key => $actionline) {
			$line = AgeMeta::find($actionline);
			$line->finished_at = config('implan.periods.actual');
			$line->status = 1;
			$line->save();
		}
	}
	
	public function assignMir($data)
	{
		$direction = Direction::find($data->direction);
		
		foreach ($data->indicators as $key => $indicator) {
			$direction->mirIndicators()->attach($indicator, ["period" => config('implan.periods.actual'), "status" => 1]);
		}
	}

	public function finishedMir($data)
	{
		
		foreach ($data->indicators as $key => $actionline) {
			$line = MirIndicator::find($actionline);
			$line->finished_at = config('implan.periods.actual');
			$line->status = 1;
			$line->save();
		}
	}

	public function dettachActionLines($data)
	{
		
		$direction = Direction::find($data->direction);
		foreach ($direction->programs()->where('period', config('implan.periods.actual'))->get() as $p_key => $program) 
		{
			foreach ($program->actions as $a_key => $action) 
			{
				$action->pm_action_lines()->detach($data->line);
			}
		}
		$direction->actionLines()->wherePivot('period', config('implan.periods.actual'))->detach($data->line);
	}
	
	public function dettachInafed($data)
	{
		$direction = Direction::find($data->direction);
		foreach ($direction->programs()->where('period', config('implan.periods.actual'))->get() as $p_key => $program) 
		{
			foreach ($program->actions as $a_key => $action) 
			{
				$action->inf_action_lines()->detach($data->line);
			}
		}
		$direction->infIndicators()->wherePivot('period', config('implan.periods.actual'))->detach($data->line);
	}

	public function dettachSchedule($data)
	{
		$direction = Direction::find($data->direction);
		foreach ($direction->programs()->where('period', config('implan.periods.actual'))->get() as $p_key => $program) 
		{
			foreach ($program->actions as $a_key => $action) 
			{
				$action->sche_metas()->detach($data->line);
			}
		}
		$direction->scheMeta()->wherePivot('period', config('implan.periods.actual'))->detach($data->line);
	}
	
	public function dettachMir($data)
	{
		// dd($data->all());
		$direction = Direction::find($data->direction);
		foreach ($direction->programs()->where('period', $data->period)->get() as $p_key => $program) 
		{
			foreach ($program->actions as $a_key => $action) 
			{
				$action->mir_idicators()->detach($data->line);
			}
		}
		$direction->mirIndicators()->wherePivot('period', $data->period)->detach($data->line);
	}

	public function getPmActionLineById($data)
	{
		return PmActionLine::find($data->id);
	}
	
	public function getInfIndicatorById($data)
	{
		return InfIndicator::find($data->id);
	}
	
	public function getScheMetaById($data)
	{
		return AgeMeta::find($data->id);
	}
	
	public function getMirIndicatorById($data)
	{
		return MirIndicator::find($data->id);
	}

	public function getResposable($id)
	{
		return Position::find($id);
	}

	public function getPmActionRejectLineById($id)
	{
		return PmActionRejected::where("pm_linea_accion_id", $id)->first();
	}
	
	public function getInfIndicatorRejectLineById($id)
	{
		return InfIndicatorRejected::where("inf_indicador_id", $id)->first();
	}
	
	public function getScheMetaRejectById($id)
	{
		return AgeMetaRejected::where("age_meta_id", $id)->first();
	}
	
	public function getMirIndicatorRejectById($id)
	{
		return MirIndicatorRejected::where("mir_indicador_id", $id)->first();
	}

	public function acceptPmActionLine($data)
	{
		$actionline = PmActionLine::find($data->id);
		$actionline->directions()->wherePivot('period', config('implan.periods.actual'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["status" => 1]);
	}

	public function rejectPmActionLine($data)
	{
		$file = $data->file("file");
		$originalName = $file->getClientOriginalName();
		$ex = $file->getClientOriginalExtension();
		$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		while (Storage::disk('local')->exists('actions_rejecteds/'.$name.".".$ex)) 
		{
			$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		}
		
		if(Storage::disk('local')->putFileAs('actions_rejecteds/', $file, $name.".".$ex))
		{
			$action = new PmActionRejected;
			$action->descripcion = $data->description;
			$action->original_nombre = $originalName;
			$action->nombre = $name;
			$action->url = 'actions_rejecteds/';
			$action->user_id = Auth::user()->id;
			$action->pm_linea_accion_id = $data->id_action_line;
			$action->save();

			$actionline = PmActionLine::find($data->id_action_line);
			$actionline->directions()->wherePivot('period', config('implan.periods.actual'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["status" => 2]);
		}
	}

	public function acceptInfIndicator($data)
	{
		$indicator = InfIndicator::find($data->id);
		$indicator->directions()->wherePivot('period', config('implan.periods.actual'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["status" => 1]);
	}

	public function rejectInfIndicator($data)
	{
		$file = $data->file("file");
		$originalName = $file->getClientOriginalName();
		$ex = $file->getClientOriginalExtension();
		$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		while (Storage::disk('local')->exists('indicators_rejecteds/'.$name.".".$ex)) 
		{
			$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		}
		if(Storage::disk('local')->putFileAs('indicators_rejecteds/', $file, $name.".".$ex))
		{
			$action = new InfIndicatorRejected;
			$action->descripcion = $data->description;
			$action->original_nombre = $originalName;
			$action->nombre = $name;
			$action->url = 'indicators_rejecteds/';
			$action->user_id = Auth::user()->id;
			$action->inf_indicador_id = $data->id_inf_indicator;
			$action->save();

			$actionline = InfIndicator::find($data->id_inf_indicator);
			$actionline->directions()->wherePivot('period', config('implan.periods.actual'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["status" => 2]);
		}
	}

	public function acceptScheMeta($data)
	{
		$indicator = AgeMeta::find($data->id);
		$indicator->directions()->wherePivot('period', config('implan.periods.actual'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["estatus" => 1]);
	}
	
	public function rejectScheMeta($data)
	{
		$file = $data->file("file");
		$originalName = $file->getClientOriginalName();
		$ex = $file->getClientOriginalExtension();
		$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		while (Storage::disk('local')->exists('metas_rejecteds/'.$name.".".$ex)) 
		{
			$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		}
		if(Storage::disk('local')->putFileAs('metas_rejecteds/', $file, $name.".".$ex))
		{
			$action = new AgeMetaRejected;
			$action->descripcion = $data->description;
			$action->original_nombre = $originalName;
			$action->nombre = $name;
			$action->url = 'metas_rejecteds/';
			$action->user_id = Auth::user()->id;
			$action->age_meta_id = $data->id_sche_meta;
			$action->save();

			$actionline = AgeMeta::find($data->id_sche_meta);
			$actionline->directions()->wherePivot('period', config('implan.periods.actual'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["estatus" => 2]);
		}
	}
	
	public function acceptMirIndicator($data)
	{
		$indicator = MirIndicator::find($data->id);
		$indicator->directions()->wherePivot('period', config('implan.periods.actual'))->updateExistingPivot($data->direction, ["status" => 1]);
	}
	
	public function rejectMirIndicator($data)
	{
		$file = $data->file("file");
		$originalName = $file->getClientOriginalName();
		$ex = $file->getClientOriginalExtension();
		$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		while (Storage::disk('local')->exists('mir_idicators_rejecteds/'.$name.".".$ex)) 
		{
			$name = substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, 50);
		}
		if(Storage::disk('local')->putFileAs('mir_idicators_rejecteds/', $file, $name.".".$ex))
		{
			$action = new MirIndicatorRejected;
			$action->descripcion = $data->description;
			$action->original_nombre = $originalName;
			$action->nombre = $name;
			$action->url = 'mir_idicators_rejecteds/';
			$action->user_id = Auth::user()->id;
			$action->mir_indicador_id = $data->id_mir_indicator;
			$action->save();

			$actionline = MirIndicator::find($data->id_mir_indicator);
			$actionline->directions()->wherePivot('period', config('implan.periods.actual'))->updateExistingPivot(Auth::user()->profile->position->direction->id, ["status" => 2]);
		}
	}

	// public function storeComponent($data)
	// {
	// 	$program = new Component;
	// 	$program->nombre = $data->name;
	// 	$program->objetivo = $data->description;
	// 	$program->programa_id = $data->program;
	// 	$program->direccion_id = $data->direction_id;
	// 	$program->periodo = 2021;
	// 	$program->save();
	// }

	public function getProgramById($id)
	{
		return Program::find($id);
	}

	public function deleteComponent($data)
	{
		$program = MirComponent::find($data->component);
		if(!empty($program->pm_action_lines))
		{
			foreach ($program->pm_action_lines as $key => $actionline) {
				$program->pm_action_lines()->detach($actionline->id);
			}
		}
		
		if(!empty($program->inf_action_lines))
		{
			foreach ($program->inf_action_lines as $key => $actionline) {
				$program->inf_action_lines()->detach($actionline->id);
			}
		}
		
		if(!empty($program->sche_metas))
		{
			foreach ($program->sche_metas as $key => $actionline) {
				$program->sche_metas()->detach($actionline->id);
			}
		}

		if(count($program->actions) > 0)
		{
			foreach ($program->actions as $key => $action) {
				//Eliminar las Subacciones
				if(count($action->subactions) > 0)
				{
					foreach ($action->subactions as $key => $subaction) {
						$subaction->delete();
					}
				}
				//Eliminar lineas de Accion asignadas a la acción
				if(!empty($action->pm_action_lines))
				{
					foreach ($action->pm_action_lines as $key => $actionline) {
						$action->pm_action_lines()->detach($actionline->id);
					}
				}
				
				if(!empty($action->inf_action_lines))
				{
					foreach ($action->inf_action_lines as $key => $actionline) {
						$action->inf_action_lines()->detach($actionline->id);
					}
				}
				
				if(!empty($action->sche_metas))
				{
					foreach ($action->sche_metas as $key => $actionline) {
						$action->sche_metas()->detach($actionline->id);
					}
				}
				
				if(!empty($action->mir_idicators))
				{
					foreach ($action->mir_idicators as $key => $actionline) {
						$action->mir_idicators()->detach($actionline->id);
					}
				}
				//Eliminar la acción
				$action->delete();
			}
		}

		if($program->delete())
		{
			return 1;
		}else
		{
			return 0;
		}
	}

	public function storeComponent($data)
	{
		// dd($data);
		$component = new MirComponent;
		$component->periodo = 2021;
		$component->direction_id = $data->direction;
		$component->area_id = $data->area;
		$component->actividad = $data->activity;
		$component->unidad_medida = $data->unity;
		$component->meta_anual = $data->year;
		$component->trimestre_1 = $data->first;
		$component->trimestre_2 = $data->second;
		$component->trimestre_3 = $data->thirth;
		$component->trimestre_4 = $data->four;
		$component->valor = $data->value;
		$component->orientacion = $data->type;
		$component->beneficiarios = $data->beneficiarios;
		$component->periodo_verificacion = $data->periodo;
		$component->dimension = $data->dimension;
		$component->formula = $data->formula;
		$component->objetivo = $data->objetivo;
		$component->puesto_id = $data->responsable;
		$component->save();

		if(!empty($data->pm_normatives) > 0)
		{
			foreach ($data->pm_normatives as $key => $pm_normative) {
				$component->pm_action_lines()->attach($pm_normative);
			}
		}

		if(!empty($data->inf_normatives) > 0)
		{
			foreach ($data->inf_normatives as $key => $inf_normative) {
				$component->inf_action_lines()->attach($inf_normative);
			}
		}
		
		if(!empty($data->sche_normatives) > 0)
		{
			foreach ($data->sche_normatives as $key => $sche_normative) {
				$component->sche_metas()->attach($sche_normative);
			}
		}
	}

	public function editComponent($data)
	{
		// dd($data);
		$action = MirComponent::find($data->component_id);
		$action->actividad = $data->activity;
		$action->unidad_medida = $data->unity;
		$action->meta_anual = $data->year;
		$action->trimestre_1 = $data->first;
		$action->trimestre_2 = $data->second;
		$action->trimestre_3 = $data->thirth;
		$action->trimestre_4 = $data->four;
		$action->valor = $data->value;
		$action->orientacion = $data->type;
		$action->beneficiarios = $data->beneficiarios;
		$action->periodo_verificacion = $data->periodo;
		$action->dimension = $data->dimension;
		$action->formula = $data->formula;
		$action->objetivo = $data->objetivo;
		$action->puesto_id = $data->responsable;

		$action->save();

		if(!empty($action->pm_action_lines))
		{
			foreach ($action->pm_action_lines as $key => $actionline) {
				$action->pm_action_lines()->detach($actionline->id);
			}
		}
		
		if(!empty($action->inf_action_lines))
		{
			foreach ($action->inf_action_lines as $key => $actionline) {
				$action->inf_action_lines()->detach($actionline->id);
			}
		}
		
		if(!empty($action->sche_metas))
		{
			foreach ($action->sche_metas as $key => $actionline) {
				$action->sche_metas()->detach($actionline->id);
			}
		}
		

		if(!empty($data->pm_normatives))
		{
			foreach ($data->pm_normatives as $key => $pm_normative) {
				$action->pm_action_lines()->attach($pm_normative);
			}
		}

		if(!empty($data->inf_normatives))
		{
			foreach ($data->inf_normatives as $key => $inf_normative) {
				$action->inf_action_lines()->attach($inf_normative);
			}
		}
		if(!empty($data->sche_normatives))
		{
			foreach ($data->sche_normatives as $key => $inf_normative) {
				$action->sche_metas()->attach($inf_normative);
			}
		}
	}

	public function storeActionComponent($data)
	{
		// dd($data);
		$action = new ActionsComponent;
		$action->actividad = $data->activity;
		$action->unidad_medida = $data->unity;
		$action->meta_anual = $data->year;
		$action->trimestre_1 = $data->first;
		$action->trimestre_2 = $data->second;
		$action->trimestre_3 = $data->thirth;
		$action->trimestre_4 = $data->four;
		$action->componente_id = $data->component;
		$action->valor = $data->value;
		$action->orientacion = $data->type;
		$action->beneficiarios = $data->beneficiarios;
		$action->periodo_verificacion = $data->periodo;
		$action->dimension = $data->dimension;
		$action->formula = $data->formula;
		$action->objetivo = $data->objetivo;
		$action->puesto_id = $data->responsable;
		$action->save();

		if(!empty($data->pm_normatives) > 0)
		{
			foreach ($data->pm_normatives as $key => $pm_normative) {
				$action->pm_action_lines()->attach($pm_normative);
			}
		}

		if(!empty($data->inf_normatives) > 0)
		{
			foreach ($data->inf_normatives as $key => $inf_normative) {
				$action->inf_action_lines()->attach($inf_normative);
			}
		}
		
		if(!empty($data->sche_normatives) > 0)
		{
			foreach ($data->sche_normatives as $key => $sche_normative) {
				$action->sche_metas()->attach($sche_normative);
			}
		}
		
		if(!empty($data->mir_normatives) > 0)
		{
			foreach ($data->mir_normatives as $key => $mir_normative) {
				$action->mir_idicators()->attach($mir_normative);
			}
		}

	}
	
	public function updateActionComponent($data)
	{
		$action = ActionsComponent::find($data->id);
		$action->actividad = $data->activity;
		$action->unidad_medida = $data->unity;
		$action->meta_anual = $data->year;
		$action->trimestre_1 = $data->first;
		$action->trimestre_2 = $data->second;
		$action->trimestre_3 = $data->thirth;
		$action->trimestre_4 = $data->four;
		$action->valor = $data->value;
		$action->orientacion = $data->type;
		$action->beneficiarios = $data->beneficiarios;
		$action->periodo_verificacion = $data->periodo;
		$action->dimension = $data->dimension;
		$action->formula = $data->formula;
		$action->objetivo = $data->objetivo;
		$action->puesto_id = $data->responsable;

		$action->save();

		if(!empty($action->pm_action_lines))
		{
			foreach ($action->pm_action_lines as $key => $actionline) {
				$action->pm_action_lines()->detach($actionline->id);
			}
		}
		
		if(!empty($action->inf_action_lines))
		{
			foreach ($action->inf_action_lines as $key => $actionline) {
				$action->inf_action_lines()->detach($actionline->id);
			}
		}
		
		if(!empty($action->sche_metas))
		{
			foreach ($action->sche_metas as $key => $actionline) {
				$action->sche_metas()->detach($actionline->id);
			}
		}
		
		if(!empty($action->mir_idicators))
		{
			foreach ($action->mir_idicators as $key => $actionline) {
				$action->mir_idicators()->detach($actionline->id);
			}
		}

		if(!empty($data->pm_normatives))
		{
			foreach ($data->pm_normatives as $key => $pm_normative) {
				$action->pm_action_lines()->attach($pm_normative);
			}
		}

		if(!empty($data->inf_normatives))
		{
			foreach ($data->inf_normatives as $key => $inf_normative) {
				$action->inf_action_lines()->attach($inf_normative);
			}
		}
		if(!empty($data->sche_normatives))
		{
			foreach ($data->sche_normatives as $key => $inf_normative) {
				$action->sche_metas()->attach($inf_normative);
			}
		}
		
		if(!empty($data->mir_normatives))
		{
			foreach ($data->mir_normatives as $key => $mir_normative) {
				$action->mir_idicators()->attach($mir_normative);
			}
		}

	}

	public function deleteActionComponent($data)
	{
		$action = ActionsComponent::find($data->action);

		//Eliminar lineas de Accion asignadas a la acción
		if(!empty($action->pm_action_lines))
		{
			foreach ($action->pm_action_lines as $key => $actionline) {
				$action->pm_action_lines()->detach($actionline->id);
			}
		}
		
		if(!empty($action->inf_action_lines))
		{
			foreach ($action->inf_action_lines as $key => $actionline) {
				$action->inf_action_lines()->detach($actionline->id);
			}
		}
		
		if(!empty($action->sche_metas))
		{
			foreach ($action->sche_metas as $key => $actionline) {
				$action->sche_metas()->detach($actionline->id);
			}
		}
		
		//Eliminar la acción
		if($action->delete())
		{
			return 1;
		}else
		{
			return 0;
		}
	}

	public function getActionComponentById($id)
	{
		return ActionsComponent::find($id);
	}

	public function getDataMyPoa()
	{
		$data = [
			'pm' => Auth::user()->profile->position->direction->actionLines()->wherePivot("status", 1)->get(),
			'inf' => Auth::user()->profile->position->direction->infIndicators()->wherePivot("status", 1)->get()
		];
		return $data;
	}

	/*******************************************************************************************************************
     * *
     * * Functions for new Planning 
     * *
     * ****************************************************************************************************************/

	function storeArea($data){
		$area = new MirAreas;
		$area->nombre = $data->name;
		$area->descripcion = $data->description;
		$area->direccion_id = $data->direction_id;
		$area->programa_id = $data->program;
		$area->periodo = 2021;
		$area->save();
	}
	
	function updateArea($data){
		$area = MirAreas::find($data->area_id);
		$area->nombre = $data->name;
		$area->descripcion = $data->description;
		$area->programa_id = $data->program;
		$area->save();
	}
	
	function getAreaById($id){
		return MirAreas::find($id);
	}

	function getProgramsMir(){
		return ProgramMir::orderBy("id", "DESC")->get();
	}
	
	function getProgramMir($id){
		return ProgramMir::find($id);
	}
	
	function getObjectivesByDirection($id){
		return PmObjective::where("pm_eje_id", $id)->get();
	}
	
	function storeProgramMir($data){
		$program = new ProgramMir;
		$program->clave = $data->clave;
		$program->nombre = $data->name;
		$program->objetivo_fin = $data->objective_fin;
		$program->objetivo_prop = $data->objective_prop;
		$program->presupuesto = $data->presupuesto;
		$program->save();
	}
	
	function updateProgramMir($data){
		$program = ProgramMir::find($data->id);
		$program->clave = $data->clave;
		$program->nombre = $data->name;
		$program->objetivo_fin = $data->objective_fin;
		$program->objetivo_prop = $data->objective_prop;
		$program->presupuesto = $data->presupuesto;
		$program->save();
	}

	function getComponentById($id){
		return MirComponent::find($id);
	}

	/*eliminar area */
	public function deleteAreaMir($data)
	{

		$area = MirAreas::find($data->area);
		foreach($area->components as $program)
		{
			if(!empty($program->pm_action_lines))
			{
				foreach ($program->pm_action_lines as $key => $actionline) {
					$program->pm_action_lines()->detach($actionline->id);
				}
			}
			
			if(!empty($program->inf_action_lines))
			{
				foreach ($program->inf_action_lines as $key => $actionline) {
					$program->inf_action_lines()->detach($actionline->id);
				}
			}
			
			if(!empty($program->sche_metas))
			{
				foreach ($program->sche_metas as $key => $actionline) {
					$program->sche_metas()->detach($actionline->id);
				}
			}
	
			if(count($program->actions) > 0)
			{
				foreach ($program->actions as $key => $action) {
					//Eliminar lineas de Accion asignadas a la acción
					if(!empty($action->pm_action_lines))
					{
						foreach ($action->pm_action_lines as $key => $actionline) {
							$action->pm_action_lines()->detach($actionline->id);
						}
					}
					
					if(!empty($action->inf_action_lines))
					{
						foreach ($action->inf_action_lines as $key => $actionline) {
							$action->inf_action_lines()->detach($actionline->id);
						}
					}
					
					if(!empty($action->sche_metas))
					{
						foreach ($action->sche_metas as $key => $actionline) {
							$action->sche_metas()->detach($actionline->id);
						}
					}
					
					if(!empty($action->mir_idicators))
					{
						foreach ($action->mir_idicators as $key => $actionline) {
							$action->mir_idicators()->detach($actionline->id);
						}
					}
					//Eliminar la acción
					$action->delete();
				}
			}
	
			try {
				$program->delete();
			} catch (\Throwable $th) {
				//no hacer nada seguir el bucle
			}


		}

		if($area->delete())
		{
			return 1;
		}

		return false;
		
	}
	/*fin eliminar area*/

	public function getActionMirById($id)
	{
		return ActionsComponent::find($id);
	}
	
}