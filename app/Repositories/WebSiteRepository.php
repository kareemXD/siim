<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;
use DB;
use Storage;
//Models
use App\User;
use App\Models\Notices;

//Services
use Illuminate\Support\Facades\Hash;

class WebSiteRepository
{
    public function getAllNews()
    {
        return Notices::all();        
    }
    
    public function storeNew($data)
    {   
        DB::connection("web-page")->beginTransaction();
        try {
            $last_id = DB::connection('web-page')->table('noticias')->orderBy('id', 'desc')->first();
            if($last_id)
            {
                $id = $last_id->id;
                $id = (int)$id;
                $id = $id+1;
            }
            else 
            {
               $id = 1;     
            }
            $new = new Notices;
            $new->titulo = '@lang("news.new-'.$id.'.title")';
            $new->descripcioncorta = '@lang("news.new-'.$id.'.short-description")';
            $new->descripcionencabezado = '@lang("news.new-'.$id.'.header-description")';
            $new->descripcioncompleta = '@lang("news.new-'.$id.'.complete-description")';
            $new->save();

            //Inserta en el archivo de lenguaje de español de la pagina web principal
            $file = __DIR__."../../../../pagina-web/resources/lang/es/news.php";
            // $time = Notices::find($id);
          
            setlocale(LC_ALL, 'es_ES');
            $time = $new->created_at;   
            $time->format("F");
            $mes = $time->formatLocalized('%B');
            $texto = "    'new-".$id."' => [
            'title' => '".$data->titulo."',
            'short-description' => '".$data->descripcionCorta."',
            'complete-description' => '".$data->descripcionCompleta."',
            'header-description' => '".$data->descripcionEncabezado."',
            'date' => 'Fecha de Publicación: ".$time->format("d")." de ".$mes." del ".$time->format("Y").".',
        ], \n";

            $fc = fopen($file, "r");
            while (!feof($fc)) 
            {
                $buffer = fgets($fc, 4096);
                $lines[] = $buffer;
            }
            
            fclose($fc);

            $f = fopen($file, "w") or die("No se puede abrir el archivo");

            $lineCount = count($lines);
            for ($i = 0; $i < $lineCount- 1; $i++) {
                fwrite($f, $lines[$i]);
            }
            fwrite($f, $texto );

            fwrite($f, $lines[$lineCount-1]);
            fclose($f);

            // Inserta en el archivo de lenguaje de ingles
            $file2 = __DIR__."../../../../pagina-web/resources/lang/en/news.php";
            setlocale(LC_ALL, 'es_ES');
            $time = $new->created_at;   
            $time->format("F");
            $mes = $time->formatLocalized('%B');
            $texto = "    'new-".$id."' => [
            'title' => '".$data->title."',
            'short-description' => '".$data->shortDescription."',
            'complete-description' => '".$data->completeDescription."',
            'header-description' => '".$data->headerDescription."',
            'date' => 'Fecha de Publicación: ".$time->format("d")." de ".$mes." del ".$time->format("Y").".',
        ], \n";

            $fc2 = fopen($file2, "r");
            while (!feof($fc2)) 
            {
                $buffer2 = fgets($fc2, 4096);
                $lines2[] = $buffer2;
            }
            
            fclose($fc2);

            $f2 = fopen($file2, "w") or die("No se puede abrir el archivo");

            $lineCount2 = count($lines2);
            for ($i = 0; $i < $lineCount2- 1; $i++) {
                fwrite($f2, $lines2[$i]);
            }
            fwrite($f2, $texto );

            fwrite($f2, $lines2[$lineCount2-1]);
            fclose($f2);
            
        } catch (Exception $e) {
            DB::connection("web-page")->rollback();
        }
        DB::connection("web-page")->commit();
    }

    public function update()
    {
       $file_es = include __DIR__."../../../../pagina-web/resources/lang/es/news.php";
       $file_en = include __DIR__."../../../../pagina-web/resources/lang/en/news.php";
    }
}