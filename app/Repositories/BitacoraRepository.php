<?php
namespace App\Repositories;

use Carbon\Carbon;
use Auth;

//Models
use App\User;
use App\Models\Bitacora;

class BitacoraRepository
{
	public function insertAction($data)
	{
		$report = new Bitacora;

		$report->titulo = $data["title"];
		$report->action = $data["action"];
		$report->user_id = $data["user"];
		$report->direccion_id = $data["direction"];
		$report->tipo = $data["type"];

		$report->save();
	}
}