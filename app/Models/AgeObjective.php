<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgeObjective extends Model
{
    protected $table = "age_objetivos";

    public function normative()
    {
        return $this->belongsTo('App\Models\Normative', 'normativa_id');
    }
    
    public function metas()
    {
        return $this->hasMany('App\Models\AgeMeta', 'age_objetivo_id');
    }

    public function getLaTotal()
    {
        $total = 0;
        $total += count($this->metas);
        return $total;
    }

    public function getLaWorked($trimester = null)
    {
        $actionsLine = $this->metas;

        $total = 0;
        foreach ($actionsLine as $key => $line) 
        {
            $total_actions = 0;

            foreach ($line->actions as $key => $action_line) {
                if($action_line->program->period == config('implan.periods.previous'))
                {
                    $total_actions++;
                }
            }
            if($total_actions > 0)
            {
                // $total_actions = count($line->actions);
                $fraction = 1 / $total_actions;
                // switch ($trimester) {
                //     case '1':
                //         $actions = $line->actions()->where("trimestre_1", ">", 0)->count();
                //         break;
                //     case '2':
                //         $actions = $line->actions()->where("trimestre_2", ">", 0)->count();
                //         break;
                //     case '3':
                //         $actions = $line->actions()->where("trimestre_3", ">", 0)->count();
                //         break;
                //     case '4':
                //         $actions = $line->actions()->where("trimestre_4", ">", 0)->count();
                //         break;
                //     default:
                //         $actions = count($line->actions);
                //         break;
                // }
                $actions = 0;
                switch ($trimester) {
                    case '1':
                        $actions_line = $line->actions()->where("trimestre_1", ">", 0)->get();
                        foreach ($actions_line as $key => $action_line) {
                            if($action_line->program->period == config('implan.periods.previous'))
                            {
                                $actions++;
                            }
                        }
                        break;
                    case '2':
                        $actions_line = $line->actions()->where("trimestre_2", ">", 0)->get();
                        foreach ($actions_line as $key => $action_line) {
                            if($action_line->program->period == config('implan.periods.previous'))
                            {
                                $actions++;
                            }
                        }
                        break;
                    case '3':
                        $actions_line = $line->actions()->where("trimestre_3", ">", 0)->get();
                        foreach ($actions_line as $key => $action_line) {
                            if($action_line->program->period == config('implan.periods.previous'))
                            {
                                $actions++;
                            }
                        }
                        break;
                    case '4':
                        $actions_line = $line->actions()->where("trimestre_4", ">", 0)->get();
                        foreach ($actions_line as $key => $action_line) {
                            if($action_line->program->period == config('implan.periods.previous'))
                            {
                                $actions++;
                            }
                        }
                        break;
                    default:
                        $actions = $total_actions;
                        break;
                }
                $total += $actions * $fraction;
            }
        }

        return round($total,2);
    }

    public function getLaFinished($trimester)
    {
        $total = 0;
        $total_actions = 0;
        $finished = 0;
        foreach ($this->metas as $line) 
        {
            $total_actions = 0;

            foreach ($line->actions as $key => $action_line) {
                if($action_line->program->period == config('implan.periods.previous'))
                {
                    $total_actions++;
                }
            }
            if($total_actions > 0)
            {
                // $total_actions = count($line->actions);
                $fraction = 1 / $total_actions;
                switch ($trimester) {
                    case '1':
                        foreach ($line->actions as $action) {
                            if($action->trimestre_1 > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                if((($action->trimestre_1_r * 100 ) / $action->trimestre_1) >= 100)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                    case '2':
                        foreach ($line->actions as $action) {
                            if($action->trimestre_2 > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                if((($action->trimestre_2_r * 100 ) / $action->trimestre_2) >= 100)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                    case '3':
                        foreach ($line->actions as $action) {
                            if($action->trimestre_3 > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                if((($action->trimestre_3_p * 100 ) / $action->trimestre_3) >= 100)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                    case '4':
                        foreach ($line->actions as $action) {
                            if($action->trimestre_4 > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                if((($action->trimestre_4_r * 100 ) / $action->trimestre_4) >= 100)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                        
                    default:
                        foreach ($line->actions as $action) {
                            if($action->meta_anual > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                if((($action->meta_anual_r * 100 ) / $action->meta_anual) >= 100)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                }
                $total += $finished * $fraction;
                $finished = 0;
            }
        }
        return round($total,2);
    }

    public function getLaProcess($trimester)
    {
        $total = 0;
        $total_actions = 0;
        $finished = 0;
        foreach ($this->metas as $line) 
        {
            $total_actions = 0;

            foreach ($line->actions as $key => $action_line) {
                if($action_line->program->period == config('implan.periods.previous'))
                {
                    $total_actions++;
                }
            }
            if($total_actions > 0)
            {
                // $total_actions = count($line->actions);
                $fraction = 1 / $total_actions;
                switch ($trimester) {
                    case '1':
                        foreach ($line->actions as $action) {
                            if($action->trimestre_1 > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                $advance = (($action->trimestre_1_r * 100 ) / $action->trimestre_1);
                                if($advance  < 100 && $advance > 0)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                    case '2':
                        foreach ($line->actions as $action) {
                            if($action->trimestre_2 > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                $advance = (($action->trimestre_2_r * 100 ) / $action->trimestre_2);
                                if($advance  < 100 && $advance > 0)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                    case '3':
                        foreach ($line->actions as $action) {
                            if($action->trimestre_3 > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                $advance = (($action->trimestre_3_p * 100 ) / $action->trimestre_3);
                                if($advance  < 100 && $advance > 0)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                    case '4':
                        foreach ($line->actions as $action) {
                            if($action->trimestre_4 > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                $advance = (($action->trimestre_4_r * 100 ) / $action->trimestre_4);
                                if($advance  < 100 && $advance > 0)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                        
                    default:
                        foreach ($line->actions as $action) {
                            if($action->meta_anual > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                $advance = (($action->meta_anual_r * 100 ) / $action->meta_anual);
                                if( $advance < 100 && $advance > 0)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                }
                $total += $finished * $fraction;
                $finished = 0;
            }
        }
        return round($total,2);
    }

    public function getLaNull($trimester)
    {
        $total = 0;
        $total_actions = 0;
        $finished = 0;
        foreach ($this->metas as $line) 
        {
            $total_actions = 0;

            foreach ($line->actions as $key => $action_line) {
                if($action_line->program->period == config('implan.periods.previous'))
                {
                    $total_actions++;
                }
            }
            if($total_actions > 0)
            {
                // $total_actions = count($line->actions);
                $fraction = 1 / $total_actions;
                switch ($trimester) {
                    case '1':
                        foreach ($line->actions as $action) {
                            if($action->trimestre_1 > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                if((($action->trimestre_1_r * 100 ) / $action->trimestre_1) == 0)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                    case '2':
                        foreach ($line->actions as $action) {
                            if($action->trimestre_2 > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                if((($action->trimestre_2_r * 100 ) / $action->trimestre_2) == 0)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                    case '3':
                        foreach ($line->actions as $action) {
                            if($action->trimestre_3 > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                if((($action->trimestre_3_p * 100 ) / $action->trimestre_3) == 0)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                    case '4':
                        foreach ($line->actions as $action) {
                            if($action->trimestre_4 > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                if((($action->trimestre_4_r * 100 ) / $action->trimestre_4) == 0)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                        
                    default:
                        foreach ($line->actions as $action) {
                            if($action->meta_anual > 0 && $action->program->period == config('implan.periods.previous'))
                            {
                                if((($action->meta_anual_r * 100 ) / $action->meta_anual) == 0)
                                {
                                    $finished++;
                                }
                            }
                        }
                        break;
                }
                $total += $finished * $fraction;
                $finished = 0;
            }
        }
        return round($total,2);
    }
}
