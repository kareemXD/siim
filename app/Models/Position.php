<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = "puestos";

    public function direction()
    {
        return $this->belongsTo('App\Models\Direction', 'direccion_id');
    }

    public function profile()
    {
        return $this->hasMany('App\Models\Profile', 'puesto_id');
    }

    public function offices()
    {
        return $this->belongsToMany('App\Models\Offices', 'puesto_has_oficios', 'puesto_id', 'oficio_id')->withPivot('type');
    }
}
