<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MirIndicatorType extends Model
{
    protected $table = "mir_tipos_indicador";
}
