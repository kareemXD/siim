<?php

namespace App\Models\NewPlanning;

use Illuminate\Database\Eloquent\Model;

class MirComponent extends Model
{
    protected $table = "mir_componentes";

    public function actions()
    {
        return $this->hasMany('App\Models\NewPlanning\ActionsComponent', 'componente_id');
    }

    public function direction()
    {
        return $this->belongsTo('App\Models\Direction', 'direccion_id');
    }

    public function responsable()
    {
        return $this->belongsTo('App\Models\Position', 'puesto_id');
    }
    
    public function program()
    {
        return $this->belongsTo('App\Models\NewPlanning\ProgramMir', 'programa_id');
    }

    public function pm_action_lines()
    {
        return $this->belongsToMany('App\Models\PmActionLine', 'pm_linea_accion_has_mir_componente', 'mir_componente_id', 'pm_lineas_accion_id');
    }
    
    public function inf_action_lines()
    {
        return $this->belongsToMany('App\Models\InfIndicator', 'inf_indicador_has_mir_componente', 'mir_componente_id', 'inf_indicadores_id');
    }
    
    public function sche_metas()
    {
        return $this->belongsToMany('App\Models\AgeMeta', 'age_meta_has_mir_componente', 'mir_componente_id', 'age_metas_id');
    }
}
