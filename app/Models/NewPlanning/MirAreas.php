<?php

namespace App\Models\NewPlanning;

use Illuminate\Database\Eloquent\Model;

class MirAreas extends Model
{
    protected $table = "mir_areas";

    public function components()
    {
        return $this->hasMany('App\Models\NewPlanning\MirComponent', 'area_id');
    }

    public function direction()
    {
        return $this->belongsTo('App\Models\Direction', 'direccion_id');
    }

    public function program()
    {
        return $this->belongsTo('App\Models\NewPlanning\ProgramMir', 'programa_id');
    }
}
