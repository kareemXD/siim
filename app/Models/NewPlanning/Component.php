<?php

namespace App\Models\NewPlanning;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    protected $table = "componentes";

    public function actions()
    {
        return $this->hasMany('App\Models\NewPlanning\ActionsComponent', 'componente_id');
    }

    public function direction()
    {
        return $this->belongsTo('App\Models\Direction', 'direccion_id');
    }
    
    public function program()
    {
        return $this->belongsTo('App\Models\NewPlanning\ProgramMir', 'programa_id');
    }

    public function getAdvance()
    {
        $action = Component::find($this->id);
        if($action->meta_anual > 0)
        {
            $meta =  round(($action->meta_anual_r * 100 ) / $action->meta_anual, 2);
            if($meta >=100){
                return 100;
            }
            return $meta;
        }

        return 0;
    }
    
    public function getAdvanceColor()
    {
        $action = Component::find($this->id);
        $color = "bg-red";
        if($action->meta_anual > 0)
        {
            $advance = ($action->meta_anual_r * 100 ) / $action->meta_anual;
            if($action->orientacion == 0)
            {
                if ($advance > 0 && $advance < 100) {
                    $color = "bg-yellow";
                } else if ($advance >= 100 ) {
                    $color = "bg-green";
                }
            }
            else if($action->orientacion == 1)
            {
                if ($advance < 100) {
                    $color = "bg-green";
                }
            }
        }

        return $color;
    }

    public function getAdvanceTrimestre()
    {
        $action = Component::find($this->id);
        if($action->trimestre_1 > 0)
        {
            $avance = (floatval($action->trimestre_1_r) / floatval($action->trimestre_1)) *100;
            $meta =  round($avance, 2);
            if($avance >=100){
                return 100;
            }
            return $meta;
        }

        return 0;
    }

    public function getAdvanceColorTrimestre()
    {
        $action = Component::find($this->id);
        $color = "bg-red";
        if($action->trimestre_1 > 0)
        {
            $advance = (floatval($action->trimestre_1_r)) / floatval($action->trimestre_1) * 100;
            if($action->orientacion == 0)
            {
                if ($advance > 0 && $advance < 100) {
                    $color = "bg-yellow";
                } else if ($advance >= 100 ) {
                    $color = "bg-green";
                }
            }
            else if($action->orientacion == 1)
            {
                if ($advance < 100) {
                    $color = "bg-green";
                }
            }
        }

        return $color;
    }

}
