<?php

namespace App\Models\NewPlanning;

use Illuminate\Database\Eloquent\Model;

class ActionsComponent extends Model
{
    protected $table = "acciones_componente";

    public function pm_action_lines()
    {
        return $this->belongsToMany('App\Models\PmActionLine', 'pm_linea_accion_has_accion_componente', 'accion_componente_id', 'pm_lineas_accion_id');
    }
    
    public function inf_action_lines()
    {
        return $this->belongsToMany('App\Models\InfIndicator', 'inf_indicador_has_accion_componente', 'accion_componente_id', 'inf_indicadores_id');
    }
    
    public function sche_metas()
    {
        return $this->belongsToMany('App\Models\AgeMeta', 'age_meta_has_accion_componente', 'accion_componente_id', 'age_metas_id');
    }
    public function responsable()
    {
        return $this->belongsTo('App\Models\Position', 'puesto_id');
    }


    public function getAdvance()
    {
        $action = ActionsComponent::find($this->id);
        if($action->meta_anual > 0)
        {
            $meta =  round(($action->meta_anual_r * 100 ) / $action->meta_anual, 2);
            if($meta >=100){
                return 100;
            }
            return $meta;
        }

        return 0;
    }
    
    public function getAdvanceColor()
    {
        $action = ActionsComponent::find($this->id);
        $color = "bg-red";
        if($action->meta_anual > 0)
        {
            $advance = ($action->meta_anual_r / $action->meta_anual)* 100;
            if($action->orientacion == 0)
            {
                if ($advance > 0 && $advance < 100) {
                    $color = "bg-yellow";
                } else if ($advance >= 100 ) {
                    $color = "bg-green";
                }
            }
            else if($action->orientacion == 1)
            {
                if ($advance < 100) {
                    $color = "bg-green";
                }
            }
        }

        return $color;
    }

    public function getAdvanceTrimestre()
    {
        $action = ActionsComponent::find($this->id);
        if($action->trimestre_1 > 0)
        {
            $avance = (floatval($action->trimestre_1_r) / floatval($action->trimestre_1)) *100;
            $meta =  round($avance, 2);
            if($avance >=100){
                return 100;
            }
            return $meta;
        }

        return 0;
    }

    public function getAdvanceColorTrimestre()
    {
        $action = ActionsComponent::find($this->id);
        $color = "bg-red";
        if($action->trimestre_1 > 0)
        {
            $advance = (floatval($action->trimestre_1_r)) / floatval($action->trimestre_1) * 100;
            if($action->orientacion == 0)
            {
                if ($advance > 0 && $advance < 100) {
                    $color = "bg-yellow";
                } else if ($advance >= 100 ) {
                    $color = "bg-green";
                }
            }
            else if($action->orientacion == 1)
            {
                if ($advance < 100) {
                    $color = "bg-green";
                }
            }
        }

        return $color;
    }
}
