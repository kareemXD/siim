<?php

namespace App\Models\NewPlanning;

use Illuminate\Database\Eloquent\Model;

class ProgramMir extends Model
{
    protected $table = "programas_mir";

    public function eje()
    {
        return $this->belongsTo('App\Models\PmAxe', 'id_eje');
    }
}
