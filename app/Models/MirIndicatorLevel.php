<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MirIndicatorLevel extends Model
{
    protected $table = "mir_indicador_niveles";
}
