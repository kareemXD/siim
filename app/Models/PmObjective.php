<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmObjective extends Model
{
    protected $table = "pm_objetivos";

    public function axe()
    {
        return $this->belongsTo('App\Models\PmAxe', 'pm_eje_id');
    }

    public function strategies()
    {
        return $this->hasMany('App\Models\PmStrategy', 'pm_objetivo_id');
    }
}
