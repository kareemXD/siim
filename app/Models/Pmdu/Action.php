<?php

namespace App\Models\Pmdu;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $connection = 'web-page';
    protected $table = "acciones";
}
