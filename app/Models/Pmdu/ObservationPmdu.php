<?php

namespace App\Models\Pmdu;

use Illuminate\Database\Eloquent\Model;

class ObservationPmdu extends Model
{
    protected $table = "observaciones_pmdu";
    protected $connection = 'web-page';

    public function sections()
    {
        return $this->hasMany('App\Models\Pmdu\PmduSection', 'id_observacion');
    }
}
