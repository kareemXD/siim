<?php

namespace App\Models\Pmdu;

use Illuminate\Database\Eloquent\Model;

class VisitPmdu extends Model
{
    protected $connection = 'web-page';
    protected $table = "visitas_pmdu";
}
