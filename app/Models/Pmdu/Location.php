<?php

namespace App\Models\Pmdu;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $connection = 'web-page';
    protected $table = "localidades";
}
