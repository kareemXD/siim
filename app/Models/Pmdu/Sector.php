<?php

namespace App\Models\Pmdu;

use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{
    protected $connection = 'web-page';
    protected $table = "sectores";
}
