<?php

namespace App\Models\Pmdu;

use Illuminate\Database\Eloquent\Model;

class Need extends Model
{
    protected $connection = 'web-page';
    protected $table = "necesidades";
}
