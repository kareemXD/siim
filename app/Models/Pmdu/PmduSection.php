<?php

namespace App\Models\Pmdu;

use Illuminate\Database\Eloquent\Model;

class PmduSection extends Model
{
    protected $connection = 'web-page';
    protected $table = "observaciones_secciones";
    public $timestamps = false;
    public $incrementing = false;
}
