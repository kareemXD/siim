<?php

namespace App\Models\Oromapas;

use Illuminate\Database\Eloquent\Model;

class padron extends Model
{
    protected $connection = "cartoWeb";
    protected $table = "oromapas_padron";
    public $timestamps = false;
}
