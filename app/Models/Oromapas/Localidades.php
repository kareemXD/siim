<?php

namespace App\Models\Oromapas;

use Illuminate\Database\Eloquent\Model;

class Localidades extends Model
{
    protected $connection = "cartoWeb";
    protected $table = "oromapas_localidades";
    public $timestamps = false;
    protected $fillable = [ 'nombre'];
}
