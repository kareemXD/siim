<?php

namespace App\Models\Oromapas;

use Illuminate\Database\Eloquent\Model;

class sanitaria extends Model
{
    protected $connection = "cartoWeb";
    protected $table = "oromapas_sanitaria";
    public $timestamps = false;
    protected $fillable = [ 'layer' ,'pendiente', 'geodata'];
}
