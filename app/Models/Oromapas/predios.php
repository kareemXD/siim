<?php

namespace App\Models\Oromapas;

use Illuminate\Database\Eloquent\Model;

class predios extends Model
{
    protected $connection = "cartoWeb";
    protected $table = "oromapas_predios";
    public $timestamps = false;
    protected $fillable = ['folio',
                            'fecha' ,
                            'cuenta',
                            'manzana' ,
                            'predio'  ,
                            'propietario',
                            'direccion',
                            'no_ext',
                            'no_int' ,
                            'colonia',
                            'calle1',
                            'calle2',
                            'giro_comercial' ,
                            'nom_comer' ,
                            'no_tomas' ,
                            'medidor',
                            'serie_med',
                            'lec_actual' ,
                            'construcc',
                            'deshabit',
                            'tipo_zona' ,
                            'delegacion' ,
                            'observacion',
                            'geodata'];
}
