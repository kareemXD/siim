<?php

namespace App\Models\Oromapas;

use Illuminate\Database\Eloquent\Model;

class poligonos extends Model
{
    protected $connection = "cartoWeb";
    protected $table = "oromapas_poligonos";
    public $timestamps = false;
    protected $fillable = [ 'cvgeo', 'cve_ent', 'cve_mun','cve_loc','nomgeo','ambito','geodata'];
}
