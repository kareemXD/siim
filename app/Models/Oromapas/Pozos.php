<?php

namespace App\Models\Oromapas;

use Illuminate\Database\Eloquent\Model;

class Pozos extends Model
{
    protected $connection = "cartoWeb";
    protected $table = "oromapas_pozos_test";
    public $timestamps = false;
    protected $fillable = ['nivel_raz','nivel_arras','diferencia','geodata'];
}
