<?php

namespace App\Models\Oromapas;

use Illuminate\Database\Eloquent\Model;

class Oromapas extends Model
{
    protected $connection = "cartoWeb";
    protected $table = "oromapas_hidraulica";
    public $timestamps = false;
    protected $fillable = [ 'layer' ,'pendiente', 'geodata'];
}
