<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfIndicatorRejected extends Model
{
    protected $table = "inf_indicadores_rechazados";
}
