<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfSchedule extends Model
{
    protected $table = "inf_agendas";

    public function normative()
    {
        return $this->belongsTo('App\Models\Normative', 'normativa_id');
    }
    
    public function axes()
    {
        return $this->hasMany('App\Models\InfAxe', 'inf_agenda_id');
    }
}
