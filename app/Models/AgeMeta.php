<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//Models
use App\Models\Position;

class AgeMeta extends Model
{
    protected $table = "age_metas";

    public function objective()
    {
        return $this->belongsTo('App\Models\AgeObjective', 'age_objetivo_id');
    }

    public function directions()
    {
        return $this->belongsToMany('App\Models\Direction', 'age_meta_has_direccion', 'age_metas_id', 'direcciones_id')->withPivot('estatus', "period");
    }

    public function actions()
    {
        return $this->belongsToMany('App\Models\ActionPoa', 'age_meta_has_acciones_poa', 'age_metas_id', 'acciones_poa_id');
    }

    public function getResposable($id)
    {
        return Position::find($id);
    }
}
