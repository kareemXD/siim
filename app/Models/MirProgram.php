<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MirProgram extends Model
{
    protected $table = "mir_programas";

    public function normative()
    {
        return $this->belongsTo('App\Models\Normative', 'normativa_id');
    }
    
    public function indicators()
    {
        return $this->hasMany('App\Models\MirIndicator', 'mir_programa_id');
    }
}
