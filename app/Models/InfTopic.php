<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfTopic extends Model
{
    protected $table = "inf_temas";

    public function axe()
    {
        return $this->belongsTo('App\Models\InfAxe', 'inf_eje_id');
    }

    public function indicators()
    {
        return $this->hasMany('App\Models\InfIndicator', 'inf_tema_id');
    }

    public function scheduledByTri($trimester)
    {
        $actionsLine = $this->indicators;

        $total = 0;
        foreach ($actionsLine as $key => $line) 
        {
            $total_actions = 0;

            foreach ($line->actions as $key => $action_line) {
                if($action_line->program->period == config('implan.periods.previous'))
                {
                    $total_actions++;
                }
            }
            if($total_actions > 0)
            {
            // if(count($line->actions) > 0)
            // {
                // $total_actions = count($line->actions);
                $fraction = 1 / $total_actions;
                // switch ($trimester) {
                //     case '1':
                //         $actions = $line->actions()->where("trimestre_1", ">", 0)->count();
                //         break;
                //     case '2':
                //         $actions = $line->actions()->where("trimestre_2", ">", 0)->count();
                //         break;
                //     case '3':
                //         $actions = $line->actions()->where("trimestre_3", ">", 0)->count();
                //         break;
                //     case '4':
                //         $actions = $line->actions()->where("trimestre_4", ">", 0)->count();
                //         break;
                //     default:
                //         $actions = count($line->actions);
                //         break;
                // }
                $actions = 0;
                switch ($trimester) {
                    case '1':
                        $actions_line = $line->actions()->where("trimestre_1", ">", 0)->get();
                        foreach ($actions_line as $key => $action_line) {
                            if($action_line->program->period == config('implan.periods.previous'))
                            {
                                $actions++;
                            }
                        }
                        break;
                    case '2':
                        $actions_line = $line->actions()->where("trimestre_2", ">", 0)->get();
                        foreach ($actions_line as $key => $action_line) {
                            if($action_line->program->period == config('implan.periods.previous'))
                            {
                                $actions++;
                            }
                        }
                        break;
                    case '3':
                        $actions_line = $line->actions()->where("trimestre_3", ">", 0)->get();
                        foreach ($actions_line as $key => $action_line) {
                            if($action_line->program->period == config('implan.periods.previous'))
                            {
                                $actions++;
                            }
                        }
                        break;
                    case '4':
                        $actions_line = $line->actions()->where("trimestre_4", ">", 0)->get();
                        foreach ($actions_line as $key => $action_line) {
                            if($action_line->program->period == config('implan.periods.previous'))
                            {
                                $actions++;
                            }
                        }
                        break;
                    default:
                        $actions = $total_actions;
                        break;
                }
                $total += $actions * $fraction;
            }
        }
        return $total;
    }
}
