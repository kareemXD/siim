<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//Models
use App\Models\Position;

class InfIndicator extends Model
{
    protected $table = "inf_indicadores";

    public function topic()
    {
        return $this->belongsTo('App\Models\InfTopic', 'inf_tema_id');
    }

    public function directions()
    {
        return $this->belongsToMany('App\Models\Direction', 'inf_indicador_has_direcciones', 'inf_indicador_id', 'direccion_id')->withPivot("status", "period");
    }

    public function actions()
    {
        return $this->belongsToMany('App\Models\ActionPoa', 'inf_indicador_has_accion_poa', 'inf_indicadores_id', 'acciones_poa_id');
    }

    public function getResposable($id)
    {
        return Position::find($id);
    }
}
