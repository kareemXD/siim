<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DirectionMetadata extends Model
{
    protected $table = "metadata_direction";
}
