<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmLineActionHasDirection extends Model
{
    protected $table = "direcciones_has_pm_linea_accion";
}
