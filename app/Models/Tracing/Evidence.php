<?php

namespace App\Models\Tracing;

use Illuminate\Database\Eloquent\Model;

class Evidence extends Model
{
    protected $table = "evidencias";

    public function activity()
    {
        return $this->belongsTo('App\Models\Tracing\MonthlyActivity', 'actividad_mesual_id');
    }
}
