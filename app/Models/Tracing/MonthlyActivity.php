<?php

namespace App\Models\Tracing;

use Illuminate\Database\Eloquent\Model;

class MonthlyActivity extends Model
{
    protected $table = "actividades_mesuales";

    public function locations()
    {
        return $this->belongsToMany('App\Models\Location', 'localidades_has_actividades_mesuales', 'actividades_mesuales_id', 'localidades_id');
    }

    public function evidences()
    {
        return $this->hasMany('App\Models\Tracing\Evidence', 'actividad_mesual_id');
    }
}
