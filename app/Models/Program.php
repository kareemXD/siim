<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    protected $table = "programas";

    public function actions()
    {
        return $this->hasMany('App\Models\ActionPoa', 'programas_id');
    }

    public function direction()
    {
        return $this->belongsTo('App\Models\Direction', 'direccion_id');
    }
}
