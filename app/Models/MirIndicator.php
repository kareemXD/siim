<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//Models
use App\Models\Position;

class MirIndicator extends Model
{
    protected $table = "mir_indicadores";

    public function program()
    {
        return $this->belongsTo('App\Models\MirProgram', 'mir_programa_id');
    }
    
    public function level()
    {
        return $this->belongsTo('App\Models\MirIndicatorLevel', 'mir_indicador_nivel_id');
    }
    
    public function type()
    {
        return $this->belongsTo('App\Models\MirIndicatorType', 'mir_tipo_indicador_id');
    }

    public function directions()
    {
        return $this->belongsToMany('App\Models\Direction', 'direcciones_has_mir_indicadores', 'mir_indicadores_id', 'direcciones_id')->withPivot('status', "period");
    }

    public function actions()
    {
        return $this->belongsToMany('App\Models\ActionPoa', 'mir_indicador_has_acciones_poa', 'mir_indicadores_id', 'acciones_poa_id');
    }

    public function getResposable($id)
    {
        return Position::find($id);
    }
}
