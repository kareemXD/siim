<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActionPoa extends Model
{
    protected $table = "acciones_poa";

    public function subactions()
    {
        return $this->hasMany('App\Models\SubactionPoa', 'accion_poa_id');
    }
    
    public function program()
    {
        return $this->belongsTo('App\Models\Program', 'programas_id');
    }

    public function pm_action_lines()
    {
        return $this->belongsToMany('App\Models\PmActionLine', 'pm_linea_accion_has_accion_poa', 'acciones_poa_id', 'pm_lineas_accion_id');
    }
    
    public function inf_action_lines()
    {
        return $this->belongsToMany('App\Models\InfIndicator', 'inf_indicador_has_accion_poa', 'acciones_poa_id', 'inf_indicadores_id');
    }
    
    public function sche_metas()
    {
        return $this->belongsToMany('App\Models\AgeMeta', 'age_meta_has_acciones_poa', 'acciones_poa_id', 'age_metas_id');
    }
    public function mir_idicators()
    {
        return $this->belongsToMany('App\Models\MirIndicator', 'mir_indicador_has_acciones_poa', 'acciones_poa_id', 'mir_indicadores_id');
    }
    
    public function responsable()
    {
        return $this->belongsTo('App\Models\Position', 'puesto_id');
    }

    public function getAdvance()
    {
        $action = ActionPoa::find($this->id);
        if($action->meta_anual > 0)
        {
            return round(($action->meta_anual_r * 100 ) / $action->meta_anual, 2);
        }

        return 0;
    }
    
    public function getAdvanceColor()
    {
        $action = ActionPoa::find($this->id);
        $color = "bg-red";
        if($action->meta_anual > 0)
        {
            $advance = ($action->meta_anual_r * 100 ) / $action->meta_anual;
            if($action->orientacion == 0)
            {
                if ($advance > 0 && $advance < 100) {
                    $color = "bg-yellow";
                } else if ($advance >= 100 ) {
                    $color = "bg-green";
                }
            }
            else if($action->orientacion == 1)
            {
                if ($advance < 100) {
                    $color = "bg-green";
                }
            }
        }

        return $color;
    }
}
