<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offices extends Model
{
    protected $table = "oficios";

    public function responsable()
    {        
        return $this->belongsToMany('App\Models\Position', 'puesto_has_oficios', 'oficio_id', 'puesto_id')->withPivot('type');   
    }
    
    public function dad()
    {
        return $this->belongsTo('App\Models\Offices', 'id_padre');
    }

    public function offices()
    {
        return $this->hasMany('App\Models\Offices', 'id_padre', 'id' );
    }

    public function response()
    {
        return $this->hasOne('App\Models\Offices', 'id_padre', 'id' );
    }
}
