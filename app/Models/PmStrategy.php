<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmStrategy extends Model
{
    protected $table = "pm_estrategias";

    public function objective()
    {
        return $this->belongsTo('App\Models\PmObjective', 'pm_objetivo_id');
    }

    public function actionLines()
    {
        return $this->hasMany('App\Models\PmActionLine', 'pm_estrategia_id');
    }

    public function scheduled()
    {
        $actionsLine = $this->actionLines;

        $total = 0;
        foreach ($actionsLine as $key => $line) {
            if(count($line->actions) > 0)
            {
                $total++;
            }
        }
        return $total;
    }
    
    public function scheduledByTri($trimester)
    {
        $actionsLine = $this->actionLines;
        $action2 = 0;
        $total = 0;
        foreach ($actionsLine as $key => $line) 
        {
            $total_actions = 0;

            foreach ($line->actions as $key => $action_line) {
                if($action_line->program->period == config('implan.periods.previous'))
                {
                    $total_actions++;
                }
            }
            if($total_actions > 0)
            {
                // dd($total_actions, $line);
                // $actions_line = count($line->actions);
                // $total_actions = count($line->actions);
                $action = 0;
                $fraction = 1 / $total_actions;
                switch ($trimester) {
                    case '1':
                        $actions_line = $line->actions()->where("trimestre_1", ">", 0)->get();
                        foreach ($actions_line as $key => $action_line) {
                            if($action_line->program->period == config('implan.periods.previous'))
                            {
                                $action++;
                            }
                        }
                        break;
                    case '2':
                        $actions_line = $line->actions()->where("trimestre_2", ">", 0)->get();
                        foreach ($actions_line as $key => $action_line) {
                            if($action_line->program->period == config('implan.periods.previous'))
                            {
                                $action++;
                            }
                        }
                        break;
                    case '3':
                        $actions_line = $line->actions()->where("trimestre_3", ">", 0)->get();
                        foreach ($actions_line as $key => $action_line) {
                            if($action_line->program->period == config('implan.periods.previous'))
                            {
                                $action++;
                            }
                        }
                        break;
                    case '4':
                        $actions_line = $line->actions()->where("trimestre_4", ">", 0)->get();
                        foreach ($actions_line as $key => $action_line) {
                            if($action_line->program->period == config('implan.periods.previous'))
                            {
                                $action++;
                            }
                        }
                        break;
                    default:
                        $action = $total_actions;
                        break;
                }
                $total += $action * $fraction;
            }
        }
        return $total;
    }
    
    public function scheduledPercentage()
    {
        $total = count($this->actionLines);
        $scheduled = $this->scheduled();
        $advance = ($scheduled * 100 ) / $total;

        return round($advance,2);
    }

    public function annualAdvancePercentage()
    {
        $actionsLine = $this->actionLines;
        // dd($actionsLine);
        $adv = [];
        $totales = 0;
        foreach ($actionsLine as $line) {
            if(count($line->actions) > 0)
            {
                foreach ($line->actions as  $action) {
                    if($action->program->period == config('implan.periods.previous'))
                    {
                        $adv[] = $action->getAdvance();
                        $totales += 100;
                    }
                }
            }
        }
        $r1 = intval(array_sum($adv));
        $r2 = $totales;
        // dd($r1, $r2);
        // $advance = round(($r1 * 100)/$r2, 2);
        return $r2 == 0 ? 0 : (round(($r1 * 100)/$r2, 2));
    }
}
