<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubactionPoa extends Model
{
    protected $table = "subacciones_poa";

    public function getAdvance()
    {
        return ($this->meta_anual_r * 100 ) / $this->meta_anual;
    }
    
    public function getAdvanceColor()
    {
        $advance = ($this->meta_anual_r * 100 ) / $this->meta_anual;
        $color = "bg-red";
        if($this->orientacion == 0)
        {
            if ($advance > 0 && $advance < 100) {
                $color = "bg-yellow";
            } else if ($advance >= 100 ) {
                $color = "bg-green";
            }
        }
        else if($this->orientacion == 1)
        {
            if ($advance < 100) {
                $color = "bg-green";
            }
        }

        return $color;
    }
}
