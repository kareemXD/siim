<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//Models
use App\Models\Position;

class PmActionLine extends Model
{
    protected $table = "pm_lineas_accion";

    public function strategy()
    {
        return $this->belongsTo('App\Models\PmStrategy', 'pm_estrategia_id');
    }

    public function directions()
    {
        return $this->belongsToMany('App\Models\Direction', 'direcciones_has_pm_linea_accion', 'pm_linea_accion_id', 'direccion_id')->withPivot('status', "period");
    }

    public function actions()
    {
        return $this->belongsToMany('App\Models\ActionPoa', 'pm_linea_accion_has_accion_poa', 'pm_lineas_accion_id', 'acciones_poa_id');
    }

    public function getResposable($id)
    {
        return Position::find($id);
    }

    
}
