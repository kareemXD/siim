<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "perfiles";

    public function position()
    {
        return $this->belongsTo('App\Models\Position', 'puesto_id');
    }
    
    public function user()
    {
        return $this->hasOne('App\User', 'perfil_id');
    }

    public function office()
    {
        return $this->hasMany('App\Offices');
    }
}
