<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table = 'folios';

    public function user()
    {
        return $this->belongsTo('App\User', 'users_id');
    }

    public function priority()
    {
        return $this->belongsTo('App\Models\Priority', 'prioridad_id');
    }
    
    public function status()
    {
        return $this->belongsTo('App\Models\Status', 'estatus_id');
    }
}
