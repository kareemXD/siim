<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PmActionRejected extends Model
{
    protected $table = "pm_lineas_accion_rechazadas";
}
