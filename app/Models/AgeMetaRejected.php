<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgeMetaRejected extends Model
{
    protected $table = "age_metas_rechazadas";
}
