<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use PDF;
use Carbon\Carbon;
//Repositories
//Repositories
use App\Repositories\StadisticsRepository;
use App\Repositories\PlanningRepository;

class StadisticsController extends Controller
{
    private $stadisticsRepository,
            $planningRepository;

    public function __construct(
        StadisticsRepository $stadisticsRepository,
        PlanningRepository $planningRepository
    ){
        $this->stadisticsRepository = $stadisticsRepository;
        $this->planningRepository = $planningRepository;
    }
    
    public function actionsPMD()
    {
        $directions = $this->planningRepository->getAllDirections();
        $axes = $this->planningRepository->getAxes();

        $stadistics = $this->stadisticsRepository->getStadisticsPMD2();
        // dd($stadistics);
        session(["stadistics" => $stadistics]);
        session(["trimester" => null]);
        return view("stadistics.index", compact("directions", "axes", "stadistics"));
    }
    
    public function actionsPMDCustom(Request $request)
    {
        // dd($request);
        $directions = $this->planningRepository->getAllDirections();
        $axes = $this->planningRepository->getAxes();
        if ($request->direction[0] == null && $request->axe[0] == null) {
            $stadistics = $this->stadisticsRepository->getStadisticsPMD2(null, null, $request->trimester);
        }else if ($request->direction[0] != null && $request->axe[0] == null) {
            $stadistics = $this->stadisticsRepository->getStadisticsPMD2($request->direction, null, $request->trimester );
        }else if ($request->direction[0] == null && $request->axe[0] != null) {
            $stadistics = $this->stadisticsRepository->getStadisticsPMD2(null, $request->axe, $request->trimester);
        }else {
            $stadistics = $this->stadisticsRepository->getStadisticsPMD2($request->direction, $request->axe, $request->trimester);
        }
        // dd($stadistics);
        session(["stadistics" => $stadistics]);
        session(["trimester" => $request->trimester]);

        return view("stadistics.index", compact("directions", "axes", "stadistics"));
    }

    public function actionsPMDPDF(Request $request)
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        $this->stadisticsRepository->saveImage($request->image);
        $stadistics = session("stadistics");
        $trimester = session("trimester")."° Trimestre";
        if(!session("trimester"))
        {
            $trimester = "Informe Anual";
        }
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use ($trimester){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor-implan.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Estadisticas Acciones PMD", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(16);
            $pdf->Cell(0, 0, "( ".$trimester." )", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.actions_pmd')->with(compact('stadistics'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');
        $imagen = public_path('assets/graphics/actionsPMD.png');
        PDF::Image($imagen, 15, 140, 335, 216, '', '', '', true, 300, '', false, false, 1, false, false, false);
        // PDF::Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
        PDF::SetTitle("Estadisticas Acciones PMD a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Estadisticas Acciones PMD a ".$date_now->format('d-M-Y').".pdf");
    }
    
    public function actionsPMD2()
    {
        $directions = $this->planningRepository->getAllDirections();
        $axes = $this->planningRepository->getAxes();

        $stadistics = $this->stadisticsRepository->getStadistics();
        // dd($stadistics);
        session(["stadistics_pmd" => $stadistics]);
        session(["trimester_pmd" => null]);
        return view("stadistics.index2", compact("directions", "axes", "stadistics"));
    }
    
    public function actionsPMD2Custom(Request $request)
    {
        // dd($request);
        $directions = $this->planningRepository->getAllDirections();
        $axes = $this->planningRepository->getAxes();
        if ($request->direction[0] == null && $request->axe[0] == null) {
            $stadistics = $this->stadisticsRepository->getStadistics(null, null, $request->trimester);
        }else if ($request->direction[0] != null && $request->axe[0] == null) {
            $stadistics = $this->stadisticsRepository->getStadistics($request->direction, null, $request->trimester );
        }else if ($request->direction[0] == null && $request->axe[0] != null) {
            $stadistics = $this->stadisticsRepository->getStadistics(null, $request->axe, $request->trimester);
        }else {
            $stadistics = $this->stadisticsRepository->getStadistics($request->direction, $request->axe, $request->trimester);
        }
        // dd($stadistics);
        session(["stadistics_pmd" => $stadistics]);
        session(["trimester_pmd" => $request->trimester]);

        return view("stadistics.index2", compact("directions", "axes", "stadistics"));
    }

    public function actionsPMD2PDF(Request $request)
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        $this->stadisticsRepository->saveImage($request->image);
        $stadistics = session("stadistics_pmd");
        $trimester = session("trimester_pmd")."° Trimestre";
        if(!session("trimester"))
        {
            $trimester = "Informe Anual";
        }
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use ($trimester){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor-implan.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Estadisticas Acciones PMD", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(16);
            $pdf->Cell(0, 0, "( ".$trimester." )", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.actions_pmd')->with(compact('stadistics'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');
        $imagen = public_path('assets/graphics/actionsPMD.png');
        PDF::Image($imagen, 15, 140, 335, 216, '', '', '', true, 300, '', false, false, 1, false, false, false);
        // PDF::Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
        PDF::SetTitle("Estadisticas Acciones PMD a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Estadisticas Acciones PMD a ".$date_now->format('d-M-Y').".pdf");
    }
    
    public function actionsADM()
    {
        $directions = $this->planningRepository->getAllDirections();
        $axes = $this->planningRepository->getAxesADM();

        $stadistics = $this->stadisticsRepository->getStadisticsADM();
        // dd($stadistics);
        session(["stadistics_adm" => $stadistics]);
        session(["trimester_adm" => null]);
        return view("stadistics.actions_adm", compact("directions", "axes", "stadistics"));
    }
    
    public function actionsADMCustom(Request $request)
    {
        // dd($request);
        $directions = $this->planningRepository->getAllDirections();
        $axes = $this->planningRepository->getAxesADM();
        if ($request->direction[0] == null && $request->axe[0] == null) {
            $stadistics = $this->stadisticsRepository->getStadisticsADM(null, null, $request->trimester);
        }else if ($request->direction[0] != null && $request->axe[0] == null) {
            $stadistics = $this->stadisticsRepository->getStadisticsADM($request->direction, null, $request->trimester );
        }else if ($request->direction[0] == null && $request->axe[0] != null) {
            $stadistics = $this->stadisticsRepository->getStadisticsADM(null, $request->axe, $request->trimester);
        }else {
            $stadistics = $this->stadisticsRepository->getStadisticsADM($request->direction, $request->axe, $request->trimester);
        }
        // dd($stadistics);
        session(["stadistics_adm" => $stadistics]);
        session(["trimester_adm" => $request->trimester]);

        return view("stadistics.actions_adm", compact("directions", "axes", "stadistics"));
    }

    public function actionsADMPDF(Request $request)
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        $this->stadisticsRepository->saveImageInafed($request->image);
        $stadistics = session("stadistics_adm");
        $trimester = session("trimester_adm")."° Trimestre";
        if(!session("trimester"))
        {
            $trimester = "Informe Anual";
        }
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use ($trimester){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor-implan.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Estadisticas Acciones ADM", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(16);
            $pdf->Cell(0, 0, "( ".$trimester." )", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.actions_adm')->with(compact('stadistics'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');
        $imagen = public_path('assets/graphics/actionsInafed.png');
        PDF::Image($imagen, 15, 140, 335, 216, '', '', '', true, 300, '', false, false, 1, false, false, false);
        // PDF::Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
        PDF::SetTitle("Estadisticas Acciones ADM a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Estadisticas Acciones ADM a ".$date_now->format('d-M-Y').".pdf");
    }
    
    public function actionsA2030()
    {
        $directions = $this->planningRepository->getAllDirections();
        $axes = $this->planningRepository->getAxesA2030();

        $stadistics = $this->stadisticsRepository->getStadisticsA2030();
        // dd($stadistics);
        session(["stadistics_a2030" => $stadistics]);
        session(["trimester_a2030" => null]);
        return view("stadistics.actions_a2030", compact("directions", "axes", "stadistics"));
    }
    
    public function actionsA2030Custom(Request $request)
    {
        // dd($request);
        $directions = $this->planningRepository->getAllDirections();
        $axes = $this->planningRepository->getAxesA2030();
        if ($request->direction[0] == null && $request->axe[0] == null) {
            $stadistics = $this->stadisticsRepository->getStadisticsA2030(null, null, $request->trimester);
        }else if ($request->direction[0] != null && $request->axe[0] == null) {
            $stadistics = $this->stadisticsRepository->getStadisticsA2030($request->direction, null, $request->trimester );
        }else if ($request->direction[0] == null && $request->axe[0] != null) {
            $stadistics = $this->stadisticsRepository->getStadisticsA2030(null, $request->axe, $request->trimester);
        }else {
            $stadistics = $this->stadisticsRepository->getStadisticsA2030($request->direction, $request->axe, $request->trimester);
        }
        // dd($stadistics);
        session(["stadistics_a2030" => $stadistics]);
        session(["trimester_a2030" => $request->trimester]);

        return view("stadistics.actions_a2030", compact("directions", "axes", "stadistics"));
    }

    public function actionsA2030PDF(Request $request)
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        $this->stadisticsRepository->saveImageAgenda2030($request->image);
        $stadistics = session("stadistics_a2030");
        $trimester = session("trimester_a2030")."° Trimestre";
        if(!session("trimester"))
        {
            $trimester = "Informe Anual";
        }
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use ($trimester){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor-implan.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Estadisticas Acciones Agenda 2030", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(16);
            $pdf->Cell(0, 0, "( ".$trimester." )", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.actions_a2030')->with(compact('stadistics'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');
        $imagen = public_path('assets/graphics/actionsagenda2030.png');
        PDF::Image($imagen, 15, 140, 335, 216, '', '', '', true, 300, '', false, false, 1, false, false, false);
        // PDF::Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
        PDF::SetTitle("Estadisticas Acciones Agenda 2030 a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Estadisticas Acciones Agenda 2030 a ".$date_now->format('d-M-Y').".pdf");
    }

    public function linesPMD()
    {
        $axes = $this->planningRepository->getAxes();
        // error_log('----------------->Some message here<-------------------');
        return view("stadistics.lines_pmd", compact("axes"));
    }

    public function pmd(Request $request)
    {
        $stadistics = $this->stadisticsRepository->getStadisticsPMD($request->trimester);

        session(["stadistics_pmd" => $stadistics]);
        session(["trimester" => $request->trimester]);
        return view("stadistics.pmd", compact("stadistics"));
    }

    public function pmdPDF(Request $request)
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        $this->stadisticsRepository->saveImage($request->image);
        $stadistics = session("stadistics_pmd");
        $trimester = session("trimester")."° Trimestre";
        if(!session("trimester"))
        {
            $trimester = "Informe Anual";
        }
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use ($trimester){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor-implan.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Estadisticas Acciones PMD", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(16);
            $pdf->Cell(0, 0, "( ".$trimester." )", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.pmd')->with(compact('stadistics'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');
        $imagen = public_path('assets/graphics/actionsPMD.png');
        PDF::Image($imagen, 15, 140, 335, 216, '', '', '', true, 300, '', false, false, 1, false, false, false);
        // PDF::Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
        PDF::SetTitle("Estadisticas Plan Municipal a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Estadisticas Plan Municipal a ".$date_now->format('d-M-Y').".pdf");
    }

    public function actionLinesPMD(Request $request)
    {
        $stadistics = $this->stadisticsRepository->getStadisticsActionLinesPMD($request->trimester);

        session(["stadistics_pmd_actions" => $stadistics]);
        return view("stadistics.pmd_actions", compact("stadistics"));
    }
    
    public function actionLinesADM(Request $request)
    {
        $stadistics = $this->stadisticsRepository->getStadisticsActionLinesADM($request->trimester);

        session(["stadistics_adm_actions" => $stadistics]);
        return view("stadistics.adm_actions", compact("stadistics"));
    }

    public function actionLinesA2030(Request $request)
    {
        $stadistics = $this->stadisticsRepository->getStadisticsActionLinesA2030($request->trimester);

        session(["stadistics_a2030_actions" => $stadistics]);
        return view("stadistics.a2030_actions", compact("stadistics"));
    }

    public function actionLinesPMDPDF()
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        // $this->stadisticsRepository->saveImage($request->image);
        $stadistics = session("stadistics_pmd_actions");
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor-implan.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Lineas no Programadas PMD", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.pmd_actions')->with(compact('stadistics'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');
        // $imagen = public_path('assets/graphics/actionsPMD.png');
        // PDF::Image($imagen, 15, 140, 335, 216, '', '', '', true, 300, '', false, false, 1, false, false, false);
        // PDF::Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
        PDF::SetTitle("Estadisticas Plan Municipal a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Estadisticas Plan Municipal a ".$date_now->format('d-M-Y').".pdf");
    }
    
    public function actionLinesADMPDF()
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        // $this->stadisticsRepository->saveImage($request->image);
        $stadistics = session("stadistics_adm_actions");
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor-implan.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Lineas no Programadas ADM", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.adm_actions')->with(compact('stadistics'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');
        // $imagen = public_path('assets/graphics/actionsPMD.png');
        // PDF::Image($imagen, 15, 140, 335, 216, '', '', '', true, 300, '', false, false, 1, false, false, false);
        // PDF::Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
        PDF::SetTitle("Estadisticas Plan Municipal a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Estadisticas Plan Municipal a ".$date_now->format('d-M-Y').".pdf");
    }
    
    public function actionLinesA2030PDF()
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        // $this->stadisticsRepository->saveImage($request->image);
        $stadistics = session("stadistics_a2030_actions");
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor-implan.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Lineas no Programadas Agenda 2030", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.a2030_actions')->with(compact('stadistics'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');
        // $imagen = public_path('assets/graphics/actionsPMD.png');
        // PDF::Image($imagen, 15, 140, 335, 216, '', '', '', true, 300, '', false, false, 1, false, false, false);
        // PDF::Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
        PDF::SetTitle("Estadisticas Plan Municipal a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Estadisticas Plan Municipal a ".$date_now->format('d-M-Y').".pdf");
    }

    public function inafed(Request $request)
    {
        $stadistics = $this->stadisticsRepository->getStadisticsInafed($request->trimester);

        session(["stadistics_inafed" => $stadistics]);
        session(["trimester_inafed" => $request->trimester]);
        return view("stadistics.inafed", compact("stadistics"));
    }

    public function inafedPDF(Request $request)
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        $this->stadisticsRepository->saveImageInafed($request->image);
        $stadistics = session("stadistics_inafed");
        $trimester = session("trimester_inafed")."° Trimestre";
        if(!session("trimester_inafed"))
        {
            $trimester = "Informe Anual";
        }
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use ($trimester){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor-implan.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Estadisticas Indicadores INAFED", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(16);
            $pdf->Cell(0, 0, "( ".$trimester." )", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.inafed')->with(compact('stadistics'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');
        $imagen = public_path('assets/graphics/actionsInafed.png');
        PDF::Image($imagen, 15, 140, 335, 216, '', '', '', true, 300, '', false, false, 1, false, false, false);
        // PDF::Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
        PDF::SetTitle("Estadisticas Indicadores INAFED a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Estadisticas Indicadores INAFED a ".$date_now->format('d-M-Y').".pdf");
    }
    
    public function agenda2030(Request $request)
    {
        $stadistics = $this->stadisticsRepository->getStadisticsAgenda2030($request->trimester);

        session(["stadistics_agenda2030" => $stadistics]);
        session(["trimester_agenda2030" => $request->trimester]);
        return view("stadistics.agenda2030", compact("stadistics"));
    }

    public function agenda2030PDF(Request $request)
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        $this->stadisticsRepository->saveImageAgenda2030($request->image);
        $stadistics = session("stadistics_agenda2030");
        $trimester = session("trimester_agenda2030")."° Trimestre";
        if(!session("trimester_agenda2030"))
        {
            $trimester = "Informe Anual";
        }
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) use ($trimester){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor-implan.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Estadisticas Indicadores Agenda 2030", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(16);
            $pdf->Cell(0, 0, "( ".$trimester." )", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.agenda2030')->with(compact('stadistics'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');
        $imagen = public_path('assets/graphics/actionsagenda2030.png');
        PDF::Image($imagen, 15, 140, 335, 216, '', '', '', true, 300, '', false, false, 1, false, false, false);
        // PDF::Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
        PDF::SetTitle("Estadisticas Indicadores Agenda 2030 a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Estadisticas Indicadores Agenda 2030 a ".$date_now->format('d-M-Y').".pdf");
    }
}
