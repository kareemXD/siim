<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Services
use Carbon\Carbon;
use PDF;
use Auth;

//Repositories
use App\Repositories\NewReportRepository;
use App\Repositories\PlanningRepository;

class NewReportController extends Controller
{
    private $reportRepository,
            $planningRepository;

    public function __construct(
        NewReportRepository $reportRepository,
        PlanningRepository $planningRepository
    ){
        $this->reportRepository = $reportRepository;
        $this->planningRepository = $planningRepository;
    }

    public function generateMir()
    {
        $date_now = Carbon::now('America/Mexico_City');
        $data = $this->reportRepository->getDataMyMir(2021);
        $direction = Auth::user()->profile->position->direction;
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Matriz de Indicadores 2021", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');

            $pdf->SetY(-10);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 10, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LEGAL');
        $html = view('partials.new-PDF.generate_mir_report')->with(compact('data', 'direction'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
    }
    
    public function generateMirDirection($id)
    {
        $date_now = Carbon::now('America/Mexico_City');
        $direction = $this->planningRepository->getDirectionById($id);
        $data = $this->reportRepository->getDataMirDirection($direction, 2021);
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Matriz de Indicadores 2021", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');

            $pdf->SetY(-10);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 10, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LEGAL');
        $html = view('partials.new-PDF.generate_mir_report')->with(compact('data', 'direction'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
    }

    //reporte trimestral del avance de la mir
    public function generateAdvanceMirDirection($id)
    {
        $date_now = Carbon::now('America/Mexico_City');
        $direction = $this->planningRepository->getDirectionById($id);
        $data = $this->reportRepository->getDataMirDirection($direction, 2021);
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('AVANCE MIR');
        PDF::SetSubject('AVANCE MIR');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Avance 1er Trimestre 2021", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');

            $pdf->SetY(-10);
            // Set font
            $pdf->SetFont('helvetica', 'I', 8);
            // Page number
            $pdf->Cell(0, 10, 'Pagina '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'M');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LEGAL');
        $html = view('partials.new-PDF.generate_advance_mir_report')->with(compact('data', 'direction'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
    }
    
}
