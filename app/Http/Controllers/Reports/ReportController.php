<?php

namespace App\Http\Controllers\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Services
use Carbon\Carbon;
use PDF;
use Auth;

//Repositories
use App\Repositories\ReportRepository;
use App\Repositories\PlanningRepository;

class ReportController extends Controller
{
    private $reportRepository,
            $planningRepository;

    public function __construct(
        ReportRepository $reportRepository,
        PlanningRepository $planningRepository
    ){
        $this->reportRepository = $reportRepository;
        $this->planningRepository = $planningRepository;
    }

    public function generatePOA()
    {
        $date_now = Carbon::now('America/Mexico_City');
        $data = $this->reportRepository->getDataMyPoa(config('implan.periods.previous'));
        $direction = Auth::user()->profile->position->direction;
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Programa Operativo Anual ".config('implan.periods.previous'), 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.generate_poa_report')->with(compact('data', 'direction'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
    }
    
    

    public function generatePOADirection($id)
    {
        $date_now = Carbon::now('America/Mexico_City');
        $direction = $this->planningRepository->getDirectionById($id);
        $data = $this->reportRepository->getDataPoaDirection($direction, config('implan.periods.previous'));
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Programa Operativo Anual ".config('implan.periods.previous'), 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.generate_poa_report')->with(compact('data', 'direction'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("POA ".$direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("POA ".$direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
    }
    
    public function statusActionLines()
    {
        $date_now = Carbon::now('America/Mexico_City');
        $data = $this->reportRepository->statusActionLines();
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('Reporte de Lineas de Accion');
        PDF::SetSubject('Reporte');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Reporte de Lineas de Accion", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetFont('', '', 10);
        
        $data_line = $data["0"];
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.status_pdm_actions')->with(compact('data_line'))->render();
        PDF::writeHTML($html, true, false, true, false, '');
        
        $data_line = $data["1"];
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.status_pdm_actions')->with(compact('data_line'))->render();
        PDF::writeHTML($html, true, false, true, false, '');

        $data_line = $data["2"];
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.status_pdm_actions')->with(compact('data_line'))->render();
        PDF::writeHTML($html, true, false, true, false, '');

        $data_line = $data["3"];
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.status_pdm_actions')->with(compact('data_line'))->render();
        PDF::writeHTML($html, true, false, true, false, '');
            

        PDF::SetTitle("Reporte de Lineas de Accion a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Reporte de Lineas de Accion a ".$date_now->format('d-M-Y').".pdf");
    }
   
    public function statusAdvanceActionLines()
    {
        $date_now = Carbon::now('America/Mexico_City');
        $data = $this->reportRepository->statusActionLines();
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('Reporte de Lineas de Accion');
        PDF::SetSubject('Reporte');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Reporte de Lineas de Accion", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetFont('', '', 10);
        
        $data_line = $data["0"];
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.status_advance_actions')->with(compact('data_line'))->render();
        // dd($html);
        PDF::writeHTML($html, true, false, true, false, '');
        
        $data_line = $data["1"];
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.status_advance_actions')->with(compact('data_line'))->render();
        PDF::writeHTML($html, true, false, true, false, '');

        $data_line = $data["2"];
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.status_advance_actions')->with(compact('data_line'))->render();
        PDF::writeHTML($html, true, false, true, false, '');
        // dd("entro");
        
        $data_line = $data["3"];
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.status_advance_actions')->with(compact('data_line'))->render();
        // dd($html);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("Reporte de Lineas de Accion a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Reporte de Lineas de Accion a ".$date_now->format('d-M-Y').".pdf");
    }

    public function generateAdvancePOA()
    {
        $date_now = Carbon::now('America/Mexico_City');
        if(config('implan.times.planning'))
        {
            $period = config('implan.periods.previous');
        }else {
            $period = config('implan.periods.actual');
        }
        $data = $this->reportRepository->getDataMyPoa($period);
        $direction = Auth::user()->profile->position->direction;
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Programa Operativo Anual ".config('implan.periods.previous'), 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.generate_poa_report_advance')->with(compact('data', 'direction'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
    }
    
    public function generatePoaAdvanceDirection($id)
    {
        $date_now = Carbon::now('America/Mexico_City');
        if(config('implan.times.planning'))
        {
            $period = config('implan.periods.previous');
        }else {
            $period = config('implan.periods.actual');
        }
        $data = $this->reportRepository->getDataPoaByDirection($id, $period);
        $direction = $this->planningRepository->getDirectionById($id);
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Programa Operativo Anual ".config('implan.periods.previous'), 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.generate_poa_report_advance')->with(compact('data', 'direction'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
    }

    public function annualReport()
    {
        $meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        //Configuraciones del Texto
        $title = array('name'=>'Times New Roman','size' => 16,'bold' => true);
        $subtitle = array('name'=>'Times New Roman','size' => 14,'bold' => true);
        $bold = array('name'=>'Times New Roman','size' => 12,'bold' => true);
        $parafrace = array('name'=>'Times New Roman','size' => 10,'bold' => false);

        //Obtenemos los Datos
        $directions = $this->reportRepository->getDirectionWithMonthlyActivities();
        
        //Creamos el Archivo
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        foreach ($directions as $direction) 
        {
            //Configuracion del Archivo
            $section = $phpWord->addSection();
            $header = $section->addHeader();
            $header->addWatermark('assets/images/Hoja_Membretada.png', 
            array(
                'width' => 591, 
                'height' => 850, 
                'marginTop' => -45,
                'marginLeft' => -71,
                'posHorizontal' => 'absolute',
                'posVertical' => 'absolute',
            ));

            //Seccion donde se Agregara ekl texto
            $section->addText("");
            $section->addText($direction->nombre, $title);
            $last_mounth = "";
            foreach ($direction->activities()->orderBy('year', 'ASC')->orderBy('mes', 'ASC')->get() as $key => $activity) {
                $new_mounth = $meses[$activity->mes - 1]." del ".$activity->year;

                if ($new_mounth != $last_mounth) {
                    $section->addText($new_mounth, $subtitle);
                    $section->addText("----------------------------------------------------------------------------------------------------------------------------");
                }

                $textrun = $section->addTextRun();
                $textrun->addText("Localidad(es): ", $bold);
                foreach ($activity->locations as $location) {
                    $textrun->addText($location->nombre.", ", $parafrace);
                }

                $textrun = $section->addTextRun();
                $textrun->addText("Nombre o Tema de la Actividad Relevante: ", $bold);
                $textrun->addText($activity->accion, $parafrace);

                $textrun = $section->addTextRun();
                $textrun->addText("Presupuesto : ", $bold);
                $textrun->addText("$ ".$activity->monto, $parafrace);
                
                $textrun = $section->addTextRun();
                $textrun->addText("Beneficiarios : ", $bold);
                $textrun->addText($activity->beneficiarios_h." Hombres, ".$activity->beneficiarios_m." Mujeres", $parafrace);
                
                $textrun = $section->addTextRun();
                $textrun->addText("Descripción : ", $bold);
                $textrun->addText($activity->descripcion, $parafrace);
                
                if(count($activity->evidences) > 0)
                {
                    $textrun = $section->addTextRun();
                    $textrun->addText("Anexos : ", $bold);
                    foreach ($activity->evidences as $key => $evidence) {
                        $textrun = $section->addTextRun();
                        $textrun->addLink(route('download.evidence.by.id', $evidence->id), route('download.evidence.by.id', $evidence->id));
                    }
                }
                $section->addText("----------------------------------------------------------------------------------------------------------------------------");

                $last_mounth = $new_mounth;
            }
        }
        
        // $section->addImage("assets/plugins/preview/preview_pdf.png");  
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('files/Avance-Informe-Anual.docx');
        return response()->download(public_path('files/Avance-Informe-Anual.docx'));
    }

    public function generateAnnualReport()
    {
        $date_now = Carbon::now('America/Mexico_City');
        $data = $this->reportRepository->getDataAnnualReportAdvance();
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Reporte de Avance del Informe Anual", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.generate_annual_report_advance')->with(compact('data'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
    }

    public function generateInafedAdvanceDirection($id)
    {
        $date_now = Carbon::now('America/Mexico_City');
        if(config('implan.times.planning'))
        {
            $period = config('implan.periods.previous');
        }else {
            $period = config('implan.periods.actual');
        }
        $data = $this->reportRepository->getDataInafedByDirection($id, $period);
        $direction = $this->planningRepository->getDirectionById($id);
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Avance de INAFED", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.generate_inafed_report_advance')->with(compact('data', 'direction'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("INAFED ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("INAFED ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
    }
    
    public function generateAgenda2030AdvanceDirection($id)
    {
        $date_now = Carbon::now('America/Mexico_City');
        if(config('implan.times.planning'))
        {
            $period = config('implan.periods.previous');
        }else {
            $period = config('implan.periods.actual');
        }
        $data = $this->reportRepository->getDataAgenda2030ByDirection($id, $period);
        $direction = $this->planningRepository->getDirectionById($id);
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Avance de Agenda 2030", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.generate_agenda2030_report_advance')->with(compact('data', 'direction'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("Agenda2030 ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Agenda2030 ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
    }
    
    public function generateMirAdvanceDirection($id)
    {
        $date_now = Carbon::now('America/Mexico_City');
        if(config('implan.times.planning'))
        {
            $period = config('implan.periods.previous');
        }else {
            $period = config('implan.periods.actual');
        }
        $data = $this->reportRepository->getDataMirByDirection($id, $period);
        $direction = $this->planningRepository->getDirectionById($id);
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Avance de las MIR", 0, 0, 'C');
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.generate_mir_report_advance')->with(compact('data', 'direction'))->render();
        PDF::SetFont('', '', 10);
        PDF::writeHTML($html, true, false, true, false, '');

        PDF::SetTitle("Mir ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Mir ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
    }
}
