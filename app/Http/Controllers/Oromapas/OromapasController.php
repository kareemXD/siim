<?php

namespace App\Http\Controllers\Oromapas;
// Register autoloader
use App\Repositories\OromapasRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Shapefile\Shapefile;
use Shapefile\ShapefileException;
use Shapefile\ShapefileReader;
use Illuminate\Http\UploadedFile;
use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use App\Models\Oromapas\Oromapas;
use App\Models\Oromapas\sanitaria;
use App\Models\Oromapas\predios;
use App\Models\Oromapas\poligonos;
use App\Models\Oromapas\padron;
use App\Models\Oromapas\Pozos;
use Illuminate\Support\Collection as Collection;
use DB;
use Storage;
use DataTables;
// use Yajra\DataTables\DataTables;
/**
 * Handles the file upload
 *
 * @param FileReceiver $receiver
 *
 * @return \Illuminate\Http\JsonResponse
 *
 * @throws UploadMissingFileException
 *
 */
class OromapasController extends Controller
{
    public function __construct(
        OromapasRepository $OromapasRepository
    )
    {
        $this->OromapasRepository = $OromapasRepository;
    }

    public function index()
    {
        $predios =  $this->OromapasRepository->getPredios();
        $sanitarias =  $this->OromapasRepository->getSanitarias();
        $hidraulicas =  $this->OromapasRepository->getHidraulicas();
        $poligonos =  $this->OromapasRepository->getPoligonos();
        $localidades =  $this->OromapasRepository->getLocalidades();
        $pozos =  $this->OromapasRepository->getPozos();
        return view("moduls.oromapas.all_oromapas", compact('predios',"sanitarias","hidraulicas","poligonos" ,"localidades", "pozos"));
    }
    
    public function registrarView()
    {
        $localidades =  $this->OromapasRepository->getLocalidades();
        return view("moduls.oromapas.registro",['localidades' => $localidades]);
    }
    
    public function registrar(Request $request)
    {   
    
        set_time_limit(600);
        $handler = "";
        // create the file receiver
        if($request->hasFile('sanitaria'))
        {
            $receiver = new FileReceiver("sanitaria", $request, HandlerFactory::classFromRequest($request));
            $input = "sanitaria";
                // check if the upload is success, throw exception or return response you need
            if ($receiver->isUploaded() === false) {
                throw new UploadMissingFileException();
            }
            // receive the file
            $save = $receiver->receive();
            // check if the upload has finished (in chunk mode it will send smaller files)
            if ($save->isFinished()) {
                // save the file and return any response you need, current example uses `move` function. If you are
                // not using move, you need to manually delete the file by unlink($save->getFile()->getPathname())
                echo $this->saveFile($save->getFile(), $input);
            
            }
            // we are in chunk mode, lets send the current progress
            /** @var AbstractHandler $handler */
            $handler = $save->handler();
            // return response()->json([
            //     "done" => $handler->getPercentageDone(),
            //     'status' => true
            // ]);
        }

        if($request->hasFile("poligono"))
        {
            $receiver = new FileReceiver("poligono", $request, HandlerFactory::classFromRequest($request));
            $input = "poligono";
                // check if the upload is success, throw exception or return response you need
                if ($receiver->isUploaded() === false) {
                throw new UploadMissingFileException();
            }
            // receive the file
            $save = $receiver->receive();
            // check if the upload has finished (in chunk mode it will send smaller files)
            if ($save->isFinished()) {
                // save the file and return any response you need, current example uses `move` function. If you are
                // not using move, you need to manually delete the file by unlink($save->getFile()->getPathname())
                echo $this->saveFile($save->getFile(), $input);
            
            }
            // we are in chunk mode, lets send the current progress
            /** @var AbstractHandler $handler */
            $handler = $save->handler();
            // return response()->json([
            //     "done" => $handler->getPercentageDone(),
            //     'status' => true
            // ]);
        }

        if($request->hasFile('predios'))
        {
            $receiver = new FileReceiver("predios", $request, HandlerFactory::classFromRequest($request));
            $input = "predios";
                // check if the upload is success, throw exception or return response you need
                if ($receiver->isUploaded() === false) {
                throw new UploadMissingFileException();
            }
            // receive the file
            $save = $receiver->receive();
            // check if the upload has finished (in chunk mode it will send smaller files)
            if ($save->isFinished()) {
                // save the file and return any response you need, current example uses `move` function. If you are
                // not using move, you need to manually delete the file by unlink($save->getFile()->getPathname())
                echo $this->saveFile($save->getFile(), $input, $request->localidad);
            
            }
            // dd("entro");
            // we are in chunk mode, lets send the current progress
            /** @var AbstractHandler $handler */
            $handler = $save->handler();
            // return response()->json([
            //     "done" => $handler->getPercentageDone(),
            //     'status' => true
            // ]);
        }

        if($request->hasFile('hidraulica'))
        {
            $receiver = new FileReceiver("hidraulica", $request, HandlerFactory::classFromRequest($request));
            $input = "hidraulica";
            // check if the upload is success, throw exception or return response you need
            if ($receiver->isUploaded() === false) {
                throw new UploadMissingFileException();
            }
            // receive the file
            $save = $receiver->receive();
            // check if the upload has finished (in chunk mode it will send smaller files)
            if ($save->isFinished()) {
                // save the file and return any response you need, current example uses `move` function. If you are
                // not using move, you need to manually delete the file by unlink($save->getFile()->getPathname())
                echo $this->saveFile($save->getFile(), $input);
            
            }
            // we are in chunk mode, lets send the current progress
            /** @var AbstractHandler $handler */
            $handler = $save->handler();
            
        }
       
        if($request->hasFile('pozos'))
        {
            $receiver = new FileReceiver("pozos", $request, HandlerFactory::classFromRequest($request));
            $input = "pozos";
            // check if the upload is success, throw exception or return response you need
            if ($receiver->isUploaded() === false) {
                throw new UploadMissingFileException();
            }
            // receive the file
            $save = $receiver->receive();
            // check if the upload has finished (in chunk mode it will send smaller files)
            if ($save->isFinished()) {
                // save the file and return any response you need, current example uses `move` function. If you are
                // not using move, you need to manually delete the file by unlink($save->getFile()->getPathname())
                echo $this->saveFile($save->getFile(), $input);
            
            }
            // we are in chunk mode, lets send the current progress
            /** @var AbstractHandler $handler */
            $handler = $save->handler();
            
        }

        // return response()->json([
        //     "done" => $handler->getPercentageDone(),
        //     'status' => true
        // ]);
    }

    public function saveFile($data, $input, $localidad = "")
    {
        if($data)
        {
            $filename = $data->getClientOriginalName();
            $type = $data->getClientOriginalExtension();
            $carpeta = substr($filename, 0, -4);
            try {
                if(Storage::disk('public')->putFileAs('shapes/'.$carpeta, $data ,$filename))
                {
                    
                    $zip = new \ZipArchive();
                    $dir =  storage_path('app/public/shapes/'.$carpeta.'/'.$filename);
                    $fileShape =  storage_path('app/public/shapes/'.$carpeta.'/');
                    $x = $zip->open($dir);
                    if ($x === true)
                    {
                        $zip->extractTo($fileShape); 
                        $zip->close();
                        unlink($dir);
                        $url = storage_path('app/public/shapes/'.$carpeta."/");
                        $gestor = opendir($url);
                        //Recorre todos los elementos del directorio
                        while (($archivo = readdir($gestor)) !== false)  {
                            // Se muestran todos los archivos y carpetas excepto "." y ".."
                            if ($archivo != "." && $archivo != "..") {
                                $ext = explode(".",$archivo)[1];
                                rename($url.'/'.$archivo, $url.'/'.$carpeta.".".$ext);
                            }    
                        }
                        try {
                            // Open shapefile
                            $ShapeFile = new ShapefileReader(storage_path('app/public/shapes/'.$carpeta.'/'.$carpeta.'.shp'));
                            
                            while ($geometry = $ShapeFile->fetchRecord()) {
                                if ($geometry->isDeleted()) {
                                    continue;
                                }
                                // dd($record->getDataArray());
                                $record = $geometry->getDataArray();
                                switch ($input) {
                                    case 'predios':
                                        $padron = padron::where('contrato','=' ,$record['CUENTA'])->first();

                                        if(!empty($padron))
                                        {
                                            $array[] = [
                                                'contrato' => $padron->contrato,
                                                'calle' => $padron->calle,
                                                'numero' =>$padron->numero,
                                                'entreCalle1' =>  $padron->entrecalle1,
                                                'entreCalle2'=>  $padron->entrecalle2,
                                                'colonia' => $padron->colonia,
                                                'localidad' => $padron->localidad,
                                                'tipoServicio' => $padron->tipoServicio,
                                                'giro' => $padron->giro,
                                                'tieneAgua' => $padron->tieneAgua,
                                                'tieneDrenaje' => $padron->tieneDrenaje,
                                                'medidorSerie' => $padron->medidorSerie,
                                                "shp" => $geometry->getWKT()
                                            ];
                                        }
                                        else
                                        {
                                            $array[] = [
                                                'contrato' => $record['CUENTA'],
                                                'calle' =>  (!isset($record['DIRECCION']) || empty($record['DIRECCION'])) ? 'Indefinido':  $record['DIRECCION']  ,
                                                'numero' =>  (!isset($record['NO_EXT']) || empty($record['NO_EXT']) ) ? 'Indefinido' : $record['NO_EXT'] ,
                                                'entreCalle1' =>  (!isset($record['CALLE1']) || empty($record['CALLE1'])) ? 'Indefinido' : $record['CALLE1'],
                                                'entreCalle2'=>  (!isset($record['CALLE2']) || empty($record['CALLE2'])) ? 'Indefinido' : $record['CALLE2'],
                                                'colonia' => (!isset($record['COLONIA']) || empty($record['COLONIA'])) ? 'Indefinido' : $record['COLONIA'],
                                                'localidad' => (!isset($record['LOCALIDAD']) || empty($record['LOCALIDAD'])) ? 'Indefinido' : $record['LOCALIDAD'],
                                                'tipoServicio' => 'Indefinido',
                                                'giro' => 'Indefinido',
                                                'tieneAgua' => 'Indefinido',
                                                'tieneDrenaje' => 'Indefinido',
                                                'medidorSerie' => 'Indefinido',
                                                "shp" => $geometry->getWKT()
                                            ];
                                        }
                                        break;
                                    
                                    case 'sanitaria':
                                        $array[] = [
                                            'layer' => $record["dbf"]["LAYER"],
                                            'pendiente' => $record["dbf"]["PENDIENTE"],
                                            'lonigitud' => $record["dbf"]["LONGITUD"],
                                            'diametro' => $record["dbf"]["DIAMETRO"],
                                            'estado' => $record["dbf"]["ESTADO"],
                                            'shp' => $geometry->getWKT()
                                        ];
                                        break;

                                    case 'pozos':
                                        $array[] = [
                                            'nivel_raz' => $record["dbf"]["NIVEL_RAS"],
                                            'layer' => $record["dbf"]["LAYER"],
                                            'nivel_arras' => $record["dbf"]["NIVEL_ARRA"],
                                            'diferencia' => $record["dbf"]["DIFERENCIA"],
                                            'shp' => $geometry->getWKT()
                                        ];
                                        break;
                                    
                                    case 'hidraulica':
                                        $array[] = [
                                            'layer' => $record["dbf"]["LAYER"],
                                            'pendiente' => $record["dbf"]["DIAMETRO"],
                                            'longitud' => $record["dbf"]["LONGITUD"],
                                            'estado' => $record["dbf"]["ESTADO"],
                                            'shp' => $geometry->getWKT()
                                        ];
                                        break;
                                    
                                    case 'poligono':
                                        $array[] = [
                                            'cvgeo' => $record["dbf"]["CVEGEO"],
                                            'cve_ent' => $record["dbf"]["CVE_ENT"],
                                            'cve_mun' => $record["dbf"]["CVE_MUN"],
                                            'cve_loc' => $record["dbf"]["CVE_LOC"],
                                            'nomgeo' => $record["dbf"]["NOMGEO"],
                                            'ambito' => $record["dbf"]["AMBITO"],
                                            'shp' => $geometry->getWKT()
                                        ];
                                        break;
                                }
                            }
                            // dd($array);
                            if($input == "sanitaria")
                            {
                                $collection = Collection::make($array);
                                $collection->chunk(100);
                                foreach ($collection as $value) {
                                    if($value["shp"] != "MULTIPOLYGON()"  && $value["shp"] != NULL )
                                    {
                                        $sanitaria = new sanitaria();
                                        $sanitaria->layer = $value["layer"];
                                        $sanitaria->pendiente = $value["pendiente"];
                                        $sanitaria->estado = $value["estado"];
                                        $sanitaria->geodata = DB::raw("ST_GeomFromText('".$value["shp"]."', 32613)"); 
                                        $allintests[] = $sanitaria->attributesToArray();
                                        sanitaria::insert($allintests);
                                    }
                                    $allintests = [];
                                }
                            } 
                            if($input == "pozos")
                            {
                                $collection = Collection::make($array);
                                $collection->chunk(100);
                                foreach ($collection as $value) {
                                    if($value["shp"] != "MULTIPOLYGON()"  && $value["shp"] != NULL)
                                    {
                                        $pozo = new Pozos();
                                        $pozo->nivel_raz = $value["nivel_raz"];
                                        $pozo->nivel_arras = $value["nivel_arras"];
                                        $pozo->layer = $value["layer"];
                                        $pozo->diferencia = $value["diferencia"];
                                        $pozo->geodata = DB::raw("ST_GeomFromText('".$value["shp"]."', 32613)"); 
                                        $allintests[] = $pozo->attributesToArray();
                                        Pozos::insert($allintests);
                                    }
                                    $allintests = [];
                                }
                            } 

                            if($input == "hidraulica")
                            {
                                $collection = Collection::make($array);
                                $collection->chunk(100);
                                foreach ($collection as $value) {
                                    if($value["shp"] != "MULTIPOLYGON()"  && $value["shp"] != NULL )
                                    {
                                        $sanitaria = new Oromapas();
                                        $sanitaria->layer = $value["layer"];
                                        $sanitaria->pendiente = $value["pendiente"];
                                        $sanitaria->longitud = $value["longitud"];
                                        $sanitaria->estado = $value["estado"];
                                        $sanitaria->geodata = DB::raw("ST_GeomFromText('".$value["shp"]."', 32613)"); 
                                        $allintests[] = $sanitaria->attributesToArray();
                                        Oromapas::insert($allintests);
                                    }
                                    $allintests = [];
                                }
                            }


                            if($input == "predios")
                            {
                                $collection = Collection::make($array);
                                $collection->chunk(100);
                                foreach ($collection as $value) {
                                    if($value["shp"] != "MULTIPOLYGON()" && $value["shp"] != NULL )
                                    {
                                        $predio = new predios();
                                        $predio->contrato = $value['contrato'];
                                        $predio->calle = $value['calle'];
                                        $predio->numero = $value['numero'];
                                        $predio->entreCalle1 = $value['entreCalle1'];
                                        $predio->entreCalle2 = $value['entreCalle2'];
                                        $predio->colonia = $value['colonia'];
                                        $predio->localidad = $value['localidad'];
                                        $predio->tipoServicio = $value['tipoServicio'];
                                        $predio->giro = $value['giro'];
                                        $predio->tieneAgua  = $value['tieneAgua'];
                                        $predio->tieneDrenaje = $value['tieneDrenaje'];
                                        $predio->medidorSerie = $value['medidorSerie'];
                                        $predio->geodata = DB::raw("ST_GeomFromText('".$value["shp"]."', 32613)"); 
                                        $allintests[] = $predio->attributesToArray();
                                        DB::connection('cartoWeb')->table($localidad)->insert($allintests);
                                    }
                                    $allintests = [];
                                }
                            }

                            if($input == "poligono")
                            {
                                $collection = Collection::make($array);
                                $collection->chunk(100);
                                foreach ($collection as $value) {
                                    if($value["shp"] != "MULTIPOLYGON()"  && $value["shp"] != NULL)
                                    {
                                        $sanitaria = new poligonos();
                                        $sanitaria->cvgeo = $value["cvgeo"];
                                        $sanitaria->cve_ent = $value["cve_ent"];
                                        $sanitaria->cve_mun = $value["cve_mun"];
                                        $sanitaria->cve_loc = $value["cve_loc"];
                                        $sanitaria->nombre = $value["nomgeo"];
                                        $sanitaria->geodata = DB::raw("ST_GeomFromText('".$value["shp"]."', 32613)"); 
                                        $allintests[] = $sanitaria->attributesToArray();
                                        poligonos::insert($allintests);
                                        $allintests = [];
                                    }
                                }
                            }
                            // $dir = $fileShape;   
                            // if(is_dir($dir)){
                            //     $files = new \RecursiveIteratorIterator(
                            //         new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS), \RecursiveIteratorIterator::CHILD_FIRST
                            //     );
                            //     foreach($files as $file){
                            //         if ($file->isDir()){
                            //             rmdir($file->getRealPath());
                            //         }else{
                            //             unlink($file->getRealPath());
                            //         }
                            //     }
                            //     rmdir($dir);
                            // }
                            // $files = glob($fileShape.'*'); //obtenemos todos los nombres de los ficheros
                            // foreach($files as $file){
                            //     if(is_file($file))
                            //     @unlink($file); //elimino el fichero
                            // }
                            // foreach(scandir($fileShape) as $file) {
                            //     if ('.' === $file || '..' === $file) continue;
                            //     gc_collect_cycles();
                            //     @unlink("$fileShape"."$file");
                            // } 
                            // rmdir($fileShape);
                            echo "3";
                            session(["alert" => 'Archivos registrados con exito']);
                        } catch (ShapefileException $e) {
                            // Print detailed error information
                            // var_dump('Error '.$e->getCode().' ('.$e->getErrorType().'): '.$e->getMessage());
                            echo "2";
                            session(["alert" => 'Error '.$e->getCode().' ('.$e->getErrorType().'): '.$e->getMessage()]);
                            // session(["alert" => "El archivo shape tuvo un problema al ser leído por favor revisa tu archivo shape antes de adjuntarlo"]);
                        }
                    }
                    else
                    {
                        echo  "error2";
                    }
                } 
                else 
                {	
                    echo "Error1";
                }
            } catch (\ErrorException $th) {
    
                session(["alert" => $th]);
                echo "1";
            }
        }
    }

    public function getPredio(Request $request)
    {
        $predio = $this->OromapasRepository->getPredioById($request);
        return response()->json($predio);
    }

    public function getHidraulica(Request $request)
    {
        $hidraulica = $this->OromapasRepository->getHidraulicaById($request->id);
        return $hidraulica;
    }

    public function getPozo(Request $request)
    {
        $pozo = $this->OromapasRepository->getPozoById($request->id);
        return $pozo;
    }

    public function getSanitaria(Request $request)
    {
        $sanitaria = $this->OromapasRepository->getSanitariaById($request->id);
        return $sanitaria;
    }

    public function editPredio(Request $request)
    {
        // dd($request->all());
        $predio = $this->OromapasRepository->editPredio($request);
        return response()->json($predio);
    }

    public function editHidraulica(Request $request)
    {
        $hidraulica = $this->OromapasRepository->editHidraulica($request);
        return $hidraulica;
    }

    public function editPozo(Request $request)
    {
        $pozo = $this->OromapasRepository->editPozo($request);
        return $pozo;
    }

    public function editSanitaria(Request $request)
    {
        $sanitaria = $this->OromapasRepository->editSanitaria($request);
        return $sanitaria;
    }

    public function getPrediosLocalidad(Request $request)
    {
        $predios = $this->OromapasRepository->getLocalidad($request->localidad);
        return $predios;
    }

    public function updateCredit(Request $request)
    {
        $contador = $this->OromapasRepository->updateCredit($request);
        return redirect()->back()->with("alert", "Se actualizaron con exito: ". $contador["datos"]. " contratos de predios."  );
    }

    public function cutPolygon(Request $request)
    {
        return $this->OromapasRepository->cutPolygon($request);
    }

    public function deletePolygon(Request $request)
    {
        $this->OromapasRepository->deletePolygon($request);
    }

    public function createPolygon(Request $request)
    {
        // dd($request->all());
        $this->OromapasRepository->createPolygon($request);
        // session(["alert" => 'Poligono creado con exito']);
    }
}
