<?php

namespace App\Http\Controllers\Planning;
use Auth;

use PDF;
use carbon\Carbon;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


//Repositories
use App\Repositories\PlanningRepository;

class PlanningController extends Controller
{
    private $planningRepository;

    public function __construct(
        PlanningRepository $planningRepository
    ){
        $this->planningRepository = $planningRepository;
    }

    /******************************** Function for Assign Action Lines  Municipal Plan************************************/
    public function municipalPlan()
    {
        $axes = $this->planningRepository->getAxes();
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.planning.municipal_plan", compact("axes", "directions"));
    }

    public function assignMunicipalPlan(Request $request)
    {
        $this->validate($request, [
            'direction' => 'required',
            'actionlines' => 'required',
        ],[
            'direction.required' => 'Por favor seleccione la direccion a la que asignara las Lineas de Acción.',
            'actionlines.required' => 'Por favor seleccione almenos una Lineas de Acción.'
        ]);

        $this->planningRepository->assignActionLines($request);
        return redirect()->route("municipal.plan")->with("alert", "Se Asignaron correctamente las Lineas de Acción.");
    }
    
    public function finishedMunicipalPlan(Request $request)
    {
        $this->validate($request, [
            'actionlines' => 'required',
        ],[
            'actionlines.required' => 'Por favor seleccione almenos una Lineas de Acción.'
        ]);

        $this->planningRepository->finishedActionLines($request);
        return redirect()->route("municipal.plan")->with("alert", "Se Asignaron correctamente las Lineas de Acción.");
    }

    public function dettachMunicipalPlan(Request $request)
    {
        $this->planningRepository->dettachActionLines($request);
    }

    public function municipalPlanClient()
    {
        return view("moduls.planning.normatives_asigned");
    }

    public function getPmActionLineById(Request $request)
    {
        $data = $this->planningRepository->getPmActionLineById($request);
        $data->objective = $data->strategy->objective;
        $data->axe = $data->strategy->objective->axe;
        $data->normative = $data->strategy->objective->axe->normative;
        return $data;
    }

    public function getPmActionRejectLineById(Request $request)
    {
        $data = $this->planningRepository->getPmActionLineById($request);
        $data->objective = $data->strategy->objective;
        $data->axe = $data->strategy->objective->axe;
        $data->normative = $data->strategy->objective->axe->normative;
        $data->reject = $this->planningRepository->getPmActionRejectLineById($request->id);
        return $data;
    }

    public function acceptPmActionLine(Request $request)
    {
        $this->validate($request, [
            'id' => "required"
        ]);
        $this->planningRepository->acceptPmActionLine($request);
        return redirect()->back()->with("alert", "Se Asignaron correctamente las Lineas de Acción.");
    }

    public function rejectPmActionLine(Request $request)
    {
        $this->validate($request, [
            'description' => "required",
            'id_action_line' => "required",
            'file' => "required",
        ]);
        $this->planningRepository->rejectPmActionLine($request);
        return redirect()->back()->with("alert", "Se rechazo la Linea de Acción.");
    }

    /******************************** Function for Assign Action Lines  INAFED ************************************/

    public function inafed()
    {
        $schedules = $this->planningRepository->getSchedulesInafed();
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.planning.inafed", compact("schedules", "directions"));
    }

    public function assignInafed(Request $request)
    {
        $this->validate($request, [
            'direction' => 'required',
            'indicators' => 'required',
        ],[
            'direction.required' => 'Por favor seleccione la direccion a la que asignarán los Indicadores.',
            'indicators.required' => 'Por favor seleccione al menos un Indicador.'
        ]);

        $this->planningRepository->assignIndicators($request);
        return redirect()->route("inafed")->with("alert", "Se Asignaron correctamente los Indicadores.");
    }

    public function dettachInafed(Request $request)
    {
        $this->planningRepository->dettachInafed($request);
    }

    public function getInfIndicatorById(Request $request)
    {
        $data = $this->planningRepository->getInfIndicatorById($request);
        $data->axe = $data->topic->axe;
        $data->schedule = $data->topic->axe->schedule;
        $data->normative = $data->topic->axe->schedule->normative;
        return $data;
    }

    public function getInfIndicatorRejectLineById(Request $request)
    {
        $data = $this->planningRepository->getInfIndicatorById($request);
        $data->axe = $data->topic->axe;
        $data->schedule = $data->topic->axe->schedule;
        $data->normative = $data->topic->axe->schedule->normative;
        $data->reject = $this->planningRepository->getInfIndicatorRejectLineById($request->id);
        return $data;
    }

    public function acceptInfIndicator(Request $request)
    {
        $this->validate($request, [
            'id' => "required",
        ]);
        $this->planningRepository->acceptInfIndicator($request);
        return redirect()->back()->with("alert", "Se Asigno correctamente el Indicador.");
    }

    public function rejectInfIndicator(Request $request)
    {
        $this->validate($request, [
            'description' => "required",
            'id_inf_indicator' => "required",
            'file' => "required",
        ]);
        $this->planningRepository->rejectInfIndicator($request);
        return redirect()->back()->with("alert", "Se rechazo la Linea de Acción.");
    }

    /******************************** Function for Assign Action Lines  Mi ************************************/
    public function schedule()
    {
        $objectives = $this->planningRepository->getObjectivesSchedule();
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.planning.schedule", compact("objectives", "directions"));
    }

    public function assignSchedule(Request $request)
    {
        $this->validate($request, [
            'direction' => 'required',
            'metas' => 'required',
        ],[
            'direction.required' => 'Por favor seleccione la direccion a la que asignarán las Metas.',
            'metas.required' => 'Por favor seleccione al menos una Meta.'
        ]);

        $this->planningRepository->assignSchedule($request);
        return redirect()->route("schedule")->with("alert", "Se Asignaron correctamente las Metas.");
    }

    public function dettachSchedule(Request $request)
    {
        return $this->planningRepository->dettachSchedule($request);
    }

    public function getScheMetaById(Request $request)
    {
        $data = $this->planningRepository->getScheMetaById($request);
        $data->normative = $data->objective->normative;
        $data->objective = $data->objective;
        return $data;
    }

    public function getScheMetaRejectById(Request $request)
    {
        $data = $this->planningRepository->getScheMetaById($request);
        $data->normative = $data->objective->normative;
        $data->objective = $data->objective;
        $data->reject = $this->planningRepository->getScheMetaRejectById($request->id);
        return $data;
    }

    public function acceptScheMeta(Request $request)
    {
        $this->validate($request, [
            'id' => "required",
        ]);

        $this->planningRepository->acceptScheMeta($request);
        return redirect()->back()->with("alert", "Se Asigno correctamente la Meta.");
    }

    public function rejectScheMeta(Request $request)
    {
        $this->validate($request, [
            'description' => "required",
            'id_sche_meta' => "required",
            'file' => "required",
        ]);
        $this->planningRepository->rejectScheMeta($request);
        return redirect()->back()->with("alert", "Se rechazo la Meta.");
    }
    
    /******************************** Function for Assign Indicators of Mirs ************************************/
    public function mir()
    {
        $programs = $this->planningRepository->getProgramsMir();
        // $periods = $this->planningRepository->getProgramsMirPeriods();
        $types = $this->planningRepository->getMirIdicatorsTypes();
        $directions = $this->planningRepository->getAllDirections();
        // dd($programs);
        return view("moduls.planning.mir", compact("programs", "types", "directions"));
    }

    public function assignMir(Request $request)
    {
        $this->validate($request, [
            'direction' => 'required',
            'indicators' => 'required',
        ],[
            'direction.required' => 'Por favor seleccione la direccion a la que asignarán las Metas.',
            'indicators.required' => 'Por favor seleccione al menos un Indicadore.'
        ]);

        $this->planningRepository->assignMir($request);
        return redirect()->route("mir")->with("alert", "Se Asignaron correctamente las Metas.");
    }

    public function dettachMir(Request $request)
    {
        return $this->planningRepository->dettachMir($request);
    }

    public function getMirIndicatorById(Request $request)
    {
        $data = $this->planningRepository->getMirIndicatorById($request);
        $data->normative = $data->program->normative;
        $data->program = $data->program;
        $data->level = $data->level;
        return $data;
    }

    public function getMirIndicatorRejectById(Request $request)
    {
        $data = $this->planningRepository->getMirIndicatorById($request);
        $data->normative = $data->program->normative;
        $data->program = $data->program;
        $data->level = $data->level;
        $data->reject = $this->planningRepository->getMirIndicatorRejectById($request->id);
        return $data;
    }

    public function acceptMirIndicator(Request $request)
    {
        $this->validate($request, [
            'id' => "required",
            'direction' => "required",
        ]);
        $this->planningRepository->acceptMirIndicator($request);
        return redirect()->back()->with("alert", "Se Asigno correctamente la Meta.");
    }

    public function rejectMirIndicator(Request $request)
    {
        $this->validate($request, [
            'description' => "required",
            'id_mir_indicator' => "required",
            'file' => "required",
        ]);
        $this->planningRepository->rejectMirIndicator($request);
        return redirect()->back()->with("alert", "Se rechazo la Meta.");
    }

    /******************************** Function for make POA  ************************************/

    public function makePOA()
    {
        $direction = Auth::user()->profile->position->direction;
        return view("moduls.planning.make_program", compact("direction"));
    }

    public function storeProgram(Request $request)
    {
        $this->validate($request,[
            'name' => "required",
            'description' => "required",
            'direction_id' => "required"
        ]);
        $this->planningRepository->storeProgram($request);
        return redirect()->back()->with("alert", "Programa registrado con exito.");
    }
    
    public function updateProgram($id)
    {
        $program = $this->planningRepository->getProgramById($id);
        return view("moduls.planning.make_poa", compact("program"));
    }
    
    public function deleteProgram(Request $request)
    {
        $program = $this->planningRepository->deleteProgram($request);

        return $program;
    }

    public function newActionPoa($id)
    {
        $normatives = $this->planningRepository->getNormatives();
        $program = $this->planningRepository->getProgramById($id);
        $direction = $this->planningRepository->getDirectionById($program->direccion_id);
        return view("moduls.planning.make_poa_action", compact("normatives", "program", "direction"));
    }

    public function storeActionPoa(Request $request)
    {
        $this->validate($request, [
            "program" => 'required',
            "responsable" => 'required',
            "activity" => 'required',
            "unity" => 'required',
            "year" => 'required',
            "first" => 'required',
            "second" => 'required',
            "thirth" => 'required',
            "four" => 'required',
            "type" => 'required',
            "value" => 'required',
            "subactivity" => 'nullable',
            "subyear" => 'nullable',
            "subfirst" => 'nullable',
            "subsecond" => 'nullable',
            "subthirth" => 'nullable',
            "subfour" => 'nullable',
        ]);
        
        $this->planningRepository->storeActionPoa($request);
        return redirect()->route("update.program", $request->program)->with("alert", "Se créo una nueva actividad.");
    }

    public function updateActionPoa(Request $request)
    {
        $this->validate($request, [
            "pm_normatives" => 'nullable',
            "activity" => 'required',
            "responsable" => 'required',
            "unity" => 'required',
            "year" => 'required',
            "first" => 'required',
            "second" => 'required',
            "thirth" => 'required',
            "four" => 'required',
            "type" => 'required',
            "value" => 'required',
            "subactivity" => 'nullable',
            "subyear" => 'nullable',
            "subfirst" => 'nullable',
            "subsecond" => 'nullable',
            "subthirth" => 'nullable',
            "subfour" => 'nullable',
        ]);
        $this->planningRepository->updateActionPoa($request);
        return redirect()->route("update.program", $request->program)->with("alert", "Se actualizó una actividad.");
    }

    public function deleteActionPoa(Request $request)
    {
        $action = $this->planningRepository->deleteActionPoa($request);

        return $action;
    }

    public function getActionLinesByNormative(Request $request)
    {
        $actionLines = $this->planningRepository->getActionLinesByNormative($request);
        return $actionLines;
    }
    
    public function editActionPoa($id)
    {
        $normatives = $this->planningRepository->getNormatives();
        $action = $this->planningRepository->getActionPoaById($id);
        $program = $this->planningRepository->getProgramById($action->programas_id);
        $direction = $this->planningRepository->getDirectionById($program->direccion_id);
        return view("moduls.planning.edit_poa_action", compact("action", "normatives", "program", "direction"));
    }
    
    /******************************** Function for Assigned Normative ************************************/
    public function allPOA()
    {
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.planning.all_poas", compact("directions"));
    }
    
    public function viewPoaDirection($id)
    {
        $direction = $this->planningRepository->getDirectionById($id);
        return view("moduls.planning.make_program_direction", compact("direction"));
    }
    
    /******************************** Function for Assigned Normative ************************************/

    public function assignedNormative()
    {
        $periods = $this->planningRepository->getProgramsMirPeriods();
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.planning.all_normatives_asigned", compact("directions", "periods"));
    }
    
    

    public function generateMyPoa()
    {
        $date_now = Carbon::now('America/Mexico_City');
        $data = $this->planningRepository->getDataMyPoa();
        
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('Sistema de Encuestas');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 30);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 14);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Plan Operativo Anual ( POA )", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(15);
            $pdf->Cell(0, 0, "(". Auth::user()->profile->position->direction->nombre .")", 0, 0, 'C');
            $pdf->SetFont('', '', 10);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
            $pdf->SetY(13);
            $pdf->Cell(0, 0, 'Generado por: '. Auth::user()->name, 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.generate_poa')->with(compact('data'))->render();
        
        PDF::SetFont('', '', 10);
        
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::setHeaderCallback(function($pdf) {
            
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);

            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/mem_hor.png');
            $pdf->Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 14);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "AGENDA PARA EL DESARROLLO MUNICIPAL", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(15);
            $pdf->Cell(0, 0, "(". Auth::user()->profile->position->direction->nombre .")", 0, 0, 'C');
            $pdf->SetFont('', '', 10);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
            $pdf->SetY(13);
            $pdf->Cell(0, 0, 'Generado por: '. Auth::user()->name, 0, 0, 'R');
        });
        PDF::AddPage('L', 'LETTER');
        $html = view('partials.PDF.generate_poa_inf')->with(compact('data'))->render();
        
        PDF::SetFont('', '', 10);
        
        PDF::writeHTML($html, true, false, true, false, '');
        PDF::SetTitle("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("POA ".Auth::user()->profile->position->direction->nombre." a ".$date_now->format('d-M-Y').".pdf");
    }
}
