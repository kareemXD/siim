<?php

namespace App\Http\Controllers\Planning;
use Auth;

use PDF;
use carbon\Carbon;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


//Repositories
use App\Repositories\NewPlanningRepository;

class NewPlanningController extends Controller
{
    private $planningRepository;

    public function __construct(
        NewPlanningRepository $planningRepository
    ){
        $this->planningRepository = $planningRepository;
    }

    /******************************** Function for Assign Action Lines  Municipal Plan************************************/
    public function municipalPlan()
    {
        $axes = $this->planningRepository->getAxes();
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.new-planning.municipal_plan", compact("axes", "directions"));
    }
    
    public function getPmdObjectives(Request $request)
    {
        return $this->planningRepository->getObjectivesByDirection($request->id);
    }

    public function assignMunicipalPlan(Request $request)
    {
        $this->validate($request, [
            'direction' => 'required',
            'actionlines' => 'required',
        ],[
            'direction.required' => 'Por favor seleccione la direccion a la que asignara las Lineas de Acción.',
            'actionlines.required' => 'Por favor seleccione almenos una Lineas de Acción.'
        ]);

        $this->planningRepository->assignActionLines($request);
        return redirect()->route("new.municipal.plan")->with("alert", "Se Asignaron correctamente las Lineas de Acción.");
    }
    
    public function finishedMunicipalPlan(Request $request)
    {
        $this->validate($request, [
            'actionlines' => 'required',
        ],[
            'actionlines.required' => 'Por favor seleccione almenos una Lineas de Acción.'
        ]);

        $this->planningRepository->finishedActionLines($request);
        return redirect()->route("new.municipal.plan")->with("alert", "Se Asignaron correctamente las Lineas de Acción.");
    }

    public function dettachMunicipalPlan(Request $request)
    {
        $this->planningRepository->dettachActionLines($request);
    }

    public function normativeAssignedDirection()
    {
        return view("moduls.new-planning.normatives_asigned");
    }

    public function getPmActionLineById(Request $request)
    {
        $data = $this->planningRepository->getPmActionLineById($request);
        $data->objective = $data->strategy->objective;
        $data->axe = $data->strategy->objective->axe;
        $data->normative = $data->strategy->objective->axe->normative;
        return $data;
    }

    public function getPmActionRejectLineById(Request $request)
    {
        $data = $this->planningRepository->getPmActionLineById($request);
        $data->objective = $data->strategy->objective;
        $data->axe = $data->strategy->objective->axe;
        $data->normative = $data->strategy->objective->axe->normative;
        $data->reject = $this->planningRepository->getPmActionRejectLineById($request->id);
        return $data;
    }

    public function acceptPmActionLine(Request $request)
    {
        $this->validate($request, [
            'id' => "required"
        ]);
        $this->planningRepository->acceptPmActionLine($request);
        return redirect()->back()->with("alert", "Se Asignaron correctamente las Lineas de Acción.");
    }

    public function rejectPmActionLine(Request $request)
    {
        $this->validate($request, [
            'description' => "required",
            'id_action_line' => "required",
            'file' => "required",
        ]);
        $this->planningRepository->rejectPmActionLine($request);
        return redirect()->back()->with("alert", "Se rechazo la Linea de Acción.");
    }

    /******************************** Function for Assign Action Lines  INAFED ************************************/

    public function inafed()
    {
        $schedules = $this->planningRepository->getSchedulesInafed();
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.new-planning.inafed", compact("schedules", "directions"));
    }

    public function assignInafed(Request $request)
    {
        $this->validate($request, [
            'direction' => 'required',
            'indicators' => 'required',
        ],[
            'direction.required' => 'Por favor seleccione la direccion a la que asignarán los Indicadores.',
            'indicators.required' => 'Por favor seleccione al menos un Indicador.'
        ]);

        $this->planningRepository->assignIndicators($request);
        return redirect()->route("new.inafed")->with("alert", "Se Asignaron correctamente los Indicadores.");
    }

    public function dettachInafed(Request $request)
    {
        $this->planningRepository->dettachInafed($request);
    }

    public function getInfIndicatorById(Request $request)
    {
        $data = $this->planningRepository->getInfIndicatorById($request);
        $data->topic->axe;
        $data->topic->axe->schedule;
        $data->topic->axe->schedule->normative;
        return $data;
    }

    public function getInfIndicatorRejectLineById(Request $request)
    {
        $data = $this->planningRepository->getInfIndicatorById($request);
        $data->axe = $data->topic->axe;
        $data->schedule = $data->topic->axe->schedule;
        $data->normative = $data->topic->axe->schedule->normative;
        $data->reject = $this->planningRepository->getInfIndicatorRejectLineById($request->id);
        return $data;
    }

    public function acceptInfIndicator(Request $request)
    {
        $this->validate($request, [
            'id' => "required",
        ]);
        $this->planningRepository->acceptInfIndicator($request);
        return redirect()->back()->with("alert", "Se Asigno correctamente el Indicador.");
    }

    public function rejectInfIndicator(Request $request)
    {
        $this->validate($request, [
            'description' => "required",
            'id_inf_indicator' => "required",
            'file' => "required",
        ]);
        $this->planningRepository->rejectInfIndicator($request);
        return redirect()->back()->with("alert", "Se rechazo la Linea de Acción.");
    }

    /******************************** Function for Assign Action Lines  Mi ************************************/
    public function schedule()
    {
        $objectives = $this->planningRepository->getObjectivesSchedule();
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.new-planning.schedule", compact("objectives", "directions"));
    }

    public function assignSchedule(Request $request)
    {
        $this->validate($request, [
            'direction' => 'required',
            'metas' => 'required',
        ],[
            'direction.required' => 'Por favor seleccione la direccion a la que asignarán las Metas.',
            'metas.required' => 'Por favor seleccione al menos una Meta.'
        ]);

        $this->planningRepository->assignSchedule($request);
        return redirect()->route("new.schedule")->with("alert", "Se Asignaron correctamente las Metas.");
    }

    public function dettachSchedule(Request $request)
    {
        return $this->planningRepository->dettachSchedule($request);
    }

    public function getScheMetaById(Request $request)
    {
        $data = $this->planningRepository->getScheMetaById($request);
        $data->normative = $data->objective->normative;
        $data->objective = $data->objective;
        return $data;
    }

    public function getScheMetaRejectById(Request $request)
    {
        $data = $this->planningRepository->getScheMetaById($request);
        $data->normative = $data->objective->normative;
        $data->objective = $data->objective;
        $data->reject = $this->planningRepository->getScheMetaRejectById($request->id);
        return $data;
    }

    public function acceptScheMeta(Request $request)
    {
        $this->validate($request, [
            'id' => "required",
        ]);

        $this->planningRepository->acceptScheMeta($request);
        return redirect()->back()->with("alert", "Se Asigno correctamente la Meta.");
    }

    public function rejectScheMeta(Request $request)
    {
        $this->validate($request, [
            'description' => "required",
            'id_sche_meta' => "required",
            'file' => "required",
        ]);
        $this->planningRepository->rejectScheMeta($request);
        return redirect()->back()->with("alert", "Se rechazo la Meta.");
    }
    
    /******************************** Function for Assign Indicators of Mirs ************************************/
    public function mir()
    {
        $programs = $this->planningRepository->getProgramsMir();
        $axes = $this->planningRepository->getAxes();
        return view("moduls.new-planning.mir", compact("programs", "axes"));
    }

    function storeProgramMir(Request $request){
        $this->planningRepository->storeProgramMir($request);
        return redirect()->route("new.mir")->with("alert", "Programa registrado con Éxito.");
    }
   
    function updateProgramMir(Request $request){
        $this->planningRepository->updateProgramMir($request);
        return redirect()->route("new.mir")->with("alert", "Programa actualizado con Éxito.");
    }
    
    function getProgramMir(Request $request){
        return $this->planningRepository->getProgramMir($request->id);
    }

    public function assignMir(Request $request)
    {
        $this->validate($request, [
            'direction' => 'required',
            'indicators' => 'required',
        ],[
            'direction.required' => 'Por favor seleccione la direccion a la que asignarán las Metas.',
            'indicators.required' => 'Por favor seleccione al menos un Indicadore.'
        ]);

        $this->planningRepository->assignMir($request);
        return redirect()->route("mir")->with("alert", "Se Asignaron correctamente las Metas.");
    }

    public function dettachMir(Request $request)
    {
        return $this->planningRepository->dettachMir($request);
    }

    public function getMirIndicatorById(Request $request)
    {
        $data = $this->planningRepository->getMirIndicatorById($request);
        $data->normative = $data->program->normative;
        $data->program = $data->program;
        $data->level = $data->level;
        return $data;
    }

    public function getMirIndicatorRejectById(Request $request)
    {
        $data = $this->planningRepository->getMirIndicatorById($request);
        $data->normative = $data->program->normative;
        $data->program = $data->program;
        $data->level = $data->level;
        $data->reject = $this->planningRepository->getMirIndicatorRejectById($request->id);
        return $data;
    }

    public function acceptMirIndicator(Request $request)
    {
        $this->validate($request, [
            'id' => "required",
            'direction' => "required",
        ]);
        $this->planningRepository->acceptMirIndicator($request);
        return redirect()->back()->with("alert", "Se Asigno correctamente la Meta.");
    }

    public function rejectMirIndicator(Request $request)
    {
        $this->validate($request, [
            'description' => "required",
            'id_mir_indicator' => "required",
            'file' => "required",
        ]);
        $this->planningRepository->rejectMirIndicator($request);
        return redirect()->back()->with("alert", "Se rechazo la Meta.");
    }

    /******************************** Function for make POA  ************************************/

    public function makeMirArea()
    {
        $direction = Auth::user()->profile->position->direction;
        $programs = $this->planningRepository->getProgramsMir();
        return view("moduls.new-planning.make_area", compact("direction", "programs"));
    }

    public function storeArea(Request $request){
        $this->validate($request,[
            'direction_id' => "required",
            'name' => "required",
            'program' => "required",
            'description' => "required"
        ]);
        $this->planningRepository->storeArea($request);
        return redirect()->back()->with("alert", "Programa registrado con exito.");
    }
    
    public function updateArea(Request $request){
        $this->validate($request,[
            'area_id' => "required",
            'name' => "required",
            'program' => "required",
            'description' => "required"
        ]);
        $this->planningRepository->updateArea($request);
        return redirect()->back()->with("alert", "Programa registrado con exito.");
    }

    public function makeMir($id = null)
    {
        $area = $this->planningRepository->getAreaById($id);
        $direction = $this->planningRepository->getDirectionById($area->direccion_id);
        $normatives = $this->planningRepository->getNormatives();
        return view("moduls.new-planning.make_component", compact("area", "direction", "normatives"));
    }

    public function storeComponent(Request $request)
    {
        // dd($request);
        $this->validate($request,[
            'area' => "required",
            'activity' => "required",
            'objetivo' => "required",
            'direction' => "required"
        ]);
        $this->planningRepository->storeComponent($request);
        return redirect()->back()->with("alert", "Componente registrado con exito.");
    }
    
    public function editComponent(Request $request)
    {
        // dd($request);
        $this->validate($request,[
            'activity' => "required",
            'objetivo' => "required"
        ]);

        $this->planningRepository->editComponent($request);
        return redirect()->back()->with("alert", "Componente actualizado con exito.");
    }
    
    public function getComponentById(Request $request)
    {
        $program = $this->planningRepository->getComponentById($request->component);
        $program->pm_action_lines = $program->pm_action_lines;
        $program->inf_action_lines = $program->inf_action_lines;
        $program->sche_metas = $program->sche_metas;
        return $program;
    }

    public function updateComponent($id)
    {
        $program = $this->planningRepository->getComponentById($id);
        return view("moduls.new-planning.view_action_component", compact("program"));
    }
    
    public function deleteComponent(Request $request)
    {
        $program = $this->planningRepository->deleteComponent($request);

        return $program;
    }

    public function newActionComponent($id)
    {
        $normatives = $this->planningRepository->getNormatives();
        $component = $this->planningRepository->getComponentById($id);
        $direction = $this->planningRepository->getDirectionById($component->direction_id);
        return view("moduls.new-planning.make_action_component", compact("normatives", "component", "direction"));
    }

    public function storeActionComponent(Request $request)
    {
        $this->validate($request, [
            "component" => 'required',
            "responsable" => 'required',
            "activity" => 'required',
            "unity" => 'required',
            "year" => 'required',
            "first" => 'required',
            "second" => 'required',
            "thirth" => 'required',
            "four" => 'required',
            "type" => 'required',
            "value" => 'required',
        ]);
        
        $this->planningRepository->storeActionComponent($request);
        return redirect()->route("new.update.component", $request->component)->with("alert", "Se créo una nueva actividad.");
    }

    public function updateActionComponent(Request $request)
    {
        $this->validate($request, [
            "pm_normatives" => 'nullable',
            "activity" => 'required',
            "responsable" => 'required',
            "unity" => 'required',
            "year" => 'required',
            "first" => 'required',
            "second" => 'required',
            "thirth" => 'required',
            "four" => 'required',
            "type" => 'required',
            "value" => 'required',
        ]);
        $this->planningRepository->updateActionComponent($request);
        return redirect()->route("new.update.component", $request->component)->with("alert", "Se actualizó una actividad.");
    }

    public function deleteActionComponent(Request $request)
    {
        $action = $this->planningRepository->deleteActionComponent($request);

        return $action;
    }

    public function getActionLinesByNormative(Request $request)
    {
        $actionLines = $this->planningRepository->getActionLinesByNormative($request);
        return $actionLines;
    }
    
    public function editActionComponent($id)
    {
        $action = $this->planningRepository->getActionComponentById($id);
        $normatives = $this->planningRepository->getNormatives();
        $component = $this->planningRepository->getComponentById($action->componente_id);
        $direction = $this->planningRepository->getDirectionById($component->direction_id);
        return view("moduls.new-planning.edit_component_action", compact("action", "normatives", "component", "direction"));
    }
    
    /******************************** Function for Assigned Normative ************************************/

    public function assignedNormative()
    {
        $periods = $this->planningRepository->getProgramsMirPeriods();
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.new-planning.all_normatives_asigned", compact("directions", "periods"));
    }

    public function allMir()
    {
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.new-planning.all_mirs", compact("directions"));
    }
    
    public function viewMirDirection($id)
    {
        $direction = $this->planningRepository->getDirectionById($id);
        $programs = $this->planningRepository->getProgramsMir();
        return view("moduls.new-planning.make_area", compact("direction", "programs"));
    }

    public function deleteArea(Request $request)
    {
        $action = $this->planningRepository->deleteAreaMir($request);
        return $action;
    }

    //obtener la informacion de las actividades por id
    public function getActionMirById($id)
    {
        dd($id);
    }
}
