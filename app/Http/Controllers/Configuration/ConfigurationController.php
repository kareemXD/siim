<?php

namespace App\Http\Controllers\Configuration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Services
use Auth;

//Repositories
use App\Repositories\ConfigurationRepository;

class ConfigurationController extends Controller
{
    private $configurationRepository;

    public function __construct(
        ConfigurationRepository $configurationRepository
    ){
        $this->configurationRepository = $configurationRepository;
    }

    public function MetadataDirection()
    {
        $metadata = $this->configurationRepository->getMetadataByDirectionId(Auth::user()->profile->position->direction->id);
        return view("settings.data_direction", compact("metadata"));
    }

    public function updateMetadataDirection(Request $request)
    {
        $this->configurationRepository->updateMetadataDirection($request);
        return redirect()->route("metadata.direction")->with("alert", "Se actualizaron correctamente los datos de la Dependencia");
    }

    public function cofigPOA()
    {
        $positions = $this->configurationRepository->getPositionsWithUserByAuthDirection();
        return view("settings.poa", compact("positions"));
    }
}
