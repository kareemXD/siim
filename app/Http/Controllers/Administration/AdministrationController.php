<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Repositories
use App\Repositories\AdministrationRepository;
use App\Repositories\ReportRepository;

class AdministrationController extends Controller
{
    private $administrationRepository,
            $reportRepository;

    public function __construct(
        AdministrationRepository $administrationRepository,
        ReportRepository $reportRepository
    ){
        $this->administrationRepository = $administrationRepository;
        $this->reportRepository = $reportRepository;

    }
    public function dashboard()
    {
        // $data = $this->reportRepository->getDataMyPoa();
        return view('administration.dashboard');
    }
}
