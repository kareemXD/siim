<?php

namespace App\Http\Controllers\Administration;

use PDF;
use Carbon\Carbon;
use Storage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ReponsePmduNotification;
//Repositories
use App\Repositories\AdministrationRepository;

class PmduController extends Controller
{
    private $administrationRepositoryy;

    public function __construct(
        AdministrationRepository $administrationRepository
    ){
        $this->administrationRepository = $administrationRepository;
    }

    public function observations()
    {
        $observations = $this->administrationRepository->getAllObservations();
        $sectores = $this->administrationRepository->getAllSectores();
        $locations = $this->administrationRepository->getAllLocations();
        return view('polls.observation_pmdu', compact('observations', "sectores", "locations"));
    }
    
    public function gatDataObservation(Request $request)
    {
        $observation = $this->administrationRepository->gatDataObservation($request->id);
        return $observation->load("sections");
    }

    public function stadistics()
    {
        $stadistics = $this->administrationRepository->gatDatastadistics();
        return view('polls.stadistics_pmdu', compact('stadistics'));
    }
    
    public function stadisticsPerAxe()
    {
        $stadistics = $this->administrationRepository->gatDatastadistics("axe");
        return view('polls.stadistics_axe_pmdu', compact('stadistics'));
    }

    public function responseObservationPmdu(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            "id" => "required",
            "response" => "nullable",
            "procede" => "required",
            "resend" => "required",
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with("alert", false);
        }

        $this->administrationRepository->responseObservationPmdu($request);
        // session(["page_start" => $request->page]);
        return redirect()->back()->with("alert", true)->with("page_start", $request->page);
    }
    
    public function addObservationPmdu(Request $request)
    {
        // dd($request); 
        $validator = Validator::make($request->all(), [
            "folio" => "required",
            "origin" => "required",
            "procedence" => "required",
        ]);
        if ($validator->fails()) {
            return redirect()->back()->with("alert", false);
        }
        $this->administrationRepository->addObservationPmdu($request);
        return redirect()->back()->with("alert", true);
    }

    public function generatePdfResponse($id)
    {
        // if($request->file('file'))
        // {
        //     $path = Storage::disk('signature')->putFileAs('signature', $request->file('file'), "firma.png");
        //     $path = public_path('assets/signature/firma.png');
        // }
        $meses = ['', 'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $date_now = Carbon::now('America/Mexico_City');
        $data = $this->administrationRepository->gatDataObservation($id);
        if(!$data->firmada){

            PDF::SetCreator("IMPLAN");
            PDF::SetAuthor('SIIM');
            PDF::SetTitle('POA');
            PDF::SetSubject('POA');
            PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            PDF::SetHeaderMargin(0);
            PDF::SetFooterMargin(0);
            PDF::SetAutoPageBreak(TRUE, 22);
            PDF::setFooterData(array(0,64,0), array(0,64,128));
            PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            PDF::setHeaderCallback(function($pdf) use($date_now, $meses, $data) {
                //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
                // get the current page break margin
                $bMargin = $pdf->getBreakMargin();
                // get current auto-page-break mode
                $auto_page_break = $pdf->getAutoPageBreak();
                // disable auto-page-break
                $pdf->SetAutoPageBreak(false, 0);
                // set bacground image
                $imagen = public_path('assets/images/Hoja_Membretada.png');
                $pdf->Image($imagen, 0, 0, 216, 279, '', '', '', false, 300, '', false, false, 0);
                // restore auto-page-break status
                $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
                // set the starting point for the page content
                $pdf->setPageMark();
            });
            PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            PDF::AddPage('H', 'LETTER');
            PDF::SetFont('', 'B', 10);
            PDF::SetY(30);
            PDF::Cell(0, 0, "Valle de Banderas, Bahía de Banderas, Nayarit.", 0, 0, 'R');
            PDF::SetY(35);
            PDF::Cell(0, 0, $date_now->format("d"). " de ".$meses[intval($date_now->format("m"))]. " del ".$date_now->format("Y"), 0, 0, 'R');
            PDF::SetY(40);
            PDF::Cell(0, 0, "Numero de Oficio: ". $data->folio, 0, 0, 'R');
            PDF::SetY(60);
            PDF::Cell(0, 0, "C. ".$data->nombre." ".$data->apellidos, 0, 0, 'L');
            PDF::SetY(65);
            PDF::Cell(0, 0, $data->direccion.", ".$data->localidad, 0, 0, 'L');
            PDF::SetY(70);
            PDF::Cell(0, 0, $data->correo, 0, 0, 'L');
            PDF::SetY(75);
            PDF::Cell(0, 0, $data->telefono, 0, 0, 'L');
            PDF::SetY(80);
            PDF::Cell(0, 0, "P R E S E N T E", 0, 0, 'L');
            // dd($data);
            PDF::SetFont('', 'R', 10);
            switch ($data->procede) {
                case 'Procede':
                    PDF::SetY(85);
                    $html = '<span style="text-align:justify;"><p style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nos referimos al planteamiento que formuló en ocasión de la consulta pública para la actualización del plan municipal de desarrollo urbano  
                            de Bahía de Banderas en el que señala:</p>
                            <p style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$data->observaciones.'</p>
                            <p style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Al respecto, con apego a lo dispuesto en los artículos 1 párrafo primero, 6, 8, 115 fracción VI de la Constitución Política de los 
                            Estados Unidos Mexicanos, 18,52 fracciones III y IV y 59 de la Ley de Asentamientos Humanos y Desarrollo Urbano para el Estado de Nayarit, (publicada en el 
                            periódico oficial del estado de Nayarit de 7 de agosto de 1999), así como, en  lo preceptuado en el noveno transitorio de la Ley de Asentamientos Humano, 
                            Ordenamiento Territorial y Desarrollo Urbano para el Estado de Nayarit, se informa que las observaciones, críticas y propociones concretamente planteadas 
                            fueron consideradas y una vez realizadas las modificaciones o adecuaciones procedentes al proyecto (versión abreviada y técnica) para la actualización del 
                            plan municipal de desarrollo urbano de Bahía de Banderas, Nayarit, el documento se turna al Consejo Consultivo Municipal de Desarrollo Urbano y Rural, para 
                            que proceda a emitir su opinión de conformidad al trámite legal correspondiente.</p>
                            <p style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;De antemano agradecemos sus valiosas aportacionesy el interás por participar en la construcción de este importante instrumento 
                            de planeación urbana.</p></span>';
                    PDF::writeHTML($html, true, 0, true, true);
                    $html = '<p style="text-align:center;">Atentamente<br>“Juntos Progresamos”</p>
                            <table cellspacing="5"><tr>
                                <td><br/><br/><br/><br/><br/><b>Mtro. Matías Verdín Heras</b> <br/>Director de Ordenamiento Territorial, Desarrollo Urbano y Medio Ambiente.</td>
                                <td><br/><br/><br/><br/><br/><b>Dra. Beatriz Eugenia Mártinez Sánchez</b> <br/>Directora General del Instituto Municipal de Planeación.</td>
                            </tr></table>';
                    PDF::writeHTML($html, true, 0, true, true, 'C');
                    break;
                
                case 'Procede Parcialmente':
                    PDF::SetY(85);
                    $html = '<span style="text-align:justify;"><p style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nos referimos al planteamiento que formuló en ocasión de la consulta pública para la actualización del plan municipal de desarrollo urbano  
                            de Bahía de Banderas en el que señala:</p>
                            <p style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$data->observaciones.'</p>
                            <p style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Al respecto, con apego a lo dispuesto en los artículos 1 párrafo primero, 6, 8, 115 fracción VI de la Constitución Política de los 
                            Estados Unidos Mexicanos, 18,52 fracciones III y IV y 59 de la Ley de Asentamientos Humanos y Desarrollo Urbano para el Estado de Nayarit, (publicada en el 
                            periódico oficial del estado de Nayarit de 7 de agosto de 1999), así como, en  lo preceptuado en el noveno transitorio de la Ley de Asentamientos Humano, 
                            Ordenamiento Territorial y Desarrollo Urbano para el Estado de Nayarit, se informa que las observaciones, críticas y propociones concretamente planteadas 
                            fueron consideradas y una vez realizadas las modificaciones o adecuaciones procedentes al proyecto (versión abreviada y técnica) para la actualización del 
                            plan municipal de desarrollo urbano de Bahía de Banderas, Nayarit, el documento se turna al Consejo Consultivo Municipal de Desarrollo Urbano y Rural, para 
                            que proceda a emitir su opinión de conformidad al trámite legal correspondiente.</p>
                            <p style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;De antemano agradecemos sus valiosas aportacionesy el interás por participar en la construcción de este importante instrumento 
                            de planeación urbana.</p></span>';
                    PDF::writeHTML($html, true, 0, true, true);
                    $html = '<p style="text-align:center;">Atentamente<br>“Juntos Progresamos”</p>
                            <table cellspacing="5"><tr>
                                <td><br/><br/><br/><br/><br/><b>Mtro. Matías Verdín Heras</b> <br/>Director de Ordenamiento Territorial, Desarrollo Urbano y Medio Ambiente.</td>
                                <td><br/><br/><br/><br/><br/><b>Dra. Beatriz Eugenia Mártinez Sánchez</b> <br/>Directora General del Instituto Municipal de Planeación.</td>
                            </tr></table>';
                    PDF::writeHTML($html, true, 0, true, true, 'C');
                    break;
    
                case "No Procede Rev":
                case "No Procede":
                    PDF::SetY(85);
                    $html = '<span style="text-align:justify;"><p style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nos referimos al planteamiento que formuló en ocasión de la consulta pública para la actualización del plan municipal de desarrollo urbano  
                            de Bahía de Banderas en el que señala:</p>
                            <p style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$data->observaciones.'</p>
                            <p style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Al respecto, con apego a lo dispuesto en los artículos 1 párrafo primero, 6, 8, 115 fracción VI de la Constitución Política de los 
                            Estados Unidos Mexicanos, 18,52 fracciones III y IV y 59 de la Ley de Asentamientos Humanos y Desarrollo Urbano para el Estado de Nayarit, (publicada en el 
                            periódico oficial del estado de Nayarit de 7 de agosto de 1999), así como, en  lo preceptuado en el noveno transitorio de la Ley de Asentamientos Humano, 
                            Ordenamiento Territorial y Desarrollo Urbano para el Estado de Nayarit, se informa que las observaciones, críticas y propociones concretamente planteadas 
                            fueron consideradas y una vez realizadas las modificaciones o adecuaciones procedentes al proyecto (versión abreviada y técnica) para la actualización del 
                            plan municipal de desarrollo urbano de Bahía de Banderas, Nayarit, el documento se turna al Consejo Consultivo Municipal de Desarrollo Urbano y Rural, para 
                            que proceda a emitir su opinión de conformidad al trámite legal correspondiente.</p>
                            <p style="text-align:justify;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;De antemano agradecemos sus valiosas aportacionesy el interás por participar en la construcción de este importante instrumento 
                            de planeación urbana.</p></span>';
                    PDF::writeHTML($html, true, 0, true, true);
                    $html = '<p style="text-align:center;">Atentamente<br>“Juntos Progresamos”</p>
                            <table cellspacing="5"><tr>
                                <td><br/><br/><br/><br/><br/><b>Mtro. Matías Verdín Heras</b> <br/>Director de Ordenamiento Territorial, Desarrollo Urbano y Medio Ambiente.</td>
                                <td><br/><br/><br/><br/><br/><b>Dra. Beatriz Eugenia Mártinez Sánchez</b> <br/>Directora General del Instituto Municipal de Planeación.</td>
                            </tr></table>';
                    PDF::writeHTML($html, true, 0, true, true, 'C');
                    break;
    
                case "Sin Aporte":
                    PDF::SetY(100);
                    $html = '<span style="text-align:justify;"><p style="text-align:justify;">Por este conducto reciba un cordial saludo, a su vez, agradecemos su participación en la consulta 
                             pública para la actualización del Plan Municipal de Desarrollo Urbano de Bahía de Banderas, Nayarit, a través de los comentarios plasmados
                             en la Bitácora de Observaciones con folio <b>'.$data->folio.'</b>.</p>
                             <p style="text-align:justify;">Lo invitamos a continuar participando en los procesos de planeación y desarrollo municipal, ya que estamos 
                             convencidos que, la participación social en las acciones de gobierno asegura la transcendencia y continuidad de proyectos necesarios para 
                             lograr un municipio ordenado, sustentable y resiliente, que eleve la calidad de vida de los bahiabanderenses. Sin otro particular por el 
                             momento, quedo a sus órdenes.</p></span>';
                    PDF::writeHTML($html, true, 0, true, true);
                    $html = '<br><br><p style="text-align:center;">Atentamente, su amigo y servidor</p><br><br><br>
                             <p style="text-align:center;"><b>DR. JAIME ALONSO CUEVAS TELLO<b/></p>
                             <p style="text-align:center;">PRESIDENTE MUNICIPAL DEL H. X. AYUNTAMIENTO DE BAHÍA DE BANDERAS, NAYARIT</p>';
                    PDF::writeHTML($html, true, 0, true, true, 'C');
                    break;
    
                case "Relativo a otra área":
                    PDF::SetY(100);
                    $html = '<span style="text-align:justify;"><p style="text-align:justify;">Reciba un cordial saludo, por otro lado, con relación a la opinión vertida en el folio <b>'.$data->folio.'</b> de la bitácora 
                             de la consulta pública para la actualización del Plan Municipal de Desarrollo Urbano, respecto de la cual solicita:</p>
                             <p style="text-align:justify;">'.$data->observaciones.'</p>
                             <p style="text-align:justify;">Al respecto se informa: </p>
                             <p style="text-align:justify;">'.$data->respuesta.'</p></span>';
                    PDF::writeHTML($html, true, 0, true, true);
                    
                    $html = '<br><br><p style="text-align:center;">Atentamente, su amigo y servidor</p><br><br><br>
                             <p style="text-align:center;"><b>DR. JAIME ALONSO CUEVAS TELLO<b/></p>
                             <p style="text-align:center;">PRESIDENTE MUNICIPAL DEL H. X. AYUNTAMIENTO DE BAHÍA DE BANDERAS, NAYARIT</p>';
                    PDF::writeHTML($html, true, 0, true, true, "C");
                    break;
            }
            PDF::SetFont('', 'B', 8);
            $copys = explode(";", $data->copy);
            $html = '<br><p>';
            foreach ($copys as $key => $copy) {
                # code...
                $html .= '<b>C.c.p /conocimiento efectos/ '.$copy.'<b/><br />';
            }
            $html .= '</p>';
            PDF::writeHTML($html, true, 0, true, true);
            // $html = view('partials.PDF.generate_ofice_response_pmdu')->with(compact('data'))->render();
            // PDF::SetFont('', '', 10);
            // PDF::writeHTML($html, true, false, true, false, '');
            // Storage::disk('signature')->delete("signature/firma.png");
            PDF::SetTitle($data->folio." Respuesta.pdf");
            PDF::Output($data->folio." Respuesta.pdf");
        }else{

            $file = storage_path('app/public/signatured')."/".$data->folio."-Respuesta.pdf";
            return response()->file($file);
        }
    }

    public function signatureObservationPmdu(Request $request)
    {
        
        $counters = ["firmas" => 0, "correos" => 0];
        if($request->file('file'))
        {
            $path = Storage::disk('signature')->putFileAs('signature', $request->file('file'), "firma.png");
            $path = public_path('assets/signature/firma.png');
        }
        if(count($request->folio) > 1){
            $observations = $this->administrationRepository->getAllObservationsForSing($request->folio);
        }elseif(count($request->folio) >= 1 && $request->folio[0] == null){
            $observations = $this->administrationRepository->getAllObservationsForSing();
        }
        // dd($observations);
        // dd("no Entro");
        $meses = ['', 'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $date_now = Carbon::now('America/Mexico_City');
        foreach ($observations as $key => $observation) {
            PDF::reset();
            PDF::SetCreator("IMPLAN");
            PDF::SetAuthor('SIIM');
            PDF::SetTitle('POA');
            PDF::SetSubject('POA');
            PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            PDF::SetHeaderMargin(0);
            PDF::SetFooterMargin(0);
            PDF::SetAutoPageBreak(TRUE, 22);
            PDF::setFooterData(array(0,64,0), array(0,64,128));
            PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
            PDF::setHeaderCallback(function($pdf) use($date_now, $meses, $observation) {
                //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
                // get the current page break margin
                $bMargin = $pdf->getBreakMargin();
                // get current auto-page-break mode
                $auto_page_break = $pdf->getAutoPageBreak();
                // disable auto-page-break
                $pdf->SetAutoPageBreak(false, 0);
                // set bacground image
                $imagen = public_path('assets/images/Hoja_Membretada.png');
                $pdf->Image($imagen, 0, 0, 216, 279, '', '', '', false, 300, '', false, false, 0);
                // restore auto-page-break status
                $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
                // set the starting point for the page content
                $pdf->setPageMark();
            });
            PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            PDF::AddPage('H', 'LETTER');
            PDF::SetFont('', 'B', 10);
            PDF::SetY(30);
            PDF::Cell(0, 0, "Valle de Banderas, Bahía de Banderas, Nayarit.", 0, 0, 'R');
            PDF::SetY(35);
            PDF::Cell(0, 0, $date_now->format("d"). " de ".$meses[intval($date_now->format("m"))]. " del ".$date_now->format("Y"), 0, 0, 'R');
            PDF::SetY(40);
            PDF::Cell(0, 0, "Numero de Oficio: ". $observation->folio, 0, 0, 'R');
            PDF::SetY(60);
            PDF::Cell(0, 0, "C. ".$observation->nombre." ".$observation->apellidos, 0, 0, 'L');
            PDF::SetY(65);
            PDF::Cell(0, 0, $observation->direccion.", ".$observation->localidad, 0, 0, 'L');
            PDF::SetY(70);
            PDF::Cell(0, 0, $observation->correo, 0, 0, 'L');
            PDF::SetY(75);
            PDF::Cell(0, 0, $observation->telefono, 0, 0, 'L');
            PDF::SetY(80);
            PDF::Cell(0, 0, "P R E S E N T E", 0, 0, 'L');
            // dd($observation);
            PDF::SetFont('', 'R', 10);
            switch ($observation->procede) {
                case "Sin Aporte":
                    PDF::SetY(100);
                    $html = '<span style="text-align:justify;"><p style="text-align:justify;">Por este conducto reciba un cordial saludo, a su vez, agradecemos su participación en la consulta 
                            pública para la actualización del Plan Municipal de Desarrollo Urbano de Bahía de Banderas, Nayarit, a través de los comentarios plasmados
                            en la Bitácora de Observaciones con folio <b>'.$observation->folio.'</b>.</p>
                            <p style="text-align:justify;">Lo invitamos a continuar participando en los procesos de planeación y desarrollo municipal, ya que estamos 
                            convencidos que, la participación social en las acciones de gobierno asegura la transcendencia y continuidad de proyectos necesarios para 
                            lograr un municipio ordenado, sustentable y resiliente, que eleve la calidad de vida de los bahiabanderenses. Sin otro particular por el 
                            momento, quedo a sus órdenes.</p></span>';
                    PDF::writeHTML($html, true, 0, true, true);
                    $html = '<br><br><p style="text-align:center;">Atentamente, su amigo y servidor</p>
                            <p style="text-align:center;"><img src="'.$path.'"/ height="100px"></p>
                            <p style="text-align:center;"><b>DR. JAIME ALONSO CUEVAS TELLO<b/></p>
                            <p style="text-align:center;">PRESIDENTE MUNICIPAL DEL H. X. AYUNTAMIENTO DE BAHÍA DE BANDERAS, NAYARIT</p>';
                    PDF::writeHTML($html, true, 0, true, true, 'C');
                    break;

                case "Relativo a otra área":
                    PDF::SetY(100);
                    $html = '<span style="text-align:justify;"><p style="text-align:justify;">Reciba un cordial saludo, por otro lado, con relación a la opinión vertida en el folio <b>'.$observation->folio.'</b> de la bitácora 
                            de la consulta pública para la actualización del Plan Municipal de Desarrollo Urbano, respecto de la cual solicita:</p>
                            <p style="text-align:justify;">'.$observation->observaciones.'</p>
                            <p style="text-align:justify;">Al respecto se informa: </p>
                            <p style="text-align:justify;">'.$observation->respuesta.'</p></span>';
                    PDF::writeHTML($html, true, 0, true, true);
                    
                    $html = '<br><br><p style="text-align:center;">Atentamente, su amigo y servidor</p>
                            <p style="text-align:center;"><img src="'.$path.'"/ height="100px"></p>
                            <p style="text-align:center;"><b>DR. JAIME ALONSO CUEVAS TELLO<b/></p>
                            <p style="text-align:center;">PRESIDENTE MUNICIPAL DEL H. X. AYUNTAMIENTO DE BAHÍA DE BANDERAS, NAYARIT</p>';
                    PDF::writeHTML($html, true, 0, true, true, "C");
                    break;
            }
            PDF::SetFont('', 'B', 8);
            $copys = explode(";", $observation->copy);
            $html = '<br><p>';
            foreach ($copys as $key => $copy) {
                # code...
                $html .= '<b>C.c.p /conocimiento efectos/ '.$copy.'<b/><br />';
            }
            $html .= '</p>';
            PDF::writeHTML($html, true, 0, true, true);
            PDF::SetTitle($observation->folio." Respuesta.pdf");
            Storage::disk('local')->delete("public/signatured/".$observation->folio."-Respuesta.pdf");
            $file = storage_path('app/public/signatured')."/".$observation->folio."-Respuesta.pdf";
            PDF::Output($file, 'F');
            $counters["firmas"] ++;
            $observation->firmada = true;
            if($request->observation_action == "1")
            {
                if ($observation->correo && $observation->enviado == 0) {
                    Mail::send('emails.pmdu_responsed', ['data' => $observation], function($message) use ($file, $observation) {
                        $message->to($observation->correo);
                        $message->subject("Atención de Observaciones al PMDU");
                        $message->attach($file);
                    },true);
					// Mail::to($observation->correo)->send(new ReponsePmduNotification($observation));
					if (!Mail::failures()) {
                        $observation->enviado = 1;
                        $counters["correos"] ++;
					}
				}
            }
            $observation->update();
            // dd($observation);
        }
        Storage::disk('signature')->delete("signature/firma.png");
        $alert = "Acción realizada con exito, se firmaron ".$counters["firmas"]." oficios";
        if($counters["correos"] > 0){
            $alert .= " y enviaron ".$counters["correos"]." correos.";
        }

        return redirect()->back()->with("alert_sign", $alert);
    }

    public function sendOficces(Request $request)
    {
        $counters = ["firmas" => 0, "correos" => 0];
        $observations = $this->administrationRepository->getAllObservationsForSing(null, $request->procedence);
        // dd($observations);
        foreach ($observations as $key => $observation) {
            if ($observation->correo) {
                $file = storage_path('app/public/signatured')."/".$observation->folio."-Respuesta.pdf";
                if (Storage::disk("public")->exists('signatured/'.$observation->folio."-Respuesta.pdf")) {
                    if($observation->enviado == 0){
                        Mail::send('emails.pmdu_responsed', ['data' => $observation], function($message) use ($file, $observation) {
                            $message->to($observation->correo);
                            $message->subject("Atención de Observaciones al PMDU");
                            $message->attach($file);
                        },true);
                        // Mail::to($observation->correo)->send(new ReponsePmduNotification($observation));
                        if (!Mail::failures()) {
                            $observation->enviado = 1;
                            $counters["correos"] ++;
                        }
                    }else{
                        if($request->send_again == true){
                            Mail::send('emails.pmdu_responsed', ['data' => $observation], function($message) use ($file, $observation) {
                                $message->to($observation->correo);
                                $message->subject("Atención de Observaciones al PMDU");
                                $message->attach($file);
                            },true);
                            // Mail::to($observation->correo)->send(new ReponsePmduNotification($observation));
                            if (!Mail::failures()) {
                                $observation->enviado = 1;
                                $counters["correos"] ++;
                            }
                        }
                    }
                    $observation->update();
                    if(env('MAIL_HOST', false) == 'smtp.mailtrap.io'){
                        sleep(1); //use usleep(500000) for half a second or less
                    }
                }
            }
        }

        $alert = "Acción realizada con exito, se enviaron ".$counters["correos"]." correos.";
        return redirect()->back()->with("alert_sign", $alert);
    }

    public function exportToPdf(Request $request)
    {
        $observations = $this->administrationRepository->getAllObservations($request->all());
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('POA');
        PDF::SetSubject('POA');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 30);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf) {
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0);
            // set bacground image
            $imagen = public_path('assets/images/Hoja_Membretada.png');
            $pdf->Image($imagen, 0, 0, 216, 279, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->SetFont('', 'B', 8);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('H', 'LETTER');
        $count = 0;
        foreach ($observations as $key => $data) {
            $count++;
            $html = view('partials.PDF.generate_export_pmdu')->with(compact('data', 'count'))->render();
            PDF::SetFont('', '', 10);
            PDF::writeHTML($html, true, false, true, false, '');
        }
        

        PDF::SetTitle("Reporte general de observaciones PMDU.pdf");
        PDF::Output("Reporte general de observaciones PMDU.pdf");
    }
}
