<?php

namespace App\Http\Controllers\Administration;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use PDF;
use Carbon\Carbon;

//Repositories
use App\Repositories\OromapasRepository;
use App\Repositories\AdministrationRepository;

class GeoController extends Controller
{
    public function __construct(
        OromapasRepository $OromapasRepository,
        AdministrationRepository $administrationRepository
    )
    {
        $this->OromapasRepository = $OromapasRepository;
        $this->administrationRepository = $administrationRepository;
    }

    public function index()
    {
        $localidades =  $this->administrationRepository->getAllLocationsCarto();
        return view("moduls.mapa.index", compact('localidades'));
    }
    
    public function registerMarker(Request $request)
    {
        if($this->administrationRepository->registerMarker($request)){
            return redirect()->back()->with("alert", true);
        }else{
            return redirect()->back()->with("alert", false);
        }
    }
    
    public function getDataMarker(Request $request)
    {
        $marker = $this->administrationRepository->getDataMarker($request);
        return response()->json($marker);
    }
    
    public function getDataReports(Request $request)
    {
        $data = $this->administrationRepository->getGeoDataReports($request);
        return response()->json($data);
    }

    public function updateMarker(Request $request)
    {
        $marker = $this->administrationRepository->updateMarker($request);
        return redirect()->back()->with("alert", true);
    }

    public function viewReport()
    {
        $data = $this->administrationRepository->getDataReport();
        return view("moduls.mapa.report", compact("data"));
    }

    public function printReport(Request $request)
    {
        // dd($request);
        $date_now = Carbon::now('America/Mexico_City');
        $this->administrationRepository->saveImageMapa($request->image);
        PDF::SetCreator("IMPLAN");
        PDF::SetAuthor('SIIM');
        PDF::SetTitle('Mapas');
        PDF::SetSubject('Mapas');
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::SetHeaderMargin(0);
        PDF::SetFooterMargin(0);
        PDF::SetAutoPageBreak(TRUE, 22);
        PDF::setFooterData(array(0,64,0), array(0,64,128));
        PDF::setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        PDF::setHeaderCallback(function($pdf){
            //$pdf->Image($imagen, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            // get the current page break margin
            $bMargin = $pdf->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $pdf->getAutoPageBreak();
            // disable auto-page-break
            $pdf->SetAutoPageBreak(false, 0); 
            // set bacground image
            $imagen = public_path('assets/images/mem_hor-implan.png');
            $pdf->Image($imagen, 0,0, 279, 216, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $pdf->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $pdf->setPageMark();
            $pdf->SetFont('', 'B', 16);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Información Mapa", 0, 0, 'C');
            $pdf->SetFont('', 'B', 12);
            $pdf->SetY(8);
            $pdf->Cell(0, 0, "Fecha de Creación: ". Carbon::now("America/Mexico_City")->format("d / M / Y"), 0, 0, 'R');
        });
        PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        PDF::AddPage('L', 'LETTER');
        $imagen = public_path('assets/graphics/mapa.png');
        PDF::Image($imagen, 15, 25, 335, 216, '', '', '', true, 300, '', false, false, 1, false, false, false);
        // PDF::Image($imagen, 0, 0, 279, 216, '', '', '', false, 300, '', false, false, 0);
        PDF::SetTitle("Estadisticas Acciones ADM a ".$date_now->format('d-M-Y').".pdf");
        PDF::Output("Estadisticas Acciones ADM a ".$date_now->format('d-M-Y').".pdf");
    }

    public function registerPersonFilter(Request $request)
    {
        $person = $this->administrationRepository->registerPersonFilter($request);
        // dd($person);
        if($person){
            return response()->json($person);
        }else{
            return response()->json(false);
        }
    }
    public function registerDosificador(Request $request)
    {
        $person = $this->administrationRepository->registerDosificador($request);
        // dd($person);
        if($person){
            return response()->json($person);
        }else{
            return response()->json(false);
        }
    }
    
    public function registerApoyoHospital(Request $request)
    {
        $person = $this->administrationRepository->registerApoyoHospital($request);
        // dd($person);
        if($person){
            return response()->json($person);
        }else{
            return response()->json(false);
        }
    }
    
    public function registerPorciones(Request $request)
    {
        $person = $this->administrationRepository->registerPorciones($request);
        // dd($person);
        if($person){
            return response()->json($person);
        }else{
            return response()->json(false);
        }
    }
    
    public function registerReport(Request $request)
    {
        $report = $this->administrationRepository->registerReport($request);
        // dd($report);
        if($report){
            return response()->json($report);
        }else{
            return response()->json(false);
        }
    }

    public function getUnits()
    {
        $units = $this->administrationRepository->getUnits();
        return response()->json($units);
    }
}
