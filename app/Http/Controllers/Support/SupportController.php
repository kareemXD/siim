<?php

namespace App\Http\Controllers\Support;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Repositories
use App\Repositories\AdministrationRepository;

class SupportController extends Controller
{
    private $administrationRepository;

    public function __construct(
        AdministrationRepository $administrationRepository
    ){
        $this->administrationRepository = $administrationRepository;
    }

    public function getTicketData(Request $request)
    {
        $ticket = $this->administrationRepository->getTicketById($request->id);
        return response()->json($ticket);
    }
    
    public function changeTicketStatus(Request $request)
    {
        $ticket = $this->administrationRepository->changeTicketStatus($request->id, $request->status);
        return response()->json($ticket);
    }
    
    public function finishTicket(Request $request)
    {
        $ticket = $this->administrationRepository->finishTicket($request->id, $request->comment);
        return redirect()->back()->with("alert", "Se termino correctamente un ticket.");
    }
    
    public function support(Request $request)
    {
        $tickets = $this->administrationRepository->getAllTickets();
        $priorities = $this->administrationRepository->getAllPriorities();
        return view('support.support', compact('tickets', 'priorities'));
    }

    public function storeTicket(Request $request)
    {
        $ticket = $this->administrationRepository->storeTicket($request);
        return redirect()->back()->with("alert", $ticket);
    }
}
