<?php

namespace App\Http\Controllers\Support;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;
use Illuminate\Support\Facades\Mail;
use App\Mail\OfficeNotification;
//Repositories
use App\Repositories\AdministrationRepository;
use Carbon\Carbon;

class OfficeController extends Controller
{
    private $administrationRepository;

    public function __construct(
        AdministrationRepository $administrationRepository
    )
    {
        $this->administrationRepository = $administrationRepository;
    }

    public function index()
    {
        $oficios = $this->administrationRepository->getAllOffices();
       // $oficios->resp = $oficios->responsable()->where("type", "1")->get();
        // foreach ($oficios as $oficio) 
        // {
        //     if($oficio->fecha_recepcion)
        //     {
        //         $fecha_recepcion = Carbon::parse($oficio->fecha_recepcion);
        //         $oficio->fecha_recepcion = $fecha_recepcion->format('d/M/Y ');
        //     }
            
        //     if($oficio->fecha_emision)
        //     {
        //         $fecha_emision = Carbon::parse($oficio->fecha_emision);
        //         $oficio->fecha_emision = $fecha_emision->format('d/M/Y ');
        //     }
        // }
       
        return view('moduls.offices.all_offices', compact('oficios'));
    }

    public function store(Request $request)
    {   
        // dd($request);
        if($request->tipo == 1)
        {
            $this->validate($request, [
                //'no_oficio' => 'required|unique:oficios',
                'oficio' => 'mimes:pdf|max:5120',
                'emisor' => 'required',
                'fecha_emision' => 'required',
                'asunto' => 'required',
            ],[
                'oficio.mimes' => 'El oficio debe ser formato PDF',
                'oficio.max' => 'El oficio debe ser de tamaño maximo 5mb',
                'no_oficio.unique' => 'El numero de oficio ya fue registrado',
                'emisor.required' => 'Se requiere un emisor',
                'fecha_emision.required' => 'Se requiere fecha de emision',
                'fecha_recepcion.required' => 'Se requiere fecha de recepcion',
                'asunto.required' => 'Se requiero asunto',
            ]
            );
        }
        else if($request->tipo == 0)
        {
            if($request->pmdu_field){
                $this->validate($request, [
                    // 'no_oficio' => 'required',
                    'oficio' => 'required|mimes:pdf|max:5120',
                    'emisor' => 'required',
                    'fecha_emision' => 'required',
                    'fecha_recepcion' => 'required',
                    'asunto' => 'required',
                ],[
                    'oficio.mimes' => 'El oficio debe ser formato PDF',
                    'oficio.max' => 'El oficio debe ser de tamaño maximo 5mb',
                    // 'no_oficio.unique' => 'El numero de oficio ya fue registrado',
                    'emisor.required' => 'Se requiere un emisor',
                    'fecha_emision.required' => 'Se requiere fecha de emision',
                    'fecha_recepcion.required' => 'Se requiere fecha de recepcion',
                    'asunto.required' => 'Se requiero asunto',
                ]);
            }else {
                $this->validate($request, [
                    'no_oficio' => 'required',
                    'oficio' => 'required|mimes:pdf|max:5120',
                    'emisor' => 'required',
                    'fecha_emision' => 'required',
                    'fecha_recepcion' => 'required',
                    'asunto' => 'required',
                ],[
                    'oficio.mimes' => 'El oficio debe ser formato PDF',
                    'oficio.max' => 'El oficio debe ser de tamaño maximo 5mb',
                    'no_oficio.unique' => 'El numero de oficio ya fue registrado',
                    'emisor.required' => 'Se requiere un emisor',
                    'fecha_emision.required' => 'Se requiere fecha de emision',
                    'fecha_recepcion.required' => 'Se requiere fecha de recepcion',
                    'asunto.required' => 'Se requiero asunto',
                ]);
            }

        }
        else if($request->tipo == 2)
        {
            $this->validate($request, [
                //'no_oficio' => 'required|unique:oficios',
                'oficio' => 'mimes:pdf|max:5120',
                'emisor' => 'required',
                'fecha_emision' => 'required',
                'asunto' => 'required',
            ],[
                'oficio.mimes' => 'El oficio debe ser formato PDF',
                'oficio.max' => 'El oficio debe ser de tamaño maximo 5mb',
                'no_oficio.unique' => 'El numero de oficio ya fue registrado',
                'emisor.required' => 'Se requiere un emisor',
                'fecha_emision.required' => 'Se requiere fecha de emision',
                'fecha_recepcion.required' => 'Se requiere fecha de recepcion',
                'asunto.required' => 'Se requiero asunto',
            ]
            );
        }         
        $data = $this->administrationRepository->storeOffice($request);
        return redirect()->route('offices')->with('alert', 'Oficio registrado con exito');       
    }

    public function getOffice(Request $request)
    {
        $office = $this->administrationRepository->getOfficeById($request->id);
        $office->encargado = $office->responsable()->where('type',0)->first();
        $office->withCopy = $office->responsable()->where('type', 1)->get();
        $office->resp = $office->response;
        return $office;
    }

    public function downloadOffice($id)
    {
        $office = $this->administrationRepository->getOfficeById($id);
        $office->no_oficio = str_replace('/', '-', $office->no_oficio);
        return Storage::disk("public")->download("oficios/".$office->no_oficio.'-'.$office->id.'.pdf' );
    }

    public function storeAnswer(Request $request)
    {             
        $this->validate($request, [
            'no_oficio_r' => 'required',
            'oficio' => 'required|mimes:pdf',
            'emisor_r' => 'required',
            'fecha_emision_r' => 'required',
            'fecha_recepcion_r' => 'required',
            'asunto_r' => 'required',
        ],[
            'oficio.mimes_r' => 'El oficio debe ser formato PDF',
            'no_oficio.unique_r' => 'El numero de oficio ya fue registrado'
        ]
        );     

     
                                
        $this->administrationRepository->storeAnswerOffice($request);
        return redirect()->route('offices')->with('alert', 'Oficio registrado con exito');
        
    }

    public function changeStatus(Request $request)
    {
        $this->administrationRepository->changeStatusByid($request);
        return redirect()->route('offices')->with('alert', 'Oficio marcado como leido correctamente');
    }

    public function checkOffice ($id = null)
    {
        $oficios = $this->administrationRepository->getAllOffices();
        $details = "";
        if($id != null)
        {
            $details = $this->administrationRepository->getOfficeById($id);
            $details->withCopy = $details->responsable()->where('type', 1)->get();
            $details->encargado = $details->responsable()->where('type',0)->first();
        }
        return view('moduls.offices.all_offices', compact('details', 'oficios'));
    }

    public function alertNotification(Request $request)
    {   
        $this->administrationRepository->sendNotification($request);
        return "Recordatorio enviado corrrectamente"; 
    }

    public function deleteOffice(Request $request)
    {
        $this->administrationRepository->statusDelete($request);
    }
    public function restoreOffice(Request $request)
    {
        $this->administrationRepository->statusRestore($request);
    }
}
