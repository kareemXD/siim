<?php

namespace App\Http\Controllers\Tracing;

use Auth;

use PDF;
use carbon\Carbon;
use Storage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


//Repositories
use App\Repositories\TracingRepository;
use App\Repositories\PlanningRepository;
use App\Repositories\BitacoraRepository;


class TracingController extends Controller
{
    private $tracingRepository,
            $planningRepository,
            $bitacoraRepository;

    public function __construct(
        TracingRepository $tracingRepository,
        PlanningRepository $planningRepository,
        BitacoraRepository $bitacoraRepository
    ){
        $this->tracingRepository = $tracingRepository;
        $this->planningRepository = $planningRepository;
        $this->bitacoraRepository = $bitacoraRepository;
    }

    //VARIABLES PUBLICAS
    
    //Funcion para mostrar todas las actividades que ha dado de alta cada direccion
    public function monthlyReport($direction_id = null)
    {
        
        $meses_nor = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $meses_orden=['9', '10', '11', '0', '1', '2', '3', '4', '5', '6', '7', '8'];
        $meses = [];
        $period = intval(config('implan.periods.previous'));
        foreach ($meses_orden as $key => $mes) {
            $num_mes = $mes + 1;
            if($mes > 8)
            {
                $meses[$mes] = [
                    'name' => $meses_nor[$mes],
                    'num' => $num_mes."-".($period - 1),
                ];
            }else {
                    $meses[$mes] = [
                        'name' => $meses_nor[$mes],
                        'num' => $num_mes."-".$period,
                    ];
            }
        }
        $locations = $this->tracingRepository->getAllLocations();
        if (!empty($direction_id)) 
        {
            $direction = $this->planningRepository->getDirectionById($direction_id);
        }else {
            $direction = $this->planningRepository->getDirectionById(Auth::user()->profile->position->direction->id);
        }
        
        return view('moduls.tracing.monthly_report', compact('direction', 'meses', 'meses_nor', 'locations'));
    }

    public function storeMonthlyReport(Request $request)
    {
        $this->validate($request, [
            'direction' => 'required',
            'name' => 'required',
            'beneficiaries_men' => 'required',
            'beneficiaries_women' => 'required',
            'amount' => 'required',
            'mounth' => 'required',
            'location' => 'required',
            'description' => 'required',
        ]);
        $response = $this->tracingRepository->storeMonthlyReport($request);
        
        //Reportamos en la bitacora el inicio de Session
        $meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $date = Carbon::now("America/Mexico_City");
        $user = Auth::user();
        $action = [
            "user" => $user->id,
            'type' => config("implan.bitacora.update_monthly_report"),
            'title' => "El usuario ". $user->name . " realizo una captura para el Informe Mensual el día  " .$date->format("d/m/Y"). " a las " .$date->format("H:i"),
            'action' => "El usuario ". $user->name . " realizo una captura para el Informe Mensual, con nombre '".$request->name."' para el mes de ".$meses[explode("-",$request->mounth)[0] - 1].", el día  " .$date->format("d/m/Y"). " a las " .$date->format("H:i"),
            "direction" => $user->profile->position->direccion_id,
        ];
        $this->bitacoraRepository->insertAction($action);
        //Fin del reporte de inicio de sesión

        return redirect()->route("monthly.report")->with("response", $response);
    }

    public function getMonthlyActivityById(Request $request)
    {
        $activity = $this->tracingRepository->getMonthlyActivityById($request->id);
        foreach ($activity->evidences as $key => $evidence) 
        {
            switch ($evidence->extencion) {
                case 'pdf':
                    $data = asset("/assets/plugins/preview/preview_pdf.png");
                    $evidence->storage = $data;
                    break;
                case 'txt':
                case 'rtf':
                case 'text':
                case 'plain':
                    $data = asset("/assets/plugins/preview/preview_txt.png");
                    $evidence->storage = $data;
                    break;
                case 'zip':
                case 'tgz':
                    $data = asset("/assets/plugins/preview/preview_zip.png");
                    $evidence->storage = $data;
                    break;
                case 'doc':
                case 'docx':
                    $data = asset("/assets/plugins/preview/preview_doc.png");
                    $evidence->storage = $data;
                    break;
                case 'xls':
                case 'xlx':
                case 'xlsx':
                    $data = asset("/assets/plugins/preview/preview_xls.png");
                    $evidence->storage = $data;
                    break;
                default:
                    $data = url("storage/".$evidence->url.$evidence->nombre.".".$evidence->extencion);
                    $evidence->storage = $data;
                    break;
            }
        }
        $activity->inf_evidences = $activity->evidences;
        $place = [];
        foreach ($activity->locations as $key => $location) 
        {
            $place[] = $location->id;
        }
        $activity->location = $place;
    
        return $activity;
    }

    public function downloadEvidenceById($id)
    {
        $meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

        $evidence = $this->tracingRepository->getEvidenceById($id);
        return Storage::disk("public")->download($evidence->url.$evidence->nombre.".".$evidence->extencion, "Evidencia-de-".$meses[$evidence->activity->mes - 1]."-".$evidence->id.".".$evidence->extencion);
    }


    /************************* Segimiento del POA *************************/

    public function poa()
    {
        $trimester = $this->tracingRepository->getActualTrimester();
        // dd($trimester);
        return view('moduls.tracing.tracing_poa', compact("trimester"));
    }

    public function getActionPoaById(Request $request)
    {
        $action = $this->planningRepository->getActionPoaById($request->action);
        $action->sub = $action->subactions;
        return $action;
    }

    public function storeActionGoals(Request $request)
    {
        $action = $this->tracingRepository->storeActionGoals($request);
        $trimester = $this->tracingRepository->getActualTrimester();
        //Reportamos en la bitacora el inicio de Session
        $meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $date = Carbon::now("America/Mexico_City");
        $user = Auth::user();
        $action_r = [
            "user" => $user->id,
            'type' => config("implan.bitacora.update_POA"),
            'title' => "El usuario ". $user->name . " realizo una captura para el Avance del POA el día  " .$date->format("d/m/Y"). " a las " .$date->format("H:i"),
            'action' => "El usuario ". $user->name . " realizo una captura para el Avance del POA en la actividad con nombre '".$action->actividad."' para el ".$trimester["label"].", el día  " .$date->format("d/m/Y"). " a las " .$date->format("H:i"),
            "direction" => $user->profile->position->direccion_id,
        ];
        $this->bitacoraRepository->insertAction($action_r);
        //Fin del reporte de inicio de sesión
        return redirect()->back()->with("alert", $action->actividad." se actualizo con exito.");
    }

     /************************* Segimiento del POA Administracion *************************/

    public function advancePoas()
    {
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.tracing.advance_poas", compact("directions"));
    }

    public function viewPoaAdvance(Request $request)
    {
        $direction = $this->planningRepository->getDirectionById($request->direction);
        $trimester = $this->tracingRepository->getActualTrimester($request->trimester);
        $trimester["label"] = str_replace("2021", "2020", $trimester["label"]);
        return view("moduls.tracing.advance_poa_direction", compact("direction", "trimester"));
    }

    public function advanceInafed()
    {
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.tracing.advance_inafed", compact("directions"));
    }
    
    public function advanceAgenda2030()
    {
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.tracing.advance_agenda2030", compact("directions"));
    }
    
    public function advanceMir()
    {
        $directions = $this->planningRepository->getAllDirections();
        return view("moduls.tracing.advance_mir", compact("directions"));
    }
}
