<?php

namespace App\Http\Controllers\Tracing;
use Auth;

use PDF;
use carbon\Carbon;
use Storage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\NewTracingRepository;
use App\Repositories\NewPlanningRepository;
use App\Repositories\BitacoraRepository;

class NewTracingController extends Controller
{
    private $newPlanningRepository, $newTracingRepository, $bitacoraRepository;

    public function __construct(
        NewTracingRepository $newTracingRepository,
        NewPlanningRepository $newPlanningRepository,
        BitacoraRepository $bitacoraRepository
    ){
         $this->newTracingRepository = $newTracingRepository;
        $this->newPlanningRepository = $newPlanningRepository;
        $this->bitacoraRepository = $bitacoraRepository;
    }

    //ver el avance trimestral de la mir
    public function avanceMirs()
    {
        $directions = $this->newPlanningRepository->getAllDirections();
        return view("moduls.new-tracing.avance-mirs", compact("directions"));
    }

    public function viewMirAdvance(Request $request)
    {
        $direction = $this->newPlanningRepository->getDirectionById($request->direction);
        $trimestre = $this->newTracingRepository->getActualTrimestre($request->trimestre);
        return view("moduls.new-tracing.advance_mir_direction", compact("direction", "trimestre"));
    }

    public function getActionMirById(Request $request)
    {
        $action = $this->newPlanningRepository->getActionMirById($request->action);
        return $action;
    }

    public function storeActionGoalsMir(Request $request)
    {
        $action = $this->newTracingRepository->storeActionGoals($request);
        $trimestre = $this->newTracingRepository->getActualTrimestre($request->trimestre);
        //Reportamos en la bitacora el inicio de Session
        $meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        $date = Carbon::now("America/Mexico_City");
        $user = Auth::user();
        $action_r = [
            "user" => $user->id,
            'type' => config("implan.bitacora.update_POA"),
            'title' => "El usuario ". $user->name . " realizo una captura para el Avance de la MIR el día  " .$date->format("d/m/Y"). " a las " .$date->format("H:i"),
            'action' => "El usuario ". $user->name . " realizo una captura para el Avance de la Mir en la actividad con nombre '".$action->actividad."' para el ".$trimestre["label"].", el día  " .$date->format("d/m/Y"). " a las " .$date->format("H:i"),
            "direction" => $user->profile->position->direccion_id,
        ];
        $this->bitacoraRepository->insertAction($action_r);
        //Fin del reporte de inicio de sesión
        return redirect()->back()->with("alert", $action->actividad." se actualizo con exito.");
    }

    public function mirAdvance()
    {
        $direction = Auth::user()->profile->position->direction;
        $trimestre = $this->newTracingRepository->getActualTrimestre();
        return view("moduls.new-tracing.advance_mir_direction", compact("direction", "trimestre"));
    }
}