<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//Repositories
use App\Repositories\AuthRepository;

class AuthController extends Controller
{
    private $authRepository;

    public function __construct(
        AuthRepository $authRepository
    ){
        $this->authRepository = $authRepository;
    }

    /************************** Users Administration **********************************/

    public function users()
    {
        $users = $this->authRepository->getActiveUsers();
        $roles = $this->authRepository->getAllRoles();
        $positions = $this->authRepository->getAllPositions();
        return view("auth.users", compact("users", "roles", "positions"));
    }

    public function newUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'ape_pat' => 'required|string|max:255',
            'ape_mat' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'position' => 'required',
            'role' => 'required',
        ],
        [
            "name.required" => "EL nombre del usuario es Requerido.",
            "ape_pat.required" => "EL apellido del usuario es Requerido.",
            "ape_mat.required" => "EL apellido de usuario es Requerido.",
            "email.unique" => "Este email ya existe.",
            "password.required" => "Por favor ingresa una contraseña valida.",
            "password.confirmed" => "La confimacion de contraseña es invalida.",
            "password.min" => "La contraseña debe contener minimo 6 caracteres.",
            "position.required" => "El rol es requerido.",
            "role.required" => "El rol es requerido."
        ]);
        $users = $this->authRepository->newUser($request);

        return redirect()->back()->with("alert", "Se registro correctamente un nuevo usuario.");
    }

    public function updateProfile(Request $request)
    {
        if(!empty($request->password)){
            $this->validate($request, [
                'id' => 'required',
                'name' => 'required|string|max:255',
                'ape_pat' => 'required|string|max:255',
                'ape_mat' => 'required|string|max:255',
                'password' => 'required|string|min:6|confirmed',
                'position' => 'required',
                'role' => 'required'
            ],
            [
                "name.required" => "EL nombre del usuario es Requerido.",
                "ape_pat.required" => "EL apellido del usuario es Requerido.",
                "ape_mat.required" => "EL apellido de usuario es Requerido.",
                "password.required" => "Por favor ingresa una contraseña valida.",
                "password.confirmed" => "La confimacion de contraseña es invalida.",
                "password.min" => "La contraseña debe contener minimo 6 caracteres.",
                "position.required" => "El rol es requerido.",
                "role.required" => "El rol es requerido."
            ]);

            $this->authRepository->updateUser($request);
        }else
        {
            $this->validate($request, [
                'id' => 'required',
                'name' => 'required|string|max:255',
                'ape_pat' => 'required|string|max:255',
                'ape_mat' => 'required|string|max:255',
                'position' => 'required',
                'role' => 'required'
            ],
            [
                "name.required" => "EL nombre del usuario es Requerido.",
                "ape_pat.required" => "EL apellido del usuario es Requerido.",
                "ape_mat.required" => "EL apellido de usuario es Requerido.",
                "position.required" => "El rol es requerido.",
                "role.required" => "El rol es requerido."
            ]);
            $this->authRepository->updateUserName($request);
        }
        return redirect()->back();
    }
    
    public function updateProfileClient(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
            'password' => 'required|string|min:6|confirmed',
        ],
        [
            "password.required" => "Por favor ingresa una contraseña valida.",
            "password.confirmed" => "La confimacion de contraseña es invalida.",
            "password.min" => "La contraseña debe contener minimo 6 caracteres.",
        ]);
        $this->authRepository->updateProfileClient($request);
       
        return redirect()->back();
    }
    
    public function getUserById(Request $request)
    {
        $user = $this->authRepository->getUserById($request->id);
        $user->role = $user->getRoleNames()[0];
        $user->profile = $user->profile;

        return $user;
    }

    /************************** Roles Administration **********************************/

    public function roles()
    {
        $roles = $this->authRepository->getAllRoles();
        return view("auth.roles", compact('roles'));
    }

    public function newRole()
    {
        $menus = $this->authRepository->getAllMenu();
        return view("auth.new_role", compact("menus"));
    }

    public function storeRole(Request $request)
    {
        $this->authRepository->storeRole($request);
        return redirect()->route("roles")->with("alert", "Se creo correctamente el Rol");
    }

    public function editRole($id)
    {
        $rol = $this->authRepository->getRolById($id);
        $menus = $this->authRepository->getAllMenu();
        $ids = [];

        $rolelv = $this->authRepository->getRolLavByName($rol->name);
        foreach ($rol->menus as $key => $menu) {
            $ids[] = $menu->id;
        }
        $rol->ids = $ids;
        return view("auth.edit_role", compact("rol", "menus", "rolelv"));
    }

    public function updateRole(Request $request)
    {
        $this->authRepository->updateRole($request);
        return redirect()->route("roles")->with("alert", "Se actualizó correctamente el Rol");
    }

    public function deactivateUser(Request $request)
    {
        $this->authRepository->deactivateUser($request->id);
        return response()->json("true");
    }
}
