<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Plugins 
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Auth;

use App\Repositories\SecretRepository;

class SecretController extends Controller
{
    private $secretRepository;

    public function __construct(
        SecretRepository $secretRepository
    ){
        $this->secretRepository = $secretRepository;
    }

    public function createRoles($password = null)
    {
        if($password != null)
        {
            if($password == "SistemasImplan18")
            {
                Role::create(['name' => 'Administrador']);
                Role::create(['name' => 'Cliente']);

                dd("Acción Terminada con Exíto");
            }else
            {
                dd('Acción Denegada');
            }
        }else
        {
            dd('Acción Denegada');
        }
    }

    public function assignAdministrator($password = null)
    {
        if($password != null)
        {
            if($password == "SistemasImplan18")
            {
                if(Auth::check())
                {
                    $user = Auth::user();
                    $user->assignRole('Administrador');
                    
                    dd("Acción Terminada con Exíto");
                }else {
                    dd('Acción Denegada');
                }
            }else
            {
                dd('Acción Denegada');
            }
        }else
        {
            dd('Acción Denegada');
        }
    }

    public function cleanPmdActions()
    {
        if(Auth::check())
        {
            if(Auth::user()->hasRole("Administrador"))
            {
                $data = $this->secretRepository->cleanPmdActions();
                dd($data);
            }else {
                dd('Acción Denegada');
            }
        }else {
            dd('Acción Denegada');
        }
    }

    public function makeNewPeriod()
    {
        if(Auth::check()){
            if(Auth::user()->hasRole('Administrador')){
                if($this->secretRepository->makeNewPeriod()){
                    dd("Acción Terminada con Éxito");
                }else {
                    dd("Acción Terminada con Éxito");
                }
            }
        }
    }
    
    public function cleanMirs($password = null, $period)
    {
        if($password != null) {
            if($password == "SistemasImplan18") {
                if(Auth::check()) {
                    if($this->secretRepository->cleanMirs($period)){
                        dd("Acción Terminada con Éxito");
                    }else {
                        dd("Acción Terminada sin Éxito");
                    }
                }else {
                    dd('Acción Denegada');
                }
            }
        }
    }
}
