<?php

namespace App\Http\Controllers\WebSite;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Repositories\WebSiteRepository;

class WebSiteController extends Controller
{
    private $webSiteRepository;
    
    public function __construct(
        WebSiteRepository $WebSiteRepository
    )
    {
        $this->webSiteRepository = $WebSiteRepository;
    }


    public function index()
    {
        $news = $this->webSiteRepository->getAllNews();
        return view("moduls.notices.all_notices", compact("news"));
    }

    public function newNotice()
    {
        return view("moduls.notices.new_notice");
    }

    public function storeNotice(Request $request)
    {
        $this->validate($request, [
            'titulo' => 'required',
            'descripcionCompleta' => 'required',
            'descripcionEncaebzado' => 'required',
            'descripcionCorta' => 'required',
            'file' => 'required',
            'title' => 'required',
            'shortDescription' => 'required',
            'headertDescription' => 'required',
            'completeDescription' => 'required',
        ],[
            'titulo.required' => 'El titulo en español es requerido',
            'descripcionCompleta.required' => 'La descripcion completa en español es requerido',
            'descripcionEncaebzado.required' => 'La descripcion del encabezado en español es requerido',
            'descripcionCorta.required' => 'La descripcion corta en español es requerido',
            'file.required' => 'El archivo es requerido',
            'title.required' => 'El titulo en ingles es requerido',
            'shortDescription.required' => 'La descripcion corta en ingles es requerido',
            'headertDescription.required' => 'La descripcion del encabezado en ingles es requerido',
            'completeDescription.required' => 'La descripcion completa en ingles es requerido',
        ]
        );

        $this->webSiteRepository->storeNew($request);
        return view("moduls.notices.new_notice")->with('alert', 'Noticia registrada con exito');
    }
}
