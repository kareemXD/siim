<?php

return [
    "periods" => [
        'next' => 2022, 
        'actual' => 2021, 
        'previous' => 2020,
        'planning' => 2021,
        'tracing' => 2020,
    ],
    "times" => [
        'planning' => true, 
    ],
    "metakeys" => [
        'description' => "descripcion",
        'budget' => "presupuesto",
    ],
    'metas' => [
        'valor' => [
            'absoluto' => '0',
            'relativo' => '1',
        ],
        'tipo' => [
            'entero' => '0',
            'decimal' => '1',
        ],
        'orientacion' => [
            'positiva' => '0',
            'negativa' => '1',
            'neutra' => '2', 
        ],
    ],
    'bitacora' => [
        "login" => "0",
        "update_POA" => "1",
        "update_monthly_report" => "2",
        "update_settings" => "3",
        "generate_poa" => "4",
    ],
    'offices' => [
        'status' => [
            'not_checking' => 'No revisado',
            'waiting_answer' => 'Esperando respuesta',
            'complete' => 'Realizado',
            'waiting_office' => 'En espera de oficio',
            'cancelled' => 'Cancelado'
        ],
        'types' => [
            'internal' => 'Recibido',
            'external' => 'Enviado',
            'answer' => 'Archivo de respuesta'
        ] 
    ],
];
